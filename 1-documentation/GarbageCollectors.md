The default GarbageCollector from Java 11 upwards is the
Z GarbageCollector. It is suited for most programs.

The ZGC GarbageCollector, stabilized in Java x, is especially suitable for realtime applications.
It targets extremely low pause times. It can be enabled using the following VM option:

    -XX:+UseZGC

Additional VM options regarding garbage collection are:

    -Xmx1024m
    -Xlog:gc*
