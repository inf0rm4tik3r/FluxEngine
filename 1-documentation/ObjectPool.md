1. Implement the `Reusable<T>` interface like this:

        public MyClass implements Reusable<MyClass>

2. Add the following member variables to your class:

        /**
        * ObjectPool that provides objects to the application and can store used objects for later reuse.
        */
        private static final @NotNull ObjectPool<MyClass> POOL;
        
        /**
        * The strategy which is used to reset an object of this class.
        */
        private static @NotNull AbstractReuseStrategy<MyClass> reuseStrategy;
        
        /**
        * The default reuse strategy to which the user can fall back to if he changed the {@code reuseStrategy}.
        */
        public static final @NotNull AbstractReuseStrategy<MyClass> DEFAULT_REUSE_STRATEGY;

3. Add this static initializer code to your class:

        static {
            POOL = new ObjectPool<>(() -> {
                return new MyClass();
            });
            
            DEFAULT_REUSE_STRATEGY = new AbstractReuseStrategy<>() {
                @Override
                public MyClass reset(final MyClass object) {
                    return object.myResetMethod();
                }
            };
            reuseStrategy = DEFAULT_REUSE_STRATEGY;
        }

4. Make sure you only have a single private constructor in your class:

        /**
         * Private default constructor. Gets called by the {@code acquire} method if the pool is empty.
         */
        private MyClass() {
            // TODO: Do your necessay initialization.
            // Keep this at a minimum as the object should be indefinetly resettable by its reset method.
        }

5. Define your reset method (which gets called by our AbstractReuseStrategy defined in the static initializer):

        public MyClass myResetMethod() {
            // TODO: reset this object
            return this;
        }

6. Add the following methods to your class:

        /**
         * Retrieves an object of this class from the object pool. See {@link ObjectPool#acquire()} for more information.
         *
         * @return The object. Loaded from the underlying pool or created new.
         *
         * @see ObjectPool#acquire()
         */
        public static MyClass acquire() {
            return POOL.acquire();
        }
        
        /**
         * Frees the calling object. See {@link ObjectPool#free(Reusable)} for more information.
         *
         * @see ObjectPool#free(Reusable)
         */
        public void free() {
            POOL.free(this);
        }
        
        /**
         * 
         */
        @Override
        public MyClass reset() {
            return reuseStrategy.reset(this);
        }
        
        /**
         * Changes the active {@link AbstractReuseStrategy} which determines how objects are treated when freed.
         * 
         * @param reuseStrategy
         *     The new strategy to use when resetting objects of this class.
         */
        public static void setReuseStrategy(final @NotNull AbstractReuseStrategy<MyClass> reuseStrategy) {
            MyClass.reuseStrategy = reuseStrategy;
        }
        
        /**
         * @return The current {@link AbstractReuseStrategy}.
         */
        public static @NotNull AbstractReuseStrategy<MyClass> getReuseStrategy() {
            return reuseStrategy;
        }
        
        /**
         * Restores the {@code DEFAULT_REUSE_STRATEGY} implemented by the author of this class.
         */
        public static void restoreDefaultReuseStrategy() {
            reuseStrategy = DEFAULT_REUSE_STRATEGY;
        }
        
7. You can now retrieve instances of your class by calling:

        final MyClass myObject = MyClass.acquire() // This object is guaranteed to be in its "resetted" state.
        
8. Do not forget to always call:

        myObject.free()
        
   If you are done using your object. It will then get back to the POOL to be retrieved again.
