**REMEMBER**

- `ColibriEngine.initializeLibraries()` must be called once 
  before any instance can be created.


**How should the engine be used?:**

1) It must be "ready to use". Create an instance -> add camera and objects -> render.
2) It must be vastly configurable. Adding multiple windows, rendering offscreen, ...


**How can an instance of the ColibriEngine be acquired?:**

1) By creating a new object: new ColibriEngine();
    a) This 
    

**Getting a context ready for automated rendering:**

1) Create a new ColibriEngine object.
2) Set the engine to the automated rendering flow.
   A `AbstractWorker` object is then needed which lets you specify the workload.

    
**Getting a context which is ready for custom rendering:**

1) Create a new ColibriEngine object.
2) Add a window to the window manager.
3) Set the engine to the manual rendering flow.
