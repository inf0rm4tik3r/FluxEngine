package de.colibriengine.algorithms;

import de.colibriengine.algorithms.sort.MergeSort;

import java.util.Arrays;

import de.colibriengine.algorithms.sort.MergeSort;

public class SortTest {
    
    public static void main(String[] args) {
        final Integer[] data = new Integer[]{4,2,3,5,7,274,5,2457,35,72,3,572,54,4257,245,54,34,42,15,3,4534,2,23,5,34};
        System.out.println(Arrays.toString(data));
        
        final Integer[] sorted = MergeSort.sort(data, Integer.class);
        System.out.println(Arrays.toString(sorted));
    }
    
}
