package de.colibriengine.algorithms.sort;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Array;

/**
 * Implementation of the MergeSort algorithm.
 *
 * @version 0.0.0.1
 * @time 01.08.2018 15:25
 */
public class MergeSort {
    
    public static <T extends  Comparable<T>> T[] sort(final @NotNull T[] data, final @NotNull Class<T> type) {
        return sort(data, type, 0, data.length - 1);
    }
    
    public static <T extends  Comparable<T>> T[] sort(final @NotNull T[] data, final @NotNull Class<T> type,
                                                      final int p, final int q) {
        if (p != q) {
            final int m = (p + q) / 2;
            final T[] left = sort(data, type, p, m);
            final T[] right = sort(data, type, m + 1, q);
            return merge(type, left, right);
        }
        T[] single = newArray(type, 1);
        single[0] = data[p];
        return single;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] newArray(Class<T> type, int length) {
        // Use Array native method to create array of a type only known at run time.
        return (T[]) Array.newInstance(type, length);
    }

    private static <T extends  Comparable<T>> T[] merge(final @NotNull Class<T> type,
                                                        final @NotNull T[] left,
                                                        final @NotNull T[] right) {
        int i = 0;
        int j = 0;
        final int combinedLength = left.length + right.length;
        final T[] sorted = newArray(type, combinedLength);
        
        for (int l = 0; l < combinedLength; l++) {
            if (i >= left.length) { // Did we already took everything from the left side?
                sorted[l] = right[j];
                j++;
            }
            else if (j >= right.length) { // Did we already took everything from the right side?
                sorted[l] = left[i];
                i++;
            }
            else if (left[i].compareTo((T) right[j]) < 0) { // left[i] < right[j]
                sorted[l] = left[i];
                i++;
            }
            else { // left[i] > right[j]
                sorted[l] = right[j];
                j++;
            }
        }
        
        return sorted;
    }
    
}
