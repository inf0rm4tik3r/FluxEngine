package de.colibriengine.asset

import java.io.Closeable

interface Asset : Closeable {

    override fun close()

}
