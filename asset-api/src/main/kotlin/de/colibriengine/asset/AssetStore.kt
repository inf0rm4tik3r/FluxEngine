package de.colibriengine.asset

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.util.RefCountable

class AssetStore<K : Any, V : Any>(
    val name: String,
    private val destroy: V.() -> Unit
) : Iterable<V> {

    private val store: MutableMap<K, RefCountable<V>> = mutableMapOf()

    fun destroy(callDestroyOnRemaining: Boolean = true) {
        for (entry in store.entries) {
            val (asset, refCount) = entry.value
            LOG.warn("Asset ${entry.key} (${asset}) still has a refCount of ${refCount}!")
            if (callDestroyOnRemaining) {
                LOG.info("Trying to destroy it. May work if not in use.")
                asset.destroy()
            }
        }
        store.clear()
    }

    operator fun contains(key: K): Boolean {
        return key in store
    }

    operator fun get(key: K): V? {
        return store[key]?.getAndIncrement()
    }

    fun getOrPut(key: K, defaultValue: () -> V): V {
        return when (key) {
            in store -> store[key]!!.value
            else -> {
                val default = defaultValue()
                set(key, default)
                default
            }
        }
    }

    fun peek(key: K): V? {
        return store[key]?.value
    }

    fun refCountOf(key: K): UInt? {
        return store[key]?.refCount
    }

    fun giveBack(key: K) {
        if (resourceExistsWithPositiveRefCount(key)) {
            val refCountable = store[key]!!.dec()
            if (refCountable.refCount == 0u) {
                LOG.info("RefCount of $key reached zero. Deleting the resource.")
                refCountable.value.destroy()
                store.remove(key)
            }
        } else {
            throw IllegalArgumentException(
                "A resource under the given $key doe not exist. Reference count cannot be lowered!"
            )
        }

    }

    operator fun set(key: K, value: V) {
        if (resourceExistsWithPositiveRefCount(key)) {
            throw IllegalArgumentException(
                "A resource under the given $key already exists " +
                    "with a refCount of ${store[key]!!.refCount} which is > 0. Value cannot be replaced!"
            )
        }
        store[key] = RefCountable(value, 1u)
    }

    private fun resourceExistsWithPositiveRefCount(key: K) = key in store && store[key]!!.refCount > 0u

    override fun iterator(): Iterator<V> {
        val iterator = store.iterator()
        return object : Iterator<V> {
            override fun hasNext(): Boolean = iterator.hasNext()
            override fun next(): V = iterator.next().value.value
        }
    }

    override fun toString(): String {
        return "AssetStore(name='$name', store.size='${store.size}')"
    }

    companion object {
        private val LOG = getLogger<AssetStore<*, *>>()
    }

}
