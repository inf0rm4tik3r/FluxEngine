package de.colibriengine.asset

import java.io.Closeable

interface AssetStream : Closeable {

    override fun close()

}
