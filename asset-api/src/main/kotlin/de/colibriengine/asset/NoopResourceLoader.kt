package de.colibriengine.asset

class NoopResourceLoader : ResourceLoader {

    override fun <T> load(resourceName: ResourceName, type: Class<T>, options: Any?): T {
        throw UnsupportedOperationException("NoopResourceLoader is unable to load resources.")
    }

    override fun <T> stream(resourceName: ResourceName): T {
        throw UnsupportedOperationException("NoopResourceLoader is unable to stream resources.")
    }

}
