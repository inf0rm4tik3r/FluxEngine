package de.colibriengine.asset

import de.colibriengine.asset.image.Image
import de.colibriengine.asset.image.ImageLoadOptions

interface ResourceLoader {

    fun <T> load(resourceName: ResourceName, type: Class<T>, options: Any? = null): T

    fun <T> stream(resourceName: ResourceName): T

}

inline fun <reified T> ResourceLoader.load(resourceName: ResourceName): T {
    return this.load(resourceName, T::class.java, null)
}

fun ResourceLoader.load(resourceName: ResourceName, imageLoadOptions: ImageLoadOptions): Image {
    return this.load(resourceName, Image::class.java, imageLoadOptions)
}
