package de.colibriengine.asset

@JvmInline
value class ResourceName(val s: String) {

    init {
        require(s.isNotEmpty()) { "ResourceName must not be empty!" }
    }

}
