package de.colibriengine.asset.audio

import de.colibriengine.asset.Asset

// TODO: Rename to PCMAudio or something else. Interface does not enforce the implementer to be a WAV file interpreter!
interface Wav : Asset {

    interface WavHeader {
        /** 2 bytes unsigned, 0x0001 (1) to 0xFFFF (65,535). */
        val numChannels: Int

        /**
         * 4 bytes unsigned, 0x00000001 (1) to 0xFFFFFFFF (4,294,967,295).
         * Although a java int is 4 bytes, it is signed, so need to use a long.
         **/
        val sampleRate: Long // TODO: Use Kotlins unsigned types

        /** 2 bytes unsigned, 0x0001 (1) to 0xFFFF (65,535). */
        val blockAlign: Int

        /** 2 bytes unsigned, 0x0002 (2) to 0xFFFF (65,535). */
        val validBits: Int
    }

    val header: WavHeader

    /** Number of bytes required to store a single sample. */
    val bytesPerSample: Int

    /** Number of frames within the data section. */
    val numFrames: Long

    fun logDebugInfo()

    fun readFrames(into: ShortArray, amount: Long, offset: Int = 0): Long
    fun readFrames(into: IntArray, amount: Long, offset: Int = 0): Long
    fun readFrames(into: LongArray, amount: Long, offset: Int = 0): Long
    fun readFrames(into: DoubleArray, amount: Long, offset: Int = 0): Long
    fun readFrames(into: Array<ShortArray>, amount: Long, offset: Int = 0): Long
    fun readFrames(into: Array<IntArray>, amount: Long, offset: Int = 0): Long
    fun readFrames(into: Array<LongArray>, amount: Long, offset: Int = 0): Long
    fun readFrames(into: Array<DoubleArray>, amount: Long, offset: Int = 0): Long

    fun writeFrames(from: ShortArray, amount: Long, offset: Int = 0): Long
    fun writeFrames(from: IntArray, amount: Long, offset: Int = 0): Long
    fun writeFrames(from: LongArray, amount: Long, offset: Int = 0): Long
    fun writeFrames(from: DoubleArray, amount: Long, offset: Int = 0): Long
    fun writeFrames(from: Array<ShortArray>, amount: Long, offset: Int = 0): Long
    fun writeFrames(from: Array<IntArray>, amount: Long, offset: Int = 0): Long
    fun writeFrames(from: Array<LongArray>, amount: Long, offset: Int = 0): Long
    fun writeFrames(from: Array<DoubleArray>, amount: Long, offset: Int = 0): Long

}
