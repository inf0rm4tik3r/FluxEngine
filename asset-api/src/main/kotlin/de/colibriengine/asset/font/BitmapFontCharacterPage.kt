package de.colibriengine.asset.font

data class BitmapFontCharacterPage(
    val id: UInt,
    val textureName: String
)
