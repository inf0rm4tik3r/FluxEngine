package de.colibriengine.asset.font

import de.colibriengine.asset.ResourceName
import java.lang.StringBuilder

data class BitmapFontDescriptor(
    val resourceName: ResourceName,
    val info: BitmapFontInfo,
    val common: BitmapFontCommon,
    val pages: List<BitmapFontCharacterPage>,
    val characters: Map<UInt, BitmapFontCharacter>,
    val kerning: Map<UInt, Map<UInt, BitmapFontKerning>>
) {

    fun getKerningAmt(current: Char, next: Char): Short {
        val innerKerningMap = kerning[current.code.toUInt()]
        if (innerKerningMap != null) {
            val kerning = innerKerningMap[next.code.toUInt()]
            if (kerning != null) {
                //LOG.info("Found character kerning for: {} <-> {}: {}", cCurrent, cNext, kerning.getAmount());
                return kerning.kerning
            } //else {
            //LOG.error("Unable to find character kerning for: {} <-> {}. No value in inner map!", cCurrent,
            // cNext);
            ///}
        } //else {
        //LOG.error("Unable to find character kerning for: {} <-> {}. No inner map!", cCurrent, cNext);
        //}
        return DEFAULT_KERNING
    }

    override fun toString(): String {
        val stringBuilder = StringBuilder()
        val childOffset = "  "

        // Append info and common.
        stringBuilder
            .append("BMFont {\n")
            .append(childOffset).append("Info: ").append(info.toString())
            .append(childOffset).append("Common: ").append(common.toString())

        // Append pages.
        stringBuilder.append(childOffset).append("Pages: {\n")
        for (page in pages) {
            stringBuilder.append(page.toString())
        }
        stringBuilder.append(childOffset).append("}\n")

        // Append characters.
        stringBuilder.append(childOffset).append("Characters: {\n")
        for ((_, value) in characters) {
            stringBuilder.append(value.toString())
        }
        stringBuilder.append(childOffset).append("}\n")

        // Append kerning.
        stringBuilder.append(childOffset).append("Kerning: {\n")
        for ((_, value) in kerning) {
            for ((_, value1) in value) {
                stringBuilder.append(value1.toString())
            }
        }
        stringBuilder.append(childOffset).append("}\n")
        stringBuilder.append("}")
        return stringBuilder.toString()
    }

    companion object {
        const val DEFAULT_KERNING: Short = 0.toShort()
    }
}
