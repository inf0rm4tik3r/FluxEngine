package de.colibriengine.asset.font

enum class BitmapFontDescriptorFileType {
    TEXT, XML, BINARY
}
