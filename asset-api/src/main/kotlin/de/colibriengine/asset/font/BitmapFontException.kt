package de.colibriengine.asset.font

class BitmapFontException(message: String, cause: Throwable?) : RuntimeException(message, cause)
