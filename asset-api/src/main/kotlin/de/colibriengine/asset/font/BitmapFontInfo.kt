package de.colibriengine.asset.font

/**
 * This tag holds information on how the font was generated.
 * private String fontName = "";
 * private int fontSize = 0;
 * private boolean bold = false;
 * private boolean italic = false;
 * private String charSet = "";
 * private boolean unicode = false;
 * private float stretchH = 0;
 * private boolean smooth = false;
 * private int aa = 0;
 * private int paddingTop = 0;
 * private int paddingRight = 0;
 * private int paddingBottom = 0;
 * private int paddingLeft = 0;
 * private int spacingH = 0;
 * private int spacingV = 0;
 * private int outlineThickness = 0;
 */
data class BitmapFontInfo(
    val fontName: String,
    val fontSize: UShort,
    val isBold: Boolean,
    val isItalic: Boolean,
    val charSet: String,
    val isUnicode: Boolean,
    val stretchH: Float,
    val isSmooth: Boolean,
    val aa: UByte,
    val paddingTop: UByte,
    val paddingRight: UByte,
    val paddingBottom: UByte,
    val paddingLeft: UByte,
    val spacingH: UByte,
    val spacingV: UByte,
    val outlineThickness: UByte
)
