package de.colibriengine.asset.font

data class BitmapFontKerning(
    val firstCharacter: UInt,
    val secondCharacter: UInt,
    val kerning: Short
)
