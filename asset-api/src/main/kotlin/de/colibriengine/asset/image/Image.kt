package de.colibriengine.asset.image

import de.colibriengine.asset.ResourceName
import java.nio.ByteBuffer

data class Image(
    val resourceName: ResourceName,
    val width: UInt,
    val height: UInt,
    val components: UInt,
    val data: ByteBuffer
)
