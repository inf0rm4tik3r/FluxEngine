package de.colibriengine.asset.image

data class ImageLoadOptions(

    /**
     * The texture base in OpenGL is in the bottom-left corner, not in the top-left corner. When loading images which
     * are to be used as texture, this flag should be set to true. When loading, for example, an icon to be displayed
     * in the windows taskbar or window bar, it should be set to false, as Windows defines the image base to be in the
     * top-left corner and images are not typically stored in an inverted fashion.
     */
    val flipVertically: Boolean = true

)
