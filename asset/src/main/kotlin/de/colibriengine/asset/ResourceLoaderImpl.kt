package de.colibriengine.asset

import de.colibriengine.asset.audio.Wav
import de.colibriengine.asset.audio.wav.WavFile
import de.colibriengine.asset.font.BitmapFontDescriptor
import de.colibriengine.asset.font.bitmap.loader.BitmapFontDescriptorLoader
import de.colibriengine.asset.image.Image
import de.colibriengine.asset.image.ImageLoadOptions
import de.colibriengine.asset.image.ImageLoader
import de.colibriengine.util.FileOps

class ResourceLoaderImpl : ResourceLoader {

    @Suppress("UNCHECKED_CAST")
    override fun <T> load(resourceName: ResourceName, type: Class<T>, options: Any?): T {
        val resourceUrl = FileOps.getAsURLOrFail(resourceName.s)
        return when (type) {
            Image::class.java ->
                ImageLoader.load(resourceName, options as ImageLoadOptions?) as T
            Wav::class.java ->
                WavFile.open(resourceUrl) as T
            BitmapFontDescriptor::class.java ->
                BitmapFontDescriptorLoader.load(resourceUrl, resourceName) as T
            else ->
                throw IllegalArgumentException("Unable to load resource $resourceName. Type unsupported: $type")
        }
    }

    override fun <T> stream(resourceName: ResourceName): T {
        TODO("Not yet implemented")
    }

}
