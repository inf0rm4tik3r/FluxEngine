package de.colibriengine.asset.audio.wav

import de.colibriengine.asset.audio.Wav
import de.colibriengine.buffers.BufferTools
import de.colibriengine.logging.LogUtil.getLogger
import java.io.*
import java.net.URL

@Suppress("unused")
class WavFile private constructor() : Wav {

    private enum class IOState {
        READING, WRITING, CLOSED
    }

    /** Specifies the IO State of the Wav File (used for sanity checking). */
    private var ioState: IOState? = null

    /** Output stream used for writing data. */
    private var oStream: OutputStream? = null

    /** Input stream used for reading data. */
    private var iStream: InputStream? = null

    /** Scaling factor used for int <-> float conversion. */
    private var floatScale = 0.0

    /** Offset factor used for int <-> float conversion. */
    private var floatOffset = 0.0

    /** Specify if an extra byte at the end of the data chunk is required for word alignment. */
    private var wordAlignAdjust = false

    /* ------------ Buffering ------------- */
    /** Local buffer used for IO. */
    private val buffer: ByteArray = ByteArray(BUFFER_SIZE)

    /** Points to the current position in local buffer. */
    private var bufferPointer = 0

    /** Bytes read after last read into local buffer. */
    private var bytesRead = 0

    /** Current number of frames read or written. */
    private var frameCounter: Long = 0L

    /* ------------ Wav Header ------------- */
    override lateinit var header: WavFileHeader
        private set

    /* ------------ Data ------------- */
    override var bytesPerSample: Int = 0
        private set

    override var numFrames: Long = 0
        private set

    val framesRemaining: Long
        get() = numFrames - frameCounter

    override fun readFrames(into: ShortArray, amount: Long, offset: Int): Long =
        readFramesInterleaved(amount, offset) { at -> into[at] = readNextSample().toShort() }

    override fun readFrames(into: IntArray, amount: Long, offset: Int): Long =
        readFramesInterleaved(amount, offset) { at -> into[at] = readNextSample().toInt() }

    override fun readFrames(into: LongArray, amount: Long, offset: Int): Long =
        readFramesInterleaved(amount, offset) { at -> into[at] = readNextSample() }

    override fun readFrames(into: DoubleArray, amount: Long, offset: Int): Long =
        readFramesInterleaved(amount, offset) { at ->
            into[at] = floatOffset + readNextSample().toDouble() / floatScale
        }

    override fun readFrames(into: Array<ShortArray>, amount: Long, offset: Int): Long =
        readFramesMultichannel(amount, offset) { channel, at ->
            into[channel][at] = readNextSample().toShort()
        }

    override fun readFrames(into: Array<IntArray>, amount: Long, offset: Int): Long =
        readFramesMultichannel(amount, offset) { channel, at ->
            into[channel][at] = readNextSample().toInt()
        }

    override fun readFrames(into: Array<LongArray>, amount: Long, offset: Int): Long =
        readFramesMultichannel(amount, offset) { channel, at ->
            into[channel][at] = readNextSample()
        }

    override fun readFrames(into: Array<DoubleArray>, amount: Long, offset: Int): Long =
        readFramesMultichannel(amount, offset) { channel, at ->
            into[channel][at] = floatOffset + readNextSample().toDouble() / floatScale
        }

    private fun readNextSample(): Long {
        var value: Long = 0
        for (b in 0 until bytesPerSample) {
            if (bufferPointer == bytesRead) {
                val read = iStream!!.read(buffer, 0, BUFFER_SIZE)
                if (read == -1) throw WavFileException("Not enough data available")
                bytesRead = read
                bufferPointer = 0
            }
            var v = buffer[bufferPointer].toInt()
            if (b < bytesPerSample - 1 || bytesPerSample == 1) v = v and 0xFF
            value += (v shl b * 8).toLong()
            bufferPointer++
        }
        return value
    }

    override fun writeFrames(from: ShortArray, amount: Long, offset: Int): Long =
        writeFramesInterleaved(amount, offset) { at -> writeNextSample(from[at].toLong()) }

    override fun writeFrames(from: IntArray, amount: Long, offset: Int): Long =
        writeFramesInterleaved(amount, offset) { at -> writeNextSample(from[at].toLong()) }

    override fun writeFrames(from: LongArray, amount: Long, offset: Int): Long =
        writeFramesInterleaved(amount, offset) { at -> writeNextSample(from[at]) }

    override fun writeFrames(from: DoubleArray, amount: Long, offset: Int): Long =
        writeFramesInterleaved(amount, offset) { at ->
            writeNextSample((floatScale * (floatOffset + from[at])).toLong())
        }

    override fun writeFrames(from: Array<ShortArray>, amount: Long, offset: Int): Long =
        writeFramesMultichannel(amount, offset) { channel, at ->
            writeNextSample(from[channel][at].toLong())
        }

    override fun writeFrames(from: Array<IntArray>, amount: Long, offset: Int): Long =
        writeFramesMultichannel(amount, offset) { channel, at ->
            writeNextSample(from[channel][at].toLong())
        }

    override fun writeFrames(from: Array<LongArray>, amount: Long, offset: Int): Long =
        writeFramesMultichannel(amount, offset) { channel, at ->
            writeNextSample(from[channel][at])
        }

    override fun writeFrames(from: Array<DoubleArray>, amount: Long, offset: Int): Long =
        writeFramesMultichannel(amount, offset) { channel, at ->
            writeNextSample((floatScale * (floatOffset + from[channel][at])).toLong())
        }

    private fun writeNextSample(value: Long) {
        var ramaining = value
        for (b in 0 until bytesPerSample) {
            if (bufferPointer == BUFFER_SIZE) {
                oStream!!.write(buffer, 0, BUFFER_SIZE)
                bufferPointer = 0
            }
            buffer[bufferPointer] = (ramaining and 0xFF).toByte()
            ramaining = ramaining shr 8
            bufferPointer++
        }
    }

    private inline fun readFramesInterleaved(amount: Long, offset: Int, readAction: (Int) -> Unit): Long {
        ensureReading()
        return processFramesInterleaved(amount, offset, readAction)
    }

    private inline fun writeFramesInterleaved(amount: Long, offset: Int, writeAction: (Int) -> Unit): Long {
        ensureWriting()
        return processFramesInterleaved(amount, offset, writeAction)
    }

    private inline fun readFramesMultichannel(amount: Long, offset: Int, readAction: (Int, Int) -> Unit): Long {
        ensureReading()
        return processFramesMultichannel(amount, offset, readAction)
    }

    private inline fun writeFramesMultichannel(amount: Long, offset: Int, writeAction: (Int, Int) -> Unit): Long {
        ensureWriting()
        return processFramesMultichannel(amount, offset, writeAction)
    }

    private fun ensureReading() {
        if (ioState != IOState.READING) throw WavFileException("Cannot read from this WavFile instance.")
    }

    private fun ensureWriting() {
        if (ioState != IOState.WRITING) throw WavFileException("Cannot write to this WavFile instance.")
    }

    private inline fun processFramesInterleaved(amount: Long, offset: Int, action: (Int) -> Unit): Long {
        var at = offset
        for (frame in 0 until amount) {
            if (frameCounter == numFrames)
                return frame
            for (channel in 0 until header.numChannels) {
                action(at)
                at++
            }
            frameCounter++
        }
        return amount
    }

    private inline fun processFramesMultichannel(amount: Long, offset: Int, action: (Int, Int) -> Unit): Long {
        var at = offset
        for (frame in 0 until amount) {
            if (frameCounter == numFrames)
                return frame
            for (channel in 0 until header.numChannels)
                action(channel, at)
            at++
            frameCounter++
        }
        return amount
    }

    override fun close() {
        // Close the input stream and set to null
        if (iStream != null) {
            iStream!!.close()
            iStream = null
        }
        if (oStream != null) {
            // Write out anything still in the local buffer
            if (bufferPointer > 0) oStream!!.write(buffer, 0, bufferPointer)

            // If an extra byte is required for word alignment, add it to the end
            if (wordAlignAdjust) oStream!!.write(0)

            // Close the stream and set to null
            oStream!!.close()
            oStream = null
        }

        // Flag that the stream is closed
        ioState = IOState.CLOSED
        LOG.info("Closed")
    }

    override fun logDebugInfo() {
        LOG.debug("WAV:")
        LOG.debug("-- Channels: ${header.numChannels}, Frames: $numFrames")
        LOG.debug("-- IO State: $ioState")
        LOG.debug("-- Sample Rate: ${header.sampleRate}, Block Align: ${header.blockAlign}")
        LOG.debug("-- Valid Bits: ${header.validBits}, Bytes per sample: $bytesPerSample")
    }

    companion object {
        private val LOG = getLogger(WavFile::class.java)
        private const val BUFFER_SIZE = 4096
        private const val FORMAT_CHUNK_HEADER_SIZE: Int = 16
        private const val FORMAT_CHUNK_ID: Long = 0x20746D66
        private const val DATA_CHUNK_ID: Long = 0x61746164
        private const val RIFF_CHUNK_ID: Long = 0x46464952
        private const val RIFF_TYPE_ID: Long = 0x45564157

        fun new(file: File, numChannels: Int, numFrames: Long, validBits: Int, sampleRate: Long): WavFile {
            val wavFile = WavFile()
            wavFile.numFrames = numFrames
            wavFile.bytesPerSample = (validBits + 7) / 8
            wavFile.header = WavFileHeader(
                numChannels = numChannels,
                sampleRate = sampleRate,
                blockAlign = wavFile.bytesPerSample * numChannels,
                validBits = validBits
            )

            // Sanity check arguments
            if (numChannels < 1 || numChannels > 65535) throw WavFileException("Illegal number of channels, valid range 1 to 65536")
            if (numFrames < 0) throw WavFileException("Number of frames must be positive")
            if (validBits < 2 || validBits > 65535) throw WavFileException("Illegal number of valid bits, valid range 2 to 65536")
            if (sampleRate < 0) throw WavFileException("Sample rate must be positive")

            // Create output stream for writing data
            wavFile.oStream = FileOutputStream(file)

            // Calculate the chunk sizes
            val dataChunkSize = wavFile.header.blockAlign * numFrames
            var mainChunkSize = 4 +  // Riff Type
                8 +  // Format ID and size
                16 +  // Format data
                8 +  // Data ID and size
                dataChunkSize

            // Chunks must be word aligned, so if odd number of audio data bytes
            // adjust the main chunk size
            if (dataChunkSize % 2 == 1L) {
                mainChunkSize += 1
                wavFile.wordAlignAdjust = true
            } else {
                wavFile.wordAlignAdjust = false
            }

            // Set the main chunk size
            BufferTools.putLE(RIFF_CHUNK_ID, wavFile.buffer, 0, 4)
            BufferTools.putLE(mainChunkSize, wavFile.buffer, 4, 4)
            BufferTools.putLE(RIFF_TYPE_ID, wavFile.buffer, 8, 4)

            // Write out the header
            wavFile.oStream!!.write(wavFile.buffer, 0, 12)

            // Put format data in buffer
            val averageBytesPerSecond = sampleRate * wavFile.header.blockAlign
            BufferTools.putLE(FORMAT_CHUNK_ID, wavFile.buffer, 0, 4) // Chunk ID
            BufferTools.putLE(16, wavFile.buffer, 4, 4) // Chunk Data Size
            BufferTools.putLE(1, wavFile.buffer, 8, 2) // Compression Code (Uncompressed)
            BufferTools.putLE(numChannels.toLong(), wavFile.buffer, 10, 2) // Number of channels
            BufferTools.putLE(sampleRate, wavFile.buffer, 12, 4) // Sample Rate
            BufferTools.putLE(averageBytesPerSecond, wavFile.buffer, 16, 4) // Average Bytes Per Second
            BufferTools.putLE(wavFile.header.blockAlign.toLong(), wavFile.buffer, 20, 2) // Block Align
            BufferTools.putLE(validBits.toLong(), wavFile.buffer, 22, 2) // Valid Bits

            // Write Format Chunk
            wavFile.oStream!!.write(wavFile.buffer, 0, 24)

            // Start Data Chunk
            BufferTools.putLE(DATA_CHUNK_ID, wavFile.buffer, 0, 4) // Chunk ID
            BufferTools.putLE(dataChunkSize, wavFile.buffer, 4, 4) // Chunk Data Size

            // Write Format Chunk
            wavFile.oStream!!.write(wavFile.buffer, 0, 8)

            // Calculate the scaling factor for converting to a normalised double
            if (wavFile.header.validBits > 8) {
                // If more than 8 validBits, data is signed
                // Conversion required multiplying by magnitude of max positive value
                wavFile.floatOffset = 0.0
                wavFile.floatScale = (Long.MAX_VALUE shr 64 - wavFile.header.validBits).toDouble()
            } else {
                // Else if 8 or less validBits, data is unsigned
                // Conversion required dividing by max positive value
                wavFile.floatOffset = 1.0
                wavFile.floatScale = 0.5 * ((1 shl wavFile.header.validBits) - 1)
            }

            // Finally, set the IO State
            wavFile.bufferPointer = 0
            wavFile.bytesRead = 0
            wavFile.frameCounter = 0
            wavFile.ioState = IOState.WRITING
            return wavFile
        }

        fun open(file: File): WavFile {
            LOG.info("Opening ${file.path}")
            return open(FileInputStream(file))
        }

        fun open(resourceUrl: URL): WavFile {
            LOG.info("Opening ${resourceUrl.path}")
            return open(resourceUrl.openStream())
        }

        private fun open(inputStream: InputStream): WavFile {
            val wavFile = WavFile()
            wavFile.iStream = inputStream

            val chunkSize = readFileHeader(wavFile)
            /*
            if (file.length() != chunkSize + 8)
                throw WavFileException("Header chunk size ($chunkSize) does not match file size (${file.length()})")
            */
            searchFormatAndDataChunks(wavFile)

            // Calculate the scaling factor for converting to a normalised double
            if (wavFile.header.validBits > 8) {
                // If more than 8 validBits, data is signed and conversion is required.
                // Dividing by magnitude of max negative value
                wavFile.floatOffset = 0.0
                wavFile.floatScale = (1 shl wavFile.header.validBits - 1).toDouble()
            } else {
                // Else if 8 or less validBits, data is unsigned and conversion is required.
                // Dividing by max positive value
                wavFile.floatOffset = -1.0
                wavFile.floatScale = 0.5 * ((1 shl wavFile.header.validBits) - 1)
            }
            wavFile.bufferPointer = 0
            wavFile.bytesRead = 0
            wavFile.frameCounter = 0
            wavFile.ioState = IOState.READING
            return wavFile
        }

        private fun readFileHeader(wavFile: WavFile): Long {
            val bytesRead = wavFile.iStream!!.read(wavFile.buffer, 0, 12)
            if (bytesRead != 12) throw WavFileException("Not enough wav file bytes for header")
            val riffChunkID: Long = BufferTools.getLE(wavFile.buffer, 0, 4)
            if (riffChunkID != RIFF_CHUNK_ID) throw WavFileException("Header invalid, incorrect riff chunk ID")
            val chunkSize: Long = BufferTools.getLE(wavFile.buffer, 4, 4)
            val riffTypeID: Long = BufferTools.getLE(wavFile.buffer, 8, 4)
            if (riffTypeID != RIFF_TYPE_ID) throw WavFileException("Header invalid, incorrect riff type ID")
            return chunkSize
        }

        private fun searchFormatAndDataChunks(wavFile: WavFile) {
            var foundFormat = false
            while (true) {
                val (chunkID, chunkSize) = readNextChunk(wavFile)
                // Word align the chunk size: chunkSize specifies the number of bytes holding data. However, the data
                // should be word aligned (by 2 bytes) so we need to calculate the actual number of bytes in the chunk.
                val numChunkBytes = if (chunkSize % 2 == 1L) chunkSize + 1 else chunkSize

                if (chunkID == FORMAT_CHUNK_ID) {
                    foundFormat = true

                    val headerBytesRead = wavFile.iStream!!.read(wavFile.buffer, 0, FORMAT_CHUNK_HEADER_SIZE)
                    if (headerBytesRead == -1) throw WavFileException("Could not read header bytes!")

                    val compressionCode = BufferTools.getLE(wavFile.buffer, 0, 2).toInt()
                    if (compressionCode != 1) throw WavFileException("Compression Code $compressionCode not supported")

                    wavFile.header = WavFileHeader(
                        numChannels = BufferTools.getLE(wavFile.buffer, 2, 2).toInt(),
                        sampleRate = BufferTools.getLE(wavFile.buffer, 4, 4),
                        blockAlign = BufferTools.getLE(wavFile.buffer, 12, 2).toInt(),
                        validBits = BufferTools.getLE(wavFile.buffer, 14, 2).toInt()
                    )
                    wavFile.bytesPerSample = wavFile.header.calculateBytesRequiredToStoreOneSample()

                    if (numChunkBytes > FORMAT_CHUNK_HEADER_SIZE)
                        wavFile.iStream!!.skip(numChunkBytes - FORMAT_CHUNK_HEADER_SIZE)
                } else if (chunkID == DATA_CHUNK_ID) {
                    if (!foundFormat)
                        throw WavFileException("We need format information before we can read the data chunk!")
                    if (chunkSize % wavFile.header.blockAlign != 0L)
                        throw WavFileException("Data Chunk size is not a multiple of the Block Align (bytes per frame)")
                    wavFile.numFrames = chunkSize / wavFile.header.blockAlign
                    break
                } else {
                    // Unknown chunk: Just skip over the chunk data
                    wavFile.iStream!!.skip(numChunkBytes)
                }
            }
        }

        private fun readNextChunk(wavFile: WavFile): Pair<Long, Long> {
            // The first 8 bytes contain the chunks ID and size.
            val bytesRead = wavFile.iStream!!.read(wavFile.buffer, 0, 8)
            if (bytesRead == -1) throw WavFileException("Reached end of file without finding chunk format.")
            if (bytesRead != 8) throw WavFileException("Could not read chunk header.")
            val chunkID: Long = BufferTools.getLE(wavFile.buffer, 0, 4)
            val chunkSize: Long = BufferTools.getLE(wavFile.buffer, 4, 4)
            return Pair(chunkID, chunkSize)
        }
    }

}
