package de.colibriengine.asset.font.bitmap.loader

import de.colibriengine.asset.ResourceName
import de.colibriengine.asset.font.BitmapFontDescriptor
import de.colibriengine.asset.font.BitmapFontDescriptorFileType
import de.colibriengine.asset.font.BitmapFontException
import de.colibriengine.exception.ResourceInaccessibleException
import de.colibriengine.exception.UnsupportedResourceException
import de.colibriengine.logging.LogUtil.getLogger
import java.io.BufferedInputStream
import java.io.IOException
import java.net.URL

private const val INCORRECT_DESCRIPTOR_FILE = "\"%s\" does not seem to be a correct BitmapFont descriptor file."
private const val READ_ERROR_IN_DESCRIPTOR_FILE = "Unable to read BitmapFont descriptor file \"%s\"."
private const val XML_NOT_SUPPORTED = "XML encoding is not jet supported."
private const val TEXT_NOT_SUPPORTED = "TEXT encoding is not jet supported."

private val TEST_BINARY = byteArrayOf('B'.code.toByte(), 'M'.code.toByte(), 'F'.code.toByte())
private val TEST_XML = byteArrayOf('<'.code.toByte(), '?'.code.toByte(), 'x'.code.toByte())
private val TEST_TEXT = byteArrayOf('i'.code.toByte(), 'n'.code.toByte(), 'f'.code.toByte())
private const val LONGEST_TEST = 3 // They are all 3 bytes long..

object BitmapFontDescriptorLoader {

    private val LOG = getLogger<BitmapFontDescriptorLoader>()

    fun load(resourceUrl: URL, resourceName: ResourceName): BitmapFontDescriptor {
        val path = resourceUrl.path
        LOG.info(path)
        try {
            BufferedInputStream(resourceUrl.openStream()).use {
                val fontDescriptorFileType = discoverFontDescriptorFileType(path, it)
                LOG.info("Type: $fontDescriptorFileType")
                return when (fontDescriptorFileType) {
                    BitmapFontDescriptorFileType.BINARY -> BinaryBitmapFontDescriptorLoader.parse(it, resourceName)
                    BitmapFontDescriptorFileType.XML -> throw UnsupportedResourceException(XML_NOT_SUPPORTED)
                    BitmapFontDescriptorFileType.TEXT -> throw UnsupportedResourceException(TEXT_NOT_SUPPORTED)
                    else -> throw UnsupportedResourceException("Unknown encoding/filetype!")
                }
            }
        } catch (exception: BitmapFontException) {
            throw BitmapFontException("Could not load BitmapFont: $path", exception)
        } catch (exception: IOException) {
            throw ResourceInaccessibleException.forPath(path, exception)
        }
    }

    private fun discoverFontDescriptorFileType(path: String, bis: BufferedInputStream): BitmapFontDescriptorFileType {
        // The first LONGEST_TEST bytes are enough to determine the type.
        val bytesToRead = LONGEST_TEST
        val bytes = ByteArray(bytesToRead)
        val result: Int
        try {
            // Store where we currently are in the stream.
            bis.mark(32)
            result = bis.read(bytes, 0, bytesToRead)
            if (result != bytesToRead)
                throw UnsupportedResourceException(String.format(INCORRECT_DESCRIPTOR_FILE, path))
            // Go back to our mark, so the bytes read can be re-read with the given stream.
            bis.reset()
        } catch (exception: IOException) {
            throw UnsupportedResourceException(String.format(READ_ERROR_IN_DESCRIPTOR_FILE, path), exception)
        }

        return when {
            bytes.contentEquals(TEST_BINARY) -> BitmapFontDescriptorFileType.BINARY
            bytes.contentEquals(TEST_XML) -> BitmapFontDescriptorFileType.XML
            bytes.contentEquals(TEST_TEXT) -> BitmapFontDescriptorFileType.TEXT
            else -> throw UnsupportedResourceException(
                String.format(INCORRECT_DESCRIPTOR_FILE, path) + "First 3 bytes read: ${bytes.decodeToString()}"
            )
        }
    }

}
