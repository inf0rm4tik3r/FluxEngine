package de.colibriengine.asset.image

import de.colibriengine.asset.ResourceName
import de.colibriengine.exception.TextureLoadException
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.util.FileOps.getAsURLOrFail
import org.apache.commons.io.IOUtils
import org.lwjgl.stb.STBImage
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import java.io.IOException

object ImageLoader {

    private val LOG = getLogger(this)

    fun load(resourceName: ResourceName, options: ImageLoadOptions? = null): Image {
        MemoryStack.stackPush().use { stack ->
            // Inform STBImage that the origin of our loaded image should be on the bottom left.
            STBImage.stbi_set_flip_vertically_on_load(options?.flipVertically ?: true)

            // Load the image into memory.
            // This approach guarantees us that we can load textures from any URL (from .jar for example!).
            // TODO: load directly into gpu memory!
            val imageDataByteArray: ByteArray = try {
                IOUtils.toByteArray(getAsURLOrFail(resourceName.s).openStream())
            } catch (e: IOException) {
                throw TextureLoadException("Unable to read file: $resourceName")
            }
            // Create a ByteBuffer in native memory and fill it with the loaded data.
            val imageData = MemoryUtil.memAlloc(imageDataByteArray.size)
            imageData.put(imageDataByteArray)
            imageData.flip()
            val width = stack.mallocInt(1)
            val height = stack.mallocInt(1)
            val components = stack.mallocInt(1)

            // Let STBImage load the image from the byte data.
            val image = STBImage.stbi_load_from_memory(imageData, width, height, components, 0)
                ?: throw TextureLoadException(
                    "Failed to load the texture file: ${resourceName}. Reason: ${STBImage.stbi_failure_reason()}"
                )

            // Check if an error occurred.
            LOG.info("Loaded $resourceName")
            return Image(resourceName, width.get().toUInt(), height.get().toUInt(), components.get().toUInt(), image)
        }
    }

}
