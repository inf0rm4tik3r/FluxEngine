package de.colibriengine.audio

interface AudioMaster {

    /** Initializes the underlying sound system. */
    fun initAudioSystem()

    /** Destroys/terminates the underlying sound system. */
    fun destroyAudioSystem()

}
