package de.colibriengine.audio

interface AudioSource {

    var audioTrack: AudioTrack

    var volume: Float
    var pitch: Float
    var looping: Boolean

    fun load(audioTrack: AudioTrack)
    fun unload()
    fun play()
    fun pause()
    fun stop()
    fun rewind()

    fun state(): AudioSourceState
    fun isInInitialState(): Boolean = state() == AudioSourceState.INITIAL
    fun isPlaying(): Boolean = state() == AudioSourceState.PLAYING
    fun isPaused(): Boolean = state() == AudioSourceState.PAUSED
    fun isStopped(): Boolean = state() == AudioSourceState.STOPPED

    fun destroy()

}
