package de.colibriengine.audio

import de.colibriengine.math.vector.vec3f.ChangeDetectableVec3f
import de.colibriengine.math.vector.vec3f.Vec3f

interface AudioSource3D : AudioSource2D {

    val position: ChangeDetectableVec3f
    val velocity: Vec3f
    val direction: Vec3f
    var relativeToListener: Boolean
    var rolloffFactor: Float

}
