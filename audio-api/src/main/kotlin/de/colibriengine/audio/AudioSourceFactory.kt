package de.colibriengine.audio

interface AudioSourceFactory {

    fun createSource2D(): AudioSource2D
    fun createSource3D(): AudioSource3D

}

inline fun AudioSourceFactory.createSource2D(initBlock: AudioSource2D.() -> Unit): AudioSource2D {
    val source = createSource2D()
    source.initBlock()
    return source
}

inline fun AudioSourceFactory.createSource3D(initBlock: AudioSource3D.() -> Unit): AudioSource3D {
    val source = createSource3D()
    source.initBlock()
    return source
}
