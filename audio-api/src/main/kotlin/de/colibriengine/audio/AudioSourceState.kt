package de.colibriengine.audio

enum class AudioSourceState {

    INITIAL, PLAYING, PAUSED, STOPPED

}
