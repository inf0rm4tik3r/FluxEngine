package de.colibriengine.audio

import de.colibriengine.asset.ResourceName

interface AudioTrackFactory {

    fun get(resourceName: ResourceName): AudioTrack

    /**
     * Return a track if you no longer need it. This might release its data. Using the track afterwards is
     * therefore not valid. If needed, a track can always be retrieved again by using [get].
     */
    fun giveBack(track: AudioTrack)

}
