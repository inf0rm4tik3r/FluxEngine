package de.colibriengine.audio.openal

import de.colibriengine.asset.ResourceName
import de.colibriengine.audio.AudioTrack
import de.colibriengine.logging.LogUtil.getLogger

data class OpenALAudioTrack(
    val buffer: OpenALBuffer,
    override val resourceName: ResourceName,
    override val length: Float,
    override val channels: Int,
    override val bitPerSample: Int,
    override val samples: Long,
    override val samplingRate: Int
) : AudioTrack {

    fun destroy() {
        LOG.info("Destroying track $resourceName")
        buffer.destroy()
    }

    companion object {
        private val LOG = getLogger(this)

        /** If the [NULL] track is played in a source, nothing will happen. */
        val NULL = OpenALAudioTrack(
            OpenALBuffer.NULL,
            ResourceName("-"),
            0f, 0, 0, 0L, 0
        )
    }

}
