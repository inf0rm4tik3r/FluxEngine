package de.colibriengine.audio.openal

import de.colibriengine.logging.LogUtil.getLogger
import org.lwjgl.openal.AL11

class OpenALBuffer {

    @Suppress("JoinDeclarationAndAssignment")
    val id: Int

    constructor() {
        id = AL11.alGenBuffers()
        checkForOpenALErrors()
    }

    private constructor(idOverride: Int) {
        id = idOverride
    }

    fun store(data: ShortArray, format: Int, sampleRate: UInt) {
        require(id != 0)
        AL11.alBufferData(id, format, data, sampleRate.toInt())
        checkForOpenALErrors()
    }

    fun destroy() {
        if (this == NULL) {
            return
        }
        AL11.alDeleteBuffers(id)
        checkForOpenALErrors()
        LOG.info("Deleted OpenAL buffer $id")
    }

    override fun equals(other: Any?): Boolean = when {
        this === other -> true
        javaClass != other?.javaClass -> false
        else -> {
            other as OpenALBuffer
            when {
                id != other.id -> false
                else -> true
            }
        }
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "OpenALBuffer(id=$id)"
    }

    companion object {
        val NULL = OpenALBuffer(0)
        private val LOG = getLogger(this)
    }

}
