package de.colibriengine.audio.openal

class OpenALCException(message: String) : RuntimeException(message)
