package de.colibriengine.audio.openal

import de.colibriengine.audio.AudioTrackFactory
import de.colibriengine.logging.LogUtil.getLogger
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.lwjgl.openal.AL
import org.lwjgl.openal.ALC11
import org.lwjgl.openal.ALCCapabilities
import org.lwjgl.openal.ALCapabilities

@Suppress("JoinDeclarationAndAssignment")
class OpenALContext(deviceId: Long, alcCaps: ALCCapabilities) : KoinComponent {

    val id: Long
    val caps: ALCapabilities
    val listener: OpenALListener

    private val trackFactory: OpenALTrackFactory = get<AudioTrackFactory>() as OpenALTrackFactory

    init {
        id = ALC11.alcCreateContext(deviceId, null as IntArray?)
        require(id != 0L) { "Could not open an OpenAL context!" }
        checkForOpenALCErrors(id)
        ALC11.alcMakeContextCurrent(id)
        checkForOpenALCErrors(id)
        caps = AL.createCapabilities(alcCaps)
        LOG.info("OpenAL context created: $id")

        listener = OpenALListener
    }

    fun destroy() {
        trackFactory.destroy()
    }

    companion object {
        private val LOG = getLogger(this)
    }

}
