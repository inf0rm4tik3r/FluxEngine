package de.colibriengine.audio.openal

import de.colibriengine.logging.LogUtil.getLogger
import org.lwjgl.openal.ALC
import org.lwjgl.openal.ALC11
import org.lwjgl.openal.ALCCapabilities

@Suppress("JoinDeclarationAndAssignment")
data class OpenALDevice(val deviceName: String?) {

    val id: Long
    val caps: ALCCapabilities
    val context: OpenALContext

    init {
        id = ALC11.alcOpenDevice(deviceName)
        require(id != 0L) { "Could not connect to a sound device with OpenAL. No sound solution was found!" }
        checkForOpenALCErrors(id)
        caps = ALC.createCapabilities(id)
        checkForOpenALCErrors(id)
        LOG.info("OpenAL connected to device: $id")

        context = OpenALContext(id, caps)
    }

    fun destroy() {
        context.destroy()
    }

    companion object {
        private val LOG = getLogger(this)
    }

}
