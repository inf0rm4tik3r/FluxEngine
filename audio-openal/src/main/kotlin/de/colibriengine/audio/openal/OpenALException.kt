package de.colibriengine.audio.openal

class OpenALException(message: String) : RuntimeException(message)
