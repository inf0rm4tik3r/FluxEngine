package de.colibriengine.audio.openal

import de.colibriengine.audio.AudioListener
import de.colibriengine.math.quaternion.quaternionF.ChangeDetectableQuaternionF
import de.colibriengine.math.quaternion.quaternionF.StdQuaternionF
import de.colibriengine.math.vector.vec3f.ChangeDetectableVec3f
import de.colibriengine.math.vector.vec3f.StdVec3f
import org.lwjgl.openal.AL11

object OpenALListener : AudioListener {

    override var volume: Float = 1f // overwritten in init to call setter!
        set(volume) {
            require(volume in 0f..1f) { "Volume must be in 0..1 range!"}
            field = volume
            AL11.alListenerf(AL11.AL_GAIN, volume)
            checkForOpenALErrors()
        }

    override val position: ChangeDetectableVec3f = ChangeDetectableVec3f(StdVec3f())

    override val velocity: ChangeDetectableVec3f = ChangeDetectableVec3f(StdVec3f())

    override val orientation: ChangeDetectableQuaternionF = ChangeDetectableQuaternionF(StdQuaternionF())

    private val tmpOrientation = FloatArray(6)

    init {
        volume = 1f

        position.onChange.subscribe(next = {
            AL11.alListener3f(AL11.AL_POSITION, position.x, position.y, position.z)
            checkForOpenALErrors()
        })
        position.set(0f, 0f, 0f)

        velocity.onChange.subscribe(next = {
            AL11.alListener3f(AL11.AL_VELOCITY, velocity.x, velocity.y, velocity.z)
            checkForOpenALErrors()
        })
        velocity.set(0f, 0f, 0f)

        orientation.onChange.subscribe(next = {
            orientation.forward(tmpOrientation, 0)
            orientation.up(tmpOrientation, 3)
            AL11.alListenerfv(AL11.AL_ORIENTATION, tmpOrientation)
            checkForOpenALErrors()
        })
        orientation.initIdentity()
    }

}
