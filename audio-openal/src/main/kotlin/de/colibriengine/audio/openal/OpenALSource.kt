package de.colibriengine.audio.openal

import de.colibriengine.audio.AudioSource3D
import de.colibriengine.audio.AudioSourceState
import de.colibriengine.audio.AudioTrack
import de.colibriengine.logging.LogUtil
import de.colibriengine.math.vector.vec3f.ChangeDetectableVec3f
import de.colibriengine.math.vector.vec3f.StdVec3f
import org.lwjgl.openal.AL11

class OpenALSource : AudioSource3D {

    @Suppress("JoinDeclarationAndAssignment")
    private val id: Int

    override var volume: Float = 1f // overwritten in init to call setter!
        set(value) {
            field = value
            AL11.alSourcef(id, AL11.AL_GAIN, value)
            checkForOpenALErrors()
        }

    override var pitch: Float = 1f // overwritten in init to call setter!
        set(value) {
            field = value
            AL11.alSourcef(id, AL11.AL_PITCH, value)
            checkForOpenALErrors()
        }

    override var rolloffFactor: Float = 1f // overwritten in init to call setter!
        set(value) {
            field = value
            AL11.alSourcef(id, AL11.AL_ROLLOFF_FACTOR, value)
            checkForOpenALErrors()
        }

    override var looping: Boolean = false // overwritten in init to call setter!
        set(value) {
            field = value
            AL11.alSourcei(id, AL11.AL_LOOPING, if (value) AL11.AL_TRUE else AL11.AL_FALSE)
            checkForOpenALErrors()
        }

    override var relativeToListener: Boolean = false // overwritten in init to call setter!
        set(value) {
            field = value
            AL11.alSourcei(id, AL11.AL_SOURCE_RELATIVE, if (value) AL11.AL_TRUE else AL11.AL_FALSE)
            checkForOpenALErrors()
        }

    override val position: ChangeDetectableVec3f =
        ChangeDetectableVec3f(StdVec3f(0f, 0f, 0f)) // reset in init to call setter!

    override val velocity: ChangeDetectableVec3f =
        ChangeDetectableVec3f(StdVec3f(0f, 0f, 0f)) // reset in init to call setter!

    override val direction: ChangeDetectableVec3f =
        ChangeDetectableVec3f(StdVec3f(0f, 0f, 0f)) // reset in init to call setter!

    override var audioTrack: AudioTrack = OpenALAudioTrack.NULL // overwritten in init to call setter!
        set(value) {
            field = value
            AL11.alSourcei(id, AL11.AL_BUFFER, (value as OpenALAudioTrack).buffer.id)
        }

    init {
        id = AL11.alGenSources()
        checkForOpenALErrors()

        volume = 1f
        pitch = 1f
        rolloffFactor = 1f
        looping = false
        relativeToListener = false
        position.onChange.subscribe(next = {
            AL11.alSource3f(id, AL11.AL_POSITION, position.x, position.y, position.z)
            checkForOpenALErrors()
        })
        position.set(0f, 0f, 0f)
        velocity.onChange.subscribe(next = {
            AL11.alSource3f(id, AL11.AL_VELOCITY, velocity.x, velocity.y, velocity.z)
            checkForOpenALErrors()
        })
        velocity.set(0f, 0f, 0f)
        direction.onChange.subscribe(next = {
            AL11.alSource3f(id, AL11.AL_POSITION, direction.x, direction.y, direction.z)
            checkForOpenALErrors()
        })
        direction.set(0f, 0f, 0f)
        audioTrack = OpenALAudioTrack.NULL
    }

    override fun load(audioTrack: AudioTrack) {
        this.audioTrack = audioTrack
    }

    override fun unload() = load(OpenALAudioTrack.NULL)

    override fun play() {
        if (audioTrack == OpenALAudioTrack.NULL) return
        AL11.alSourcePlay(id)
        checkForOpenALErrors()
        LOG.trace("Playing $this")
    }

    override fun pause() {
        if (audioTrack == OpenALAudioTrack.NULL) return
        AL11.alSourcePause(id)
        checkForOpenALErrors()
        LOG.trace("Pausing $this")
    }

    override fun stop() {
        if (audioTrack == OpenALAudioTrack.NULL) return
        AL11.alSourceStop(id)
        checkForOpenALErrors()
        LOG.trace("Playing $this")
    }

    override fun rewind() {
        if (audioTrack == OpenALAudioTrack.NULL) return
        AL11.alSourceRewind(id)
        checkForOpenALErrors()
        LOG.trace("Rewinding $this")
    }

    override fun state(): AudioSourceState = when (AL11.alGetSourcei(id, AL11.AL_SOURCE_STATE)) {
        AL11.AL_INITIAL -> AudioSourceState.INITIAL
        AL11.AL_PLAYING -> AudioSourceState.PLAYING
        AL11.AL_PAUSED -> AudioSourceState.PAUSED
        AL11.AL_STOPPED -> AudioSourceState.STOPPED
        else -> throw OpenALException("Unexpected state.")
    }

    override fun destroy() {
        stop()
        unload()
        AL11.alDeleteSources(id)
        checkForOpenALErrors()
        LOG.trace("Destroying $this")
    }

    companion object {
        val LOG = LogUtil.getLogger(this)
    }

}
