package de.colibriengine.audio.openal

import de.colibriengine.logging.LogUtil
import org.lwjgl.openal.AL11
import org.lwjgl.openal.ALC11

private val LOG = LogUtil.getLogger("OpenALUtil")

const val ALC_INVALID_VALUE_ERROR = "ALC_INVALID_VALUE: an invalid value was passed to an OpenAL function"
const val ALC_INVALID_DEVICE_ERROR = "ALC_INVALID_DEVICE: a bad device was passed to an OpenAL function"
const val ALC_INVALID_CONTEXT_ERROR = "ALC_INVALID_CONTEXT: a bad context was passed to an OpenAL function"
const val ALC_INVALID_ENUM_ERROR = "ALC_INVALID_ENUM: an unknown enum value was passed to an OpenAL function"
const val ALC_OUT_OF_MEMORY_ERROR =
    "ALC_OUT_OF_MEMORY: the requested operation resulted in OpenAL running out of memory"

const val AL_INVALID_NAME_ERROR = "AL_INVALID_NAME: a bad name (ID) was passed to an OpenAL function"
const val AL_INVALID_ENUM_ERROR = "AL_INVALID_ENUM: an invalid enum value was passed to an OpenAL function"
const val AL_INVALID_VALUE_ERROR = "AL_INVALID_VALUE: an invalid value was passed to an OpenAL function"
const val AL_INVALID_OPERATION_ERROR = "AL_INVALID_OPERATION: the requested operation is not valid"
const val AL_OUT_OF_MEMORY_ERROR = "AL_OUT_OF_MEMORY: the requested operation resulted in OpenAL running out of memory"

enum class OpenALErrorReportMode {
    LOG, EXCEPTION
}

/**
 * Checks for any OpenAL context error that occurred after the last call to this method. If [errorReportMode] is set to
 * [OpenALErrorReportMode.EXCEPTION], an [OpenALCException] exception will be raised. If [errorReportMode] is set to
 * [OpenALErrorReportMode.LOG], an error will be logged.
 *
 *      ALC_INVALID_VALUE - an invalid value was passed to an OpenAL function
 *      ALC_INVALID_DEVICE - a bad device was passed to an OpenAL function
 *      ALC_INVALID_CONTEXT - a bad context was passed to an OpenAL function
 *      ALC_INVALID_ENUM - an unknown enum value was passed to an OpenAL function
 *      ALC_OUT_OF_MEMORY - the requested operation resulted in OpenAL running out of memory
 */
fun checkForOpenALCErrors(
    device: Long,
    errorReportMode: OpenALErrorReportMode = OpenALErrorReportMode.EXCEPTION,
    additionalInfo: String? = null
) {
    fun msg(error: String) = if (additionalInfo == null) error else "$error - $additionalInfo"
    fun processError(error: String) = when (errorReportMode) {
        OpenALErrorReportMode.LOG -> LOG.error(msg("OpenAL context error: $error"))
        OpenALErrorReportMode.EXCEPTION -> throw OpenALException(msg("OpenAL context error: $error"))
    }
    when (val error = ALC11.alcGetError(device)) {
        ALC11.ALC_NO_ERROR -> return
        ALC11.ALC_INVALID_VALUE -> processError(ALC_INVALID_VALUE_ERROR)
        ALC11.ALC_INVALID_DEVICE -> processError(ALC_INVALID_DEVICE_ERROR)
        ALC11.ALC_INVALID_CONTEXT -> processError(ALC_INVALID_CONTEXT_ERROR)
        ALC11.ALC_INVALID_ENUM -> processError(ALC_INVALID_ENUM_ERROR)
        ALC11.ALC_OUT_OF_MEMORY -> processError(ALC_OUT_OF_MEMORY_ERROR)
        else -> processError("Unknown context error occurred! $error")
    }
}

/**
 * Checks for any OpenAL error that occurred after the last call to this method. If [errorReportMode] is set to
 * [OpenALErrorReportMode.EXCEPTION], an [OpenALException] exception will be raised. If [errorReportMode] is set to
 * [OpenALErrorReportMode.LOG], an error will be logged.
 *
 *      AL_INVALID_NAME - a bad name (ID) was passed to an OpenAL function
 *      AL_INVALID_ENUM - an invalid enum value was passed to an OpenAL function
 *      AL_INVALID_VALUE - an invalid value was passed to an OpenAL function
 *      AL_INVALID_OPERATION - the requested operation is not valid
 *      AL_OUT_OF_MEMORY - the requested operation resulted in OpenAL running out of memory
 */
fun checkForOpenALErrors(
    errorReportMode: OpenALErrorReportMode = OpenALErrorReportMode.EXCEPTION,
    additionalInfo: String? = null
) {
    fun msg(error: String) = if (additionalInfo == null) error else "$error - $additionalInfo"
    fun processError(error: String) = when (errorReportMode) {
        OpenALErrorReportMode.LOG -> LOG.error(msg("OpenAL error: $error"))
        OpenALErrorReportMode.EXCEPTION -> throw OpenALException(msg("OpenAL error: $error"))
    }
    when (val error = AL11.alGetError()) {
        AL11.AL_NO_ERROR -> return
        AL11.AL_INVALID_NAME -> processError(AL_INVALID_NAME_ERROR)
        AL11.AL_INVALID_ENUM -> processError(AL_INVALID_ENUM_ERROR)
        AL11.AL_INVALID_VALUE -> processError(AL_INVALID_VALUE_ERROR)
        AL11.AL_INVALID_OPERATION -> processError(AL_INVALID_OPERATION_ERROR)
        AL11.AL_OUT_OF_MEMORY -> processError(AL_OUT_OF_MEMORY_ERROR)
        else -> processError("Unknown error occurred! $error")
    }
}

fun openALVersion(): String? {
    return AL11.alGetString(AL11.AL_VERSION)
}

fun openALVendor(): String? {
    return AL11.alGetString(AL11.AL_VENDOR)
}

fun openALRenderer(): String? {
    return AL11.alGetString(AL11.AL_RENDERER)
}

fun openALExtensions(): String? {
    return AL11.alGetString(AL11.AL_EXTENSIONS)
}
