package de.colibriengine.buffers

import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

/**
 * BufferStorable - Classes implementing this interface have the ability to store themselves in buffers.
 *
 * @since ColibriEngine 0.0.7.0
 */
interface BufferStorable {

    /**
     * Stores the content of this object inside the specified [buffer].
     *
     * @param buffer The [ByteBuffer] in which the objects data should be stored.
     * @return The given [buffer] instance.
     * @throws UnsupportedOperationException If this operation is not supported by the implementing class.
     */
    fun storeIn(buffer: ByteBuffer): ByteBuffer

    /**
     * Stores the content of this object inside the specified [buffer].
     *
     * @param buffer The [FloatBuffer] in which the objects data should be stored.
     * @return The given [buffer] instance.
     * @throws UnsupportedOperationException If this operation is not supported by the implementing class.
     */
    fun storeIn(buffer: FloatBuffer): FloatBuffer

    /**
     * Stores the content of this object inside the specified [buffer].
     *
     * @param buffer The [DoubleBuffer] in which the objects data should be stored.
     * @return The given [buffer] instance.
     * @throws UnsupportedOperationException If this operation is not supported by the implementing class.
     */
    fun storeIn(buffer: DoubleBuffer): DoubleBuffer

}
