package de.colibriengine.buffers

import org.lwjgl.BufferUtils
import java.nio.ByteBuffer
import java.nio.FloatBuffer
import java.nio.IntBuffer
import java.util.function.Consumer

/**
 * Contains helper methods for IntBuffer and FloatBuffer allocation.
 *
 * @since ColibriEngine 0.0.1.2
 */
object BufferTools {

    /** Get little endian data from a buffer */
    fun getLE(buffer: ByteArray, pos: Int, numBytes: Int): Long {
        var at = pos
        var amount = numBytes
        amount--
        at += amount
        var value: Long = (buffer[at].toInt() and 0xFF).toLong()
        for (b in 0 until amount) {
            value = (value shl 8) + (buffer[--at].toInt() and 0xFF)
        }
        return value
    }

    /** Put little endian data in a buffer */
    fun putLE(value: Long, buffer: ByteArray, pos: Int, numBytes: Int) {
        var insert = value
        var at = pos
        for (b in 0 until numBytes) {
            buffer[at] = (insert and 0xFF).toByte()
            insert = insert shr 8
            at++
        }
    }

    /**
     * Returns any number of int values in an IntBuffer object.
     *
     * @param values All int values to put in the IntBuffer.
     * @return A new allocated and filled IntBuffer.
     */
    fun toIntBuffer(vararg values: Int): IntBuffer {
        val buff = BufferUtils.createIntBuffer(values.size)
        buff.put(values)
        buff.flip()
        return buff
    }

    /**
     * Returns all Integers stored in the ArrayList object in an IntBuffer object.
     *
     * @param values ArrayList of type Integer.
     * @return A new allocated and filled IntBuffer.
     * @throws IllegalArgumentException if the given ArrayList object is null.
     */
    fun toIntBuffer(values: ArrayList<Int>): IntBuffer {
        val buff = BufferUtils.createIntBuffer(values.size)
        values.forEach(Consumer { i: Int? -> buff.put(i!!) })
        buff.flip()
        return buff
    }

    /**
     * Returns any number of float values in a FloatBuffer object. After filling, the buffer gets flipped.
     *
     * @param values All float values to put in the FloatBuffer.
     * @return A new allocated, filled and flipped FloatBuffer.
     */
    fun asFlippedFloatBuffer(buff: FloatBuffer, vararg values: Float): FloatBuffer {
        check(buff.capacity() >= values.size) { "BufferTools: buffer capacity must be >= values.length" }
        buff.clear()
        buff.put(values)
        buff.flip()
        return buff
    }

    /**
     * Returns any number of float values in a FloatBuffer object. After filling, the buffer gets flipped.
     *
     * @param values All float values to put in the FloatBuffer.
     * @return A new allocated, filled and flipped FloatBuffer.
     */
    fun toFloatBuffer(vararg values: Float): FloatBuffer {
        val buff = BufferUtils.createFloatBuffer(values.size)
        buff.put(values)
        buff.flip()
        return buff
    }

    /**
     * Creates a ByteBuffer from the given strings.
     *
     * @param strings The strings to store into the ByteBuffer.
     * @return a filled and flipped ByteBuffer.
     */
    @JvmStatic
    fun toByteBuffer(strings: Array<String>): ByteBuffer {
        // Calculate the size of the ByteBuffer that is to be returned.
        var size = 0
        for (string in strings) {
            size += string.length + 1 // +1 for NULL-character.
        }

        // Create the ByteBuffer with the appropriate size.
        val buff = BufferUtils.createByteBuffer(size)

        // Store all strings (each followed by a NULL-character) in the ByteBuffer.
        for (string in strings) {
            buff.put(string.toByteArray())
            buff.put(0.toByte()) // The ending NULL-character to terminate the string.
        }
        buff.flip()
        return buff
    }

}
