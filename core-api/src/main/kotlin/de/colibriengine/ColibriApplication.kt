package de.colibriengine

/**
 * Provides callback methods for the ColibriEngine.
 *
 * @since ColibriEngine 0.0.5.9
 */
interface ColibriApplication {

    /**
     * Gets called once on the startup of the engine, right before the engine will enter its main loop.
     */
    fun onAppStart()

    /**
     * Gets called when the engine starts its input/update/render loop.
     */
    fun onAppLoopStarted()

    /**
     * Gets called when the engine stops its input/update/render loop.
     */
    fun onAppLoopStopped()

    /**
     * Gets called once per frame through the main loop.
     */
    fun onAppUpdate()

    /**
     * Gets called once the engine exits its main loop. Either through a feStop() call or other circumstances.
     */
    fun onAppTermination()

}
