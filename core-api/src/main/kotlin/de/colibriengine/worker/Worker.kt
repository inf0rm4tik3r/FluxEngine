package de.colibriengine.worker

import de.colibriengine.renderer.RenderingEngine

interface Worker {

    var id: UInt

    var shouldTerminate: Boolean

    val renderer: RenderingEngine

    /** Called once by a worker manager when this worker is added to it. */
    fun onAdd()

    /** Called once for every window the worker created. */
    fun onTerminate(windowId: Long)

    /** Called once per frame. The worker needs to be registered in a worker manager. */
    fun updateFrame()

    /** Called once per frame. The worker needs to be registered in a worker manager. */
    fun renderFrame()

    /**
     * Called once for every window in every frame the worker created. The worker needs to be registered in a worker
     * manager.
     */
    fun updatePerWindow(windowId: Long)

    /**
     * Called once for every window the worker created, added and still uses. This method only gets called if the worker
     * uses at least one window and the worker is not marked for termination.
     *
     * @param windowId The id of the window whose context is current in the current thread. One of the worker windows.
     */
    fun renderPerWindow(windowId: Long)

}
