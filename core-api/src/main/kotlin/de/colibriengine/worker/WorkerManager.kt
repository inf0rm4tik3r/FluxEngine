package de.colibriengine.worker

import de.colibriengine.graphics.window.Window

interface WorkerManager {

    /**
     * The given worker is added and will now be executed until you terminate or remove it again.
     *
     * @param worker The worker that gets added to the manager. Automatically assigns a new worker id.
     */
    fun addWorker(worker: Worker)
    fun addWindow(worker: Worker, window: Window)

    fun updateAndRenderAll()

    fun terminateAllWorkers()

    /** Iterates over all registered workers and terminates them if requested. */
    fun cleanup()

}
