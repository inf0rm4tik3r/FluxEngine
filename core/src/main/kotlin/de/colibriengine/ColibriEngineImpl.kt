package de.colibriengine

import de.colibriengine.asset.ResourceLoader
import de.colibriengine.audio.AudioMaster
import de.colibriengine.components.TransformCalculationComponent
import de.colibriengine.components.TransformComponent
import de.colibriengine.ecs.ECS
import de.colibriengine.ecs.registerComponent
import de.colibriengine.graphics.components.InternalRenderComponent
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.font.FontManager
import de.colibriengine.graphics.opengl.GLUtil
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.window.WindowManager
import de.colibriengine.graphics.window.WindowManagerImpl
import de.colibriengine.input.*
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.renderer.DebugRenderer
import de.colibriengine.renderer.RenderingEngine
import de.colibriengine.util.*
import de.colibriengine.vr.OvrSessionManager
import de.colibriengine.vr.oculus.OVRInitializationException
import de.colibriengine.vr.oculus.OVRLib
import de.colibriengine.vr.oculus.OvrSessionManagerImpl
import de.colibriengine.worker.WorkerManager
import de.colibriengine.worker.WorkerManagerImpl
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.lwjgl.glfw.GLFW
import org.lwjgl.system.Library
import java.io.File
import org.lwjgl.Version as LwjglVersion

/**
 * Main class of the engine providing the core functionality. Multiple instances are allowed.
 */
class ColibriEngineImpl : ColibriEngine, KoinComponent {

    override var shouldTerminate = false
    val running
        get() = !shouldTerminate

    override var terminateWithoutWindows = true
    private lateinit var application: ColibriApplication

    override val resourceLoader: ResourceLoader
    override val windowManager: WindowManager
    override val inputManager: InputManager
    override val workerManager: WorkerManager
    override val shaderManager: ShaderManager
    override val fontManager: FontManager
    override val ovrSessionManager: OvrSessionManager

    val audioMaster: AudioMaster

    override val timer: Timer
    private val inputCheckTimer: Timer

    override val ecs = get<ECS>()
    override val internalEcs = get<ECS>()

    // TODO: is only not null when the worker renders...
    override var currentRenderer: RenderingEngine? = null
    override val debugRenderer: DebugRenderer?
        get() = currentRenderer?.debugRenderer

    /**
     * Constructs and initializes a new instance of the ColibriEngine.
     * Follow the creation by calling the [ColibriEngine.start] method to start the engines main loop.
     *
     * @throws IllegalStateException if the libraries are currently not initialized.
     * @see ColibriEngine.initializeLibraries
     */
    init {
        LOG.info("-----------------------------------------")
        LOG.info("---   ColibriEngine initializes...    ---")
        LOG.info("-----------------------------------------")

        require(ecs != internalEcs) { "Engine internal ECS and user ECS should not be the same instances!" }

        LOG.info("-------   libraries   -------")
        check(librariesInitialized) { "ColibriEngine: Libraries are currently not initialized!" }

        LOG.info("----------   ecs   ----------")
        internalEcs.registerComponent<TransformComponent>()
        internalEcs.registerComponent<TransformCalculationComponent>()
        internalEcs.registerComponent<RenderComponent>()
        internalEcs.registerComponent<InternalRenderComponent>()

        ecs.registerComponent<TransformComponent>()
        ecs.registerComponent<TransformCalculationComponent>()
        ecs.registerComponent<RenderComponent>()

        LOG.info("--------   managers   -------")
        resourceLoader = get()

        ovrSessionManager = OvrSessionManagerImpl()
        windowManager = WindowManagerImpl()

        LOG.info("---------   input   ---------")
        inputManager = get()
        inputManager.joystickManager.checkJoystickConnections()

        GLUtil.setWindowManagerRef(windowManager)
        workerManager = WorkerManagerImpl(this)
        shaderManager = de.colibriengine.graphics.shader.ShaderManagerImpl()
        fontManager = get()

        LOG.info("---------   audio   ---------")
        audioMaster = get()
        audioMaster.initAudioSystem()

        LOG.info("---------   timing   --------")
        timer = Timer()
        inputCheckTimer = Timer()

        // Print system information.
        LOG.info("------   System Info   ------")
        logSystemInfo()

        LOG.info("-----------------------------------------")
        LOG.info("---   ColibriEngine initialized :)   ----")
        LOG.info("-----------------------------------------")
    }

    /**
     * Starts the engine loop. Method blocks!
     */
    override fun start(application: ColibriApplication) {
        this.application = application

        // Set the condition variable that keeps the engine running.
        shouldTerminate = false

        // Let the application initialize its data.
        this.application.onAppStart()

        // Now start by entering the main loop.
        mainLoop()

        // After shouldTerminate was set to true, perform final clean up and terminate this instance of the ColibriEngine.

        // Release all data created by this instance.
        terminate()
    }

    private fun mainLoop() {
        while (running) {
            checkInput()
            updateAndRender()
        }
    }

    private fun checkInput() {
        // Check for connected/disconnected input devices.
        if (inputCheckTimer.view.ticks() >= 3) {
            inputManager.joystickManager.checkJoystickConnections()
            inputCheckTimer.reset()
        }

        // Invokes input callbacks.
        inputManager.pollInput(false)
    }

    private fun updateAndRender() {
        updateTimer()

        // Update and render all registered worker objects.
        application.onAppUpdate()
        workerManager.updateAndRenderAll()

        workerManager.cleanup()
        // Close/Terminate all windows/worker whose shouldClose/shouldTerminate flag is true.
        // Could release the last window/worker!
        windowManager.cleanup()

        // Stop the main loop if no windows exist anymore and the "ENGINE_SHOULD_TERMINATE_WITHOUT_WINDOWS" flag is
        // set true.
        if (!windowManager.hasWindows() && terminateWithoutWindows) {
            LOG.info(
                "ColibriEngine: No windows left and ENGINE_SHOULD_TERMINATE_WITHOUT_WINDOWS -> terminating..."
            )
            shouldTerminate = true
        }
    }

    override fun terminate() {
        application.onAppTermination()
        workerManager.terminateAllWorkers()
        windowManager.releaseAllWindows()
        if (OVRLib.isInitialized()) {
            ovrSessionManager.releaseAll()
        }
        audioMaster.destroyAudioSystem()
    }

    private fun updateTimer() {
        // Update the main application timer.
        timer.tick()
        if (timer.view.elapsedTimeSP() >= 1) {
            // Another second passed. Lets print some timing information.
            if (LOG_FPS) {
                LOG.info(
                    "FPS: {}, AvgFrameTime: {}ms, MaxFrameTime: {}ms",
                    timer.view.ticks(),
                    timer.view.averageDeltaInMilliseconds(),
                    timer.view.maxDeltaInMilliseconds()
                )
            }
            timer.resetElapsedTime()
            timer.resetTicks()
            timer.resetMaxDelta()
            inputCheckTimer.tick()
        }
    }

    companion object {
        var DEBUG_MODE = false
        var LOG_FPS = true

        // Condition variable that stores whether the libraries are currently initialized or not.
        private var librariesInitialized = false

        /**
         * The logger instance used in this class.
         */
        private val LOG = getLogger(ColibriEngineImpl::class.java)

        /**
         * Initializes all engine related libraries.
         * This method needs to be called once before an instance of the ColibriEngine can be created.
         *
         * Additional calls to this method after a successful initialization but before termination will return immediately.
         *
         * @param lwjglNativesPath The path where an exported project will look for its natives. Use "natives" as an example to let an
         * executable jar search its natives in a so called subfolder next to it.
         * Passing null is permitted.
         * @throws IllegalStateException if GLFW could not be initialized.
         */
        /**
         * Calls [ColibriEngineImpl.initializeLibraries] with null.
         *
         * @see ColibriEngineImpl.initializeLibraries
         */
        @JvmStatic
        @JvmOverloads
        fun initializeLibraries(lwjglNativesPath: String? = null) {
            if (librariesInitialized) {
                LOG.info("ColibriEngine: Libraries are already initialized.")
                return
            }
            LOG.info("------   Initialize libraries...   ------")

            // Set the system property "org.lwjgl.librarypath" to lwjglNativesPath, so that an exported project will search
            // for its natives in a nativesPath called subfolder.
            if (lwjglNativesPath != null) {
                setSystemProperty(
                    "org.lwjgl.librarypath", File(lwjglNativesPath).absolutePath
                )
            }

            // Print JAVA/LWJGL/GLFW version information. Also used to let the static initializer run.
            LOG.info("JAVA version:  " + System.getProperty("java.version"))
            LOG.info("LWJGL version:  " + LwjglVersion.getVersion() + " - " + LwjglVersion.BUILD_TYPE + " (" + Library.JNI_LIBRARY_NAME + ")")
            LOG.info("GLFW version:  " + GLFW.glfwGetVersionString())

            // Initializes the GLFW library.
            WindowManagerImpl.initializeGLFW()

            // Calculates the highest supported OpenGL version of the system the program is running on.
            GLUtil.findMaxSupportedGLVersion()
            LOG.info(
                "MaxSupportedGLVersion is: {}.{}",
                GLUtil.getMaxSupportedGLVersionMajor(),
                GLUtil.getMaxSupportedGLVersionMinor()
            )

            // Initialize the OVR library.
            try {
                OVRLib.initialize()
            } catch (exception: OVRInitializationException) {
                exception.printStackTrace()
            }
            LOG.info("-----   initialization successful   -----")
            librariesInitialized = true
        }

        /**
         * Terminates all engine related libraries.
         * Needs to be called after the termination of all ColibriEngine instances.
         *
         *
         * Additional calls to this method, after a successful termination, but before another initialization, will return
         * immediately.
         */
        @JvmStatic
        fun terminateLibraries() {
            if (!librariesInitialized) {
                LOG.info("ColibriEngine: Libraries are already terminated.")
                return
            }
            LOG.info("-------   Terminate libraries...   ------")

            // Terminates the GLFW library.
            WindowManagerImpl.terminateGLFW()

            // Terminates the OVR library (includes the destruction of all previously created OVRSession instances).
            OVRLib.terminate()

            /*
             *  Add termination calls of further libraries here.
             */
            LOG.info("------   termination successful   -------")
            librariesInitialized = false
        }
    }

}
