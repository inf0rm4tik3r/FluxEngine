package de.colibriengine.graphics.window

import de.colibriengine.graphics.opengl.GLSet
import de.colibriengine.graphics.opengl.window.AbstractBuildableOpenGLWindow
import de.colibriengine.graphics.opengl.window.OpenGLWindowBuilder
import de.colibriengine.input.FECommands
import de.colibriengine.input.InputType
import de.colibriengine.logging.LogUtil.getLogger
import org.lwjgl.glfw.GLFW

/**
 * This is the default implementation of the abstract window definition. You may create your own window class
 * (`MyCustomWindow`). Windows can be created by calling:
 *
 *      WindowBuilder.newInstance(windowManager)
 *          .set*(...)
 *          [...]
 *          .build(MyCustomWindow.class);
 */
class DefaultWindow private constructor(builder: OpenGLWindowBuilder) : AbstractBuildableOpenGLWindow(builder) {

    override fun beforeDestruction() {
        // Do nothing...
    }

    override fun afterDestruction() {
        // Do nothing...
    }

    override fun errorCallback(error: Int, description: String) {
        LOG.error(
            "GLFW error ($error): $description\nIn window: $windowHandle",
            Exception("Stack trace")
        )
    }

    override fun closeCallback() {
        //LOG.trace(() -> windowHandle + " should close.");
    }

    override fun focusCallback(focused: Boolean) {
        // Activate this window after it obtained the focus.
        if (focused) {
            //LOG.trace(() -> windowHandle + " gained focus.");
            windowManager.activeWindow = this@DefaultWindow
        }
    }

    override fun sizeCallback(width: Int, height: Int) {
        //LOG.trace(() -> windowHandle + " size: " + width + ", " + height);
    }

    override fun posCallback(xpos: Int, ypos: Int) {
        //LOG.trace(() -> windowHandle + " moved: " + xpos + ", " + ypos);
    }

    override fun iconifyCallback(iconified: Boolean) {
        //LOG.trace(windowHandle + " iconified: " + iconified);
    }

    override fun maximizeCallback(maximized: Boolean) {
        //LOG.trace(windowHandle + " maximized: " + maximized);
    }

    override fun refreshCallback() {
        //LOG.trace(windowHandle + " refreshed");
    }

    override fun dropCallback(names: Array<String>) {
        // Print an information about how many files have been dropped into the window.
        //LOG.trace("DefaultWindow: %d dropped %d file%s:%n",
        //    windowHandle, names.length, names.length == 1 ? "" : "s"
        //);

        // Print the path of every dropped file.
        for (i in names.indices) {
            LOG.info("\t" + (i + 1) + ") " + names[i])
        }
    }

    override fun framebufferSizeCallback(width: Int, height: Int) {
        // Update the viewport so that we will still render correctly.
        GLSet.viewport(0, 0, width, height)
        //LOG.trace(() -> windowHandle + " new FB size: " + width + ", " + height);
    }

    override fun contentScaleCallback(xScale: Float, yScale: Float) {
        //LOG.trace(() -> windowHandle + " new content scale: " + xScale + ", " + yScale);
    }

    override fun keyCallback(key: Int, scancode: Int, action: Int, mods: Int) {
        inputManager.activeInputType = InputType.MOUSE_KEYBOARD
        if (key == FECommands.LOCK_MOUSE.kKey && action == FECommands.LOCK_MOUSE.kKeyAction) {
            cursorMode = if (cursorMode !== CursorMode.LOCKED) {
                CursorMode.LOCKED
            } else {
                CursorMode.NORMAL
            }
            inputManager.mouseKeyboardManager.resetDelta()
        }

        /*
         * Action could be GLFW_PRESS, GLFW_REPEAT or GLFW_RELEASE. REPEAT callbacks have a system dependant delay.
         * Throw them away. We are going to calculate our own GLFW_REPEAT events!
         */
        if (action != GLFW.GLFW_REPEAT) {
            inputManager.mouseKeyboardManager.invokeKeyCallbacks(key, scancode, action, mods)
        }
    }

    override fun charCallback(codepoint: Int) {}
    override fun charModsCallBack(codepoint: Int, mods: Int) {}
    override fun cursorEnterCallback(entered: Boolean) {}
    override fun cursorPosCallback(xpos: Double, ypos: Double) {
        inputManager.mouseKeyboardManager.trackCursor(xpos, ypos)
    }

    override fun mouseButtonCallback(button: Int, action: Int, mods: Int) {
        inputManager.activeInputType = InputType.MOUSE_KEYBOARD
        inputManager.mouseKeyboardManager.invokeMouseButtonCallbacks(button, action, mods)
    }

    override fun scrollCallback(xoffset: Double, yoffset: Double) {}

    companion object {
        private val LOG = getLogger<DefaultWindow>()
    }

}
