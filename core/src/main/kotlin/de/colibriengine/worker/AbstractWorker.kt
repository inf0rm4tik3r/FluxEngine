package de.colibriengine.worker

import de.colibriengine.ColibriEngine
import de.colibriengine.renderer.RenderingEngineImpl

/**
 * Representation of a worker unit that provides you with the methods of a basic program loop. Inherit from this class
 * and add any created objects to the engine through [WorkerManagerImpl.addWorker].
 */
abstract class AbstractWorker(val engine: ColibriEngine) : Worker {

    /**
     * Marks this worker for termination. A marked worker gets terminated at the start of the next frame. While
     * terminating the worker, its workerTerminate() gets called so that a final cleanup can be performed.
     */
    override var shouldTerminate = false

    override lateinit var renderer: RenderingEngineImpl

    override var id: UInt = 0u
        get() {
            if (field == 0u) {
                throw RuntimeException(
                    "Worker was not added to the worker manager before. " +
                            "Therefore no workerID could be generated. Window creation is permitted."
                )
            }
            return field
        }

}
