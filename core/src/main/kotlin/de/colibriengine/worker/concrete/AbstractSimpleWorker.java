package de.colibriengine.worker.concrete;

import de.colibriengine.ColibriEngine;
import de.colibriengine.graphics.camera.Camera;
import de.colibriengine.graphics.opengl.GLUtil;
import de.colibriengine.graphics.opengl.context.GLProfile;
import de.colibriengine.graphics.opengl.window.OpenGLWindowBuilder;
import de.colibriengine.graphics.window.DebugBehaviour;
import de.colibriengine.graphics.window.DefaultWindow;
import de.colibriengine.input.FECommands;
import de.colibriengine.input.JoystickInputCallbacks;
import de.colibriengine.input.MouseKeyboardInputCallbacks;
import de.colibriengine.math.vector.vec2i.StdVec2i;
import de.colibriengine.renderer.DebugRendererImpl;
import de.colibriengine.renderer.RenderOptionsImpl;
import de.colibriengine.renderer.RenderingEngineImpl;
import de.colibriengine.scene.AbstractScene;
import de.colibriengine.ui.GUI;
import de.colibriengine.worker.AbstractWorker;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static de.colibriengine.ColibriEngineKt.getENGINE_NAME;
import static org.lwjgl.system.MemoryUtil.NULL;

public class AbstractSimpleWorker
        extends AbstractWorker
        implements MouseKeyboardInputCallbacks, JoystickInputCallbacks {

    private @NotNull String applicationName;
    private @NotNull DefaultWindow window;
    private @Nullable AbstractScene currentScene;

    public AbstractSimpleWorker(final ColibriEngine engine) {
        super(engine);
        applicationName = "APP";
    }

    @Override
    public void onAdd() {
        window = OpenGLWindowBuilder
                .newInstance(
                        getEngine().getWindowManager(),
                        getEngine().getInputManager(),
                        getEngine().getResourceLoader()
                )
                .setTitle(applicationName + "  -  " + getENGINE_NAME())
                .setIconResources(List.of(
                        //"icon/Colibri_16.png", // Including the 16x16 icon leads to a blurry window icon..
                        "icon/Colibri_32.png",
                        "icon/Colibri_64.png",
                        "icon/Colibri_128.png",
                        "icon/Colibri_256.png",
                        "icon/Colibri_512.png"
                ))
                .setDesiredMonitor(-1)
                .setPosition(-1, -1)
                .setResolution(1920, 1080)
                .setVisible(true)
                .setResizable(true)
                .setDecorated(true)
                .setFullscreen(false)
                .setVSync(false)
                .setOpenGLTargetVersion(GLUtil.getMaxSupportedGLVersion())
                .setOpenGLProfile(GLProfile.CORE_PROFILE)
                .setOpenGLForwardCompatible(true)
                .setSharedContext(NULL)
                .setDebugMode(true)
                .setDebugBehaviour(DebugBehaviour.CREATE_EXCEPTION)
                .build(DefaultWindow.class);
        window.getClearColor().set(0.9f, 0.9f, 0.9f, 1.0f);

        getEngine().getWorkerManager().addWindow(this, window);

        // Add the window to the engines windowing system.
        // Make this window the active window, so that the following code can access this window through the engines
        // window manager instance.
        getEngine().getWindowManager().addWindow(window, true);

        // Set OpenGL states and enable the OpenGL debugging functionality.
        GLUtil.setOpenGLStates();

        // Add the worker to the windows input system to receive its input callbacks.
        getEngine().getInputManager().addMKInputCallback(this);
        getEngine().getInputManager().addJInputCallback(this);

        /* SET UP RENDERER */
        // TODO: Refactor RenderOptions to builder pattern?
        final RenderOptionsImpl renderOptions = new RenderOptionsImpl();
        //renderOptions.setSceneResolution(new StdVec2i().set(3840, 2160));
        //renderOptions.setSceneResolution(new StdVec2i().set(2560, 1440));
        renderOptions.setSceneResolution(new StdVec2i().set(1920, 1080));
        //renderOptions.setSceneResolution(new StdVec2i().set(1280, 720));
        //renderOptions.setSceneResolution(new StdVec2i().set(1280/2, 720/2));
        //renderOptions.setGuiResolution(new StdVec2i().set(3840, 2160));
        //renderOptions.setGuiResolution(new StdVec2i().set(2560, 1440));
        renderOptions.setGuiResolution(new StdVec2i().set(1920, 1080));
        //renderOptions.setGuiResolution(new StdVec2i().set(1280, 720));

        //renderOptions.getGlobalVarianceShadowMapSettings().setVarianceMaxBound();

        setRenderer(new RenderingEngineImpl(
                getEngine().getEcs(),
                getEngine().getInternalEcs(),
                getEngine().getWindowManager(),
                getEngine().getShaderManager(),
                window.getModelManager().getModelFactory(),
                renderOptions,
                new DebugRendererImpl(getEngine().getShaderManager())
        ));
    }

    @Override
    public void updateFrame() {
    }

    @Override
    public void updatePerWindow(final long window) {
        if (window == this.window.getId()) {
            getRenderer().update();

            if (this.window.getShouldClose()) {
                setShouldTerminate(true);
            }
            if (currentScene != null) {
                final Camera activeCamera = this.window.getCameraManager().getActiveCamera();
                if (activeCamera != null) {
                    activeCamera.initProjectionMatrices(getEngine().getCurrentRenderer().getRenderOptions()
                            .getSceneResolution());
                    activeCamera.calculateViewProjectionMatrices();

                    // Update the scene.
                    currentScene.updateNodeTransformation(
                            activeCamera.getOrthographicViewProjectionMatrix(),
                            activeCamera.getPerspectiveViewProjectionMatrix()
                    );
                    currentScene.updateNode(getEngine().getTimer().view);

                    // Update the GUIs.
                    for (final GUI gui : currentScene.getGuis()) {
                        gui.getRootNode().updateNode(getEngine().getTimer().view);
                    }
                }
            }
        }
    }

    @Override
    public void renderFrame() {
    }

    @Override
    public void renderPerWindow(final long window) {
        // Render the content of the "window".
        if (window == this.window.getId()) {
            if (currentScene != null) {
                // Render the scene.
                getRenderer().render(currentScene, getEngine().getTimer().view);

                // Render the GUIs.
                for (final GUI gui : currentScene.getGuis()) {
                    getRenderer().renderGUI(gui);
                }
            }
        }
    }

    @Override
    public void onTerminate(final long windowId) {
        if (windowId == window.getId()) {
            if (currentScene != null) {
                currentScene.destroyScene();
            }
            //TODO What needs to be done?
            //window.getModelManager().release(model); // Not needed. WorkerManager handles that.
            window.setShouldClose(true);
        }
    }

    @Override
    public void jAxisCallback(final int axis, final float axisValue) {
    }

    @Override
    public void jButtonCallback(final int button, final int action) {
    }

    @Override
    public void jLostConnectionCallback(final int index) {
    }

    @Override
    public void mkPosCallback(int x, int y) {
    }

    @Override
    public void mkMoveCallback(final int dx, final int dy) {
    }

    @Override
    public void mkButtonCallback(final int button, final int action, final int mods) {
    }

    @Override
    public void mkKeyCallback(final int key, final int scancode, final int action, final int mods) {
        // if (windowId == window.getId()) {
        if (key == FECommands.CLOSE_COMMAND.getKKey() && action == FECommands.CLOSE_COMMAND.getKKeyAction()) {
            getEngine().setShouldTerminate(true);
        }
        // }
    }

    public @NotNull String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(final @NotNull String applicationName) {
        this.applicationName = applicationName;
        window.setTitle(applicationName + "  -  " + getENGINE_NAME());
    }

    public @NotNull DefaultWindow getWindow() {
        return window;
    }

    public void disposeCurrentScene() {
        if (currentScene != null) {
            currentScene.destroyScene();
            currentScene = null;
        }
    }

    public void setCurrentScene(final @NotNull AbstractScene currentScene) {
        this.currentScene = currentScene;
    }

    public @Nullable AbstractScene getCurrentScene() {
        return currentScene;
    }

}
