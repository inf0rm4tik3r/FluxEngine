/*
package de.colibriengine.camera

import de.colibriengine.graphics.renderer.RenderPass
import de.colibriengine.math.quaternion.quaternionF.StdQuaternionF
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.util.Timer
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class TestCamera : AbstractCamera(1, null) {

    override val classID = 1L
    override fun init() {}
    override fun update(timing: Timer.View) {}
    override fun render(renderPass: RenderPass) {}
    override fun tearDown() {}

}

internal class AbstractCameraTest {

    private lateinit var camera: AbstractCamera

    @BeforeEach
    fun setUp() {
        camera = TestCamera()
    }

    @Test
    fun cameraFacingRotation() {
        val q = StdQuaternionF()
        camera.position.set(1f, 1f, 1f)
        camera.getCameraFacingRotation(StdVec3f(0f, 0f, 0f), q)
        val forward = q.forward(StdVec3f())
        assertEquals(StdVec3f(), forward)
    }

}
*/
