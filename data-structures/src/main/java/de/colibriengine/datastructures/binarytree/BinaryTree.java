package de.colibriengine.datastructures.binarytree;

import org.jetbrains.annotations.Nullable;

/**
 * BinaryTree
 *
 * @param <T>
 *     The class of the objects which are gonna be stored in this tree.
 * @param <C>
 *     The class in the inheritance chain of {@code T} which implements the {@link Comparable} interface.
 *     Defines how elements in this tree get compared.
 *
 * @version 0.0.0.6
 * @time 19.04.2018 09:28
 * @since ColibriEngine 0.0.7.0
 */
public class BinaryTree<T extends C, C extends Comparable<C>> {
    
    /**
     * The root element of this tree.
     */
    private @Nullable BinaryTreeNode<T, C> root;
    
    /**
     * Constructs a new BinaryTree.
     */
    public BinaryTree() {
        root = null;
    }
    
    /**
     * Inserts {@code data} into this tree.
     *
     * @param data
     *     Data object which should be added.
     */
    public void insert(final T data) {
        final BinaryTreeNode<T, C> bte = new BinaryTreeNode<>(data);
        if (root == null) {
            root = bte;
        } else {
            insert(root, bte);
        }
    }
    
    /**
     * Internal recursive method which finds the right position for the new element {@code newElem} and inserts it.
     *
     * @param root
     *     Current root to look at.
     * @param newElem
     *     The new element which has to be inserted.
     */
    private void insert(final BinaryTreeNode<T, C> root, final BinaryTreeNode<T, C> newElem) {
        // Insert at the right.
        if (newElem.getData().compareTo(root.getData()) >= 0) {
            if (root.getRight() == null) {
                root.setRight(newElem);
            } else {
                insert(root.getRight(), newElem);
            }
        }
        // Insert at the left.
        else {
            if (root.getLeft() == null) {
                root.setLeft(newElem);
            } else {
                insert(root.getLeft(), newElem);
            }
        }
    }
    
    /**
     * Removes the element of this tree which holds the given value.
     *
     * @param data
     *     The value to remove.
     */
    public void remove(final T data) {
        remove(root, data);
    }
    
    /**
     * Internal recursive method to delete the given value.
     *
     * @param root
     *     Current root to look at.
     * @param data
     *     Value to remove.
     */
    private void remove(final BinaryTreeNode<T, C> root, final T data) {
        final BinaryTreeNode<T, C> node = searchNode(root, data);
        
        if (node == null) {
            // Data could not be found. Nothing to remove...
            return;
        }
        
        final boolean isLeftOfParent = node.getParent().getLeft() == node;
        //final boolean isRightOfParent = node.getParent().getRight() == node;
        
        // Set the parent references according to our amount of children.
        if (node.isLeaf()) {
            if (isLeftOfParent) {
                root.getParent().setLeft(null);
            } else {
                root.getParent().setRight(null);
            }
        }
        // "node" has either left or right child.
        else if (node.hasOneChild()) {
            final BinaryTreeNode<T, C> child = node.hasLeft() ? node.getLeft() : node.getRight();
            
            if (isLeftOfParent) {
                node.getParent().setLeft(child);
            } else {
                node.getParent().setRight(child);
            }
        }
        // "node" has both children.
        else {
            // Find the smallest data of the right subtree.
            BinaryTreeNode<T, C> rightTreeSmallest = node.getRight();
            while (rightTreeSmallest.getLeft() != null) {
                rightTreeSmallest = rightTreeSmallest.getLeft();
            }
            
            // Set "node" to hold this data.
            node.setData(rightTreeSmallest.getData());
            
            // Remove the node which got fund in the right subtree.
            remove(rightTreeSmallest, rightTreeSmallest.getData());
        }
    }
    
    /**
     * Checks if {@code} data is contained in this tree.
     *
     * @param data
     *     Data to search for.
     *
     * @return true if this tree contains {@code} data. Otherwise false.
     */
    public boolean contains(final T data) {
        return searchNode(root, data) != null;
    }
    
    /**
     * Internal recursive method which searches the tree node that hold {@code data}.
     *
     * @param root
     *     Current node to look at.
     * @param data
     *     Data to search for.
     *
     * @return The node that holds {@code data} or null if no such {@code BinaryTreeNode} could be found.
     */
    private BinaryTreeNode<T, C> searchNode(final BinaryTreeNode<T, C> root, final T data) {
        // If there is no more element to look at, the data could not be found.
        if (root == null) {
            //System.err.println("BinaryTree: Data: \"" + data + "\" could not be found!");
            return null;
        }
        
        // Found?
        if (data == root.getData()) {
            return root;
        }
        // Data must be at the right.
        else if (data.compareTo(root.getData()) > 0) {
            return searchNode(root.getRight(), data);
        }
        // Data must be at the left.
        else {
            return searchNode(root.getLeft(), data);
        }
    }
    
}
