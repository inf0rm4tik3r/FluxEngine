package de.colibriengine.datastructures.binarytree;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * BinaryTreeNode
 *
 * @version 0.0.0.5
 * @time 19.04.2018 09:28
 * @since ColibriEngine 0.0.7.0
 */
public class BinaryTreeNode<T extends C, C extends Comparable<C>> {
    
    private @Nullable BinaryTreeNode<T, C> parent;
    private @Nullable BinaryTreeNode<T, C> left;
    private @Nullable BinaryTreeNode<T, C> right;
    private @NotNull  T                    data;
    
    /**
     * @param data
     */
    BinaryTreeNode(final @NotNull T data) {
        this.data = data;
    }
    
    /**
     * An element is considered to be a lead if it has no follower / children.
     *
     * @return Whether this tree element is a leaf.
     */
    public boolean isLeaf() {
        return (left == null && right == null);
    }
    
    /**
     * @return true if exactly one child is not null.
     */
    public boolean hasOneChild() {
        return (left != null ^ right == null);
    }
    
    /**
     * @return true if exactly both children are not null.
     */
    public boolean hasTwoChildren() {
        return (left != null && right != null);
    }
    
    public boolean hasLeft() {
        return left != null;
    }
    
    public boolean hasRight() {
        return right != null;
    }
    
    public @Nullable BinaryTreeNode<T, C> getParent() {
        return parent;
    }
    
    public void setParent(final @NotNull BinaryTreeNode<T, C> parent) {
        this.parent = parent;
    }
    
    public @Nullable BinaryTreeNode<T, C> getLeft() {
        return left;
    }
    
    public void setLeft(final @Nullable BinaryTreeNode<T, C> left) {
        if (left != null) {
            left.parent = this;
        }
        this.left = left;
    }
    
    public @Nullable BinaryTreeNode<T, C> getRight() {
        return right;
    }
    
    public void setRight(final @Nullable BinaryTreeNode<T, C> right) {
        if (right != null) {
            right.parent = this;
        }
        this.right = right;
    }
    
    public @NotNull T getData() {
        return data;
    }
    
    public void setData(final @NotNull T data) {
        this.data = data;
    }
    
}
