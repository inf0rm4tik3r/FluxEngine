package de.colibriengine.datastructures.graph

open class Edge<T>(
    val start: Node<T>,
    val end: Node<T>
) {

    open fun toSimpleString(): String = "(${start.data},${end.data})"

    override fun toString(): String = "Edge(start=$start, end=$end)"

}
