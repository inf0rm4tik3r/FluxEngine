package de.colibriengine.datastructures.list;

import de.colibriengine.exception.DataNotFoundException;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * DoublyLinkedList
 *
 * @param <T>
 *     The type of the data objects stored in this list.
 *
 * @version 0.0.0.3
 * @created 04.12.2018 21:06
 * @time 04.12.2018 21:06
 * @since ColibriEngine 0.0.7.2
 */
public class DoublyLinkedList<T> implements Iterable<T> {
    
    private @Nullable DoublyLinkedListNode<T> head;
    private @Nullable DoublyLinkedListNode<T> last;
    
    private int size;
    
    public DoublyLinkedList() {
        head = null;
        last = null;
        size = 0;
    }
    
    /**
     * Time complexity: O(n)
     *
     * @return
     *
     * @throws DataNotFoundException
     */
    public @Nullable DoublyLinkedListNode<T> searchFirstOccurrence(final @NotNull T data) {
        @Nullable DoublyLinkedListNode<T> nodeToInspect = head;
        while (nodeToInspect != null) {
            if (nodeToInspect.getData().equals(data)) {
                return nodeToInspect;
            }
            nodeToInspect = nodeToInspect.getNext();
        }
        return null;
    }
    
    /**
     * Time complexity: O(n)
     *
     * @return
     *
     * @throws DataNotFoundException
     */
    public @Nullable DoublyLinkedListNode<T> searchFirstOccurrence(final @NotNull DoublyLinkedListNode<T> node) {
        @Nullable DoublyLinkedListNode<T> nodeToInspect = head;
        while (nodeToInspect != null) {
            if (nodeToInspect == node) {
                return nodeToInspect;
            }
            nodeToInspect = nodeToInspect.getNext();
        }
        return null;
    }
    
    /**
     * Time complexity: O(n)
     *
     * @return
     */
    public @NotNull DoublyLinkedListNode<T> findFirstOccurrence(final @NotNull T data) throws DataNotFoundException {
        final @Nullable DoublyLinkedListNode<T> searchResult = searchFirstOccurrence(data);
        if (searchResult == null) {
            throw new DataNotFoundException("The data: " + data + " could not be found.");
        }
        return searchResult;
    }
    
    /**
     * Time complexity: O(n)
     *
     * @return
     */
    public @NotNull DoublyLinkedListNode<T> findFirstOccurrence(
        final @NotNull DoublyLinkedListNode<T> node
    ) throws DataNotFoundException {
        final @Nullable DoublyLinkedListNode<T> searchResult = searchFirstOccurrence(node);
        if (searchResult == null) {
            throw new DataNotFoundException("The node: " + node + " could not be found.");
        }
        return searchResult;
    }
    
    /**
     * Time complexity: O(n)
     *
     * @return
     *
     * @throws DataNotFoundException
     */
    public @Nullable DoublyLinkedListNode<T> searchLastOccurrence(final @NotNull T data) {
        @Nullable DoublyLinkedListNode<T> nodeToInspect = last;
        while (nodeToInspect != null) {
            if (nodeToInspect.getData().equals(data)) {
                return nodeToInspect;
            }
            nodeToInspect = nodeToInspect.getPrevious();
        }
        return null;
    }
    
    /**
     * Time complexity: O(n)
     *
     * @return
     *
     * @throws DataNotFoundException
     */
    public @Nullable DoublyLinkedListNode<T> searchLastOccurrence(final @NotNull DoublyLinkedListNode<T> node) {
        @Nullable DoublyLinkedListNode<T> nodeToInspect = last;
        while (nodeToInspect != null) {
            if (nodeToInspect == node) {
                return nodeToInspect;
            }
            nodeToInspect = nodeToInspect.getPrevious();
        }
        return null;
    }
    
    /**
     * Time complexity: O(n)
     *
     * @return
     */
    public @NotNull DoublyLinkedListNode<T> findLastOccurrence(final @NotNull T data) throws DataNotFoundException {
        final @Nullable DoublyLinkedListNode<T> searchResult = searchLastOccurrence(data);
        if (searchResult == null) {
            throw new DataNotFoundException("The data: " + data + " could not be found.");
        }
        return searchResult;
    }
    
    /**
     * Time complexity: O(n)
     *
     * @return
     */
    public @NotNull DoublyLinkedListNode<T> findLastOccurrence(
        final @NotNull DoublyLinkedListNode<T> node
    ) throws DataNotFoundException {
        final @Nullable DoublyLinkedListNode<T> searchResult = searchLastOccurrence(node);
        if (searchResult == null) {
            throw new DataNotFoundException("The node: " + node + " could not be found.");
        }
        return searchResult;
    }
    
    public @NotNull DoublyLinkedListNode<T> find(final @NotNull T data) throws DataNotFoundException {
        return findFirstOccurrence(data);
    }
    
    public @Nullable DoublyLinkedListNode<T> search(final @NotNull T data) {
        return searchFirstOccurrence(data);
    }
    
    public boolean contains(final @NotNull T data) {
        return searchFirstOccurrence(data) != null;
    }
    
    public boolean contains(final @NotNull DoublyLinkedListNode<T> node) {
        return searchFirstOccurrence(node) != null;
    }
    
    /**
     * Time complexity: O(1)
     */
    public void add(final @NotNull T data) {
        append(data);
    }
    
    /**
     * Prepend the given data object to the start of this list.
     * Time complexity: O(1)
     *
     * @param data
     */
    public @NotNull DoublyLinkedListNode<T> prepend(final @NotNull T data) {
        return prependNode(createNode(data));
    }
    
    public @NotNull DoublyLinkedListNode<T> prependNode(final @NotNull DoublyLinkedListNode<T> node) {
        final @Nullable DoublyLinkedListNode<T> oldHead = head;
        head = node;
        // Assumption: "head" and "last" are always both null or not null!
        if (oldHead != null) {
            head.setNext(oldHead);
            oldHead.setPrevious(head);
        }
        // If we did not had a head element in the first place, our list was empty.
        // The new head element must therefore also be the last element.
        else {
            last = head;
        }
        return head;
    }
    
    /**
     * Appends the given data object to the end of this list.
     * Time complexity: O(1)
     *
     * @param data
     */
    public @NotNull DoublyLinkedListNode<T> append(final @NotNull T data) {
        return appendNode(createNode(data));
    }
    
    public @NotNull DoublyLinkedListNode<T> appendNode(final @NotNull DoublyLinkedListNode<T> node) {
        final @Nullable DoublyLinkedListNode<T> oldLast = last;
        last = node;
        // Assumption: "head" and "last" are always both null or not null!
        if (oldLast != null) {
            last.setPrevious(oldLast);
            oldLast.setNext(last);
        }
        // If we did not had a last element in the first place, our list was empty.
        // The new last element must therefore also be the head element.
        else {
            head = last;
        }
        size++;
        return last;
    }
    
    public void removeNode(final @NotNull DoublyLinkedListNode<T> node) {
        assert contains(node) : "Given node: " + node + " is not part of this list.";
        
        // 1. Node is head and last
        final @Nullable DoublyLinkedListNode<T> previous = node.getPrevious();
        final @Nullable DoublyLinkedListNode<T> next     = node.getNext();
        
        // Update previous node if present.
        if (previous != null) {
            previous.setNext(next);
        }
        // Node to remove must be the head node.
        else {
            head = next;
        }
        
        // Update next node if present.
        if (next != null) {
            next.setPrevious(previous);
        }
        // Node to remove must be the last node.
        else {
            last = previous;
        }
        
        node.setPrevious(null);
        node.setNext(null);
        size--;
    }
    
    public @Nullable DoublyLinkedListNode<T> remove(final @NotNull T data) {
        return removeFirstOccurrence(data);
    }
    
    public @Nullable DoublyLinkedListNode<T> removeFirstOccurrence(
        final @NotNull T data
    ) throws IllegalArgumentException  {
        final @Nullable DoublyLinkedListNode<T> nodeToRemove = searchFirstOccurrence(data);
        if (nodeToRemove == null) {
             throw new IllegalArgumentException("This list did not contain: " + data);
        } else {
            removeNode(nodeToRemove);
        }
        return nodeToRemove;
    }
    
    public @Nullable DoublyLinkedListNode<T> removeLastOccurrence(final @NotNull T data) {
        final @Nullable DoublyLinkedListNode<T> nodeToRemove = searchLastOccurrence(data);
        if (nodeToRemove == null) {
            throw new IllegalArgumentException("This list did not contain: " + data);
        } else {
            removeNode(nodeToRemove);
        }
        return nodeToRemove;
    }
    
    public @NotNull DoublyLinkedList<T> removeAllOccurrences(final @NotNull T data) {
        final @NotNull DoublyLinkedList<T>               removed  = new DoublyLinkedList<>();
        final @NotNull Iterator<DoublyLinkedListNode<T>> iterator = nodeIterator();
        @NotNull DoublyLinkedListNode<T>                 current;
        
        while (iterator.hasNext()) {
            current = iterator.next();
            if (current.getData().equals(data)) {
                removed.add(current.getData());
                iterator.remove();
            }
        }
        return removed;
    }
    
    /**
     * Removes the last element by calling {@link DoublyLinkedList#removeLast()}.
     * Time complexity: O(n)
     *
     * @return The removed {@link DoublyLinkedListNode}.
     */
    public @NotNull DoublyLinkedListNode<T> remove() throws NoSuchElementException {
        return removeLast();
    }
    
    public @NotNull DoublyLinkedListNode<T> removeHead() throws NoSuchElementException {
        // The head element can only be removed if it exists.
        if (head == null) {
            throw new NoSuchElementException(
                "This list is empty and therefore has no head element which could be removed!"
            );
        }
        
        // Set the head element to its next element. This might set "head" to null!
        final @NotNull DoublyLinkedListNode<T> oldHead = head;
        head = head.getNext();
        
        // The new head must not have a previous element!
        if (head != null) {
            head.setPrevious(null);
        }
        
        // Check if the removed head element was also the last element (list was of length 1..).
        // Then reset the "last" pointer.
        if (last == oldHead) {
            last = head;
        }
        
        size--;
        return oldHead;
    }
    
    public @NotNull DoublyLinkedListNode<T> removeLast() throws NoSuchElementException {
        // The last element can only be removed if it exists.
        if (last == null) {
            throw new NoSuchElementException(
                "This list is empty and therefore has no last element which could be removed!"
            );
        }
        
        // Set the last element to its previous element. This might set "last" to null!
        final @NotNull DoublyLinkedListNode<T> oldLast = last;
        last = last.getPrevious();
        
        // The new last element  must not have a next element!
        if (last != null) {
            last.setNext(null);
        }
        
        // Check if the removed last element was also the head element (list was of length 1..).
        // Then reset the "head" pointer.
        if (head == oldLast) {
            head = last;
        }
        
        size--;
        return oldLast;
    }
    
    public int getSize() {
        return size;
    }
    
    public boolean isEmpty() {
        assert (head == null & last == null);
        return size == 0;
    }
    
    /**
     * Drops the references to the head element and last element. The structure of this removed element chain stays
     * untouched and might be still in memory if used elsewhere.
     */
    public void empty() {
        head = null;
        last = null;
        size = 0;
    }
    
    /**
     * Performs a full clear of this list. Iterates over the list, detatching all data objects and breaking links.
     * Should be performed when pooling is used for the {@link DoublyLinkedListNode}'s!
     */
    public void clear() {
        @Nullable DoublyLinkedListNode<T> currentNode = head;
        @Nullable DoublyLinkedListNode<T> nodeToInspect;
        while (currentNode != null) {
            nodeToInspect = currentNode;
            currentNode = currentNode.getNext();
            
            // TODO: reset strategy
            nodeToInspect.reset();
        }
        head = null;
        last = null;
        size = 0;
    }
    
    public DoublyLinkedListNode<T> createNode(final @NotNull T data) {
        return new DoublyLinkedListNode<>(data);
    }
    
    public @Nullable DoublyLinkedListNode<T> getHead() {
        return head;
    }
    
    public @Nullable DoublyLinkedListNode<T> getLast() {
        return last;
    }
    
    @Override
    public @NotNull DoublyLinkedListIterator<T, DoublyLinkedListNode<T>> iterator() {
        return dataIterator();
    }
    
    public @NotNull DoublyLinkedListIterator<T, DoublyLinkedListNode<T>> dataIterator() {
        return dataIteratorHeadToLast();
    }
    
    public @NotNull DoublyLinkedListIterator<T, DoublyLinkedListNode<T>> dataIteratorHeadToLast() {
        return new DoublyLinkedListDataIterator<>(
            DoublyLinkedListNode::getNext, DoublyLinkedListNode::getPrevious, this::getHead, this::removeNode
        );
    }
    
    public @NotNull DoublyLinkedListIterator<T, DoublyLinkedListNode<T>> dataIteratorLastToHead() {
        return new DoublyLinkedListDataIterator<>(
            DoublyLinkedListNode::getPrevious, DoublyLinkedListNode::getNext, this::getLast, this::removeNode
        );
    }
    
    public @NotNull DoublyLinkedListIterator<DoublyLinkedListNode<T>, DoublyLinkedListNode<T>> nodeIterator() {
        return nodeIteratorHeadToLast();
    }
    
    public @NotNull DoublyLinkedListIterator<DoublyLinkedListNode<T>, DoublyLinkedListNode<T>> nodeIteratorHeadToLast() {
        return new DoublyLinkedListNodeIterator<>(
            DoublyLinkedListNode::getNext, DoublyLinkedListNode::getPrevious, this::getHead, this::removeNode
        );
    }
    
    public @NotNull DoublyLinkedListIterator<DoublyLinkedListNode<T>, DoublyLinkedListNode<T>> nodeIteratorLastToHead() {
        return new DoublyLinkedListNodeIterator<>(
            DoublyLinkedListNode::getPrevious, DoublyLinkedListNode::getNext, this::getLast, this::removeNode
        );
    }
    
    @Override
    public @NotNull String toString() {
        final @NotNull StringBuilder sb       = new StringBuilder();
        final @NotNull Iterator<T>   iterator = dataIterator();
        @NotNull T                   data;
        sb.append('[');
        while (iterator.hasNext()) {
            data = iterator.next();
            sb.append(data.toString());
            if (iterator.hasNext()) {
                sb.append(',');
            }
        }
        sb.append(']');
        return sb.toString();
    }
    
}
