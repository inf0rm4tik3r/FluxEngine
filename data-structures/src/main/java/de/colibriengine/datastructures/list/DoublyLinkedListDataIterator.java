package de.colibriengine.datastructures.list;

import de.colibriengine.lambda.NullaryProducerLambda;
import de.colibriengine.lambda.UnaryConsumerLambda;
import de.colibriengine.lambda.UnaryProducerLambda;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * DoublyLinkedListDataIterator
 *
 * @param <DataType>
 *     The type which indicating what will be returned by a call to {@link Iterator#next()}.
 * @param <NodeType>
 *     The type of the nodes.
 */
class DoublyLinkedListDataIterator<DataType, NodeType extends DoublyLinkedListNode<DataType>>
    implements DoublyLinkedListIterator<DataType, NodeType> {
    
    private @Nullable NodeType current;
    
    private @Nullable NodeType last;
    
    private final @NotNull UnaryProducerLambda<NodeType, NodeType> forwardFunction;
    
    private final @NotNull UnaryProducerLambda<NodeType, NodeType> backwardFunction;
    
    private final @NotNull NullaryProducerLambda<NodeType> initializationFunction;
    
    private final @NotNull UnaryConsumerLambda<NodeType> removeFunction;
    
    DoublyLinkedListDataIterator(
        final @NotNull UnaryProducerLambda<NodeType, NodeType> forwardFunction,
        final @NotNull UnaryProducerLambda<NodeType, NodeType> backwardFunction,
        final @NotNull NullaryProducerLambda<NodeType> initializationFunction,
        final @NotNull UnaryConsumerLambda<NodeType> removeFunction
    ) {
        current = initializationFunction.call();
        last = null;
        this.forwardFunction = forwardFunction;
        this.backwardFunction = backwardFunction;
        this.initializationFunction = initializationFunction;
        this.removeFunction = removeFunction;
    }
    
    @Override
    public @NotNull DoublyLinkedListDataIterator<DataType, NodeType> reinit() {
        current = initializationFunction.call();
        return this;
    }
    
    @Override
    public boolean hasNext() {
        return current != null;
    }
    
    @Override
    public @NotNull DataType next() throws NoSuchElementException {
        if (current == null) {
            throw new NoSuchElementException("Iterator has no more elements.");
        }
        last = current;
        current = forwardFunction.call(current);
        return last.getData();
    }
    
    @Override
    public boolean hasPrevious() {
        return current != null;
    }
    
    @Override
    public DataType previous() {
        if (current == null) {
            throw new NoSuchElementException("Iterator has no more elements.");
        }
        last = current;
        current = backwardFunction.call(current);
        return last.getData();
    }
    
    @Override
    public int nextIndex() {
        throw new UnsupportedOperationException("Unsupported");
    }
    
    @Override
    public int previousIndex() {
        throw new UnsupportedOperationException("Unsupported");
    }
    
    @Override
    public void remove() throws IllegalStateException {
        if (last == null) {
            throw new IllegalStateException("next() must be called before remove()!");
        }
        removeFunction.call(last);
        last = null;
    }
    
    @Override
    public void set(final DataType t) {
        throw new UnsupportedOperationException("Unsupported");
    }
    
    @Override
    public void add(final DataType t) {
        throw new UnsupportedOperationException("Unsupported");
    }
    
}
