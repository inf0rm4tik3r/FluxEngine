package de.colibriengine.datastructures.list;

import java.util.ListIterator;

import org.jetbrains.annotations.NotNull;

public interface DoublyLinkedListIterator<DataType, NodeType extends DoublyLinkedListNode<?>>
    extends ListIterator<DataType> {
    
    @NotNull DoublyLinkedListIterator<DataType, NodeType> reinit();
    
}
