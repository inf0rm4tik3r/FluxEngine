package de.colibriengine.datastructures.list;

public class DoublyLinkedListTest {
    
    public static void main(final String[] args) {
        final DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
    
        System.out.println("1: " + list.toString());
        
        list.add(3);
        list.add(6);
        list.add(4);
        list.add(9);
        list.add(5);
        list.add(0);
        list.add(-2);
        list.add(7);
        list.add(5);
        list.add(-1);
    
        System.out.println("2: " + list.toString());
        
        list.remove();
    
        System.out.println("3: " + list.toString());
    
        list.remove(4);
        final DoublyLinkedList<Integer> removed = list.removeAllOccurrences(5);
        System.out.println("removed: " + removed);
        System.out.println("4: " + list.toString());
    }
    
}
