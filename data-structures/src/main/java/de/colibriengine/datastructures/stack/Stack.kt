package de.colibriengine.datastructures.stack

/**
 * Defines basic operations that a "stack" implementation must provide.
 *
 * @since ColibriEngine 0.0.6.6
 */
interface Stack<T> {

    fun push(instance: T)
    fun pop(): T
    fun isEmpty(): Boolean
    fun containsEqual(instance: T): Boolean
    fun containsReference(instance: T): Boolean
    fun clear()
    fun size(): Int

}
