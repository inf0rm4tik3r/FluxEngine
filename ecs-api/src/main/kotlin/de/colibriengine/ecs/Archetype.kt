package de.colibriengine.ecs

import java.util.*

/** Computes a hash code for a sorted set of [ComponentTypeId]s (their unsigned integers). */
fun SortedSet<ComponentTypeId>.hash(): ULong = buildString {
    append(0)
    this@hash.forEach {
        append(it.id.toString(8))
        append("9")
    }
}.toULong(10)

/**
 * An archetype is an **immutable** set of [Component]s. More specifically, for performance reasons, a set of their
 * internal ids. Archetypes are created by the [ECS] system when needed. Every [Entity] has its archetype, which always
 * reflects the [Component]s the entity is made of. An entities archetype changes, whenever a component is added or
 * removed from it. [System]s have their own archetype. It is the set of components the system is working with. A system
 * will work on all entities, whose archetypes are a supertype of the systems archetype. See [isSuperTypeOf](other:
 * Archetype).
 */
class Archetype(unsortedComponents: Set<ComponentTypeId>) {

    val components: SortedSet<ComponentTypeId> = unsortedComponents.toSortedSet()
    private val hash = components.hash()
    private val intHash = hash.toInt()

    operator fun contains(componentTypeId: ComponentTypeId): Boolean {
        return componentTypeId in components
    }

    fun size(): Int = components.size

    fun isEmpty(): Boolean = size() == 0

    fun isNotEmpty(): Boolean = !isEmpty()

    /**
     * An archetype A* is a supertype of archetype A, if all Components of A are contained in A*.
     *
     * Let A, B and C be components. Then:
     *
     *      {A,B}   is a supertype of {{A}, {B}}
     *      {A,B,C} is a supertype of {{A}, {B} {C}, {A,B}, {A,C}, {B,C}}
     *      ...
     */
    fun isSupertypeOf(other: Archetype): Boolean {
        other.components.forEach {
            if (it !in components) {
                return false
            }
        }
        return true
    }

    fun isSubtypeOf(other: Archetype): Boolean {
        components.forEach {
            if (it !in other.components) {
                return false
            }
        }
        return true
    }

    /** Creates a new archetype which is based on this archetype with the given [componentToAdd] added to it. */
    fun withComponent(componentToAdd: ComponentTypeId): Archetype {
        val changeMap = ADDITIVE_CACHE.computeIfAbsent(hash) { mutableMapOf() }
        return changeMap.computeIfAbsent(componentToAdd.id) {
            val newSet: MutableSet<ComponentTypeId> = HashSet(components)
            val added = newSet.add(componentToAdd)
            check(added) { "Component to add was already present!" }
            Archetype(newSet)
        }
    }

    /** Creates a new archetype which is based on this archetype with the given [componentToRemove] removed from it. */
    fun withoutComponent(componentToRemove: ComponentTypeId): Archetype {
        val changeMap = SUBTRACTIVE_CACHE.computeIfAbsent(hash) { mutableMapOf() }
        return changeMap.computeIfAbsent(componentToRemove.id) {
            val newSet: MutableSet<ComponentTypeId> = HashSet(components)
            val removed = newSet.remove(componentToRemove)
            check(removed) { "Component to remove was not present!" }
            Archetype(newSet)
        }
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Archetype -> false
            other === this -> true
            else -> components == other.components
        }
    }

    override fun hashCode(): Int {
        return intHash
    }

    override fun toString(): String {
        return "Archetype(${components.map { it.id }.joinToString(separator = ",")})"
    }

    companion object {
        val EMPTY: Archetype = Archetype(emptySet())
        val ADDITIVE_CACHE: MutableMap<ULong, MutableMap<UInt, Archetype>> = mutableMapOf()
        val SUBTRACTIVE_CACHE: MutableMap<ULong, MutableMap<UInt, Archetype>> = mutableMapOf()
    }

}
