package de.colibriengine.ecs

// TODO: Do we want to use a 46bit identifier instead? (ULong)
@JvmInline
value class ComponentTypeId(val id: UInt) : Comparable<ComponentTypeId> {

    override fun compareTo(other: ComponentTypeId): Int {
        return id.compareTo(other.id)
    }

}
