package de.colibriengine.ecs

import de.colibriengine.util.Timer
import kotlin.reflect.KClass

interface ECS {

    fun <C : Component> registerComponentClass(componentClass: KClass<out C>)
    fun <C : Component> unregisterComponentClass(componentClass: KClass<out C>)

    /** Returns a new entity. Its initial [Archetype] is the empty archetype. */
    fun createEntity(): Entity

    fun destroyEntity(entity: Entity)
    fun addComponent(component: Component, to: Entity)
    fun removeComponent(component: Component, from: Entity)
    fun <T : Component> removeComponent(componentClass: KClass<out T>, from: Entity)
    fun <T : Component> getComponent(componentClass: KClass<out T>, from: Entity): T?

    fun <S : System1<C1>, C1 : Component> createSystem(
        sClass: KClass<out S>,
        c1Class: KClass<out C1>,
        instance: S
    ): S

    fun <S : System2<C1, C2>, C1 : Component, C2 : Component> createSystem(
        sClass: KClass<out S>,
        c1Class: KClass<out C1>,
        c2Class: KClass<out C2>,
        instance: S
    ): S

    fun <S : System3<C1, C2, C3>, C1 : Component, C2 : Component, C3 : Component> createSystem(
        sClass: KClass<out S>,
        c1Class: KClass<out C1>,
        c2Class: KClass<out C2>,
        c3Class: KClass<out C3>,
        instance: S
    ): S

    //fun <C1 : Component> getEntitiesWith(c1: KClass<out C1>)
    //fun <C1 : Component, C2 : Component> getEntitiesWith(c1: KClass<out C1>, c2: KClass<out C2>)

    fun <C1 : Component> process(
        system: System1<C1>,
        sClass: KClass<out System1<C1>>,
        c1Class: KClass<out C1>,
        timer: Timer.View
    )

    fun <C1 : Component, C2 : Component> process(
        system: System2<C1, C2>,
        sClass: KClass<out System2<C1, C2>>,
        c1Class: KClass<out C1>,
        c2Class: KClass<out C2>,
        timer: Timer.View
    )

}

inline fun <reified C : Component> ECS.registerComponent() = registerComponentClass(C::class)
inline fun <reified C : Component> ECS.unregisterComponent() = unregisterComponentClass(C::class)

inline fun <
    reified S : System1<C1>,
    reified C1 : Component
    > ECS.createSystem1(
    instanceFactory: () -> S
): S = createSystem(S::class, C1::class, instanceFactory.invoke())

inline fun <
    reified S : System2<C1, C2>,
    reified C1 : Component,
    reified C2 : Component
    > ECS.createSystem2(
    instanceFactory: () -> S
): S = createSystem(S::class, C1::class, C2::class, instanceFactory.invoke())

inline fun <
    reified S : System3<C1, C2, C3>,
    reified C1 : Component,
    reified C2 : Component,
    reified C3 : Component
    > ECS.createSystem3(
    instanceFactory: () -> S
): S = createSystem(S::class, C1::class, C2::class, C3::class, instanceFactory.invoke())

inline fun <reified S : System1<C1>, reified C1 : Component>
    ECS.process(system: System1<C1>, timer: Timer.View) = process(system, S::class, C1::class, timer)

inline fun <reified S : System2<C1, C2>, reified C1 : Component, reified C2 : Component>
    ECS.process(system: S, timer: Timer.View) = process(system, S::class, C1::class, C2::class, timer)
