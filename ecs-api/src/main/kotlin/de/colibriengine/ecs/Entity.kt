package de.colibriengine.ecs

import kotlin.reflect.KClass

interface Entity {

    /** The identifier for this entity. Must be unique over all entities that are alive at any given time. */
    val entityId: EntityId

    var archetype: Archetype

    /**
     * Adds the given [component] to this entity. Fails if a component of the type of [component] is already present.
     */
    fun add(component: Component)

    /** Removes the given [component] from this entity. Fails if that component is not attached to this entity. */
    fun remove(component: Component)

    /**
     * Removes the component with class [componentClass] from this entity. Fails if no component of this type is
     * attached to this entity.
     */
    fun <T : Component> remove(componentClass: KClass<out T>)

    /** Returns the component of type [T] if present in this entity. Otherwise returns null. */
    operator fun <T : Component> get(componentClass: KClass<out T>): T?

}

/** Removes the component of type [T]. Fails if no component of this type is attached to this entity. */
inline fun <reified T : Component> Entity.remove() = remove(T::class)

/** Returns the component of type [T] if present in this entity. Otherwise returns null. */
inline fun <reified T : Component> Entity.get(): T? = get(T::class)

/**
 * Use the component of type [T] by retrieving it and calling [block] with it. Fails if a component of that type could
 * not be found.
 */
inline fun <reified T : Component> Entity.useOrThrow(block: T.() -> Unit) = get(T::class)!!.apply(block)

/**
 * Use the component of type [T] by retrieving it and calling [block] with it. Does nothing (does not call [block]) if a
 * component of that type could not be found.
 */
inline fun <reified T : Component> Entity.useIfPresent(block: T.() -> Unit) = get(T::class)?.apply(block)
