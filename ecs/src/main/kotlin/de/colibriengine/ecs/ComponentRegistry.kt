package de.colibriengine.ecs

import de.colibriengine.util.IdGenerator
import kotlin.reflect.KClass

class ComponentRegistry {

    private val registry: MutableMap<KClass<out Component>, ComponentTypeId> = mutableMapOf()
    private val componentIds = IdGenerator()

    fun <C : Component> componentIdOf(cClass: KClass<out C>): ComponentTypeId =
        registry[cClass] ?: throw IllegalStateException("Component ${cClass.qualifiedName} is not registered!")

    fun <C : Component> componentIdOfOrNull(cClass: KClass<out C>): ComponentTypeId? =
        registry[cClass]

    fun <T : Component> registerComponentClass(componentClass: KClass<out T>) {
        require(componentClass !in registry) {
            "Component ${componentClass.simpleName} was already registered."
        }
        registry[componentClass] = ComponentTypeId(componentIds.generate())
    }

    fun <T : Component> unregisterComponentClass(componentClass: KClass<out T>) {
        val componentTypeId: ComponentTypeId? = registry.remove(componentClass)
        require(componentTypeId != null) {
            "Component ${componentClass.simpleName} was never registered."
        }
    }

    fun createArchetypeOf(c1: KClass<out Component>): Archetype {
        return Archetype(setOf(componentIdOf(c1)))
    }

    fun createArchetypeOf(c1: KClass<out Component>, c2: KClass<out Component>): Archetype {
        return Archetype(setOf(componentIdOf(c1), componentIdOf(c2)))
    }

    fun createArchetypeOf(
        c1: KClass<out Component>,
        c2: KClass<out Component>,
        c3: KClass<out Component>
    ): Archetype {
        return Archetype(
            setOf(
                componentIdOf(c1),
                componentIdOf(c2),
                componentIdOf(c3)
            )
        )
    }

}
