package de.colibriengine.ecs

import de.colibriengine.util.Timer
import de.colibriengine.util.copyAndAddItem
import de.colibriengine.util.copyAndRemoveItem
import kotlin.reflect.KClass

class ECSImpl : ECS {

    /** Maps component types to their ID. */
    private val componentRegistry = ComponentRegistry()

    /**
     * Contains all currently known archetypes and their relation to each other. The graph can be traversed to find all
     * archetypes which are supertypes of the archetype the traversal started on.
     */
    private val archetypeGraph = ArchetypeGraphWithNodeRelation()

    /**
     * Maps systems to their archetypes. Invariant: All archetypes, present in this map as values, must also be present
     * as nodes in [archetypeGraph]!
     */
    private val systemArchetypes = SystemArchetypes(archetypeGraph)

    /**
     * Maps archetypes to a collection of entities which have this archetype. Invariant: All archetypes, present in this
     * map as keys, must also be present as nodes in [archetypeGraph]!
     */
    private val entities = EntitiesByArchetype(archetypeGraph)

    private val entityFactory = EntityFactory(this, entities)

    override fun <C : Component> registerComponentClass(componentClass: KClass<out C>) =
        componentRegistry.registerComponentClass(componentClass)

    override fun <C : Component> unregisterComponentClass(componentClass: KClass<out C>) =
        componentRegistry.unregisterComponentClass(componentClass)

    override fun createEntity(): Entity = entityFactory.create()

    override fun destroyEntity(entity: Entity) = entityFactory.destroy(entity)

    override fun addComponent(component: Component, to: Entity) {
        // Remove entity from old archetype collection.
        val oldArchetype = to.archetype
        val entitiesOfOldArchetype = entities.of(oldArchetype)
        val entityComponents: Array<Component> = entitiesOfOldArchetype.remove(to.entityId)

        // Check if archetype collection is now empty and remove it if true.
        // Note: We do not want to delete the collection of entities with the empty archetype, as the existence of this
        // is an invariant.
        if (oldArchetype.isNotEmpty() && entitiesOfOldArchetype.isEmpty()) {
            entities.removeEntitiesForArchetype(oldArchetype)
        }

        // Compute new archetype.
        val componentTypeId = componentRegistry.componentIdOf(component::class)
        val newArchetype = to.archetype.withComponent(componentTypeId)
        to.archetype = newArchetype

        // Ensure collection for new archetype exists.
        entities.ensureEntityCollectionOfArchetypeExists(newArchetype)
        val entitiesOfNewArchetype = entities.of(newArchetype)

        // Create new component array with new component at correct index.
        val atIndex: Int = entitiesOfNewArchetype.indexOf(componentTypeId)
        entitiesOfNewArchetype.add(to.entityId, entityComponents.copyAndAddItem(component, atIndex))
    }

    override fun removeComponent(component: Component, from: Entity) {
        removeComponent(component::class, from)
    }

    override fun <T : Component> removeComponent(componentClass: KClass<out T>, from: Entity) {
        val prevEntities = entities.of(from.archetype)
        val componentTypeId = componentRegistry.componentIdOf(componentClass)
        val atIndex: Int = prevEntities.indexOf(componentTypeId)

        // Remove entity from old archetype collection.
        val components: Array<Component> = prevEntities.remove(from.entityId)

        // Compute new archetype.
        (from as EntityImpl).archetype = from.archetype.withoutComponent(componentTypeId)
        // Ensure collection for new archetype exists.
        entities.ensureEntityCollectionOfArchetypeExists(from.archetype)

        val newEntities = entities.of(from.archetype)

        // Create new component array without given component at old index.
        newEntities.add(from.entityId, components.copyAndRemoveItem(atIndex))
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Component> getComponent(componentClass: KClass<out T>, from: Entity): T? {
        val entities = entities.of(from.archetype)
        val componentTypeId = componentRegistry.componentIdOfOrNull(componentClass) ?: return null
        val index: Int = entities.indexOfOrNull(componentTypeId) ?: return null
        return entities[from.entityId][index] as T
    }

    override fun <S : System1<C1>, C1 : Component> createSystem(
        sClass: KClass<out S>,
        c1Class: KClass<out C1>,
        instance: S
    ): S {
        systemArchetypes.ensureSystemArchetypeIsRegistered(sClass) {
            componentRegistry.createArchetypeOf(c1Class)
        }
        return instance
    }

    override fun <S : System2<C1, C2>, C1 : Component, C2 : Component> createSystem(
        sClass: KClass<out S>,
        c1Class: KClass<out C1>,
        c2Class: KClass<out C2>,
        instance: S
    ): S {
        systemArchetypes.ensureSystemArchetypeIsRegistered(sClass) {
            componentRegistry.createArchetypeOf(c1Class, c2Class)
        }
        return instance
    }

    override fun <S : System3<C1, C2, C3>, C1 : Component, C2 : Component, C3 : Component> createSystem(
        sClass: KClass<out S>,
        c1Class: KClass<out C1>,
        c2Class: KClass<out C2>,
        c3Class: KClass<out C3>,
        instance: S
    ): S {
        systemArchetypes.ensureSystemArchetypeIsRegistered(sClass) {
            componentRegistry.createArchetypeOf(c1Class, c2Class, c3Class)
        }
        return instance
    }

    override fun <C1 : Component> process(
        system: System1<C1>,
        sClass: KClass<out System1<C1>>,
        c1Class: KClass<out C1>,
        timer: Timer.View
    ) {
        val c1Id = componentRegistry.componentIdOf(c1Class)
        val systemArchetype = systemArchetypes.of(sClass)
        system.beforeAll()
        entities.forAllSupertypesOf(systemArchetype) {
            val idxC1 = it.indexOf(c1Id)
            it.forEach { components ->
                system.processEntity(components[idxC1] as C1, timer)
            }
        }
    }

    override fun <C1 : Component, C2 : Component> process(
        system: System2<C1, C2>,
        sClass: KClass<out System2<C1, C2>>,
        c1Class: KClass<out C1>,
        c2Class: KClass<out C2>,
        timer: Timer.View
    ) {
        val c1Id = componentRegistry.componentIdOf(c1Class)
        val c2Id = componentRegistry.componentIdOf(c2Class)
        val systemArchetype = systemArchetypes.of(sClass)
        system.beforeAll()
        entities.forAllSupertypesOf(systemArchetype) {
            val idxC1 = it.indexOf(c1Id)
            val idxC2 = it.indexOf(c2Id)
            it.forEach { components ->
                system.processEntity(components[idxC1] as C1, components[idxC2] as C2, timer)
            }
        }
    }

}
