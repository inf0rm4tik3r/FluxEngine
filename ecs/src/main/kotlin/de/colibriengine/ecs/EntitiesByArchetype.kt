package de.colibriengine.ecs

class EntitiesByArchetype(val supertypes: ArchetypeGraphWithNodeRelation) {

    val entitiesByArchetype: MutableMap<Archetype, EntitiesOfArchetype> = mutableMapOf()

    init {
        // Invariant: Archetype.EMPTY is always an existing key in the map!
        entitiesByArchetype[Archetype.EMPTY] = EntitiesOfArchetype(Archetype.EMPTY)
    }

    fun of(archetype: Archetype): EntitiesOfArchetype = entitiesByArchetype[archetype]!!

    fun ensureEntityCollectionOfArchetypeExists(archetype: Archetype) {
        if (archetype !in entitiesByArchetype) {
            entitiesByArchetype[archetype] = EntitiesOfArchetype(archetype)
            supertypes.ensureArchetypeExists(archetype)
        }
    }

    fun removeEntitiesForArchetype(archetype: Archetype) {
        val removed = entitiesByArchetype.remove(archetype)
        require(removed != null) {
            "No EntitiesOfArchetype were present for archetype $archetype"
        }
        supertypes.removeArchetype(archetype)
    }

    inline fun forAllSupertypesOf(
        archetype: Archetype,
        crossinline action: (EntitiesOfArchetype) -> Unit
    ) {
        supertypes.traverseSupertypes(of = archetype) {
            action.invoke(entitiesByArchetype[it]!!)
        }
        /*
        // Old and unoptimized but here for reference. Can be used if the graph approach is malfunctioning.
        for ((someArchetype, entitiesOfArchetype) in entitiesByArchetype) {
            if (someArchetype.isSupertypeOf(archetype)) {
                action.invoke(entitiesOfArchetype)
            }
        }
        */
    }

}
