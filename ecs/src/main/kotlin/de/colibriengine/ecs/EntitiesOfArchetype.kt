package de.colibriengine.ecs

import de.colibriengine.util.removeByReplacingWithLast

class EntitiesOfArchetype(private val archetype: Archetype) {

    val entities: MutableList<Array<Component>> = mutableListOf()
    private val entityIdToIndex: MutableMap<EntityId, Int> = mutableMapOf()
    private val componentTypeIdToIndex: Map<ComponentTypeId, Int>

    init {
        val map = mutableMapOf<ComponentTypeId, Int>()
        archetype.components.forEachIndexed { index, componentTypeId -> map[componentTypeId] = index }
        componentTypeIdToIndex = map
    }

    inline fun forEach(action: (Array<Component>) -> Unit) {
        entities.forEach(action)
    }

    fun indexOf(componentTypeId: ComponentTypeId): Int = componentTypeIdToIndex[componentTypeId]
        ?: throw IllegalArgumentException(
            "Component type $componentTypeId is not known. " +
                "Only knowing: $componentTypeIdToIndex. " +
                "Archetype is: $archetype."
        )

    fun indexOfOrNull(componentTypeId: ComponentTypeId): Int? = componentTypeIdToIndex[componentTypeId]

    fun add(entityId: EntityId, components: Array<Component>) {
        require(entityId !in entityIdToIndex) { "Entity $entityId is already present." }
        require(components.size == archetype.size()) { "Component array does not match archetype size." }
        entities.add(components) // adds to the end!
        entityIdToIndex[entityId] = entities.lastIndex
    }

    fun remove(entityId: EntityId): Array<Component> {
        require(entityId in entityIdToIndex) { "Entity $entityId is not present." }
        val index = entityIdToIndex.remove(entityId)!!
        return entities.removeByReplacingWithLast(index)
    }

    operator fun get(index: Int): Array<Component> {
        return entities[index]
    }

    operator fun get(entityId: EntityId): Array<Component> {
        return entities[entityIdToIndex[entityId]!!]
    }

    fun isEmpty(): Boolean {
        return entities.isEmpty()
    }

}
