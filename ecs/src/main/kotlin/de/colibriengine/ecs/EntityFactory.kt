package de.colibriengine.ecs

import de.colibriengine.util.IdGenerator

class EntityFactory(
    private val ecs: ECS,
    private val entities: EntitiesByArchetype
) {

    private val entityIds = IdGenerator()

    fun create(): Entity {
        val entity = EntityImpl(EntityId(entityIds.generate()), ecs)
        entities.of(entity.archetype).add(entity.entityId, arrayOf())
        return entity
    }

    fun destroy(entity: Entity) {
        val entities = entities.of(entity.archetype)
        entities.remove(entity.entityId)
        entityIds.release(entity.entityId.id)
    }

}
