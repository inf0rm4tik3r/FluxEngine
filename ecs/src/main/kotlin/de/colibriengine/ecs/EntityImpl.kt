package de.colibriengine.ecs

import kotlin.reflect.KClass

open class EntityImpl(
    override val entityId: EntityId,
    private val ecs: ECS
) : Entity {

    override var archetype: Archetype = Archetype.EMPTY

    override fun add(component: Component) {
        ecs.addComponent(component, this)
    }

    override fun remove(component: Component) {
        ecs.removeComponent(component, this)
    }

    override fun <T : Component> remove(componentClass: KClass<out T>) {
        ecs.removeComponent(componentClass, this)
    }

    override operator fun <T : Component> get(componentClass: KClass<out T>): T? {
        return ecs.getComponent(componentClass, this)
    }

}
