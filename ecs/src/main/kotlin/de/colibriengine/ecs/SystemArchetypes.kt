package de.colibriengine.ecs

import kotlin.reflect.KClass

class SystemArchetypes(val supertypes: ArchetypeGraphWithNodeRelation) {

    val archetypes: MutableMap<KClass<out System>, Archetype> = mutableMapOf()

    fun of(systemClass: KClass<out System>) = archetypes[systemClass]!!

    inline fun <S : System> ensureSystemArchetypeIsRegistered(
        systemClass: KClass<out S>,
        archetypeFactory: () -> Archetype
    ) {
        if (systemClass !in archetypes) {
            val archetype = archetypeFactory.invoke()
            archetypes[systemClass] = archetype
            supertypes.ensureArchetypeExists(archetype)
        }
    }

    fun <S : System> unregisterSystemArchetype(systemClass: KClass<out S>) {
        val archetype: Archetype = archetypes.remove(systemClass)
            ?: throw IllegalArgumentException("Unknown system $systemClass")
        supertypes.removeArchetype(archetype)
    }

}
