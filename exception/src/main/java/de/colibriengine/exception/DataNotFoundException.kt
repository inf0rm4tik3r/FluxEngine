package de.colibriengine.exception

/**
 * This exception indicates that some data could not be found.
 * Might get thrown when a search() operation does not find what its looking for.
 *
 * @time 02.08.2018 16:34
 */
class DataNotFoundException : RuntimeException {

    constructor(message: String = DEFAULT_MESSAGE)

    constructor(message: String = DEFAULT_MESSAGE, throwable: Throwable? = null)

    constructor(throwable: Throwable) : this(DEFAULT_MESSAGE, throwable)

    companion object {
        private const val DEFAULT_MESSAGE = "Data could not be found."
    }

}
