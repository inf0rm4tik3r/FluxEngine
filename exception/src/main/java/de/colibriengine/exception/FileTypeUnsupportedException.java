package de.colibriengine.exception;

/**
 * FileTypeUnsupportedException
 *
 * @version 0.0.0.2
 * @since ColibriEngine 0.0.6.0
 */

public class FileTypeUnsupportedException extends RuntimeException {

    public FileTypeUnsupportedException() {
        super();
    }

    public FileTypeUnsupportedException(final String message) {
        super(message);
    }

    public FileTypeUnsupportedException(final Throwable cause) {
        super(cause);
    }

    public FileTypeUnsupportedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
