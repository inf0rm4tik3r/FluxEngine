package de.colibriengine.exception;

import org.jetbrains.annotations.NotNull;

/**
 * This exception should be thrown if a class does not allow instantiation.
 * Provides a meaningful error message containing the given class name.
 *
 * @version 0.0.0.1
 * @time 23.09.2018 16:03
 * @since ColibriEngine 0.0.7.1
 */
public class InstantiationNotAllowedException extends RuntimeException {
    
    private static final String ERROR_MSG = "Instantiation of \"%s\" is not allowed!";
    
    public InstantiationNotAllowedException(final @NotNull String className) {
        super(String.format(ERROR_MSG, className));
    }
    
    public InstantiationNotAllowedException(final @NotNull String className, final @NotNull Throwable throwable) {
        super(String.format(ERROR_MSG, className), throwable);
    }
    
}
