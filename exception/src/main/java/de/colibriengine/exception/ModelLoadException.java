package de.colibriengine.exception;

import org.jetbrains.annotations.NotNull;

public class ModelLoadException extends RuntimeException {

    public ModelLoadException(final @NotNull String message) {
        super(message);
    }

    public ModelLoadException(final @NotNull Throwable throwable) {
        super(throwable);
    }

    public ModelLoadException(final @NotNull String message, final @NotNull Throwable throwable) {
        super(message, throwable);
    }
    
}
