package de.colibriengine.exception;

/**
 *
 */
public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(final String message) {
        super(message, null);
    }

    public ResourceNotFoundException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

}
