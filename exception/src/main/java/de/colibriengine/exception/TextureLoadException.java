package de.colibriengine.exception;

import org.jetbrains.annotations.NotNull;

/**
 * TextureLoadException
 *
 * @version 0.0.0.1
 * @time 21.03.2018 00:11
 */
public class TextureLoadException extends RuntimeException {

    public TextureLoadException(final @NotNull String message) {
        super(message, null);
    }

    public TextureLoadException(final @NotNull String message, final Throwable throwable) {
        super(message, throwable);
    }

}
