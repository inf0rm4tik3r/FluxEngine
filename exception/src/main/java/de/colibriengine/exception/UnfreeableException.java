package de.colibriengine.exception;

import org.jetbrains.annotations.NotNull;

/**
 * Thrown if a free() operations was called on an object which must not be freed.
 *
 * @version 0.0.0.1
 * @time 05.10.2018 01:22
 * @since ColibriEngine 0.0.7.2
 */
public class UnfreeableException extends UnsupportedOperationException {
    
    /**
     * Default error message being displayed.
     */
    private static final @NotNull String ERROR_MSG = "This object must not be freed!";
    
    public UnfreeableException() {
        super(ERROR_MSG);
    }
    
}
