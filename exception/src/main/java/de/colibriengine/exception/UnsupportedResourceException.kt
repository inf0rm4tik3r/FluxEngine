package de.colibriengine.exception

/**
 *
 */
class UnsupportedResourceException : RuntimeException {

    @JvmOverloads
    constructor(message: String, throwable: Throwable? = null)

    constructor(throwable: Throwable) : this("This resource is not supported.", throwable) {}

}
