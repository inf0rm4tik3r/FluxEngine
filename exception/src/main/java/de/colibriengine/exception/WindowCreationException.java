package de.colibriengine.exception;

import org.jetbrains.annotations.NotNull;

/**
 * Exception which is thrown when a window can not be created.
 */
public class WindowCreationException extends RuntimeException {
    
    private static final @NotNull String STANDARD_ERROR_MSG = "The window could not be created.";
    
    public WindowCreationException() {
        super(STANDARD_ERROR_MSG);
    }
    
    public WindowCreationException(final @NotNull String errorMsg) {
        super(errorMsg);
    }
    
    public WindowCreationException(final @NotNull Throwable causedBy) {
        super(STANDARD_ERROR_MSG, causedBy);
    }
    
    public WindowCreationException(final @NotNull String errorMsg, final @NotNull Throwable causedBy) {
        super(errorMsg, causedBy);
    }
    
}
