package de.colibriengine.graphics;

import de.colibriengine.logging.LogUtil;
import org.apache.logging.log4j.Logger;

public interface Bindable {

    /**
     * This error message will be thrown in an exceptions if the {@link Bindable#ensureNotBound()} method finds out that
     * this object is currently bound.
     */
    String MSG_IS_BOUND = "This \"%s\" is bound to the context, but must not be!";

    /**
     * This error message will be thrown in an exceptions if the {@link Bindable#ensureBound()} method finds out that
     * this object is not currently bound.
     */
    String MSG_IS_NOT_BOUND = "This \"%s\" is not bound to the context, but must be!";

    Logger LOG = LogUtil.getLogger(Bindable.class);

    /**
     * Binds this object. Further calls to {@link Bindable#isBound()} should return true!
     */
    void bind();

    /**
     * @return Whether or not this object currently bound.
     */
    boolean isBound();

    /**
     * Unbinds this object.
     *
     * @throws IllegalStateException If this object is not currently bound. Possibly thrown by a call to {@link Bindable#ensureBound()}
     */
    void unbind() throws IllegalStateException;

    /**
     * Unbinds this object if it is currently bound. Should do nothing if {@link Bindable#isBound()} returns false.
     */
    void unbindIfBound();

    /**
     * Ensures that this object is currently bound.
     *
     * @throws IllegalStateException If this object is not currently bound. Determined by a call to {@link Bindable#isBound()}
     */
    default void ensureBound() throws IllegalStateException {
        if (!isBound()) {
            throw new IllegalStateException(String.format(MSG_IS_NOT_BOUND, getClass().getSuperclass().getName()));
        }
    }

    /**
     * Ensures that this object is currently not bound.
     *
     * @throws IllegalStateException If this object is currently bound. Determined by a call to {@link Bindable#isBound()}
     */
    default void ensureNotBound() throws IllegalStateException {
        if (isBound()) {
            throw new IllegalStateException(String.format(MSG_IS_BOUND, getClass().getSuperclass().getName()));
        }
    }

}
