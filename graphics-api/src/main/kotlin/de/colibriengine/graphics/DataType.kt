package de.colibriengine.graphics

enum class DataType {

    BYTE,
    TWO_BYTES,
    THREE_BYTES,
    FOUR_BYTES,
    UNSIGNED_BYTE,

    SHORT,
    UNSIGNED_SHORT,

    INT,
    UNSIGNED_INT,

    FLOAT,
    DOUBLE;

}
