package de.colibriengine.graphics

import de.colibriengine.graphics.texture.Texture

interface GBuffer {

    val albedoTexture: Texture
    val normalTexture: Texture
    val specularTexture: Texture
    val pp1Texture: Texture
    val pp2Texture: Texture

    val depthTexture: Texture

}
