package de.colibriengine.graphics.camera

import de.colibriengine.graphics.window.Window
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.quaternion.quaternionF.QuaternionF
import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.vector.vec2i.Vec2i
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

interface Camera {

    var parentWindow: Window?

    val active: Boolean

    val position: Vec3fAccessor
    val rotation: QuaternionFAccessor
    val lookRotation: QuaternionFAccessor

    val forward: Vec3fAccessor
    val backward: Vec3fAccessor
    val left: Vec3fAccessor
    val right: Vec3fAccessor
    val up: Vec3fAccessor
    val down: Vec3fAccessor

    /** Field of view. */
    var fov: Float

    /** Distance to the near plane. */
    var zNear: Float

    /** Distance to the far plane. */
    var zFar: Float

    val viewMatrix: Mat4fAccessor

    val lastProjectionResolutionUsed: Vec2i

    val orthographicProjectionMatrix: Mat4fAccessor
    val inverseOrthographicProjectionMatrix: Mat4fAccessor
    val perspectiveProjectionMatrix: Mat4fAccessor
    val inversePerspectiveProjectionMatrix: Mat4fAccessor

    /** The combined view and orthographic projection matrices. */
    val orthographicViewProjectionMatrix: Mat4fAccessor

    /** The inverse of the combined view and orthographic projection matrices. */
    val inverseOrthographicViewProjectionMatrix: Mat4fAccessor

    /** The combined view and perspective projection matrices. */
    val perspectiveViewProjectionMatrix: Mat4fAccessor

    /** The inverse of the combined view and perspective projection matrices. */
    val inversePerspectiveViewProjectionMatrix: Mat4fAccessor

    /** Makes this camera the "active" camera in its parent window if a parent window is set. */
    fun use()

    /** Moves the camera [amount] in the specified [direction]. */
    fun move(direction: Vec3fAccessor, amount: Float)
    fun moveForward(amount: Float)
    fun moveBackward(amount: Float)
    fun moveLeft(amount: Float)
    fun moveRight(amount: Float)
    fun moveUp(amount: Float)
    fun moveDown(amount: Float)

    /**
     * Rotates the camera the specified amount around the specified axis.
     *
     * @param axis  The axis to rotate around.
     * @param angle The angle to rotate.
     */
    fun rotate(axis: Vec3fAccessor, angle: Float)

    fun initProjectionMatrices(width: Int, height: Int)
    fun initProjectionMatrices(resolution: Vec2iAccessor)

    fun initOrthographicProjectionMatrix(
        left: Float, right: Float, bottom: Float, top: Float,
        zNear: Float, zFar: Float
    )

    fun initPerspectiveProjectionMatrix(
        fov: Float, aspectRatio: Float,
        zNear: Float, zFar: Float
    )

    fun calculateViewMatrix()
    fun calculateViewProjectionMatrices()

    /**
     * Returns a quaternion which represents a rotation for an object which (if applied) lets the object face this
     * camera.
     *
     * @param sourcePosition The position of the object which wants to look at this camera.
     * @param storeIn The quaternion instance in which the rotation will be stored.
     * @return The given [storeIn] quaternion.
     */
    fun getCameraFacingRotation(sourcePosition: Vec3fAccessor, storeIn: QuaternionF): QuaternionF

    fun reset()

}
