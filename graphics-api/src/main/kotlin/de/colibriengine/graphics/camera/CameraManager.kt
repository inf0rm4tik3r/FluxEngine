package de.colibriengine.graphics.camera

/**
 * Manages collection of [Camera]'s, of which one can be the "active" camera. An object of this class is associated to
 * every [Window] and can be reached by calling [Window.cameraManager].
 */
interface CameraManager {

    /** The active camera or null if no camera is active. */
    var activeCamera: Camera?

    /** @return Whether or not there is an active camera. */
    fun hasActiveCamera(): Boolean

    /**
     * Tells whether the specified camera is part of this camera manager.
     *
     * @param camera The camera which to checked.
     * @return True if the camera is part of this camera manager.
     */
    operator fun contains(camera: Camera): Boolean

    /**
     * TODO: Investigate: We have [AbstractCamera.addToWindow]. Shouldn't this method be preferred?
     */
    fun add(camera: Camera, setActive: Boolean = false)

    /**
     * Removes the specified camera from this camera manager. The camera must be part of this manager. The state of the
     * camera will not be modified. NOTE: If the camera which is to be removed is the active camera, no camera will be
     * active after this call returns.
     *
     * @param camera The camera to remove.
     * @return true if this call removed the active window, false otherwise.
     * @throws IllegalArgumentException If the specified camera is not part of this camera manager.
     */
    fun remove(camera: Camera): Boolean

}
