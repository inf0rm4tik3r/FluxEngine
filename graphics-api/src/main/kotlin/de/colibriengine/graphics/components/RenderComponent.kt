package de.colibriengine.graphics.components

import de.colibriengine.ecs.Component
import de.colibriengine.graphics.model.Model
import de.colibriengine.graphics.rendering.BlendFactor

class InternalRenderComponent(model: Model) : RenderComponent(model)

open class RenderComponent(var model: Model) : Component {
    var active: Boolean = true
    var cullBackFaces: Boolean = true
    var transparency: Transparency = Transparency()
    var blending: Blending = Blending()
    var shadowCaster: Boolean = true
}

class Transparency {
    var isTransparent: Boolean = false
    var alphaThreshold: Float = 0.01f
}

class Blending {
    var active: Boolean = false
    var blendFuncSourceFactor: BlendFactor = BlendFactor.ONE
    var blendFuncDestinationFactor: BlendFactor = BlendFactor.ONE
}
