package de.colibriengine.graphics.fbo

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.shader.Shader
import de.colibriengine.math.vector.vec2f.StdVec2f
import de.colibriengine.math.vector.vec2f.StdVec2fFactory
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.util.IntList

interface FBOTextureRenderer<T> {

    val ecs: ECS
    val meshFactory: MeshFactory

    /* SRC */
    val srcFBO: FBO?
    val srcInputTextureIndices: IntList
    val srcSamplePos: Vec2f
    val srcSampleDimension: Vec2f

    /* DEST */
    val destFBO: FBO?
    val destOutputTextureIndices: IntList
    val destWritePos: Vec2f
    val destWriteDimension: Vec2f

    /* SHADER */
    var shader: Shader
    var shaderSetup: (T) -> Unit

    companion object {
        val DEFAULT_SRC_SAMPLE_POS: Vec2fAccessor = StdVec2fFactory.ZERO
        val DEFAULT_SRC_SAMPLE_DIMENSION: Vec2fAccessor = StdVec2fFactory.ONE
        val DEFAULT_DEST_WRITE_POS: Vec2fAccessor = StdVec2fFactory.ZERO
        val DEFAULT_DEST_WRITE_DIMENSION: Vec2fAccessor = StdVec2f().set(512f, 512f)
    }

}
