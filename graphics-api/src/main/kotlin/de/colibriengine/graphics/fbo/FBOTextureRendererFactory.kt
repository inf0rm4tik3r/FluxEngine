package de.colibriengine.graphics.fbo

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.shader.Shader
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.util.IntList

interface FBOTextureRendererFactory<T : FBOTextureRenderer<T>> {

    fun create(
        ecs: ECS,
        meshFactory: MeshFactory,
        srcFBO: FBO?,
        srcInputTextureIndices: IntList,
        srcSamplePos: Vec2f,
        srcSampleDimension: Vec2f,
        destFBO: FBO?,
        destOutputTextureIndices: IntList,
        destWritePos: Vec2f,
        destWriteDimension: Vec2f,
        shader: Shader,
        shaderSetup: (T) -> Unit
    ): T

}
