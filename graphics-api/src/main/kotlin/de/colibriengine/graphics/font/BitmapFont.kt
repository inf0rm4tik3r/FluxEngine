package de.colibriengine.graphics.font

import de.colibriengine.asset.font.BitmapFontDescriptor
import de.colibriengine.graphics.texture.Texture
import de.colibriengine.graphics.texture.TextureManager

class BitmapFont(val descriptor: BitmapFontDescriptor) {

    val textures: MutableMap<Int, Texture> = mutableMapOf()

    /**
     * Called by the engine when necessary. Given [textureManager] must be the one with which the texture of this font
     * were loaded.
     */
    fun destroy(textureManager: TextureManager) {
        for (texture in textures.values) {
            textureManager.giveBack(texture)
        }
        textures.clear()
    }

}
