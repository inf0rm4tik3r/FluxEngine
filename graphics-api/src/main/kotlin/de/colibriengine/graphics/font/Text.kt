package de.colibriengine.graphics.font

import de.colibriengine.graphics.model.Model
import de.colibriengine.math.vector.vec4f.Vec4f
import de.colibriengine.util.MutableString

interface Text {

    /** If the mode is FIXED, the size cannot be altered and updates to the text are more efficient. */
    val mode: TextMode

    /** The current size of the text. Represents the amount of characters that can be stored. */
    val size: UInt

    /** The font which is used to display the text. */
    var font: BitmapFont

    /** The color of the text. Initially set to white and being completely opaque (a=1.0). */
    val color: Vec4f

    /** The model storing the text characters. Recreated whenever this text is updated. */
    val model: Model

    /** Updates the content to [text] and returns itself afterwards. Also see update(MutableString.() -> Unit)! */
    fun update(text: String): Text

    /** Calls the [updater] to update the content. Preferable if many small alterations should be made! */
    fun update(updater: MutableString.() -> Unit): Text

}
