package de.colibriengine.graphics.font

enum class TextMode {

    FIXED,
    DYNAMIC

}
