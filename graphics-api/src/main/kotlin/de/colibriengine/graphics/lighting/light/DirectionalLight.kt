package de.colibriengine.graphics.lighting.light

import de.colibriengine.math.quaternion.quaternionF.QuaternionF

interface DirectionalLight : LightSource {

    val orientation: QuaternionF

}
