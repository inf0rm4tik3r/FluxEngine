package de.colibriengine.graphics.lighting.light

interface LightFactory {

    fun point(): PointLight
    fun spot(): SpotLight
    fun directional(): DirectionalLight

}
