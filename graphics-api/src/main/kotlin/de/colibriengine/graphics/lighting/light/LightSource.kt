package de.colibriengine.graphics.lighting.light

import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.scene.graph.SceneGraphNode

// A light is always part of the scenegraph, as its final position should depend on the position of parent entities.
interface LightSource : SceneGraphNode {

    val position: Vec3f
    val color: Vec3f
    var intensity: Float
    var castsShadows: Boolean

}
