package de.colibriengine.graphics.lighting.light

import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.scene.graph.SceneGraphNode

interface PointLight : LightSource {

    /**
     *      x := quadratic term
     *      y := linear term
     *      z := constant term
     */
    val attenuation: Vec3f

    var dropOffFactor: Float

}
