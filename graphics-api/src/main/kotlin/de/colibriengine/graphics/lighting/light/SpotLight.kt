package de.colibriengine.graphics.lighting.light

import de.colibriengine.math.quaternion.quaternionF.QuaternionF

interface SpotLight : LightSource {

    val orientation: QuaternionF

}
