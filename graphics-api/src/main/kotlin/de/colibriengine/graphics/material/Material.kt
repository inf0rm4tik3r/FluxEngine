package de.colibriengine.graphics.material

import de.colibriengine.graphics.texture.Texture
import de.colibriengine.graphics.texture.TextureManager

interface Material {

    val materialDefinition: MaterialDefinition

    val ambientTextures: MutableList<Texture>
    val diffuseTextures: MutableList<Texture>
    val specularTextures: MutableList<Texture>

    fun hasAmbientTextures(): Boolean = ambientTextures.isNotEmpty()
    fun hasDiffuseTexture(): Boolean = diffuseTextures.isNotEmpty()
    fun hasSpecularTexture(): Boolean = specularTextures.isNotEmpty()

    /**
     * Loads all textures which are referenced (as paths) in the [MaterialDefinition] which created this material
     * instance. Texture are loaded using the given [TextureManager].
     *
     * @param textureBasePath A base path under which all the textures referenced in this material will be located. For
     *     example: If our material specifies a `"wall.png"` texture path, we might call this
     *     method with `"textures/"` as the base path under which the wall texture can be found.
     *     This path must not lie under the executable path. This path may be specified empty.
     */
    fun loadTextures(textureManager: TextureManager, textureBasePath: String)

    fun free()

}
