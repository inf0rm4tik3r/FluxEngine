package de.colibriengine.graphics.material

interface MaterialFactory {

    fun createFrom(materialDefinition: MaterialDefinition): Material

}
