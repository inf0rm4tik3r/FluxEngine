package de.colibriengine.graphics.material

import java.net.URL

open class MaterialLibraryDefinition(val constructedFrom: URL?) {

    val materialDefinitions: ArrayList<MaterialDefinition> = ArrayList(8)

    fun addMaterialDefinition(materialDefinition: MaterialDefinition) {
        materialDefinitions.add(materialDefinition)
    }

}
