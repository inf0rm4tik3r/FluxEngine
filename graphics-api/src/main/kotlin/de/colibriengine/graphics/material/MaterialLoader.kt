package de.colibriengine.graphics.material

import java.net.URL

interface MaterialLoader {

    fun load(
        resourceURL: URL,
        relativePath: String
    ): MaterialLibraryDefinition

}
