package de.colibriengine.graphics.material

import de.colibriengine.math.vector.vec2f.StdVec2f
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.ZERO
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.util.StringUtil

open class MaterialTextureDefinition {

    /** Path to the referenced texture. */
    var texturePath: String = TEXTURE_PATH_DEFAULT
        set(value) {
            field = StringUtil.toForwardSlashes(value)
        }

    /** Specifies if texture blending in the horizontal direction should be turned on. Default: true. */
    var isBlendHorizontal: Boolean = BLEND_HORIZONTAL_DEFAULT

    /** Specifies if texture blending in the vertical direction should be turned on. Default: true. */
    var isBlendVertical: Boolean = BLEND_VERTICAL_DEFAULT

    /**
     * Specifies a bump multiplier. Values stored within the texture are multiplied by this value before they are
     * applied to the surface. It can be positive or negative. Extreme bump multipliers may cause odd visual results
     * because only the surface normal is perturbed and the surface position does not change. For best results, use
     * values between 0 and 1. Default: 0.
     */
    var bumpMultiplier: Float = BUMP_MULTIPLIER_DEFAULT

    /**
     * The boost option increases the sharpness, or clarity, of mip-mapped texture files. Use any non-negative floating
     * point value to represent the degree of increased clarity; the greater the value, the greater the clarity.
     * Default: 0.
     */
    var boost: Float = BOOST_DEFAULT
        set(value) {
            require(value >= 0.0f) { "Boost value can not be negative." }
            field = value
        }

    /** Turns on colour correction for textures. */
    var isColorCorrection: Boolean = COLOR_CORRECTION_DEFAULT

    /**
     * Specifies if the texture is clamped. When clamping is on, textures are restricted to 0-1 in the uvw range.
     * Default: false.
     */
    var isClamp: Boolean = CLAMP_DEFAULT

    /**
     * Modifies the range over which scalar or colour texture values may vary. `base` (x) adds a base value to the
     * texture values. A positive value makes everything brighter; a negative value makes everything dimmer. The default
     * is 0; the range is unlimited. `gain` (y) expands the range of the texture values. Increasing the number increases
     * the contrast. The default is 1; the range is unlimited.
     */
    val textureValueRange: Vec2f = StdVec2f().set(BASE_DEFAULT, GAIN_DEFAULT)

    /** Offsets the position of the texture map on the surface by shifting the position of the map origin. */
    val textureMapOffset: Vec3f = StdVec3f().set(TEXTURE_MAP_OFFSET_DEFAULT)

    /** Option scales the size of the texture pattern on the textured surface by expanding or shrinking the pattern. */
    val textureMapScale: Vec3f = StdVec3f().set(TEXTURE_MAP_SCALE_DEFAULT)

    /**
     * Option turns on turbulence for textures. Adding turbulence to a texture along a specified direction adds variance
     * to the original image and allows a simple image to be repeated over a larger area without noticeable tiling
     * effects.
     *
     * By default, the turbulence for every texture map used in a material is uvw = (0,0,0). This means that no
     * turbulence will be applied and the 2D texture will behave normally. Only when you raise the turbulence values
     * above zero will you see the effects of turbulence.
     */
    val textureTurbulence: Vec3f = StdVec3f().set(TEXTURE_TURBULENCE_DEFAULT)

    companion object {
        private const val TEXTURE_PATH_DEFAULT = ""
        private const val BLEND_HORIZONTAL_DEFAULT = true
        private const val BLEND_VERTICAL_DEFAULT = true
        private const val BUMP_MULTIPLIER_DEFAULT = 0.0f
        private const val BOOST_DEFAULT = 0.0f
        private const val COLOR_CORRECTION_DEFAULT = false
        private const val CLAMP_DEFAULT = false
        private const val BASE_DEFAULT = 0.0f
        private const val GAIN_DEFAULT = 1.0f
        private val TEXTURE_MAP_OFFSET_DEFAULT = ZERO
        private val TEXTURE_MAP_SCALE_DEFAULT = ZERO
        private val TEXTURE_TURBULENCE_DEFAULT = ZERO
    }

}
