package de.colibriengine.graphics.model

import de.colibriengine.graphics.material.MaterialLibrary
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec2f.Vec2fFactory
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fFactory
import de.colibriengine.math.vector.vec4f.Vec4f
import de.colibriengine.math.vector.vec4f.Vec4fFactory
import java.net.URL
import java.util.function.Consumer

// TODO: Make this an interface with free() being a default method.
abstract class AbstractModelDefinition(
    /** Ths URL points to the resource from which this model definition initially got constructed. */
    val constructedFrom: URL?
) {

    /** Vertex positions of this model: Each point of the object in 3D space. */
    val vertexPositions: ArrayList<Vec3f> = ArrayList()

    /** 2D texture coordinates specifying a location on a texture. */
    val vertexTextCoords: ArrayList<Vec2f> = ArrayList()

    /** Normal vector definitions. They specify the "outside" of a vertex. */
    val vertexNormals: ArrayList<Vec3f> = ArrayList()

    /** Color definitions which can be used to color vertexPositions. */
    val vertexColors: ArrayList<Vec4f> = ArrayList()

    val matLibPaths: ArrayList<String> = ArrayList()
    val matLibs: ArrayList<MaterialLibrary> = ArrayList()
    val groups: ArrayList<Group> = ArrayList()

    fun free(
        vec2fFactory: Vec2fFactory?,
        vec3fFactory: Vec3fFactory?,
        vec4fFactory: Vec4fFactory?
    ) {
        // TODO: re-enable freeing? Pre-allocate big memory block for model creation?
        if (vec2fFactory != null) {
            // vertexTextCoords.forEach(vec2fFactory::free);
            vertexTextCoords.clear()
        }
        if (vec3fFactory != null) {
            // vertexPositions.forEach(vec3fFactory::free);
            vertexPositions.clear()

            // vertexNormals.forEach(vec3fFactory::free);
            vertexNormals.clear()
        }
        if (vec4fFactory != null) {
            // vertexColors.forEach(vec4fFactory::free);
            vertexColors.clear()
        }
        matLibPaths.clear()
        matLibs.forEach(Consumer { obj: MaterialLibrary -> obj.free() })
        matLibs.clear()
        groups.forEach(Consumer { obj: Group -> obj.free() })
        groups.clear()
    }

    // TODO: add position
    fun addVertex(vertex: Vec3f) {
        vertexPositions.add(vertex)
    }

    fun addVertexTexCoord(vertexTexCoord: Vec2f) {
        vertexTextCoords.add(vertexTexCoord)
    }

    fun addVertexNormal(vertexNormal: Vec3f) {
        vertexNormals.add(vertexNormal)
    }

    fun addVertexColor(vertexColor: Vec4f) {
        vertexColors.add(vertexColor)
    }

    fun addMatLibPath(matLibPath: String) {
        matLibPaths.add(matLibPath)
    }

    fun addMatLib(matLib: MaterialLibrary) {
        matLibs.add(matLib)
    }

    fun addGroup(group: Group) {
        groups.add(group)
    }

}
