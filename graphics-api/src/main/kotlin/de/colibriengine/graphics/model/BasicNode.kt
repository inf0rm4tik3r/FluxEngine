package de.colibriengine.graphics.model

import de.colibriengine.ecs.Entity
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer

class BasicNode(entity: Entity) : AbstractSceneGraphNode(entity) {

    override fun init() {
    }

    override fun update(timing: Timer.View) {
    }

    override fun destroy() {
    }

}
