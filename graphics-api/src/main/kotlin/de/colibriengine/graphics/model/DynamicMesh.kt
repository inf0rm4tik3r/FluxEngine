package de.colibriengine.graphics.model

import de.colibriengine.graphics.material.Material
import de.colibriengine.graphics.rendering.PrimitiveMode

interface DynamicMesh {

    val primitiveMode: PrimitiveMode

    var material: Material?

    /** Renders this mesh. Note that this mesh must be bound first! */
    fun render()

    fun clear()

    fun release()

}
