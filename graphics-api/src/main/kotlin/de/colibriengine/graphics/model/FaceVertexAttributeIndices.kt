package de.colibriengine.graphics.model

import de.colibriengine.buffers.BufferStorable

/**
 * An instance of FaceVertexAttributeIndices contains the indices into arrays of vertex attribute data, making up a
 * primitive made up of [getVertexCount] vertices.
 */
interface FaceVertexAttributeIndices : BufferStorable {

    fun getVertexCount(): Int

    fun getPositionIndices(): IntArray

    fun getNormalIndices(): IntArray

    fun getTexCoordIndices(): IntArray

    fun getColorIndices(): IntArray

}
