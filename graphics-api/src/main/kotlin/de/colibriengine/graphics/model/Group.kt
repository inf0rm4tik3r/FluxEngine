package de.colibriengine.graphics.model

import de.colibriengine.graphics.rendering.PrimitiveMode

/** A group is a part of a mesh. It contains data in a persistent format that is rendered in one primitive mode. */
interface Group {

    val name: String

    val primitiveMode: PrimitiveMode

    val vertexDataSize: Int
    val indexDataSize: Int

    val allowPersistentlyMappedBuffers: Boolean

    val faceVertexAttributeIndices: List<FaceVertexAttributeIndices>
    val materialName: String?

    fun free()

}
