package de.colibriengine.graphics.model

interface MeshFactory {

    fun createFrom(group: Group, modelDefinition: AbstractModelDefinition): Mesh

}
