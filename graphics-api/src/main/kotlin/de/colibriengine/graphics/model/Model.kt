package de.colibriengine.graphics.model

import java.net.URL

interface Model {

    // TODO: Use ResourceName instead.
    val constructedFrom: URL?

    val active: Boolean

    val meshes: List<Mesh>

    fun createFrom(modelDefinition: AbstractModelDefinition)

    fun render()

    // TODO: Maybe move this somewhere else, so that not everybody can call this method.
    //       Calling it without informing the manager can lead to inconsistencies.
    fun release()

}
