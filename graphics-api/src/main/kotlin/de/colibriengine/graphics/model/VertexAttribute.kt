package de.colibriengine.graphics.model

import de.colibriengine.graphics.DataType

data class VertexAttribute(val index: Int, val size: Int, val dataType: DataType, val bytes: Int) {

    init {
        require(size >= 0)
        require(size <= 4)
        require(bytes >= 0)
    }

}
