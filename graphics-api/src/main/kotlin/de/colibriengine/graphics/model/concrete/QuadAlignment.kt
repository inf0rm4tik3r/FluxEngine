package de.colibriengine.graphics.model.concrete

enum class QuadAlignment {

    CENTERED,
    TOP_LEFT_ALIGNED,
    TOP_RIGHT_ALIGNED,
    BOTTOM_LEFT_ALIGNED,
    BOTTOM_RIGHT_ALIGNED

}
