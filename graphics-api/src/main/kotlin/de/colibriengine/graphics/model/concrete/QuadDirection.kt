package de.colibriengine.graphics.model.concrete

enum class QuadDirection {

    XY,
    XZ

}
