package de.colibriengine.graphics.rendering

enum class PrimitiveMode {

    POINTS,
    LINES,
    TRIANGLES

}
