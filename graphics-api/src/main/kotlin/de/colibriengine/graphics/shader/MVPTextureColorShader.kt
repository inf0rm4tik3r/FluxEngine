package de.colibriengine.graphics.shader

import de.colibriengine.math.vector.vec4f.Vec4fAccessor

interface MVPTextureColorShader : MVPTextureShader {

    fun setColor(color: Vec4fAccessor)

}
