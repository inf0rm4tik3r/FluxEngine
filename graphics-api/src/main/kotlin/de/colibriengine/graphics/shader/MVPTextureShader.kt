package de.colibriengine.graphics.shader

/** A shader which has a model-view-projection matrix and a texture. Provides the appropriate setter methods. */
interface MVPTextureShader : MVPShader {

    /**
     * Sets the texture unit from which the sampler should sample its data.
     *
     * @param textureUnit The texture unit to sample from.
     */
    fun setTexture(textureUnit: Int)

}
