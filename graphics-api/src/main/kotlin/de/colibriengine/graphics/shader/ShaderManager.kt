package de.colibriengine.graphics.shader

interface ShaderManager {

    /**
     * Requests a shader source file in {@code String} representation from this manager object. If the file was
     * never requested before it gets loaded and cached. Subsequent calls to this method will return the cached
     * {@code String} rather than accessing the file again, unless the source got dismissed by a call to {@link
     * ShaderManagerImpl#dismissShaderSource(String)}.
     *
     * @param path Specify the source you want to receive by specifying a path relative to the working directory.
     *     Example: "shaders/myShader.vs"
     * @return A String with the content of the specified file.
     * @throws IllegalArgumentException if the given path argument is null.
     */
    fun requestShaderSource(path: String): String

    /**
     * Removes the specified shader source from the cache.
     *
     * @param path Specify the source you want to dismiss by specifying a path relative to the working directory.
     *     Example: "shaders/myShader.vs"
     * @throws IllegalArgumentException if the given path argument is null.
     */
    fun dismissShaderSource(path: String)

}
