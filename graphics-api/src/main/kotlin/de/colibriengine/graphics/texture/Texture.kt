package de.colibriengine.graphics.texture

interface Texture {

    val resourceName: String // TODO: Change to ResourceName if OpenGLTexture is converted to Kotlin.

    val options: TextureOptions

    val target: TextureTarget

    @Deprecated("Always bind a texture with a specific texture index!")
    fun bind()

    fun bind(textureUnit: Int)

    fun unbind(textureUnit: Int)

    fun destroy()

}
