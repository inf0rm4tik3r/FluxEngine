package de.colibriengine.graphics.texture

import de.colibriengine.asset.ResourceName
import de.colibriengine.asset.image.Image

interface TextureFactory {

    fun create(
        resourceName: ResourceName,
        options: TextureOptions,
        format: ImageFormat,
        width: UInt,
        height: UInt
    ): Texture

    fun createFromImage(
        image: Image,
        options: TextureOptions,
        format: ImageFormat
    ): Texture

    fun createFromImageSRBG(
        image: Image,
        options: TextureOptions
    ): Texture

}
