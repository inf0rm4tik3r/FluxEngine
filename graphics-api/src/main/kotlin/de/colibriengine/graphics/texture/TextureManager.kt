package de.colibriengine.graphics.texture

import de.colibriengine.asset.ResourceName

interface TextureManager {

    /**
     * Requests the texture specified through {@code path}. If the texture was already loaded (from this instance), it
     * gets returned from this instances texture cache. This method will otherwise initialte the loading of the texture
     * with a call to {@link TextureManagerImpl#loadTexture(URL, int)}, store the result in the texture cache and return
     * it.
     *
     * @param resourceName The path of the texture. It is advised to provide a relative path inside the current working
     *     directory. For example: "textures/wall.png".
     * @param options ...
     * @return The loaded texture. Ready to be used inside OpenGL.
     * @throws ResourceNotFoundException If the resource object specified through {@code path} could not be found.
     * @throws TextureLoadException If {@link TextureManagerImpl#loadTexture(URL, int)} threw a TextureLoadException. =>
     *     The system is unable to load the texture.
     */
    // TODO: A "texture" is one level above a simple resource (image). We might just want to take an "image" here...
    // But consider: Performance? Streaming?
    fun requestTexture(resourceName: ResourceName, options: TextureOptions): Texture

    fun giveBack(texture: Texture)

    /**
     * Releases all cached textures and looses their references.
     * Must be called if this object should no longer be needed.
     * TODO: Call this!
     */
    fun destroy()

}
