package de.colibriengine.graphics.texture

import de.colibriengine.math.vector.vec4f.Vec4fAccessor

data class TextureOptions(

    val target: TextureTarget,

    /**
     * Defines the way texels are retrieved from a texture when samples are taken from outside the standard 0..1 range.
     */
    val wrapType: TextureWrapType,

    /**
     * The minification filter. It is applied when an image is zoomed out so far that multiple pixels (texels) on the
     * source image make up a single pixel (fragment) on the display screen.
     */
    val minFilter: TextureFilterType,

    /**
     * The magnification filter. It is applied when an image is zoomed in so close that one pixel (texel) on the source
     * image takes up multiple pixels (fragments) on the display screen.
     */
    val magFilter: TextureFilterType,

    val generateMipmaps: Boolean = false,

    val borderColor: Vec4fAccessor? = null

) {
    companion object {
        /** Only nearest filtering. No mipmap-generation. */
        val RAW_2D = TextureOptions(
            TextureTarget.TEXTURE_2D,
            TextureWrapType.REPEAT,
            TextureFilterType.NEAREST,
            TextureFilterType.NEAREST,
            false
        )

        /** Linear filtering using mipmaps. */
        val SMOOTH_2D = TextureOptions(
            TextureTarget.TEXTURE_2D,
            TextureWrapType.REPEAT,
            TextureFilterType.LINEAR_MIPMAP_LINEAR,
            TextureFilterType.LINEAR,
            true
        )
    }
}
