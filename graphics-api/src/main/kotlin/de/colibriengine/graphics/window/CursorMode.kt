package de.colibriengine.graphics.window

enum class CursorMode {

    NORMAL,
    HIDDEN,
    LOCKED

}
