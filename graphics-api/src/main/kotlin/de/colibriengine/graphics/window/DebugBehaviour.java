package de.colibriengine.graphics.window;

/**
 * DebugBehaviour
 *
 * @version 0.0.0.1
 * @time 22.09.2018 12:38
 * @since ColibriEngine 0.0.7.1
 */
public enum DebugBehaviour {
    
    /**
     * Caught errors will neither create nor throw an exception. They are essentially "unhandled" completely.
     */
    NO_EXCEPTION,
    
    /**
     * For each error caught, an exception will be generated and printed to the windows logging facility.
     */
    CREATE_EXCEPTION,
    
    /**
     * For each error caught, an exception will be generated and thrown.
     */
    THROW_EXCEPTION
    
}
