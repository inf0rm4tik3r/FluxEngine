package de.colibriengine.graphics.window

import de.colibriengine.util.ifNotNull
import org.lwjgl.glfw.*

/**
 * The [WindowCallbacks.initialize] method must be called after creation to set the callbacks.
 */
class WindowCallbacks(
    private val window: Window,
    private val windowInternals: WindowInternals
) {
    private val errorCallback: GLFWErrorCallback? = null
    private var closeCallback: GLFWWindowCloseCallback? = null
    private var focusCallback: GLFWWindowFocusCallback? = null
    private var sizeCallback: GLFWWindowSizeCallback? = null
    private var posCallback: GLFWWindowPosCallback? = null
    private var iconifyCallback: GLFWWindowIconifyCallback? = null
    private var maximizeCallback: GLFWWindowMaximizeCallback? = null
    private var refreshCallback: GLFWWindowRefreshCallback? = null
    private var dropCallback: GLFWDropCallback? = null
    private var framebufferSizeCallback: GLFWFramebufferSizeCallback? = null
    private var contentScaleCallback: GLFWWindowContentScaleCallback? = null
    private var keyCallback: GLFWKeyCallback? = null
    private var charCallback: GLFWCharCallback? = null
    private var charModsCallback: GLFWCharModsCallback? = null
    private var cursorEnterCallback: GLFWCursorEnterCallback? = null
    private var cursorPosCallback: GLFWCursorPosCallback? = null
    private var mouseButtonCallback: GLFWMouseButtonCallback? = null
    private var scrollCallback: GLFWScrollCallback? = null

    fun initialize() {
        // TODO: Investigate why enabling this code leads to random crashes during start up.
        /*
        GLFW.glfwSetErrorCallback(
            errorCallback = new GLFWErrorCallback() {
                @Override
                public void invoke(final int error, final long description) {
                    window.errorCallback(error, MemoryUtil.memUTF8(description));
                }
            });
        */

        closeCallback = object : GLFWWindowCloseCallback() {
            override fun invoke(windowHandle: Long) = window.closeCallback()
        }
        GLFW.glfwSetWindowCloseCallback(window.id, closeCallback)

        focusCallback = object : GLFWWindowFocusCallback() {
            override fun invoke(windowHandle: Long, focused: Boolean) = window.focusCallback(focused)
        }
        GLFW.glfwSetWindowFocusCallback(window.id, focusCallback)

        sizeCallback = object : GLFWWindowSizeCallback() {
            override fun invoke(windowHandle: Long, width: Int, height: Int) {
                windowInternals.cacheWindowSize(width, height)
                window.sizeCallback(width, height)
            }
        }
        GLFW.glfwSetWindowSizeCallback(window.id, sizeCallback)

        posCallback = object : GLFWWindowPosCallback() {
            override fun invoke(windowHandle: Long, xpos: Int, ypos: Int) {
                windowInternals.cacheWindowPosition(xpos, ypos)
                window.posCallback(xpos, ypos)
            }
        }
        GLFW.glfwSetWindowPosCallback(window.id, posCallback)

        iconifyCallback = object : GLFWWindowIconifyCallback() {
            override fun invoke(windowHandle: Long, iconified: Boolean) = window.iconifyCallback(iconified)
        }
        GLFW.glfwSetWindowIconifyCallback(window.id, iconifyCallback)

        maximizeCallback = object : GLFWWindowMaximizeCallback() {
            override fun invoke(windowHandle: Long, maximized: Boolean) = window.maximizeCallback(maximized)
        }
        GLFW.glfwSetWindowMaximizeCallback(window.id, maximizeCallback)

        refreshCallback = object : GLFWWindowRefreshCallback() {
            override fun invoke(windowHandle: Long) = window.refreshCallback()
        }
        GLFW.glfwSetWindowRefreshCallback(window.id, refreshCallback)

        dropCallback = object : GLFWDropCallback() {
            override fun invoke(windowHandle: Long, count: Int, names: Long) {
                windowInternals.dropCallbackDecode(count, names)
            }
        }
        GLFW.glfwSetDropCallback(window.id, dropCallback)

        framebufferSizeCallback = object : GLFWFramebufferSizeCallback() {
            override fun invoke(windowHandle: Long, width: Int, height: Int) {
                windowInternals.cacheFramebufferSize(width, height)
                window.framebufferSizeCallback(width, height)
            }
        }
        GLFW.glfwSetFramebufferSizeCallback(window.id, framebufferSizeCallback)

        contentScaleCallback = object : GLFWWindowContentScaleCallback() {
            override fun invoke(windowHandle: Long, xscale: Float, yscale: Float) {
                windowInternals.cacheWindowContentScale(xscale, yscale)
                window.contentScaleCallback(xscale, yscale)
            }
        }
        GLFW.glfwSetWindowContentScaleCallback(window.id, contentScaleCallback)

        keyCallback = object : GLFWKeyCallback() {
            override fun invoke(windowHandle: Long, key: Int, scancode: Int, action: Int, mods: Int) =
                window.keyCallback(key, scancode, action, mods)
        }
        GLFW.glfwSetKeyCallback(window.id, keyCallback)

        charCallback = object : GLFWCharCallback() {
            override fun invoke(windowHandle: Long, codepoint: Int) = window.charCallback(codepoint)
        }
        GLFW.glfwSetCharCallback(window.id, charCallback)

        charModsCallback = object : GLFWCharModsCallback() {
            override fun invoke(windowHandle: Long, codepoint: Int, mods: Int) =
                window.charModsCallBack(codepoint, mods)
        }
        GLFW.glfwSetCharModsCallback(window.id, charModsCallback)

        cursorEnterCallback = object : GLFWCursorEnterCallback() {
            override fun invoke(windowHandle: Long, entered: Boolean) = window.cursorEnterCallback(entered)
        }
        GLFW.glfwSetCursorEnterCallback(window.id, cursorEnterCallback)

        cursorPosCallback = object : GLFWCursorPosCallback() {
            override fun invoke(windowHandle: Long, xpos: Double, ypos: Double) {
                windowInternals.cacheCursorPosition(xpos, ypos)
                window.cursorPosCallback(xpos, ypos)
            }
        }
        GLFW.glfwSetCursorPosCallback(window.id, cursorPosCallback)

        mouseButtonCallback = object : GLFWMouseButtonCallback() {
            override fun invoke(windowHandle: Long, button: Int, action: Int, mods: Int) =
                window.mouseButtonCallback(button, action, mods)
        }
        GLFW.glfwSetMouseButtonCallback(window.id, mouseButtonCallback)

        scrollCallback = object : GLFWScrollCallback() {
            override fun invoke(windowHandle: Long, xoffset: Double, yoffset: Double) =
                window.scrollCallback(xoffset, yoffset)
        }
        GLFW.glfwSetScrollCallback(window.id, scrollCallback)
    }

    fun destroy() {
        ifNotNull(errorCallback) { it.free() }
        ifNotNull(closeCallback) { it.free() }
        ifNotNull(focusCallback) { it.free() }
        ifNotNull(sizeCallback) { it.free() }
        ifNotNull(posCallback) { it.free() }
        ifNotNull(iconifyCallback) { it.free() }
        ifNotNull(maximizeCallback) { it.free() }
        ifNotNull(refreshCallback) { it.free() }
        ifNotNull(dropCallback) { it.free() }
        ifNotNull(framebufferSizeCallback) { it.free() }
        ifNotNull(contentScaleCallback) { it.free() }
        ifNotNull(keyCallback) { it.free() }
        ifNotNull(charCallback) { it.free() }
        ifNotNull(charModsCallback) { it.free() }
        ifNotNull(cursorEnterCallback) { it.free() }
        ifNotNull(cursorPosCallback) { it.free() }
        ifNotNull(mouseButtonCallback) { it.free() }
        ifNotNull(scrollCallback) { it.free() }
    }

}
