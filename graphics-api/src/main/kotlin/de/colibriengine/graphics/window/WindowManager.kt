package de.colibriengine.graphics.window

interface WindowManager {

    val windows: List<Window>

    /**
     * Makes the specified window the active window. On update returns immediately if the specified window is already
     * active. Adds a reference to the previously active window (if present) in a focus history list. Makes the window
     * visible!
     */
    var activeWindow: Window?

    /**
     * Defines which windows are processed. Set this parameter true, and only the currently active window gets
     * processed. Otherwise all currently visible windows.
     */
    var onlyProcessActiveWindow: Boolean

    /**
     * Tells whether the specified window is part of this window manager.
     *
     * @param window The window which to checked.
     * @return True if the window is part of this window manager.
     */
    operator fun contains(window: Window): Boolean

    fun hasWindows(): Boolean = windows.isNotEmpty()

    /**
     * Adds the specified window to the engines windowing system. Returns immediately if the window has already been
     * addN.
     *
     * If you want to remove the window from the windowing system, call [AbstractWindow.setShouldClose] using `true` and
     * the window will get removed from the engine as fast as possible. After the close flag of a window has been set,
     * the worker manager will not call the parent worker update and render method for that particular window anymore.
     *
     * @param window The window object to add.
     * @param activate If set true, the given window will automatically become active through the
     *     [WindowManagerImpl.setActiveWindow] method, making its context current and the window visible.
     */
    fun addWindow(window: Window, activate: Boolean = true)

    /**
     * Performs a cleaning of the registered windows. Every window that has its shouldClose flag set to true will get
     * removed properly.
     *
     *
     * This method could release the last window!
     * Securing further calls to the windowManager with hasWindows() is advisable.
     *
     *
     * DO NOT CALL THIS METHOD YOURSELF!
     * TODO: to be shortened!!
     */
    fun cleanup()

    /**
     * Releases all registered windows. A following call to getActiveWindow() will result in a null reference.
     * No opengl context will be current after this method returns.
     */
    fun releaseAllWindows()

}
