package de.colibriengine.graphics.camera

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.window.Window
import de.colibriengine.math.matrix.mat4f.Mat4f
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.matrix.mat4f.StdMat4f
import de.colibriengine.math.quaternion.quaternionF.*
import de.colibriengine.math.vector.vec2i.StdVec2i
import de.colibriengine.math.vector.vec2i.Vec2i
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3f.*
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.UNIT_Y_AXIS
import de.colibriengine.scene.graph.AbstractSceneGraphNode

private const val DEFAULT_FOV = 70.0f
private const val DEFAULT_Z_NEAR = 0.1f
private const val DEFAULT_Z_FAR = 1000.0f
private val INITIAL_TRANSLATION = StdImmutableVec3f.builder().of(0f, 0f, 0f)
private val INITIAL_ROTATION = StdImmutableQuaternionF.builder().of(0f, 0f, 0f, 1f)
private val INITIAL_SCALE = StdImmutableVec3f.builder().of(1f, 1f, 1f)

/** Base class for camera implementations. */
abstract class AbstractCamera(entity: Entity, parentWindow: Window?) : Camera, AbstractSceneGraphNode(entity) {

    override var parentWindow: Window? = null
        set(value) {
            // Return early and save performance if this camera is already a child of the specified parent.
            if (field === value) return
            // This camera should only be known in one window at a time. Remove it from its old parent if necessary.
            field?.let { removeFromWindow(it) }
            // Update the "parent window" reference.
            field = value
            // And add the camera to the new parent window.
            field?.let { addToWindow(it) }
        }

    override var fov = DEFAULT_FOV
        set(value) {
            field = value
            reinitProjectionMatrices()
        }

    override var zNear = DEFAULT_Z_NEAR
        set(value) {
            field = value
            reinitProjectionMatrices()
        }

    override var zFar = DEFAULT_Z_FAR
        set(value) {
            field = value
            reinitProjectionMatrices()
        }

    override val position: ChangeDetectableVec3f get() = transform.translation
    override val rotation: ChangeDetectableQuaternionF get() = transform.rotation
    override val lookRotation: QuaternionFAccessor get() = StdQuaternionF().initLookRotation(forward, up)

    private val _tmpCamForward: Vec3f = StdVec3f()
    private val _tmpCamBackward: Vec3f = StdVec3f()
    private val _tmpCamLeft: Vec3f = StdVec3f()
    private val _tmpCamRight: Vec3f = StdVec3f()
    private val _tmpCamUp: Vec3f = StdVec3f()
    private val _tmpCamDown: Vec3f = StdVec3f()
    override val forward: Vec3fAccessor get() = rotation.forward(_tmpCamForward)
    override val backward: Vec3fAccessor get() = rotation.backward(_tmpCamBackward)
    override val left: Vec3fAccessor get() = rotation.left(_tmpCamLeft)
    override val right: Vec3fAccessor get() = rotation.right(_tmpCamRight)
    override val up: Vec3fAccessor get() = rotation.up(_tmpCamUp)
    override val down: Vec3fAccessor get() = rotation.down(_tmpCamDown)

    private val intermediateCameraTranslationMatrix: Mat4f = StdMat4f()
    private val intermediateCameraRotationMatrix: Mat4f = StdMat4f()

    protected val _viewMatrix: Mat4f = StdMat4f()
    override val viewMatrix: Mat4fAccessor get() = _viewMatrix

    override val lastProjectionResolutionUsed: Vec2i = StdVec2i()

    private val _orthographicProjectionMatrix: Mat4f = StdMat4f()
    override val orthographicProjectionMatrix: Mat4fAccessor get() = _orthographicProjectionMatrix
    private val _inverseOrthographicProjectionMatrix: Mat4f = StdMat4f()
    override val inverseOrthographicProjectionMatrix: Mat4fAccessor get() = _inverseOrthographicProjectionMatrix

    private val _perspectiveProjectionMatrix: Mat4f = StdMat4f()
    override val perspectiveProjectionMatrix: Mat4fAccessor get() = _perspectiveProjectionMatrix
    private val _inversePerspectiveProjectionMatrix: Mat4f = StdMat4f()
    override val inversePerspectiveProjectionMatrix: Mat4fAccessor get() = _inversePerspectiveProjectionMatrix

    private val _orthographicViewProjectionMatrix: Mat4f = StdMat4f()
    override val orthographicViewProjectionMatrix: Mat4fAccessor get() = _orthographicViewProjectionMatrix
    private val _inverseOrthographicViewProjectionMatrix: Mat4f = StdMat4f()
    override val inverseOrthographicViewProjectionMatrix: Mat4fAccessor get() = _inverseOrthographicViewProjectionMatrix

    private val _perspectiveViewProjectionMatrix: Mat4f = StdMat4f()
    override val perspectiveViewProjectionMatrix: Mat4fAccessor get() = _perspectiveViewProjectionMatrix
    private val _inversePerspectiveViewProjectionMatrix: Mat4f = StdMat4f()
    override val inversePerspectiveViewProjectionMatrix: Mat4fAccessor get() = _inversePerspectiveViewProjectionMatrix

    private val tmpMat4f: Mat4f = StdMat4f()

    override val active: Boolean
        get() = parentWindow?.cameraManager?.activeCamera === this

    init {
        transform.translation.set(INITIAL_TRANSLATION)
        transform.rotation.set(INITIAL_ROTATION)
        transform.scale.set(INITIAL_SCALE)

        // TODO: inspect
        this.parentWindow = parentWindow
    }

    private fun removeFromWindow(window: Window) {
        window.cameraManager.remove(this)
    }

    private fun addToWindow(window: Window) {
        window.cameraManager.add(this, false)
    }

    override fun use() {
        parentWindow?.cameraManager?.activeCamera = this
    }

    private fun reinitProjectionMatrices() {
        initProjectionMatrices(lastProjectionResolutionUsed.x, lastProjectionResolutionUsed.y)
    }

    override fun initProjectionMatrices(resolution: Vec2iAccessor) {
        initProjectionMatrices(resolution.x, resolution.y)
    }

    override fun initProjectionMatrices(width: Int, height: Int) {
        lastProjectionResolutionUsed.set(width, height)
        initOrthographicProjectionMatrix(0f, width.toFloat(), 0f, height.toFloat(), zNear, zFar)
        initPerspectiveProjectionMatrix(fov, width.toFloat() / height.toFloat(), zNear, zFar)
    }

    // TODO: performance: Only generate inverse if data really changed.
    override fun initOrthographicProjectionMatrix(
        left: Float, right: Float, bottom: Float, top: Float,
        zNear: Float, zFar: Float
    ) {
        _orthographicProjectionMatrix.initOrthographicProjectionRH(left, right, bottom, top, zNear, zFar)
        _inverseOrthographicProjectionMatrix.set(orthographicProjectionMatrix).inverse()
    }

    // TODO: performance: Only generate inverse if data really changed.
    override fun initPerspectiveProjectionMatrix(
        fov: Float, aspectRatio: Float,
        zNear: Float, zFar: Float
    ) {
        _perspectiveProjectionMatrix.initPerspectiveProjectionRH(fov, aspectRatio, zNear, zFar)
        _inversePerspectiveProjectionMatrix.set(perspectiveProjectionMatrix).inverse()
    }

    override fun calculateViewMatrix() {
        intermediateCameraTranslationMatrix.initTranslation(-position.x, -position.y, -position.z)
        intermediateCameraRotationMatrix.initRotation(rotation)
        _viewMatrix.set(intermediateCameraRotationMatrix).times(intermediateCameraTranslationMatrix)
    }

    override fun calculateViewProjectionMatrices() {
        calculateViewMatrix()
        calculateOrthographicProjectionMatrix()
        calculatePerspectiveProjectionMatrix()
    }

    private fun calculateOrthographicProjectionMatrix() {
        _orthographicViewProjectionMatrix.set(orthographicProjectionMatrix).times(viewMatrix)
        _inverseOrthographicViewProjectionMatrix.set(orthographicViewProjectionMatrix).inverse()
    }

    private fun calculatePerspectiveProjectionMatrix() {
        _perspectiveViewProjectionMatrix.set(perspectiveProjectionMatrix).times(viewMatrix)
        _inversePerspectiveViewProjectionMatrix.set(perspectiveViewProjectionMatrix).inverse()
    }

    override fun move(direction: Vec3fAccessor, amount: Float) {
        position.plus(
            direction.x * amount,
            direction.y * amount,
            direction.z * amount
        )
    }

    fun moveWithoutChangeDetection(direction: Vec3fAccessor, amount: Float) {
        position.plusWithoutChangeDetection(
            direction.x * amount,
            direction.y * amount,
            direction.z * amount
        )
    }

    // Inverted the use of the backward/forward direction because of the z axis pointing to the screen.
    override fun moveForward(amount: Float) = move(backward, amount)
    override fun moveBackward(amount: Float) = move(forward, amount)
    override fun moveLeft(amount: Float) = move(left, amount)
    override fun moveRight(amount: Float) = move(right, amount)
    override fun moveUp(amount: Float) = move(up, amount)
    override fun moveDown(amount: Float) = move(down, amount)

    override fun rotate(axis: Vec3fAccessor, angle: Float) {
        transform.rotate(axis, angle)
    }

    override fun getCameraFacingRotation(sourcePosition: Vec3fAccessor, storeIn: QuaternionF): QuaternionF {
        //val q = tmpMat4f.initLookAtRH(sourcePosition, UNIT_Y_AXIS, position).asQuaternion(storeIn)
        //val q = storeIn.initLookRotation(StdVec3f().set(sourcePosition).minus(position), UNIT_Y_AXIS)
        val q = storeIn.initLookAt(sourcePosition, position, UNIT_Y_AXIS)
        q.w = -q.w // TODO: Why do we have to do this???
        return q
    }

    override fun reset() = transform.initIdentity()

}
