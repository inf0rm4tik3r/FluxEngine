package de.colibriengine.graphics.camera

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.camera.concrete.ActionCameraImpl
import de.colibriengine.graphics.camera.concrete.ArcBallCameraImpl
import de.colibriengine.graphics.camera.concrete.SimpleCameraImpl
import de.colibriengine.graphics.camera.type.ActionCamera
import de.colibriengine.graphics.camera.type.ArcBallCamera
import de.colibriengine.graphics.camera.type.SimpleCamera
import de.colibriengine.graphics.window.Window

class CameraFactoryImpl(val ecs: ECS, val window: Window) : CameraFactory {

    override fun simple(): SimpleCamera {
        return SimpleCameraImpl(ecs.createEntity(), window)
    }

    override fun action(): ActionCamera {
        return ActionCameraImpl(ecs.createEntity(), window)
    }

    override fun arcBall(): ArcBallCamera {
        return ArcBallCameraImpl(ecs.createEntity(), window)
    }

}
