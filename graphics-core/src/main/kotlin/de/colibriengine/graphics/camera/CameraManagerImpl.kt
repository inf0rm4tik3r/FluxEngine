package de.colibriengine.graphics.camera

private const val CONTAINED_ERROR = "This camera is already part of this camera manager!"
private const val NOT_CONTAINED_ERROR = "This camera is not part of this camera manager!"
private const val INITIAL_CAMERAS_CAPACITY = 1

class CameraManagerImpl : CameraManager {

    private val cameras: MutableList<Camera> = ArrayList(INITIAL_CAMERAS_CAPACITY)

    override var activeCamera: Camera? = null
        set(camera) = when (camera) {
            null -> field = null
            else -> {
                require(camera in this) { NOT_CONTAINED_ERROR }
                field = camera
            }
        }

    override fun hasActiveCamera(): Boolean = activeCamera != null

    override operator fun contains(camera: Camera): Boolean {
        return cameras.contains(camera)
    }

    override fun add(camera: Camera, setActive: Boolean) {
        require(camera !in this) { CONTAINED_ERROR }
        cameras.add(camera)
        if (setActive) {
            activeCamera = camera
        }
    }

    override fun remove(camera: Camera): Boolean {
        require(camera in this) { NOT_CONTAINED_ERROR }
        cameras.remove(camera)
        if (activeCamera === camera) {
            activeCamera = null
            return true
        }
        return false
    }

}
