package de.colibriengine.graphics.camera

/**
 * The available camera projection types.
 * TODO: Give cameras a ProjectionType / ProjectionMode
 * @since ColibriEngine 0.0.7.2
 */
enum class ProjectionType {

    /** Flat projection. */
    ORTHOGRAPHIC,

    /** Natural perspective vision including the correct perception of a scenes depth. */
    PERSPECTIVE

}
