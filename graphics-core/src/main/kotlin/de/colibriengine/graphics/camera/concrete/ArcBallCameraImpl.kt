package de.colibriengine.graphics.camera.concrete

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.camera.AbstractCamera
import de.colibriengine.graphics.camera.type.ArcBallCamera
import de.colibriengine.graphics.window.CursorMode
import de.colibriengine.graphics.window.Window
import de.colibriengine.input.FECommands
import de.colibriengine.input.JoystickInputCallbacks
import de.colibriengine.input.MouseKeyboardInputCallbacks
import de.colibriengine.input.gamepads.XboxOneGamepad
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.math.vector.vec3f.ChangeDetectableVec3f
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.UNIT_X_AXIS
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.UNIT_Y_AXIS
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.util.Timer
import org.lwjgl.glfw.GLFW

@Suppress("MemberVisibilityCanBePrivate")
class ArcBallCameraImpl(
    entity: Entity,
    parentWindow: Window?
) : ArcBallCamera, AbstractCamera(entity, parentWindow), MouseKeyboardInputCallbacks, JoystickInputCallbacks {

    var movementSpeed = 2.0f
    var inSprint = false
    var sprintMultiplier = 2.0f
    var mouseSensitivity = 0.1f
    var gamepadSensitivity = 1.0f
    var degreesPerSecond = 180.0f
    var rotationFactor = 10.0f
    var slowlyApproachPoles = false

    val lookAt: ChangeDetectableVec3f = ChangeDetectableVec3f(StdVec3f(0.0f, 0.0f, 0.0f))
    var poleThresholdInDegrees: Float = 10.0f
        set(value) {
            require(value >= 0f) { "Value must be positive!" }
            require(value <= 90f) { "Value must be less than or equal to 90 degrees!" }
            field = value
        }
    var distanceRange: ClosedFloatingPointRange<Float> = 1f..20f
        set(value) {
            require(value.start >= 0f) { "Range must start with a positive value!" }
            require(value.endInclusive >= value.start) { "Range must not go downwards!" }
            field = value
        }

    private var frameMovAmt = 0f
    private var frameGamepadRotAmt = 0f

    private val _focusVector: Vec3f = StdVec3f()
    private val _fwd: Vec3f = StdVec3f()
    private val _right: Vec3f = StdVec3f()
    private val _up: Vec3f = StdVec3f()

    init {
        position.onChange.subscribe(next = { correctPositionAndRotation() })
        lookAt.onChange.subscribe(next = { correctPositionAndRotation() })
    }

    private fun correctPositionAndRotation() {
        correctPosition()
        correctRotation()
    }

    private fun correctPosition() {
        val distance = _focusVector.set(position).minus(lookAt).length()
        when {
            distance in distanceRange -> return
            distance < distanceRange.start -> enforceDistanceToLookAt(distance, distanceRange.start)
            distance > distanceRange.endInclusive -> enforceDistanceToLookAt(distance, distanceRange.endInclusive)
        }
    }

    private fun enforceDistanceToLookAt(currentDistance: Float, targetDistance: Float) {
        // Edge case: position and lookAt are equal -> shift position on x-axis to meet minimum requirement.
        if (currentDistance == 0f) moveWithoutChangeDetection(UNIT_X_AXIS, distanceRange.start)
        position.setWithoutChangeDetection(lookAt).plus(_focusVector.normalize().times(targetDistance))
        // No rotation-correction necessary, as invocations of this method are followed by a correction!
    }

    /**
     * The orientation of the camera must not be corrected after moving the camera. As the overwritten
     * calculateViewMatrix() method generates the view matrix without taking this cameras orientation into account,
     * it can theoretically be arbitrary. But some operations require this cameras orientation to always represent
     * the orientation from its current position to the [lookAt] target. This method corrects the orientation by
     * recalculating it based on the current [position] and [lookAt].
     */
    private fun correctRotation() {
        _fwd.set(position).minus(lookAt)
        _right.set(_fwd).cross(UNIT_Y_AXIS)
        _up.set(_right).cross(_fwd)
        rotation.initLookRotation(_fwd, _up)
    }

    override fun calculateViewMatrix() {
        _viewMatrix.initLookAtRH(position, UNIT_Y_AXIS, lookAt)
    }

    fun rotateAroundLookAt(horizontalShift: Float, verticalShift: Float) {
        // Get the direction in which we are currently looking.
        // Edge case: Performing a rotation when stying exactly on the target is not possible.
        _focusVector.set(position).minus(lookAt)
        if (_focusVector.length() == 0f) {
            return
        }

        // Compute the right vector of the camera.
        _fwd.set(_focusVector).invert().normalize()
        _right.set(_fwd).cross(UNIT_Y_AXIS)
        if (!slowlyApproachPoles) {
            _right.normalize()
        }

        // Rotate the camera vertically and horizontally by shifting its position.
        _focusVector.rotate(_right, computeLimitedVerticalShift(verticalShift))
        _focusVector.rotate(UNIT_Y_AXIS, horizontalShift)
        _focusVector.plus(lookAt)
        position.set(_focusVector) // Triggers position and orientation correction!
    }

    private fun computeLimitedVerticalShift(verticalShift: Float): Float {
        // Limit the verticalShift so that a certain threshold at the edges of the range is not passed.
        // The angle lies between 0 (looking down) and 180 (looking up)
        val currentAngle = _focusVector.angleDegrees(UNIT_Y_AXIS)
        assert(currentAngle in RANGE)
        val maxAngle = RANGE.endInclusive - poleThresholdInDegrees
        val minAngle = RANGE.start + poleThresholdInDegrees
        val targetedAngle = currentAngle + verticalShift
        return when {
            targetedAngle > maxAngle -> maxAngle - currentAngle
            targetedAngle < minAngle -> minAngle - currentAngle
            else -> verticalShift
        }
    }

    private fun shouldUpdate(): Boolean = active
        && parentWindow!!.cursorMode === CursorMode.LOCKED
        && parentWindow!!.cameraManager.activeCamera === this

    override fun init() {}

    override fun update(timing: Timer.View) {
        if (!shouldUpdate()) return
        frameMovAmt = movementSpeed * timing.deltaSP() * (if (inSprint) sprintMultiplier else 1f)
        frameGamepadRotAmt = gamepadSensitivity * degreesPerSecond * timing.deltaSP()
    }

    override fun destroy() {}

    override fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {
        if (!shouldUpdate()) return
        // TODO: This line seems unnecessary. If the mouse is moved, mk-input is activated and this expression
        //  should evaluate to true. If the joystick is used, no mouse movement occurs and this method will not be called...
        // if (parentInput!!.activeInputType === InputType.MOUSE_KEYBOARD) {
        // Camera movement
        when {
            key == FECommands.MOVE_FORWARD.kKey ->
                rotateAroundLookAt(0f, -frameMovAmt * rotationFactor)
            key == FECommands.MOVE_BACKWARD.kKey ->
                rotateAroundLookAt(0f, frameMovAmt * rotationFactor)
            key == FECommands.MOVE_LEFT.kKey ->
                rotateAroundLookAt(-frameMovAmt * rotationFactor, 0f)
            key == FECommands.MOVE_RIGHT.kKey ->
                rotateAroundLookAt(frameMovAmt * rotationFactor, 0f)
            key == FECommands.MOVE_UP.kKey -> moveForward(frameMovAmt)
            key == FECommands.MOVE_DOWN.kKey -> moveBackward(frameMovAmt)
            key == FECommands.SPRINT.kKey && action == GLFW.GLFW_PRESS -> inSprint = true
            key == FECommands.SPRINT.kKey && action == GLFW.GLFW_RELEASE -> inSprint = false
            key == FECommands.RESET_CAMERA.kKey && action == FECommands.RESET_CAMERA.kKeyAction -> reset()
        }
        // }
    }

    override fun mkMoveCallback(dx: Int, dy: Int) {
        if (!shouldUpdate()) return
        // TODO: This line seems unnecessary. If the mouse is moved, mk-input is activated and this expression
        //  should evaluate to true. If the joystick is used, no mouse movement occurs and this method will not be called...
        // if (parentInput!!.activeInputType === InputType.MOUSE_KEYBOARD) {
        rotateAroundLookAt(mouseSensitivity * dx, mouseSensitivity * dy)
        // }
    }

    override fun mkButtonCallback(button: Int, action: Int, mods: Int) {}

    override fun jAxisCallback(axis: Int, axisValue: Float) {
        if (!shouldUpdate()) return
        // There is no need to check if a value is out of the gamepads threshold values.
        // The jAxisCallback method only gets called if the new axis value is out of the gamepads threshold.
        val zoom = axisValue * frameMovAmt
        val angle = axisValue * frameGamepadRotAmt
        when (axis) {
            XboxOneGamepad.LS_X, XboxOneGamepad.RS_X -> rotateAroundLookAt(angle, 0f)
            XboxOneGamepad.LS_Y, XboxOneGamepad.RS_Y -> rotateAroundLookAt(0f, angle)
            XboxOneGamepad.LT -> moveBackward(zoom)
            XboxOneGamepad.RT -> moveForward(zoom)
            else -> LOG.warn("Unused axis: $axis")
        }
    }

    override fun jButtonCallback(button: Int, action: Int) {
        if (!shouldUpdate()) return
        if (button == FECommands.RESET_CAMERA.jButton && action == FECommands.RESET_CAMERA.jButtonAction) reset()
    }

    override fun jLostConnectionCallback(index: Int) {}

    companion object {
        private val LOG = getLogger(this)
        private val RANGE = 0f..180f
    }

}
