package de.colibriengine.graphics.camera.concrete

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.camera.AbstractCamera
import de.colibriengine.graphics.camera.type.SimpleCamera
import de.colibriengine.graphics.window.Window
import de.colibriengine.input.JoystickInputCallbacks
import de.colibriengine.input.MouseKeyboardInputCallbacks
import de.colibriengine.util.Timer

class SimpleCameraImpl(
    entity: Entity,
    parentWindow: Window?
) : SimpleCamera, AbstractCamera(entity, parentWindow), MouseKeyboardInputCallbacks, JoystickInputCallbacks {

    override fun init() {}

    override fun update(timing: Timer.View) {}

    override fun destroy() {}

    override fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {}

    override fun mkMoveCallback(dx: Int, dy: Int) {}

    override fun jAxisCallback(axis: Int, axisValue: Float) {}

    override fun jButtonCallback(button: Int, action: Int) {}

}
