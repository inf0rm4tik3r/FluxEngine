package de.colibriengine.graphics.font

import de.colibriengine.asset.AssetStore
import de.colibriengine.asset.ResourceLoader
import de.colibriengine.asset.ResourceName
import de.colibriengine.asset.load
import de.colibriengine.graphics.texture.TextureManager
import de.colibriengine.graphics.texture.TextureOptions
import de.colibriengine.util.StringUtil

class FontManagerImpl(
    private val resourceLoader: ResourceLoader,
    private val textureManager: TextureManager
) : FontManager {

    private val fonts: AssetStore<ResourceName, BitmapFont> = AssetStore("Fonts") {
        this.destroy(textureManager)
    }

    override fun destroy() {
        fonts.destroy(callDestroyOnRemaining = true)
    }

    override fun requestFont(resourceName: ResourceName): BitmapFont {
        return fonts.getOrPut(resourceName) { loadFont(resourceName) }
    }

    override fun giveBack(font: BitmapFont) {
        fonts.giveBack(font.descriptor.resourceName)
    }

    private fun loadFont(resourceName: ResourceName): BitmapFont {
        val font = BitmapFont(resourceLoader.load(resourceName))
        loadTextures(font)
        return font
    }

    private fun loadTextures(bitmapFont: BitmapFont) {
        for (page in bitmapFont.descriptor.pages) {
            bitmapFont.textures[page.id.toInt()] = textureManager.requestTexture(
                ResourceName("${StringUtil.removeLastPart(bitmapFont.descriptor.resourceName.s)}/${page.textureName}"),
                TextureOptions.RAW_2D
            )
        }
    }

}
