package de.colibriengine.graphics.font

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.rendering.BlendFactor
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer

class Text3D(
    entity: Entity,
    textFactory: TextFactory,
    textMode: TextMode,
    size: UInt,
    font: BitmapFont
) : AbstractSceneGraphNode(entity) {

    val text: Text = textFactory.create(textMode, size, font)

    init {
        val renderComponent = RenderComponent(text.model)
        renderComponent.transparency.isTransparent = true
        renderComponent.transparency.alphaThreshold = 0.99f
        renderComponent.blending.active = true
        renderComponent.blending.blendFuncSourceFactor = BlendFactor.SRC_ALPHA
        renderComponent.blending.blendFuncDestinationFactor = BlendFactor.ONE_MINUS_SRC_ALPHA
        entity.add(renderComponent)
    }

    override fun init() {}
    override fun update(timing: Timer.View) {}
    override fun destroy() {}

}
