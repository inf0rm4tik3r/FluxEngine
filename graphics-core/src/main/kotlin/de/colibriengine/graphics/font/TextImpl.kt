package de.colibriengine.graphics.font

import de.colibriengine.asset.font.BitmapFontCharacter
import de.colibriengine.asset.font.BitmapFontDescriptor
import de.colibriengine.graphics.font.TextMode.DYNAMIC
import de.colibriengine.graphics.font.TextMode.FIXED
import de.colibriengine.graphics.material.MaterialDefinition
import de.colibriengine.graphics.material.MaterialImpl
import de.colibriengine.graphics.model.*
import de.colibriengine.graphics.model.concrete.QuadImpl
import de.colibriengine.graphics.rendering.PrimitiveMode
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.math.vector.vec2f.*
import de.colibriengine.math.vector.vec3f.*
import de.colibriengine.math.vector.vec4f.StdVec4f
import de.colibriengine.math.vector.vec4f.StdVec4fFactory
import de.colibriengine.math.vector.vec4f.Vec4f
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.util.MutableString
import java.nio.ByteBuffer

class TextImpl(
    override val mode: TextMode,
    length: UInt,
    font: BitmapFont,
    meshFactory: MeshFactory
) : Text {

    private val text: MutableString = MutableString(length)

    override val size: UInt
        get() {
            return text.length
        }

    override var font: BitmapFont = font
        set(value) {
            field = value
            if (model.active) {
                createMaterial()
            }
        }

    override val color: Vec4f = StdVec4f(1.0f, 1.0f, 1.0f, 1.0f)

    override val model: Model = ModelImpl(meshFactory)

    /* Used when updating the model... */
    private var indexCount = 0
    private var vertexCount = 0
    private val tmpPosBottomRight: Vec3f = StdVec3f()
    private val tmpPosTopRight: Vec3f = StdVec3f()
    private val tmpPosTopLeft: Vec3f = StdVec3f()
    private val tmpPosBottomLeft: Vec3f = StdVec3f()
    private val tmpTexCoordBottomRight: Vec2f = StdVec2f()
    private val tmpTexCoordTopRight: Vec2f = StdVec2f()
    private val tmpTexCoordTopLeft: Vec2f = StdVec2f()
    private val tmpTexCoordBottomLeft: Vec2f = StdVec2f()

    init {
        if (mode == FIXED) {
            createModel()
        }
    }

    /**
     * Updates the content of this text.
     * - Recreates the model of this text if size is set to [TextImpl.DYNAMIC_SIZE].
     * - Refills the models buffer with the new text glyphs if a fixed size is set.
     *
     *
     * This method is rather expensive if this texts size is set to [TextImpl.DYNAMIC_SIZE]! Using a fixed
     * size is much more efficient, as there is no need to recreate the text model (including all necessary buffers)
     * on the GPU.
     *
     * @param text The new string to display.
     * @return This object to allow method chaining.
     */
    override fun update(newText: String): TextImpl {
        require(!(mode == FIXED && newText.length.toUInt() > size)) {
            "The length of the new text exceeds the specified (FIXED) character amount of: $size"
        }
        text.put(newText, 0, -1)

        // (Re)create the model if we are not operating on a fixed-size text object.
        // TODO: Not recreate all the time? Use larger buffer like ArrayList and only recreate when exceeded.
        // TODO: Then store the index up to which the buffer is used. Only render this many characters from the buffer.
        if (mode == DYNAMIC) {
            createModel()
        }

        // And refill the models buffer.
        fillModel()
        return this
    }

    override fun update(updater: MutableString.() -> Unit): TextImpl {
        // The MutableString is of fixed length. No further checks necessary.
        text.updater()

        // (Re)create the model if we are not operating on a fixed-size text object.
        if (mode == DYNAMIC) {
            createModel()
        }

        // And refill the models buffer.
        fillModel()
        return this
    }

    private fun createModel() {
        if (model.active) {
            model.release()
        }
        val vertexDataSize: UInt
        val indexDataSize: UInt
        if (mode == DYNAMIC) {
            vertexDataSize = text.length * VERTEX_POSITIONS_PER_CHARACTER
            indexDataSize = text.length * QuadImpl.INDICES.toUInt()
        } else {
            vertexDataSize = size * VERTEX_POSITIONS_PER_CHARACTER
            indexDataSize = size * QuadImpl.INDICES.toUInt()
        }
        val modelDefinition = CustomModelDefinition()
        val group = GroupImpl(vertexDataSize.toInt(), indexDataSize.toInt())
        group.primitiveMode = PrimitiveMode.TRIANGLES
        group.allowPersistentlyMappedBuffers = true
        modelDefinition.addGroup(group)
        model.createFrom(modelDefinition)
        //modelDefinition.free(null, null, null);

        createMaterial()
    }

    private fun fillModel() {
        val mesh = model.meshes[0]
        val dataBuffer = mesh.dataBuffer
        val indexBuffer = mesh.indexBuffer
        dataBuffer.bind()
        val dataBufferMapping = dataBuffer.mapWriteOnly()
        indexBuffer.bind()
        val indexBufferMapping = indexBuffer.mapWriteOnly()
        indexBuffer.unbind()
        calculateCharacters(dataBufferMapping, indexBufferMapping)
        mesh.indicesToDraw = indexCount
        dataBuffer.bind()
        dataBuffer.unmap()
        indexBuffer.bind()
        indexBuffer.unmap()
        indexBuffer.unbind()
    }

    private fun createMaterial() {
        val materialDefinition = MaterialDefinition()
        val material = MaterialImpl(materialDefinition) // TODO: Use MaterialFactory/Manager instead.
        for (page in font.descriptor.pages) {
            material.ambientTextures.add(font.textures[0]!!)
        }
        model.meshes[0].material = material
    }

    private fun calculateCharacters(vertices: ByteBuffer, indices: ByteBuffer) {
        vertexCount = 0
        indexCount = 0
        var line = 1f
        var x = 0f
        var y = 0f
        val lineHeight = font.descriptor.common.lineHeight.toFloat()
        val texW = font.descriptor.common.scaleW.toFloat()
        val texH = font.descriptor.common.scaleH.toFloat()
        for (i in 0 until size.toInt()) {
            val c = text.charAt(i)

            //LOG.debug("Setting up character: ID = " + (int) c + ", char = " + c);
            val character = font.descriptor.characters[c.code.toUInt()]
            if (character != null) {

                //LOG.debug("Found character description: " + character);

                // Apply kerning if there is another character in the text...
                x += getKerning(i, c).toFloat()
                calculateCharacter(vertices, indices, character, x, y, line, lineHeight, texW, texH)

                // Advance our current cursor position (x).
                x += character.xAdvance + EXTRA_CHARACTER_SPACING
            } else {
                LOG.error("Unable to find character $c with code ${c.code} in font $font.")
            }

            // Check if the cursor should jump into the next line.
            if ((i + 1) % CHARACTERS_PER_LINE == 0) {
                line++
                y -= lineHeight + EXTRA_LINE_SPACING
                x = 0f // Reset x!
            }
        }
    }

    private fun calculateCharacter(
        vertices: ByteBuffer, indices: ByteBuffer,
        character: BitmapFontCharacter,
        x: Float, y: Float,
        line: Float, lineHeight: Float,
        texW: Float, texH: Float
    ) {
        /* POSITIONS */
        val posXLeft: Float = x + character.xOffset
        val posXRight: Float = x + character.xOffset + character.width
        val posYTop: Float = y - character.yOffset
        val posYBottom: Float = y - (character.yOffset + character.height)
        tmpPosBottomRight.set(posXRight, posYBottom, ZERO_F)
        tmpPosTopRight.set(posXRight, posYTop, ZERO_F)
        tmpPosTopLeft.set(posXLeft, posYTop, ZERO_F)
        tmpPosBottomLeft.set(posXLeft, posYBottom, ZERO_F)

        /* TEXTURE COORDINATES */
        val texCoordXLeft: Float = character.x / texW
        val texCoordXRight: Float = (character.x + character.width) / texW
        val texCoordYTop: Float = (texH - character.y) / texH
        val texCoordYBottom: Float = (texH - character.y - character.height) / texH
        tmpTexCoordBottomRight.set(texCoordXRight, texCoordYBottom)
        tmpTexCoordTopRight.set(texCoordXRight, texCoordYTop)
        tmpTexCoordTopLeft.set(texCoordXLeft, texCoordYTop)
        tmpTexCoordBottomLeft.set(texCoordXLeft, texCoordYBottom)

        /* STORE VERTICES AND INDICES */
        storeVertex(
            vertices,
            tmpPosBottomRight,
            UNIT_Z_AXIS,
            tmpTexCoordBottomRight,
            WHITE
        )
        storeVertex(vertices, tmpPosTopRight, UNIT_Z_AXIS, tmpTexCoordTopRight, WHITE)
        storeVertex(vertices, tmpPosTopLeft, UNIT_Z_AXIS, tmpTexCoordTopLeft, WHITE)
        storeVertex(vertices, tmpPosBottomLeft, UNIT_Z_AXIS, tmpTexCoordBottomLeft, WHITE)

        // First triangle.
        indices.putInt(vertexCount)
        indices.putInt(vertexCount + 1)
        indices.putInt(vertexCount + 2)

        // Second triangle.
        indices.putInt(vertexCount)
        indices.putInt(vertexCount + 2)
        indices.putInt(vertexCount + 3)
        indexCount += QuadImpl.INDICES
        vertexCount += QuadImpl.VERTICES
    }

    private fun storeVertex(
        buffer: ByteBuffer,
        position: Vec3fAccessor,
        normal: Vec3fAccessor,
        texCoord: Vec2fAccessor,
        color: Vec4fAccessor
    ) {
        position.storeIn(buffer)
        normal.storeIn(buffer)
        texCoord.storeIn(buffer)
        color.storeIn(buffer)
    }

    /**
     * @param cCurrentIndex The index of the current character in the string.
     * @param cCurrent      The current character under processing.
     * @return The difference in horizontal offset between the given current character and the next character (if
     * existent).
     */
    private fun getKerning(cCurrentIndex: Int, cCurrent: Char): Short {
        return when {
            cCurrentIndex < size.toInt() - 1 -> font.descriptor.getKerningAmt(cCurrent, text.charAt(cCurrentIndex + 1))
            else -> BitmapFontDescriptor.DEFAULT_KERNING
        }
    }

    override fun toString(): String {
        return this.javaClass.simpleName + ": \"" + text + "\""
    }

    companion object {
        private val LOG = getLogger(TextImpl::class.java)
        private const val VERTEX_POSITIONS_PER_CHARACTER = 4u
        private const val VERTEX_NORMALS_PER_CHARACTER = 1u
        private const val VERTEX_TEX_COORDS_PER_CHARACTER = 4u
        private const val CHARACTERS_PER_LINE = 30
        private const val WIDTH_PER_LINE = 30
        private const val EXTRA_CHARACTER_SPACING = 0f
        private const val EXTRA_LINE_SPACING = -100f
        private const val ZERO_F = 0.0f

        private val UNIT_Z_AXIS: Vec3f = StdVec3f().set(StdVec3fFactory.UNIT_Z_AXIS)
        private val WHITE: Vec4f = StdVec4f().set(StdVec4fFactory.WHITE)
    }

}
