package de.colibriengine.graphics.lighting.light

import de.colibriengine.ecs.ECS
import de.colibriengine.ecs.useIfPresent
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.icons.LightIcon
import de.colibriengine.graphics.model.ModelNode
import de.colibriengine.graphics.window.WindowManager
import de.colibriengine.math.vector.vec3d.StdVec3dFactory.Companion.ONE
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer

// TODO: Do not depend on ShaderManager.
// TODO: Perform update through ECS system?
// TODO: Move light data into LigtComponent, PointLightComponent, ...
abstract class AbstractLight internal constructor(
    ecs: ECS,
    val windowManager: WindowManager,
    lightIcon: LightIcon
) : AbstractSceneGraphNode(ecs.createEntity()), LightSource {

    override val color: Vec3f = StdVec3f().set(ONE)
    override var intensity: Float = 3.0f
    override var castsShadows: Boolean = false

    /** Represents a visual which is rendered into the scene if the user wants to interact with this light. */
    private val visual: AbstractSceneGraphNode

    override val position: Vec3f
        get() = transform.translation

    private val tmpVec3f: Vec3f = StdVec3f()

    init {
        visual = ModelNode(ecs.createEntity(), lightIcon.model)
        visual.entity.useIfPresent<RenderComponent> {
            transparency.isTransparent = true
            transparency.alphaThreshold = 0.99f
            shadowCaster = false
        }
        add(visual)
    }

    override fun update(timing: Timer.View) {
        // Only update the objects orientation if a camera to look to is present.
        windowManager.activeWindow?.cameraManager?.activeCamera?.let {
            // This rotates the lights visual so that it faces to the current camera.
            it.getCameraFacingRotation(position, visual.transform.rotation)

            // This positions the lights visual slightly behind the light (as seen from the current camera).
            val dirToLight = tmpVec3f.set(position).minus(it.position)
            visual.transform.translation.set(
                if (dirToLight.length() != 0f) dirToLight.normalize().times(0.5f) else dirToLight
            )
        }
    }

    override fun destroy() {}

}
