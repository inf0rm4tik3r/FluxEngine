package de.colibriengine.graphics.lighting.light

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.icons.LightIcon
import de.colibriengine.graphics.window.WindowManager

class LightFactoryImpl(
    private val ecs: ECS,
    private val windowManager: WindowManager,
    private val lightIcon: LightIcon
) : LightFactory {

    override fun point(): PointLight {
        return PointLightImpl(ecs, windowManager, lightIcon)
    }

    override fun spot(): SpotLight {
        return SpotLightImpl(ecs, windowManager, lightIcon)
    }

    override fun directional(): DirectionalLight {
        return DirectionalLightImpl(ecs, windowManager, lightIcon)
    }

}
