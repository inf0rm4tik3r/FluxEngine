package de.colibriengine.graphics.lighting.light

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.icons.LightIcon
import de.colibriengine.graphics.window.WindowManager
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f

class PointLightImpl(
    ecs: ECS,
    windowManager: WindowManager,
    lightIcon: LightIcon
) : AbstractLight(ecs, windowManager, lightIcon), PointLight {

    override val attenuation: Vec3f = StdVec3f().set(0.3f, 0.2f, 1.0f)

    override var dropOffFactor = 500f

    override fun init() {}

}
