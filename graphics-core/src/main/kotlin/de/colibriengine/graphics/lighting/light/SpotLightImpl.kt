package de.colibriengine.graphics.lighting.light

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.icons.LightIcon
import de.colibriengine.graphics.window.WindowManager
import de.colibriengine.math.quaternion.quaternionF.QuaternionF

class SpotLightImpl(
    ecs: ECS,
    windowManager: WindowManager,
    lightIcon: LightIcon
) : AbstractLight(ecs, windowManager, lightIcon), SpotLight {

    override val orientation: QuaternionF
        get() = transform.rotation

    override fun init() {}

}
