package de.colibriengine.graphics.material

import java.util.function.Consumer

class MaterialLibraryImpl constructor(
    val materialLibraryDefinition: MaterialLibraryDefinition
) : MaterialLibrary {

    override val materials: MutableList<MaterialImpl> = mutableListOf()

    init {
        // Create materials from the given material definitions.
        for (materialDefinition in materialLibraryDefinition.materialDefinitions) {
            val material = MaterialImpl(materialDefinition) // TODO: Use MaterialFactory instead?
            materials.add(material)
        }
    }

    override fun free() {
        materials.forEach(Consumer { obj: MaterialImpl -> obj.free() })
        materials.clear()
    }

}
