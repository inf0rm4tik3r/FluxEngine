package de.colibriengine.graphics.material.mtl;

import de.colibriengine.graphics.material.MaterialDefinition;
import org.jetbrains.annotations.NotNull;

public class MTLMaterial extends MaterialDefinition {

    public MTLMaterial() {
        super();
    }

    public MTLMaterial(@NotNull String name) {
        super(name);
    }

}
