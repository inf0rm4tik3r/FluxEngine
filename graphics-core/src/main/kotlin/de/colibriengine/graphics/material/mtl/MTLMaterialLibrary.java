package de.colibriengine.graphics.material.mtl;

import de.colibriengine.graphics.material.MaterialLibraryDefinition;

import java.net.URL;

import org.jetbrains.annotations.NotNull;

public class MTLMaterialLibrary extends MaterialLibraryDefinition {

    public MTLMaterialLibrary(final @NotNull URL matLibURL) {
        super(matLibURL);
    }

}
