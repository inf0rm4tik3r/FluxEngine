package de.colibriengine.graphics.material.mtl.loader;

import de.colibriengine.exception.MaterialLoadException;
import de.colibriengine.exception.ModelLoadException;
import de.colibriengine.graphics.material.MaterialLoader;
import de.colibriengine.graphics.material.mtl.MTLMaterial;
import de.colibriengine.graphics.material.mtl.MTLMaterialLibrary;
import de.colibriengine.graphics.material.mtl.MTLMaterialTexture;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;

/**
 * Loader capable of loading .mtl files into an {@link MTLMaterialLibrary}.
 *
 * @see <a href="https://github.com/Alhadis/language-wavefront/blob/master/specs/mtl.rst">MTL-Spec</a>
 */
public class MTLLoader implements MaterialLoader {

    private static final String NEWMTL_DEF = "newmtl ";
    private static final int NEWMTL_DEF_LEN = 7;
    // Ambient color.
    private static final String KA_SPECTRAL_DEF = "Ka spectral ";
    private static final int KA_SPECTRAL_DEF_LEN = 12;
    private static final String KA_XYZ_DEF = "Ka xyz ";
    private static final int KA_XYZ_DEF_LEN = 7;
    private static final String KA_RGB_DEF = "Ka ";
    private static final int KA_RGB_DEF_LEN = 3;
    // Diffuse color.
    private static final String KD_SPECTRAL_DEF = "Kd spectral ";
    private static final int KD_SPECTRAL_DEF_LEN = 12;
    private static final String KD_XYZ_DEF = "Kd xyz ";
    private static final int KD_XYZ_DEF_LEN = 7;
    private static final String KD_RGB_DEF = "Kd ";
    private static final int KD_RGB_DEF_LEN = 3;
    // Specular color.
    private static final String KS_SPECTRAL_DEF = "Ks spectral ";
    private static final int KS_SPECTRAL_DEF_LEN = 12;
    private static final String KS_XYZ_DEF = "Ks xyz ";
    private static final int KS_XYZ_DEF_LEN = 7;
    private static final String KS_RGB_DEF = "Ks ";
    private static final int KS_RGB_DEF_LEN = 3;
    // Transmission filter.
    private static final String TF_SPECTRAL_DEF = "Tf spectral ";
    private static final int TF_SPECTRAL_DEF_LEN = 12;
    private static final String TF_XYZ_DEF = "Tf xyz ";
    private static final int TF_XYZ_DEF_LEN = 7;
    private static final String TF_RGB_DEF = "Tf ";
    private static final int TF_RGB_DEF_LEN = 3;
    // Illumination model.
    private static final String ILLUM_DEF = "illum ";
    private static final int ILLUM_DEF_LEN = 6;
    // Dissolve factors.
    private static final String DISSOLVE_HALO_DEF = "d -halo ";
    private static final int DISSOLVE_HALO_DEF_LEN = 8;
    private static final String DISSOLVE_DEF = "d ";
    private static final int DISSOLVE_DEF_LEN = 2;
    // Specular exponent.
    private static final String NS_DEF = "Ns ";
    private static final int NS_DEF_LEN = 3;
    // Sharpness exponent.
    private static final String SHARPNESS_DEF = "sharpness ";
    private static final int SHARPNESS_DEF_LEN = 10;
    // Optical density.
    private static final String NI_DEF = "Ni ";
    private static final int NI_DEF_LEN = 3;
    // Ambient color texture.
    private static final String MAP_KA_DEF = "map_Ka ";
    private static final int MAP_KA_DEF_LEN = 7;
    // Diffuse color texture.
    private static final String MAP_KD_DEF = "map_Kd ";
    private static final int MAP_KD_DEF_LEN = 7;
    // Specular color texture.
    private static final String MAP_KS_DEF = "map_Ks ";
    private static final int MAP_KS_DEF_LEN = 7;
    // Specular exponent texture.
    private static final String MAP_NS_DEF = "map_Ns ";
    private static final int MAP_NS_DEF_LEN = 7;
    // Dissolve texture.
    private static final String MAP_D_DEF = "map_d ";
    private static final int MAP_D_DEF_LEN = 6;
    // Decal texture.
    private static final String MAP_DECAL_DEF = "decal ";
    private static final int MAP_DECAL_DEF_LEN = 6;
    // Displacement texture.
    private static final String MAP_DISPLACEMENT_DEF = "disp ";
    private static final int MAP_DISPLACEMENT_DEF_LEN = 5;
    // Bump texture.
    private static final String MAP_BUMP_DEF = "bump ";
    private static final int MAP_BUMP_DEF_LEN = 5;
    // Normal texture.
    private static final String MAP_NORMAL_DEF = "norm ";
    private static final int MAP_NORMAL_DEF_LEN = 5;
    // Texture antialiasing.
    private static final String MAP_AA_DEF = "map_aa ";
    private static final int MAP_AA_DEF_LEN = 7;

    private static final String PART_DELIMITER = " ";

    private MTLMaterial currentMaterial;

    /**
     * Constructs a new MTLLoader.
     */
    public MTLLoader() {
        reset();
    }

    /**
     * Resets this loader back to its initial state.
     * This is necessary if we want to reuse a particular loader instance.
     */
    public void reset() {
        currentMaterial = null;
    }

    /**
     * Load an .mtl file and return an instance of {@link MTLMaterialLibrary}.
     * Referenced textures will not be loaded!
     *
     * @param matLibURL The URL which references the file to load.
     * @return An {@link MTLMaterialLibrary} object filled with the material definitions.
     * @throws MaterialLoadException If the referenced file could not be loaded / parsed.
     */
    public @NotNull MTLMaterialLibrary load(
            @NotNull URL matLibURL,
            @NotNull String relativePath
    ) throws MaterialLoadException {
        System.out.println("MTLLoader#load: \"" + matLibURL.getPath() + "\"");

        // Create a new material library instance.
        final MTLMaterialLibrary lib = new MTLMaterialLibrary(matLibURL);

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(matLibURL.openStream()))) {
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {

                // Remove any leading and trailing whitespaces from the line.
                line = line.trim();

                // Load a "newmtl" definition.
                if (line.startsWith(NEWMTL_DEF)) {
                    parseMaterialStart(lib, line.substring(NEWMTL_DEF_LEN).trim());
                }

                // Load a "Ka spectral" definition.
                else if (line.startsWith(KA_SPECTRAL_DEF)) {
                    parseKaSpectral(lib, line.substring(KA_SPECTRAL_DEF_LEN).trim());
                }
                // Load a "Ka xyz" definition.
                else if (line.startsWith(KA_XYZ_DEF)) {
                    parseKaXYZ(lib, line.substring(KA_XYZ_DEF_LEN).trim());
                }
                // Load a "Ka rgb" definition.
                else if (line.startsWith(KA_RGB_DEF)) {
                    parseKaRGB(lib, line.substring(KA_RGB_DEF_LEN).trim());
                }

                // Load a "Kd spectral" definition.
                else if (line.startsWith(KD_SPECTRAL_DEF)) {
                    parseKdSpectral(lib, line.substring(KD_SPECTRAL_DEF_LEN).trim());
                }
                // Load a "Kd xyz" definition.
                else if (line.startsWith(KD_XYZ_DEF)) {
                    parseKdXYZ(lib, line.substring(KD_XYZ_DEF_LEN).trim());
                }
                // Load a "Kd rgb" definition.
                else if (line.startsWith(KD_RGB_DEF)) {
                    parseKdRGB(lib, line.substring(KD_RGB_DEF_LEN).trim());
                }

                // Load a "Ks spectral" definition.
                else if (line.startsWith(KS_SPECTRAL_DEF)) {
                    parseKsSpectral(lib, line.substring(KS_SPECTRAL_DEF_LEN).trim());
                }
                // Load a "Ks xyz" definition.
                else if (line.startsWith(KS_XYZ_DEF)) {
                    parseKsXYZ(lib, line.substring(KS_XYZ_DEF_LEN).trim());
                }
                // Load a "Ks rgb" definition.
                else if (line.startsWith(KS_RGB_DEF)) {
                    parseKsRGB(lib, line.substring(KS_RGB_DEF_LEN).trim());
                }

                // Load a "Tf spectral" definition.
                else if (line.startsWith(TF_SPECTRAL_DEF)) {
                    parseTfSpectral(lib, line.substring(TF_SPECTRAL_DEF_LEN).trim());
                }
                // Load a "Tf xyz" definition.
                else if (line.startsWith(TF_XYZ_DEF)) {
                    parseTfXYZ(lib, line.substring(TF_XYZ_DEF_LEN).trim());
                }
                // Load a "Tf rgb" definition.
                else if (line.startsWith(TF_RGB_DEF)) {
                    parseTfRGB(lib, line.substring(TF_RGB_DEF_LEN).trim());
                }

                // Illumination model definition.
                else if (line.startsWith(ILLUM_DEF)) {
                    parseIllum(lib, line.substring(ILLUM_DEF_LEN).trim());
                }

                // Load a "d -halo" definition.
                else if (line.startsWith(DISSOLVE_HALO_DEF)) {
                    parseDissolveHalo(lib, line.substring(DISSOLVE_HALO_DEF_LEN).trim());
                }
                // Load a "d" definition.
                else if (line.startsWith(DISSOLVE_DEF)) {
                    parseDissolve(lib, line.substring(DISSOLVE_DEF_LEN).trim());
                }

                // Load a "Ns" definition.
                else if (line.startsWith(NS_DEF)) {
                    parseNs(lib, line.substring(NS_DEF_LEN).trim());
                }

                // Load a "sharpness" definition.
                else if (line.startsWith(SHARPNESS_DEF)) {
                    parseSharpness(lib, line.substring(SHARPNESS_DEF_LEN).trim());
                }

                // Load a "Ni" definition.
                else if (line.startsWith(NI_DEF)) {
                    parseNi(lib, line.substring(NI_DEF_LEN).trim());
                }

                // Load a "map_Ka" definition.
                else if (line.startsWith(MAP_KA_DEF)) {
                    parseMapKa(lib, relativePath, line.substring(MAP_KA_DEF_LEN).trim());
                }
                // Load a "map_Kd" definition.
                else if (line.startsWith(MAP_KD_DEF)) {
                    parseMapKd(lib, relativePath, line.substring(MAP_KD_DEF_LEN).trim());
                }
                // Load a "map_Ks" definition.
                else if (line.startsWith(MAP_KS_DEF)) {
                    parseMapKs(lib, relativePath, line.substring(MAP_KS_DEF_LEN).trim());
                }
                // Load a "map_Ns" definition.
                else if (line.startsWith(MAP_NS_DEF)) {
                    parseMapNs(lib, relativePath, line.substring(MAP_NS_DEF_LEN).trim());
                }
                // Load a "map_d" definition.
                else if (line.startsWith(MAP_D_DEF)) {
                    parseMapD(lib, relativePath, line.substring(MAP_D_DEF_LEN).trim());
                }

                // Load a "decal" definition.
                else if (line.startsWith(MAP_DECAL_DEF)) {
                    parseMapDecal(lib, relativePath, line.substring(MAP_DECAL_DEF_LEN).trim());
                }
                // Load a "disp" definition.
                else if (line.startsWith(MAP_DISPLACEMENT_DEF)) {
                    parseMapDisplacement(lib, relativePath, line.substring(MAP_DISPLACEMENT_DEF_LEN).trim());
                }
                // Load a "bump" definition.
                else if (line.startsWith(MAP_BUMP_DEF)) {
                    parseMapBump(lib, relativePath, line.substring(MAP_BUMP_DEF_LEN).trim());
                }
                // Load a "norm" definition.
                else if (line.startsWith(MAP_NORMAL_DEF)) {
                    parseMapNormal(lib, relativePath, line.substring(MAP_NORMAL_DEF_LEN).trim());
                }

                // Load a "map_d" definition.
                else if (line.startsWith(MAP_AA_DEF)) {
                    parseMapAA(lib, line.substring(MAP_AA_DEF_LEN).trim());
                }

            }
        } catch (final IOException e) {
            throw new ModelLoadException("The .obj file could not be accessed.", e);
        }

        // Save the currently opened material.
        // It would otherwise not be stored, and may be part of the next object...
        saveCurrentMaterial(lib);

        // ..if this object does not get reset.
        // This object must be resetted, so that this method can be called again whiteout side effects.
        reset();

        return lib;
    }

    // TODO: remove whe possible
    private void ensureCurrentMaterialExists() {
        /*
        if (currentMaterial == null) {
            currentMaterial = new MTLMaterial();
        }
        */
        Objects.requireNonNull(currentMaterial);
    }

    private void saveCurrentMaterial(final @NotNull MTLMaterialLibrary lib) {
        if (currentMaterial != null) {
            lib.addMaterialDefinition(currentMaterial);
        }
    }

    private void parseMaterialStart(@NotNull MTLMaterialLibrary lib,
                                    @NotNull String line) {
        saveCurrentMaterial(lib);
        currentMaterial = null;
        currentMaterial = new MTLMaterial(line);
    }

    private void parseKaSpectral(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        throw new UnsupportedOperationException("\"Ka spectral\" definitions are not supported.");
    }

    private void parseKaXYZ(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        throw new UnsupportedOperationException("\"Ka xyz\" definitions are not supported.");
    }

    private void parseKaRGB(@NotNull MTLMaterialLibrary lib,
                            @NotNull String line) {
        ensureCurrentMaterialExists();

        final String[] parts = line.split(PART_DELIMITER);
        final int partsLength = parts.length;

        if (!(partsLength == 1 || partsLength == 3)) {
            throw new RuntimeException(
                    "Invalid amount of arguments at: \"Ka \" definition. Only 1 or 3 are allowed, but found: " +
                            partsLength + " " + Arrays.toString(parts)
            );
        }

        // Only R (red) got specified. Assuming G (green) nd B (blue) are equal to R.
        if (partsLength == 1) {
            currentMaterial.getAmbientColor().set(
                    Float.parseFloat(parts[0]),
                    Float.parseFloat(parts[0]),
                    Float.parseFloat(parts[0])
            );
        }

        // R, G and B given.
        currentMaterial.getAmbientColor().set(
                Float.parseFloat(parts[0]),
                Float.parseFloat(parts[1]),
                Float.parseFloat(parts[2])
        );
    }

    private void parseKdSpectral(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        throw new UnsupportedOperationException("\"Kd spectral\" definitions are not supported.");
    }

    private void parseKdXYZ(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        throw new UnsupportedOperationException("\"Kd xyz\" definitions are not supported.");
    }

    private void parseKdRGB(@NotNull MTLMaterialLibrary lib,
                            @NotNull String line) {
        ensureCurrentMaterialExists();

        final String[] parts = line.split(PART_DELIMITER);
        final int partsLength = parts.length;

        if (!(partsLength == 1 || partsLength == 3)) {
            throw new RuntimeException(
                    "Invalid amount of arguments at: \"Kd \" definition. Only 1 or 3 are allowed, but found: " +
                            partsLength + " " + Arrays.toString(parts)
            );
        }

        // Only R (red) got specified. Assuming G (green) nd B (blue) are equal to R.
        if (partsLength == 1) {
            currentMaterial.getDiffuseColor().set(
                    Float.parseFloat(parts[0]),
                    Float.parseFloat(parts[0]),
                    Float.parseFloat(parts[0])
            );
        }

        // R, G and B given.
        currentMaterial.getDiffuseColor().set(
                Float.parseFloat(parts[0]),
                Float.parseFloat(parts[1]),
                Float.parseFloat(parts[2])
        );
    }

    private void parseKsSpectral(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        throw new UnsupportedOperationException("\"Ks spectral\" definitions are not supported.");
    }

    private void parseKsXYZ(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        throw new UnsupportedOperationException("\"Ks xyz\" definitions are not supported.");
    }

    private void parseKsRGB(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        ensureCurrentMaterialExists();

        final String[] parts = line.split(PART_DELIMITER);
        final int partsLength = parts.length;

        if (!(partsLength == 1 || partsLength == 3)) {
            throw new RuntimeException(
                    "Invalid amount of arguments at: \"Ks \" definition. Only 1 or 3 are allowed, but found: " +
                            partsLength + " " + Arrays.toString(parts)
            );
        }

        // Only R (red) got specified. Assuming G (green) nd B (blue) are equal to R.
        if (partsLength == 1) {
            currentMaterial.getSpecularColor().set(
                    Float.parseFloat(parts[0]),
                    Float.parseFloat(parts[0]),
                    Float.parseFloat(parts[0])
            );
        }

        // R, G and B given.
        currentMaterial.getSpecularColor().set(
                Float.parseFloat(parts[0]),
                Float.parseFloat(parts[1]),
                Float.parseFloat(parts[2])
        );
    }

    private void parseTfSpectral(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        throw new UnsupportedOperationException("\"Tf spectral\" definitions are not supported.");
    }

    private void parseTfXYZ(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        throw new UnsupportedOperationException("\"Tf xyz\" definitions are not supported.");
    }

    private void parseTfRGB(@NotNull MTLMaterialLibrary lib,
                            @NotNull String line) {
        ensureCurrentMaterialExists();

        final String[] parts = line.split(PART_DELIMITER);
        final int partsLength = parts.length;

        if (!(partsLength == 1 || partsLength == 3)) {
            throw new RuntimeException(
                    "Invalid amount of arguments at: \"Tf \" definition. Only 1 or 3 are allowed, but found: " +
                            partsLength + " " + Arrays.toString(parts)
            );
        }

        // Only R (red) got specified. Assuming G (green) nd B (blue) are equal to R.
        if (partsLength == 1) {
            currentMaterial.getTransmissionFilter().set(
                    Float.parseFloat(parts[0]),
                    Float.parseFloat(parts[0]),
                    Float.parseFloat(parts[0])
            );
        }

        // R, G and B given.
        currentMaterial.getTransmissionFilter().set(
                Float.parseFloat(parts[0]),
                Float.parseFloat(parts[1]),
                Float.parseFloat(parts[2])
        );
    }

    private void parseIllum(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        ensureCurrentMaterialExists();

        final String illuminationModelIDString = line.replace("illum_", "");
        final int illuminationModelID = Integer.parseInt(illuminationModelIDString);

        currentMaterial.setIlluminationModelID(illuminationModelID);
    }

    private void parseDissolveHalo(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setDissolveViewAngleFactor(Float.parseFloat(line));
    }

    private void parseDissolve(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setDissolveFactor(Float.parseFloat(line));
    }

    private void parseNs(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setSpecularExponent(Float.parseFloat(line));
    }

    private void parseSharpness(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setSharpness(Float.parseFloat(line));
    }

    private void parseNi(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setOpticalDensity(Float.parseFloat(line));
    }

    private void parseMapKa(final @NotNull MTLMaterialLibrary lib, @NotNull String relativePath, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setAmbientColorTexture(createTextureInfo(relativePath, line));
    }

    private void parseMapKd(final @NotNull MTLMaterialLibrary lib, @NotNull String relativePath, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setDiffuseColorTexture(createTextureInfo(relativePath, line));
    }

    private void parseMapKs(final @NotNull MTLMaterialLibrary lib, @NotNull String relativePath, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setSpecularColorTexture(createTextureInfo(relativePath, line));
    }

    private void parseMapNs(final @NotNull MTLMaterialLibrary lib, @NotNull String relativePath, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setSpecularExponentTexture(createTextureInfo(relativePath, line));
    }

    private void parseMapD(final @NotNull MTLMaterialLibrary lib, @NotNull String relativePath, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setDissolveTexture(createTextureInfo(relativePath, line));
    }

    private void parseMapDecal(final @NotNull MTLMaterialLibrary lib, @NotNull String relativePath, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setDecalTexture(createTextureInfo(relativePath, line));
    }

    private void parseMapDisplacement(final @NotNull MTLMaterialLibrary lib, @NotNull String relativePath, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setDisplacementTexture(createTextureInfo(relativePath, line));
    }

    private void parseMapBump(final @NotNull MTLMaterialLibrary lib, @NotNull String relativePath, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setBumpTexture(createTextureInfo(relativePath, line));
    }

    private void parseMapNormal(final @NotNull MTLMaterialLibrary lib, @NotNull String relativePath, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setNormalTexture(createTextureInfo(relativePath, line));
    }

    private void parseMapAA(final @NotNull MTLMaterialLibrary lib, final @NotNull String line) {
        ensureCurrentMaterialExists();
        currentMaterial.setTextureAA(Boolean.parseBoolean(line));
    }

    private @NotNull MTLMaterialTexture createTextureInfo(@NotNull String relativePath,
                                                          @NotNull String line) {
        final String[] parts = line.split(PART_DELIMITER);
        final int partsLength = parts.length;

        if (partsLength == 0) {
            throw new RuntimeException(
                    "Invalid amount of arguments at texture definition. Needs at least one. Found: 0 in \"" + line + "\""
            );
        }
        String path = parts[partsLength - 1];

        final int lastSlash = relativePath.lastIndexOf("/");
        final String pathAbsolute = relativePath.substring(0, lastSlash + 1) + path;

        final MTLMaterialTexture texture = new MTLMaterialTexture();
        texture.setTexturePath(pathAbsolute);

        return texture;
    }

}
