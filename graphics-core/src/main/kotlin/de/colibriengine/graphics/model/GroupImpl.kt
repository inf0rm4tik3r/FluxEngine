package de.colibriengine.graphics.model

import de.colibriengine.graphics.rendering.PrimitiveMode

/**
 * Only used in ModelDefinition's to define models. Not in actual rendering.
 * The size of the buffer objects created for this group depend only on the amount of data stored in this group.
 */
// TODO: Why is the size put in but not updated when adding faces?
// TODO: Why can we not always rely on the amount of faces in the group?
// TODO: Why is it allowed that the primitive mode is changes midway of adding faces?..
class GroupImpl(override val vertexDataSize: Int, override val indexDataSize: Int) : Group {

    /** The name of this group; */
    override var name: String = DEFAULT_GROUP_NAME

    /** The material which should be used for this mesh. */
    override var materialName: String? = null

    /** Defines which primitives this group provides. */
    override var primitiveMode: PrimitiveMode = DEFAULT_PRIMITIVE_MODE

    /** The geometry defining faces of this mesh. */
    override val faceVertexAttributeIndices: MutableList<FaceVertexAttributeIndicesImpl> = ArrayList(DEFUALT_EXPECTED_FACE_AMOUNT)

    override var allowPersistentlyMappedBuffers: Boolean = false

    fun addFace(face: FaceVertexAttributeIndicesImpl) {
        faceVertexAttributeIndices.add(face)
    }

    override fun free() {
        faceVertexAttributeIndices.clear()
    }

    companion object {
        /** The default name for each newly created group. */
        private const val DEFAULT_GROUP_NAME = "default"

        /** The default primitive mode tu use. */
        private val DEFAULT_PRIMITIVE_MODE = PrimitiveMode.TRIANGLES

        private const val DEFUALT_EXPECTED_FACE_AMOUNT = 32

        const val DYNAMIC_SIZE = -1
    }

}
