package de.colibriengine.graphics.model

import de.colibriengine.graphics.model.concrete.*
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

class ModelFactoryImpl(override val meshFactory: MeshFactory) : ModelFactory {

    override fun create(from: AbstractModelDefinition): Model {
        val model = ModelImpl(meshFactory, from.constructedFrom)
        model.createFrom(from)
        return model
    }

    override fun coordinateSystem(
        start: Vec3fAccessor,
        min: Float,
        max: Float,
        subdivisionSmall: Float,
        subdivisionBig: Float
    ): CoordinateSystem {
        return CoordinateSystemImpl(start, min, max, subdivisionSmall, subdivisionBig, meshFactory)
    }

    override fun circle(axisA: Vec3fAccessor, axisB: Vec3fAccessor, radius: Float, outerVertexCount: UInt): Circle {
        return CircleImpl.create(axisA, axisB, radius, outerVertexCount.toInt(), meshFactory)
    }

    override fun quad(direction: QuadDirection, alignment: QuadAlignment): QuadImpl {
        return QuadImpl.newInstance(direction, alignment, meshFactory)
    }

    override fun quad(direction: QuadDirection, alignment: QuadAlignment, textureRepetitions: Float): QuadImpl {
        return QuadImpl.newInstance(direction, alignment, textureRepetitions, meshFactory)
    }

}
