package de.colibriengine.graphics.model

import de.colibriengine.graphics.material.MaterialLibrary
import java.net.URL
import java.util.function.Consumer

/**
 * Represents a loaded model. Provides the functionality of drawing this model. Takes the context of the parent window
 * in account to only use available OpenGL features.
 */
open class ModelImpl @JvmOverloads constructor(
    private val meshFactory: MeshFactory,
    override var constructedFrom: URL? = null
) : Model {

    private var _active = false
    override val active: Boolean
        get() = _active

    private val _meshes: MutableList<Mesh> = ArrayList(4)
    override val meshes: List<Mesh>
        get() = _meshes

    private var materialLibraries: MutableList<MaterialLibrary>? = null

    /** @minContextRequired 3.3 */
    override fun createFrom(modelDefinition: AbstractModelDefinition) {
        check(!active) { "Do not call createFrom if the model is already active." }
        // Update the constructedFrom parameter.
        constructedFrom = modelDefinition.constructedFrom
        materialLibraries = modelDefinition.matLibs
        for (group in modelDefinition.groups) {
            addMesh(meshFactory.createFrom(group, modelDefinition))
        }
        _active = true
    }

    /** @minContextRequired 3.3 */
    override fun render() {
        for (mesh in meshes) {
            mesh.render()
        }
    }

    /** Releases all meshes. */
    override fun release() {
        if (materialLibraries != null) {
            materialLibraries!!.forEach(Consumer { obj: MaterialLibrary -> obj.free() })
            materialLibraries!!.clear()
        }
        _meshes.forEach(Consumer { obj: Mesh -> obj.release() })
        _meshes.clear()
        _active = false
    }

    fun addMesh(mesh: Mesh) {
        _meshes.add(mesh)
    }

}
