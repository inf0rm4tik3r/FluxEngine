package de.colibriengine.graphics.model;

import de.colibriengine.math.vector.vec2f.StdVec2f;
import de.colibriengine.math.vector.vec2f.Vec2f;
import de.colibriengine.math.vector.vec2f.Vec2fFactory;
import de.colibriengine.math.vector.vec3f.StdVec3f;
import de.colibriengine.math.vector.vec3f.Vec3f;
import de.colibriengine.math.vector.vec3f.Vec3fFactory;
import de.colibriengine.math.vector.vec4f.Vec4f;
import de.colibriengine.math.vector.vec4f.Vec4fFactory;
import de.colibriengine.buffers.BufferStorable;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;

public class Vertex implements BufferStorable {

    public static final int BYTES = Vec3f.BYTES + Vec3f.BYTES + Vec2f.BYTES + Vec4f.BYTES;

    private final VertexIndices indexSet;

    private Vec3f position;
    private Vec3f normal;
    private Vec2f texCoord;
    private Vec4f color;

    private int textureID;

    public Vertex(
            final VertexIndices indexSet,
            final Vec3f position, final Vec3f normal, final Vec2f texCoord, final Vec4f color
    ) {
        this.indexSet = indexSet;

        this.position = position;
        this.normal = normal;
        this.texCoord = texCoord;
        this.color = color;
    }

    public void release(Vec2fFactory vec2fFactory, Vec3fFactory vec3fFactory, Vec4fFactory vec4fFactory) {
        vec3fFactory.free(position);
        vec3fFactory.free(normal);
        vec2fFactory.free(texCoord);
        vec4fFactory.free(color);
    }

    @Override
    public String toString() {
        return "Vertex: " +
                "Position = " + position
                + ", Normal = " + normal
                + ", TexCoord = " + texCoord
                + ", Color = " + color
                + ", TextureID = " + textureID;
    }

    /**
     * Provides a hashCode, that represents the Vertex.
     * If two {@link Vertex} objects are equal according to the equals() method, they must have the same
     * hashCode() value!
     */
    @Override
    public int hashCode() {
        //Start with a basic hashcode.
        int hashCode = 17;

        //Compute a single int hashcode for every field and combine it with the existing hashcode.
        hashCode = 31 * hashCode + position.hashCode();
        hashCode = 31 * hashCode + normal.hashCode();
        hashCode = 31 * hashCode + texCoord.hashCode();
        hashCode = 31 * hashCode + color.hashCode();
        hashCode = 31 * hashCode + textureID;

        //Return the final hashcode.
        return hashCode;
    }

    /**
     * Provides a comparison with the passed object.
     * The given object has to be an object of {@link Vertex}. Otherwise, the check will fail.
     * If the memory addresses are the same, the result of the check is generally true.
     *
     * @return True if {@code object} and {@code this} contain the same values.
     */
    @Override
    public boolean equals(final Object object) {
        if (!(object instanceof Vertex)) {
            return false;
        }
        if (object == this) {
            return true;
        }

        final Vertex other = (Vertex) object;

        return (
                this.position.equals(other.position) &&
                        this.normal.equals(other.normal) &&
                        this.texCoord.equals(other.texCoord) &&
                        this.color.equals(other.color) &&
                        this.textureID == other.textureID
        );
    }

    @Override
    public ByteBuffer storeIn(@NotNull final ByteBuffer buffer) {
        position.storeIn(buffer);
        normal.storeIn(buffer);
        texCoord.storeIn(buffer);
        color.storeIn(buffer);
        //buffer.putInt(textureID);
        return buffer;
    }

    @Override
    public FloatBuffer storeIn(@NotNull final FloatBuffer buffer) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("This object may only be stored in a ByteBuffer!");
    }

    @Override
    public DoubleBuffer storeIn(@NotNull final DoubleBuffer buffer) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("This object may only be stored in a ByteBuffer!");
    }

    /* Getter and Setter */

    public VertexIndices getIndexSet() {
        return indexSet;
    }

    public Vec3f getPosition() {
        return position;
    }

    public void setPosition(final StdVec3f position) {
        this.position = position;
    }

    public Vec3f getNormal() {
        return normal;
    }

    public void setNormal(final StdVec3f normal) {
        this.normal = normal;
    }

    public Vec2f getTexCoord() {
        return texCoord;
    }

    public void setTexCoord(final StdVec2f texCoord) {
        this.texCoord = texCoord;
    }

    public Vec4f getColor() {
        return color;
    }

    public void setColor(final Vec4f color) {
        this.color = color;
    }

    public int getTextureID() {
        return textureID;
    }

    public void setTextureID(final int textureID) {
        this.textureID = textureID;
    }

}
