package de.colibriengine.graphics.model

data class VertexIndices(

    val positionIndex: Int,
    val normalIndex: Int,
    val texCoordIndex: Int,
    val colorIndex: Int

)
