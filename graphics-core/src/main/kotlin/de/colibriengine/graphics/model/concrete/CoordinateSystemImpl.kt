package de.colibriengine.graphics.model.concrete

import de.colibriengine.graphics.model.*
import de.colibriengine.graphics.rendering.PrimitiveMode
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

class CoordinateSystemImpl(
    start: Vec3fAccessor,
    min: Float, max: Float,
    subdivisionSmall: Float, subdivisionBig: Float, // TODO: use subdivisionBig!
    meshFactory: MeshFactory
) : ModelImpl(meshFactory, null), CoordinateSystem {

    init {
        val modelDefinition = CustomModelDefinition()
        val group = GroupImpl(GroupImpl.DYNAMIC_SIZE, GroupImpl.DYNAMIC_SIZE)
        group.primitiveMode = PrimitiveMode.LINES
        modelDefinition.addGroup(group)
        val subBigLineLength = (max - min) / 20.0f
        val subSmallLineLength = subBigLineLength / 4.0f
        var index = 1

        // x base line
        modelDefinition.addVertex(StdVec3f().set(start).plus(min, 0f, 0f))
        modelDefinition.addVertex(StdVec3f().set(start).plus(max, 0f, 0f))
        group.addFace(FaceVertexAttributeIndicesImpl.line(index, index + 1))
        index += 2

        // y base line
        modelDefinition.addVertex(StdVec3f().set(start).plus(0f, min, 0f))
        modelDefinition.addVertex(StdVec3f().set(start).plus(0f, max, 0f))
        group.addFace(FaceVertexAttributeIndicesImpl.line(index, index + 1))
        index += 2

        // z base line
        modelDefinition.addVertex(StdVec3f().set(start).plus(0f, 0f, min))
        modelDefinition.addVertex(StdVec3f().set(start).plus(0f, 0f, max))
        group.addFace(FaceVertexAttributeIndicesImpl.line(index, index + 1))
        index += 2

        // +x axis
        val tmp: Vec3f = StdVec3f()
        tmp.set(start).plus(0f, subSmallLineLength, 0f)
        run {
            var i = start.x
            while (i < Math.abs(max)) {
                tmp.plus(subdivisionSmall, 0f, 0f) // do next step
                modelDefinition.addVertex(StdVec3f().set(tmp)) // Top
                tmp.plus(0f, -2 * subSmallLineLength, 0f)
                modelDefinition.addVertex(StdVec3f().set(tmp)) // Bottom
                tmp.plus(0f, subSmallLineLength, subSmallLineLength)
                modelDefinition.addVertex(StdVec3f().set(tmp)) // Front
                tmp.plus(0f, 0f, -2 * subSmallLineLength)
                modelDefinition.addVertex(StdVec3f().set(tmp)) // Back
                tmp.plus(0f, subSmallLineLength, subSmallLineLength)
                group.addFace(FaceVertexAttributeIndicesImpl.line(index, index + 1))
                group.addFace(FaceVertexAttributeIndicesImpl.line(index + 2, index + 3))
                index += 4
                i += subdivisionSmall
            }
        }

        // +y axis
        tmp.set(start).plus(subSmallLineLength, 0f, 0f)
        var i = start.y
        while (i < Math.abs(max)) {
            tmp.plus(0f, subdivisionSmall, 0f) // do next step
            modelDefinition.addVertex(StdVec3f().set(tmp)) // Right
            tmp.plus(-2 * subSmallLineLength, 0f, 0f)
            modelDefinition.addVertex(StdVec3f().set(tmp)) // Left
            tmp.plus(subSmallLineLength, 0f, subSmallLineLength)
            modelDefinition.addVertex(StdVec3f().set(tmp)) // Front
            tmp.plus(0f, 0f, -2 * subSmallLineLength)
            modelDefinition.addVertex(StdVec3f().set(tmp)) // Back
            tmp.plus(subSmallLineLength, 0f, subSmallLineLength)
            group.addFace(FaceVertexAttributeIndicesImpl.line(index, index + 1))
            group.addFace(FaceVertexAttributeIndicesImpl.line(index + 2, index + 3))
            index += 4
            i += subdivisionSmall
        }
        createFrom(modelDefinition)
        // modelDefinition.free(null, vec3fFactory, null);
    }

}
