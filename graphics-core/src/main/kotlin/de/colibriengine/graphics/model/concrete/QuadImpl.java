package de.colibriengine.graphics.model.concrete;

import de.colibriengine.graphics.model.*;
import de.colibriengine.graphics.rendering.PrimitiveMode;
import de.colibriengine.math.vector.vec2f.StdVec2fFactory;
import de.colibriengine.math.vector.vec2f.Vec2f;
import de.colibriengine.math.vector.vec2f.Vec2fFactory;
import de.colibriengine.math.vector.vec3f.StdVec3fFactory;
import de.colibriengine.math.vector.vec3f.Vec3fFactory;
import de.colibriengine.math.vector.vec4f.StdVec4fFactory;
import de.colibriengine.math.vector.vec4f.Vec4fFactory;
import org.jetbrains.annotations.NotNull;

/**
 * Represents a basic quad (made up of two triangles).
 */
public class QuadImpl extends ModelImpl implements Quad {

    public static final int VERTICES = 4;
    public static final int INDICES = 6;

    private static final float ZERO_F = 0.0f;
    private static final float ONE_F = 1.0f;

    private final Vec2fFactory vec2fFactory = new StdVec2fFactory();
    private final Vec3fFactory vec3fFactory = new StdVec3fFactory();
    private final Vec4fFactory vec4fFactory = new StdVec4fFactory();

    /**
     * Constructs a unit quad. Ready for rendering.
     */
    private QuadImpl(final @NotNull MeshFactory meshFactory) {
        super(meshFactory, null);
    }

    public static @NotNull QuadImpl newInstance(
            final @NotNull QuadDirection direction,
            final @NotNull QuadAlignment alignment,
            final @NotNull MeshFactory meshFactory
    ) {
        final @NotNull QuadImpl quad = new QuadImpl(meshFactory);
        quad.init(direction, alignment, ONE_F);
        return quad;
    }

    public static @NotNull QuadImpl newInstance(
            final @NotNull QuadDirection direction,
            final @NotNull QuadAlignment alignment,
            final float textureRepetitions,
            final @NotNull MeshFactory meshFactory
    ) {
        final @NotNull QuadImpl quad = new QuadImpl(meshFactory);
        quad.init(direction, alignment, textureRepetitions);
        return quad;
    }

    public static @NotNull QuadImpl newInstance(
            final @NotNull QuadDirection direction,
            final @NotNull QuadAlignment alignment,
            final @NotNull Vec2f uvBottomRight,
            final @NotNull Vec2f uvTopRight,
            final @NotNull Vec2f uvTopLeft,
            final @NotNull Vec2f uvBottomLeft,
            final @NotNull MeshFactory meshFactory
    ) {
        final @NotNull QuadImpl quad = new QuadImpl(meshFactory);
        quad.init(direction, alignment, uvBottomRight, uvTopRight, uvTopLeft, uvBottomLeft);
        return quad;
    }


    private void init(
            final @NotNull QuadDirection direction,
            final @NotNull QuadAlignment alignment,
            final float textureRepetitions
    ) {
        init(
                direction, alignment,
                vec2fFactory.acquireDirty().set(textureRepetitions, ZERO_F), // bottom right
                vec2fFactory.acquireDirty().set(textureRepetitions, textureRepetitions), // top right
                vec2fFactory.acquireDirty().set(ZERO_F, textureRepetitions), // top left
                vec2fFactory.acquireDirty().set(ZERO_F, ZERO_F) // bottom left
        );
    }

    private void init(
            final @NotNull QuadDirection direction,
            final @NotNull QuadAlignment alignment,
            final @NotNull Vec2f uvBottomRight,
            final @NotNull Vec2f uvTopRight,
            final @NotNull Vec2f uvTopLeft,
            final @NotNull Vec2f uvBottomLeft
    ) {
        final CustomModelDefinition modelDefinition = new CustomModelDefinition();

        final float pos = alignment == QuadAlignment.CENTERED ? +0.5f : 1.0f;
        final float neg = alignment == QuadAlignment.CENTERED ? -0.5f : 0.0f;

        switch (direction) {
            case XY -> {
                switch (alignment) {
                    case CENTERED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, neg, ZERO_F)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, pos, ZERO_F)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, pos, ZERO_F)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, neg, ZERO_F)); // bottom left
                    }
                    case TOP_LEFT_ALIGNED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, -pos, ZERO_F)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, neg, ZERO_F)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, neg, ZERO_F)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, -pos, ZERO_F)); // bottom left
                    }
                    case TOP_RIGHT_ALIGNED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, -pos, ZERO_F)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, neg, ZERO_F)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(-pos, neg, ZERO_F)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(-pos, -pos, ZERO_F)); // bottom left
                    }
                    case BOTTOM_LEFT_ALIGNED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, neg, ZERO_F)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, pos, ZERO_F)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, pos, ZERO_F)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, neg, ZERO_F)); // bottom left
                    }
                    case BOTTOM_RIGHT_ALIGNED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, neg, ZERO_F)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, pos, ZERO_F)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(-pos, pos, ZERO_F)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(-pos, neg, ZERO_F)); // bottom left
                    }
                }
                modelDefinition.addVertexNormal(vec3fFactory.acquireDirty()
                        .set(StdVec3fFactory.Companion.getUNIT_Z_AXIS()));
            }
            case XZ -> {
                switch (alignment) {
                    case CENTERED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, ZERO_F, pos)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, ZERO_F, neg)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, neg)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, pos)); // bottom left
                    }
                    case TOP_LEFT_ALIGNED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, ZERO_F, pos)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, ZERO_F, neg)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, neg)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, pos)); // bottom left
                    }
                    case TOP_RIGHT_ALIGNED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, pos)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, neg)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(-pos, ZERO_F, neg)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(-pos, ZERO_F, pos)); // bottom left
                    }
                    case BOTTOM_LEFT_ALIGNED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, ZERO_F, neg)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(pos, ZERO_F, -pos)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, -pos)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, neg)); // bottom left
                    }
                    case BOTTOM_RIGHT_ALIGNED -> {
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, neg)); // bottom right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(neg, ZERO_F, -pos)); // top right
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(-pos, ZERO_F, -pos)); // top left
                        modelDefinition.addVertex(vec3fFactory.acquireDirty().set(-pos, ZERO_F, neg)); // bottom left
                    }
                }
                modelDefinition.addVertexNormal(vec3fFactory.acquireDirty()
                        .set(StdVec3fFactory.Companion.getUNIT_Y_AXIS()));
            }
            default -> throw new RuntimeException("Unknown axis combination (QuadDirection): " + direction);
        }

        modelDefinition.addVertexTexCoord(uvBottomRight); // bottom right
        modelDefinition.addVertexTexCoord(uvTopRight); // top right
        modelDefinition.addVertexTexCoord(uvTopLeft); // top left
        modelDefinition.addVertexTexCoord(uvBottomLeft); // bottom left

        modelDefinition.addVertexColor(vec4fFactory.acquireDirty().set(StdVec4fFactory.Companion.getWHITE()));

        final GroupImpl group = new GroupImpl(VERTICES, INDICES);
        group.setPrimitiveMode(PrimitiveMode.TRIANGLES);
        group.addFace(
                new FaceVertexAttributeIndicesImpl(
                        1, 2, 3,
                        1, 1, 1,
                        1, 2, 3,
                        1, 1, 1)
        );
        group.addFace(
                new FaceVertexAttributeIndicesImpl(
                        1, 3, 4,
                        1, 1, 1,
                        1, 3, 4,
                        1, 1, 1)
        );
        modelDefinition.addGroup(group);

        createFrom(modelDefinition);
        // modelDefinition.free(vec2fFactory, vec3fFactory, vec4fFactory);
    }

}
