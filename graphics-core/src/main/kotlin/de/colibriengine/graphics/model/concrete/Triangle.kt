package de.colibriengine.graphics.model.concrete

import de.colibriengine.graphics.model.*
import de.colibriengine.graphics.rendering.PrimitiveMode
import de.colibriengine.math.vector.vec2f.StdVec2f
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec4f.StdVec4f
import de.colibriengine.math.vector.vec4f.StdVec4fFactory.Companion.WHITE

class Triangle(meshFactory: MeshFactory) : ModelImpl(meshFactory, null) {

    init {
        val modelDefinition = CustomModelDefinition()
        modelDefinition.addVertex(StdVec3f().set(+0.5f, -0.5f, 0.0f))
        modelDefinition.addVertex(StdVec3f().set(+0.0f, +0.5f, 0.0f))
        modelDefinition.addVertex(StdVec3f().set(-0.5f, -0.5f, 0.0f))
        modelDefinition.addVertexTexCoord(StdVec2f().set(1.0f, 0.0f)) // bottom right
        modelDefinition.addVertexTexCoord(StdVec2f().set(0.5f, 1.0f)) // top middle
        modelDefinition.addVertexTexCoord(StdVec2f().set(0.0f, 0.0f)) // bottom left
        modelDefinition.addVertexNormal(StdVec3f().set(+1.0f, -1.0f, 0.0f)) // bottom right
        modelDefinition.addVertexNormal(StdVec3f().set(+0.0f, +1.0f, 0.0f)) // top middle
        modelDefinition.addVertexNormal(StdVec3f().set(-1.0f, -1.0f, 0.0f)) // bottom left
        modelDefinition.addVertexColor(StdVec4f().set(WHITE))
        val group = GroupImpl(VERTICES, INDICES)
        group.primitiveMode = PrimitiveMode.TRIANGLES
        group.addFace(
            FaceVertexAttributeIndicesImpl(
                1, 2, 3,
                1, 2, 3,
                1, 2, 3,
                1, 1, 1
            )
        )
        modelDefinition.addGroup(group)
        createFrom(modelDefinition)
    }

    companion object {
        const val VERTICES = 3
        const val INDICES = 3
    }

}
