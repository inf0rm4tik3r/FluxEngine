package de.colibriengine.graphics.model.fbx;

import org.jetbrains.annotations.NotNull;

public class FBXArrayData<T> {
    
    private final @NotNull T[] array;
    
    public FBXArrayData(final @NotNull T[] array) {
        this.array = array;
    }
    
    public T[] getArray() {
        return array;
    }
    
}
