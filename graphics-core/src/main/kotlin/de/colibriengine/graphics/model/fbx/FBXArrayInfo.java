package de.colibriengine.graphics.model.fbx;

import de.colibriengine.util.ChecksKt;
import org.jetbrains.annotations.NotNull;

public class FBXArrayInfo {

    private final @NotNull FBXArrayType arrayType;

    private final int encoding;

    private final int arrayLength;

    private final int compressedLength;

    public static FBXArrayInfo.Builder builder() {
        return new FBXArrayInfo.Builder();
    }

    public static class Builder {
        private FBXArrayType arrayType;
        private int encoding;
        private boolean encodingSet = false;
        private int arrayLength;
        private boolean arrayLengthSet = false;
        private int compressedLength;
        private boolean compressedLengthSet = false;

        public FBXArrayInfo.Builder setArrayType(final @NotNull FBXArrayType arrayType) {
            this.arrayType = arrayType;
            return this;
        }

        public FBXArrayInfo.Builder setEncoding(final int encoding) {
            this.encoding = encoding;
            encodingSet = true;
            return this;
        }

        public FBXArrayInfo.Builder setArrayLength(final int arrayLength) {
            this.arrayLength = arrayLength;
            arrayLengthSet = true;
            return this;
        }

        public FBXArrayInfo.Builder setCompressedLength(final int compressedLength) {
            this.compressedLength = compressedLength;
            compressedLengthSet = true;
            return this;
        }

        public @NotNull FBXArrayInfo build() {
            ChecksKt.ifNull(arrayType, () -> {
                throw new IllegalStateException("ArrayType was not set!");
            });
            ChecksKt.ifFalse(encodingSet, () -> {
                throw new IllegalStateException("Encoding was not set!");
            });
            ChecksKt.ifFalse(arrayLengthSet, () -> {
                throw new IllegalStateException("ArrayLength was not set!");
            });
            return new FBXArrayInfo(this);
        }
    }

    private FBXArrayInfo(final @NotNull FBXArrayInfo.Builder builder) {
        arrayType = builder.arrayType;
        encoding = builder.encoding;
        arrayLength = builder.arrayLength;
        compressedLength = builder.compressedLength;
    }

    public @NotNull FBXArrayType getArrayType() {
        return arrayType;
    }

    public int getEncoding() {
        return encoding;
    }

    public int getArrayLength() {
        return arrayLength;
    }

    public int getCompressedLength() {
        return compressedLength;
    }

}
