package de.colibriengine.graphics.model.fbx;

import de.colibriengine.logging.LogUtil;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class FBXDocument {

    private final @NotNull FBXHeader header;

    private final @NotNull FBXNode rootNode;

    private final @NotNull FBXFooter footer;

    private static final @NotNull Logger LOG = LogUtil.getLogger(FBXDocument.class);

    public static FBXDocument.Builder builder() {
        return new FBXDocument.Builder();
    }

    public static class Builder {
        private @Nullable FBXHeader header;
        private @Nullable FBXNode rootNode;
        private @Nullable FBXFooter footer;

        public FBXDocument.Builder setHeader(final @NotNull FBXHeader header) {
            this.header = header;
            return this;
        }

        public FBXDocument.Builder setRootNode(final @NotNull FBXNode rootNode) {
            this.rootNode = rootNode;
            return this;
        }

        public FBXDocument.Builder setFooter(final @NotNull FBXFooter footer) {
            this.footer = footer;
            return this;
        }

        public @NotNull FBXDocument build() {
            return new FBXDocument(this);
        }
    }

    private FBXDocument(final @NotNull FBXDocument.Builder builder) {
        assert builder.header != null;
        header = Objects.requireNonNull(builder.header, "Header was not set!");
        rootNode = Objects.requireNonNull(builder.rootNode, "RootNode was not set!");
        footer = Objects.requireNonNull(builder.footer, "Footer was not set!");
    }

    public void print() {
        LOG.info("======= FBX HEADER =======");
        header.print();
        LOG.info("======= FBX ROOT NODE =======");
        rootNode.print();
        LOG.info("======= FBX FOOTER =======");
        footer.print();
    }

    public @NotNull FBXHeader getHeader() {
        return header;
    }

    public @NotNull FBXNode getRootNode() {
        return rootNode;
    }

    public @NotNull FBXFooter getFooter() {
        return footer;
    }

}
