package de.colibriengine.graphics.model.fbx;

import de.colibriengine.logging.LogUtil;

import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class FBXHeader {
    
    private final @NotNull String introduction;
    private final          int    number;
    private final          long   versionNumber;
    
    private static final @NotNull Logger LOG = LogUtil.getLogger(FBXHeader.class);
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static class Builder {
        String  introduction;
        boolean introductionSet  = false;
        int     number;
        boolean numberSet        = false;
        long    versionNumber;
        boolean versionNumberSet = false;
        
        public Builder setIntroduction(final @NotNull String introduction) {
            this.introduction = introduction;
            introductionSet = true;
            return this;
        }
        
        public Builder setNumber(final int number) {
            this.number = number;
            numberSet = true;
            return this;
        }
        
        public Builder setVersionNumber(final long versionNumber) {
            this.versionNumber = versionNumber;
            versionNumberSet = true;
            return this;
        }
        
        public @NotNull FBXHeader build() {
            if (!introductionSet) {
                throw new IllegalStateException("Introduction was not set!");
            }
            if (!numberSet) {
                throw new IllegalStateException("Number was not set!");
            }
            if (!versionNumberSet) {
                throw new IllegalStateException("VersionNumber was not set!");
            }
            return new FBXHeader(introduction, number, versionNumber);
        }
    }
    
    private FBXHeader(final @NotNull String introduction, final int number, final long versionNumber) {
        this.introduction = introduction;
        this.number = number;
        this.versionNumber = versionNumber;
    }
    
    public void print() {
        LOG.info(this);
    }
    
    public @NotNull String getIntroduction() {
        return introduction;
    }
    
    public int getNumber() {
        return number;
    }
    
    public long getVersionNumber() {
        return versionNumber;
    }
    
    @Override
    public String toString() {
        return "Introduction: " + introduction + ", Number: " + number + ", VersionNumber: " + versionNumber;
    }
}
