package de.colibriengine.graphics.model.fbx;

import de.colibriengine.logging.LogUtil;

import java.util.ArrayList;

import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class FBXNode {
    
    private final @NotNull String name;
    
    private final @NotNull ArrayList<FBXPropertyRecord> properties;
    
    private final @NotNull ArrayList<FBXNode> children;
    
    private static final @NotNull Logger LOG = LogUtil.getLogger(FBXNode.class);
    
    public FBXNode(final @NotNull String name, final int numProperties, final int childListLen) {
        this.name = name;
        properties = new ArrayList<>(numProperties);
        children = new ArrayList<>(childListLen);
    }
    
    public void print() {
        LOG.info("Node: \"" + name + "\"");
        LOG.info("Properties: ");
        properties.forEach(FBXPropertyRecord::print);
        LOG.info("Children: ");
        children.forEach(FBXNode::print);
    }
    
    public @NotNull String getName() {
        return name;
    }
    
    public void addProperty(final @NotNull FBXPropertyRecord propertyRecord) {
        properties.add(propertyRecord);
    }
    
    public void addChild(final @NotNull FBXNode child) {
        children.add(child);
    }
    
    public @NotNull ArrayList<FBXPropertyRecord> getProperties() {
        return properties;
    }
    
    public @NotNull ArrayList<FBXNode> getChildren() {
        return children;
    }
    
}
