package de.colibriengine.graphics.model.fbx;

import de.colibriengine.logging.LogUtil;

import java.util.Arrays;

import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class FBXPropertyRecord {
    
    private Object value;
    
    private static final @NotNull Logger LOG = LogUtil.getLogger(FBXPropertyRecord.class);
    
    public void print() {
        LOG.info("Property value: " + (value instanceof Object[] ? Arrays.toString((Object[])value) : value));
    }
    
    public Object getValue() {
        return value;
    }
    
    public void setValue(final Object value) {
        this.value = value;
    }
    
}
