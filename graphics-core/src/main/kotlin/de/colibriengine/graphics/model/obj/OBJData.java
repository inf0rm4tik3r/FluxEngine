package de.colibriengine.graphics.model.obj;

import de.colibriengine.graphics.model.AbstractModelDefinition;

import java.net.URL;

public class OBJData extends AbstractModelDefinition {

    public OBJData(final URL constructedFrom) {
        super(constructedFrom);
    }
    
}
