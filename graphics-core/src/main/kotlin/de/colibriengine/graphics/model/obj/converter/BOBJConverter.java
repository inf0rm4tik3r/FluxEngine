package de.colibriengine.graphics.model.obj.converter;

import de.colibriengine.math.vector.vec2f.StdVec2f;
import de.colibriengine.math.vector.vec2f.Vec2f;
import de.colibriengine.math.vector.vec3f.StdVec3f;
import de.colibriengine.math.vector.vec3f.Vec3f;
import de.colibriengine.graphics.model.FaceVertexAttributeIndicesImpl;
import de.colibriengine.util.ByteOps;
import de.colibriengine.util.FileOps;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Objects;

public final class BOBJConverter {

    private static final byte BOBJ_UNDEFINED_SECTION = -1;
    private static final byte BOBJ_HEADER_SECTION = 0;
    private static final byte BOBJ_V_SECTION = 1;
    private static final byte BOBJ_VN_SECTION = 2;
    private static final byte BOBJ_VT_SECTION = 3;
    private static final byte BOBJ_F_SECTION = 4;

    private byte lastSection;

    private final ArrayList<StdVec3f> vTemp;
    private final ArrayList<StdVec3f> vnTemp;
    private final ArrayList<StdVec2f> vtTemp;
    private final ArrayList<FaceVertexAttributeIndicesImpl> fTemp;

    public static void main(final String[] args) {
        new BOBJConverter();
    }

    private BOBJConverter() {
        // Initialization
        final int initialSize = 256;
        vTemp = new ArrayList<>(initialSize);
        vnTemp = new ArrayList<>(initialSize);
        vtTemp = new ArrayList<>(initialSize);
        fTemp = new ArrayList<>(initialSize);
        lastSection = BOBJ_UNDEFINED_SECTION;

        System.out.println("Converting to BOBJ:");
        convertToBOBJ(
                FileOps.INSTANCE.getFile("res/models/sponza/sponza.mtl", false),
                FileOps.INSTANCE.getFile("res/models/sponza/sponza.bobj", true)
        );

        System.out.println("Converting to OBJ");
        convertToOBJ(
                FileOps.INSTANCE.getFile("res/models/sponza/sponza.bobj", false),
                FileOps.INSTANCE.getFile("res/models/sponza/sponza_reconv.mtl", true)
        );
    }

    private void resetState() {
        vTemp.clear();
        vnTemp.clear();
        vtTemp.clear();
        fTemp.clear();
        lastSection = BOBJ_UNDEFINED_SECTION;
    }

    public final void convertToOBJ(final @NotNull File bobj, final @NotNull File obj) {
        Objects.requireNonNull(bobj);
        Objects.requireNonNull(obj);

        try (InputStream fis = Files.newInputStream(bobj.toPath())) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(Files.newBufferedWriter(obj.toPath()))) {

                final StringBuilder line = new StringBuilder();
                final byte[] sectionInfo = new byte[Byte.BYTES + Integer.BYTES]; // secId(byte) + secLength(int)
                final byte[] vec3Data = new byte[Vec3f.FLOATS * Float.BYTES]; // 12 byte chunk.
                final byte[] vec2Data = new byte[Vec2f.FLOATS * Float.BYTES]; // 8 byte chunk.
                final byte[] faceData = new byte[FaceVertexAttributeIndicesImpl.INTS * Integer.BYTES]; // 36 byte chunk.

                while (fis.read(sectionInfo) != -1) {
                    final int sectionLength = ByteOps.byteArrayToInt(sectionInfo, 1);

                    switch (sectionInfo[0]) {
                        case BOBJ_V_SECTION:
                            // sectionInfo[1] = amount of floats.
                            for (int i = 0; i < sectionLength * Float.BYTES; i += vec3Data.length) {
                                // Its guaranteed that (sectionInfo[1] * Float.BYTES) bytes can be read.
                                //noinspection ResultOfMethodCallIgnored
                                fis.read(vec3Data);

                                line.setLength(0);
                                line.append("v ")
                                        .append(ByteOps.byteArrayToFloat(vec3Data, 0)) // First float.
                                        .append(" ")
                                        .append(ByteOps.byteArrayToFloat(vec3Data, 4)) // Second float.
                                        .append(" ")
                                        .append(ByteOps.byteArrayToFloat(vec3Data, 8)); // Third float.

                                bufferedWriter.write(line.toString());
                                bufferedWriter.newLine();
                            }
                            break;

                        case BOBJ_VN_SECTION:
                            for (int i = 0; i < sectionLength * Float.BYTES; i += vec3Data.length) {
                                //noinspection ResultOfMethodCallIgnored
                                fis.read(vec3Data);

                                line.setLength(0);
                                line.append("vn ")
                                        .append(ByteOps.byteArrayToFloat(vec3Data, 0)) // First float.
                                        .append(" ")
                                        .append(ByteOps.byteArrayToFloat(vec3Data, 4)) // Second float.
                                        .append(" ")
                                        .append(ByteOps.byteArrayToFloat(vec3Data, 8)); // Third float.

                                bufferedWriter.write(line.toString());
                                bufferedWriter.newLine();
                            }
                            break;

                        case BOBJ_VT_SECTION:
                            for (int i = 0; i < sectionLength * Float.BYTES; i += vec2Data.length) {
                                //noinspection ResultOfMethodCallIgnored
                                fis.read(vec2Data);

                                line.setLength(0);
                                line.append("vt ")
                                        .append(ByteOps.byteArrayToFloat(vec2Data, 0)) // First float.
                                        .append(" ")
                                        .append(ByteOps.byteArrayToFloat(vec2Data, 4)); // Second float.

                                bufferedWriter.write(line.toString());
                                bufferedWriter.newLine();
                            }
                            break;

                        case BOBJ_F_SECTION:
                            for (int i = 0; i < sectionLength * Integer.BYTES; i += faceData.length) {
                                //noinspection ResultOfMethodCallIgnored
                                fis.read(faceData);

                                // vertex indices at:   0  4  8
                                // normal indices at:  12 16 20
                                // tex    indices at:  24 28 32
                                line.setLength(0);
                                line.append("f ")
                                        // First vertex.
                                        .append(ByteOps.byteArrayToInt(faceData, 0))
                                        .append("/")
                                        .append(ByteOps.byteArrayToInt(faceData, 24))
                                        .append("/")
                                        .append(ByteOps.byteArrayToInt(faceData, 12))
                                        // Second vertex.
                                        .append(" ")
                                        .append(ByteOps.byteArrayToInt(faceData, 4))
                                        .append("/")
                                        .append(ByteOps.byteArrayToInt(faceData, 28))
                                        .append("/")
                                        .append(ByteOps.byteArrayToInt(faceData, 16))
                                        // Third vertex.
                                        .append(" ")
                                        .append(ByteOps.byteArrayToInt(faceData, 8))
                                        .append("/")
                                        .append(ByteOps.byteArrayToInt(faceData, 32))
                                        .append("/")
                                        .append(ByteOps.byteArrayToInt(faceData, 20));

                                bufferedWriter.write(line.toString());
                                bufferedWriter.newLine();
                            }
                            break;

                        default:
                            throw new RuntimeException("Unknown section id: " + sectionInfo[0]);
                    }
                }
            }
        } catch (final IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public final void convertToBOBJ(final @NotNull File obj, final @NotNull File bobj) {
        Objects.requireNonNull(obj);
        Objects.requireNonNull(bobj);

        resetState();

        String[] splittedLine;

        // Try-With-resources
        try (BufferedReader bufferedReader = new BufferedReader(Files.newBufferedReader(obj.toPath()))) {
            try (OutputStream fos = Files.newOutputStream(bobj.toPath())) {

                for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {

                    splittedLine = line.split(" ");
                    // Convert comments to byte array.
                    if (line.startsWith("# ")) {
                        // Intentionally do nothing.
                    }
                    // Convert the declaration of a material library.
                    else if (line.startsWith("mtllib ")) {
                        // TODO
                    }
                    // Convert the declaration of a material change.
                    else if (line.startsWith("usemtl ")) {
                        // TODO
                    }
                    // Convert a vertex position.
                    else if (line.startsWith("v ")) {
                        // Start v data section.
                        if (!inSection(BOBJ_V_SECTION)) {
                            writeSection(fos, lastSection);
                            lastSection = BOBJ_V_SECTION;
                        }
                        vTemp.add(
                                new StdVec3f().set(
                                        Float.parseFloat(splittedLine[splittedLine.length - 3]),
                                        Float.parseFloat(splittedLine[splittedLine.length - 2]),
                                        Float.parseFloat(splittedLine[splittedLine.length - 1])
                                )
                        );
                    }
                    // Convert a vertex normal.
                    else if (line.startsWith("vn ")) {
                        // Start vn data section.
                        if (!inSection(BOBJ_VN_SECTION)) {
                            writeSection(fos, lastSection);
                            lastSection = BOBJ_VN_SECTION;
                        }

                        vnTemp.add(
                                new StdVec3f().set(
                                        Float.parseFloat(splittedLine[splittedLine.length - 3]),
                                        Float.parseFloat(splittedLine[splittedLine.length - 2]),
                                        Float.parseFloat(splittedLine[splittedLine.length - 1])
                                )
                        );
                    }
                    // Convert a vertex texture.
                    else if (line.startsWith("vt ")) {
                        // Start vt data section.
                        if (!inSection(BOBJ_VT_SECTION)) {
                            writeSection(fos, lastSection);
                            lastSection = BOBJ_VT_SECTION;
                        }

                        vtTemp.add(
                                new StdVec2f().set(
                                        Float.parseFloat(splittedLine[splittedLine.length - 2]),
                                        Float.parseFloat(splittedLine[splittedLine.length - 1])
                                )
                        );
                    }
                    // Convert a face.
                    else if (line.startsWith("f ")) {
                        // Start a f data section.
                        if (!inSection(BOBJ_F_SECTION)) {
                            writeSection(fos, lastSection);
                            lastSection = BOBJ_F_SECTION;
                        }

                        final int[] vertexIndices = new int[3];
                        final int[] normalIndices = new int[3];
                        final int[] texCoordIndices = new int[3];

                        // Face data only contains position and normal indices.
                        if (line.contains("//")) {
                            for (int i = 0; i < splittedLine.length; i++) {
                                final String[] slashSeparated = splittedLine[i].split("/");
                                vertexIndices[i] = Integer.parseInt(slashSeparated[0]);
                                texCoordIndices[i] = 0;
                                normalIndices[i] = Integer.parseInt(slashSeparated[1]);
                            }
                        }
                        // Face data contains position, texture and normal indices.
                        else if (line.contains("/")) {
                            for (int i = 0; i < 3; i++) {
                                // i+1 because "f"(0) -> "x/y/z"(1)  -> "x/y/z"(2)  -> "x/y/z"(3)
                                final String[] slashSeparated = splittedLine[i + 1].split("/");
                                vertexIndices[i] = Integer.parseInt(slashSeparated[0]);
                                texCoordIndices[i] = Integer.parseInt(slashSeparated[1]);
                                normalIndices[i] = Integer.parseInt(slashSeparated[2]);
                            }
                        }
                        // Face data only contains position indices.
                        else {
                            vertexIndices[0] = Integer.parseInt(splittedLine[splittedLine.length - 3]);
                            vertexIndices[1] = Integer.parseInt(splittedLine[splittedLine.length - 2]);
                            vertexIndices[2] = Integer.parseInt(splittedLine[splittedLine.length - 1]);
                            for (int i = 0; i < 3; i++) {
                                texCoordIndices[i] = 0;
                                normalIndices[i] = 0;
                            }
                        }

                        fTemp.add(new FaceVertexAttributeIndicesImpl(3, vertexIndices, normalIndices, texCoordIndices));
                    }

                }
                // No more lines to read in: -> write last section!
                writeSection(fos, lastSection);
            }
        } catch (final IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private boolean inSection(final byte sectionID) {
        return lastSection == sectionID;
    }

    private void writeSection(final @NotNull OutputStream fos, final byte sectionID) {
        try {
            switch (sectionID) {
                case BOBJ_UNDEFINED_SECTION:
                    // First line was read. Intentionally do nothing.
                    break;

                case BOBJ_V_SECTION:
                    fos.write(BOBJ_V_SECTION);
                    fos.write(ByteOps.int2ByteArray(vTemp.size() * Vec3f.FLOATS));
                    for (final StdVec3f v : vTemp) {
                        fos.write(v.bytes());
                    }
                    vTemp.clear(); // TODO: change when pooling for Vec3f is used
                    break;

                case BOBJ_VN_SECTION:
                    fos.write(BOBJ_VN_SECTION);
                    fos.write(ByteOps.int2ByteArray(vnTemp.size() * Vec3f.FLOATS));
                    for (final StdVec3f vn : vnTemp) {
                        fos.write(vn.bytes());
                    }
                    vnTemp.clear();
                    break;

                case BOBJ_VT_SECTION:
                    fos.write(BOBJ_VT_SECTION);
                    fos.write(ByteOps.int2ByteArray(vtTemp.size() * Vec2f.FLOATS));
                    for (final StdVec2f vt : vtTemp) {
                        fos.write(vt.bytes());
                    }
                    vtTemp.clear();
                    break;

                case BOBJ_F_SECTION:
                    fos.write(BOBJ_F_SECTION);
                    fos.write(ByteOps.int2ByteArray(fTemp.size() * FaceVertexAttributeIndicesImpl.INTS));
                    for (final FaceVertexAttributeIndicesImpl f : fTemp) {
                        fos.write(f.bytes());
                    }
                    fTemp.clear();
                    break;

                default:
                    throw new IllegalArgumentException("BOBJConverter: writeSection: unknown sectionID!");
            }
        } catch (final IOException ioe) {
            System.err.println("IOException in writeSection() while writing section: " + sectionID);
            ioe.printStackTrace();
        }
    }

}
