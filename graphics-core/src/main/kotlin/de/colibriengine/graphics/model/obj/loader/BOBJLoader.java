package de.colibriengine.graphics.model.obj.loader;

import de.colibriengine.exception.ModelLoadException;
import de.colibriengine.math.vector.vec2f.Vec2fFactory;
import de.colibriengine.math.vector.vec3f.Vec3fFactory;
import de.colibriengine.graphics.model.AbstractModelDefinition;

import java.net.URL;

import de.colibriengine.graphics.model.ModelLoader;
import org.jetbrains.annotations.NotNull;

public class BOBJLoader implements ModelLoader {

    @NotNull
    @Override
    public AbstractModelDefinition load(@NotNull URL resourceURL,
                                        @NotNull String relativePath,
                                        @NotNull Vec2fFactory vec2fFactory,
                                        @NotNull Vec3fFactory vec3fFactory) throws ModelLoadException {
        throw new UnsupportedOperationException("not implemented");
    }

}
