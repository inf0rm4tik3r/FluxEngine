package de.colibriengine.graphics.model.obj.loader;

import de.colibriengine.exception.ModelLoadException;
import de.colibriengine.graphics.model.FaceVertexAttributeIndicesImpl;
import de.colibriengine.graphics.model.GroupImpl;
import de.colibriengine.graphics.model.ModelLoader;
import de.colibriengine.graphics.model.obj.OBJData;
import de.colibriengine.math.vector.vec2f.Vec2fFactory;
import de.colibriengine.math.vector.vec3f.Vec3fFactory;
import de.colibriengine.objectpooling.Reusable;
import de.colibriengine.util.StringUtil;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

/**
 * OBJLoader
 *
 * @see <a href="https://github.com/Alhadis/language-wavefront/blob/master/specs/obj.pdf">OBJ Spec</a>
 * @since ColibriEngine 0.0.7.0
 */
public class OBJLoader implements ModelLoader, Reusable<OBJLoader> {

    private static final String VERTEX_DEF = "v ";
    private static final int VERTEX_DEF_LEN = 2;
    private static final String TEXTURE_COORD_DEF = "vt ";
    private static final int TEXTURE_COORD_DEF_LEN = 3;
    private static final String NORMAL_DEF = "vn ";
    private static final int NORMAL_DEF_LEN = 3;
    private static final String FACE_DEF = "f ";
    private static final int FACE_DEF_LEN = 2;
    private static final String OBJECT_DEF = "o ";
    private static final int OBJECT_DEF_LEN = 2;
    private static final String GROUP_DEF = "g ";
    private static final int GROUP_DEF_LEN = 2;
    private static final String MATERIAL_DEF = "mtllib ";
    private static final int MATERIAL_DEF_LEN = 7;
    private static final String MATERIAL_CHANGE_DEF = "usemtl ";
    private static final int MATERIAL_CHANGE_DEF_LEN = 7;
    private static final String COMMENT_DEF = "# ";
    private static final int COMMENT_DEF_LEN = 2;

    private static final String PART_DELIMITER = " ";
    private static final String F_VTN_DELIMITER = "/";
    private static final String F_VN_DELIMITER = "//";

    /**
     * The groups which are currently getting filled with data.
     * Faces of a model always belong to one or multiple group(s)!
     * At each definition of "g [name1, ...]" (group definition), new {@link GroupImpl}s must be created and used.
     */
    private final ArrayList<GroupImpl> currentGroups;
    private static final int CURRENT_GROUPS_INITIAL_SIZE = 4;

    /**
     * Constructs a new OBJLoader instance.
     */
    public OBJLoader() {
        currentGroups = new ArrayList<>(CURRENT_GROUPS_INITIAL_SIZE);
        reset();
    }

    @Override
    public OBJLoader reset() {
        clearGroups();
        return this;
    }

    /**
     * Automatically calls {@link OBJLoader#reset()} before returning the {@code OBJData} object, so that tis method
     * can be called again without any side effects.
     */
    @Override
    public @NotNull OBJData load(
            final @NotNull URL resourceURL,
            @NotNull String relativePath,
            @NotNull Vec2fFactory vec2fFactory,
            @NotNull Vec3fFactory vec3fFactory
    ) throws ModelLoadException {
        System.out.println("OBJLoader#load: \"" + resourceURL.getPath() + "\" loading...");

        // Create a new obj instance.
        final OBJData obj = new OBJData(resourceURL);

        try (final BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(resourceURL.openStream()))
        ) {
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {

                // Load a vertex position.
                if (line.startsWith(VERTEX_DEF)) {
                    parseVertex(obj, line.substring(VERTEX_DEF_LEN).trim(), vec3fFactory);
                }
                // Load a texture coordinate.
                else if (line.startsWith(TEXTURE_COORD_DEF)) {
                    parseTextureCoord(obj, line.substring(TEXTURE_COORD_DEF_LEN).trim(), vec2fFactory);
                }

                // Load a normal vector.
                else if (line.startsWith(NORMAL_DEF)) {
                    parseNormal(obj, line.substring(NORMAL_DEF_LEN).trim(), vec3fFactory);
                }

                // Load faces (with indices for vertex, texCoord and normal).
                else if (line.startsWith(FACE_DEF)) {
                    parseFaces(obj, line.substring(FACE_DEF_LEN).trim());
                }

                // Save and open objects.
                else if (line.startsWith(OBJECT_DEF)) {
                    parseObject(obj, line.substring(OBJECT_DEF_LEN).trim());
                }
                // Save and open groups.
                else if (line.startsWith(GROUP_DEF)) {
                    parseGroups(obj, line.substring(GROUP_DEF_LEN).trim());
                }

                // Load a material library.
                else if (line.startsWith(MATERIAL_DEF)) {
                    parseMaterial(obj, relativePath, line.substring(MATERIAL_DEF_LEN).trim());
                }

                // Load a material change.
                else if (line.startsWith(MATERIAL_CHANGE_DEF)) {
                    parseMaterialChange(obj, line.substring(MATERIAL_CHANGE_DEF_LEN).trim());
                }

                // Load a comment.
                else if (line.startsWith(COMMENT_DEF)) {
                    parseComment(obj, line.substring(COMMENT_DEF_LEN).trim());
                }

            }
        } catch (final IOException e) {
            throw new ModelLoadException("The .obj file could not be accessed.", e);
        }

        // Save all groups which are still opened.
        // These last groups would otherwise not be stored, and may be part of the next object...
        saveGroups(obj);

        // ..if this object does not get reset.
        // This object must be resetted, so that this method can be called again whiteout side effects.
        reset();

        return obj;
    }

    private void parseVertex(final @NotNull OBJData obj,
                             final @NotNull String line,
                             @NotNull Vec3fFactory vec3fFactory) {
        final String[] parts = line.split(PART_DELIMITER);
        final int partsLength = parts.length;

        obj.addVertex(
                vec3fFactory.acquireDirty().set(
                        Float.parseFloat(parts[0]),
                        Float.parseFloat(parts[1]),
                        Float.parseFloat(parts[2])
                )
        );
    }

    private void parseTextureCoord(final @NotNull OBJData obj,
                                   final @NotNull String line,
                                   @NotNull Vec2fFactory vec2fFactory) {
        final String[] parts = line.split(PART_DELIMITER);

        obj.addVertexTexCoord(
                vec2fFactory.acquireDirty().set(
                        Float.parseFloat(parts[0]),
                        Float.parseFloat(parts[1])
                )
        );
    }

    private void parseNormal(final @NotNull OBJData obj,
                             final @NotNull String line,
                             @NotNull Vec3fFactory vec3fFactory) {
        final String[] parts = line.split(PART_DELIMITER);

        obj.addVertexNormal(
                vec3fFactory.acquireDirty().set(
                        Float.parseFloat(parts[0]),
                        Float.parseFloat(parts[1]),
                        Float.parseFloat(parts[2])
                )
        );
    }

    /**
     * Parses a line starting with "f " and adds {@link FaceVertexAttributeIndicesImpl}'s to the specified {@code obj} object.
     *
     * @param obj  The OBJData to add information to.
     * @param line The line to process/parse.
     * @see <a href="https://stackoverflow.com/questions/23723993/converting-quadriladerals-in-an-obj-file-into-triangles">
     * converting-quadriladerals-in-an-obj-file-into-triangles
     * </a>
     */
    private void parseFaces(final @NotNull OBJData obj, final @NotNull String line) {
        // Faces must be stored inside a Group. Open a "default" if none is currently opened.
        ensureGroupExists();

        final String[] parts = line.split(PART_DELIMITER);
        final int partsLength = parts.length;

        final boolean vtn = line.contains(F_VTN_DELIMITER); // Indices for vertex, texture, and normal data present.
        final boolean vn = line.contains(F_VN_DELIMITER);  // Indices for vertex and normal data present.
        // final boolean v   = !(vtn || vn);                // Only vertex indices present. REDUNDANT...

        String[] vertexParts;
        int[] positionIndices = null;
        int[] normalIndices = null;
        int[] texCoordIndices = null;

        // TODO: Check if triangles or lines are specified: => clculate vertexCount
        final int vertexCount = 3;
        // Each face gets constructed from vertexCount vertices, where each vertex is of (v, t and n).
        try {
            for (int i = 1; i <= (partsLength - 2); i++) {
                if (vn) {
                    positionIndices = new int[vertexCount];
                    normalIndices = new int[vertexCount];

                    vertexParts = parts[0].split(F_VN_DELIMITER);
                    positionIndices[0] = Integer.parseInt(vertexParts[0]);
                    normalIndices[0] = Integer.parseInt(vertexParts[1]);
                    vertexParts = parts[i].split(F_VN_DELIMITER);
                    positionIndices[1] = Integer.parseInt(vertexParts[0]);
                    normalIndices[1] = Integer.parseInt(vertexParts[1]);
                    vertexParts = parts[i + 1].split(F_VN_DELIMITER);
                    positionIndices[2] = Integer.parseInt(vertexParts[0]);
                    normalIndices[2] = Integer.parseInt(vertexParts[1]);
                } else if (vtn) {
                    positionIndices = new int[vertexCount];
                    normalIndices = new int[vertexCount];
                    texCoordIndices = new int[vertexCount];

                    vertexParts = parts[0].split(F_VTN_DELIMITER);
                    positionIndices[0] = Integer.parseInt(vertexParts[0]);
                    texCoordIndices[0] = Integer.parseInt(vertexParts[1]);
                    normalIndices[0] = Integer.parseInt(vertexParts[2]);

                    vertexParts = parts[i].split(F_VTN_DELIMITER);
                    positionIndices[1] = Integer.parseInt(vertexParts[0]);
                    texCoordIndices[1] = Integer.parseInt(vertexParts[1]);
                    normalIndices[1] = Integer.parseInt(vertexParts[2]);

                    vertexParts = parts[i + 1].split(F_VTN_DELIMITER);
                    positionIndices[2] = Integer.parseInt(vertexParts[0]);
                    texCoordIndices[2] = Integer.parseInt(vertexParts[1]);
                    normalIndices[2] = Integer.parseInt(vertexParts[2]);
                } else {
                    positionIndices = new int[vertexCount];

                    for (int j = 0; j < vertexCount; j++) {
                        vertexParts = parts[0].split(F_VTN_DELIMITER);
                        positionIndices[0] = Integer.parseInt(vertexParts[0]);
                        vertexParts = parts[i].split(F_VTN_DELIMITER);
                        positionIndices[1] = Integer.parseInt(vertexParts[0]);
                        vertexParts = parts[i + 1].split(F_VTN_DELIMITER);
                        positionIndices[2] = Integer.parseInt(vertexParts[0]);
                    }
                }

                // Add the face definition to all currently opened groups.
                for (final GroupImpl group : currentGroups) {
                    group.addFace(new FaceVertexAttributeIndicesImpl(vertexCount, positionIndices, normalIndices, texCoordIndices));
                }
            }
        } catch (final NumberFormatException e) {
            System.err.println("At line: " + line);
            throw e;
        }
    }

    private void parseObject(final @NotNull OBJData obj, final @NotNull String line) {
        saveGroups(obj);
        clearGroups();

        // Just open a single group with the object name.
        openGroup(line);
    }

    private void parseGroups(final @NotNull OBJData obj, final @NotNull String line) {
        saveGroups(obj);
        clearGroups();

        for (final String groupName : line.split(PART_DELIMITER)) {
            openGroup(groupName);
        }
    }

    private void parseMaterial(final @NotNull OBJData obj,
                               @NotNull String relativePath,
                               final @NotNull String line) {
        final int lastSlash = relativePath.lastIndexOf("/");
        final String matLibPathAbsolute = relativePath.substring(0, lastSlash + 1) + line;
        final String matLibPathRelative = StringUtil.getRelativePath(matLibPathAbsolute);
        obj.addMatLibPath(matLibPathRelative);
    }

    private void parseMaterialChange(final @NotNull OBJData obj, final @NotNull String line) {
        for (final GroupImpl group : currentGroups) {
            group.setMaterialName(line);
        }
    }

    private void parseComment(final @NotNull OBJData obj, final @NotNull String comment) {
        // Intentionally do nothing... We do not care about any comments.
    }

    /**
     * Creates a new {@link GroupImpl} to fill if no group is currently opened on this loader.
     */
    private void ensureGroupExists() {
        if (currentGroups.isEmpty()) {
            // The new group will automatically has a default name attached to it.
            currentGroups.add(new GroupImpl(GroupImpl.DYNAMIC_SIZE, GroupImpl.DYNAMIC_SIZE));
        }
    }

    /**
     * Stores ("closes") all currently opened groups.
     */
    private void saveGroups(final @NotNull OBJData obj) {
        for (final GroupImpl group : currentGroups) {
            obj.addGroup(group);
        }
    }

    /**
     * Get rid of all group-references which are now stored in the OBJData object.
     */
    private void clearGroups() {
        currentGroups.clear();
    }

    /**
     * Open a new {@link GroupImpl} with the specified {@code groupName}.
     *
     * @param groupName The name of the group to open.
     */
    private void openGroup(final @NotNull String groupName) {
        final GroupImpl group = new GroupImpl(GroupImpl.DYNAMIC_SIZE, GroupImpl.DYNAMIC_SIZE);
        group.setName(groupName);
        currentGroups.add(group);
    }

}
