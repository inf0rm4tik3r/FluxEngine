package de.colibriengine.graphics.model.sceneentities

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.model.ModelFactory
import de.colibriengine.graphics.model.concrete.Quad
import de.colibriengine.graphics.model.concrete.QuadAlignment
import de.colibriengine.graphics.model.concrete.QuadDirection
import de.colibriengine.math.vector.vec2f.StdVec2f
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2i.StdVec2i
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer

class AlignedQuad : AbstractSceneGraphNode {

    constructor(
        entity: Entity,
        injectedQuad: Quad
    ) : super(entity) {
        entity.add(RenderComponent(injectedQuad))
    }

    constructor(
        entity: Entity,
        modelFactory: ModelFactory
    ) : super(entity) {
        entity.add(RenderComponent(getQuad(modelFactory)))
    }

    override fun init() {}

    override fun update(timing: Timer.View) {}

    fun alignSimple(
        x: Float, y: Float, z: Float, width: Float, height: Float,
        depth: Float
    ) {
        transform.setTranslation(x, y, z)
        transform.scale.set(width, height, depth)
    }

    fun alignSimple(
        translationView: Vec3fAccessor,
        scaleView: Vec3fAccessor
    ) {
        transform.setTranslation(translationView.x, translationView.y, translationView.z)
        transform.scale.set(scaleView.x, scaleView.y, scaleView.z)
    }

    fun alignSimple(
        translationView: Vec3iAccessor,
        scaleView: Vec3iAccessor
    ) {
        transform.setTranslation(translationView.x.toFloat(), translationView.y.toFloat(), translationView.z.toFloat())
        transform.scale.set(scaleView.x.toFloat(), scaleView.y.toFloat(), scaleView.z.toFloat())
    }

    fun alignSimple(x: Float, y: Float, width: Float, height: Float) {
        transform.setTranslation(x, y, 0.0f)
        transform.scale.set(width, height, 1.0f)
    }

    fun alignSimple(
        translationView: Vec2fAccessor,
        scaleView: Vec2fAccessor
    ) {
        transform.setTranslation(translationView.x, translationView.y, 0.0f)
        transform.scale.set(scaleView.x, scaleView.y, 1.0f)
    }

    fun alignSimple(
        translationView: Vec2iAccessor,
        scaleView: Vec2iAccessor
    ) {
        transform.setTranslation(translationView.x.toFloat(), translationView.y.toFloat(), 0.0f)
        transform.scale.set(scaleView.x.toFloat(), scaleView.y.toFloat(), 1.0f)
    }

    fun alignSimple(width: Float, height: Float) {
        transform.setTranslation(0.0f, 0.0f, 0.0f)
        transform.scale.set(width, height, 1.0f)
    }

    fun alignSimple(scale: Vec2iAccessor) {
        alignSimple(scale.x.toFloat(), scale.y.toFloat())
    }

    fun align(
        destWidth: Float,
        destHeight: Float,
        srcWidth: Float,
        srcHeight: Float
    ) {
        val destAspectRatio = destWidth / destHeight
        val srcAspectRatio = srcWidth / srcHeight
        val x: Float
        val y: Float
        val width: Float
        val height: Float

        // Borders on top and bottom.
        if (srcAspectRatio < destAspectRatio) {
            val widthScaled = destHeight * srcAspectRatio
            val xForScaledWidth = (destWidth - widthScaled) / 2.0f

            // Set width to screen width, adjust height.
            width = widthScaled
            x = xForScaledWidth
            height = destHeight
            y = 0f
        } else {
            val heightScaled = destWidth / srcAspectRatio
            val yForScaledHeight = (destHeight - heightScaled) / 2.0f

            // Set height to screen height, adjust width.
            width = destWidth
            x = 0f
            height = heightScaled
            y = yForScaledHeight
        }
        transform.setTranslation(x, y, 0.0f)
        transform.scale.set(width, height, 1.0f)
    }

    fun align(destDimension: StdVec2f, srcDimension: StdVec2f) {
        align(
            destDimension.x, destDimension.y,
            srcDimension.x, srcDimension.y
        )
    }

    fun align(destDimension: StdVec2i, srcDimension: StdVec2i) {
        align(
            destDimension.x.toFloat(), destDimension.y.toFloat(),
            srcDimension.x.toFloat(), srcDimension.y
                .toFloat()
        )
    }

    override fun destroy() {}

    companion object {

        private var QUAD: Quad? = null

        fun getQuad(modelFactory: ModelFactory): Quad {
            if (QUAD == null) {
                QUAD = modelFactory.quad(QuadDirection.XY, QuadAlignment.BOTTOM_LEFT_ALIGNED)
            }
            return QUAD as Quad
        }

    }

}
