package de.colibriengine.graphics.model.sceneentities

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.material.MaterialDefinition
import de.colibriengine.graphics.material.MaterialImpl
import de.colibriengine.graphics.material.MaterialTextureDefinition
import de.colibriengine.graphics.model.ModelFactory
import de.colibriengine.graphics.model.concrete.Quad
import de.colibriengine.graphics.model.concrete.QuadAlignment
import de.colibriengine.graphics.model.concrete.QuadDirection
import de.colibriengine.graphics.texture.TextureManager
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer

class TexturedQuad private constructor(
    entity: Entity,
    textureManager: TextureManager,
    texturePath: String,
    injectedQuad: Quad
) : AbstractSceneGraphNode(entity) {

    constructor(
        entity: Entity,
        modelFactory: ModelFactory,
        textureManager: TextureManager,
        texturePath: String
    ) : this(entity, modelFactory, textureManager, texturePath, 1)

    constructor(
        entity: Entity,
        modelFactory: ModelFactory,
        textureManager: TextureManager,
        texturePath: String,
        textureRepetitions: Int
    ) : this(
        entity, textureManager, texturePath,
        modelFactory.quad(QuadDirection.XZ, QuadAlignment.CENTERED, textureRepetitions.toFloat())
    )

    init {
        val ambientTexture = MaterialTextureDefinition()
        ambientTexture.texturePath = texturePath
        val materialDefinition = MaterialDefinition()
        materialDefinition.ambientColorTexture = ambientTexture
        val material = MaterialImpl(materialDefinition) // TODO: Use MaterialFactory/Manager instead.
        material.loadTextures(textureManager, "")
        injectedQuad.meshes[0].material = material

        entity.add(RenderComponent(injectedQuad))
    }

    override fun init() {}
    override fun update(timing: Timer.View) {}

    override fun destroy() {}

}
