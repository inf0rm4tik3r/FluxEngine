package de.colibriengine.graphics.model.sceneentities

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.material.MaterialDefinition
import de.colibriengine.graphics.material.MaterialImpl
import de.colibriengine.graphics.material.MaterialTextureDefinition
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.model.concrete.Triangle
import de.colibriengine.graphics.texture.TextureManagerImpl
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer

class TexturedTriangle(
    entity: Entity,
    meshFactory: MeshFactory,
    textureManager: TextureManagerImpl,
    texturePath: String
) : AbstractSceneGraphNode(entity) {

    private val triangle: Triangle = Triangle(meshFactory)

    init {
        val ambientTexture = MaterialTextureDefinition()
        ambientTexture.texturePath = texturePath
        val materialDefinition = MaterialDefinition()
        materialDefinition.ambientColorTexture = ambientTexture
        val material = MaterialImpl(materialDefinition) // TODO: Use MaterialFactory/Manager instead.
        material.loadTextures(textureManager, "")
        triangle.meshes[0].material = material
        entity.add(RenderComponent(triangle))
    }

    override fun init() {}
    override fun update(timing: Timer.View) {}
    override fun destroy() {}

}
