package de.colibriengine.graphics.shader;

import de.colibriengine.util.FileOps;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;

/**
 * Provides a caching mechanism for shader sources and compiled shader programs.
 *
 * @since ColibriEngine 0.0.6.8
 */
public class ShaderManagerImpl implements ShaderManager {

    private final HashMap<Integer, String> cachedSources;
    private final HashMap<Integer, Shader> cachedPrograms;
    public static final int INITIAL_CAPACITY = 16;

    /**
     * Constructs a new manager with empty caches of an initial size sufficient to hold 16 shader sources / programs.
     */
    public ShaderManagerImpl() {
        cachedSources = new HashMap<>(INITIAL_CAPACITY);
        cachedPrograms = new HashMap<>(INITIAL_CAPACITY);
    }

    /* SHADER SOURCE MANAGEMENT */

    @Override
    public @NotNull String requestShaderSource(final @NotNull String path) throws IllegalArgumentException {
        // TODO: check last modification time of the file
        final URL fileURL = FileOps.INSTANCE.getAsURL(path);

        if (fileURL == null) {
            throw new RuntimeException("The following path does not seem to be correct: " + path);
        }

        final int fileURLHash = fileURL.hashCode();

        // Return the shader source code immediately if it is in the cache..
        if (cachedSources.containsKey(fileURLHash)) {
            System.out.println("ShaderManager: requestShaderSource(): fetched from cache.");
            return cachedSources.get(fileURLHash);
        }
        // or Load the source code from its file and store it in the cache.
        else {
            final String shaderSource = loadShaderSource(fileURL);

            // Put the loaded shader source in the cache. Check if a mapping for the fileURLHash existed.
            final String previous = cachedSources.put(fileURLHash, shaderSource);
            if (previous != null) {
                System.err.println("ShaderManager: requestShaderSource(): pathURL hashcode matching. " +
                        "Previously stored source code got dismissed.");
            }

            return shaderSource;
        }
    }

    /**
     * Loads the code of a shader file into a string object. Pass the returned string to the
     * {@code addShaderSource(String, Type)} method, providing the proper shader type, to partially build up the
     * shader program. Repeat the addShaderSource(loadShaderSource(...)) for every shader you need.
     *
     * @param shaderSourceURL The location of the shader relatively to the working directory as a string object. Must
     *                        not be null. Has to point to an actual file in the systems file system.
     * @return The code of the shader, provided by the given path argument. The returned object can not be null.
     * @throws IllegalArgumentException if the given path argument is null.
     */
    private String loadShaderSource(final URL shaderSourceURL) throws IllegalArgumentException {
        if (shaderSourceURL == null) {
            throw new IllegalArgumentException(
                    "AbstractShader: loadShaderSource(): path argument in requestShaderSource() did not point to a valid file.");
        }

        // Create the StringBuilder that will hold the read lines.
        final StringBuilder shaderCode = new StringBuilder();

        // Create a BufferedReader to read the font file (try-with-resources).
        try (final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(shaderSourceURL.openStream()))) {
            String line;
            // Simply append every line to the StringBuilder object.
            try {
                while ((line = bufferedReader.readLine()) != null) {
                    shaderCode.append(line).append("\n");
                }
            } catch (final IOException ioe) {
                System.err.println("ShaderManager: loadShaderSource: I/O error occurred in readLine()  ");
            }

        } catch (final IOException ioe) {
            System.err.println("ShaderManager: loadShaderSource: Unable to open the input stream for: " +
                    shaderSourceURL.getPath());
            ioe.printStackTrace();
        }

        // Return the complete content of the file unmodified ion a string object
        return shaderCode.toString();
    }

    public void dismissShaderSource(final @NotNull String path) throws IllegalArgumentException {
        final URL fileURL = FileOps.INSTANCE.getAsURL(path);
        assert fileURL != null;
        final int fileURLHash = fileURL.hashCode();

        // Remove the shader source from the cache..
        if (cachedSources.containsKey(fileURLHash)) {
            System.out.println("ShaderManager: dismissShaderSource(): source removed from cache: " + fileURL.getPath
                    ());
            cachedSources.remove(fileURLHash);
        }
        // or do nothing if the source code of the specified source code is currently not cached.
        else {
            System.out.println("ShaderManager: dismissShaderSource(): source is not cached: " + fileURL.getPath());
        }
    }

    public void dismissAllShaderSources() {
        cachedSources.clear();
    }

    /* SHADER PROGRAM MANAGEMENT */

    // TODO: ..

}
