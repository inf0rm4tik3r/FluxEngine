package de.colibriengine.graphics.texture

import de.colibriengine.asset.AssetStore
import de.colibriengine.asset.ResourceLoader
import de.colibriengine.asset.ResourceName
import de.colibriengine.asset.image.Image
import de.colibriengine.asset.load
import de.colibriengine.logging.LogUtil.getLogger

class TextureManagerImpl(
    private val resourceLoader: ResourceLoader,
    private val textureFactory: TextureFactory
) : TextureManager {

    private val textures: AssetStore<Int, Texture> = AssetStore("Textures") {
        this.destroy()
    }

    override fun destroy() {
        textures.destroy(callDestroyOnRemaining = true)
    }

    override fun requestTexture(resourceName: ResourceName, options: TextureOptions): Texture {
        LOG.debug("Texture $resourceName requested.")
        val hash = createHash(resourceName, options)
        return textures.getOrPut(hash) { createTexture(resourceLoader.load(resourceName), options) }
    }

    override fun giveBack(texture: Texture) {
        textures.giveBack(createHash(ResourceName(texture.resourceName), texture.options))
    }

    private fun createHash(resourceName: ResourceName, options: TextureOptions): Int =
        1 + 37 * resourceName.hashCode() + 37 * options.hashCode()

    private fun createTexture(image: Image, options: TextureOptions): Texture {
        LOG.info("Creating texture for $image")
        return textureFactory.createFromImageSRBG(image, options)
    }

    companion object {
        private val LOG = getLogger(this)
    }

}
