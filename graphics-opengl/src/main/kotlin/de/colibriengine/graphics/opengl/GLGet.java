package de.colibriengine.graphics.opengl;

import de.colibriengine.math.vector.vec4i.Vec4i;
import de.colibriengine.graphics.opengl.buffer.GLBufferTarget;
import de.colibriengine.graphics.opengl.texture.GLTextureTarget;

import java.nio.IntBuffer;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.opengl.ARBFramebufferObject;
import org.lwjgl.opengl.EXTFramebufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.system.MemoryStack;

import static org.lwjgl.opengl.ARBIndirectParameters.GL_PARAMETER_BUFFER_BINDING_ARB;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_BINDING_1D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_BINDING_2D;
import static org.lwjgl.opengl.GL11.GL_VIEWPORT;
import static org.lwjgl.opengl.GL11.glGetIntegerv;
import static org.lwjgl.opengl.GL12.GL_TEXTURE_BINDING_3D;
import static org.lwjgl.opengl.GL13.GL_ACTIVE_TEXTURE;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_BINDING_CUBE_MAP;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER_BINDING;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER_BINDING;
import static org.lwjgl.opengl.GL20.GL_CURRENT_PROGRAM;
import static org.lwjgl.opengl.GL20.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS;
import static org.lwjgl.opengl.GL20.GL_MAX_TEXTURE_IMAGE_UNITS;
import static org.lwjgl.opengl.GL21.GL_PIXEL_PACK_BUFFER_BINDING;
import static org.lwjgl.opengl.GL21.GL_PIXEL_UNPACK_BUFFER_BINDING;
import static org.lwjgl.opengl.GL30.GL_MAX_RENDERBUFFER_SIZE;
import static org.lwjgl.opengl.GL30.GL_RENDERBUFFER_BINDING;
import static org.lwjgl.opengl.GL30.GL_TEXTURE_BINDING_1D_ARRAY;
import static org.lwjgl.opengl.GL30.GL_TEXTURE_BINDING_2D_ARRAY;
import static org.lwjgl.opengl.GL30.GL_TRANSFORM_FEEDBACK_BUFFER_BINDING;
import static org.lwjgl.opengl.GL30.GL_VERTEX_ARRAY_BINDING;
import static org.lwjgl.opengl.GL31.GL_TEXTURE_BINDING_BUFFER;
import static org.lwjgl.opengl.GL31.GL_TEXTURE_BINDING_RECTANGLE;
import static org.lwjgl.opengl.GL31.GL_UNIFORM_BUFFER_BINDING;
import static org.lwjgl.opengl.GL32.GL_TEXTURE_BINDING_2D_MULTISAMPLE;
import static org.lwjgl.opengl.GL32.GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY;
import static org.lwjgl.opengl.GL40.GL_DRAW_INDIRECT_BUFFER_BINDING;
import static org.lwjgl.opengl.GL40.GL_TEXTURE_BINDING_CUBE_MAP_ARRAY;
import static org.lwjgl.opengl.GL42.GL_ATOMIC_COUNTER_BUFFER_BINDING;
import static org.lwjgl.opengl.GL42.GL_COPY_READ_BUFFER_BINDING;
import static org.lwjgl.opengl.GL42.GL_COPY_WRITE_BUFFER_BINDING;
import static org.lwjgl.opengl.GL43.GL_DISPATCH_INDIRECT_BUFFER_BINDING;
import static org.lwjgl.opengl.GL43.GL_MAX_VERTEX_ATTRIB_BINDINGS;
import static org.lwjgl.opengl.GL43.GL_SHADER_STORAGE_BUFFER_BINDING;
import static org.lwjgl.opengl.GL44.GL_TEXTURE_BUFFER_BINDING;
import static org.lwjgl.opengl.GL46.GL_PARAMETER_BUFFER_BINDING;

/**
 * GLGet - Provides methods for querying certain OpenGL states or system defined constants.
 * Each method contained in this class typically makes a glGet..() call to OpenGL, which will degrade performance if
 * used (often). Try to only include calls to this classes methods for the purpose of debugging your application or
 * initially asking for some system/implementation dependant/defined constants.
 *
 * All methods of this class require an OpenGL context to be current in the current thread!
 *
 * @version 0.0.0.3
 * @created ?
 * @time 07.12.2018 23:20
 * @since ColibriEngine 0.0.7.1
 */
@SuppressWarnings("WeakerAccess")
public class GLGet {
    
    /**
     * Queries a single integer value from OpenGL.
     *
     * @param parameterName
     *     The parameter name to query.
     *
     * @return The value of this parameter name as returned by the current OpenGL context.
     */
    private static int getInteger1(final int parameterName) {
        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer ib = stack.mallocInt(1);
            glGetIntegerv(parameterName, ib);
            return ib.get();
        }
    }
    
    /**
     * Queries a four-dimensional integer value from OpenGL.
     *
     * @param parameterName
     *     The parameter name to query.
     *
     * @return The value of this parameter name as returned by the current OpenGL context.
     */
    private static @NotNull Vec4i getInteger4(final int parameterName, Vec4i storeIn) {
        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer ib = stack.mallocInt(4);
            glGetIntegerv(parameterName, ib);
            return storeIn.set(ib.get(), ib.get(), ib.get(), ib.get());
        }
    }
    
    /**
     * @return The currently set viewport parameters.
     *
     * @minContextRequired 2.0
     */
    public static @NotNull Vec4i viewport(Vec4i storeIn) {
        return getInteger4(GL_VIEWPORT, storeIn);
    }
    
    /**
     * @return The currently active shader program.
     *
     * @minContextRequired 2.0
     */
    public static int activeShaderProgram() {
        return getInteger1(GL_CURRENT_PROGRAM);
    }
    
    /**
     * @return The currently active (0 based!) texture unit.
     *
     * @minContextRequired 1.3
     */
    public static int activeTextureUnit() {
        return getInteger1(GL_ACTIVE_TEXTURE) - GL_TEXTURE0;
    }
    
    /**
     * @param target
     *     The target for which the bound texture should be queried.
     *
     * @return The texture currently bound to the specified {@code target} of the active texture unit.
     *
     * @minContextRequired 4.0
     */
    public static int textureBinding(final @NotNull GLTextureTarget target) {
        switch (target) {
            case GL_TEXTURE_1D:
                return textureBinding1D();
            case GL_TEXTURE_1D_ARRAY:
                return textureBinding1DArray();
            case GL_TEXTURE_2D:
                return textureBinding2D();
            case GL_TEXTURE_2D_ARRAY:
                return textureBinding2DArray();
            case GL_TEXTURE_2D_MULTISAMPLE:
                return textureBinding2DMultisample();
            case GL_TEXTURE_2D_MULTISAMPLE_ARRAY:
                return textureBinding2DMultisampleArray();
            case GL_TEXTURE_3D:
                return textureBinding3D();
            case GL_TEXTURE_CUBE_MAP:
                return textureBindingCubeMap();
            case GL_TEXTURE_CUBE_MAP_ARRAY:
                return textureBindingCubeMapArray();
            case GL_TEXTURE_BUFFER:
                return textureBindingBuffer();
            case GL_TEXTURE_RECTANGLE:
                return textureBindingRectangle();
            default:
                throw new RuntimeException("Target can not be processed.");
        }
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_1D} target of the active texture unit.
     *
     * @minContextRequired 1.1
     */
    public static int textureBinding1D() {
        return getInteger1(GL_TEXTURE_BINDING_1D);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_1D_ARRAY} target of the active texture
     *     unit.
     *
     * @minContextRequired 3.0
     */
    public static int textureBinding1DArray() {
        return getInteger1(GL_TEXTURE_BINDING_1D_ARRAY);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_2D} target of the active texture unit.
     *
     * @minContextRequired 1.1
     */
    public static int textureBinding2D() {
        return getInteger1(GL_TEXTURE_BINDING_2D);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_2D_ARRAY} target of the active texture
     *     unit.
     *
     * @minContextRequired 3.0
     */
    public static int textureBinding2DArray() {
        return getInteger1(GL_TEXTURE_BINDING_2D_ARRAY);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_2D_MULTISAMPLE} target of the active
     *     texture unit.
     *
     * @minContextRequired 3.2
     */
    public static int textureBinding2DMultisample() {
        return getInteger1(GL_TEXTURE_BINDING_2D_MULTISAMPLE);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_2D_MULTISAMPLE_ARRAY} target of the
     *     active texture unit.
     *
     * @minContextRequired 3.2
     */
    public static int textureBinding2DMultisampleArray() {
        return getInteger1(GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_3D} target of the active texture unit.
     *
     * @minContextRequired 1.2
     */
    public static int textureBinding3D() {
        return getInteger1(GL_TEXTURE_BINDING_3D);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_CUBE_MAP} target of the active
     *     texture unit.
     *
     * @minContextRequired 1.3
     */
    public static int textureBindingCubeMap() {
        return getInteger1(GL_TEXTURE_BINDING_CUBE_MAP);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_CUBE_MAP_ARRAY} target of the active
     *     texture unit.
     *
     * @minContextRequired 4.0
     */
    public static int textureBindingCubeMapArray() {
        return getInteger1(GL_TEXTURE_BINDING_CUBE_MAP_ARRAY);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_BUFFER} target of the active
     *     texture unit.
     *
     * @minContextRequired 1.3
     */
    public static int textureBindingBuffer() {
        return getInteger1(GL_TEXTURE_BINDING_BUFFER);
    }
    
    /**
     * @return The texture currently bound to the {@link GLTextureTarget#GL_TEXTURE_RECTANGLE} target of the active
     *     texture unit.
     *
     * @minContextRequired 3.1
     */
    public static int textureBindingRectangle() {
        return getInteger1(GL_TEXTURE_BINDING_RECTANGLE);
    }
    
    /**
     * @return The currently bound VAO handle.
     */
    public static int vertexArrayObjectBinding() {
        return getInteger1(GL_VERTEX_ARRAY_BINDING);
    }
    
    /**
     * @param target
     *     The target for which to retrieve the currently bound texture.
     *
     * @return The buffer handle currently bound to {@code target}.
     */
    public static int bufferBinding(final @NotNull GLBufferTarget target) {
        switch (target) {
            case GL_ARRAY_BUFFER:
                return arrayBufferBinding();
            case GL_ELEMENT_ARRAY_BUFFER:
                return elementArrayBufferBinding();
            case GL_PIXEL_PACK_BUFFER:
                return pixelPackBufferBinding();
            case GL_PIXEL_UNPACK_BUFFER:
                return pixelUnpackBufferBinding();
            case GL_TRANSFORM_FEEDBACK_BUFFER:
                return transformFeedbackBufferBinding();
            case GL_UNIFORM_BUFFER:
                return transformFeedbackBufferBinding();
            case GL_TEXTURE_BUFFER:
                return textureBufferBinding();
            case GL_COPY_READ_BUFFER:
                return copyReadBufferBinding();
            case GL_COPY_WRITE_BUFFER:
                return copyWriteBufferBinding();
            case GL_DRAW_INDIRECT_BUFFER:
                return drawIndirectBufferBinding();
            case GL_ATOMIC_COUNTER_BUFFER:
                return atomicCounterBufferBinding();
            case GL_DISPATCH_INDIRECT_BUFFER:
                return dispatchIndirectBufferBinding();
            case GL_SHADER_STORAGE_BUFFER:
                return shaderStorageBufferBinding();
            case GL_PARAMETER_BUFFER_ARB:
                return parameterBufferARBBinding();
            case GL_PARAMETER_BUFFER:
                return parameterBufferBinding();
            default:
                throw new RuntimeException("Target can not be processed.");
        }
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_ARRAY_BUFFER} target.
     *
     * @minContextRequired 1.5
     */
    public static int arrayBufferBinding() {
        return getInteger1(GL_ARRAY_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_ELEMENT_ARRAY_BUFFER} target.
     *
     * @minContextRequired 1.5
     */
    public static int elementArrayBufferBinding() {
        return getInteger1(GL_ELEMENT_ARRAY_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_PIXEL_PACK_BUFFER} target.
     *
     * @minContextRequired 2.1
     */
    public static int pixelPackBufferBinding() {
        return getInteger1(GL_PIXEL_PACK_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_PIXEL_UNPACK_BUFFER} target.
     *
     * @minContextRequired 2.1
     */
    public static int pixelUnpackBufferBinding() {
        return getInteger1(GL_PIXEL_UNPACK_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_TRANSFORM_FEEDBACK_BUFFER} target.
     *
     * @minContextRequired 3.0
     */
    public static int transformFeedbackBufferBinding() {
        return getInteger1(GL_TRANSFORM_FEEDBACK_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_UNIFORM_BUFFER} target.
     *
     * @minContextRequired 3.1
     */
    public static int uniformBufferBinding() {
        return getInteger1(GL_UNIFORM_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_TEXTURE_BUFFER} target.
     *
     * @minContextRequired 4.4
     */
    public static int textureBufferBinding() {
        return getInteger1(GL_TEXTURE_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_COPY_READ_BUFFER} target.
     *
     * @minContextRequired 4.2
     */
    public static int copyReadBufferBinding() {
        return getInteger1(GL_COPY_READ_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_COPY_WRITE_BUFFER} target.
     *
     * @minContextRequired 4.2
     */
    public static int copyWriteBufferBinding() {
        return getInteger1(GL_COPY_WRITE_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_DRAW_INDIRECT_BUFFER} target.
     *
     * @minContextRequired 4.0
     */
    public static int drawIndirectBufferBinding() {
        return getInteger1(GL_DRAW_INDIRECT_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_ATOMIC_COUNTER_BUFFER} target.
     *
     * @minContextRequired 4.2
     */
    public static int atomicCounterBufferBinding() {
        return getInteger1(GL_ATOMIC_COUNTER_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_DISPATCH_INDIRECT_BUFFER} target.
     *
     * @minContextRequired 4.3
     */
    public static int dispatchIndirectBufferBinding() {
        return getInteger1(GL_DISPATCH_INDIRECT_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_SHADER_STORAGE_BUFFER} target.
     *
     * @minContextRequired 4.3
     */
    public static int shaderStorageBufferBinding() {
        return getInteger1(GL_SHADER_STORAGE_BUFFER_BINDING);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_PARAMETER_BUFFER_ARB} target.
     *
     * @minContextRequired ARBIndirectParameters
     */
    public static int parameterBufferARBBinding() {
        return getInteger1(GL_PARAMETER_BUFFER_BINDING_ARB);
    }
    
    /**
     * @return The buffer currently bound to the {@link GLBufferTarget#GL_PARAMETER_BUFFER} target.
     *
     * @minContextRequired 4.6
     */
    public static int parameterBufferBinding() {
        return getInteger1(GL_PARAMETER_BUFFER_BINDING);
    }
    
    /**
     * @return The currently bound draw framebuffer.
     *
     * @throws UnsupportedOperationException
     *     If no valid code path exists.
     * @minContextRequired 3.0 or ARBFramebufferObject or EXTFramebufferObject
     */
    public static int drawFramebufferBinding() throws UnsupportedOperationException {
        if (GLUtil.isContextAtLeast(3, 0)) {
            return getInteger1(GL30.GL_DRAW_FRAMEBUFFER_BINDING);
        } else if (GLUtil.getCaps().GL_ARB_framebuffer_object) {
            return getInteger1(ARBFramebufferObject.GL_DRAW_FRAMEBUFFER_BINDING);
        } /* else if (GLUtil.getCaps().GL_EXT_framebuffer_object) {
            return getInteger1(EXTFramebufferObject.GL_FRAMEBUFFER_BINDING_EXT);
        } */ else {
            GLUtil.throwNoPathError();
            // Unreachable but necessary because IntelliJ doesn't recognize the immediate throw...
            return -1;
        }
    }
    
    /**
     * @return The currently bound read framebuffer.
     *
     * @throws UnsupportedOperationException
     *     If no valid code path exists.
     * @minContextRequired 3.0 or ARBFramebufferObject or EXTFramebufferObject
     */
    public static int readFramebufferBinding() throws UnsupportedOperationException {
        if (GLUtil.isContextAtLeast(3, 0)) {
            return getInteger1(GL30.GL_READ_FRAMEBUFFER_BINDING);
        } else if (GLUtil.getCaps().GL_ARB_framebuffer_object) {
            return getInteger1(ARBFramebufferObject.GL_READ_FRAMEBUFFER_BINDING);
        } /* else if (GLUtil.getCaps().GL_EXT_framebuffer_object) {
            return getInteger1(EXTFramebufferObject.GL_FRAMEBUFFER_BINDING_EXT);
        } */ else {
            GLUtil.throwNoPathError();
            // Unreachable but necessary because IntelliJ doesn't recognize the immediate throw...
            return -1;
        }
    }
    
    /**
     * @return The currently bound renderbuffer.
     *
     * @throws UnsupportedOperationException
     *     If no valid code path exists.
     * @minContextRequired 3.0 or ARBFramebufferObject or EXTFramebufferObject
     */
    public static int renderbufferBinding() throws UnsupportedOperationException {
        if (GLUtil.isContextAtLeast(3, 0)) {
            return getInteger1(GL_RENDERBUFFER_BINDING);
        } else if (GLUtil.getCaps().GL_ARB_framebuffer_object) {
            return getInteger1(ARBFramebufferObject.GL_RENDERBUFFER_BINDING);
        } else if (GLUtil.getCaps().GL_EXT_framebuffer_object) {
            return getInteger1(EXTFramebufferObject.GL_RENDERBUFFER_BINDING_EXT);
        } else {
            GLUtil.throwNoPathError();
            // Unreachable but necessary because IntelliJ doesn't recognize the immediate throw...
            return -1;
        }
    }
    
    
    /* IMPLEMENTATION DEFINED CONSTANTS */
    
    /**
     * @return The maximum supported texture image units that can be used to access texture maps from the fragment
     *     shader. The value must be at least 16.
     *
     * @minContextRequired 2.0
     */
    public static int maxTextureImageUnits() {
        return getInteger1(GL_MAX_TEXTURE_IMAGE_UNITS);
    }
    
    /**
     * @return The maximum supported texture image units that can be used to access texture maps from the vertex shader
     *     and the fragment processor combine. If both the vertex shader and the fragment processing stage access the
     *     same texture image unit, then that counts as using two texture image units against this limit. The value must
     *     be at least 48.
     *
     * @minContextRequired 2.0
     */
    public static int maxCombinedTextureImageUnits() {
        return getInteger1(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS);
    }
    
    /**
     * @return The maximum number of vertex buffers that may be bound.
     *
     * @minContextRequired 4.3
     */
    public static int maxVertexAttribBindings() {
        return getInteger1(GL_MAX_VERTEX_ATTRIB_BINDINGS);
    }
    
    /**
     * @return The maximum size allowed for the width and height of a renderbuffer.
     *
     * @minContextRequired 3.0
     */
    public static int maxRenderbufferSize() {
        return getInteger1(GL_MAX_RENDERBUFFER_SIZE);
    }
    
    
    /* STRINGS */
    
    /**
     * @return The version of the current context.
     *
     * @minContextRequired 1.1
     */
    public static @NotNull String version() {
        final @Nullable String version = GL11.glGetString(GL11.GL_VERSION);
        return version == null ? "" : version;
    }
    
    /**
     * @return The version of the maximum supported shading language in the current context.
     *
     * @minContextRequired 2.0
     */
    public static @NotNull String shadingLanguageVersion() {
        final @Nullable String shadingLanguageVersion = GL11.glGetString(GL20.GL_SHADING_LANGUAGE_VERSION);
        return shadingLanguageVersion == null ? "" : shadingLanguageVersion;
    }
    
    /**
     * @return The vendor of the current systems graphics interface.
     *
     * @minContextRequired 1.1
     */
    public static @NotNull String vendor() {
        final @Nullable String vendor = GL11.glGetString(GL11.GL_VENDOR);
        return vendor == null ? "" : vendor;
    }
    
    /**
     * @return The name of the current systems graphics interface.
     *
     * @minContextRequired 1.1
     */
    public static @NotNull String renderer() {
        final @Nullable String renderer = GL11.glGetString(GL11.GL_RENDERER);
        return renderer == null ? "" : renderer;
    }
    
}
