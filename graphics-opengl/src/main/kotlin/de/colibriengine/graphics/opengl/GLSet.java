package de.colibriengine.graphics.opengl;

import de.colibriengine.logging.LogUtil;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import de.colibriengine.graphics.opengl.context.GLContextInformation;
import de.colibriengine.graphics.opengl.context.ContextStateTracker;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

/**
 * Provides convenience methods to set state variables of the current OpenGL context.
 * <b>NOTE:</b> Calling on of the methods contained in this class will not result in the change of any
 * {@link ContextStateTracker} instance!
 *
 * @version 0.0.0.2
 * @since ColibriEngine 0.0.7.2
 */
public class GLSet {

    private static final Logger LOG = LogUtil.getLogger(GLSet.class);

    /**
     * The information instance of the currently active context.
     * Must be updated each time a context switch is performed using the
     * {@link GLSet#setContextInformation(GLContextInformation)} method!
     */
    private static @Nullable GLContextInformation CONTEXT_INFORMATION;

    private static final @NotNull String MSG_CONTEXT_INFORMATION_NOT_SET = "ContextInformation reference was not set.";

    /**
     * Update the context information reference.
     *
     * @param contextInformation A reference to the context information instance of the currently active context (window).
     */
    public static void setContextInformation(final @Nullable GLContextInformation contextInformation) {
        CONTEXT_INFORMATION = contextInformation;
    }

    /**
     * Activate the specified texture unit.
     *
     * @param textureUnit The texture unit which should be made active.
     *                    Zero based!
     *                    Must not be negative!
     *                    Must not exceed the size of supported texture units as given by
     *                    ({@link GLContextInformation#getMaxCombinedTextureImageUnits()} - 1) (as this variable is 0-based).
     * @throws IllegalArgumentException If debugging is active and the given texture unit was either smaller than 0 or equal to or larger than the
     *                                  amount of available texture units on the current system.
     * @throws IllegalStateException    If debugging is active and the context information reference was not set.
     */
    public static void activeTextureUnit(final int textureUnit) throws IllegalArgumentException, IllegalStateException {
        assert CONTEXT_INFORMATION != null;
        assert !(textureUnit < 0) : "Negative texture units do not exist!";
        assert !(textureUnit >= CONTEXT_INFORMATION.getMaxCombinedTextureImageUnits()) :
                "The given texture unit (" + textureUnit + ") exceeds the range of " +
                        "available combined texture units on the current system (" +
                        CONTEXT_INFORMATION.getMaxCombinedTextureImageUnits() + ")!";

        GL13.glActiveTexture(GL13.GL_TEXTURE0 + textureUnit);
    }

    @SuppressWarnings("Duplicates")
    public static void viewport(final int x, final int y, final int width, final int height) {
        assert CONTEXT_INFORMATION != null;
        assert !(x < 0) : "x must not be negative!";
        assert !(y < 0) : "x must not be negative!";
        assert !(width < 0) : "x must not be negative!";
        assert !(height < 0) : "x must not be negative!";

        GL11.glViewport(x, y, width, height);
        CONTEXT_INFORMATION.getCST().setViewport(x, y, width, height);
    }

    public static void viewport(final @Nullable Vec2iAccessor position, final int width, final int height) {
        final int posX, posY;
        if (position != null) {
            posX = position.getX();
            posY = position.getY();
        } else {
            posX = 0;
            posY = 0;
        }
        if (width == 0) {
            LOG.warn("width is 0!");
        }
        if (height == 0) {
            LOG.warn("height is 0!");
        }
        viewport(posX, posY, width, height);
    }

    public static void viewport(final int x, final int y, final @NotNull Vec2iAccessor dimension) {
        viewport(x, y, dimension.getX(), dimension.getY());
    }

    public static void viewport(final @Nullable Vec2iAccessor position,
                                final @NotNull Vec2iAccessor dimension) {
        viewport(position, dimension.getX(), dimension.getY());
    }

}
