package de.colibriengine.graphics.opengl.buffer;

import de.colibriengine.graphics.Buffer;
import de.colibriengine.graphics.opengl.GLUtil;
import de.colibriengine.logging.LogUtil;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.opengl.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Buffer - Encapsulation of OpenGL buffer objects.
 */
@SuppressWarnings("WeakerAccess")
public class GLBuffer implements Buffer {

    /**
     * First possible index in a buffers data store.
     */
    public static final int BUFFER_START = 0;

    /**
     * Specifies the value which must be used to break an existing buffer binding.
     */
    public static final int NO_BOND = 0;

    /**
     * The buffer handle must have this value assigned to it if it is currently not initialized by OpenGL.
     * (Does not represent an OpenGL id/handle.)
     */
    private static final int BUFFER_UNSET = -1;

    /**
     * The OpenGL handle of this buffer.
     */
    private int buffer = BUFFER_UNSET;

    /**
     * The size of the current buffer in bytes. Will be set when calling {@link GLBuffer#create()}.
     */
    private int sizeInBytes;

    /**
     * Stores the target of this OpenGL buffer.
     */
    private @NotNull GLBufferTarget target;

    /**
     * The initial capacity of the storageFlags ArrayList.
     */
    private static final int STORAGE_FLAGS_INITIAL_CAPACITY = 4;

    /**
     * Stores the flags with which the buffers storage got/will be created.
     */
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private final @NotNull List<GLBufferStorageFlag> storageFlags;
    private boolean hasPersistentFlag;
    private int combinedStorageFlags;

    /**
     * May hold a reference to the mapping of this buffer. Used as a cache. Allows the getMapping* methods to directly
     * return the ByteBuffer at the mapping pointer.
     */
    private @Nullable ByteBuffer mapping;

    /**
     * Stores if this buffer is currently created.
     */
    private boolean created;

    /**
     * Stores if this buffers storage is currently created.
     */
    private boolean storageCreated;

    private static final Logger LOG = LogUtil.getLogger(GLBuffer.class);

    /**
     * @param target The GL target to which this buffer should be bound. One of:
     */
    public GLBuffer(final @NotNull GLBufferTarget target) {
        this.target = target;
        storageFlags = new ArrayList<>(STORAGE_FLAGS_INITIAL_CAPACITY);
        hasPersistentFlag = false;
        mapping = null;
        created = false;
        storageCreated = false;
    }

    /**
     * Must not be called from outside this class!
     *
     * @param created Whether or not this buffer is created.
     */
    @Override
    public void setCreated(final boolean created) {
        this.created = created;
    }

    @Override
    public boolean isCreated() {
        return created;
    }

    /**
     * Creates the buffer in the current OpenGL context.
     * Each consecutive call to this method will trigger a call of {@link GLBuffer#release()}, if it was not called
     * before by the user. This allows a reusage of this object and must be done to free the currently created
     * buffer storage!
     * <p>
     * DSA available: The created buffer will not be bound. The context remains unchanged.
     * DSA not available: The created buffer will be bound to the {@code target} specified in this object.
     *
     * @throws UnsupportedOperationException If the current OpenGL context does not fulfill the minimum version requirement.
     * @minContextRequired 1.5
     */
    public void create() throws UnsupportedOperationException {
        // We must release the buffer first, before it can be reassigned / recreated!
        if (created) {
            release();
        }

        // DSA available.
        if (GLUtil.isContextAtLeast(4, 5)) {
            buffer = GL45.glCreateBuffers();
        } else if (GLUtil.getCaps().GL_ARB_direct_state_access) {
            buffer = ARBDirectStateAccess.glCreateBuffers();
        }
        // EXTDirectStateAccess does not include the glCreateBuffers function.
        // DSA not available.
        else if (GLUtil.isContextAtLeast(1, 5)) {
            buffer = GL15.glGenBuffers();
            bind();
        }
        // Command is not supported by the current OpenGL context.
        else {
            GLUtil.throwNoPathError();
        }

        // The buffer is now "created"!
        this.created = true;
    }

    /**
     * Releases this buffer, freeing GPU storage, deleting the OpenGL handle.
     * This object can thereafter get recreated through a call to {@link GLBuffer#create()}.
     * This method must only be called when this buffer is created!
     *
     * @throws IllegalStateException If this buffer is not created.
     * @minContextRequired 1.5
     */
    public void release() throws IllegalStateException {
        ensureCreated();

        unbind();
        forceUnmap();

        // Delete the OpenGL / GPU resources occupied by this buffer.
        GL15.glDeleteBuffers(buffer);

        // Reset the OpenGL handle to an uninitialized state.
        buffer = BUFFER_UNSET;

        // Inform the CST that this buffer is no longer bound to the context.
        if (isBound()) {
            GLUtil.getCST().setBoundBufferAt(target, NO_BOND);
        }
        // TODO: Check smth like PIXEL_PACK_BUFFER, COPY_READ_BUFFER, COPY_WRITE_BUFFER.

        // Reset the storageCreated flag, as storage must be reconfigured after this buffer got recreated.
        this.storageCreated = false;

        // This buffer is no longer "created".
        this.created = false;
    }

    public void releaseIfCreated() {
        if (isCreated()) {
            release();
        }
    }

    /**
     * Updates the storage flags of this buffer.
     * Must be called when this buffer is not created.
     *
     * @param storageFlags The storage flags which will be used the next time the storage of this buffer gets created.
     * @throws IllegalStateException If this buffers storage is currently created.
     */
    public void setStorageFlags(final @NotNull GLBufferStorageFlag... storageFlags) throws IllegalStateException {
        ensureStorageNotCreated();

        // The storageFlags list will be refilled with the supplied storage flags.
        this.storageFlags.clear();

        //noinspection NullableProblems
        for (@Nullable GLBufferStorageFlag storageFlag : storageFlags) {
            // The application may use coherent buffers if the context is 4.4 or higher or ARBBufferStorage
            // is available.
            if (storageFlag == GLBufferStorageFlag.GL_MAP_COHERENT_BIT) {
                if (!GLUtil.isContextAtLeast(4, 4)) {
                    if (GLUtil.getCaps().GL_ARB_buffer_storage) {
                        storageFlag = GLBufferStorageFlag.GL_MAP_COHERENT_BIT_ARB;
                    }
                    // The context otherwise doesn't support them!
                    else {
                        LOG.warn(
                                "Application tried to use coherent buffer in context version: {}",
                                GLUtil.getContextVersionString()
                        );
                        storageFlag = null;
                    }
                }
            }

            // The application may use persistently mapped buffers if the context is 4.4 or higher or ARBBufferStorage
            // is available.
            if (storageFlag == GLBufferStorageFlag.GL_MAP_PERSISTENT_BIT) {
                hasPersistentFlag = true;
                if (!GLUtil.isContextAtLeast(4, 4)) {
                    if (GLUtil.getCaps().GL_ARB_buffer_storage) {
                        storageFlag = GLBufferStorageFlag.GL_MAP_PERSISTENT_BIT_ARB;
                    }
                    // The context otherwise doesn't support them!
                    else {
                        LOG.warn(
                                "Application tried to use persistently mapped buffer in context version: {}",
                                GLUtil.getContextVersionString()
                        );
                        storageFlag = null;
                        hasPersistentFlag = false;
                    }
                }
            }

            // The previous checks may have nulled the current reference.
            if (storageFlag != null) {
                this.storageFlags.add(storageFlag);
            }
        }

        combinedStorageFlags = GLBufferStorageFlag.combine(storageFlags);
    }

    /**
     * Must not be called from outside this class!
     *
     * @param storageCreated Whether or not the storage is currently created.
     */
    @Override
    public void setStorageCreated(final boolean storageCreated) {
        this.storageCreated = storageCreated;
    }

    @Override
    public boolean isStorageCreated() {
        return this.storageCreated;
    }

    /**
     * Creates storage for this buffer object. Must be called before the buffer can be used.
     *
     * @param sizeInBytes The size of the buffer in bytes.
     */
    public void createStorage(final int sizeInBytes) {
        ensureCreated();
        ensureStorageNotCreated();

        this.sizeInBytes = sizeInBytes;

        // DSA available.
        if (GLUtil.isContextAtLeast(4, 5)) {
            GL45.glNamedBufferStorage(buffer, sizeInBytes, combinedStorageFlags);
        } else if (GLUtil.getCaps().GL_ARB_direct_state_access) {
            ARBDirectStateAccess.glNamedBufferStorage(buffer, sizeInBytes, combinedStorageFlags);
        }
        // EXTDirectStateAccess does not contain a glNamedBufferStorage function.
        // DSA not available.
        else if (GLUtil.isContextAtLeast(4, 4)) {
            ensureBound();
            GL44.glBufferStorage(target.id, sizeInBytes, combinedStorageFlags);
        } else if (GLUtil.isContextAtLeast(1, 5)) {
            ensureBound();
            GL15.glBufferData(target.id, sizeInBytes, GL15.GL_DYNAMIC_DRAW); // TODO: convert flags ??
        }
        // Command is not supported by the current OpenGL context.
        else {
            GLUtil.throwNoPathError();
        }

        this.storageCreated = true;
    }

    /**
     * Must only be called when this buffer and its data store are created.
     * If you are uncertain you may check both with calls to {@link GLBuffer#isCreated()} and
     * {@link GLBuffer#isStorageCreated()}.
     *
     * @return The size of this buffers data store.
     * @throws IllegalStateException If this buffer or its data store was is not currently created.
     */
    public int getSizeInBytes() throws IllegalStateException {
        return sizeInBytes;
    }

    // TODO: Should bind() check if the buffer is created?

    /**
     * @minContextRequired 1.5
     */
    public void bind() {
        // Only bind this buffer if its isn't already bound at its target.
        if (GLUtil.getCST().getBoundBufferAt(target) != buffer) {
            GL15.glBindBuffer(target.id, buffer);
            GLUtil.getCST().setBoundBufferAt(target, buffer);
        }
    }

    public void bindAsCopyReadBuffer() {
        // Only bind this buffer if its isn't already bound as the COPY_READ_BUFFER.
        if (GLUtil.getCST().getBoundBufferAt(GLBufferTarget.GL_COPY_READ_BUFFER) != buffer) {
            GL15.glBindBuffer(GLBufferTarget.GL_COPY_READ_BUFFER.id, buffer);
            GLUtil.getCST().setBoundBufferAt(GLBufferTarget.GL_COPY_READ_BUFFER, buffer);
        }
    }

    public void bindAsCopyWriteBuffer() {
        // Only bind this buffer if its isn't already bound as the COPY_WRITE_BUFFER.
        if (GLUtil.getCST().getBoundBufferAt(GLBufferTarget.GL_COPY_WRITE_BUFFER) != buffer) {
            GL15.glBindBuffer(GLBufferTarget.GL_COPY_WRITE_BUFFER.id, buffer);
            GLUtil.getCST().setBoundBufferAt(GLBufferTarget.GL_COPY_WRITE_BUFFER, buffer);
        }
    }

    /**
     * Should be called after this buffer got created / initialized.
     * Basically unbinds this buffer if it must (if it was bound in the creation process).
     *
     * @minContextRequired 3.0
     */
    public void finish() {
        unbindIfBound();
    }

    /**
     * @return Whether or not this buffer is currently bound to the current context.
     */
    public boolean isBound() {
        return GLUtil.getCST().getBoundBufferAt(target) == buffer;
    }

    /**
     * Erases the current buffer binding from the context.
     * After a call to this method returns, no buffer will be bound to the context (at this objects target).
     *
     * @minContextRequired 3.0
     */
    public void unbind() {
        GL15.glBindBuffer(target.id, NO_BOND);
        GLUtil.getCST().setBoundBufferAt(target, NO_BOND);
    }

    @Override
    public void unbindIfBound() {
        if (isBound()) {
            unbind();
        }
    }

    /**
     * Fills and updates the data of this buffer with {@code data}.
     *
     * @param startIndex The index / offset at which the insertion of {@code data} starts.
     * @param data       The new data to insert into the buffer.
     * @throws IllegalArgumentException If the given {@code startIndex} is not in the range [0, {@link GLBuffer#sizeInBytes - 1}].
     *                                  Or if the {@code data} is too long and cannot be put in the buffer at index {@code startIndex}.
     * @throws IllegalStateException    If this buffer or its storage was not jet created, or if this buffer is not bound when using a DSA function
     *                                  is not possible.
     */
    public void fill(final long startIndex, final @NotNull ByteBuffer data)
            throws IllegalArgumentException, IllegalStateException {
        ensureCreated();
        ensureStorageCreated();

        if (startIndex < 0) {
            throw new IllegalArgumentException("The index must be positive!");
        }
        if (startIndex >= sizeInBytes) {
            throw new IllegalArgumentException("The index must be smaller then the size of this buffers data store!");
        }
        if (startIndex + data.remaining() >= sizeInBytes) {
            throw new IllegalArgumentException("The specified data is too long to be put at startIndex: " + startIndex);
        }

        // DAS available.
        if (GLUtil.isContextAtLeast(4, 5)) {
            GL45.glNamedBufferSubData(buffer, startIndex, data);
        } else if (GLUtil.getCaps().GL_ARB_direct_state_access) {
            ARBDirectStateAccess.glNamedBufferSubData(buffer, startIndex, data);
        } else if (GLUtil.getCaps().GL_EXT_direct_state_access) {
            EXTDirectStateAccess.glNamedBufferSubDataEXT(buffer, startIndex, data);
        }

        // DSA not available.
        ensureBound();
        if (GLUtil.isContextAtLeast(1, 5)) {
            GL15.glBufferSubData(target.id, startIndex, data);
        } else if (GLUtil.getCaps().GL_ARB_vertex_buffer_object) {
            ARBVertexBufferObject.glBufferSubDataARB(target.id, startIndex, data);
        }

        // Function not supported.
        GLUtil.throwNoPathError();
    }

    public void fill(final @NotNull ByteBuffer data) {
        fill(0, data);
    }

    private static @NotNull String copyArgsAsString(
            final long fromSize, final long toSize, final long readOffset,
            final long writeOffset, final long size
    ) {
        return "Given values:" +
                "[ from-size: " + fromSize +
                ", to-size: " + toSize +
                ", readOffset: " + readOffset +
                ", writeOffset: " + writeOffset +
                ", size: " + size +
                " ]";
    }

    /**
     * Copies (some) content from the {@code from} buffer to the {@code to} buffer.
     * Consider calling the {@link GLBuffer#copyFrom(GLBuffer, long, long, long)} and
     * {@link GLBuffer#copyTo(GLBuffer, long, long, long)} methods instead!
     *
     * @param from        The buffer from which to copy.
     * @param to          The buffer to copy into.
     * @param readOffset  The starting position in the {@code from} buffer from which the copy should start.
     * @param writeOffset The starting position in the {@code to} buffer at which the data should be copied.
     * @param size        The amount of data to copy.
     * @throws IllegalStateException         If either the {@code from} or {@code to} buffer or their data store was not created.
     * @throws UnsupportedOperationException If the current OpenGL context is unable to perform this operation.
     * @throws IllegalArgumentException      If one of the following assertions does not hold:
     *                                       <ul>
     *                                       <li>size >= 0</li>
     *                                       <li>readOffset >= 0</li>
     *                                       <li>readOffset <= from.sizeInBytes</li>
     *                                       <li>readOffset + size <= from.sizeInBytes</li>
     *                                       <li>writeOffset >= 0</li>
     *                                       <li>writeOffset <= to.sizeInBytes</li>
     *                                       <li>writeOffset + size <= to.sizeInBytes</li>
     *                                       </ul>
     */
    public static void copy(
            final @NotNull GLBuffer from, final @NotNull GLBuffer to,
            final long readOffset, final long writeOffset, final long size
    ) throws IllegalStateException, UnsupportedOperationException, IllegalArgumentException {
        from.ensureCreated();
        from.ensureStorageCreated();
        to.ensureCreated();
        to.ensureStorageCreated();

        if (size < 0) {
            throw new IllegalArgumentException(
                    "The size of bytes to copy must not be negative!\n" +
                            GLBuffer.copyArgsAsString(from.sizeInBytes, to.sizeInBytes, readOffset, writeOffset, size)
            );
        }
        if (readOffset < 0) {
            throw new IllegalArgumentException(
                    "The readOffset must not be negative!\n" +
                            GLBuffer.copyArgsAsString(from.sizeInBytes, to.sizeInBytes, readOffset, writeOffset, size)
            );
        }
        if (readOffset > from.sizeInBytes) {
            throw new IllegalArgumentException(
                    "The readOffset must be smaller then the size of the \"from\" buffers data store!\n" +
                            GLBuffer.copyArgsAsString(from.sizeInBytes, to.sizeInBytes, readOffset, writeOffset, size)
            );
        }
        if (readOffset + size > from.sizeInBytes) {
            throw new IllegalArgumentException(
                    "(readOffset + size) must be smaller then the from-size (\"from\" buffers data store size)!\n" +
                            GLBuffer.copyArgsAsString(from.sizeInBytes, to.sizeInBytes, readOffset, writeOffset, size)
            );
        }
        if (writeOffset < 0) {
            throw new IllegalArgumentException(
                    "The writeOffset must not be negative!\n" +
                            GLBuffer.copyArgsAsString(from.sizeInBytes, to.sizeInBytes, readOffset, writeOffset, size)
            );
        }
        if (writeOffset > to.sizeInBytes) {
            throw new IllegalArgumentException(
                    "The writeOffset must be smaller then the size of the \"to\" buffers data store!\n" +
                            GLBuffer.copyArgsAsString(from.sizeInBytes, to.sizeInBytes, readOffset, writeOffset, size)
            );
        }
        if (writeOffset + size > to.sizeInBytes) {
            throw new IllegalArgumentException(
                    "(writeOffset + size) must be smaller then the to-size (\"to\" buffers data store size)!\n" +
                            GLBuffer.copyArgsAsString(from.sizeInBytes, to.sizeInBytes, readOffset, writeOffset, size)
            );
        }

        // DSA available.
        if (GLUtil.isContextAtLeast(4, 5)) {
            GL45.glCopyNamedBufferSubData(from.buffer, to.buffer, readOffset, writeOffset, size);
        } else if (GLUtil.getCaps().GL_ARB_direct_state_access) {
            ARBDirectStateAccess.glCopyNamedBufferSubData(from.buffer, to.buffer, readOffset, writeOffset, size);
        } else if (GLUtil.getCaps().GL_EXT_direct_state_access) {
            EXTDirectStateAccess.glNamedCopyBufferSubDataEXT(from.buffer, to.buffer, readOffset, writeOffset, size);
        }

        // Calculate the read and write targets to use.
        final @NotNull GLBufferTarget readTarget, writeTarget;
        // Bind both buffers to the COPY_READ and COPY_WRITE binding points respectively if they operate at the same
        // binding point.
        if (from.target.id == to.target.id) {
            from.bindAsCopyReadBuffer();
            readTarget = GLBufferTarget.GL_COPY_READ_BUFFER;
            to.bindAsCopyWriteBuffer();
            writeTarget = GLBufferTarget.GL_COPY_WRITE_BUFFER;
        }
        // Otherwise only bind the buffers which are not already bound to their actual target to one of the helper
        // targets (COPY_READ and COPY_WRITE).
        else {
            if (from.isBound()) {
                readTarget = from.target;
            } else {
                from.bindAsCopyReadBuffer();
                readTarget = GLBufferTarget.GL_COPY_READ_BUFFER;
            }
            if (to.isBound()) {
                writeTarget = to.target;
            } else {
                to.bindAsCopyWriteBuffer();
                writeTarget = GLBufferTarget.GL_COPY_READ_BUFFER;
            }
        }

        // DSA not available. => Perform a copy from readTarget to writeTarget.
        if (GLUtil.isContextAtLeast(3, 1)) {
            GL31.glCopyBufferSubData(readTarget.id, writeTarget.id, readOffset, writeOffset, size);
        } else if (GLUtil.getCaps().GL_ARB_copy_buffer) {
            ARBCopyBuffer.glCopyBufferSubData(readTarget.id, writeTarget.id, readOffset, writeOffset, size);
        }

        // Buffer copy not supported.
        GLUtil.throwNoPathError();
    }

    /**
     * @param other       The buffer to copy into.
     * @param readOffset  The starting position in {@code this} buffer from which the copy should start.
     * @param writeOffset The starting position in the {@code other} buffer at which the data should be copied.
     * @param size        The amount of data to copy.
     * @throws IllegalStateException         If either this or the {@code other} buffer or their data store was not created.
     * @throws UnsupportedOperationException If the current OpenGL context is unable to perform this operation.
     * @see GLBuffer#copy(GLBuffer, GLBuffer, long, long, long)
     */
    public void copyTo(final @NotNull GLBuffer other, final long readOffset, final long writeOffset, final long size)
            throws IllegalStateException, UnsupportedOperationException {
        GLBuffer.copy(this, other, readOffset, writeOffset, size);
    }

    /**
     * @param other       The buffer to copy from.
     * @param readOffset  The starting position in the {@code other} buffer from which the copy should start.
     * @param writeOffset The starting position in {@code this} buffer at which the data should be copied.
     * @param size        The amount of data to copy.
     * @see GLBuffer#copy(GLBuffer, GLBuffer, long, long, long)
     */
    public void copyFrom(final @NotNull GLBuffer other, final long readOffset, final long writeOffset, final long size)
            throws IllegalStateException, UnsupportedOperationException {
        GLBuffer.copy(other, this, readOffset, writeOffset, size);
    }

    /**
     * At OpenGL >4.5: The buffer must not be bound to allow the mapping.
     * At OpenGL <4.5: The buffer must be bound first!
     *
     * @return The ByteBuffer at which new data can be written.
     * @throws IllegalStateException         If this buffer is not created.
     * @throws UnsupportedOperationException If this buffer can not be mapped.
     */
    @Override
    public @NotNull ByteBuffer mapWriteOnly() throws IllegalStateException, UnsupportedOperationException {
        ensureCreated();

        if (hasPersistentFlag) {
            if (mapping == null) {
                _mapWriteOnly();
            }
            mapping.rewind();
            return mapping;
        } else {
            return _mapWriteOnly();
        }
    }

    /**
     * @return The mapped buffer on success or null..
     * @throws IllegalStateException         If this buffer is not created.
     * @throws UnsupportedOperationException If this buffer can not be mapped.
     */
    private @NotNull ByteBuffer _mapWriteOnly() throws IllegalStateException, UnsupportedOperationException {
        // DSA available.
        if (GLUtil.isContextAtLeast(4, 5)) {
            mapping = GL45.glMapNamedBufferRange(
                    buffer, BUFFER_START, sizeInBytes, combinedStorageFlags
            );
        } else if (GLUtil.getCaps().GL_ARB_direct_state_access) {
            mapping = ARBDirectStateAccess.glMapNamedBufferRange(
                    buffer, BUFFER_START, sizeInBytes, combinedStorageFlags
            );
        } else if (GLUtil.getCaps().GL_EXT_direct_state_access) {
            mapping = EXTDirectStateAccess.glMapNamedBufferRangeEXT(
                    buffer, BUFFER_START, sizeInBytes, combinedStorageFlags
            );
        }
        // DSA not available.
        else if (GLUtil.isContextAtLeast(3, 0)) {
            ensureBound();
            mapping = GL30.glMapBufferRange(
                    target.id, BUFFER_START, sizeInBytes, combinedStorageFlags
            );
        } else {
            GLUtil.throwNoPathError();
        }

        //noinspection ConstantConditions
        return mapping;
    }

    /**
     * DSA available: The buffer must not be bound to allow the un-mapping.
     * DSA not available: The buffer must be bound first!
     *
     * @throws IllegalStateException If this buffer or its data store is not created.
     */
    public void unmap() throws IllegalStateException {
        ensureCreated();
        ensureStorageCreated();

        if (!hasPersistentFlag) {
            forceUnmap();
        }
        // Otherwise keep the buffer mapped, as it is a persistent buffer.
    }

    public void forceUnmap() {
        if (GLUtil.isContextAtLeast(4, 5)) {
            GL45.glUnmapNamedBuffer(buffer);
        } else {
            ensureBound();
            GL15.glUnmapBuffer(target.id);
        }
        mapping = null;
    }

    /**
     * Must only be called when this buffer is not created.
     *
     * @param target The new target of this buffer.
     * @throws IllegalStateException If this buffer is currently created.
     */
    public void setTarget(final @NotNull GLBufferTarget target) throws IllegalStateException {
        ensureNotCreated();
        this.target = target;
    }

    /**
     * @return The GL target to which this buffer should be bound.
     */
    public @NotNull GLBufferTarget getTarget() {
        return target;
    }

    /**
     * IMPORTANT: The buffers {@link GLBuffer#isCreated()}} method must return true, in order for this function to
     * not fail with an IllegalStateException!
     * An inactive buffer (not currently initialized) has no valid OpenGL handle!
     *
     * @return The handle of this buffer.
     * @throws IllegalStateException If this buffer is not created.
     */
    public int getHandle() throws IllegalStateException {
        ensureCreated();
        return buffer;
    }

}
