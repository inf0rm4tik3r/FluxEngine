package de.colibriengine.graphics.opengl.buffer;

import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.ARBBufferStorage;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL44;

import java.util.ArrayList;
import java.util.List;

public enum GLBufferStorageFlag {

    GL_MAP_READ_BIT(GL30.GL_MAP_READ_BIT),
    GL_MAP_WRITE_BIT(GL30.GL_MAP_WRITE_BIT),
    GL_MAP_COHERENT_BIT(GL44.GL_MAP_COHERENT_BIT),
    GL_MAP_COHERENT_BIT_ARB(ARBBufferStorage.GL_MAP_COHERENT_BIT),
    GL_MAP_PERSISTENT_BIT(GL44.GL_MAP_PERSISTENT_BIT),
    GL_MAP_PERSISTENT_BIT_ARB(ARBBufferStorage.GL_MAP_PERSISTENT_BIT);

    /**
     * Immutable list view of the constants of this enum.
     */
    public static final @NotNull List<GLBufferStorageFlag> values = List.of(GLBufferStorageFlag.values());

    /**
     * Stores the amount of available enum values.
     */
    public static final int SIZE = GLBufferStorageFlag.values().length;

    /**
     * The value of this constant in OpenGL.
     */
    public final int id;

    GLBufferStorageFlag(final int id) {
        this.id = id;
    }

    public static int combine(final @NotNull GLBufferStorageFlag... storageFlags) {
        // Return 0 if no flags should be combine.
        if (storageFlags.length == 0) {
            return 0;
        }
        // Load first flag into result variable.
        int orResult = storageFlags[0].id;
        // And "or" that variable with every other flag.
        for (int i = 1; i < storageFlags.length; i++) {
            orResult |= storageFlags[i].id;
        }
        return orResult;
    }

    public static int combine(final @NotNull ArrayList<GLBufferStorageFlag> storageFlags) {
        // Return 0 if no flags should be combine.
        if (storageFlags.isEmpty()) {
            return 0;
        }
        // Load first flag into result variable.
        int orResult = storageFlags.get(0).id;
        // And "or" that variable with every other flag.
        for (int i = 1; i < storageFlags.size(); i++) {
            orResult |= storageFlags.get(i).id;
        }
        return orResult;
    }

    /**
     * Get the enum which corresponds to the specified ordinal.
     * Time complexity: O(1)
     *
     * @param ordinal The ordinal value of which the corresponding enum value should be returned.
     * @return The GLBufferStorageFlags of ordinal {@code ordinal}.
     * @throws IllegalArgumentException If the given ordinal matches none of the enum constants.
     */
    public static @NotNull GLBufferStorageFlag fromOrdinal(final int ordinal) throws IllegalArgumentException {
        if (ordinal < 0 || ordinal >= values.size()) {
            throw new IllegalArgumentException("There is no enum with ordinal: " + ordinal);
        }
        return values.get(ordinal);
    }

    /**
     * Get the enum which corresponds to the specified id.
     * Time complexity: O(n), where n := Amount of values in this enum.
     *
     * @param id The id of which the corresponding enum value should be returned.
     * @return The GLBufferStorageFlags with {@code id}.
     * @throws IllegalArgumentException If the given id does not match any of the id's of the enum constants.
     */
    public static @NotNull GLBufferStorageFlag fromID(final int id) throws IllegalArgumentException {
        for (final GLBufferStorageFlag current : values) {
            if (current.id == id) {
                return current;
            }
        }
        throw new IllegalArgumentException("There is no enum with ID: " + id);
    }

}
