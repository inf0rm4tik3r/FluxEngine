package de.colibriengine.graphics.opengl.buffer;

import java.util.List;

import org.lwjgl.opengl.ARBIndirectParameters;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL40;
import org.lwjgl.opengl.GL42;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GL46;

/**
 * <table summary="available targets">
 * <tr>
 * <td>{@link GL15#GL_ARRAY_BUFFER ARRAY_BUFFER}</td>
 * <td>{@link GL15#GL_ELEMENT_ARRAY_BUFFER ELEMENT_ARRAY_BUFFER}</td>
 * <td>{@link GL21#GL_PIXEL_PACK_BUFFER PIXEL_PACK_BUFFER}</td>
 * <td>{@link GL21#GL_PIXEL_UNPACK_BUFFER PIXEL_UNPACK_BUFFER}</td>
 * </tr>
 * <tr>
 * <td>{@link GL30#GL_TRANSFORM_FEEDBACK_BUFFER TRANSFORM_FEEDBACK_BUFFER}</td>
 * <td>{@link GL31#GL_UNIFORM_BUFFER UNIFORM_BUFFER}</td>
 * <td>{@link GL31#GL_TEXTURE_BUFFER TEXTURE_BUFFER}</td>
 * <td>{@link GL31#GL_COPY_READ_BUFFER COPY_READ_BUFFER}</td>
 * </tr>
 * <tr>
 * <td>{@link GL31#GL_COPY_WRITE_BUFFER COPY_WRITE_BUFFER}</td>
 * <td>{@link GL40#GL_DRAW_INDIRECT_BUFFER DRAW_INDIRECT_BUFFER}</td>
 * <td>{@link GL42#GL_ATOMIC_COUNTER_BUFFER ATOMIC_COUNTER_BUFFER}</td>
 * <td>{@link GL43#GL_DISPATCH_INDIRECT_BUFFER DISPATCH_INDIRECT_BUFFER}</td>
 * </tr>
 * <tr>
 * <td>{@link GL43#GL_SHADER_STORAGE_BUFFER SHADER_STORAGE_BUFFER}</td>
 * <td>{@link ARBIndirectParameters#GL_PARAMETER_BUFFER_ARB PARAMETER_BUFFER_ARB}</td>
 * </tr>
 * </table>
 */
public enum GLBufferTarget {
    
    /**
     * From GL 1.5
     */
    GL_ARRAY_BUFFER(GL15.GL_ARRAY_BUFFER),
    
    /**
     * From GL 1.5
     */
    GL_ELEMENT_ARRAY_BUFFER(GL15.GL_ELEMENT_ARRAY_BUFFER),
    
    /**
     * From GL 2.1
     */
    GL_PIXEL_PACK_BUFFER(GL21.GL_PIXEL_PACK_BUFFER),
    
    /**
     * From GL 2.1
     */
    GL_PIXEL_UNPACK_BUFFER(GL21.GL_PIXEL_UNPACK_BUFFER),
    
    /**
     * From GL 3.0
     */
    GL_TRANSFORM_FEEDBACK_BUFFER(GL30.GL_TRANSFORM_FEEDBACK_BUFFER),
    
    /**
     * From GL 3.1
     */
    GL_UNIFORM_BUFFER(GL31.GL_UNIFORM_BUFFER),
    
    /**
     * From GL 3.1
     */
    GL_TEXTURE_BUFFER(GL31.GL_TEXTURE_BUFFER),
    
    /**
     * From GL 3.1
     */
    GL_COPY_READ_BUFFER(GL31.GL_COPY_READ_BUFFER),
    
    /**
     * From GL 3.1
     */
    GL_COPY_WRITE_BUFFER(GL31.GL_COPY_WRITE_BUFFER),
    
    /**
     * From GL 4.0
     */
    GL_DRAW_INDIRECT_BUFFER(GL40.GL_DRAW_INDIRECT_BUFFER),
    
    /**
     * From GL 4.2
     */
    GL_ATOMIC_COUNTER_BUFFER(GL42.GL_ATOMIC_COUNTER_BUFFER),
    
    /**
     * From GL 4.3
     */
    GL_DISPATCH_INDIRECT_BUFFER(GL43.GL_DISPATCH_INDIRECT_BUFFER),
    
    /**
     * From GL 4.3
     */
    GL_SHADER_STORAGE_BUFFER(GL43.GL_SHADER_STORAGE_BUFFER),
    
    /**
     * From ARBIndirectParameters
     */
    GL_PARAMETER_BUFFER_ARB(ARBIndirectParameters.GL_PARAMETER_BUFFER_ARB),
    
    /**
     * From GL 4.6
     */
    GL_PARAMETER_BUFFER(GL46.GL_PARAMETER_BUFFER);
    
    /**
     * Immutable list view of the constants of this enum.
     */
    public static final List<GLBufferTarget> values = List.of(GLBufferTarget.values());
    
    /**
     * Stores the amount of available OpenGL buffer targets.
     */
    public static final int SIZE = GLBufferTarget.values().length;
    
    /**
     * The value of this constant in OpenGL.
     */
    public final int id;
    
    GLBufferTarget(final int id) {
        this.id = id;
    }
    
    /**
     * Get the enum which corresponds to the specified ordinal.
     * Time complexity: O(1)
     *
     * @param ordinal
     *     The ordinal value of which the corresponding enum value should be returned.
     *
     * @return The GLBufferTarget of ordinal {@code ordinal}.
     */
    public static GLBufferTarget fromOrdinal(final int ordinal) {
        if (ordinal < 0 || ordinal >= values.size()) {
            throw new IllegalArgumentException("There is not enum with ordinal: " + ordinal);
        }
        return values.get(ordinal);
    }
    
    /**
     * Get the enum which corresponds to the specified id.
     * Time complexity: O(n), where n := Amount of values in this enum.
     *
     * @param id
     *     The id of which the corresponding enum value should be returned.
     *
     * @return The GLBufferTarget with {@code id}.
     */
    public static GLBufferTarget fromID(final int id) {
        for (final GLBufferTarget current : values) {
            if (current.id == id) {
                return current;
            }
        }
        throw new IllegalArgumentException("There is not enum with ID: " + id);
    }
    
}
