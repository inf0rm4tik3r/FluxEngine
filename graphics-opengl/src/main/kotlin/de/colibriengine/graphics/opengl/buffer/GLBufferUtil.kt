package de.colibriengine.graphics.opengl.buffer

import java.nio.ByteBuffer

inline fun GLBuffer.modify(modifyAction: (ByteBuffer) -> Unit) {
    val bufferData: ByteBuffer = mapWriteOnly()
    modifyAction.invoke(bufferData)
    unmap()
}

inline fun modifyGLBuffers(
    buffer1: GLBuffer,
    modifyAction: (ByteBuffer) -> Unit
) {
    val buffer1Data: ByteBuffer = buffer1.mapWriteOnly()
    modifyAction.invoke(buffer1Data)
    buffer1.unmap()
}

inline fun modifyGLBuffers(
    buffer1: GLBuffer,
    buffer2: GLBuffer,
    modifyAction: (ByteBuffer, ByteBuffer) -> Unit
) {
    val buffer1Data: ByteBuffer = buffer1.mapWriteOnly()
    val buffer2Data: ByteBuffer = buffer2.mapWriteOnly()
    modifyAction.invoke(buffer1Data, buffer2Data)
    buffer1.unmap()
    buffer2.unmap()
}

inline fun modifyGLBuffers(
    buffer1: GLBuffer,
    buffer2: GLBuffer,
    buffer3: GLBuffer,
    modifyAction: (ByteBuffer, ByteBuffer, ByteBuffer) -> Unit
) {
    val buffer1Data: ByteBuffer = buffer1.mapWriteOnly()
    val buffer2Data: ByteBuffer = buffer2.mapWriteOnly()
    val buffer3Data: ByteBuffer = buffer3.mapWriteOnly()
    modifyAction.invoke(buffer1Data, buffer2Data, buffer3Data)
    buffer1.unmap()
    buffer2.unmap()
    buffer3.unmap()
}

inline fun modifyGLBuffers(
    buffer1: GLBuffer,
    buffer2: GLBuffer,
    buffer3: GLBuffer,
    buffer4: GLBuffer,
    modifyAction: (ByteBuffer, ByteBuffer, ByteBuffer, ByteBuffer) -> Unit
) {
    val buffer1Data: ByteBuffer = buffer1.mapWriteOnly()
    val buffer2Data: ByteBuffer = buffer2.mapWriteOnly()
    val buffer3Data: ByteBuffer = buffer3.mapWriteOnly()
    val buffer4Data: ByteBuffer = buffer4.mapWriteOnly()
    modifyAction.invoke(buffer1Data, buffer2Data, buffer3Data, buffer4Data)
    buffer1.unmap()
    buffer2.unmap()
    buffer3.unmap()
    buffer4.unmap()
}

inline fun modifyGLBuffers(
    buffers: Array<GLBuffer>,
    modifyAction: (Array<ByteBuffer>) -> Unit
) {
    modifyAction.invoke(Array(buffers.size) {
        buffers[it].mapWriteOnly()
    })
    for (buffer in buffers) {
        buffer.unmap()
    }
}
