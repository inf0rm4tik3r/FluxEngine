package de.colibriengine.graphics.opengl.buffer

import de.colibriengine.graphics.DataType
import org.lwjgl.opengl.GL11

enum class GLDataType(
    /** The value of this constant in OpenGL. */
    val id: Int
) {

    BYTE(GL11.GL_BYTE),
    TWO_BYTES(GL11.GL_2_BYTES),
    THREE_BYTES(GL11.GL_3_BYTES),
    FOUR_BYTES(GL11.GL_4_BYTES),
    UNSIGNED_BYTE(GL11.GL_UNSIGNED_BYTE),

    SHORT(GL11.GL_SHORT),
    UNSIGNED_SHORT(GL11.GL_UNSIGNED_SHORT),

    INT(GL11.GL_INT),
    UNSIGNED_INT(GL11.GL_UNSIGNED_INT),

    FLOAT(GL11.GL_FLOAT),
    DOUBLE(GL11.GL_DOUBLE);

    companion object {
        fun from(dataType: DataType): GLDataType {
            return when (dataType) {
                DataType.BYTE -> BYTE
                DataType.TWO_BYTES -> TWO_BYTES
                DataType.THREE_BYTES -> THREE_BYTES
                DataType.FOUR_BYTES -> FOUR_BYTES
                DataType.UNSIGNED_BYTE -> UNSIGNED_BYTE
                DataType.SHORT -> SHORT
                DataType.UNSIGNED_SHORT -> UNSIGNED_SHORT
                DataType.INT -> INT
                DataType.UNSIGNED_INT -> UNSIGNED_INT
                DataType.FLOAT -> FLOAT
                DataType.DOUBLE -> DOUBLE
            }
        }
    }

}
