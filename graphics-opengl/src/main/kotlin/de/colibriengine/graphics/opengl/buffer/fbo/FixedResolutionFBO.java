package de.colibriengine.graphics.opengl.buffer.fbo;

import de.colibriengine.graphics.opengl.texture.GLTexelDataFormat;
import de.colibriengine.graphics.opengl.texture.GLTexelDataType;
import de.colibriengine.graphics.texture.ImageFormat;
import de.colibriengine.graphics.texture.TextureFilterType;
import de.colibriengine.math.vector.vec2i.StdVec2i;
import de.colibriengine.math.vector.vec2i.Vec2i;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import org.jetbrains.annotations.NotNull;

public abstract class FixedResolutionFBO extends InvalidatableFBO {

    /**
     * The {@link FixedResolutionFBO#resolution} variable must be set (both x and y) to be equal to this variable if
     * there is no resolution set!
     */
    private static final int RESOLUTION_UNDEFINED = Integer.MIN_VALUE;

    /**
     * Message to display in an exception if the resolution is not set at a point were it was necessary to be set.
     *
     * @see FixedResolutionFBO#ensureResolutionIsSet()
     */
    private static final String MSG_RESOLUTION_UNDEFINED = "The texture resolution was not specified!";

    /**
     * The width and height of the contained textures. Can be changed, but keep in mind that this leeds to a recreation
     * of this {@code FBO} instance!
     * Consider using multiple {@code FBO} instances if different resolutions should be supported.
     */
    private final @NotNull Vec2i resolution;

    /**
     * Constructs a new FBO without initializing it (no OpenGL initialization!).
     * The {@link FixedResolutionFBO#setResolution(int, int)} method must be called manually.
     * The {@link GLFBO#init()} function must be called manually.
     */
    public FixedResolutionFBO() {
        resolution = new StdVec2i().set(RESOLUTION_UNDEFINED, RESOLUTION_UNDEFINED);
    }

    /**
     * Constructs and initializes a new FBO.
     * Manually calling {@link GLFBO#init()} after this is not necessary.
     *
     * @param textureWidth  The width of the FBO textures.
     * @param textureHeight The height of the FBO textures.
     */
    public FixedResolutionFBO(final int textureWidth, final int textureHeight) {
        resolution = new StdVec2i().set(textureWidth, textureHeight);
    }

    protected void addTexture(
            final @NotNull ImageFormat internalFormat,
            final @NotNull GLTexelDataFormat texelDataFormat, final @NotNull GLTexelDataType texelDataType,
            final @NotNull TextureFilterType minFilter, final @NotNull TextureFilterType magFilter
    ) {
        super.addTexture(resolution, internalFormat, texelDataFormat, texelDataType, minFilter, magFilter);
    }

    protected void addTexture(
            final @NotNull ImageFormat internalFormat,
            final @NotNull TextureFilterType minFilter
    ) {
        super.addTexture(resolution, internalFormat, minFilter);
    }

    protected void initDepthTexture(
            final @NotNull ImageFormat internalFormat,
            final @NotNull GLFBOAttachment attachment) {
        super.initDepthTexture(resolution, internalFormat, attachment);
    }

    public void addRenderbuffer(final @NotNull ImageFormat imageFormat) {
        super.addRenderbuffer(resolution, imageFormat);
    }

    protected void initDepthStencilRenderbuffer(final @NotNull ImageFormat format) {
        super.initDepthStencilRenderbuffer(resolution, format);
    }

    /**
     * Checks if this object is in a state which allows the initialization of this object through
     * {@link GLFBO#init()}.
     *
     * @throws IllegalStateException If this object is in an invalid state.
     */
    protected void ensureResolutionIsSet() throws IllegalStateException {
        if (resolution.getX() == RESOLUTION_UNDEFINED || resolution.getY() == RESOLUTION_UNDEFINED) {
            throw new IllegalStateException(MSG_RESOLUTION_UNDEFINED);
        }
    }

    /**
     * Updates the resolution of this FBO.
     * A call to this method will invalidate this object!
     *
     * @param textureWidth  The new x-resolution to use.
     * @param textureHeight The new y-resolution to use.
     * @see FixedResolutionFBO#invalidate()
     */
    public void setResolution(final int textureWidth, final int textureHeight) {
        resolution.set(textureWidth, textureHeight);
        invalidate();
    }

    /**
     * Updates the resolution of this FBO.
     * A call to this method will invalidate this object!
     *
     * @param newResolution The new resolution to use.
     * @see FixedResolutionFBO#invalidate()
     */
    public void setResolution(final @NotNull Vec2iAccessor newResolution) {
        resolution.set(newResolution);
        invalidate();
    }

    /**
     * @return The width of the textures of this {@code FBO} instance.
     */
    public int xResolution() {
        return resolution.getX();
    }

    /**
     * Updates the texture width to bind.
     * A call to this method will invalidate this object!
     *
     * @param xResolution The new texture width to use.
     * @see FixedResolutionFBO#invalidate()
     */
    public void setXResolution(final int xResolution) {
        resolution.setX(xResolution);
        invalidate();
    }

    /**
     * @return The height of the textures of this {@code FBO} instance.
     */
    public int yResolution() {
        return resolution.getY();
    }

    /**
     * Updates the texture height to bind.
     * A call to this method will invalidate this object!
     *
     * @param yResolution The new texture height to use.
     * @see FixedResolutionFBO#invalidate()
     */
    public void setYResolution(final int yResolution) {
        resolution.setY(yResolution);
        invalidate();
    }

    /**
     * @return The current resolution of this FBO.
     */
    public @NotNull Vec2iAccessor getResolution() {
        return resolution;
    }

}
