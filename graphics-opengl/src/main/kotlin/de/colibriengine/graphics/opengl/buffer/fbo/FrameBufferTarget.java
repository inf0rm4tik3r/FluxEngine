package de.colibriengine.graphics.opengl.buffer.fbo;

import org.lwjgl.opengl.GL30;

/**
 * Provides the targets at which a frame buffer object ({@link GLFBO}) can be bound to.
 */
public enum FrameBufferTarget {
    
    /**
     * The FBO will be available for both read and write operations.
     */
    DRAW_AND_READ(GL30.GL_FRAMEBUFFER),
    
    /**
     * The FBO will be available for write operations.
     */
    DRAW(GL30.GL_DRAW_FRAMEBUFFER),
    
    /**
     * The FBO will be available for read operations.
     */
    READ(GL30.GL_READ_FRAMEBUFFER);
    
    /**
     * The value of this constant in OpenGL.
     */
    public final int id;
    
    FrameBufferTarget(final int id) {
        this.id = id;
    }
    
}
