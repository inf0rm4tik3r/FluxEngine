package de.colibriengine.graphics.opengl.buffer.fbo

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.fbo.FBO
import de.colibriengine.graphics.fbo.FBOTextureRendererFactory
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.shader.Shader
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.util.IntList

class GLFBOTextureRendererFactory : FBOTextureRendererFactory<GLFBOTextureRenderer> {

    override fun create(
        ecs: ECS,
        meshFactory: MeshFactory,
        srcFBO: FBO?,
        srcInputTextureIndices: IntList,
        srcSamplePos: Vec2f,
        srcSampleDimension: Vec2f,
        destFBO: FBO?,
        destOutputTextureIndices: IntList,
        destWritePos: Vec2f,
        destWriteDimension: Vec2f,
        shader: Shader,
        shaderSetup: (GLFBOTextureRenderer) -> Unit
    ): GLFBOTextureRenderer {
        return GLFBOTextureRenderer(
            ecs, meshFactory,
            srcFBO, srcInputTextureIndices, srcSamplePos, srcSampleDimension,
            destFBO, destOutputTextureIndices, destWritePos, destWriteDimension,
            shader,
            shaderSetup
        )
    }

}
