package de.colibriengine.graphics.opengl.context;

import de.colibriengine.exception.OutOfSyncException;
import de.colibriengine.graphics.Creatable;
import de.colibriengine.graphics.opengl.GLGet;
import de.colibriengine.graphics.opengl.GLSet;
import de.colibriengine.graphics.opengl.GLUtil;
import de.colibriengine.graphics.opengl.buffer.GLBuffer;
import de.colibriengine.graphics.opengl.buffer.GLBufferTarget;
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBO;
import de.colibriengine.graphics.opengl.buffer.fbo.FrameBufferTarget;
import de.colibriengine.graphics.opengl.buffer.rbo.RBO;
import de.colibriengine.graphics.opengl.buffer.vao.VAO;
import de.colibriengine.graphics.opengl.texture.GLTextureTarget;
import de.colibriengine.graphics.opengl.texture.OpenGlTexture;
import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader;
import de.colibriengine.logging.LogBehaviour;
import de.colibriengine.logging.LogUtil;
import de.colibriengine.math.vector.vec4i.StdVec4i;
import de.colibriengine.math.vector.vec4i.Vec4i;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

/**
 * Used to keep track of the current state of a OpenGL context inside this Java application.
 * <ul>
 * <li>Removes the need to constantly bother the driver with questions like: "What's currently bound to X, Y, ..?".</li>
 * <li>Enables greater debugging capabilities for individual subsystems.</li>
 * </ul>
 * <p>
 * Stores information about the following topics:
 * <ul>
 * <li>Active program object</li>
 * <li>Currently bound VAO</li>
 * <li>Currently bound buffers (for each available: {@link GLBufferTarget})</li>
 * <li>Currently bound FBO for reading</li>
 * <li>Currently bound FBO for drawing</li>
 * <li>Currently set FBO readBuffers</li>
 * <li>Currently set FBO drawBuffers</li>
 * <li>Currently active texture unit</li>
 * <li>Currently bound textures (for each available: {@link GLTextureTarget})</li>
 * </ul>
 * <p>
 * TODO: implements Creatable really necessary
 *
 * @since ColibriEngine 0.0.7.0
 */
@SuppressWarnings("WeakerAccess")
public class ContextStateTracker implements Creatable {

    private static final Logger LOG = LogUtil.getLogger(ContextStateTracker.class);

    /**
     * Debugging is used to check if this {@code ContextStateTracker} is in sync with the current context state.
     * These sync checks will be executed before or after any modification was made to a
     * {@code ContextStateTracker} instance.
     * Enabling the debugging ensures at runtime that the {@code ContextStateTracker} instances are always "up to date"
     * and that the information stored in them can be used as if they would come directly from the context. But keep
     * in mind that this will cause an immense overhead!
     */
    public static final boolean DEBUG = false;

    private static final @NotNull LogBehaviour LOG_BEHAVIOUR = LogBehaviour.THROW_EXCEPTION;

    private static final @NotNull String MSG_POSSIBLY_BAD_INPUT = "Possibly bad input detected";

    /**
     * Stores if this object is currently "created".
     */
    private boolean created;

    /**
     * The currently active shader program.
     */
    private int activeShaderProgram;

    /**
     * The index of the currently bound VAO.
     */
    private int boundVAO;

    /**
     * Stores the indices of the currently bound buffers.
     * This array will be initialized to a length equal to {@link GLBufferTarget#SIZE}. Which means it will have enough
     * slots to store the binding buffer binding target made available by OpenGL.
     */
    private int[] boundBuffers;

    /**
     * The framebuffer currently bound for write operations.
     * Might be equal to {@link ContextStateTracker#boundReadFramebuffer}.
     */
    private int boundDrawFramebuffer;

    /**
     * The framebuffer currently bound for read operations.
     * Might be equal to {@link ContextStateTracker#boundDrawFramebuffer}.
     */
    private int boundReadFramebuffer;

    private int activeTextureUnit;

    private int[][] boundTextures;

    /**
     * The currently bound renderbuffer.
     */
    private int boundRenderbuffer;

    /**
     * The current viewport configuration.
     */
    private int
            viewportX,
            viewportY,
            viewportWidth,
            viewportHeight;

    private final Vec4i tmpVec4i = new StdVec4i();

    /**
     * Must not be called from outside this class!
     *
     * @param created Whether or not this object is created.
     */
    @Override
    public void setCreated(final boolean created) {
        this.created = created;
    }

    @Override
    public boolean isCreated() {
        return created;
    }

    /**
     * Initializes this ContextStateTracker to its initial state.
     *
     * @param contextInformation Contains information about the context for which this state tracker should be.
     */
    public void init(final @NotNull GLContextInformation contextInformation) {
        // No program is active.
        activeShaderProgram = AbstractOpenGLShader.NO_BOND;

        // There is no VAO bound.
        boundVAO = VAO.NO_BOND;

        // Create the array which stores the bound buffers in a size sufficient to hold the buffer references for all
        // available OpenGL buffer targets.
        boundBuffers = new int[GLBufferTarget.SIZE];
        // Every buffer target must be set as if there is no buffer bound.
        Arrays.fill(boundBuffers, GLBuffer.NO_BOND);

        // Create an array of size 2.
        boundDrawFramebuffer = GLFBO.DEFAULT_FRAMEBUFFER_ID;
        boundReadFramebuffer = GLFBO.DEFAULT_FRAMEBUFFER_ID;

        // Create an two dimensional array which stores the bound textures for each available texture unit.
        boundTextures = new int[contextInformation.getMaxTextureImageUnits()][GLTextureTarget.SIZE];

        // This CST is now active. Calls to its member functions are now allowed.
        this.created = true;
    }

    public void update() {

    }

    public void dump() {
        LOG.debug("ActiveShaderProgram: {}", activeShaderProgram);
        LOG.debug("BoundVAO: {}", boundVAO);
        LOG.debug("BoundBuffers: {}", Arrays.toString(boundBuffers));
        LOG.debug("BoundDrawFramebuffer: {}", boundDrawFramebuffer);
        LOG.debug("BoundReadFramebuffer: {}", boundReadFramebuffer);
        LOG.debug("ActiveTextureUnit: {}", activeTextureUnit);
        for (int i = 0; i < GLUtil.getContextInformation().getMaxTextureImageUnits(); i++) {
            LOG.debug("BoundTextures: unit({}) {}", i, Arrays.toString(boundTextures[i]));
        }
        LOG.debug("BoundRenderbuffer: {}", boundRenderbuffer);
    }

    /*
     *  VIEWPORT
     */

    @SuppressWarnings("Duplicates")
    public void setViewport(final int x, final int y, final int width, final int height) {
        assert created;
        assert !(x < 0) : "x must not be negative!";
        assert !(y < 0) : "x must not be negative!";
        assert !(width < 0) : "x must not be negative!";
        assert !(height < 0) : "x must not be negative!";

        viewportX = x;
        viewportY = y;
        viewportWidth = width;
        viewportHeight = height;
    }

    public int getViewportX() {
        assert created;
        return viewportX;
    }

    public int getViewportY() {
        assert created;
        return viewportY;
    }

    public int getViewportWidth() {
        assert created;
        return viewportWidth;
    }

    public int getViewportHeight() {
        assert created;
        return viewportHeight;
    }

    /*
     * PROGRAM
     */

    /**
     * Update the currently active shader program.
     *
     * @param activeShaderProgram The shader program which is now active.
     */
    public void setActiveShaderProgram(final int activeShaderProgram) {
        assert created;
        assert !(activeShaderProgram < AbstractOpenGLShader.NO_BOND) : "Negative shader program handle specified.";
        this.activeShaderProgram = activeShaderProgram;
        if (DEBUG) {
            LOG.debug("Active shader program: {}", activeShaderProgram);
            checkInSync();
        }
    }

    /**
     * @return Whether or not a shader program is currently active.
     */
    public boolean hasActiveShaderProgram() {
        assert created;
        if (DEBUG) {
            checkInSync();
        }
        return activeShaderProgram != AbstractOpenGLShader.NO_BOND;
    }

    public int getActiveShaderProgram() {
        assert created;
        if (DEBUG) {
            checkInSync();
        }
        return activeShaderProgram;
    }

    /*
     * VAO
     */

    /**
     * Stores which VAO is currently bound.
     *
     * @param boundVAO The OpenGL handle of the currently bound VAO.
     */
    public void setBoundVAO(final int boundVAO) {
        if (DEBUG) {
            ensureCreated();
            // Negative values most likely resulted from a logical error.
            if (boundVAO < VAO.NO_BOND) {
                error(MSG_POSSIBLY_BAD_INPUT + ": Negative VAO handle specified.");
            }
        }
        this.boundVAO = boundVAO;
        if (DEBUG) {
            LOG.debug("Bound VAO: {}", boundVAO);
            checkInSync();
        }
    }

    /**
     * @return Whether or not a VAO is currently bound.
     */
    public boolean hasBoundVAO() {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return boundVAO != VAO.NO_BOND;
    }

    /**
     * @return The index of the currently bound vertex array object (VAO). Might be {@link VAO#NO_BOND} if there is
     * no VAO bound.
     * @see ContextStateTracker#hasBoundVAO()
     */
    public int getBoundVAO() {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return boundVAO;
    }

    /*
     * BUFFER (general)
     */

    public void setBoundBufferAt(final @NotNull GLBufferTarget target, final int boundBuffer) {
        setBoundBufferAt(target, boundBuffer, true);
    }

    public void setBoundBufferAt(
            final @NotNull GLBufferTarget target, final int boundBuffer, final boolean allowSyncCheck
    ) {
        if (DEBUG) {
            ensureCreated();
            if (boundBuffer < GLBuffer.NO_BOND) {
                error(MSG_POSSIBLY_BAD_INPUT + ": Negative buffer handle specified.");
            }
        }
        boundBuffers[target.ordinal()] = boundBuffer;
        if (DEBUG) {
            LOG.debug("Bound buffer at {}: {}", target.name(), boundBuffer);
            if (allowSyncCheck) {
                checkInSync();
            }
        }
    }

    public boolean hasBoundBufferAt(final @NotNull GLBufferTarget target) {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return boundBuffers[target.ordinal()] != GLBuffer.NO_BOND;
    }

    public int getBoundBufferAt(final @NotNull GLBufferTarget target) {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return boundBuffers[target.ordinal()];
    }

    /*
     * FBO
     */

    public void setBoundFramebuffer(final @NotNull FrameBufferTarget target, final int boundFramebuffer) {
        if (DEBUG) {
            ensureCreated();
            if (boundFramebuffer < GLBuffer.NO_BOND) {
                error(MSG_POSSIBLY_BAD_INPUT + ": Negative framebuffer handle specified.");
            }
        }
        switch (target) {
            case DRAW_AND_READ:
                boundDrawFramebuffer = boundFramebuffer;
                boundReadFramebuffer = boundFramebuffer;
                break;
            case DRAW:
                boundDrawFramebuffer = boundFramebuffer;
                break;
            case READ:
                boundReadFramebuffer = boundFramebuffer;
                break;
            default:
                throw new AssertionError("Unknown FrameBufferTarget given.");
        }
        if (DEBUG) {
            LOG.debug("Bound {} framebuffer: {}", target.name(), boundFramebuffer);
            checkInSync();
        }
    }

    public void setBoundDrawFramebuffer(final int boundFramebuffer) {
        setBoundFramebuffer(FrameBufferTarget.DRAW, boundFramebuffer);
    }

    public void setBoundReadFramebuffer(final int boundFramebuffer) {
        setBoundFramebuffer(FrameBufferTarget.READ, boundFramebuffer);
    }

    public int getBoundDrawFramebuffer() {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return boundDrawFramebuffer;
    }

    public int getBoundReadFramebuffer() {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return boundReadFramebuffer;
    }

    /*
     * RBO
     */

    public void setBoundRenderbuffer(final int boundRenderbuffer) {
        if (DEBUG) {
            ensureCreated();
            if (boundRenderbuffer < GLBuffer.NO_BOND) {
                error(MSG_POSSIBLY_BAD_INPUT + ": Negative renderbuffer handle specified.");
            }
        }
        this.boundRenderbuffer = boundRenderbuffer;
        if (DEBUG) {
            LOG.debug("Bound renderbuffer: {}", boundRenderbuffer);
            checkInSync();
        }
    }

    public boolean hasBoundRenderbuffer() {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return boundRenderbuffer != RBO.NO_BOND;
    }

    public int getBoundRenderbuffer() {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return boundRenderbuffer;
    }


    /*
     * TEXTURE UNITS
     */

    public void setActiveTextureUnit(final int activeTextureUnit) {
        if (DEBUG) {
            ensureCreated();
        }

        if (activeTextureUnit < 0) {
            error(MSG_POSSIBLY_BAD_INPUT + ": Negative texture unit specified.");
        }

        this.activeTextureUnit = activeTextureUnit;
        //LOG.debug("Activated texture unit {}", textureUnit);

        if (DEBUG) {
            checkInSync();
        }
    }

    public int getActiveTextureUnit() {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return activeTextureUnit;
    }

    /*
     * TEXTURES
     */

    public void setBoundTexture(final int textureUnit, final @NotNull GLTextureTarget textureTarget, final int texture) {
        if (DEBUG) {
            ensureCreated();
            if (textureUnit < OpenGlTexture.TEXTURE_UNIT_0) {
                error(MSG_POSSIBLY_BAD_INPUT + ": Negative texture unit specified.");
            }
            if (texture < OpenGlTexture.NO_BOND) {
                error(MSG_POSSIBLY_BAD_INPUT + ": Negative texture handle specified.");
            }
        }
        boundTextures[textureUnit][textureTarget.ordinal()] = texture;
        if (DEBUG) {
            LOG.debug(
                    "Bound texture at unit {}, target {}({}): {}",
                    textureUnit, textureTarget.name(), textureTarget.ordinal(), texture
            );
            checkInSync();
        }
    }

    public int getBoundTexture(final int textureUnit, final @NotNull GLTextureTarget textureTarget) {
        if (DEBUG) {
            checkInSync();
            ensureCreated();
        }
        return boundTextures[textureUnit][textureTarget.ordinal()];
    }

    public void removeTextureBindings(final @NotNull GLTextureTarget textureTarget, final int texture) {
        if (DEBUG) {
            ensureCreated();
            if (texture < OpenGlTexture.NO_BOND) {
                error(MSG_POSSIBLY_BAD_INPUT + ": Negative texture handle specified.");
            }
        }
        // Remove all occurrences of the given texture handle.
        for (int unit = 0; unit < boundTextures.length; unit++) {
            for (int targetBinding = 0; targetBinding < boundTextures[unit].length; targetBinding++) {
                if (boundTextures[unit][targetBinding] == texture) {
                    boundTextures[unit][targetBinding] = OpenGlTexture.NO_BOND;
                }
            }
        }
        if (DEBUG) {
            checkInSync();
        }
    }

    /*
     * LOGGING
     */

    /**
     * Logs a message using the {@link LogBehaviour} {@link ContextStateTracker#LOG_BEHAVIOUR} defined in this class.
     *
     * @param customErrorMessage A custom message which will be added to the output.
     */
    private void error(final @NotNull String customErrorMessage) throws OutOfSyncException {
        switch (LOG_BEHAVIOUR) {
            case LOG_MESSAGE:
                LOG.warn(customErrorMessage);
                break;
            case LOG_EXCEPTION:
                new OutOfSyncException(customErrorMessage).printStackTrace();
                break;
            case THROW_EXCEPTION:
                throw new OutOfSyncException(customErrorMessage);
            default:
                throw new IllegalStateException("Unexpected switch fallthrough");
        }
    }

    /*
     * SYNC CHECKS
     */

    /**
     * Performs a full check over all cached context states.
     * Please note that this is an <b>EXTREMELY EXPENSIVE OPERATION</b>, as the whole GL context must be queried!
     *
     * @throws OutOfSyncException Whenever any state variable is not synchronized.
     */
    public void checkInSync() throws OutOfSyncException {
        checkViewportInSync();
        checkShaderProgramInSync();
        checkVAOInSync();
        checkBuffersInSync();
        checkFramebufferInSync();
        checkRenderbufferInSync();
        checkActiveTextureUnitInSync();
        checkTexturesInSync();
    }

    /**
     * Checks that the cached viewport dimensions are in sync with the current viewport in the current context.
     *
     * @throws OutOfSyncException Whenever any state variable is not synchronized.
     */
    private void checkViewportInSync() throws OutOfSyncException {
        Vec4i active = GLGet.viewport(tmpVec4i);
        if (viewportX != active.getX()) {
            throw new OutOfSyncException("viewportX", viewportX, active.getX());
        }
        if (viewportY != active.getY()) {
            throw new OutOfSyncException("viewportY", viewportY, active.getY());
        }
        if (viewportWidth != active.getZ()) {
            throw new OutOfSyncException("viewportWidth", viewportWidth, active.getZ());
        }
        if (viewportHeight != active.getW()) {
            throw new OutOfSyncException("viewportHeight", viewportHeight, active.getW());
        }
    }

    /**
     * Checks that the cached active shader program is in sync with the active shader program in the current context.
     *
     * @throws OutOfSyncException Whenever any state variable is not synchronized.
     */
    private void checkShaderProgramInSync() throws OutOfSyncException {
        final int active = GLGet.activeShaderProgram();
        if (activeShaderProgram != active) {
            throw new OutOfSyncException("Shader program", activeShaderProgram, active);
        }
    }

    /**
     * Checks if the cached VAO handle is in sync with the current context state.
     *
     * @throws OutOfSyncException Whenever any state variable is not synchronized.
     */
    private void checkVAOInSync() throws OutOfSyncException {
        final int current = GLGet.vertexArrayObjectBinding();
        if (boundVAO != current) {
            throw new OutOfSyncException("VAO", boundVAO, current);
        }
    }

    /**
     * Checks if the cached buffer handles are in sync with the current context state.
     *
     * @throws OutOfSyncException Whenever any state variable is not synchronized.
     */
    private void checkBuffersInSync() throws OutOfSyncException {
        for (int i = 0; i < boundBuffers.length; i++) {
            final GLBufferTarget target = GLBufferTarget.fromOrdinal(i);
            final int current = GLGet.bufferBinding(target);
            if (boundBuffers[i] != current) {
                throw new OutOfSyncException("Buffer binding: " + target.name(), boundBuffers[i], current);
            }
        }
    }

    /**
     * Checks that the cached bound draw and read framebuffers are in sync with the bound framebuffers in the
     * current context.
     *
     * @throws OutOfSyncException Whenever any state variable is not synchronized.
     */
    private void checkFramebufferInSync() throws OutOfSyncException {
        final int currentDrawFramebuffer = GLGet.drawFramebufferBinding();
        final int currentReadFramebuffer = GLGet.readFramebufferBinding();
        if (boundDrawFramebuffer != currentDrawFramebuffer) {
            throw new OutOfSyncException("DrawFramebuffer", boundDrawFramebuffer, currentDrawFramebuffer);
        }
        if (boundReadFramebuffer != currentReadFramebuffer) {
            throw new OutOfSyncException("ReadFramebuffer", boundReadFramebuffer, currentReadFramebuffer);
        }
    }

    /**
     * Checks that the cached bound renderbuffer is in sync with the bound renderbuffer in the current context.
     *
     * @throws OutOfSyncException Whenever any state variable is not synchronized.
     */
    private void checkRenderbufferInSync() throws OutOfSyncException {
        final int current = GLGet.renderbufferBinding();
        if (boundRenderbuffer != current) {
            throw new OutOfSyncException("Renderbuffer", boundRenderbuffer, current);
        }
    }

    /**
     * Checks that the cached active texture
     *
     * @throws OutOfSyncException Whenever any state variable is not synchronized.
     */
    private void checkActiveTextureUnitInSync() throws OutOfSyncException {
        final int current = GLGet.activeTextureUnit();
        if (activeTextureUnit != current) {
            throw new OutOfSyncException("Texture unit", activeTextureUnit, current);
        }
    }

    /**
     * Checks that the currently bound textures in OpenGL are the same as the ones we think are bound.
     *
     * @throws OutOfSyncException Whenever any state variable is not synchronized.
     */
    private void checkTexturesInSync() throws OutOfSyncException {
        // Store the currently active texture unit, so that it can be restored after cycling through all texture units.
        final int currentTextureUnit = GLGet.activeTextureUnit();
        for (int textureUnit = 0; textureUnit < boundTextures.length; textureUnit++) {
            // Activate the current texture unit to scan.
            GLSet.activeTextureUnit(textureUnit);

            // And check each binding-port of that texture unit.
            for (int textureTarget = 0; textureTarget < boundTextures[textureUnit].length; textureTarget++) {
                // Acquire the enum TextureTarget from its integer representation.
                final GLTextureTarget target = GLTextureTarget.fromOrdinal(textureTarget);
                // Ask OpenGL which texture is actually bound.
                final int boundTexture = GLGet.textureBinding(target);

                if (boundTextures[textureUnit][textureTarget] != boundTexture) {
                    throw new OutOfSyncException(
                            "Texture binding at " + target.name() + " of texture unit " + textureUnit,
                            boundTextures[textureUnit][textureTarget],
                            boundTexture
                    );
                }
            }
        }
        // Reset the active texture unit as we changed it during the process.
        GLSet.activeTextureUnit(currentTextureUnit);
    }

}
