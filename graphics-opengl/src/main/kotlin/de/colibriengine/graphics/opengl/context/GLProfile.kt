package de.colibriengine.graphics.opengl.context

import org.lwjgl.glfw.GLFW

/**
 * Lets you choose between the CORE profile (in which old and therefore deprecated OpenGL functionality got removed) and
 * the COMPATIBILITY profile, which still includes everything. Using the CORE profile is highly recommended.
 */
enum class GLProfile(
    /** The value (int representation) of this OpenGL profile. */
    val id: Int
) {

    /** Excludes and therefore forbids calling/using deprecated/old OpenGL functionality. */
    CORE_PROFILE(GLFW.GLFW_OPENGL_CORE_PROFILE),

    /** Includes the complete set of OpenGL functions. */
    COMPATIBILITY_PROFILE(GLFW.GLFW_OPENGL_COMPAT_PROFILE);

}
