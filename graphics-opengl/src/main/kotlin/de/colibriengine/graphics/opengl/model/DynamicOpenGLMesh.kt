package de.colibriengine.graphics.opengl.model

import de.colibriengine.graphics.material.Material
import de.colibriengine.graphics.model.DynamicMesh
import de.colibriengine.graphics.model.VertexAttributes
import de.colibriengine.graphics.opengl.buffer.*
import de.colibriengine.graphics.opengl.buffer.vao.VAO
import de.colibriengine.graphics.opengl.rendering.GLPrimitiveMode
import de.colibriengine.graphics.rendering.PrimitiveMode
import org.lwjgl.opengl.GL11
import java.nio.ByteBuffer

/**
 * A dynamic mesh has a current [capacity]. Its data buffers can be filled with elements of [primitiveMode] until the
 * capacity is reached. When the model is rendered, only elements up to [size] are rendered. The mesh can be [clear]ed
 * and refilled with data. A mesh consists of vertices. Each vertex is made up of a set of attributes. For example:
 * position, normal, texture coordinate. The attributes used for this mesh must be configured at instantiation time.
 * If [allowAttributeChanges] is set, then attributes can be added and removed freely. For this to function with good
 * performance, data for each attribute is held in a separate buffer. Normally, attribute data is stored interleaved in
 * a single buffer.
 */
class DynamicOpenGLMesh(
    override val primitiveMode: PrimitiveMode,
    val capacity: Int,
    // val indexed: Boolean, // TODO: Is this a good idea? Manually updating a mesh would constantly break optimal indexing.
    val attributes: VertexAttributes,
    val allowAttributeChanges: Boolean // TODO: make this work
) : DynamicMesh {

    /** The amount of primitives in this meshes buffer. */
    var size: Int = 0

    /** The amount of primitives that can still be stored in this meshes buffers. */
    val left: Int
        get() = capacity - size

    /** When in indexed drawing mode, this represents the amount of indices OpenGL should draw. */
    //private var indicesToDraw: Int = 0

    /** When not in indexed drawing mode, this represents the amount of vertices OpenGL should draw. */
    //private var verticesToDraw: Int = 0

    private val glPrimitiveMode: GLPrimitiveMode = GLPrimitiveMode.from(primitiveMode)

    private val vao: VAO = VAO()

    /** Holds the indices of vertices to draw if indexed rendering is activated. */
    val indexBuffer: GLBuffer = GLBuffer(GLBufferTarget.GL_ELEMENT_ARRAY_BUFFER)

    /** Holds the interleaved vertex data. */
    val dataBuffer: GLBuffer = GLBuffer(GLBufferTarget.GL_ARRAY_BUFFER)

    /** Holds the vertex data for individual attributes. */
    val attributeBuffers: Array<GLBuffer> = Array(attributes.size) {
        GLBuffer(GLBufferTarget.GL_ARRAY_BUFFER)
    }

    override var material: Material? = null

    init {
        //if (indexed) {
        //    initBufferStorage(indexBuffer, capacity, BYTES_PER_INT)
        //    initBufferStorage(dataBuffer, capacity, attributes.combinedAttributeSize)
        //} else {
        attributes.forEachIndexed { index, _, attribute ->
            initBufferStorage(attributeBuffers[index], capacity, attribute.size)
        }
        //}
        clear()
        initVao()
    }

    private fun initVao() {
        vao.create()
        attributes.forEachIndexed { index, _, attribute ->
            vao.enableVertexAttrib(attribute.index)
            vao.setVertexBuffer(attributeBuffers[index], index, 0, attribute.bytes)
            vao.setAttribFormat(attribute.index, attribute.size, GLDataType.from(attribute.dataType), false, 0)
            vao.setAttribBinding(attribute.index, index)
        }
        vao.finish()
    }

    // TODO: Disable attribute and use default instead.
    //vao.disableVertexAttrib(OpenGLMesh.COLOR_ATTRIB_INDEX)
    // Setup a default color. This color will be used if the mesh does not specify specific color values.
    //GL20.glVertexAttrib4f(OpenGLMesh.COLOR_ATTRIB_INDEX, 1.0f, 1.0f, 1.0f, 1.0f)

    // TODO: initVaoIndexed()

    inline fun storeInterleaved(amount: Int, action: (ByteBuffer) -> Unit) {
        require(amount <= left)
        modifyGLBuffers(dataBuffer, action)
        size += amount
    }

    inline fun store(amount: Int, action: (Array<ByteBuffer>) -> Unit) {
        //require(!indexed)
        require(amount <= left)
        modifyGLBuffers(attributeBuffers, action)
        size += amount
    }

    inline fun store(amount: Int, action: (ByteBuffer, ByteBuffer) -> Unit) {
        //require(!indexed)
        require(amount <= left)
        require(attributeBuffers.size == 2)
        modifyGLBuffers(attributeBuffers[0], attributeBuffers[1]) { dataBuffer0, dataBuffer1 ->
            // Modify buffer positions to allow appending to them.
            dataBuffer0.position(attributes.set[0].second.bytes * size)
            dataBuffer1.position(attributes.set[1].second.bytes * size)
            val db0p = dataBuffer0.position()
            val db1p = dataBuffer1.position()
            action(dataBuffer0, dataBuffer1)
            require(dataBuffer0.position() == db0p + amount * attributes.set[0].second.bytes) { "old: $db0p, new: ${dataBuffer0.position()}" }
            require(dataBuffer1.position() == db1p + amount * attributes.set[1].second.bytes) { "old: $db1p, new: ${dataBuffer1.position()}" }
        }
        size += amount
    }

    inline fun store(amount: Int, action: (ByteBuffer, ByteBuffer, ByteBuffer) -> Unit) {
        //require(!indexed)
        require(amount <= left)
        require(attributeBuffers.size == 3)
        modifyGLBuffers(
            attributeBuffers[0],
            attributeBuffers[1],
            attributeBuffers[2]
        ) { dataBuffer0, dataBuffer1, dataBuffer2 ->
            // Modify buffer positions to allow appending to them.
            dataBuffer0.position(attributes.set[0].second.bytes * size)
            dataBuffer1.position(attributes.set[1].second.bytes * size)
            dataBuffer2.position(attributes.set[2].second.bytes * size)
            val db0p = dataBuffer0.position()
            val db1p = dataBuffer1.position()
            val db2p = dataBuffer2.position()
            action(dataBuffer0, dataBuffer1, dataBuffer2)
            require(dataBuffer0.position() == db0p + amount * attributes.set[0].second.bytes) { "old: $db0p, new: ${dataBuffer0.position()}" }
            require(dataBuffer1.position() == db1p + amount * attributes.set[1].second.bytes) { "old: $db1p, new: ${dataBuffer1.position()}" }
            require(dataBuffer2.position() == db2p + amount * attributes.set[2].second.bytes) { "old: $db2p, new: ${dataBuffer2.position()}" }
        }
        size += amount
    }

    override fun clear() {
        // We do not erase any data on the GPU that was previously uploaded.
        // We just start again by assuming that no dat was already stored, therefore invalidating the buffer storage.
        size = 0
    }

    private fun initBufferStorage(buffer: GLBuffer, capacity: Int, singleElementByteCount: Int) {
        buffer.create()
        buffer.setStorageFlags(GLBufferStorageFlag.GL_MAP_WRITE_BIT, GLBufferStorageFlag.GL_MAP_PERSISTENT_BIT)
        buffer.createStorage(capacity * singleElementByteCount)
    }

    override fun render() {
        bind()
        //when {
        //    indexed -> GL11.glDrawElements(glPrimitiveMode.id, indicesToDraw, GL11.GL_UNSIGNED_INT, 0)
        //    else -> GL11.glDrawArrays(glPrimitiveMode.id, 0, verticesToDraw)
        //}
        GL11.glDrawArrays(glPrimitiveMode.id, 0, size)
        unbind()
    }

    private fun bind() {
        vao.bind()
        material?.apply {
            var atTextureUnit = 0
            if (hasAmbientTextures()) {
                for (ambientTexture in ambientTextures) {
                    ambientTexture.bind(atTextureUnit++)
                }
            }
        }
    }

    private fun unbind() {
        vao.unbind()
        material?.apply {
            var atTextureUnit = 0
            if (hasAmbientTextures()) {
                for (ambientTexture in ambientTextures) {
                    ambientTexture.unbind(atTextureUnit++)
                }
            }
        }
    }

    override fun release() {
        vao.releaseIfCreated()
        dataBuffer.releaseIfCreated()
        for (attributeBuffer in attributeBuffers) {
            attributeBuffer.releaseIfCreated()
        }
    }

}
