package de.colibriengine.graphics.opengl.model

import de.colibriengine.graphics.model.AbstractModelDefinition
import de.colibriengine.graphics.model.Group
import de.colibriengine.graphics.model.Mesh
import de.colibriengine.graphics.model.MeshFactory

object GLMeshFactory : MeshFactory {

    override fun createFrom(group: Group, modelDefinition: AbstractModelDefinition): Mesh {
        return OpenGLMesh.createFrom(group, modelDefinition)
    }

}
