package de.colibriengine.graphics.opengl.model;

import de.colibriengine.buffers.BufferStorable;
import de.colibriengine.graphics.material.Material;
import de.colibriengine.graphics.material.MaterialLibrary;
import de.colibriengine.graphics.model.*;
import de.colibriengine.graphics.opengl.buffer.GLBuffer;
import de.colibriengine.graphics.opengl.buffer.GLBufferStorageFlag;
import de.colibriengine.graphics.opengl.buffer.GLBufferTarget;
import de.colibriengine.graphics.opengl.buffer.GLDataType;
import de.colibriengine.graphics.opengl.buffer.vao.VAO;
import de.colibriengine.graphics.opengl.rendering.GLPrimitiveMode;
import de.colibriengine.graphics.rendering.PrimitiveMode;
import de.colibriengine.graphics.texture.Texture;
import de.colibriengine.logging.LogUtil;
import de.colibriengine.math.vector.vec2f.Vec2f;
import de.colibriengine.math.vector.vec3f.Vec3f;
import de.colibriengine.math.vector.vec4f.Vec4f;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.glVertexAttrib4f;

public class OpenGLMesh implements Mesh {

    private static final Logger LOG = LogUtil.getLogger(OpenGLMesh.class);

    private static final int POSITION_ATTRIB_INDEX = 0;
    private static final int NORMAL_ATTRIB_INDEX = 1;
    private static final int TEXCOORD_ATTRIB_INDEX = 2;
    private static final int COLOR_ATTRIB_INDEX = 3;
    private static final int MATERIAL_INDEX_ATTRIB_INDEX = 4;

    private final GLPrimitiveMode primitiveMode;

    private final boolean indexed;

    private int vertexDataSize;
    private int indexDataSize;

    private final @NotNull VAO vao;
    private final @NotNull GLBuffer indexBuffer;
    private final @NotNull GLBuffer dataBuffer;
    private final @NotNull GLBuffer positionBuffer;
    private final @NotNull GLBuffer normalBuffer;
    private final @NotNull GLBuffer texCoordBuffer;
    private final @NotNull GLBuffer colorBuffer;
    private final @NotNull GLBuffer materialIndexBuffer;

    private int verticesToDraw = -1;
    private int indicesToDraw = -1;

    // TODO: support a list of materials and a material index as a vertex attribute.
    private @Nullable Material material = null;

    private final VertexDataProducer vertexDataProducer = new VertexDataProducer();

    private OpenGLMesh(
            final PrimitiveMode primitiveMode, final boolean indexed,
            final int vertexDataSize, final int indexDataSize,
            final boolean persistentlyMappedBuffers
    ) {
        this.primitiveMode = GLPrimitiveMode.Companion.from(primitiveMode);
        this.indexed = indexed;
        this.vertexDataSize = vertexDataSize;
        this.indexDataSize = indexDataSize;

        vao = new VAO();
        indexBuffer = new GLBuffer(GLBufferTarget.GL_ELEMENT_ARRAY_BUFFER);
        dataBuffer = new GLBuffer(GLBufferTarget.GL_ARRAY_BUFFER);
        positionBuffer = new GLBuffer(GLBufferTarget.GL_ARRAY_BUFFER);
        normalBuffer = new GLBuffer(GLBufferTarget.GL_ARRAY_BUFFER);
        texCoordBuffer = new GLBuffer(GLBufferTarget.GL_ARRAY_BUFFER);
        colorBuffer = new GLBuffer(GLBufferTarget.GL_ARRAY_BUFFER);
        materialIndexBuffer = new GLBuffer(GLBufferTarget.GL_ARRAY_BUFFER);
    }

    public static OpenGLMesh createFrom(
            final @NotNull Group group,
            final @NotNull AbstractModelDefinition modelDefinition
    ) {
        LOG.debug("Creating mesh from mesh group: {}", group.getName());

        final OpenGLMesh mesh = new OpenGLMesh(
                group.getPrimitiveMode(),
                true, // TODO: Make dynamic
                group.getVertexDataSize(),
                group.getIndexDataSize(),
                group.getAllowPersistentlyMappedBuffers()
        );

        // Search for the specified material.
        for (final MaterialLibrary materialLibrary : modelDefinition.getMatLibs()) {
            // TODO: Access materials by string key in hashmap.
            for (final Material material : materialLibrary.getMaterials()) {
                if (material.getMaterialDefinition().getName().equals(group.getMaterialName())) {
                    mesh.material = material;
                }
            }
        }

        if (mesh.indexed) {
            mesh.fillBuffersWithIndexedData(modelDefinition, group);
        } else {
            mesh.fillBuffersWithData(modelDefinition, group);
        }

        return mesh;
    }

    private void fillBuffersWithIndexedData(
            final @NotNull AbstractModelDefinition modelDefinition,
            final @NotNull Group group
    ) {
        final List<Vertex> vertexData = vertexDataProducer.genVertexData(
                group.getFaceVertexAttributeIndices(),
                modelDefinition
        );
        final List<Vertex> uniqueVertexData = new ArrayList<>();
        final List<Integer> indices = new ArrayList<>();

        // This call will fill the uniqueVertexData and indices lists.
        vertexDataProducer.genUniqueVertexData(vertexData, uniqueVertexData, indices);

        initBufferVertexDataBuffer(dataBuffer, uniqueVertexData, Vertex.BYTES);
        initIndexBuffer(indexBuffer, indices, Integer.BYTES);
        initVAOIndexed();

        indicesToDraw = indices.size();
    }

    private void fillBuffersWithData(
            final @NotNull AbstractModelDefinition modelDefinition,
            final @NotNull Group group
    ) {
        initBufferVertexDataBuffer(positionBuffer, modelDefinition.getVertexPositions(), Vec3f.BYTES);
        initBufferVertexDataBuffer(normalBuffer, modelDefinition.getVertexNormals(), Vec3f.BYTES);
        initBufferVertexDataBuffer(texCoordBuffer, modelDefinition.getVertexTextCoords(), Vec2f.BYTES);
        initBufferVertexDataBuffer(colorBuffer, modelDefinition.getVertexColors(), Vec4f.BYTES);
        initVAO();

        switch (primitiveMode) {
            case TRIANGLES -> verticesToDraw = group.getFaceVertexAttributeIndices().size() * 3;
            case LINES -> verticesToDraw = group.getFaceVertexAttributeIndices().size() * 2;
            case POINTS -> verticesToDraw = group.getFaceVertexAttributeIndices().size(); // TODO: Check if this works..
            default -> throw new IllegalStateException("This primitive mode is not supported!");
        }
    }

    /**
     * @minContextRequired 3.0
     */
    public void bind() {
        vao.bind();

        if (material != null) {
            // LOG.info("mat {} {}", material.getAmbientTextures().size(),  material.getDiffuseTextures().size());
            if (material.hasAmbientTextures()) {
                // LOG.info("bind {} {}", material.getAmbientTextures().size(), material.getAmbientTextures().get(0).getOpenGLHandle());
                List<Texture> ambientTextures = material.getAmbientTextures();
                int size = ambientTextures.size();
                int atTextureUnit = 0;
                //noinspection ForLoopReplaceableByForEach
                for (int i = 0; i < size; i++) {
                    ambientTextures.get(i).bind(atTextureUnit);
                    atTextureUnit++;
                }
            }
        }
    }

    /**
     * @minContextRequired 3.0
     */
    public void unbind() {
        vao.unbind();

        if (material != null) {
            if (material.hasAmbientTextures()) {
                List<Texture> ambientTextures = material.getAmbientTextures();
                int size = ambientTextures.size();
                int atTextureUnit = 0;
                //noinspection ForLoopReplaceableByForEach
                for (int i = 0; i < size; i++) {
                    ambientTextures.get(i).unbind(atTextureUnit);
                    atTextureUnit++;
                }
            }
        }
    }

    /**
     * @minContextRequired 3.0
     */
    @Override
    public void release() {
        if (vao.isCreated()) {
            vao.release();
        }
        if (indexBuffer.isCreated()) {
            indexBuffer.release();
        }
        if (positionBuffer.isCreated()) {
            positionBuffer.release();
        }
        if (normalBuffer.isCreated()) {
            normalBuffer.release();
        }
        if (texCoordBuffer.isCreated()) {
            texCoordBuffer.release();
        }
        if (colorBuffer.isCreated()) {
            colorBuffer.release();
        }
        if (materialIndexBuffer.isCreated()) {
            materialIndexBuffer.release();
        }
    }

    @Override
    public void render() {
        bind();
        // The Model constructor ensured that primitiveMode is one of GL_LINES, GL_TRIANGLES, etc
        if (indexed) {
            glDrawElements(primitiveMode.getId(), indicesToDraw, GL_UNSIGNED_INT, 0);
        } else {
            glDrawArrays(primitiveMode.getId(), 0, verticesToDraw);
        }
        unbind();
    }

    private void initVAO() {
        vao.create();

        // Bind the position buffer to the vertex array object.
        if (positionBuffer.isCreated()) {
            vao.enableVertexAttrib(POSITION_ATTRIB_INDEX);
            vao.setVertexBuffer(positionBuffer, 0, 0, Vec3f.BYTES);
            vao.setAttribFormat(POSITION_ATTRIB_INDEX, VAO.THREE, GLDataType.FLOAT, false, 0);
            vao.setAttribBinding(POSITION_ATTRIB_INDEX, 0);
        } else {
            vao.disableVertexAttrib(POSITION_ATTRIB_INDEX);
        }

        // Bind the normal buffer to the vertex array object.
        if (normalBuffer.isCreated()) {
            vao.enableVertexAttrib(NORMAL_ATTRIB_INDEX);
            vao.setVertexBuffer(normalBuffer, 1, 0, Vec3f.BYTES);
            vao.setAttribFormat(NORMAL_ATTRIB_INDEX, VAO.THREE, GLDataType.FLOAT, false, 0);
            vao.setAttribBinding(NORMAL_ATTRIB_INDEX, 1);
        } else {
            vao.disableVertexAttrib(NORMAL_ATTRIB_INDEX);
        }

        // Bind the texture coordinate buffer to the vertex array object.
        if (texCoordBuffer.isCreated()) {
            vao.enableVertexAttrib(TEXCOORD_ATTRIB_INDEX);
            vao.setVertexBuffer(texCoordBuffer, 2, 0, Vec2f.BYTES);
            vao.setAttribFormat(TEXCOORD_ATTRIB_INDEX, VAO.TWO, GLDataType.FLOAT, false, 0);
            vao.setAttribBinding(TEXCOORD_ATTRIB_INDEX, 2);
        } else {
            vao.disableVertexAttrib(TEXCOORD_ATTRIB_INDEX);
        }

        // Bind the color buffer to the vertex array object.
        if (colorBuffer.isCreated()) {
            vao.enableVertexAttrib(COLOR_ATTRIB_INDEX);
            vao.setVertexBuffer(colorBuffer, 3, 0, Vec4f.BYTES);
            vao.setAttribFormat(COLOR_ATTRIB_INDEX, VAO.FOUR, GLDataType.FLOAT, false, 0);
            vao.setAttribBinding(COLOR_ATTRIB_INDEX, 3);
        }
        // Or use a default color.
        else {
            vao.disableVertexAttrib(COLOR_ATTRIB_INDEX);
            // Setup a default color. This color will be used if the mesh does not specify specific color values.
            glVertexAttrib4f(COLOR_ATTRIB_INDEX, 1.0f, 1.0f, 1.0f, 1.0f);
        }

        vao.finish();
    }

    private void initVAOIndexed() {
        vao.create();

        // We only use a single interleaved buffer.
        vao.setVertexBuffer(dataBuffer, 0, 0, Vertex.BYTES);

        // Bind the position buffer to the vertex array object.
        vao.enableVertexAttrib(POSITION_ATTRIB_INDEX);
        vao.setAttribFormat(POSITION_ATTRIB_INDEX, VAO.THREE, GLDataType.FLOAT, false, 0);
        vao.setAttribBinding(POSITION_ATTRIB_INDEX, 0);

        vao.enableVertexAttrib(NORMAL_ATTRIB_INDEX);
        /* 12 = 3*4 bytes after position */
        vao.setAttribFormat(NORMAL_ATTRIB_INDEX, VAO.THREE, GLDataType.FLOAT, false, Vec3f.BYTES);
        vao.setAttribBinding(NORMAL_ATTRIB_INDEX, 0);

        vao.enableVertexAttrib(TEXCOORD_ATTRIB_INDEX);
        /* 24 = 3*4+3*4 bytes after position and normal*/
        vao.setAttribFormat(TEXCOORD_ATTRIB_INDEX, VAO.TWO, GLDataType.FLOAT, false, Vec3f.BYTES +
                Vec3f.BYTES);
        vao.setAttribBinding(TEXCOORD_ATTRIB_INDEX, 0);

        vao.enableVertexAttrib(COLOR_ATTRIB_INDEX);
        /* 32 = 3*4+3*4+2*4 bytes after position and normal and texCoord */
        vao.setAttribFormat(COLOR_ATTRIB_INDEX, VAO.FOUR, GLDataType.FLOAT, false, Vec3f.BYTES +
                Vec3f.BYTES +
                Vec2f.BYTES);
        vao.setAttribBinding(COLOR_ATTRIB_INDEX, 0);

        //vao.enableVertexAttrib(MATERIAL_INDEX_ATTRIB_INDEX);
        /* 24 = 2*(3*4) bytes after position and normal*/
        //vao.setAttribFormat(MATERIAL_INDEX_ATTRIB_INDEX, VAO.ONE, GL_INT, false, Vec3f.BYTES + Vec3f.BYTES + Vec2f
        // .BYTES + Vec4f.BYTES);
        //vao.setAttribBinding(MATERIAL_INDEX_ATTRIB_INDEX, 0);

        // Bind the index buffer.
        vao.setIndexBuffer(indexBuffer);

        vao.finish();
    }

    public <T extends BufferStorable> void writeData(
            final @NotNull ByteBuffer bufferData,
            final @NotNull T data
    ) {
        data.storeIn(bufferData);
    }

    public <T extends BufferStorable> void writeData(
            final @NotNull ByteBuffer bufferData,
            final @NotNull List<T> data
    ) {
        data.forEach((T entry) -> entry.storeIn(bufferData));
    }

    public <T extends BufferStorable> void initBufferVertexDataBuffer(
            final GLBuffer buffer,
            final List<T> data,
            final int singleDataByteCount
    ) {
        // Create the buffer on the GPU.
        final int bufferSize = vertexDataSize == GroupImpl.DYNAMIC_SIZE ?
                data.size() * singleDataByteCount :
                vertexDataSize * singleDataByteCount;
        buffer.create();
        buffer.setStorageFlags(GLBufferStorageFlag.GL_MAP_WRITE_BIT, GLBufferStorageFlag.GL_MAP_PERSISTENT_BIT);
        buffer.createStorage(bufferSize);
        // Map the buffer for writing.
        final ByteBuffer bufferData = buffer.mapWriteOnly();
        // Write the vertex data into the buffer.
        writeData(bufferData, data);
        // Data store now contains our vertex data. Unmap the buffer.
        buffer.unmap();
    }

    public <T extends BufferStorable> void initIndexBuffer(
            final GLBuffer buffer,
            final List<Integer> data,
            final int singleDataByteCount
    ) {
        // Create the buffer on the GPU.
        final int bufferSize = indexDataSize == GroupImpl.DYNAMIC_SIZE ?
                data.size() * singleDataByteCount :
                indexDataSize * singleDataByteCount;
        buffer.create();
        buffer.setStorageFlags(GLBufferStorageFlag.GL_MAP_WRITE_BIT, GLBufferStorageFlag.GL_MAP_PERSISTENT_BIT);
        buffer.createStorage(bufferSize);
        // Map the buffer for writing.
        final ByteBuffer bufferData = buffer.mapWriteOnly();
        // Write the vertex data into the buffer.
        for (final Integer integer : data) {
            bufferData.putInt(integer);
        }
        // Data store now contains our vertex data. Unmap the buffer.
        buffer.unmap();
    }

    @Override
    public PrimitiveMode getPrimitiveMode() {
        return primitiveMode.toPrimitiveMode();
    }

    public @NotNull VAO getVAO() {
        return vao;
    }

    public @NotNull GLBuffer getIndexBuffer() {
        return indexBuffer;
    }

    public @NotNull GLBuffer getDataBuffer() {
        return dataBuffer;
    }

    public @NotNull GLBuffer getPositionBuffer() {
        return positionBuffer;
    }

    public @NotNull GLBuffer getNormalBuffer() {
        return normalBuffer;
    }

    public @NotNull GLBuffer getTexCoordBuffer() {
        return texCoordBuffer;
    }

    public @NotNull GLBuffer getColorBuffer() {
        return colorBuffer;
    }

    public @NotNull GLBuffer getMaterialIndexBuffer() {
        return materialIndexBuffer;
    }

    @Override
    public int getIndicesToDraw() {
        return indicesToDraw;
    }

    @Override
    public void setIndicesToDraw(final int indicesToDraw) {
        this.indicesToDraw = indicesToDraw;
    }

    @Override
    public int getVerticesToDraw() {
        return verticesToDraw;
    }

    @Override
    public void setVerticesToDraw(final int verticesToDraw) {
        this.verticesToDraw = verticesToDraw;
    }

    @Override
    public @Nullable Material getMaterial() {
        return material;
    }

    @Override
    public void setMaterial(final @Nullable Material material) {
        this.material = material;
    }

}
