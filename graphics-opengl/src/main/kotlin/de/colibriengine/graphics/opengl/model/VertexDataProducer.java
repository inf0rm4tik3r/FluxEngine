package de.colibriengine.graphics.opengl.model;

import de.colibriengine.graphics.model.AbstractModelDefinition;
import de.colibriengine.graphics.model.FaceVertexAttributeIndices;
import de.colibriengine.graphics.model.Vertex;
import de.colibriengine.graphics.model.VertexIndices;
import de.colibriengine.math.vector.vec2f.StdVec2f;
import de.colibriengine.math.vector.vec2f.Vec2f;
import de.colibriengine.math.vector.vec3f.StdVec3f;
import de.colibriengine.math.vector.vec3f.Vec3f;
import de.colibriengine.math.vector.vec4f.StdVec4f;
import de.colibriengine.math.vector.vec4f.StdVec4fFactory;
import de.colibriengine.math.vector.vec4f.Vec4f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VertexDataProducer {

    public List<Vertex> genVertexData(final List<FaceVertexAttributeIndices> faces,
                                       final AbstractModelDefinition modelDefinition) {
        final ArrayList<Vertex> vertexData = new ArrayList<>();

        ArrayList<Vec3f> vertexPositions = modelDefinition.getVertexPositions();
        ArrayList<Vec3f> vertexNormals = modelDefinition.getVertexNormals();
        ArrayList<Vec2f> vertexTexCoords = modelDefinition.getVertexTextCoords();
        ArrayList<Vec4f> vertexColors = modelDefinition.getVertexColors();

        for (final FaceVertexAttributeIndices face : faces) {
            for (int i = 0; i < face.getVertexCount(); i++) {

                final int vertexPositionIndex;
                final Vec3f vertexPosition;
                face.getPositionIndices();
                if (face.getPositionIndices().length > i) {
                    vertexPositionIndex = face.getPositionIndices()[i] - 1;
                    vertexPosition = vertexPositions.get(vertexPositionIndex);
                } else {
                    vertexPositionIndex = 0;
                    vertexPosition = new StdVec3f();
                }

                final int vertexNormalIndex;
                final Vec3f vertexNormal;
                if (face.getNormalIndices() != null && face.getNormalIndices().length > i) {
                    vertexNormalIndex = face.getNormalIndices()[i] - 1;
                    vertexNormal = vertexNormals.get(vertexNormalIndex);
                } else {
                    vertexNormalIndex = 0;
                    vertexNormal = new StdVec3f();
                }

                final int vertexTexCoordIndex;
                final Vec2f vertexTexCoord;
                if (face.getTexCoordIndices() != null && face.getTexCoordIndices().length > i) {
                    vertexTexCoordIndex = face.getTexCoordIndices()[i] - 1;
                    vertexTexCoord = vertexTexCoords.get(vertexTexCoordIndex);
                } else {
                    vertexTexCoordIndex = 0;
                    vertexTexCoord = new StdVec2f();
                }

                final int vertexColorIndex;
                final Vec4f vertexColor;
                if (face.getColorIndices() != null && face.getColorIndices().length > i) {
                    vertexColorIndex = face.getColorIndices()[i] - 1;
                    vertexColor = vertexColors.get(vertexColorIndex);
                } else {
                    vertexColorIndex = 0;
                    vertexColor = new StdVec4f().set(StdVec4fFactory.Companion.getWHITE());
                }

                final VertexIndices indexSet = new VertexIndices(
                        vertexPositionIndex, vertexNormalIndex, vertexTexCoordIndex, vertexColorIndex
                );

                vertexData.add(new Vertex(indexSet, vertexPosition, vertexNormal, vertexTexCoord, vertexColor));
            }
        }

        return vertexData;
    }

    public void genUniqueVertexData(
            final List<Vertex> vertexData,
            final List<Vertex> uniqueVertexData,
            final List<Integer> indices
    ) {
        final HashMap<VertexIndices, Integer> indexSetToIndex = new HashMap<>();

        int index = 0;

        for (final Vertex vertex : vertexData) {
            // Use the index if already visited.
            if (indexSetToIndex.containsKey(vertex.getIndexSet())) {
                indices.add(indexSetToIndex.get(vertex.getIndexSet()));
            } else {
                indices.add(index);

                uniqueVertexData.add(vertex);
                indexSetToIndex.put(vertex.getIndexSet(), index);
                index++;
            }
        }
    }

}
