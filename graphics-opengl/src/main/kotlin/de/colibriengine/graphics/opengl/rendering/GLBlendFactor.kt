package de.colibriengine.graphics.opengl.rendering

import de.colibriengine.graphics.rendering.BlendFactor
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL14
import org.lwjgl.opengl.GL15
import org.lwjgl.opengl.GL33

enum class GLBlendFactor(
    /** The value of this constant in OpenGL. */
    val id: Int
) {

    GL_ZERO(GL11.GL_ZERO),
    GL_ONE(GL11.GL_ONE),
    GL_SRC_COLOR(GL11.GL_SRC_COLOR),
    GL_ONE_MINUS_SRC_COLOR(GL11.GL_ONE_MINUS_SRC_COLOR),
    GL_DST_COLOR(GL11.GL_DST_COLOR),
    GL_ONE_MINUS_DST_COLOR(GL11.GL_ONE_MINUS_DST_COLOR),
    GL_SRC_ALPHA(GL11.GL_SRC_ALPHA),
    GL_ONE_MINUS_SRC_ALPHA(GL11.GL_ONE_MINUS_SRC_ALPHA),
    GL_DST_ALPHA(GL11.GL_DST_ALPHA),
    GL_ONE_MINUS_DST_ALPHA(GL11.GL_ONE_MINUS_DST_ALPHA),
    GL_CONSTANT_COLOR(GL14.GL_CONSTANT_COLOR),
    GL_ONE_MINUS_CONSTANT_COLOR(GL14.GL_ONE_MINUS_CONSTANT_COLOR),
    GL_CONSTANT_ALPHA(GL14.GL_CONSTANT_ALPHA),
    GL_ONE_MINUS_CONSTANT_ALPHA(GL14.GL_ONE_MINUS_CONSTANT_ALPHA),
    GL_SRC_ALPHA_SATURATE(GL11.GL_SRC_ALPHA_SATURATE),
    GL_SRC1_COLOR(GL33.GL_SRC1_COLOR),
    GL_ONE_MINUS_SRC1_COLOR(GL33.GL_ONE_MINUS_SRC1_COLOR),
    GL_SRC1_ALPHA(GL15.GL_SRC1_ALPHA),
    GL_ONE_MINUS_SRC1_ALPHA(GL33.GL_ONE_MINUS_SRC1_ALPHA);

    companion object {

        fun from(blendFactor: BlendFactor): GLBlendFactor {
            return when (blendFactor) {
                BlendFactor.ZERO -> GL_ZERO
                BlendFactor.ONE -> GL_ONE
                BlendFactor.SRC_COLOR -> GL_SRC_COLOR
                BlendFactor.ONE_MINUS_SRC_COLOR -> GL_ONE_MINUS_SRC_COLOR
                BlendFactor.DST_COLOR -> GL_DST_COLOR
                BlendFactor.ONE_MINUS_DST_COLOR -> GL_ONE_MINUS_DST_COLOR
                BlendFactor.SRC_ALPHA -> GL_SRC_ALPHA
                BlendFactor.ONE_MINUS_SRC_ALPHA -> GL_ONE_MINUS_SRC_ALPHA
                BlendFactor.DST_ALPHA -> GL_DST_ALPHA
                BlendFactor.ONE_MINUS_DST_ALPHA -> GL_ONE_MINUS_DST_ALPHA
                BlendFactor.CONSTANT_COLOR -> GL_CONSTANT_COLOR
                BlendFactor.ONE_MINUS_CONSTANT_COLOR -> GL_ONE_MINUS_CONSTANT_COLOR
                BlendFactor.CONSTANT_ALPHA -> GL_CONSTANT_ALPHA
                BlendFactor.ONE_MINUS_CONSTANT_ALPHA -> GL_ONE_MINUS_CONSTANT_ALPHA
                BlendFactor.SRC_ALPHA_SATURATE -> GL_SRC_ALPHA_SATURATE
                BlendFactor.SRC1_COLOR -> GL_SRC1_COLOR
                BlendFactor.ONE_MINUS_SRC1_COLOR -> GL_ONE_MINUS_SRC1_COLOR
                BlendFactor.SRC1_ALPHA -> GL_SRC1_ALPHA
                BlendFactor.ONE_MINUS_SRC1_ALPHA -> GL_ONE_MINUS_SRC1_ALPHA
            }
        }

    }

}
