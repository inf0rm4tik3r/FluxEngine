package de.colibriengine.graphics.opengl.rendering

import org.lwjgl.opengl.GL11

enum class GLPolygonFace(
    /** The value of this constant in OpenGL. */
    val id: Int
) {

    GL_FRONT(GL11.GL_FRONT),
    GL_BACK(GL11.GL_BACK),
    GL_FRONT_AND_BACK(GL11.GL_FRONT_AND_BACK);

}
