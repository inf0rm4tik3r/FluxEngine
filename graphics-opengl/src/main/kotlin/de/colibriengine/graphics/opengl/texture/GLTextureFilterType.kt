package de.colibriengine.graphics.opengl.texture;

import de.colibriengine.graphics.texture.TextureFilterType
import org.lwjgl.opengl.GL11

/**
 * TextureFilterType - Presents the available texture filtering types.
 */
enum class GLTextureFilterType(
    /** The value of this constant in OpenGL. */
    val id: Int
) {

    GL_NEAREST(GL11.GL_NEAREST),
    GL_LINEAR(GL11.GL_LINEAR),
    GL_NEAREST_MIPMAP_NEAREST(GL11.GL_NEAREST_MIPMAP_NEAREST),
    GL_LINEAR_MIPMAP_NEAREST(GL11.GL_LINEAR_MIPMAP_NEAREST),
    GL_NEAREST_MIPMAP_LINEAR(GL11.GL_NEAREST_MIPMAP_LINEAR),
    GL_LINEAR_MIPMAP_LINEAR(GL11.GL_LINEAR_MIPMAP_LINEAR);

    companion object {
        fun from(textureFilterType: TextureFilterType): GLTextureFilterType {
            return when (textureFilterType) {
                TextureFilterType.NEAREST -> GL_NEAREST
                TextureFilterType.LINEAR -> GL_LINEAR
                TextureFilterType.NEAREST_MIPMAP_NEAREST -> GL_NEAREST_MIPMAP_NEAREST
                TextureFilterType.LINEAR_MIPMAP_NEAREST -> GL_LINEAR_MIPMAP_NEAREST
                TextureFilterType.NEAREST_MIPMAP_LINEAR -> GL_NEAREST_MIPMAP_LINEAR
                TextureFilterType.LINEAR_MIPMAP_LINEAR -> GL_LINEAR_MIPMAP_LINEAR
            }
        }
    }

}
