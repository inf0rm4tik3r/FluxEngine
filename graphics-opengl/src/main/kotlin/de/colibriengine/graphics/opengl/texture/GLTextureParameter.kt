package de.colibriengine.graphics.opengl.texture

import org.lwjgl.opengl.GL11

enum class GLTextureParameter(
    /** The value of this constant in OpenGL. */
    val id: Int
) {

    GL_TEXTURE_BORDER_COLOR(GL11.GL_TEXTURE_BORDER_COLOR);

}
