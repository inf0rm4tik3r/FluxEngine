package de.colibriengine.graphics.opengl.texture;

import de.colibriengine.graphics.texture.TextureWrapType;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

/**
 * TextureWrapType - Presents the available texture wrapping types.
 */
public enum GLTextureWrapType {

    GL_CLAMP(GL11.GL_CLAMP),
    GL_CLAMP_TO_BORDER(GL13.GL_CLAMP_TO_BORDER),
    GL_REPEAT(GL11.GL_REPEAT);

    /**
     * The value of this constant in OpenGL.
     */
    public final int id;

    GLTextureWrapType(final int id) {
        this.id = id;
    }

    public static GLTextureWrapType from(TextureWrapType textureWrapType) {
        switch (textureWrapType) {
            case CLAMP -> {
                return GL_CLAMP;
            }
            case CLAMP_TO_BORDER -> {
                return GL_CLAMP_TO_BORDER;
            }
            case REPEAT -> {
                return GL_REPEAT;
            }
            default -> throw new IllegalArgumentException("Unknown texture wrap type.");
        }
    }

}
