package de.colibriengine.graphics.opengl.window

import de.colibriengine.asset.ResourceName
import de.colibriengine.exception.WindowCreationException
import de.colibriengine.graphics.window.WindowHelper.calculateMonitorToUse
import de.colibriengine.graphics.window.WindowHelper.calculateVideoModeToUse
import de.colibriengine.logging.LogUtil.getLogger
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL11
import org.lwjgl.system.MemoryUtil

/**
 * Adds build-functionality to the abstract window specification. THIS CLASS SHOULD BE EXTENDED by YOU to define your
 * personal window.
 */
abstract class AbstractBuildableOpenGLWindow(builder: OpenGLWindowBuilder) :
    AbstractDebuggableOpenGLWindow(builder.windowManager, builder.inputManager, builder.resourceLoader) {

    /**
     * Uses the provided builder instance to create the window!
     *
     * @param builder The builder with which this window should be created.
     * @throws WindowCreationException If the window creation went wrong.
     */
    init {
        //beforeCreate(builder);
        create(builder)
        //afterCreate();
    }

    /**
     * This method will not free the builder (leaving it intact). It can then be used to build multiple windows.
     *
     * @param builder The fully set up window builder instance which contains all necessary data to create the window.
     * @throws WindowCreationException If the window could not be created. This might happen if the requested OpenGL
     *     context version is unsupported by the system.
     */
    @Throws(WindowCreationException::class)
    private fun create(builder: OpenGLWindowBuilder) {
        setUpGLFWHints(builder)

        // Specify the monitor on which the window should be initially created/placed.
        // Use the primary monitor or the one with the specified index.
        monitor = calculateMonitorToUse(builder.desiredMonitor.toLong())
        val res = builder.resolution
        // Get the video mode of the monitor we want to bind.
        videoMode = calculateVideoModeToUse(
            monitor, res.x, res.y, -1
        )

        // Create the window.
        windowHandle = if (builder.fullscreen) {
            GLFW.glfwCreateWindow(
                videoMode!!.width(), videoMode!!.height(),
                builder.title, monitor, builder.sharedContext
            )
        } else {
            GLFW.glfwCreateWindow(
                videoMode!!.width(), videoMode!!.height(),
                builder.title, MemoryUtil.NULL, builder.sharedContext
            )
        }

        // Check whether the creation of the window went wrong.
        if (windowHandle == MemoryUtil.NULL) {
            val error = "Failed to create the GLFW window!"
            LOG.error(error)
            throw WindowCreationException(error)
        }
        val pos = builder.position
        // Moving the window is only allowed when the window is not in fullscreen mode.
        if (!builder.fullscreen) {
            // Center the window if at least one position argument is less the 0.
            if (pos.x < 0 || pos.y < 0) {
                center()
            } else {
                GLFW.glfwSetWindowPos(
                    windowHandle,
                    if (!builder.decorated) pos.x else pos.x + 1,
                    if (!builder.decorated) pos.y else pos.y + 31
                )
            }
        }

        // Cache the window properties (size, position, etc.).
        cacheWindowPosition()
        cacheWindowSize()
        cacheFramebufferSize()
        cacheWindowContentScale()
        cacheMonitorContentScale()
        cacheCursorPosition()

        // Make the OpenGL context current in the current thread.
        makeContextCurrent()

        // Set the icon of the window if an icon was specified.
        icon = builder.iconResources.map { ResourceName(it) }

        // This line is critical for LWJGL's interoperation with GLFW's OpenGL context, or any context that is managed
        // externally. LWJGL detects the context that is current in the current thread, creates the GLCapabilities
        // instance and makes the OpenGL bindings available for bind.
        GL.createCapabilities()

        // The context got created. => The context information object bound to this window object must
        // be initialized!
        contextInformation.load(id)
        contextInformation.printInfo()

        // Mark this object as being created and "ready to use".
        isCreated = true

        // Enable vSync if condition variable is set true.
        GLFW.glfwSwapInterval(if (builder.vSync) 1 else 0)
        if (contextInformation.isContextAtLeast(4, 3) && builder.debugMode) {
            enableDebugging(builder.debugBehaviour)
        }

        // Initialize all the callbacks.
        createCallbacks()

        // Print information of the actual data of the window to the console.
        printInfo()
    }

    /**
     * Sets the GLFW "window hints" appropriately (matching the requested settings as closely as possible).
     *
     * @param builder The fully set up window builder instance which contains all necessary data to create the window.
     */
    private fun setUpGLFWHints(builder: OpenGLWindowBuilder) {
        // Restore the window hints to their defaults to override any changes done by earlier window creations.
        GLFW.glfwDefaultWindowHints()

        // Set the window hints to the user specified values.
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, if (builder.visible) GL11.GL_TRUE else GL11.GL_FALSE)
        GLFW.glfwWindowHint(GLFW.GLFW_FOCUSED, GL11.GL_FALSE)
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, if (builder.resizable) GL11.GL_TRUE else GL11.GL_FALSE)
        GLFW.glfwWindowHint(GLFW.GLFW_DECORATED, if (builder.decorated) GL11.GL_TRUE else GL11.GL_FALSE)

        // Specify the context to create
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, builder.openGLTargetVersion.x)
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, builder.openGLTargetVersion.y)
        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, builder.openGLProfile.id)
        GLFW.glfwWindowHint(
            GLFW.GLFW_OPENGL_FORWARD_COMPAT,
            if (builder.openGLForwardCompatible) GL11.GL_TRUE else GL11.GL_FALSE
        )
        GLFW.glfwWindowHint(
            GLFW.GLFW_OPENGL_DEBUG_CONTEXT,
            if (builder.debugMode) GL11.GL_TRUE else GL11.GL_FALSE
        )
        GLFW.glfwWindowHint(GLFW.GLFW_RED_BITS, 10)
        GLFW.glfwWindowHint(GLFW.GLFW_GREEN_BITS, 10)
        GLFW.glfwWindowHint(GLFW.GLFW_BLUE_BITS, 10)
        GLFW.glfwWindowHint(GLFW.GLFW_ALPHA_BITS, 2)

        // Tell GLFW that we want an SRGB backbuffer.
        GLFW.glfwWindowHint(GLFW.GLFW_SRGB_CAPABLE, GL11.GL_TRUE)
        GLFW.glfwWindowHint(GLFW.GLFW_DOUBLEBUFFER, GL11.GL_TRUE)
    }

    companion object {
        private val LOG = getLogger<AbstractBuildableOpenGLWindow>()
    }

}
