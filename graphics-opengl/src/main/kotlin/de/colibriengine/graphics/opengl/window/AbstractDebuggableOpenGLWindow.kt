package de.colibriengine.graphics.opengl.window

import de.colibriengine.asset.ResourceLoader
import de.colibriengine.graphics.window.DebugBehaviour
import de.colibriengine.graphics.window.DebugMessage
import de.colibriengine.graphics.window.WindowManager
import de.colibriengine.input.InputManager
import de.colibriengine.logging.LogUtil.getLogger
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL43
import org.lwjgl.opengl.GL43C
import org.lwjgl.opengl.GLDebugMessageCallback
import org.lwjgl.system.MemoryUtil
import org.lwjgl.system.MemoryUtil.memUTF8

/**
 * An {@link AbstractOpenGLWindow} with additional debugging capabilities.
 * Debugging must be enabled through a call to {@link AbstractDebuggableOpenGLWindow#enableDebugging(DebugBehaviour)}.
 */
abstract class AbstractDebuggableOpenGLWindow(
    windowManager: WindowManager,
    inputManager: InputManager,
    resourceLoader: ResourceLoader
) : AbstractOpenGLWindow(windowManager, inputManager, resourceLoader) {

    // Debugging is disabled by default.
    private var debuggingActive = false

    /** GL callback references. Release them at termination. */
    private var glDebugMessageCallback: GLDebugMessageCallback? = null

    //TODO: allow debugging from the old KHR extension if maxOGLVersion is < 4.3.

    /**
     * Activates the debugging of OpenGL calls. Remember: The OpenGL API will only return error messages if the OpenGL
     * context was created in debug mode! Therefore set the window hint: `glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT,
     * GL_TRUE)` before creating a window. The `createDebugMessage(...)` method is used to create a formatted error
     * string. You can consider overwriting it. This method has no effect and will returns immediately if the debugging
     * had already started.
     *
     * @param debugBehavior Defines how the callback method will act if it gets called upon an error. Use one of:
     *
     *     DebugBehaviour.NO_EXCEPTION : Just prints out the generated error.
     *
     *     DebugBehaviour.CREATE_EXCEPTION : Prints out the error in conjunction with an exceptions stack trace.
     *     Preferable.
     *
     *     DebugBehaviour.THROW_EXCEPTION : Prints out the error and throws an exception, causing the application to
     *     terminate on the error that occurs first if the exception does not get caught.
     *
     * @throws IllegalArgumentException if the parameter does not match one of the specified constants.
     * @throws UnsupportedOperationException If the context version of this window is not greater or equal to 4.3.
     *     @minContextRequired 4.3
     */
    protected fun enableDebugging(debugBehavior: DebugBehaviour) {
        // Return immediately if the debugging for this context was already enabled.
        if (debuggingActive) {
            return
        }

        // The window must be created in order to enable debugging!
        check(isCreated) { "This window must first be created before its debugging capabilities can be enabled." }

        // The window must be at least of context version 4.3!
        if (!contextInformation.isContextAtLeast(4, 3)) {
            throw UnsupportedOperationException(
                "This windows context is not >= 4.3! Debugging capabilities are not available."
            )
        }
        assert(GLFW.glfwGetCurrentContext() != MemoryUtil.NULL) { "There is no context current on the current thread. Debugging can not be enabled." }
        GL11.glEnable(GL43.GL_DEBUG_OUTPUT_SYNCHRONOUS)

        // Receive the error messages in the overwritten invoke method.
        GL43.glDebugMessageCallback( // Store the generated callback, so a reference exists. Protects it from getting garbage collected.
            createDebugMessageCallback(debugBehavior).also {
                glDebugMessageCallback = it
            },  // Use the currently active window handle as the user parameter.
            GLFW.glfwGetCurrentContext()
        )

        // Set the control filter so that every message comes thru.
        GL43.glDebugMessageControl(
            GL11.GL_DONT_CARE,
            GL11.GL_DONT_CARE,
            GL11.GL_DONT_CARE,
            BufferUtils.createIntBuffer(0),
            true
        )
        debuggingActive = true

        // Print info about the successful establishment of the debug callback.
        LOG.info("AbstractWindow: Successfully created the debug callback.")
    }


    /**
     * Creates a new [GLDebugMessageCallback] object whose invoke method is overwritten to handle callbacks.
     *
     * @param debugBehavior Argument passed from {@link AbstractDebuggableOpenGLWindow#enableDebugging(DebugBehaviour)}.
     * @return A new {@code GLDebugMessageCallback} object.
     *
     * @minContextRequired 4.3
     */
    private fun createDebugMessageCallback(debugBehavior: DebugBehaviour): GLDebugMessageCallback =
        object : GLDebugMessageCallback() {
            override fun invoke(
                source: Int, type: Int, id: Int, severity: Int,
                length: Int, message: Long, userParam: Long
            ) {
                // Create an informational String that describes the error that occurred.
                val msg: DebugMessage = createDebugMessage(source, type, id, severity, length, message, userParam)
                //LOG.info(msg.getMessage())

                // Do nothing, inform the user through a stackTrace output, or throw an exception which will
                // stop the execution directly.
                when (debugBehavior) {
                    DebugBehaviour.NO_EXCEPTION -> { /* Intentionally do nothing. */
                    }
                    DebugBehaviour.CREATE_EXCEPTION -> {
                        if (!msg.isNotification) {
                            val runtimeException = RuntimeException(msg.message)
                            runtimeException.printStackTrace()
                        }
                    }
                    DebugBehaviour.THROW_EXCEPTION -> {
                        if (!msg.isNotification) {
                            throw RuntimeException(msg.message)
                        }
                    }
                    else -> throw IllegalStateException("Unexpected")
                }
            }
        }

    /**
     * Creates a message from the given callback parameters. You are release to override this method if you want the error
     * messages to be differently formatted.
     *
     * @param source Where did the error came from?
     * @param type Of what type is the error?
     * @param id Specific OpenGL error id.
     * @param severity How severe is the occurred error?
     * @param length The length of the message.
     * @param message The starting memory address of the message.
     * @param userParam The user parameter. This will be the context in which the error occurred.
     * @return an object of DebugMessage which includes a formatted error string and a boolean which is used to determine
     *     whether or not the error message is just a notification.
     *
     * @minContextRequired 4.3
     *
     * @see DebugMessage
     */
    private fun createDebugMessage(
        source: Int, type: Int, id: Int, severity: Int,
        length: Int, message: Long, userParam: Long
    ): DebugMessage {
        // Create a StringBuilder that will store the final output.
        val debugMsg = StringBuilder()

        debugMsg.append("--------------- opengl debug-message callback ---------------\n")

        // Get the error message by decoding of the memory address and its length.
        debugMsg.append(memUTF8(message)).append("\n")

        // Store the source that generated the error.
        debugMsg.append("source: ")
        when (source) {
            GL43C.GL_DEBUG_SOURCE_API -> debugMsg.append("GL_DEBUG_SOURCE_API")
            GL43C.GL_DEBUG_SOURCE_SHADER_COMPILER -> debugMsg.append("GL_DEBUG_SOURCE_SHADER_COMPILER")
            GL43C.GL_DEBUG_SOURCE_WINDOW_SYSTEM -> debugMsg.append("GL_DEBUG_SOURCE_WINDOW_SYSTEM")
            GL43C.GL_DEBUG_SOURCE_THIRD_PARTY -> debugMsg.append("GL_DEBUG_SOURCE_THIRD_PARTY")
            GL43C.GL_DEBUG_SOURCE_APPLICATION -> debugMsg.append("GL_DEBUG_SOURCE_APPLICATION")
            GL43C.GL_DEBUG_SOURCE_OTHER -> debugMsg.append("GL_DEBUG_SOURCE_OTHER")
            else -> throw IllegalStateException("Unexpected")
        }
        debugMsg.append(", ")

        // Store the type of the occurred error.
        debugMsg.append("type: ")
        when (type) {
            GL43C.GL_DEBUG_TYPE_ERROR -> debugMsg.append("ERROR")
            GL43C.GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR -> debugMsg.append("DEPRECATED_BEHAVIOR")
            GL43C.GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR -> debugMsg.append("UNDEFINED_BEHAVIOR")
            GL43C.GL_DEBUG_TYPE_PERFORMANCE -> debugMsg.append("PERFORMANCE")
            GL43C.GL_DEBUG_TYPE_PORTABILITY -> debugMsg.append("PORTABILITY")
            GL43C.GL_DEBUG_TYPE_MARKER -> debugMsg.append("MARKER")
            GL43C.GL_DEBUG_TYPE_PUSH_GROUP -> debugMsg.append("PUSH_GROUP")
            GL43C.GL_DEBUG_TYPE_POP_GROUP -> debugMsg.append("POP_GROUP")
            GL43C.GL_DEBUG_TYPE_OTHER -> debugMsg.append("OTHER")
            else -> throw IllegalStateException("Unexpected")
        }
        debugMsg.append(", ")

        // Store the specific id of the occurred error.
        debugMsg.append("id: ").append(id).append("  , ")

        // Store how severe the occurred error was.
        debugMsg.append("severity: ")
        var isNotification = false
        when (severity) {
            GL43C.GL_DEBUG_SEVERITY_LOW -> debugMsg.append("LOW")
            GL43C.GL_DEBUG_SEVERITY_MEDIUM -> debugMsg.append("MEDIUM")
            GL43C.GL_DEBUG_SEVERITY_HIGH -> debugMsg.append("HIGH")
            GL43C.GL_DEBUG_SEVERITY_NOTIFICATION -> {
                isNotification = true
                debugMsg.append("NOTIFICATION")
            }
            else -> throw IllegalStateException("Unexpected")
        }
        debugMsg.append(", ")

        // Store the context the error occurred in.
        debugMsg.append("context: ").append(userParam)

        debugMsg.append("\n")
        debugMsg.append("-------------------------------------------------------------\n")

        return DebugMessage(debugMsg.toString(), isNotification)
    }

    /**
     * Deactivates the debugging of the OpenGL application. If the debugging had already stopped, this method has no
     * effect.
     *
     * @minContextRequired 4.3
     */
    protected fun disableDebugging() {
        if (!debuggingActive) {
            return
        }
        GL11.glDisable(GL43.GL_DEBUG_OUTPUT_SYNCHRONOUS)

        // Sets the stored reference and callback to null.
        //TODO needed?
        glDebugMessageCallback!!.free()
        glDebugMessageCallback = null
        GL43.glDebugMessageCallback(null, MemoryUtil.NULL)
        debuggingActive = false

        // Print info about the successful establishment of the debug callback.
        LOG.info("AbstractWindow: Successfully disabled the debug callback.")
    }

    companion object {
        private val LOG = getLogger<AbstractDebuggableOpenGLWindow>()
    }

}
