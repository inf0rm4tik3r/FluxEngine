package de.colibriengine.graphics.opengl.window

class NoopOpenGLWindow private constructor(builder: OpenGLWindowBuilder) : AbstractBuildableOpenGLWindow(builder) {

    override fun beforeDestruction() {
        // Do nothing
    }

    override fun afterDestruction() {
        // Do nothing
    }

    override fun errorCallback(error: Int, description: String) {
        // Do nothing
    }

    override fun closeCallback() {
        // Do nothing
    }

    override fun focusCallback(focused: Boolean) {
        // Do nothing
    }

    override fun sizeCallback(width: Int, height: Int) {
        // Do nothing
    }

    override fun posCallback(xpos: Int, ypos: Int) {
        // Do nothing
    }

    override fun iconifyCallback(iconified: Boolean) {
        // Do nothing
    }

    override fun maximizeCallback(maximized: Boolean) {
        // Do nothing
    }

    override fun refreshCallback() {
        // Do nothing
    }

    override fun dropCallback(names: Array<String>) {
        // Do nothing
    }

    override fun framebufferSizeCallback(width: Int, height: Int) {
        // Do nothing
    }

    override fun contentScaleCallback(xScale: Float, yScale: Float) {
        // Do nothing
    }

    override fun keyCallback(key: Int, scancode: Int, action: Int, mods: Int) {
        // Do nothing
    }

    override fun charCallback(codepoint: Int) {
        // Do nothing
    }

    override fun charModsCallBack(codepoint: Int, mods: Int) {
        // Do nothing
    }

    override fun cursorEnterCallback(entered: Boolean) {
        // Do nothing
    }

    override fun cursorPosCallback(xpos: Double, ypos: Double) {
        // Do nothing
    }

    override fun mouseButtonCallback(button: Int, action: Int, mods: Int) {
        // Do nothing
    }

    override fun scrollCallback(xoffset: Double, yoffset: Double) {
        // Do nothing
    }

}
