package de.colibriengine.input

/**
 * Represents an action the user can produce by using Keyboard+Mouse or Gamepad.
 *
 * @since ColibriEngine 0.0.6.2
 */
data class Command(

    val kKey: Int,
    val kKeyAction: Int,
    val jButton: Int,
    val jButtonAction: Int,
    val jAxis: Int

)
