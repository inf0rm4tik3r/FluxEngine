package de.colibriengine.input

enum class InputType {

    MOUSE_KEYBOARD, JOYSTICK

}
