package de.colibriengine.input

/**
 * JoystickInputCallbacks
 *
 * @since ColibriEngine 0.0.6.7
 */
interface JoystickInputCallbacks {

    fun jAxisCallback(axis: Int, axisValue: Float) {}
    fun jButtonCallback(button: Int, action: Int) {}
    fun jLostConnectionCallback(index: Int) {}

}
