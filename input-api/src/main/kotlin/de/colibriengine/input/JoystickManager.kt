package de.colibriengine.input

interface JoystickManager {

    fun checkJoystickConnections();

    fun updateJoystickData()

}
