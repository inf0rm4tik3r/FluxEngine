package de.colibriengine.input

/**
 * The InputCallback interface delivers keyboard and mouse callback methods. Implement this interface in classes where
 * you need the generated input and add objects of that class to the engine by using the
 * [InputManager.addMKInputCallback] method.
 *
 * @since ColibriEngine 0.0.5.4
 */
interface MouseKeyboardInputCallbacks {

    fun mkPosCallback(x: Int, y: Int) {}
    fun mkMoveCallback(dx: Int, dy: Int) {}
    fun mkButtonCallback(button: Int, action: Int, mods: Int) {}
    fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {}

}
