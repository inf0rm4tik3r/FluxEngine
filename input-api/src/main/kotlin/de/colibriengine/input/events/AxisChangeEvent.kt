package de.colibriengine.input.events

import de.colibriengine.objectpooling.ObjectPool

/**
 * An event that represents a change of an axis.
 *
 * @since ColibriEngine 0.0.6.7
 */
class AxisChangeEvent private constructor() {

    var axis: Int = 0
        private set

    var value: Float = 0.0f
        private set

    operator fun set(axis: Int, value: Float): AxisChangeEvent {
        this.axis = axis
        this.value = value
        return this
    }

    fun free() {
        POOL.free(this)
    }

    fun reset(): AxisChangeEvent {
        // Intentionally do nothing.
        return this
    }

    companion object {
        private val POOL = ObjectPool(::AxisChangeEvent, AxisChangeEvent::reset)

        @JvmStatic
        fun acquire(): AxisChangeEvent {
            return POOL.acquire()
        }
    }

}
