package de.colibriengine.input.events

import de.colibriengine.objectpooling.ObjectPool

/**
 * An event that represents a change of a button.
 *
 * @since ColibriEngine 0.0.6.7
 */
class ButtonChangeEvent private constructor() {

    var button: Int = 0
        private set

    var action: Int = 0
        private set

    operator fun set(button: Int, action: Int): ButtonChangeEvent {
        this.button = button
        this.action = action
        return this
    }

    fun free() {
        POOL.free(this)
    }

    fun reset(): ButtonChangeEvent {
        // Intentionally do nothing.
        return this
    }

    companion object {
        private val POOL = ObjectPool(::ButtonChangeEvent, ButtonChangeEvent::reset)

        @JvmStatic
        fun acquire(): ButtonChangeEvent {
            return POOL.acquire()
        }
    }
}
