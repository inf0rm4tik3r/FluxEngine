package de.colibriengine.input.gamepads

/**
 * @since ColibriEngine 0.0.6.2
 */
interface Gamepad {

    /** Default values for each axis of the gamepad. Typically the values reported whenever a stick is not touched. */
    var axisDefaultValues: IntArray

    /** Threshold values for each axis of the gamepad. */
    var axisThresholds: FloatArray

    /**
     * Check if the specified value for the given axis is out of the threshold, which got defined for this specific
     * axis.
     *
     * @param axis The axis for which a value should be checked.
     * @param value The value to check.
     * @return Returns true, if `value` is either below the negative or above the positive threshold of the specified
     *     `axis`.
     */
    fun outOfThreshold(axis: Int, value: Float): Boolean {
        val positive = axisDefaultValues[axis] + axisThresholds[axis]
        val negative = axisDefaultValues[axis] - axisThresholds[axis]
        return value < negative || value > positive
    }

    /**
     * Returns either the supplied value, or 0.0f, if the `value` lies in the threshold of the specified `axis`.
     *
     * @param axis The axis for which a value should be checked.
     * @param value The value to check.
     * @return Returns either the supplied value, or 0.0f, if the `value` lies in the threshold of the specified `axis`.
     */
    fun thresholdChecked(axis: Int, value: Float): Float = if (outOfThreshold(axis, value)) value else 0.0f

}
