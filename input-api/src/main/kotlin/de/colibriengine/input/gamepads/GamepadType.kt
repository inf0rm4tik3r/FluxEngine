package de.colibriengine.input.gamepads

/**
 * Provides a set of supported gamepads.
 *
 * @since ColibriEngine 0.0.6.2
 */
enum class GamepadType {

    XBOX_360,
    XBOX_ONE

}
