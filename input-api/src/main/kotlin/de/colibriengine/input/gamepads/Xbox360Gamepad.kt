package de.colibriengine.input.gamepads

/**
 * Xbox360Gamepad
 *
 * @since ColibriEngine 0.0.6.2
 */
object Xbox360Gamepad : Gamepad {

    /** Button mapping. */
    const val A = 0
    const val B = 1
    const val X = 2
    const val Y = 3
    const val LB = 4
    const val RB = 5
    const val BACK = 6
    const val START = 7
    const val LS_BUTTON = 8
    const val RS_BUTTON = 9
    const val DPAD_UP = 10
    const val DPAD_RIGHT = 11
    const val DPAD_DOWN = 12
    const val DPAD_LEFT = 13

    /** Axis mapping. */
    const val LS_X = 0
    const val LS_Y = 1
    const val LTRT = 2
    const val RS_Y = 3
    const val RS_X = 4

    override var axisDefaultValues: IntArray = IntArray(6)
    override var axisThresholds: FloatArray = FloatArray(5)

    init {
        axisDefaultValues[LS_X] = 0
        axisDefaultValues[LS_Y] = 0
        axisDefaultValues[LTRT] = 0
        axisDefaultValues[RS_X] = 0
        axisDefaultValues[RS_Y] = 0

        axisThresholds[LS_X] = 0.1f
        axisThresholds[LS_Y] = 0.1f
        axisThresholds[LTRT] = 0.0f
        axisThresholds[RS_Y] = 0.1f
        axisThresholds[RS_X] = 0.1f
    }

}
