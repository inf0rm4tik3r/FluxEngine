package de.colibriengine.input

import de.colibriengine.input.events.AxisChangeEvent
import de.colibriengine.input.events.ButtonChangeEvent
import de.colibriengine.input.events.ConnectionLossEvent

private const val INITIAL_CAPACITY = 8

class JoystickEvents {

    private val _connectionLossEvents: MutableList<ConnectionLossEvent> = ArrayList(INITIAL_CAPACITY / 4)
    private val _axisChangeEvents: MutableList<AxisChangeEvent> = ArrayList(INITIAL_CAPACITY)
    private val _buttonChangeEvents: MutableList<ButtonChangeEvent> = ArrayList(INITIAL_CAPACITY)

    val connectionLossEvents: List<ConnectionLossEvent> = _connectionLossEvents
    val axisChangeEvents: List<AxisChangeEvent> = _axisChangeEvents
    val buttonChangeEvents: List<ButtonChangeEvent> = _buttonChangeEvents

    fun addConnectionLossEvent(clEvent: ConnectionLossEvent) {
        _connectionLossEvents.add(clEvent)
    }

    fun addAxisChangeEvent(axisChangeEvent: AxisChangeEvent) {
        _axisChangeEvents.add(axisChangeEvent)
    }

    fun addButtonChangeEvent(buttonChangeEvent: ButtonChangeEvent) {
        _buttonChangeEvents.add(buttonChangeEvent)
    }

    fun isNotEmpty(): Boolean =
        connectionLossEvents.isNotEmpty() || axisChangeEvents.isNotEmpty() || buttonChangeEvents.isNotEmpty()

    fun clear() {
        _connectionLossEvents.forEach { it.free() }
        _connectionLossEvents.clear()
        _axisChangeEvents.forEach { it.free() }
        _axisChangeEvents.clear()
        _buttonChangeEvents.forEach { it.free() }
        _buttonChangeEvents.clear()
    }

}
