package de.colibriengine.input

class NoopJoystickManager : JoystickManager {
    
    override fun checkJoystickConnections() {
        // Do nothing
    }

    override fun updateJoystickData() {
        // Do nothing
    }
    
}
