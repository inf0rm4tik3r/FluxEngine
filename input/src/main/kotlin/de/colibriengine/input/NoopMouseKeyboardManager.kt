package de.colibriengine.input

import de.colibriengine.math.vector.vec2d.StdVec2d
import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2i.StdVec2i
import de.colibriengine.math.vector.vec2i.Vec2iAccessor

class NoopMouseKeyboardManager : MouseKeyboardManager {

    override val mousePos: Vec2iAccessor = StdVec2i()
    override val mouseDelta: Vec2dAccessor = StdVec2d()

    override fun resetDelta() {
        // Do nothing
    }

    override fun trackCursor(xPos: Double, yPos: Double) {
        // Do nothing
    }

    override fun invokeKeyCallbacks(key: Int, scancode: Int, action: Int, mods: Int) {
        // Do nothing
    }

    override fun invokeMousePosCallbacks(x: Int, y: Int) {
        // Do nothing
    }

    override fun invokeMouseMoveCallbacks(dx: Int, dy: Int) {
        // Do nothing
    }

    override fun invokeMouseButtonCallbacks(button: Int, action: Int, mods: Int) {
        // Do nothing
    }

    override fun isKeyPressed(keyCode: Int): Boolean {
        // Do nothing
        return false
    }

    override fun isKeyReleased(keyCode: Int): Boolean {
        // Do nothing
        return false
    }

}
