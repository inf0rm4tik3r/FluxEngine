package de.colibriengine.lambda

/**
 * This lambda consumes two arguments and produces no output.
 *
 * @param C1 The type of the first argument consumed by the [call] method.
 * @param C2 The type of the second argument consumed by the [call] method.
 * @since ColibriEngine 0.0.7.2
 */
@FunctionalInterface
@Deprecated("use kotlin instead :)")
interface BinaryConsumerLambda<C1, C2> {

    fun call(argument1: C1, argument2: C2)

}
