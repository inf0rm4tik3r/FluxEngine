package de.colibriengine.lambda

/**
 * Consumes nothing and produces an output.
 *
 * @param P The type of the object which gets produced.
 * @since ColibriEngine 0.0.7.2
 */
@FunctionalInterface
@Deprecated("use kotlin instead :)")
interface NullaryProducerLambda<P> {

    fun call(): P

}
