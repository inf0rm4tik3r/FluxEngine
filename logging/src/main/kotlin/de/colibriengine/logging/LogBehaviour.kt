package de.colibriengine.logging

/**
 * LogBehaviour - Hints for how some information should be logged.
 *
 * @since ColibriEngine 0.0.7.1
 */
enum class LogBehaviour {

    /**
     * Log the information through a logger.
     */
    LOG_MESSAGE,

    /**
     * Create an exception and print its stack trace.
     */
    LOG_EXCEPTION,

    /**
     * Create and throw an exception.
     */
    THROW_EXCEPTION

}
