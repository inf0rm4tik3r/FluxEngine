package de.colibriengine.logging

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

/** Provides utility functions for working with the underlying logging system. */
object LogUtil {

    /** The common package in which all engine related classes lie. */
    private const val DEFAULT_PACKAGE_NAME = "de.colibriengine."

    /** Path to the log4j2 configuration file. */
    private const val LOG4J2_CONFIGURATION_FILE_PATH = "config/log4j2.xml"

    private val log: Logger by lazy {
        // Bypassing SystemUtil.setSystemProperty(), because SystemUtil would not be able to create a logger.
        System.setProperty("log4j.configurationFile", LOG4J2_CONFIGURATION_FILE_PATH)

        // As the logging facility is now initialized, we can obtain our first logger.
        val logger = LogManager.getLogger(LogUtil::class.java)
        logger.trace("Logging initialized using configuration at: $LOG4J2_CONFIGURATION_FILE_PATH")

        logger
    }

    /**
     * Returns a logger instance for the specified class. The name of this instance will contain the classes full
     * class path excluding the default package name of this engine denoted in [LogUtil.DEFAULT_PACKAGE_NAME]. If the
     * specified class does not lie in this package, the loggers name will just be the full class path.
     *
     * @param clazz The class for which the logger should be created.
     * @return A log4j logger instance.
     */
    @JvmStatic
    fun getLogger(clazz: Class<*>): Logger {
        log.trace("For: " + clazz.canonicalName)

        // This line might be used to compress the output a little bit.
        //return LogManager.getLogger(clazz.getCanonicalName().replace(DEFAULT_PACKAGE_NAME, ""));
        return LogManager.getLogger(clazz.simpleName)
    }

    inline fun <reified T> getLogger() : Logger {
        return getLogger(T::class.java)
    }

    @JvmStatic
    fun getLogger(type: Any): Logger {
        val name = type::class.java.name
        val simpleName = name.replaceRange(0, name.lastIndexOf('.') + 1, "")
        val kotlinPruned = simpleName.replace("\$Companion", "")
        return getLogger(kotlinPruned)
    }

    @JvmStatic
    fun getLogger(name: String): Logger {
        log.trace("For: $name")
        return LogManager.getLogger(name)
    }

}
