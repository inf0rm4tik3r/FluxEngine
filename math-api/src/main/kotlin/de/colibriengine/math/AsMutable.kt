package de.colibriengine.math

interface AsMutable<MutableType> {

    /**
     * Returns a mutable version of this object.
     *
     * @return A copy of this object which can be mutated.
     */
    fun asMutable(): MutableType

}
