package de.colibriengine.math

interface Copyable<Type> {

    /**
     * Creates a deep copy of this object.
     *
     * @return A copy of this object in the exact same state as the original, distinguishable only by their memory
     *     reference.
     */
    fun copy(): Type;

}
