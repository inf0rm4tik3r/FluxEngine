package de.colibriengine.math.matrix

import de.colibriengine.math.matrix.mat2d.Mat2dFactory
import de.colibriengine.math.matrix.mat2f.Mat2fFactory
import de.colibriengine.math.matrix.mat3d.Mat3dFactory
import de.colibriengine.math.matrix.mat3f.Mat3fFactory
import de.colibriengine.math.matrix.mat4d.Mat4dFactory
import de.colibriengine.math.matrix.mat4f.Mat4fFactory

interface MatrixFactories {

    fun mat2f(): Mat2fFactory
    fun mat2d(): Mat2dFactory
    fun mat3f(): Mat3fFactory
    fun mat3d(): Mat3dFactory
    fun mat4f(): Mat4fFactory
    fun mat4d(): Mat4dFactory

}
