package de.colibriengine.math.matrix.mat2d

import de.colibriengine.math.vector.vec2d.Vec2dAccessor

/** Immutable version of the [Mat2d]. */
interface ImmutableMat2d : Mat2dAccessor {

    interface Builder {
        fun of(value: Double): ImmutableMat2d

        fun of(
            m00: Double, m01: Double,
            m10: Double, m11: Double
        ): ImmutableMat2d

        fun of(other: Mat2dAccessor): ImmutableMat2d

        fun ofColumns(
            column1: Vec2dAccessor,
            column2: Vec2dAccessor
        ): ImmutableMat2d

        fun ofRows(
            row1: Vec2dAccessor,
            row2: Vec2dAccessor
        ): ImmutableMat2d

        fun setM00(m00: Double): Builder
        fun setM01(m01: Double): Builder
        fun setM10(m10: Double): Builder
        fun setM11(m11: Double): Builder

        fun set(value: Double): Builder
        operator fun set(
            m00: Double, m01: Double,
            m10: Double, m11: Double
        ): Builder

        fun set(other: Mat2dAccessor): Builder

        fun setColumn1(column1: Vec2dAccessor): Builder
        fun setColumn2(column2: Vec2dAccessor): Builder

        fun setRow1(row1: Vec2dAccessor): Builder
        fun setRow2(row2: Vec2dAccessor): Builder

        fun build(): ImmutableMat2d
    }

}
