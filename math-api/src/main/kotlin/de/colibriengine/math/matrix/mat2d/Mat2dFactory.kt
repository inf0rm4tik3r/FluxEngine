package de.colibriengine.math.matrix.mat2d

/** Every [Mat2d] factory implementation should expose these methods. */
interface Mat2dFactory {

    fun acquire(): Mat2d

    fun acquireDirty(): Mat2d

    fun free(mat2d: Mat2d)

    fun immutableMat2dBuilder(): ImmutableMat2d.Builder

    fun zero(): ImmutableMat2d

    fun one(): ImmutableMat2d

    fun identity(): ImmutableMat2d

}
