package de.colibriengine.math.matrix.mat2d

interface Mat2dStaticOps {

    fun determinantOf(
        m00: Double, m01: Double,
        m10: Double, m11: Double
    ): Double

}
