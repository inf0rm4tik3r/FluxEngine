package de.colibriengine.math.matrix.mat2f

import de.colibriengine.math.vector.vec2f.Vec2fAccessor

/** Immutable version of the [Mat2f]. */
interface ImmutableMat2f : Mat2fAccessor {

    interface Builder {
        fun of(value: Float): ImmutableMat2f

        fun of(
            m00: Float, m01: Float,
            m10: Float, m11: Float
        ): ImmutableMat2f

        fun of(other: Mat2fAccessor): ImmutableMat2f

        fun ofColumns(
            column1: Vec2fAccessor,
            column2: Vec2fAccessor
        ): ImmutableMat2f

        fun ofRows(
            row1: Vec2fAccessor,
            row2: Vec2fAccessor
        ): ImmutableMat2f

        fun setM00(m00: Float): Builder
        fun setM01(m01: Float): Builder
        fun setM10(m10: Float): Builder
        fun setM11(m11: Float): Builder

        fun set(value: Float): Builder
        fun set(
            m00: Float, m01: Float,
            m10: Float, m11: Float
        ): Builder

        fun set(other: Mat2fAccessor): Builder

        fun setColumn1(column1: Vec2fAccessor): Builder
        fun setColumn2(column2: Vec2fAccessor): Builder

        fun setRow1(row1: Vec2fAccessor): Builder
        fun setRow2(row2: Vec2fAccessor): Builder

        fun build(): ImmutableMat2f
    }

}
