package de.colibriengine.math.matrix.mat2f

import de.colibriengine.math.matrix.mat2d.Mat2dAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor

/** Defines all mutating methods of the 2x2 float matrix. */
interface Mat2fMutator {

    /** First row, first column. */
    var m00: Float

    /** First row, second column. */
    var m01: Float

    /** Second row, first column. */
    var m10: Float

    /** Second row, second column. */
    var m11: Float

    /**
     * Sets the value in the first row and first column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM00(value: Float): Mat2fMutator

    /**
     * Sets the value in the first row and second column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM01(value: Float): Mat2fMutator

    /**
     * Sets the value in the second row and first column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM10(value: Float): Mat2fMutator

    /**
     * Sets the value in the second row and second column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM11(value: Float): Mat2fMutator

    /**
     * Sets the value at the cell defined by the intersection of [row] and [column] in the graphical layout of this 2x2
     * matrix.
     *
     * @param row The row to look in.
     * @param column The column to look in.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the row or the column index are not in the `[0..1]` range.
     */
    operator fun set(row: Int, column: Int, value: Float): Mat2fMutator

    /**
     * Sets the values of the specified [row] to the provided `(x y)` vector.
     *
     * @param row The index of the row to set. Must be in range `[0..1]`.
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given row index is not in range `[0..1]`.
     */
    fun setRow(row: Int, x: Float, y: Float): Mat2fMutator

    /**
     * Sets the values of the first row to the provided `(x y)` vector.
     *
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @return This instance for method chaining.
     */
    fun setRow1(x: Float, y: Float): Mat2fMutator

    /**
     * Sets the values of the second row to the provided `(x y)` vector.
     *
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @return This instance for method chaining.
     */
    fun setRow2(x: Float, y: Float): Mat2fMutator

    /**
     * Sets the values of the specified [row] to the values provided by the [vector] instance.
     *
     * @param row The index of the row to set. Must be in range `[0..1]`.
     * @param vector The vector which holds the new values fot the specified row.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given row index is not in range `[0..1]`.
     */
    fun setRow(row: Int, vector: Vec2fAccessor): Mat2fMutator

    /**
     * Sets the values of the first row to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the fist row.
     * @return This instance for method chaining.
     */
    fun setRow1(vector: Vec2fAccessor): Mat2fMutator

    /**
     * Sets the values of the second row to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the second row.
     * @return This instance for method chaining.
     */
    fun setRow2(vector: Vec2fAccessor): Mat2fMutator

    /**
     * Sets the values of the specified [column] to the provided `(x y)` vector.
     *
     * @param column The index of the column to set. Must be in range `[0..1]`.
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given column index is not in range `[0..1]`.
     */
    fun setColumn(column: Int, x: Float, y: Float): Mat2fMutator

    /**
     * Sets the values of the first column to the provided `(x y)` vector.
     *
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @return This instance for method chaining.
     */
    fun setColumn1(x: Float, y: Float): Mat2fMutator

    /**
     * Sets the values of the second column to the provided `(x y)` vector.
     *
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @return This instance for method chaining.
     */
    fun setColumn2(x: Float, y: Float): Mat2fMutator

    /**
     * Sets the values of the specified [column] to the values provided by the [vector] instance.
     *
     * @param column The index of the column to set. Must be in range `[0..1]`.
     * @param vector The vector which holds the new values fot the specified column.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given column index is not in range `[0..1]`.
     */
    fun setColumn(column: Int, vector: Vec2fAccessor): Mat2fMutator

    /**
     * Sets the values of the first column to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the fist column.
     * @return This instance for method chaining.
     */
    fun setColumn1(vector: Vec2fAccessor): Mat2fMutator

    /**
     * Sets the values of the second column to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the second column.
     * @return This instance for method chaining.
     */
    fun setColumn2(vector: Vec2fAccessor): Mat2fMutator

    /**
     * Sets all components of this matrix to the given value.
     *
     * @param value The value to use for each component of this matrix.
     * @return This instance for method chaining.
     */
    fun set(value: Float): Mat2fMutator

    /**
     * Sets the matrix to the specified values.
     *
     *      ( m00, m01 )
     *      ( m10, m11 )
     *
     * @param m00 First row.
     * @param m01 -
     * @param m10 Second row.
     * @param m11 -
     * @return This instance for method chaining.
     */
    operator fun set(
        m00: Float, m01: Float,
        m10: Float, m11: Float
    ): Mat2fMutator

    /**
     * Sets the values of this 2x2 matrix to the values of the given 2x2 matrix.
     *
     * @param other The matrix object to copy.
     * @return This instance for method chaining.
     */
    fun set(other: Mat2fAccessor): Mat2fMutator

    /**
     * Sets the values of this 2x2 matrix to the values of the given 2x2 matrix.
     *
     * @param other The matrix object to copy.
     * @return This instance for method chaining.
     */
    fun set(other: Mat2dAccessor): Mat2fMutator

    /**
     * Copies all values from the specified data array.
     *
     * @param matrixData 2x2 float array containing the matrix data to copy.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given matrix does not have a dimension of 2x2.
     */
    fun set(matrixData: Array<FloatArray>): Mat2fMutator

    /**
     * Initializes the zero matrix of the form:
     *
     *      ( 0.0, 0.0 )
     *      ( 0.0, 0.0 )
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Mat2fMutator

    /**
     * Initializes the identity matrix of the form:
     *
     *      ( 1.0, 0.0 )
     *      ( 0.0, 1.0 )
     *
     * @return This instance for method chaining.
     */
    fun initIdentity(): Mat2fMutator

    /**
     * Adds the given float value componentwise to this matrix and returns this object afterwards.
     *
     * @param value The float value which gets added.
     * @return This instance for method chaining.
     */
    fun plus(value: Float): Mat2fMutator

    /**
     * Adds the given 2x2 matrix componentwise to this matrix and returns this object afterwards.
     *
     * @param other The matrix which values get added to this matrix.
     * @return This instance for method chaining.
     */
    fun plus(other: Mat2fAccessor): Mat2fMutator

    /**
     * Subtracts the given float value componentwise from this matrix and returns this object afterwards.
     *
     * @param value The float value which gets subtracted.
     * @return This instance for method chaining.
     */
    fun minus(value: Float): Mat2fMutator

    /**
     * Subtracts the given 2x2 matrix componentwise from this matrix and returns this object afterwards.
     *
     * @param other The matrix which values get subtracted from this matrix.
     * @return This instance for method chaining.
     */
    fun minus(other: Mat2fAccessor): Mat2fMutator

    /**
     * Multiplies this matrix with a float value and returns this object afterwards.
     *
     * @param value Float value which which every field of this matrix gets multiplied.
     * @return This instance for method chaining.
     */
    fun times(value: Float): Mat2fMutator

    /**
     * Multiplies this matrix with the given matrix and returns this object afterwards.
     *
     * @param other The 2x2 matrix with which this matrix gets multiplied.
     * @return This instance for method chaining.
     */
    fun times(other: Mat2fAccessor): Mat2fMutator

    /**
     * Multiplies this matrix with itself and returns this object afterwards.
     *
     * @return This instance for method chaining.
     */
    fun mulSelf(): Mat2fMutator

    /**
     * Calculates the inverse of this matrix and returns this object afterwards. May be expensive to compute! Only use
     * when necessary!
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the determinant of this matrix is zero.
     */
    fun inverse(): Mat2fMutator

    /**
     * Transposes this matrix around its major diagonal and returns this object afterwards. Rows to columns, columns to
     * rows...
     *
     *      ( 00 01 )      ( 00 10 )
     *      ( 10 11 )  ->  ( 01 11 )
     *
     * @return This instance for method chaining.
     */
    fun transpose(): Mat2fMutator

    /**
     * Raises this matrix to the power of [exponent].
     * - M^-1 -> inverse();
     * - M^0 -> I (Identity matrix)
     * - M^1 -> M
     * - M^(n>1) -> n-1 multiplications
     * - I^n -> I
     *
     * @param exponent Exponent by which this matrix is raised.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given exponent is smaller than -1.
     */
    fun pow(exponent: Int): Mat2fMutator

    /**
     * Sets all components of this matrix to their absolute values.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Mat2fMutator

    /**
     * Makes the components more user / reader-friendly.
     *
     * @return This object for method chaining.
     */
    fun shorten(): Mat2fMutator

}
