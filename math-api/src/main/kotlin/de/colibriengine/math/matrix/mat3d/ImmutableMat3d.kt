package de.colibriengine.math.matrix.mat3d

import de.colibriengine.math.vector.vec3d.Vec3dAccessor

/** Immutable version of the [Mat3d]. */
interface ImmutableMat3d : Mat3dAccessor {

    interface Builder {
        fun of(value: Double): ImmutableMat3d

        fun of(
            m00: Double, m01: Double, m02: Double,
            m10: Double, m11: Double, m12: Double,
            m20: Double, m21: Double, m22: Double
        ): ImmutableMat3d

        fun of(other: Mat3dAccessor): ImmutableMat3d

        fun ofColumns(
            column1: Vec3dAccessor,
            column2: Vec3dAccessor,
            column3: Vec3dAccessor
        ): ImmutableMat3d

        fun ofRows(
            row1: Vec3dAccessor,
            row2: Vec3dAccessor,
            row3: Vec3dAccessor
        ): ImmutableMat3d

        fun set00(m00: Double): Builder
        fun set01(m01: Double): Builder
        fun set02(m02: Double): Builder
        fun set10(m10: Double): Builder
        fun set11(m11: Double): Builder
        fun set12(m12: Double): Builder
        fun set20(m20: Double): Builder
        fun set21(m21: Double): Builder
        fun set22(m22: Double): Builder
        fun set(value: Double): Builder

        operator fun set(
            m00: Double, m01: Double,
            m02: Double,
            m10: Double, m11: Double, m12: Double,
            m20: Double, m21: Double, m22: Double
        ): Builder

        fun set(other: Mat3dAccessor): Builder

        fun setColumn1(column1: Vec3dAccessor): Builder
        fun setColumn2(column2: Vec3dAccessor): Builder
        fun setColumn3(column3: Vec3dAccessor): Builder

        fun setRow1(row1: Vec3dAccessor): Builder
        fun setRow2(row2: Vec3dAccessor): Builder
        fun setRow3(row3: Vec3dAccessor): Builder

        fun build(): ImmutableMat3d
    }

}
