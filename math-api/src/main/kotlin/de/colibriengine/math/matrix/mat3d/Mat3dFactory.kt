package de.colibriengine.math.matrix.mat3d

/** Every [Mat3d] factory implementation should expose these methods. */
interface Mat3dFactory {

    fun acquire(): Mat3d

    fun acquireDirty(): Mat3d

    fun free(mat3d: Mat3d)

    fun immutableMat3dBuilder(): ImmutableMat3d.Builder

    fun zero(): ImmutableMat3d

    fun one(): ImmutableMat3d

    fun identity(): ImmutableMat3d

}
