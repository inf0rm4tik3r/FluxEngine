package de.colibriengine.math.matrix.mat3d

import de.colibriengine.math.matrix.mat3f.Mat3fAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor

/** Defines all mutating methods of the 3x3 double matrix. */
interface Mat3dMutator {

    /** First row, first column. */
    var m00: Double

    /** First row, second column. */
    var m01: Double

    /** First row, third column. */
    var m02: Double

    /** Second row, first column. */
    var m10: Double

    /** Second row, second column. */
    var m11: Double

    /** Second row, third column. */
    var m12: Double

    /** Third row, first column. */
    var m20: Double

    /** Third row, second column. */
    var m21: Double

    /** Third row, third column. */
    var m22: Double

    /**
     * Sets the value in the first row and first column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM00(value: Double): Mat3dMutator

    /**
     * Sets the value in the first row and second column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM01(value: Double): Mat3dMutator

    /**
     * Sets the value in the first row and third column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM02(value: Double): Mat3dMutator

    /**
     * Sets the value in the second row and first column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM10(value: Double): Mat3dMutator

    /**
     * Sets the value in the second row and second column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM11(value: Double): Mat3dMutator

    /**
     * Sets the value in the second row and third column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM12(value: Double): Mat3dMutator

    /**
     * Sets the value in the third row and first column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM20(value: Double): Mat3dMutator

    /**
     * Sets the value in the third row and second column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM21(value: Double): Mat3dMutator

    /**
     * Sets the value in the third row and third column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM22(value: Double): Mat3dMutator

    /**
     * Sets the value at the cell defined by the intersection of [row] and [column] in the graphical layout of this 3x3
     * matrix.
     *
     * @param row The row to look in.
     * @param column The column to look in.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the row or the column index are not in the `[0..1]` range.
     */
    operator fun set(row: Int, column: Int, value: Double): Mat3dMutator

    /**
     * Sets the values of the specified [row] to the provided `(x y z)` vector.
     *
     * @param row The index of the row to set. Must be in range `[0..1]`.
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @param z Value for the third column.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given row index is not in range `[0..1]`.
     */
    fun setRow(row: Int, x: Double, y: Double, z: Double): Mat3dMutator

    /**
     * Sets the values of the first row to the provided `(x y z)` vector.
     *
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @param z Value for the third column.
     * @return This instance for method chaining.
     */
    fun setRow1(x: Double, y: Double, z: Double): Mat3dMutator

    /**
     * Sets the values of the second row to the provided `(x y z)` vector.
     *
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @param z Value for the third column.
     * @return This instance for method chaining.
     */
    fun setRow2(x: Double, y: Double, z: Double): Mat3dMutator

    /**
     * Sets the values of the third row to the provided `(x y z)` vector.
     *
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @param z Value for the third column.
     * @return This instance for method chaining.
     */
    fun setRow3(x: Double, y: Double, z: Double): Mat3dMutator

    /**
     * Sets the values of the specified [row] to the values provided by the [vector] instance.
     *
     * @param row The index of the row to set. Must be in range `[0..1]`.
     * @param vector The vector which holds the new values fot the specified row.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given row index is not in range `[0..1]`.
     */
    fun setRow(row: Int, vector: Vec3dAccessor): Mat3dMutator

    /**
     * Sets the values of the first row to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the fist row.
     * @return This instance for method chaining.
     */
    fun setRow1(vector: Vec3dAccessor): Mat3dMutator

    /**
     * Sets the values of the second row to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the second row.
     * @return This instance for method chaining.
     */
    fun setRow2(vector: Vec3dAccessor): Mat3dMutator

    /**
     * Sets the values of the third row to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the third row.
     * @return This instance for method chaining.
     */
    fun setRow3(vector: Vec3dAccessor): Mat3dMutator

    /**
     * Sets the values of the specified [column] to the provided `(x y z)` vector.
     *
     * @param column The index of the column to set. Must be in range `[0..1]`.
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @param z Value for the third column.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given column index is not in range `[0..1]`.
     */
    fun setColumn(column: Int, x: Double, y: Double, z: Double): Mat3dMutator

    /**
     * Sets the values of the first column to the provided `(x y z)` vector.
     *
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @param z Value for the third column.
     * @return This instance for method chaining.
     */
    fun setColumn1(x: Double, y: Double, z: Double): Mat3dMutator

    /**
     * Sets the values of the second column to the provided `(x y z)` vector.
     *
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @param z Value for the third column.
     * @return This instance for method chaining.
     */
    fun setColumn2(x: Double, y: Double, z: Double): Mat3dMutator

    /**
     * Sets the values of the third column to the provided `(x y z)` vector.
     *
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @param z Value for the third column.
     * @return This instance for method chaining.
     */
    fun setColumn3(x: Double, y: Double, z: Double): Mat3dMutator

    /**
     * Sets the values of the specified [column] to the values provided by the [vector] instance.
     *
     * @param column The index of the column to set. Must be in range `[0..1]`.
     * @param vector The vector which holds the new values fot the specified column.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given column index is not in range `[0..1]`.
     */
    fun setColumn(column: Int, vector: Vec3dAccessor): Mat3dMutator

    /**
     * Sets the values of the first column to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the fist column.
     * @return This instance for method chaining.
     */
    fun setColumn1(vector: Vec3dAccessor): Mat3dMutator

    /**
     * Sets the values of the second column to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the second column.
     * @return This instance for method chaining.
     */
    fun setColumn2(vector: Vec3dAccessor): Mat3dMutator

    /**
     * Sets the values of the third column to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the third column.
     * @return This instance for method chaining.
     */
    fun setColumn3(vector: Vec3dAccessor): Mat3dMutator

    /**
     * Sets all components of this matrix to the given value.
     *
     * @param value The value to use for each component of this matrix.
     * @return This instance for method chaining.
     */
    fun set(value: Double): Mat3dMutator

    /**
     * Sets the matrix to the specified values.
     *
     *      ( m00, m01, m02 )
     *      ( m10, m11, m12 )
     *      ( m20, m21, m22 )
     *
     * @return This instance for method chaining.
     */
    operator fun set(
        m00: Double, m01: Double, m02: Double,
        m10: Double, m11: Double, m12: Double,
        m20: Double, m21: Double, m22: Double
    ): Mat3dMutator

    /**
     * Sets the values of this 3x3 matrix to the values of the given 3x3 matrix.
     *
     * @param other The matrix object to copy.
     * @return This instance for method chaining.
     */
    fun set(other: Mat3fAccessor): Mat3dMutator

    /**
     * Sets the values of this 3x3 matrix to the values of the given 3x3 matrix.
     *
     * @param other The matrix object to copy.
     * @return This instance for method chaining.
     */
    fun set(other: Mat3dAccessor): Mat3dMutator

    /**
     * Copies all values from the specified data array.
     *
     * @param matrixData 3x3 double array containing the matrix data to copy.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given matrix does not have a dimension of 3x3.
     */
    fun set(matrixData: Array<DoubleArray>): Mat3dMutator

    /**
     * Initializes the zero matrix of the form:
     *
     *      ( 0.0, 0.0, 0.0 )
     *      ( 0.0, 0.0, 0.0 )
     *      ( 0.0, 0.0, 0.0 )
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Mat3dMutator

    /**
     * Initializes the identity matrix of the form:
     *
     *      ( 1.0, 0.0, 0.0 )
     *      ( 0.0, 1.0, 0.0 )
     *      ( 0.0, 0.0, 1.0 )
     *
     * @return This instance for method chaining.
     */
    fun initIdentity(): Mat3dMutator

    /**
     * Initializes a matrix that performs a 2-dimensional rotation by [angleAroundX] degrees around the x-axis.
     *
     * @param angleAroundX The amount of rotation around the x-axis in degrees.
     * @return This instance for method chaining.
     */
    fun initRotationX(angleAroundX: Double): Mat3dMutator

    /**
     * Initializes a matrix that performs a 2-dimensional rotation by [angleAroundY] degrees around the y-axis.
     *
     * @param angleAroundY The amount of rotation around the y-axis in degrees.
     * @return This instance for method chaining.
     */
    fun initRotationY(angleAroundY: Double): Mat3dMutator

    /**
     * Initializes a matrix that performs a 2-dimensional rotation by [angleAroundZ] degrees around the z-axis.
     *
     * @param angleAroundZ The amount of rotation around the z-axis in degrees.
     * @return This instance for method chaining.
     */
    fun initRotationZ(angleAroundZ: Double): Mat3dMutator

    /**
     * Initializes a rotation matrix from the given rotation angles.
     *
     * @param angleAroundX The amount of rotation around the x-axis in degrees.
     * @param angleAroundY The amount of rotation around the y-axis in degrees.
     * @param angleAroundZ The amount of rotation around the z-axis in degrees.
     * @return This instance for method chaining.
     */
    fun initRotation(angleAroundX: Double, angleAroundY: Double, angleAroundZ: Double): Mat3dMutator

    /**
     * Initializes a rotation matrix from the given vector of angles.
     *
     * @param angles The vector instance whose values represent the rotation amount per axis. x: Rotation around the
     *     x-axis in degrees. y: Rotation around the y-axis in degrees. z: Rotation around the z-axis in degrees.
     * @return This instance for method chaining.
     */
    fun initRotation(angles: Vec3dAccessor): Mat3dMutator

    /**
     * Initializes a rotation matrix from a rotation axis and a rotation amount.
     *
     * @param axis The Vec3f object which represents the axis we will rotate around.
     * @param angle The amount of rotation around the specified [axis] in degrees.
     * @return This instance for method chaining.
     */
    fun initRotation(axis: Vec3dAccessor, angle: Double): Mat3dMutator

    /**
     * Adds the given double value componentwise to this matrix and returns this object afterwards.
     *
     * @param value The double value which gets added.
     * @return This instance for method chaining.
     */
    fun plus(value: Double): Mat3dMutator

    /**
     * Adds the given 3x3 matrix componentwise to this matrix and returns this object afterwards.
     *
     * @param other The matrix which values get added to this matrix.
     * @return This instance for method chaining.
     */
    fun plus(other: Mat3dAccessor): Mat3dMutator

    /**
     * Subtracts the given double value componentwise from this matrix and returns this object afterwards.
     *
     * @param value The double value which gets subtracted.
     * @return This instance for method chaining.
     */
    fun minus(value: Double): Mat3dMutator

    /**
     * Subtracts the given 3x3 matrix componentwise from this matrix and returns this object afterwards.
     *
     * @param other The matrix which values get subtracted from this matrix.
     * @return This instance for method chaining.
     */
    fun minus(other: Mat3dAccessor): Mat3dMutator

    /**
     * Multiplies this matrix with a double value and returns this object afterwards.
     *
     * @param value Double value which which every field of this matrix gets multiplied.
     * @return This instance for method chaining.
     */
    fun times(value: Double): Mat3dMutator

    /**
     * Multiplies this matrix with the given matrix and returns this object afterwards.
     *
     * @param other The 3x3 matrix with which this matrix gets multiplied.
     * @return This instance for method chaining.
     */
    fun times(other: Mat3dAccessor): Mat3dMutator

    /**
     * Multiplies this matrix with itself and returns this object afterwards.
     *
     * @return This instance for method chaining.
     */
    fun mulSelf(): Mat3dMutator

    /**
     * Calculates the inverse of this matrix and returns this object afterwards. May be expensive to compute! Only use
     * when necessary!
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the determinant of this matrix is zero.
     */
    fun inverse(): Mat3dMutator

    /**
     * Transposes this matrix around its major diagonal and returns this object afterwards. Rows to columns, columns to
     * rows...
     *
     *      ( 00 01 02 )      ( 00 10 20 )
     *      ( 10 11 12 )  ->  ( 01 11 21 )
     *      ( 20 21 22 )      ( 02 12 22 )
     *
     * @return This instance for method chaining.
     */
    fun transpose(): Mat3dMutator

    /**
     * Raises this matrix to the power of [exponent].
     * - M^-1 -> inverse();
     * - M^0 -> I (Identity matrix)
     * - M^1 -> M
     * - M^(n>1) -> n-1 multiplications
     * - I^n -> I
     *
     * @param exponent Exponent by which this matrix is raised.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given exponent is smaller than -1.
     */
    fun pow(exponent: Int): Mat3dMutator

    /**
     * Sets all components of this matrix to their absolute values.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Mat3dMutator

    /**
     * Makes the components more user / reader-friendly.
     *
     * @return This object for method chaining.
     */
    fun shorten(): Mat3dMutator

}
