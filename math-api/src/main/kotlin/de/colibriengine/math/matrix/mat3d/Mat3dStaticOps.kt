package de.colibriengine.math.matrix.mat3d

interface Mat3dStaticOps {

    fun determinantOf(
        m00: Double, m01: Double, m02: Double,
        m10: Double, m11: Double, m12: Double,
        m20: Double, m21: Double, m22: Double
    ): Double

}
