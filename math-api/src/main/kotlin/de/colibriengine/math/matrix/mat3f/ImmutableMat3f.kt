package de.colibriengine.math.matrix.mat3f

import de.colibriengine.math.vector.vec3f.Vec3fAccessor

/** Immutable version of the [Mat3f]. */
interface ImmutableMat3f : Mat3fAccessor {

    interface Builder {
        fun of(value: Float): ImmutableMat3f

        fun of(
            m00: Float, m01: Float, m02: Float,
            m10: Float, m11: Float, m12: Float,
            m20: Float, m21: Float, m22: Float
        ): ImmutableMat3f

        fun of(other: Mat3fAccessor): ImmutableMat3f

        fun ofColumns(
            column1: Vec3fAccessor,
            column2: Vec3fAccessor,
            column3: Vec3fAccessor
        ): ImmutableMat3f

        fun ofRows(
            row1: Vec3fAccessor,
            row2: Vec3fAccessor,
            row3: Vec3fAccessor
        ): ImmutableMat3f

        fun set00(m00: Float): Builder
        fun set01(m01: Float): Builder
        fun set02(m02: Float): Builder
        fun set10(m10: Float): Builder
        fun set11(m11: Float): Builder
        fun set12(m12: Float): Builder
        fun set20(m20: Float): Builder
        fun set21(m21: Float): Builder
        fun set22(m22: Float): Builder

        fun set(value: Float): Builder

        operator fun set(
            m00: Float, m01: Float, m02: Float,
            m10: Float, m11: Float, m12: Float,
            m20: Float, m21: Float, m22: Float
        ): Builder

        fun set(other: Mat3fAccessor): Builder

        fun setColumn1(column1: Vec3fAccessor): Builder
        fun setColumn2(column2: Vec3fAccessor): Builder
        fun setColumn3(column3: Vec3fAccessor): Builder

        fun setRow1(row1: Vec3fAccessor): Builder
        fun setRow2(row2: Vec3fAccessor): Builder
        fun setRow3(row3: Vec3fAccessor): Builder

        fun build(): ImmutableMat3f
    }

}
