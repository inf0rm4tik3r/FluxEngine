package de.colibriengine.math.matrix.mat3f

import de.colibriengine.math.Viewable
import de.colibriengine.math.matrix.mat3d.Mat3dAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

/** Provides the general functionality of a 3x3 matrix in float precision. */
interface Mat3f : Mat3fAccessor, Mat3fMutator, Viewable<Mat3fView> {

    override var m00: Float
    override var m01: Float
    override var m02: Float
    override var m10: Float
    override var m11: Float
    override var m12: Float
    override var m20: Float
    override var m21: Float
    override var m22: Float

    override fun setM00(value: Float): Mat3f
    override fun setM01(value: Float): Mat3f
    override fun setM02(value: Float): Mat3f
    override fun setM10(value: Float): Mat3f
    override fun setM11(value: Float): Mat3f
    override fun setM12(value: Float): Mat3f
    override fun setM20(value: Float): Mat3f
    override fun setM21(value: Float): Mat3f
    override fun setM22(value: Float): Mat3f

    override fun set(row: Int, column: Int, value: Float): Mat3f

    override fun setRow(row: Int, x: Float, y: Float, z: Float): Mat3f
    override fun setRow1(x: Float, y: Float, z: Float): Mat3f
    override fun setRow2(x: Float, y: Float, z: Float): Mat3f
    override fun setRow3(x: Float, y: Float, z: Float): Mat3f
    override fun setRow(row: Int, vector: Vec3fAccessor): Mat3f
    override fun setRow1(vector: Vec3fAccessor): Mat3f
    override fun setRow2(vector: Vec3fAccessor): Mat3f
    override fun setRow3(vector: Vec3fAccessor): Mat3f

    override fun setColumn(column: Int, x: Float, y: Float, z: Float): Mat3f
    override fun setColumn1(x: Float, y: Float, z: Float): Mat3f
    override fun setColumn2(x: Float, y: Float, z: Float): Mat3f
    override fun setColumn3(x: Float, y: Float, z: Float): Mat3f
    override fun setColumn(column: Int, vector: Vec3fAccessor): Mat3f
    override fun setColumn1(vector: Vec3fAccessor): Mat3f
    override fun setColumn2(vector: Vec3fAccessor): Mat3f
    override fun setColumn3(vector: Vec3fAccessor): Mat3f

    override fun set(value: Float): Mat3f
    override fun set(
        m00: Float, m01: Float, m02: Float,
        m10: Float, m11: Float, m12: Float,
        m20: Float, m21: Float, m22: Float
    ): Mat3f

    override fun set(other: Mat3fAccessor): Mat3f
    override fun set(other: Mat3dAccessor): Mat3f
    override fun set(matrixData: Array<FloatArray>): Mat3f

    override fun initZero(): Mat3f
    override fun initIdentity(): Mat3f
    override fun initRotationX(angleAroundX: Float): Mat3f
    override fun initRotationY(angleAroundY: Float): Mat3f
    override fun initRotationZ(angleAroundZ: Float): Mat3f
    override fun initRotation(angleAroundX: Float, angleAroundY: Float, angleAroundZ: Float): Mat3f
    override fun initRotation(angles: Vec3fAccessor): Mat3f
    override fun initRotation(axis: Vec3fAccessor, angle: Float): Mat3f

    override fun plus(value: Float): Mat3f
    override fun plus(other: Mat3fAccessor): Mat3f

    override fun minus(value: Float): Mat3f
    override fun minus(other: Mat3fAccessor): Mat3f

    override fun times(value: Float): Mat3f
    override fun mulSelf(): Mat3f
    override fun times(other: Mat3fAccessor): Mat3f

    override fun inverse(): Mat3f
    override fun transpose(): Mat3f
    override fun pow(exponent: Int): Mat3f
    override fun abs(): Mat3f
    override fun shorten(): Mat3f

    @Suppress("MemberVisibilityCanBePrivate", "unused")
    companion object {
        /** The number of float values used to represent this matrix. */
        const val FLOATS = 9

        /** The number of bytes used to represent this matrix object. 9 float values of 4 bytes each => 36 bytes. */
        const val BYTES = FLOATS * java.lang.Float.BYTES

        /** The amount of rows this matrix contains. */
        const val ROW_AMT = 3

        /** The amount of columns this matrix contains. */
        const val COLUMN_AMT = 3

        /** Index of the first row or column. */
        const val FIRST = 0

        /** Index of the second row or column. */
        const val SECOND = 1

        /** Index of the third row or column. */
        const val THIRD = 2
    }

}
