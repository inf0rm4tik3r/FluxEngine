package de.colibriengine.math.matrix.mat3f

/** Every [Mat3f] factory implementation should expose these methods. */
interface Mat3fFactory {

    fun acquire(): Mat3f

    fun acquireDirty(): Mat3f

    fun free(mat3f: Mat3f)

    fun immutableMat3fBuilder(): ImmutableMat3f.Builder

    fun zero(): ImmutableMat3f

    fun one(): ImmutableMat3f

    fun identity(): ImmutableMat3f

}
