package de.colibriengine.math.matrix.mat3f

interface Mat3fStaticOps {

    fun determinantOf(
        m00: Float, m01: Float, m02: Float,
        m10: Float, m11: Float, m12: Float,
        m20: Float, m21: Float, m22: Float
    ): Float

}
