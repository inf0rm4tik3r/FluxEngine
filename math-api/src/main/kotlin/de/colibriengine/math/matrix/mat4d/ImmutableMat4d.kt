package de.colibriengine.math.matrix.mat4d

import de.colibriengine.math.vector.vec4d.Vec4dAccessor

/** Immutable version of the [Mat4d]. */
interface ImmutableMat4d : Mat4dAccessor {

    interface Builder {
        fun of(value: Double): ImmutableMat4d

        fun of(
            m00: Double, m01: Double, m02: Double, m03: Double,
            m10: Double, m11: Double, m12: Double, m13: Double,
            m20: Double, m21: Double, m22: Double, m23: Double,
            m30: Double, m31: Double, m32: Double, m33: Double
        ): ImmutableMat4d

        fun of(other: Mat4dAccessor): ImmutableMat4d

        fun ofColumns(
            column1: Vec4dAccessor,
            column2: Vec4dAccessor,
            column3: Vec4dAccessor,
            column4: Vec4dAccessor
        ): ImmutableMat4d

        fun ofRows(
            row1: Vec4dAccessor,
            row2: Vec4dAccessor,
            row3: Vec4dAccessor,
            row4: Vec4dAccessor
        ): ImmutableMat4d

        fun set00(m00: Double): Builder
        fun set01(m01: Double): Builder
        fun set02(m02: Double): Builder
        fun set03(m03: Double): Builder
        fun set10(m10: Double): Builder
        fun set11(m11: Double): Builder
        fun set12(m12: Double): Builder
        fun set13(m13: Double): Builder
        fun set20(m20: Double): Builder
        fun set21(m21: Double): Builder
        fun set22(m22: Double): Builder
        fun set23(m23: Double): Builder
        fun set30(m30: Double): Builder
        fun set31(m31: Double): Builder
        fun set32(m32: Double): Builder
        fun set33(m33: Double): Builder

        fun set(value: Double): Builder

        operator fun set(
            m00: Double, m01: Double, m02: Double, m03: Double,
            m10: Double, m11: Double, m12: Double, m13: Double,
            m20: Double, m21: Double, m22: Double, m23: Double,
            m30: Double, m31: Double, m32: Double, m33: Double
        ): Builder

        fun set(other: Mat4dAccessor): Builder

        fun setColumn1(column1: Vec4dAccessor): Builder
        fun setColumn2(column2: Vec4dAccessor): Builder
        fun setColumn3(column3: Vec4dAccessor): Builder
        fun setColumn4(column4: Vec4dAccessor): Builder

        fun setRow1(row1: Vec4dAccessor): Builder
        fun setRow2(row2: Vec4dAccessor): Builder
        fun setRow3(row3: Vec4dAccessor): Builder
        fun setRow4(row4: Vec4dAccessor): Builder

        fun build(): ImmutableMat4d
    }

}
