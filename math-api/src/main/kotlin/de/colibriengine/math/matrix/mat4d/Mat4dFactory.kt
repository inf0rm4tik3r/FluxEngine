package de.colibriengine.math.matrix.mat4d

/** Every [Mat4d] factory implementation should expose these methods. */
interface Mat4dFactory {

    fun acquire(): Mat4d

    fun acquireDirty(): Mat4d

    fun free(mat4d: Mat4d)

    fun immutableMat4dBuilder(): ImmutableMat4d.Builder

    fun zero(): ImmutableMat4d

    fun one(): ImmutableMat4d

    fun identity(): ImmutableMat4d

}
