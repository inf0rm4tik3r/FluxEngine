package de.colibriengine.math.matrix.mat4d

import de.colibriengine.math.matrix.mat3d.Mat3dAccessor
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec4d.Vec4dAccessor

/** Defines all mutating methods of the 4x4 double matrix. */
interface Mat4dMutator {

    /** First row, first column. */
    var m00: Double

    /** First row, second column. */
    var m01: Double

    /** First row, third column. */
    var m02: Double

    /** First row, fourth column. */
    var m03: Double

    /** Second row, first column. */
    var m10: Double

    /** Second row, second column. */
    var m11: Double

    /** Second row, third column. */
    var m12: Double

    /** Second row, fourth column. */
    var m13: Double

    /** Third row, first column. */
    var m20: Double

    /** Third row, second column. */
    var m21: Double

    /** Third row, third column. */
    var m22: Double

    /** Third row, fourth column. */
    var m23: Double

    /** Fourth row, first column. */
    var m30: Double

    /** Fourth row, second column. */
    var m31: Double

    /** Fourth row, third column. */
    var m32: Double

    /** Fourth row, fourth column. */
    var m33: Double

    /**
     * Sets the value in the first row and first column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM00(value: Double): Mat4dMutator

    /**
     * Sets the value in the first row and second column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM01(value: Double): Mat4dMutator

    /**
     * Sets the value in the first row and third column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM02(value: Double): Mat4dMutator

    /**
     * Sets the value in the first row and fourth column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM03(value: Double): Mat4dMutator

    /**
     * Sets the value in the second row and first column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM10(value: Double): Mat4dMutator

    /**
     * Sets the value in the second row and second column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM11(value: Double): Mat4dMutator

    /**
     * Sets the value in the second row and third column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM12(value: Double): Mat4dMutator

    /**
     * Sets the value in the second row and fourth column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM13(value: Double): Mat4dMutator

    /**
     * Sets the value in the third row and first column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM20(value: Double): Mat4dMutator

    /**
     * Sets the value in the third row and second column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM21(value: Double): Mat4dMutator

    /**
     * Sets the value in the third row and third column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM22(value: Double): Mat4dMutator

    /**
     * Sets the value in the third row and fourth column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM23(value: Double): Mat4dMutator

    /**
     * Sets the value in the fourth row and first column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM30(value: Double): Mat4dMutator

    /**
     * Sets the value in the fourth row and second column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM31(value: Double): Mat4dMutator

    /**
     * Sets the value in the fourth row and third column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM32(value: Double): Mat4dMutator

    /**
     * Sets the value in the fourth row and fourth column.
     *
     * @param value The new value of the cell.
     * @return This instance for method chaining.
     */
    fun setM33(value: Double): Mat4dMutator

    /**
     * Sets the value at the cell defined by the intersection of [row] and [column] in the graphical layout of this 4x4
     * matrix.
     *
     * @param row The row to look in.
     * @param column The column to look in.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the row or the column index are not in the `[0..1]` range.
     */
    operator fun set(row: Int, column: Int, value: Double): Mat4dMutator

    /**
     * Sets the values of the specified [row] to the provided `(x y z w)` vector.
     *
     * @param row The index of the row to set. Must be in range `[0..1]`.
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given row index is not in range `[0..1]`.
     */
    fun setRow(row: Int, x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the specified [row] to the values provided by the [vector] instance.
     *
     * @param row The index of the row to set. Must be in range `[0..1]`.
     * @param vector The vector which holds the new values fot the specified row.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given row index is not in range `[0..1]`.
     */
    fun setRow(row: Int, vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets the values of the first row to the provided `(x y z w)` vector.
     *
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     */
    fun setRow1(x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the second row to the provided `(x y z w)` vector.
     *
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     */
    fun setRow2(x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the third row to the provided `(x y z w)` vector.
     *
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     */
    fun setRow3(x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the fourth row to the provided `(x y z w)` vector.
     *
     * @param x Value for the first column.
     * @param y Value for the second column.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     */
    fun setRow4(x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the first row to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the fist row.
     * @return This instance for method chaining.
     */
    fun setRow1(vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets the values of the second row to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the second row.
     * @return This instance for method chaining.
     */
    fun setRow2(vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets the values of the third row to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the third row.
     * @return This instance for method chaining.
     */
    fun setRow3(vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets the values of the fourth row to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the fourth row.
     * @return This instance for method chaining.
     */
    fun setRow4(vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets the values of the specified [column] to the provided `(x y z w)` vector.
     *
     * @param column The index of the column to set. Must be in range `[0..1]`.
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given column index is not in range `[0..1]`.
     */
    fun setColumn(column: Int, x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the specified [column] to the values provided by the [vector] instance.
     *
     * @param column The index of the column to set. Must be in range `[0..1]`.
     * @param vector The vector which holds the new values fot the specified column.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given column index is not in range `[0..1]`.
     */
    fun setColumn(column: Int, vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets the values of the first column to the provided `(x y z w)` vector.
     *
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     */
    fun setColumn1(x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the second column to the provided `(x y z w)` vector.
     *
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     */
    fun setColumn2(x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the third column to the provided `(x y z w)` vector.
     *
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     */
    fun setColumn3(x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the fourth column to the provided `(x y z w)` vector.
     *
     * @param x Value for the first row.
     * @param y Value for the second row.
     * @param z Value for the third column.
     * @param w Value for the fourth column.
     * @return This instance for method chaining.
     */
    fun setColumn4(x: Double, y: Double, z: Double, w: Double): Mat4dMutator

    /**
     * Sets the values of the first column to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the fist column.
     * @return This instance for method chaining.
     */
    fun setColumn1(vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets the values of the second column to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the second column.
     * @return This instance for method chaining.
     */
    fun setColumn2(vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets the values of the third column to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the third column.
     * @return This instance for method chaining.
     */
    fun setColumn3(vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets the values of the fourth column to the values provided by the [vector] instance.
     *
     * @param vector The vector which holds the new values fot the fourth column.
     * @return This instance for method chaining.
     */
    fun setColumn4(vector: Vec4dAccessor): Mat4dMutator

    /**
     * Sets all components of this matrix to the given value.
     *
     * @param value The value to use for each component of this matrix.
     * @return This instance for method chaining.
     */
    fun set(value: Double): Mat4dMutator

    /**
     * Sets the matrix to the specified values.
     *
     *      ( m00, m01, m02, m03 )
     *      ( m10, m11, m12, m13 )
     *      ( m20, m21, m22, m23 )
     *      ( m30, m31, m32, m33 )
     *
     * @param m00 First row.
     * @param m01
     * @param m02
     * @param m03
     * @param m10 Second row.
     * @param m11
     * @param m12
     * @param m13
     * @param m20 Third row.
     * @param m21
     * @param m22
     * @param m23
     * @param m30 Fourth row.
     * @param m31
     * @param m32
     * @param m33
     * @return This instance for method chaining.
     */
    operator fun set(
        m00: Double, m01: Double, m02: Double, m03: Double,
        m10: Double, m11: Double, m12: Double, m13: Double,
        m20: Double, m21: Double, m22: Double, m23: Double,
        m30: Double, m31: Double, m32: Double, m33: Double
    ): Mat4dMutator

    /**
     * Sets the values of this 4x4 matrix to the values of the given 4x4 matrix.
     *
     * @param other The matrix object to copy.
     * @return This instance for method chaining.
     */
    fun set(other: Mat4fAccessor): Mat4dMutator

    /**
     * Sets the values of this 4x4 matrix to the values of the given 4x4 matrix.
     *
     * @param other The matrix object to copy.
     * @return This instance for method chaining.
     */
    fun set(other: Mat4dAccessor): Mat4dMutator

    /**
     * Sets the values of this 4x4 matrix to the values of the given 3x3 matrix by adding a fourth row and column like
     * this:
     *
     *      ( m00, m01, m02, 0.0 )
     *      ( m10, m11, m12, 0.0 )
     *      ( m20, m21, m22, 0.0 )
     *      ( 0.0, 0.0, 0.0, 1.0 )
     *
     * @param other The 3x3 matrix to copy in the upper left corner.
     * @return This instance for method chaining.
     */
    fun set(other: Mat3dAccessor): Mat4dMutator

    /**
     * Sets the values of this 4x4 matrix to the values of the given 3x3 matrix by adding a fourth row and column filled
     * with the specified value.
     *
     *      ( m00,      m01,      m02,      fillWith )
     *      ( m10,      m11,      m12,      fillWith )
     *      ( m20,      m21,      m22,      fillWith )
     *      ( fillWith, fillWith, fillWith, fillWith )
     *
     * @param other The 3x3 matrix to copy in the upper left corner.
     * @param fillWith Value to fill the new cells with.
     * @return This instance for method chaining.
     */
    operator fun set(other: Mat3dAccessor, fillWith: Double): Mat4dMutator

    /**
     * Sets the values of this 4x4 matrix to the values of the given 3x3 matrix by adding a fourth row and column filled
     * with the specified values.
     *
     *      ( m00,        m01,        m02,        fillWith03 )
     *      ( m10,        m11,        m12,        fillWith13 )
     *      ( m20,        m21,        m22,        fillWith23 )
     *      ( fillWith30, fillWith31, fillWith32, fillWith33 )
     *
     * @param other The 3x3 matrix to copy in the upper left corner.
     * @param fillWith03 First row, fourth column.
     * @param fillWith13 Second row, fourth column.
     * @param fillWith23 Third row, fourth column.
     * @param fillWith30 Fourth row, first column.
     * @param fillWith31 Fourth row, second column.
     * @param fillWith32 Fourth row, third column.
     * @param fillWith33 Fourth row, fourth column.
     * @return This instance for method chaining.
     */
    operator fun set(
        other: Mat3dAccessor,
        fillWith03: Double, fillWith13: Double, fillWith23: Double,
        fillWith30: Double, fillWith31: Double, fillWith32: Double,
        fillWith33: Double
    ): Mat4dMutator

    /**
     * Copies all values from the specified data array.
     *
     * @param matrixData 4x4 double array containing the matrix data to copy.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given matrix does not have a dimension of 4x4.
     */
    fun set(matrixData: Array<DoubleArray>): Mat4dMutator

    /**
     * Initializes the zero matrix of the form:
     *
     *      ( 0.0, 0.0, 0.0, 0.0 )
     *      ( 0.0, 0.0, 0.0, 0.0 )
     *      ( 0.0, 0.0, 0.0, 0.0 )
     *      ( 0.0, 0.0, 0.0, 0.0 )
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Mat4dMutator

    /**
     * Initializes the identity matrix of the form:
     *
     *      ( 1.0, 0.0, 0.0, 0.0 )
     *      ( 0.0, 1.0, 0.0, 0.0 )
     *      ( 0.0, 0.0, 1.0, 0.0 )
     *      ( 0.0, 0.0, 0.0, 1.0 )
     *
     * @return This instance for method chaining.
     */
    fun initIdentity(): Mat4dMutator

    /**
     * Initializes the bias matrix of the form:
     *
     *      ( 0.5, 0.0, 0.0, 0.5 )
     *      ( 0.0, 0.5, 0.0, 0.5 )
     *      ( 0.0, 0.0, 0.5, 0.5 )
     *      ( 0.0, 0.0, 0.0, 1.0 )
     *
     * @return This instance for method chaining.
     */
    fun initBias(): Mat4dMutator

    /**
     * Initializes a translation matrix from three double values.
     *
     *      ( 1.0, 0.0, 0.0,   x )
     *      ( 0.0, 1.0, 0.0,   y )
     *      ( 0.0, 0.0, 1.0,   z )
     *      ( 0.0, 0.0, 0.0, 1.0 )
     *
     * @param x Translation on the x-axis.
     * @param y Translation on the y-axis.
     * @param z Translation on the z-axis.
     * @return This instance for method chaining.
     */
    fun initTranslation(x: Double, y: Double, z: Double): Mat4dMutator

    /**
     * Initializes a translation matrix from a vector.
     *
     *      ( 1.0, 0.0, 0.0, translationView.x() )
     *      ( 0.0, 1.0, 0.0, translationView.y() )
     *      ( 0.0, 0.0, 1.0, translationView.z() )
     *      ( 0.0, 0.0, 0.0,                 1.0 )
     *
     * @param translation The 3-dimensional vector whose values represent the translation per axis.
     * @return This instance for method chaining.
     */
    fun initTranslation(translation: Vec3dAccessor): Mat4dMutator

    /**
     * Initializes a rotation matrix from three double values.
     *
     * @param angleAroundX The rotation amount around the x-axis.
     * @param angleAroundY The rotation amount around the y-axis.
     * @param angleAroundZ The rotation amount around the z-axis.
     * @return This instance for method chaining.
     */
    fun initRotation(angleAroundX: Double, angleAroundY: Double, angleAroundZ: Double): Mat4dMutator

    /**
     * Initializes a rotation matrix from a vector of angles.
     *
     * @param angles The 3-dimensional vector whose values represent the rotation amount per axis. x: Rotation around
     *     the x-axis. y: Rotation around the y-axis. z: Rotation around the z-axis.
     * @return This instance for method chaining.
     */
    fun initRotation(angles: Vec3dAccessor): Mat4dMutator

    /**
     * Initializes a rotation matrix from a rotation axis and a rotation amount.
     *
     * @param axis The 3-dimensional vector which represents the axis we will rotate around.
     * @param angle The amount of rotation around the specified [axis] in degrees.
     * @return This instance for method chaining.
     */
    fun initRotation(axis: Vec3dAccessor, angle: Double): Mat4dMutator

    /**
     * Initializes a rotation matrix from a forward, up and right vector. They must be perpendicular to each other! All
     * vectors must be normalized!
     *
     * @param forward The normalized forward directed vector.
     * @param up The normalized top directed vector.
     * @param right The normalized right directed vector.
     * @return This instance for method chaining.
     */
    fun initRotation(
        forward: Vec3dAccessor,
        up: Vec3dAccessor,
        right: Vec3dAccessor
    ): Mat4dMutator

    /**
     * Initializes a rotation matrix from a forward and up vector. Calculates the missing right vector with the cross
     * product. All vectors must be normalized.
     *
     * @param forward The normalized forward directed vector.
     * @param up The normalized top directed vector.
     * @return This instance for method chaining.
     */
    fun initRotation(
        forward: Vec3dAccessor,
        up: Vec3dAccessor
    ): Mat4dMutator

    /**
     * Initializes a rotation matrix from a quaternion.
     *
     * @param rotation The quaternion which holds the rotation information.
     * @return This instance for method chaining.
     */
    fun initRotation(rotation: QuaternionDAccessor): Mat4dMutator

    /**
     * Initializes a scaling matrix from three double values.
     *
     * @param scaleX Scale on the x-axis.
     * @param scaleY Scale on the y-axis.
     * @param scaleZ Scale on the z-axis.
     * @return This instance for method chaining.
     */
    fun initScale(scaleX: Double, scaleY: Double, scaleZ: Double): Mat4dMutator

    /**
     * Initializes a scaling matrix from one double value.
     *
     * @param scale Scale on the x, y and z axis.
     * @return This instance for method chaining.
     */
    fun initScale(scale: Double): Mat4dMutator

    /**
     * Initializes a scaling matrix from a vector.
     *
     * @param scaleView The 3-dimensional vector whose values represent the scale on each axis.
     * @return This instance for method chaining.
     */
    fun initScale(scaleView: Vec3dAccessor): Mat4dMutator

    /**
     * Initializes a frustum projection matrix. Define left, right, bottom and top parameters in world coordinates.
     *
     * @param left Left clipping plane.
     * @param right Right clipping plane.
     * @param bottom Bottom clipping plane.
     * @param top Top clipping plane.
     * @param zNear Distance to the near clipping plane.
     * @param zFar Distance to the far clipping plane.
     * @return This instance for method chaining.
     */
    fun initFrustumProjection(
        left: Double, right: Double, bottom: Double, top: Double,
        zNear: Double, zFar: Double
    ): Mat4dMutator

    /**
     * Initializes a left handed (Direct3D style) perspective projection matrix.
     *
     * @param fov Field-of-view in degrees.
     * @param aspectRatio The width of the window divided by the height of the window.
     * @param zNear Distance to the near clipping plane.
     * @param zFar Distance to the far clipping plane.
     * @return This instance for method chaining.
     */
    fun initPerspectiveProjectionLH(
        fov: Double, aspectRatio: Double,
        zNear: Double, zFar: Double
    ): Mat4dMutator

    /**
     * Initializes a right handed (OpenGL style) perspective projection matrix.
     *
     * @param fov Field-of-view in degrees.
     * @param aspectRatio The width of the window divN by the height of the window.
     * @param zNear Distance to the near clipping plane.
     * @param zFar Distance to the far clipping plane.
     * @return This instance for method chaining.
     */
    fun initPerspectiveProjectionRH(
        fov: Double, aspectRatio: Double,
        zNear: Double, zFar: Double
    ): Mat4dMutator

    /**
     * Initializes a left handed (Direct3D style) orthographic projection matrix.
     *
     * @param left X-Position of the window.
     * @param right Width of the window.
     * @param bottom Y-Position of the window.
     * @param top Height of the window.
     * @param zNear Distance to the near clipping plane.
     * @param zFar Distance to the far clipping plane.
     * @return This instance for method chaining.
     */
    fun initOrthographicProjectionLH(
        left: Double, right: Double, bottom: Double, top: Double,
        zNear: Double, zFar: Double
    ): Mat4dMutator

    /**
     * Initializes a right handed (OpenGL style) orthographic projection matrix.
     *
     * @param left X-Position of the window.
     * @param right Width of the window.
     * @param bottom Y-Position of the window.
     * @param top Height of the window.
     * @param zNear Distance to the near clipping plane.
     * @param zFar Distance to the far clipping plane.
     * @return This instance for method chaining.
     */
    fun initOrthographicProjectionRH(
        left: Double, right: Double, bottom: Double, top: Double,
        zNear: Double, zFar: Double
    ): Mat4dMutator

    /**
     * Initializes a right handed (OpenGL style) lookat matrix.
     *
     * @param source The current position of the camera.
     * @param targetUp The current up vector of the camera.
     * @param pointOfInterest The point you want to look at.
     * @return This instance for method chaining.
     */
    fun initLookAtRH(
        source: Vec3dAccessor,
        targetUp: Vec3dAccessor,
        pointOfInterest: Vec3dAccessor
    ): Mat4dMutator

    /**
     * Adds the given double value componentwise to this matrix and returns this object afterwards.
     *
     * @param value The double value which gets added.
     * @return This instance for method chaining.
     */
    fun plus(value: Double): Mat4dMutator

    /**
     * Adds the given 4x4 matrix componentwise to this matrix and returns this object afterwards.
     *
     * @param other The matrix whose values get added to this matrix.
     * @return This instance for method chaining.
     */
    fun plus(other: Mat4dAccessor): Mat4dMutator

    /**
     * Subtracts the given double value componentwise from this matrix and returns this object afterwards.
     *
     * @param value The double value which gets subtracted.
     * @return This instance for method chaining.
     */
    fun minus(value: Double): Mat4dMutator

    /**
     * Subtracts the given 4x4 matrix componentwise from this matrix and returns this object afterwards.
     *
     * @param other The matrix whose values get subtracted from this matrix.
     * @return This instance for method chaining.
     */
    fun minus(other: Mat4dAccessor): Mat4dMutator

    /**
     * Multiplies this matrix with a double value and returns this object afterwards.
     *
     * @param value Double value with which every field of this matrix gets multiplied.
     * @return This instance for method chaining.
     */
    fun times(value: Double): Mat4dMutator

    /**
     * Multiplies this matrix with the given matrix and returns this object afterwards.
     *
     * @param other The 4x4 matrix with which this matrix gets multiplied.
     * @return This instance for method chaining.
     */
    fun times(other: Mat4dAccessor): Mat4dMutator

    /**
     * Multiplies this matrix with itself and return this object afterwards.
     *
     * @return This instance for method chaining.
     */
    fun mulSelf(): Mat4dMutator

    /**
     * Calculates the inverse of this matrix using Cramer's Rule and returns this object afterwards. Heavy function!
     * Only use when necessary!
     *
     * @return This instance for method chaining.
     */
    fun inverse(): Mat4dMutator

    /**
     * Transposes this matrix around its major diagonal and returns this object afterwards. Rows to columns, columns to
     * rows...
     *
     *      ( 00 01 02 03 )      ( 00 10 20 30 )
     *      ( 10 11 12 13 )  ->  ( 01 11 21 31 )
     *      ( 20 21 22 23 )      ( 02 12 22 32 )
     *      ( 30 31 32 33 )      ( 03 13 23 33 )
     *
     * @return This instance for method chaining.
     */
    fun transpose(): Mat4dMutator

    /**
     * Raises this matrix to the power of [exponent].
     * - M^-1 -> inverse();
     * - M^0 -> I (Identity matrix)
     * - M^1 -> M
     * - M^(n>1) -> n-1 multiplications
     * - I^n -> I
     *
     * @param exponent Exponent by which this matrix is raised.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given exponent is smaller than -1.
     */
    fun pow(exponent: Int): Mat4dMutator

    /**
     * Sets all components of this matrix to their absolute values.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Mat4dMutator

    /**
     * Makes the components more user / reader-friendly.
     *
     * @return This object for method chaining.
     */
    fun shorten(): Mat4dMutator

}
