package de.colibriengine.math.matrix.mat4f

import de.colibriengine.math.vector.vec4f.Vec4fAccessor

/** Immutable version of the [Mat4f]. */
interface ImmutableMat4f : Mat4fAccessor {

    interface Builder {
        fun of(value: Float): ImmutableMat4f

        fun of(
            m00: Float, m01: Float, m02: Float, m03: Float,
            m10: Float, m11: Float, m12: Float, m13: Float,
            m20: Float, m21: Float, m22: Float, m23: Float,
            m30: Float, m31: Float, m32: Float, m33: Float
        ): ImmutableMat4f

        fun of(other: Mat4fAccessor): ImmutableMat4f

        fun ofColumns(
            column1: Vec4fAccessor,
            column2: Vec4fAccessor,
            column3: Vec4fAccessor,
            column4: Vec4fAccessor
        ): ImmutableMat4f

        fun ofRows(
            row1: Vec4fAccessor,
            row2: Vec4fAccessor,
            row3: Vec4fAccessor,
            row4: Vec4fAccessor
        ): ImmutableMat4f

        fun set00(m00: Float): Builder
        fun set01(m01: Float): Builder
        fun set02(m02: Float): Builder
        fun set03(m03: Float): Builder
        fun set10(m10: Float): Builder
        fun set11(m11: Float): Builder
        fun set12(m12: Float): Builder
        fun set13(m13: Float): Builder
        fun set20(m20: Float): Builder
        fun set21(m21: Float): Builder
        fun set22(m22: Float): Builder
        fun set23(m23: Float): Builder
        fun set30(m30: Float): Builder
        fun set31(m31: Float): Builder
        fun set32(m32: Float): Builder
        fun set33(m33: Float): Builder

        fun set(value: Float): Builder

        operator fun set(
            m00: Float, m01: Float, m02: Float, m03: Float,
            m10: Float, m11: Float, m12: Float, m13: Float,
            m20: Float, m21: Float, m22: Float, m23: Float,
            m30: Float, m31: Float, m32: Float, m33: Float
        ): Builder

        fun set(other: Mat4fAccessor): Builder

        fun setColumn1(column1: Vec4fAccessor): Builder
        fun setColumn2(column2: Vec4fAccessor): Builder
        fun setColumn3(column3: Vec4fAccessor): Builder
        fun setColumn4(column4: Vec4fAccessor): Builder

        fun setRow1(row1: Vec4fAccessor): Builder
        fun setRow2(row2: Vec4fAccessor): Builder
        fun setRow3(row3: Vec4fAccessor): Builder
        fun setRow4(row4: Vec4fAccessor): Builder

        fun build(): ImmutableMat4f
    }

}
