package de.colibriengine.math.matrix.mat4f

import de.colibriengine.math.Viewable
import de.colibriengine.math.matrix.mat3f.Mat3fAccessor
import de.colibriengine.math.matrix.mat4d.Mat4dAccessor
import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor

/** Provides the general functionality of a 4x4 matrix in float precision. */
interface Mat4f : Mat4fAccessor, Mat4fMutator, Viewable<Mat4fView> {

    override var m00: Float
    override var m01: Float
    override var m02: Float
    override var m03: Float
    override var m10: Float
    override var m11: Float
    override var m12: Float
    override var m13: Float
    override var m20: Float
    override var m21: Float
    override var m22: Float
    override var m23: Float
    override var m30: Float
    override var m31: Float
    override var m32: Float
    override var m33: Float

    override fun setM00(value: Float): Mat4f
    override fun setM01(value: Float): Mat4f
    override fun setM02(value: Float): Mat4f
    override fun setM03(value: Float): Mat4f
    override fun setM10(value: Float): Mat4f
    override fun setM11(value: Float): Mat4f
    override fun setM12(value: Float): Mat4f
    override fun setM13(value: Float): Mat4f
    override fun setM20(value: Float): Mat4f
    override fun setM21(value: Float): Mat4f
    override fun setM22(value: Float): Mat4f
    override fun setM23(value: Float): Mat4f
    override fun setM30(value: Float): Mat4f
    override fun setM31(value: Float): Mat4f
    override fun setM32(value: Float): Mat4f
    override fun setM33(value: Float): Mat4f

    override fun set(row: Int, column: Int, value: Float): Mat4f

    override fun setRow(row: Int, x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setRow1(x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setRow2(x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setRow3(x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setRow4(x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setRow(row: Int, vector: Vec4fAccessor): Mat4f
    override fun setRow1(vector: Vec4fAccessor): Mat4f
    override fun setRow2(vector: Vec4fAccessor): Mat4f
    override fun setRow3(vector: Vec4fAccessor): Mat4f
    override fun setRow4(vector: Vec4fAccessor): Mat4f

    override fun setColumn(column: Int, x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setColumn1(x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setColumn2(x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setColumn3(x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setColumn4(x: Float, y: Float, z: Float, w: Float): Mat4f
    override fun setColumn(column: Int, vector: Vec4fAccessor): Mat4f
    override fun setColumn1(vector: Vec4fAccessor): Mat4f
    override fun setColumn2(vector: Vec4fAccessor): Mat4f
    override fun setColumn3(vector: Vec4fAccessor): Mat4f
    override fun setColumn4(vector: Vec4fAccessor): Mat4f

    override fun set(value: Float): Mat4f
    override fun set(
        m00: Float, m01: Float, m02: Float, m03: Float,
        m10: Float, m11: Float, m12: Float, m13: Float,
        m20: Float, m21: Float, m22: Float, m23: Float,
        m30: Float, m31: Float, m32: Float, m33: Float
    ): Mat4f

    override fun set(other: Mat4fAccessor): Mat4f
    override fun set(other: Mat4dAccessor): Mat4f
    override fun set(other: Mat3fAccessor): Mat4f
    override fun set(other: Mat3fAccessor, fillWith: Float): Mat4f
    override fun set(
        other: Mat3fAccessor,
        fillWith03: Float, fillWith13: Float, fillWith23: Float,
        fillWith30: Float, fillWith31: Float, fillWith32: Float,
        fillWith33: Float
    ): Mat4f

    override fun set(matrixData: Array<FloatArray>): Mat4f

    override fun initZero(): Mat4f
    override fun initIdentity(): Mat4f
    override fun initBias(): Mat4f
    override fun initTranslation(x: Float, y: Float, z: Float): Mat4f
    override fun initTranslation(translation: Vec3fAccessor): Mat4f
    override fun initRotation(angleAroundX: Float, angleAroundY: Float, angleAroundZ: Float): Mat4f
    override fun initRotation(angles: Vec3fAccessor): Mat4f
    override fun initRotation(axis: Vec3fAccessor, angle: Float): Mat4f
    override fun initRotation(forward: Vec3fAccessor, up: Vec3fAccessor, right: Vec3fAccessor): Mat4f
    override fun initRotation(forward: Vec3fAccessor, up: Vec3fAccessor): Mat4f
    override fun initRotation(rotation: QuaternionFAccessor): Mat4f
    override fun initScale(scaleX: Float, scaleY: Float, scaleZ: Float): Mat4f
    override fun initScale(scale: Float): Mat4f
    override fun initScale(scaleView: Vec3fAccessor): Mat4f

    override fun initFrustumProjection(
        left: Float, right: Float, bottom: Float, top: Float,
        zNear: Float, zFar: Float
    ): Mat4f

    override fun initPerspectiveProjectionLH(
        fov: Float, aspectRatio: Float,
        zNear: Float, zFar: Float
    ): Mat4f

    override fun initPerspectiveProjectionRH(
        fov: Float, aspectRatio: Float,
        zNear: Float, zFar: Float
    ): Mat4f

    override fun initOrthographicProjectionLH(
        left: Float, right: Float, bottom: Float, top: Float,
        zNear: Float, zFar: Float
    ): Mat4f

    override fun initOrthographicProjectionRH(
        left: Float, right: Float, bottom: Float, top: Float,
        zNear: Float, zFar: Float
    ): Mat4f

    override fun initLookAtRH(
        source: Vec3fAccessor,
        targetUp: Vec3fAccessor,
        pointOfInterest: Vec3fAccessor
    ): Mat4f

    override fun plus(value: Float): Mat4f
    override fun plus(other: Mat4fAccessor): Mat4f

    override fun minus(value: Float): Mat4f
    override fun minus(other: Mat4fAccessor): Mat4f

    override fun times(value: Float): Mat4f
    override fun times(other: Mat4fAccessor): Mat4f
    override fun mulSelf(): Mat4f

    override fun inverse(): Mat4f

    override fun transpose(): Mat4f

    override fun pow(exponent: Int): Mat4f

    override fun shorten(): Mat4f

    @Suppress("MemberVisibilityCanBePrivate", "unused")
    companion object {
        /** The number of float values used to represent this matrix. */
        const val FLOATS = 16

        /** The number of bytes used to represent this matrix object. 16 float values of 4 bytes each => 64 bytes. */
        const val BYTES = FLOATS * java.lang.Float.BYTES

        /** The amount of rows this matrix contains. */
        const val ROW_AMT = 4

        /** The amount of columns this matrix contains. */
        const val COLUMN_AMT = 4

        /** Index of the first row. */
        const val FIRST_ROW = 0

        /** Index of the second row. */
        const val SECOND_ROW = 1

        /** Index of the third row. */
        const val THIRD_ROW = 2

        /** Index of the fourth row. */
        const val FOURTH_ROW = 3

        /** Index of the first column. */
        const val FIRST_COLUMN = 0

        /** Index of the second column. */
        const val SECOND_COLUMN = 1

        /** Index of the third column. */
        const val THIRD_COLUMN = 2

        /** Index of the fourth column. */
        const val FOURTH_COLUMN = 3
    }

}
