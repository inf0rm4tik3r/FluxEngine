package de.colibriengine.math.matrix.mat4f

/** Every [Mat4f] factory implementation should expose these methods. */
interface Mat4fFactory {

    fun acquire(): Mat4f

    fun acquireDirty(): Mat4f

    fun free(mat4f: Mat4f)

    fun immutableMat4fBuilder(): ImmutableMat4f.Builder

    fun zero(): ImmutableMat4f

    fun one(): ImmutableMat4f

    fun identity(): ImmutableMat4f

}
