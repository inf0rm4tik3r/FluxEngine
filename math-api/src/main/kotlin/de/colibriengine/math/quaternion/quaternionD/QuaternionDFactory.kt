package de.colibriengine.math.quaternion.quaternionD

/** Every [QuaternionD] factory implementation should expose these methods. */
interface QuaternionDFactory {

    fun acquire(): QuaternionD

    fun acquireDirty(): QuaternionD

    fun free(quaternionD: QuaternionD)

    fun immutableQuaternionDBuilder(): ImmutableQuaternionD.Builder

    fun identity(): ImmutableQuaternionD

}
