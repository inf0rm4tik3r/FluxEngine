package de.colibriengine.math.quaternion.quaternionD

import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.quaternion.quaternionF.QuaternionFMutator
import de.colibriengine.math.vector.vec3d.Vec3dAccessor

/** Defines all mutating methods of the double quaternion. */
interface QuaternionDMutator {

    /** The first vector component (x) of this quaternion. */
    var x: Double

    /** The second vector component (y) of this quaternion. */
    var y: Double

    /** The third vector component (z) of this quaternion. */
    var z: Double

    /** The scalar part (w) of this quaternion. */
    var w: Double

    /**
     * Sets the x component of this quaternion to the specified value.
     *
     * @param x The new value for the x component.
     * @return This instance for method chaining.
     */
    fun setX(x: Double): QuaternionDMutator

    /**
     * Sets the y component of this quaternion to the specified value.
     *
     * @param y The new value for the y component.
     * @return This instance for method chaining.
     */
    fun setY(y: Double): QuaternionDMutator

    /**
     * Sets the z component of this quaternion to the specified value.
     *
     * @param z The new value for the z component.
     * @return This instance for method chaining.
     */
    fun setZ(z: Double): QuaternionDMutator

    /**
     * Sets the w component of this quaternion to the specified value.
     *
     * @param w The new value for the w component.
     * @return This instance for method chaining.
     */
    fun setW(w: Double): QuaternionDMutator

    /**
     * Sets the vector part of this quaternion to the vector specified by `(x y z)` and the scalar part to [w].
     *
     * @param x The x component of the vector part.
     * @param y The y component of the vector part.
     * @param z The z component of the vector part.
     * @param w The value of / for the scalar part.
     * @return This instance for method chaining.
     */
    fun set(x: Double, y: Double, z: Double, w: Double): QuaternionDMutator

    /**
     * Sets the vector and scalar parts of this quaternion to the vector and scalar parts of [other] respectively.
     *
     * @param other The quaternion which vector and scalar parts should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: QuaternionFAccessor): QuaternionDMutator

    /**
     * Sets the vector and scalar parts of this quaternion to the vector and scalar parts of [other] respectively.
     *
     * @param other The quaternion which vector and scalar parts should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: QuaternionDAccessor): QuaternionDMutator

    /**
     * Initializes this quaternion to the unit quaternion. Its vector part is set to 0 and its scalar part is set to 1.
     * It then looks like this: [0, 0, 0, 1]. The identity quaternion represents no rotation. Multiplying a quaternion
     * with this identity leaves the quaternion unchanged!
     *
     * @return This instance for method chaining.
     */
    fun initIdentity(): QuaternionDMutator

    /**
     * Initializes this quaternion using the specified rotation axis and rotation angle. It becomes a normalized unit
     * quaternion representing an orientation rotated angle degrees around the specified axis vector. The rotation axis
     * must not be normalized!
     *
     * @param axisX The x component of the axis to rotate around.
     * @param axisY The y component of the axis to rotate around.
     * @param axisZ The z component of the axis to rotate around.
     * @param angle The amount of rotation around the specified axis in degrees.
     * @return This instance for method chaining.
     */
    fun initRotation(axisX: Double, axisY: Double, axisZ: Double, angle: Double): QuaternionDMutator

    /**
     * Initializes this quaternion using the specified rotation axis and rotation angle. It becomes a normalized, unit
     * quaternion representing an orientation rotated angle degrees around the specified axis vector. The rotation axis
     * must not be normalized!
     *
     * @param axis The axis to rotate around.
     * @param angle The amount of rotation around the specified axis in degrees.
     * @return This instance for method chaining.
     */
    fun initRotation(axis: Vec3dAccessor, angle: Double): QuaternionDMutator

    /**
     * Initializes this quaternion to represent a rotation around the unit x axis of [angle] degrees. The same effect
     * can be obtained by calling `initRotation(UNIT_X_AXIS, angle)`.
     *
     * @param angle The degrees to rotate around the unit x axis.
     * @return This instance for method chaining.
     */
    fun initXRotation(angle: Double): QuaternionDMutator

    /**
     * Initializes this quaternion to represent a rotation around the unit y axis of [angle] degrees. The same effect
     * can be obtained by calling `initRotation(UNIT_Y_AXIS, angle)`.
     *
     * @param angle The degrees to rotate around the unit y axis.
     * @return This instance for method chaining.
     */
    fun initYRotation(angle: Double): QuaternionDMutator

    /**
     * Initializes this quaternion to represent a rotation around the unit z axis of [angle] degrees. The same effect
     * can be obtained by calling `initRotation(UNIT_Z_AXIS, angle)`.
     *
     * @param angle The degrees to rotate around the unit z axis.
     * @return This instance for method chaining.
     */
    fun initZRotation(angle: Double): QuaternionDMutator

    /**
     * Initializes this quaternion to represent a rotation specified by the 3 angles (around the unit axes). Try to
     * avoid this method as it might be slower than the alternatives!
     *
     * @param angleAroundX Amount of degrees to rotate around the x-Axis.
     * @param angleAroundY Amount of degrees to rotate around the y-Axis.
     * @param angleAroundZ Amount of degrees to rotate around the z-Axis.
     * @return This instance for method chaining.
     */
    fun initRotation(angleAroundX: Double, angleAroundY: Double, angleAroundZ: Double): QuaternionDMutator

    /**
     * Initializes this quaternion to represent a rotation specified by the 3 angles (around the unit axes) which
     * are stored in the vector accessible by [angles]. Try to avoid this method as it might be slower than the
     * alternatives!
     *
     * @param angles The vector which stores the axis-angle information.
     * @return This instance for method chaining.
     */
    fun initRotation(angles: Vec3dAccessor): QuaternionDMutator

    /**
     * Initializes this quaternion so that its defined rotation lets us rotate in a way that the forward direction of
     * this quaternion goes from the source to the target position while ensuring a specific up direction.
     *
     * @param sourcePosition The position from which to look.
     * @param targetPosition The position to look at.
     * @param upDirection The direction to the top when looking from the source to the target position.
     * @return This instance for method chaining.
     */
    fun initLookAt(
        sourcePosition: Vec3dAccessor,
        targetPosition: Vec3dAccessor,
        upDirection: Vec3dAccessor
    ): QuaternionDMutator

    /**
     * **See:** [what-is-the-source-code-of-quaternionlookrotation (unity)]
     * (https://answers.unity.com/questions/467614/what-is-the-source-code-of-quaternionlookrotation.html) *
     *
     * @param forward The direction in which we want to look.
     * @param up The up direction of the viewer.
     * @return This instance for method chaining.
     */
    fun initLookRotation(forward: Vec3dAccessor, up: Vec3dAccessor): QuaternionDMutator

    /**
     * Normalizes this quaternion. Same operation as normalizing a vector.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the quaternion is of length 0.
     */
    fun normalize(): QuaternionDMutator

    /**
     * Inverts this quaternion.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the quaternion is of length 0.
     */
    fun invert(): QuaternionDMutator

    /**
     * Inverts the x, y and z components (vector part) of this quaternion (the real / scalar part remains unchanged).
     *
     * @return This instance for method chaining.
     */
    fun conjugate(): QuaternionDMutator

    /**
     * Multiplies all components of this quaternion with the given scalar value.
     *
     * @param scalar The value to mul with.
     * @return This instance for method chaining.
     */
    operator fun times(scalar: Double): QuaternionDMutator

    /**
     * Multiplies this quaternion with the given vector.
     *
     * @param vector The vector to multiply with.
     * @return This instance for method chaining.
     */
    operator fun times(vector: Vec3dAccessor): QuaternionDMutator

    /**
     * Multiplies this quaternion with the specified quaternion and stores the result in this object.
     *
     * @param other The other quaternion to multiply with.
     * @return This instance for method chaining.
     * @see "Grundlagen der 3D-Programmierung" S.517 -> 7.96
     */
    operator fun times(other: QuaternionDAccessor): QuaternionDMutator

    /**
     * Divides this quaternion by the given [other] quaternion.
     *
     * @param other The quaternion to divide by.
     * @return This instance for method chaining.
     */
    operator fun div(other: QuaternionDAccessor): QuaternionDMutator

    /**
     * Performs a linear interpolation to the [target] quaternion. Distance between each step is equal across the entire
     * interpolation.
     *
     * @param target The target quaternion.
     * @param lerpFactor The step of interpolation.
     * @return This instance for method chaining.
     */
    fun lerp(target: QuaternionDAccessor, lerpFactor: Double): QuaternionDMutator

    /**
     * Performs a spherical linear interpolation to the [target] quaternion. The interpolation is mapped as though on a
     * quarter segment of a circle. The distant between each step is not equidistant.
     *
     * @param target The target quaternion.
     * @param lerpFactor The step of interpolation.
     * @return This instance for method chaining.
     */
    fun slerp(target: QuaternionDAccessor, lerpFactor: Double): QuaternionDMutator

}
