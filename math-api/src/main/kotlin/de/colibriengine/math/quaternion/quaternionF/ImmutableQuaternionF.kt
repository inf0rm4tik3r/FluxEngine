package de.colibriengine.math.quaternion.quaternionF

/** Immutable version of the [QuaternionF]. */
interface ImmutableQuaternionF : QuaternionFAccessor {

    interface Builder {
        fun of(x: Float, y: Float, z: Float, w: Float): ImmutableQuaternionF

        fun of(quaternionFAccessor: QuaternionFAccessor): ImmutableQuaternionF

        fun setX(x: Float): Builder

        fun setY(y: Float): Builder

        fun setZ(z: Float): Builder

        fun setW(w: Float): Builder

        fun set(x: Float, y: Float, z: Float, w: Float): Builder

        fun set(quaternionFAccessor: QuaternionFAccessor): Builder

        fun build(): ImmutableQuaternionF
    }

}
