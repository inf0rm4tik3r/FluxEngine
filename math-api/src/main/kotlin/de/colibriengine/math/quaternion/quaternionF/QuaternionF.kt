package de.colibriengine.math.quaternion.quaternionF

import de.colibriengine.math.Viewable
import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

/** Provides the general functionality of a quaternion in float precision. */
interface QuaternionF : QuaternionFAccessor, QuaternionFMutator, Viewable<QuaternionFView> {

    override var x: Float
    override var y: Float
    override var z: Float
    override var w: Float

    override fun setX(x: Float): QuaternionF
    override fun setY(y: Float): QuaternionF
    override fun setZ(z: Float): QuaternionF
    override fun setW(w: Float): QuaternionF

    override fun set(x: Float, y: Float, z: Float, w: Float): QuaternionF
    override fun set(other: QuaternionFAccessor): QuaternionF
    override fun set(other: QuaternionDAccessor): QuaternionF

    override fun initIdentity(): QuaternionF
    override fun initRotation(axisX: Float, axisY: Float, axisZ: Float, angle: Float): QuaternionF
    override fun initRotation(axis: Vec3fAccessor, angle: Float): QuaternionF
    override fun initXRotation(angle: Float): QuaternionF
    override fun initYRotation(angle: Float): QuaternionF
    override fun initZRotation(angle: Float): QuaternionF
    override fun initRotation(angleAroundX: Float, angleAroundY: Float, angleAroundZ: Float): QuaternionF
    override fun initRotation(angles: Vec3fAccessor): QuaternionF
    override fun initLookAt(
        sourcePosition: Vec3fAccessor,
        targetPosition: Vec3fAccessor,
        upDirection: Vec3fAccessor
    ): QuaternionF
    override fun initLookRotation(forward: Vec3fAccessor, up: Vec3fAccessor): QuaternionF

    override fun normalize(): QuaternionF
    override fun invert(): QuaternionF
    override fun conjugate(): QuaternionF

    override operator fun times(scalar: Float): QuaternionF
    override operator fun times(vector: Vec3fAccessor): QuaternionF
    override operator fun times(other: QuaternionFAccessor): QuaternionF

    override operator fun div(other: QuaternionFAccessor): QuaternionF

    override fun lerp(target: QuaternionFAccessor, lerpFactor: Float): QuaternionF
    override fun slerp(target: QuaternionFAccessor, lerpFactor: Float): QuaternionF

    @Suppress("MemberVisibilityCanBePrivate", "unused")
    companion object {
        /** The number of float values used to represent this quaternion. */
        const val FLOATS = 4

        /**
         * The number of bytes used to represent this quaternion object. 4 float values of 4 bytes each => 16 bytes.
         */
        const val BYTES = FLOATS * java.lang.Float.BYTES
    }

}
