package de.colibriengine.math.quaternion.quaternionF

import de.colibriengine.buffers.BufferStorable
import de.colibriengine.math.matrix.mat4f.Mat4f
import de.colibriengine.math.vector.vec3f.Vec3f

/** Defines all non-mutable methods of the float quaternion. */
interface QuaternionFAccessor : BufferStorable {

    /** The first vector component (x) of this quaternion. */
    val x: Float

    /** The second vector component (y) of this quaternion. */
    val y: Float

    /** The third vector component (z) of this quaternion. */
    val z: Float

    /** The scalar part (w) of this quaternion. */
    val w: Float

    /** Stores the vector part of this quaternion in [storeIn] and returns [storeIn]. */
    fun xyz(storeIn: Vec3f): Vec3f

    /** Stores the vector part of this quaternion in [storeIn] and returns [storeIn]. Alias for [xyz]. */
    fun vectorPart(storeIn: Vec3f): Vec3f

    /** The scalar part of this quaternion. Alias for [w]. */
    fun scalarPart(): Float

    /** Returns true if the vector part is 0 and the scalar part is 1. Quaternion must look like: [0, 0, 0, 1] */
    fun isIdentity(): Boolean

    /** Calculates the squared length of this quaternion. */
    fun lengthSquared(): Float

    /** Calculates the length of this quaternion. */
    fun length(): Float

    /** Stores the forward direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun forward(storeIn: Vec3f): Vec3f

    /** Stores the forward direction of this quaternion in [storeIn] starting at index [index]. */
    fun forward(storeIn: FloatArray, index: Int = 0)

    /** Stores the backward direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun backward(storeIn: Vec3f): Vec3f

    /** Stores the backward direction of this quaternion in [storeIn] starting at index [index]. */
    fun backward(storeIn: FloatArray, index: Int = 0)

    /** Stores the up direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun up(storeIn: Vec3f): Vec3f

    /** Stores the up direction of this quaternion in [storeIn] starting at index [index]. */
    fun up(storeIn: FloatArray, index: Int = 0)

    /** Stores the down direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun down(storeIn: Vec3f): Vec3f

    /** Stores the down direction of this quaternion in [storeIn] starting at index [index]. */
    fun down(storeIn: FloatArray, index: Int = 0)

    /** Stores the right direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun right(storeIn: Vec3f): Vec3f

    /** Stores the right direction of this quaternion in [storeIn] starting at index [index]. */
    fun right(storeIn: FloatArray, index: Int = 0)

    /** Stores the left direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun left(storeIn: Vec3f): Vec3f

    /** Stores the left direction of this quaternion in [storeIn] starting at index [index]. */
    fun left(storeIn: FloatArray, index: Int = 0)

    /** Stores the matrix representation of this quaternion in [storeIn] and returns [storeIn]. */
    fun asMatrix(storeIn: Mat4f): Mat4f

    /** Returns a byte array which contains the [x] [y] [z] and [w] components in this order. */
    fun bytes(): ByteArray

    /** Provides a shortened string representation by restricting the decimal places of each component. */
    fun toFormattedString(): String

}
