package de.colibriengine.math.quaternion.quaternionF

/** Every [QuaternionF] factory implementation should expose these methods. */
interface QuaternionFFactory {

    fun acquire(): QuaternionF

    fun acquireDirty(): QuaternionF

    fun free(quaternionF: QuaternionF)

    fun immutableQuaternionFBuilder(): ImmutableQuaternionF.Builder

    fun identity(): ImmutableQuaternionF

}
