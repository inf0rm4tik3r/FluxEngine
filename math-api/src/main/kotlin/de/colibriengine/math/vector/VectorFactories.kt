package de.colibriengine.math.vector

import de.colibriengine.math.vector.vec2d.Vec2dFactory
import de.colibriengine.math.vector.vec2f.Vec2fFactory
import de.colibriengine.math.vector.vec2i.Vec2iFactory
import de.colibriengine.math.vector.vec2l.Vec2lFactory
import de.colibriengine.math.vector.vec3d.Vec3dFactory
import de.colibriengine.math.vector.vec3f.Vec3fFactory
import de.colibriengine.math.vector.vec3i.Vec3iFactory
import de.colibriengine.math.vector.vec3l.Vec3lFactory
import de.colibriengine.math.vector.vec4d.Vec4dFactory
import de.colibriengine.math.vector.vec4f.Vec4fFactory
import de.colibriengine.math.vector.vec4i.Vec4iFactory
import de.colibriengine.math.vector.vec4l.Vec4lFactory

interface VectorFactories {

    fun vec2f(): Vec2fFactory
    fun vec2d(): Vec2dFactory
    fun vec2i(): Vec2iFactory
    fun vec2l(): Vec2lFactory

    fun vec3f(): Vec3fFactory
    fun vec3d(): Vec3dFactory
    fun vec3i(): Vec3iFactory
    fun vec3l(): Vec3lFactory

    fun vec4f(): Vec4fFactory
    fun vec4d(): Vec4dFactory
    fun vec4i(): Vec4iFactory
    fun vec4l(): Vec4lFactory

}
