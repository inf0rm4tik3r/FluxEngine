package de.colibriengine.math.vector.vec2d

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec2d]. */
interface ImmutableVec2d : Vec2dAccessor, AsMutable<Vec2d> {

    interface Builder {
        fun of(value: Double): ImmutableVec2d
        fun of(x: Double, y: Double): ImmutableVec2d
        fun of(other: Vec2dAccessor): ImmutableVec2d

        fun setX(x: Double): Builder
        fun setY(y: Double): Builder

        fun set(value: Double): Builder
        fun set(x: Double, y: Double): Builder
        fun set(other: Vec2dAccessor): Builder

        fun build(): ImmutableVec2d
    }

}
