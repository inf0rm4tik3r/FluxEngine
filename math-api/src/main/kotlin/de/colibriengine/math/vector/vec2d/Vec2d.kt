package de.colibriengine.math.vector.vec2d

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec2l.Vec2lAccessor

/** Provides the general functionality of a two dimensional vector in double precision. */
@Suppress("MemberVisibilityCanBePrivate")
interface Vec2d : Vec2dAccessor, Vec2dMutator, Viewable<Vec2dView>, Copyable<Vec2d>, AsImmutable<ImmutableVec2d> {

    override var x: Double
    override var y: Double

    /*
    override fun setX(x: Double): Vec2d
    override fun setY(y: Double): Vec2d
    */

    override fun set(value: Double): Vec2d
    override fun set(x: Double, y: Double): Vec2d
    override fun set(other: Vec2fAccessor): Vec2d
    override fun set(other: Vec2dAccessor): Vec2d
    override fun set(other: Vec2iAccessor): Vec2d
    override fun set(other: Vec2lAccessor): Vec2d

    override fun initZero(): Vec2d

    override operator fun plus(addend: Double): Vec2d
    override operator fun plus(addends: Vec2dAccessor): Vec2d
    override fun plus(addendX: Double, addendY: Double): Vec2d

    override operator fun minus(subtrahend: Double): Vec2d
    override operator fun minus(subtrahends: Vec2dAccessor): Vec2d
    override fun minus(subtrahendX: Double, subtrahendY: Double): Vec2d

    override operator fun times(factor: Double): Vec2d
    override operator fun times(factors: Vec2dAccessor): Vec2d
    override fun times(factorX: Double, factorY: Double): Vec2d

    override operator fun div(divisor: Double): Vec2d
    override operator fun div(divisors: Vec2dAccessor): Vec2d
    override fun div(divisorX: Double, divisorY: Double): Vec2d

    override fun normalize(): Vec2d
    override fun setLengthTo(targetValue: Double): Vec2d
    override fun invert(): Vec2d
    override fun abs(): Vec2d
    override fun shorten(): Vec2d

    companion object {
        /** The number of double values used to represent this vector. */
        const val DOUBLES = 2

        /** The number of bytes used to represent this vector object. */
        const val BYTES = DOUBLES * java.lang.Double.BYTES
    }

}
