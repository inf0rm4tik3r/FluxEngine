package de.colibriengine.math.vector.vec2d

import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec2l.Vec2lAccessor

/** Defines all mutating methods of the two dimensional double vector. */
interface Vec2dMutator {

    /** The first component (x) of this vector. */
    var x: Double

    /** The second component (y) of this vector. */
    var y: Double

    /**
     * Sets the x and y components of this vector to the specified [value].
     *
     * @param value The new value assigned to each component.
     * @return This instance for method chaining.
     */
    fun set(value: Double): Vec2dMutator

    /**
     * Sets the x and y components of this vector to the specified values.
     *
     * @param x The new value for the x component.
     * @param y The new value for the y component.
     * @return This instance for method chaining.
     */
    fun set(x: Double, y: Double): Vec2dMutator

    /**
     * Sets the x and y components of this vector to the x and y components of [other] respectively.
     *
     * @param other The other vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2fAccessor): Vec2dMutator

    /**
     * Sets the x and y components of this vector to the x and y components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2dAccessor): Vec2dMutator

    /**
     * Sets the x and y components of this vector to the x and y components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2iAccessor): Vec2dMutator

    /**
     * Sets the x and y components of this vector to the x and y components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2lAccessor): Vec2dMutator

    /**
     * Sets the x and y components of this vector to zero (0).
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Vec2dMutator

    /**
     * Adds the given double value to the x and y components of this vector respectively.
     *
     * @param addend The value to add.
     * @return This instance for method chaining.
     */
    operator fun plus(addend: Double): Vec2dMutator

    /**
     * Adds the given values (x and y) to the x and y components of this vector respectively.
     *
     * @param addendX The value to add to this vectors x component.
     * @param addendY The value to add to this vectors y component.
     * @return This instance for method chaining.
     */
    fun plus(addendX: Double, addendY: Double): Vec2dMutator

    /**
     * Adds the values of the x and y components of the specified [addends] vector to the x and y components of this
     * vector respectively.
     *
     * @param addends The vector from which to read the addends.
     * @return This instance for method chaining.
     */
    operator fun plus(addends: Vec2dAccessor): Vec2dMutator

    /**
     * Subtracts the given double value from the x and y components of this vector respectively.
     *
     * @param subtrahend The value to subtract.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahend: Double): Vec2dMutator

    /**
     * Subtracts the given values (x and y) from the x and y components of this vector respectively.
     *
     * @param subtrahendX The value to subtract from this vectors x component.
     * @param subtrahendY The value to subtract from this vectors y component.
     * @return This instance for method chaining.
     */
    fun minus(subtrahendX: Double, subtrahendY: Double): Vec2dMutator

    /**
     * Subtracts the values of the x and y components of the specified [subtrahends] vector from the x and y components
     * of this vector respectively.
     *
     * @param subtrahends The vector from which to read the subtrahends.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahends: Vec2dAccessor): Vec2dMutator

    /**
     * Multiplies the given double value to the x and y components of this vector respectively.
     *
     * @param factor The value to multiply with.
     * @return This instance for method chaining.
     */
    operator fun times(factor: Double): Vec2dMutator

    /**
     * Multiplies the given values (x and y) to the x and y components of this vector respectively.
     *
     * @param factorX The value to multiply this vectors x component with.
     * @param factorY The value to multiply this vectors y component with.
     * @return This instance for method chaining.
     */
    fun times(factorX: Double, factorY: Double): Vec2dMutator

    /**
     * Multiplies the values of the x and y components of the specified [factors] vector to the x and y components of
     * this vector respectively.
     *
     * @param factors The vector from which to read the factors.
     * @return This instance for method chaining.
     */
    operator fun times(factors: Vec2dAccessor): Vec2dMutator

    /**
     * Divides the given double value from the x and y components of this vector respectively.
     *
     * @param divisor The value to divide by.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisor: Double): Vec2dMutator

    /**
     * Divides the x and y components of this vector by the given values (x and y) respectively.
     *
     * @param divisorX The value by which this vectors x component is to be divided.
     * @param divisorY The value by which this vectors y component is to be divided.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    fun div(divisorX: Double, divisorY: Double): Vec2dMutator

    /**
     * Divides the x and y components of this vector by the values of the x and y components of the specified [divisors]
     * vector respectively.
     *
     * @param divisors The vector from which to read the divisors.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisors: Vec2dAccessor): Vec2dMutator

    /**
     * Normalizes this vector by setting its length to ~1.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun normalize(): Vec2dMutator

    /**
     * Sets the length of this vector to ~ the specified value.
     *
     * @param targetValue The new length to scale to.
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun setLengthTo(targetValue: Double): Vec2dMutator

    /**
     * Inverts the x and y components of this vector.
     *
     *      x = -x
     *      y = -y
     *
     * @return This instance for method chaining.
     */
    fun invert(): Vec2dMutator

    /**
     * Sets the x and y components of this vector to their absolute values respectively.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Vec2dMutator

    /**
     * Makes the components more user / reader-friendly.
     *
     * @return This object for method chaining.
     */
    fun shorten(): Vec2dMutator

}
