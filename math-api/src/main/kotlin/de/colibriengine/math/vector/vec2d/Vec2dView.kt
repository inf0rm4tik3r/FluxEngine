package de.colibriengine.math.vector.vec2d

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec2dView. */
interface Vec2dView : Vec2dAccessor, AsMutable<Vec2d>, AsImmutable<ImmutableVec2d>
