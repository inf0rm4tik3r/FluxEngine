package de.colibriengine.math.vector.vec2f

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec2f]. */
interface ImmutableVec2f : Vec2fAccessor, AsMutable<Vec2f> {

    interface Builder {
        fun of(value: Float): ImmutableVec2f
        fun of(x: Float, y: Float): ImmutableVec2f
        fun of(other: Vec2fAccessor): ImmutableVec2f

        fun setX(x: Float): Builder
        fun setY(y: Float): Builder

        fun set(value: Float): Builder
        fun set(x: Float, y: Float): Builder
        fun set(other: Vec2fAccessor): Builder

        fun build(): ImmutableVec2f
    }

}
