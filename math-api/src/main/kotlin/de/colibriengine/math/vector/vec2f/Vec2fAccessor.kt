package de.colibriengine.math.vector.vec2f

import de.colibriengine.buffers.BufferStorable

/** Defines all non-mutable methods of the two dimensional float vector. */
interface Vec2fAccessor : BufferStorable {

    /** The first component (x) of this vector. */
    val x: Float

    /** The second component (y) of this vector. */
    val y: Float

    /** Returns true if at least one component is zero. */
    fun hasZeroComponent(): Boolean

    /** Returns the length of this vector. */
    fun length(): Double

    /** Returns the squared length of this vector. Less computationally expensive then [length]. */
    fun squaredLength(): Double

    /** Returns the lowest component of this vector. */
    fun min(): Float

    /** Returns the highest component of this vector. */
    fun max(): Float

    /**
     * Calculates the scalar product / dot product of this and the given vector.
     *
     * @param other The second vector with which the dot product gets calculated.
     * @return The dot product in double precision.
     */
    infix fun dot(other: Vec2fAccessor): Double

    /**
     * Performs a linear interpolation between this and the target vector.
     *
     * @param target The target vector.
     * @param lerpFactor Amount of interpolation. Use values in the [0,...,1] (both inclusive) range.
     * @return The interpolated vector.
     * @throws IllegalArgumentException If the specified [lerpFactor] is either negative or greater than 1.
     */
    fun lerp(target: Vec2fAccessor, lerpFactor: Float): Vec2f

    /**
     * Performs a linear interpolation between this and the target vector.
     *
     * @param target The target vector.
     * @param lerpFactor Amount of interpolation. Any value can be used.
     * @return The interpolated vector.
     */
    fun lerpFree(target: Vec2fAccessor, lerpFactor: Float): Vec2f

    /**
     * Returns a byte array which represents this vector.
     *
     * @return A byte array. Its length is equal to the dimension of this vector. The vectors components are stored in
     *     the ordinary ordering of the vectors components (first to last).
     */
    fun bytes(): ByteArray

    /**
     * Provides a shortened string representation of this vector. Decimal places of each component are restricted. Maybe
     * more reader-friendly.
     *
     * @return A descriptive string of this vector which contains its components.
     */
    fun toFormattedString(): String

}
