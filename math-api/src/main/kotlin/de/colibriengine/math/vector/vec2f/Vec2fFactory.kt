package de.colibriengine.math.vector.vec2f

/** Every [Vec2f] factory implementation should expose these methods. */
interface Vec2fFactory {

    fun acquire(): Vec2f

    fun acquireDirty(): Vec2f

    fun free(vec2f: Vec2f)

    fun immutableVec2fBuilder(): ImmutableVec2f.Builder

    fun zero(): ImmutableVec2f

    fun one(): ImmutableVec2f

}
