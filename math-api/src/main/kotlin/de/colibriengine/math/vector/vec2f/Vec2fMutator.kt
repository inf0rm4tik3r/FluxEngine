package de.colibriengine.math.vector.vec2f

import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2d.Vec2dMutator
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec2l.Vec2lAccessor

/** Defines all mutating methods of the two dimensional float vector. */
interface Vec2fMutator {

    /** The first component (x) of this vector. */
    var x: Float

    /** The second component (y) of this vector. */
    var y: Float

    /**
     * Sets the x and y components of this vector to the specified [value].
     *
     * @param value The new value assigned to each component.
     * @return This instance for method chaining.
     */
    fun set(value: Float): Vec2fMutator

    /**
     * Sets the x and y components of this vector to the specified values.
     *
     * @param x The new value for the x component.
     * @param y The new value for the y component.
     * @return This instance for method chaining.
     */
    fun set(x: Float, y: Float): Vec2fMutator

    /**
     * Sets the x and y components of this vector to the x and y components of [other] respectively.
     *
     * @param other The other vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2fAccessor): Vec2fMutator

    /**
     * Sets the x and y components of this vector to the x and y components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2dAccessor): Vec2fMutator

    /**
     * Sets the x and y components of this vector to the x and y components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2iAccessor): Vec2fMutator

    /**
     * Sets the x and y components of this vector to the x and y components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2lAccessor): Vec2fMutator

    /**
     * Sets the x and y components of this vector to zero (0).
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Vec2fMutator

    /**
     * Adds the given float value to the x and y components of this vector respectively.
     *
     * @param addend The value to add.
     * @return This instance for method chaining.
     */
    operator fun plus(addend: Float): Vec2fMutator

    /**
     * Adds the given values (x and y) to the x and y components of this vector respectively.
     *
     * @param addendX The value to add to this vectors x component.
     * @param addendY The value to add to this vectors y component.
     * @return This instance for method chaining.
     */
    fun plus(addendX: Float, addendY: Float): Vec2fMutator

    /**
     * Adds the values of the x and y components of the specified [addends] vector to the x and y components of this
     * vector respectively.
     *
     * @param addends The vector from which to read the addends.
     * @return This instance for method chaining.
     */
    operator fun plus(addends: Vec2fAccessor): Vec2fMutator

    /**
     * Subtracts the given float value from the x and y components of this vector respectively.
     *
     * @param subtrahend The value to subtract.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahend: Float): Vec2fMutator

    /**
     * Subtracts the given values (x and y) from the x and y components of this vector respectively.
     *
     * @param subtrahendX The value to subtract from this vectors x component.
     * @param subtrahendY The value to subtract from this vectors y component.
     * @return This instance for method chaining.
     */
    fun minus(subtrahendX: Float, subtrahendY: Float): Vec2fMutator

    /**
     * Subtracts the values of the x and y components of the specified [subtrahends] vector from the x and y components
     * of this vector respectively.
     *
     * @param subtrahends The vector from which to read the subtrahends.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahends: Vec2fAccessor): Vec2fMutator

    /**
     * Multiplies the given float value to the x and y components of this vector respectively.
     *
     * @param factor The value to multiply with.
     * @return This instance for method chaining.
     */
    operator fun times(factor: Float): Vec2fMutator

    /**
     * Multiplies the given values (x and y) to the x and y components of this vector respectively.
     *
     * @param factorX The value to multiply this vectors x component with.
     * @param factorY The value to multiply this vectors y component with.
     * @return This instance for method chaining.
     */
    fun times(factorX: Float, factorY: Float): Vec2fMutator

    /**
     * Multiplies the values of the x and y components of the specified [factors] vector to the x and y components of
     * this vector respectively.
     *
     * @param factors The vector from which to read the factors.
     * @return This instance for method chaining.
     */
    operator fun times(factors: Vec2fAccessor): Vec2fMutator

    /**
     * Divides the given float value from the x and y components of this vector respectively.
     *
     * @param divisor The value to divide by.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisor: Float): Vec2fMutator

    /**
     * Divides the x and y components of this vector by the given values (x and y) respectively.
     *
     * @param divisorX The value by which this vectors x component is to be divided.
     * @param divisorY The value by which this vectors y component is to be divided.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    fun div(divisorX: Float, divisorY: Float): Vec2fMutator

    /**
     * Divides the x and y components of this vector by the values of the x and y components of the specified [divisors]
     * vector respectively.
     *
     * @param divisors The vector from which to read the divisors.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisors: Vec2fAccessor): Vec2fMutator

    /**
     * Normalizes this vector by setting its length to ~1.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun normalize(): Vec2fMutator

    /**
     * Sets the length of this vector to ~ the specified value.
     *
     * @param targetValue The new length to scale to.
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun setLengthTo(targetValue: Double): Vec2fMutator

    /**
     * Inverts the x and y components of this vector.
     *
     *      x = -x
     *      y = -y
     *
     * @return This instance for method chaining.
     */
    fun invert(): Vec2fMutator

    /**
     * Sets the x and y components of this vector to their absolute values respectively.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Vec2fMutator

    /**
     * Makes the components more user / reader-friendly.
     *
     * @return This object for method chaining.
     */
    fun shorten(): Vec2fMutator

}
