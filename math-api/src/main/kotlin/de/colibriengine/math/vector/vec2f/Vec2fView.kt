package de.colibriengine.math.vector.vec2f

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec2fView. */
interface Vec2fView : Vec2fAccessor, AsMutable<Vec2f>, AsImmutable<ImmutableVec2f>
