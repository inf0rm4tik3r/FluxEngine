package de.colibriengine.math.vector.vec2i

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2l.Vec2lAccessor

/** Provides the general functionality of a two dimensional vector in int precision. */
interface Vec2i : Vec2iAccessor, Vec2iMutator, Viewable<Vec2iView>, Copyable<Vec2i>, AsImmutable<ImmutableVec2i> {

    override var x: Int
    override var y: Int

    /*
    override fun setX(x: Int): Vec2i
    override fun setY(y: Int): Vec2i
    */

    override fun set(value: Int): Vec2i
    override fun set(x: Int, y: Int): Vec2i
    override fun set(other: Vec2fAccessor): Vec2i
    override fun set(other: Vec2dAccessor): Vec2i
    override fun set(other: Vec2iAccessor): Vec2i
    override fun set(other: Vec2lAccessor): Vec2i

    override fun initZero(): Vec2i

    override operator fun plus(addend: Int): Vec2i
    override operator fun plus(addends: Vec2iAccessor): Vec2i
    override fun plus(addendX: Int, addendY: Int): Vec2i

    override operator fun minus(subtrahend: Int): Vec2i
    override operator fun minus(subtrahends: Vec2iAccessor): Vec2i
    override fun minus(subtrahendX: Int, subtrahendY: Int): Vec2i

    override operator fun times(factor: Int): Vec2i
    override operator fun times(factors: Vec2iAccessor): Vec2i
    override fun times(factorX: Int, factorY: Int): Vec2i

    override operator fun div(divisor: Int): Vec2i
    override operator fun div(divisors: Vec2iAccessor): Vec2i
    override fun div(divisorX: Int, divisorY: Int): Vec2i

    override fun normalize(): Vec2i
    override fun setLengthTo(targetValue: Double): Vec2i
    override fun invert(): Vec2i
    override fun abs(): Vec2i

    companion object {
        /** The number of int values used to represent this vector. */
        const val INTS = 2

        /** The number of bytes used to represent this vector object. */
        const val BYTES = INTS * Integer.BYTES
    }

}
