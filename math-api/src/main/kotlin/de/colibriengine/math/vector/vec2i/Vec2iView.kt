package de.colibriengine.math.vector.vec2i

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec2iView. */
interface Vec2iView : Vec2iAccessor, AsMutable<Vec2i>, AsImmutable<ImmutableVec2i>
