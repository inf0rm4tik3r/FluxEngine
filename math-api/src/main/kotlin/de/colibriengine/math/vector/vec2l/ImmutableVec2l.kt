package de.colibriengine.math.vector.vec2l

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec2l]. */
interface ImmutableVec2l : Vec2lAccessor, AsMutable<Vec2l> {

    interface Builder {
        fun of(value: Long): ImmutableVec2l
        fun of(x: Long, y: Long): ImmutableVec2l
        fun of(other: Vec2lAccessor): ImmutableVec2l

        fun setX(x: Long): Builder
        fun setY(y: Long): Builder

        fun set(value: Long): Builder
        fun set(x: Long, y: Long): Builder
        fun set(other: Vec2lAccessor): Builder

        fun build(): ImmutableVec2l
    }

}
