package de.colibriengine.math.vector.vec2l

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor

/** Provides the general functionality of a two dimensional vector in long precision. */
@Suppress("MemberVisibilityCanBePrivate")
interface Vec2l : Vec2lAccessor, Vec2lMutator, Viewable<Vec2lView>, Copyable<Vec2l>, AsImmutable<ImmutableVec2l> {

    override var x: Long
    override var y: Long

    /*
    override fun setX(x: Long): Vec2l
    override fun setY(y: Long): Vec2l
    */

    override fun set(value: Long): Vec2l
    override fun set(x: Long, y: Long): Vec2l
    override fun set(other: Vec2fAccessor): Vec2l
    override fun set(other: Vec2dAccessor): Vec2l
    override fun set(other: Vec2iAccessor): Vec2l
    override fun set(other: Vec2lAccessor): Vec2l

    override fun initZero(): Vec2l

    override operator fun plus(addend: Long): Vec2l
    override operator fun plus(addends: Vec2lAccessor): Vec2l
    override fun plus(addendX: Long, addendY: Long): Vec2l

    override operator fun minus(subtrahend: Long): Vec2l
    override operator fun minus(subtrahends: Vec2lAccessor): Vec2l
    override fun minus(subtrahendX: Long, subtrahendY: Long): Vec2l

    override operator fun times(factor: Long): Vec2l
    override operator fun times(factors: Vec2lAccessor): Vec2l
    override fun times(factorX: Long, factorY: Long): Vec2l

    override operator fun div(divisor: Long): Vec2l
    override operator fun div(divisors: Vec2lAccessor): Vec2l
    override fun div(divisorX: Long, divisorY: Long): Vec2l

    override fun normalize(): Vec2l
    override fun setLengthTo(targetValue: Double): Vec2l
    override fun invert(): Vec2l
    override fun abs(): Vec2l

    companion object {
        /** The number of long values used to represent this vector. */
        const val LONGS = 2

        /** The number of bytes used to represent this vector object. */
        const val BYTES = LONGS * java.lang.Long.BYTES
    }

}
