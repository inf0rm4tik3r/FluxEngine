package de.colibriengine.math.vector.vec2l

/** Every [Vec2l] factory implementation should expose these methods. */
interface Vec2lFactory {

    fun acquire(): Vec2l

    fun acquireDirty(): Vec2l

    fun free(vec2l: Vec2l)

    fun immutableVec2lBuilder(): ImmutableVec2l.Builder

    fun zero(): ImmutableVec2l

    fun one(): ImmutableVec2l

}
