package de.colibriengine.math.vector.vec2l

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec2lView. */
interface Vec2lView : Vec2lAccessor, AsMutable<Vec2l>, AsImmutable<ImmutableVec2l>
