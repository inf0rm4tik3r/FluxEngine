package de.colibriengine.math.vector.vec3d

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec3d]. */
interface ImmutableVec3d : Vec3dAccessor, AsMutable<Vec3d> {

    interface Builder {
        fun of(value: Double): ImmutableVec3d
        fun of(x: Double, y: Double, z: Double): ImmutableVec3d
        fun of(vec3dAccessor: Vec3dAccessor): ImmutableVec3d

        fun setX(x: Double): Builder
        fun setY(y: Double): Builder
        fun setZ(z: Double): Builder

        fun set(value: Double): Builder
        fun set(x: Double, y: Double, z: Double): Builder
        fun set(vec3dAccessor: Vec3dAccessor): Builder

        fun build(): ImmutableVec3d
    }

}
