package de.colibriengine.math.vector.vec3d

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec3l.Vec3lAccessor

/** Provides the general functionality of a three dimensional vector in double precision. */
@Suppress("MemberVisibilityCanBePrivate")
interface Vec3d : Vec3dAccessor, Vec3dMutator, Viewable<Vec3dView>, Copyable<Vec3d>, AsImmutable<ImmutableVec3d> {

    override var x: Double
    override var y: Double
    override var z: Double

    override fun set(value: Double): Vec3d
    override fun set(x: Double, y: Double, z: Double): Vec3d
    override fun set(other: Vec3fAccessor): Vec3d
    override fun set(other: Vec3dAccessor): Vec3d
    override fun set(other: Vec3iAccessor): Vec3d
    override fun set(other: Vec3lAccessor): Vec3d
    override fun set(other: Vec2fAccessor, z: Double): Vec3d

    override fun initZero(): Vec3d

    override operator fun plus(addend: Double): Vec3d
    override operator fun plus(addends: Vec3dAccessor): Vec3d
    override fun plus(addendX: Double, addendY: Double, addendZ: Double): Vec3d

    override operator fun minus(subtrahend: Double): Vec3d
    override operator fun minus(subtrahends: Vec3dAccessor): Vec3d
    override fun minus(subtrahendX: Double, subtrahendY: Double, subtrahendZ: Double): Vec3d

    override operator fun times(factor: Double): Vec3d
    override operator fun times(factors: Vec3dAccessor): Vec3d
    override fun times(factorX: Double, factorY: Double, factorZ: Double): Vec3d

    override operator fun div(divisor: Double): Vec3d
    override operator fun div(divisors: Vec3dAccessor): Vec3d
    override fun div(divisorX: Double, divisorY: Double, divisorZ: Double): Vec3d

    override fun normalize(): Vec3d
    override fun setLengthTo(targetValue: Double): Vec3d
    override fun invert(): Vec3d
    override fun abs(): Vec3d
    override fun shorten(): Vec3d

    override fun cross(other: Vec3dAccessor): Vec3d

    override fun reflect(normalizedNormal: Vec3dAccessor): Vec3d
    override fun refract(normalizedNormal: Vec3dAccessor, eta: Double): Vec3d

    override fun rotate(quaternion: QuaternionDAccessor): Vec3d
    override fun rotate(axis: Vec3dAccessor, angle: Double): Vec3d

    companion object {
        /** The number of double values used to represent this vector. */
        const val DOUBLES = 3

        /** The number of bytes used to represent this vector object. */
        const val BYTES = DOUBLES * java.lang.Double.BYTES
    }

}
