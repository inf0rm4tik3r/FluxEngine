package de.colibriengine.math.vector.vec3d

import de.colibriengine.buffers.BufferStorable
import de.colibriengine.math.vector.vec2d.Vec2d

/** Defines all non-mutable methods of the three dimensional double vector. */
interface Vec3dAccessor : BufferStorable {

    /** The first component (x) of this vector. */
    val x: Double

    /** The second component (y) of this vector. */
    val y: Double

    /** The third component (z) of this vector. */
    val z: Double

    /** Returns [x] and [y] in a new [Vec2d]. */
    fun xy(): Vec2d

    /** Returns [y] and [z] in a new [Vec2d]. */
    fun yz(): Vec2d

    /** Returns [x] and [z] in a new [Vec2d]. */
    fun xz(): Vec2d

    /** Returns true if at least one component is zero. */
    fun hasZeroComponent(): Boolean

    /** Calculates the length of this vector. */
    fun length(): Double

    /** Returns the squared length of this vector. Less computationally expensive then [length]. */
    fun squaredLength(): Double

    /** Returns the lowest component of this vector. */
    fun min(): Double

    /** Returns the component between [min] and [max]. */
    fun mid(): Double

    /** Returns the highest component of this vector. */
    fun max(): Double

    /**
     * Calculates the scalar product / dot product of this and the given vector.
     *
     * @param other The second vector with which the dot product gets calculated.
     * @return The dot product in double precision.
     */
    infix fun dot(other: Vec3dAccessor): Double

    /**
     * Calculates the angle between this vector and the given vector. Both this and the given vector remain unchanged.
     *
     * @param other The vector against which the angle gets calculated.
     * @return Angle in radians in floating point precision.
     * @throws IllegalStateException Whenever this or the specified vector are exactly of length 0.
     */
    fun angleRadians(other: Vec3dAccessor): Double

    /**
     * Calculates the angle between this vector and the given vector. Both this and the given vector remain unchanged.
     *
     * @param other The vector against which the angle gets calculated.
     * @return Angle in degrees in floating point precision.
     * @throws IllegalStateException Whenever this or the specified vector are exactly of length 0.
     */
    fun angleDegrees(other: Vec3dAccessor): Double

    /**
     * Performs a linear interpolation between this and the target vector.
     *
     * @param target The target vector.
     * @param lerpFactor Amount of interpolation. Use values in the [0,...,1] (both inclusive) range.
     * @return The interpolated vector.
     * @throws IllegalArgumentException If the specified [lerpFactor] is either negative or greater than 1.
     */
    fun lerp(target: Vec3dAccessor, lerpFactor: Double): Vec3d

    /**
     * Performs a linear interpolation between this and the target vector.
     *
     * @param target The target vector.
     * @param lerpFactor Amount of interpolation. Any value can be used.
     * @return The interpolated vector.
     */
    fun lerpFree(target: Vec3dAccessor, lerpFactor: Double): Vec3d

    /**
     * Returns a byte array which represents this vector.
     *
     * @return A byte array. Its length is equal to the dimension of this vector. The vectors components are stored in
     *     the ordinary ordering of the vectors components (first to last).
     */
    fun bytes(): ByteArray

    /**
     * Provides a shortened string representation of this vector. Decimal places of each component are restricted. Maybe
     * more reader-friendly.
     *
     * @return A descriptive string of this vector which contains its components.
     */
    fun toFormattedString(): String

}
