package de.colibriengine.math.vector.vec3d

/** Every [Vec3d] factory implementation should expose these methods. */
interface Vec3dFactory {

    fun acquire(): Vec3d

    fun acquireDirty(): Vec3d

    fun free(vec3d: Vec3d)

    fun immutableVec3dBuilder(): ImmutableVec3d.Builder

    fun zero(): ImmutableVec3d

    fun one(): ImmutableVec3d

    fun unitXAxis(): ImmutableVec3d

    fun unitYAxis(): ImmutableVec3d

    fun unitZAxis(): ImmutableVec3d

    fun negativeUnitXAxis(): ImmutableVec3d

    fun negativeUnitYAxis(): ImmutableVec3d

    fun negativeUnitZAxis(): ImmutableVec3d

}
