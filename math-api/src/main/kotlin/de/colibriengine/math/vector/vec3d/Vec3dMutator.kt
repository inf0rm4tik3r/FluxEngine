package de.colibriengine.math.vector.vec3d

import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec3l.Vec3lAccessor

/** Defines all mutating methods of the three dimensional double vector. */
interface Vec3dMutator {

    /** The first component (x) of this vector. */
    var x: Double

    /** The second component (y) of this vector. */
    var y: Double

    /** The third component (z) of this vector. */
    var z: Double

    /**
     * Sets the x, y and z components of this vector to the specified [value].
     *
     * @param value The new value assigned to each component.
     * @return This instance for method chaining.
     */
    fun set(value: Double): Vec3dMutator

    /**
     * Sets the x, y and z components of this vector to the specified values.
     *
     * @param x The new value for the x component.
     * @param y The new value for the y component.
     * @param z The new value for the z component.
     * @return This instance for method chaining.
     */
    fun set(x: Double, y: Double, z: Double): Vec3dMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The other vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3fAccessor): Vec3dMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3dAccessor): Vec3dMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3iAccessor): Vec3dMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3lAccessor): Vec3dMutator

    /**
     * Sets the x and y components of this vector to the x and y components of [other] respectively and uses the
     * specified [z] value for the last component.
     *
     * @param other The vector which component values should be taken / copied.
     * @param z The value for the last component.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2fAccessor, z: Double): Vec3dMutator

    /**
     * Sets the x, y and z components of this vector to zero (0).
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Vec3dMutator

    /**
     * Adds the given double value to the x, y and z components of this vector respectively.
     *
     * @param addend The value to add.
     * @return This instance for method chaining.
     */
    operator fun plus(addend: Double): Vec3dMutator

    /**
     * Adds the given values (x, y and z) to the x, y and z components of this vector respectively.
     *
     * @param addendX The value to add to this vectors x component.
     * @param addendY The value to add to this vectors y component.
     * @param addendZ The value to add to this vectors z component.
     * @return This instance for method chaining.
     */
    fun plus(addendX: Double, addendY: Double, addendZ: Double): Vec3dMutator

    /**
     * Adds the values of the x, y and z components of the specified [addends] vector to the x, y and z components of
     * this vector respectively.
     *
     * @param addends The vector from which to read the addends.
     * @return This instance for method chaining.
     */
    operator fun plus(addends: Vec3dAccessor): Vec3dMutator

    /**
     * Subtracts the given double value from the x, y and z components of this vector respectively.
     *
     * @param subtrahend The value to subtract.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahend: Double): Vec3dMutator

    /**
     * Subtracts the given values (x, y and z) from the x, y and z components of this vector respectively.
     *
     * @param subtrahendX The value to subtract from this vectors x component.
     * @param subtrahendY The value to subtract from this vectors y component.
     * @param subtrahendZ The value to subtract from this vectors z component.
     * @return This instance for method chaining.
     */
    fun minus(subtrahendX: Double, subtrahendY: Double, subtrahendZ: Double): Vec3dMutator

    /**
     * Subtracts the values of the x, y and z components of the specified [subtrahends] vector from the x, y and z
     * components of this vector respectively.
     *
     * @param subtrahends The vector from which to read the subtrahends.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahends: Vec3dAccessor): Vec3dMutator

    /**
     * Multiplies the given double value to the x, y and z components of this vector respectively.
     *
     * @param factor The value to multiply with.
     * @return This instance for method chaining.
     */
    operator fun times(factor: Double): Vec3dMutator

    /**
     * Multiplies the given values (x, y and z) to the x, y and z components of this vector respectively.
     *
     * @param factorX The value to multiply this vectors x component with.
     * @param factorY The value to multiply this vectors y component with.
     * @param factorZ The value to multiply this vectors z component with.
     * @return This instance for method chaining.
     */
    fun times(factorX: Double, factorY: Double, factorZ: Double): Vec3dMutator

    /**
     * Multiplies the values of the x, y and z components of the specified [factors] vector to the x, y and z components
     * of this vector respectively.
     *
     * @param factors The vector from which to read the factors.
     * @return This instance for method chaining.
     */
    operator fun times(factors: Vec3dAccessor): Vec3dMutator

    /**
     * Divides the given double value from the x, y and z components of this vector respectively.
     *
     * @param divisor The value to divide by.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisor: Double): Vec3dMutator

    /**
     * Divides the x, y and z components of this vector by the given values (x, y and z) respectively.
     *
     * @param divisorX The value by which this vectors x component is to be divided.
     * @param divisorY The value by which this vectors y component is to be divided.
     * @param divisorZ The value by which this vectors z component is to be divided.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    fun div(divisorX: Double, divisorY: Double, divisorZ: Double): Vec3dMutator

    /**
     * Divides the x, y and z components of this vector by the values of the x, y and z components of the specified
     * [divisors] vector respectively.
     *
     * @param divisors The vector from which to read the divisors.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisors: Vec3dAccessor): Vec3dMutator

    /**
     * Normalizes this vector by setting its length to ~1.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun normalize(): Vec3dMutator

    /**
     * Sets the length of this vector to ~ the specified value.
     *
     * @param targetValue The new length to scale to.
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun setLengthTo(targetValue: Double): Vec3dMutator

    /**
     * Inverts the x, y and z components of this vector.
     *
     *      x = -x
     *      y = -y
     *      z = -z
     *
     * @return This instance for method chaining.
     */
    fun invert(): Vec3dMutator

    /**
     * Sets the x, y and z components of this vector to their absolute values respectively.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Vec3dMutator

    /**
     * Makes the components more user / reader-friendly.
     *
     * @return This object for method chaining.
     */
    fun shorten(): Vec3dMutator

    /**
     * Sets this vector to be perpendicular to the plane spanned by this and the given vector. Order of calculation is
     * important. vector1.cross(vector2) != vector2.cross(vector1)! Direction will be reversed.
     *
     * @param other The vector with which the cross product gets calculated.
     * @return This instance for method chaining. It will be in a 90 degree angle to this and the given vector.
     */
    fun cross(other: Vec3dAccessor): Vec3dMutator

    /**
     * Reflects this vector at the plane specified by the planes normalized normal vector.
     *
     * @param normalizedNormal The normalized normal vector of the plane to reflect of.
     * @return This instance for method chaining. Properly reflected (angle in = angle out).
     */
    fun reflect(normalizedNormal: Vec3dAccessor): Vec3dMutator

    /**
     * Refracts this vector of a given plane specified by its normal vector and the refraction index eta. This vector
     * and the given normal vector must be normalized! Consider using a predefined refraction index for a specific
     * material.
     *
     * @param normalizedNormal The normalized normal vector of the plane to reflect of.
     * @return This instance for method chaining. Properly refracted. Result will be a unit vector.
     */
    fun refract(normalizedNormal: Vec3dAccessor, eta: Double): Vec3dMutator

    /**
     * Rotates this vector by the rotation information in the given [quaternion].
     *
     * @param quaternion The quaternion to rotate by.
     * @return This instance for method chaining.
     */
    fun rotate(quaternion: QuaternionDAccessor): Vec3dMutator

    /**
     * Rotates this vector [angle] degrees around the specified [axis].
     *
     * @param axis The axis to rotate around.
     * @param angle The angle to rotate by.
     * @return This instance for method chaining.
     */
    fun rotate(axis: Vec3dAccessor, angle: Double): Vec3dMutator

    /**
     * Creates an orthonormal basis with [a] and [b] as a basis and calculating the missing axis [c].
     *
     * Preconditions:
     * - Neither [a] or [b] must be normalized.
     * - [c] can be of any content.
     *
     * Possible modifications:
     * - [a] will be normalized, if it wasn't already
     * - [b] will be modified to be orthogonal to [a], if it wasn't already
     * - [c] will be set from calculating the axis orthogonal to [a] and [b]
     *
     * Postconditions:
     * - [a], [b], and [c] are all orthogonal to one-another
     * - [a], [b], and [c] are all normalized
     */
    fun makeOrthonormalBasis(a: Vec3d, b: Vec3d, c: Vec3d)

}
