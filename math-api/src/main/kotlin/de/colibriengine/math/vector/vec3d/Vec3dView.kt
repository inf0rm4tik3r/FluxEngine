package de.colibriengine.math.vector.vec3d

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec3dView. */
interface Vec3dView : Vec3dAccessor, AsMutable<Vec3d>, AsImmutable<ImmutableVec3d>
