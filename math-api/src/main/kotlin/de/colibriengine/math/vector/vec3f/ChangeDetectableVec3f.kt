package de.colibriengine.math.vector.vec3f

import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec3l.Vec3lAccessor
import de.colibriengine.reactive.Subject

class ChangeDetectableVec3f(private val vec: Vec3f) : Vec3f by vec {

    val onChange: Subject<Boolean> = Subject()

    var changed: Boolean = false
        set(value) {
            field = value
            onChange.next(value)
        }

    override var x: Float
        get() = vec.x
        set(value) {
            vec.x = value
            changed = true
        }

    override var y: Float
        get() = vec.y
        set(value) {
            vec.y = value
            changed = true
        }

    override var z: Float
        get() = vec.z
        set(value) {
            vec.z = value
            changed = true
        }

    override fun plus(addend: Float): Vec3f {
        vec.plus(addend)
        changed = true
        return vec
    }

    override fun set(value: Float): Vec3f {
        vec.set(value)
        changed = true
        return vec
    }

    override fun set(x: Float, y: Float, z: Float): Vec3f {
        vec.set(x, y, z)
        changed = true
        return vec
    }

    override fun set(other: Vec3fAccessor): Vec3f {
        vec.set(other)
        changed = true
        return vec
    }

    fun setWithoutChangeDetection(other: Vec3fAccessor): Vec3f {
        return vec.set(other)
    }

    override fun set(other: Vec3dAccessor): Vec3f {
        vec.set(other)
        changed = true
        return vec
    }

    override fun set(other: Vec3iAccessor): Vec3f {
        vec.set(other)
        changed = true
        return vec
    }

    override fun set(other: Vec3lAccessor): Vec3f {
        vec.set(other)
        changed = true
        return vec
    }

    override fun set(other: Vec2fAccessor, z: Float): Vec3f {
        vec.set(other, z)
        changed = true
        return vec
    }

    override fun initZero(): Vec3f {
        vec.initZero()
        changed = true
        return vec
    }

    override fun plus(addends: Vec3fAccessor): Vec3f {
        vec.plus(addends)
        changed = true
        return vec
    }

    override fun plus(addendX: Float, addendY: Float, addendZ: Float): Vec3f {
        vec.plus(addendX, addendY, addendZ)
        changed = true
        return vec
    }

    fun plusWithoutChangeDetection(addendX: Float, addendY: Float, addendZ: Float): Vec3f {
        return vec.plus(addendX, addendY, addendZ)
    }

    override fun minus(subtrahend: Float): Vec3f {
        vec.minus(subtrahend)
        changed = true
        return vec
    }

    override fun minus(subtrahends: Vec3fAccessor): Vec3f {
        vec.minus(subtrahends)
        changed = true
        return vec
    }

    override fun minus(subtrahendX: Float, subtrahendY: Float, subtrahendZ: Float): Vec3f {
        vec.minus(subtrahendX, subtrahendY, subtrahendZ)
        changed = true
        return vec
    }

    override fun times(factor: Float): Vec3f {
        vec.times(factor)
        changed = true
        return vec
    }

    override fun times(factors: Vec3fAccessor): Vec3f {
        vec.times(factors)
        changed = true
        return vec
    }

    override fun times(factorX: Float, factorY: Float, factorZ: Float): Vec3f {
        vec.times(factorX, factorY, factorZ)
        changed = true
        return vec
    }

    override fun div(divisor: Float): Vec3f {
        vec.div(divisor)
        changed = true
        return vec
    }

    override fun div(divisors: Vec3fAccessor): Vec3f {
        vec.div(divisors)
        changed = true
        return vec
    }

    override fun div(divisorX: Float, divisorY: Float, divisorZ: Float): Vec3f {
        vec.div(divisorX, divisorY, divisorZ)
        changed = true
        return vec
    }

    override fun normalize(): Vec3f {
        vec.normalize()
        changed = true
        return vec
    }

    override fun setLengthTo(targetValue: Float): Vec3f {
        vec.setLengthTo(targetValue)
        changed = true
        return vec
    }

    override fun invert(): Vec3f {
        vec.invert()
        changed = true
        return vec
    }

    override fun abs(): Vec3f {
        vec.abs()
        changed = true
        return vec
    }

    override fun shorten(): Vec3f {
        vec.shorten()
        changed = true
        return vec
    }

    override fun toString(): String {
        return "$vec - changed: $changed"
    }

}
