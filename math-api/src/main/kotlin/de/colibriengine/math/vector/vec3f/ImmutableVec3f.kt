package de.colibriengine.math.vector.vec3f

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec3f]. */
interface ImmutableVec3f : Vec3fAccessor, AsMutable<Vec3f> {

    interface Builder {
        fun of(value: Float): ImmutableVec3f
        fun of(x: Float, y: Float, z: Float): ImmutableVec3f
        fun of(other: Vec3fAccessor): ImmutableVec3f

        fun setX(x: Float): Builder
        fun setY(y: Float): Builder
        fun setZ(z: Float): Builder

        fun set(value: Float): Builder
        fun set(x: Float, y: Float, z: Float): Builder
        fun set(other: Vec3fAccessor): Builder

        fun build(): ImmutableVec3f
    }

}
