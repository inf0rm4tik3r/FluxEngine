package de.colibriengine.math.vector.vec3f

import de.colibriengine.math.Viewable
import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec3l.Vec3lAccessor

/** Provides the general functionality of a three dimensional vector in float precision. */
interface Vec3f : Vec3fAccessor, Vec3fMutator, Viewable<Vec3fView> {

    override var x: Float
    override var y: Float
    override var z: Float

    override fun set(value: Float): Vec3f
    override fun set(x: Float, y: Float, z: Float): Vec3f
    override fun set(other: Vec3fAccessor): Vec3f
    override fun set(other: Vec3dAccessor): Vec3f
    override fun set(other: Vec3iAccessor): Vec3f
    override fun set(other: Vec3lAccessor): Vec3f
    override fun set(other: Vec2fAccessor, z: Float): Vec3f

    override fun initZero(): Vec3f

    override operator fun plus(addend: Float): Vec3f
    override operator fun plus(addends: Vec3fAccessor): Vec3f
    override fun plus(addendX: Float, addendY: Float, addendZ: Float): Vec3f

    override operator fun minus(subtrahend: Float): Vec3f
    override operator fun minus(subtrahends: Vec3fAccessor): Vec3f
    override fun minus(subtrahendX: Float, subtrahendY: Float, subtrahendZ: Float): Vec3f

    override operator fun times(factor: Float): Vec3f
    override operator fun times(factors: Vec3fAccessor): Vec3f
    override fun times(factorX: Float, factorY: Float, factorZ: Float): Vec3f

    override operator fun div(divisor: Float): Vec3f
    override operator fun div(divisors: Vec3fAccessor): Vec3f
    override fun div(divisorX: Float, divisorY: Float, divisorZ: Float): Vec3f

    override fun normalize(): Vec3f
    override fun setLengthTo(targetValue: Float): Vec3f
    override fun invert(): Vec3f
    override fun abs(): Vec3f
    override fun shorten(): Vec3f

    override infix fun cross(other: Vec3fAccessor): Vec3f

    override fun reflect(normalizedNormal: Vec3fAccessor): Vec3f
    override fun refract(normalizedNormal: Vec3fAccessor, eta: Double): Vec3f

    override fun rotate(quaternion: QuaternionFAccessor): Vec3f
    override fun rotate(axis: Vec3fAccessor, angle: Float): Vec3f

    companion object {
        /** The number of float values used to represent this vector. */
        const val FLOATS = 3

        /** The number of bytes used to represent this vector object. */
        const val BYTES = FLOATS * java.lang.Float.BYTES
    }

}
