package de.colibriengine.math.vector.vec3f

/** Every [Vec3f] factory implementation should expose these methods. */
interface Vec3fFactory {

    fun acquire(): Vec3f

    fun acquireDirty(): Vec3f

    fun free(vec3f: Vec3f)

    fun immutableVec3fBuilder(): ImmutableVec3f.Builder

    fun zero(): ImmutableVec3f

    fun one(): ImmutableVec3f

    fun unitXAxis(): ImmutableVec3f

    fun unitYAxis(): ImmutableVec3f

    fun unitZAxis(): ImmutableVec3f

    fun negativeUnitXAxis(): ImmutableVec3f

    fun negativeUnitYAxis(): ImmutableVec3f

    fun negativeUnitZAxis(): ImmutableVec3f

}
