package de.colibriengine.math.vector.vec3f

import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec3l.Vec3lAccessor

/** Defines all mutating methods of the three dimensional float vector. */
interface Vec3fMutator {

    /** The first component (x) of this vector. */
    var x: Float

    /** The second component (y) of this vector. */
    var y: Float

    /** The third component (z) of this vector. */
    var z: Float

    /**
     * Sets the x, y and z components of this vector to the specified [value].
     *
     * @param value The new value assigned to each component.
     * @return This instance for method chaining.
     */
    fun set(value: Float): Vec3fMutator

    /**
     * Sets the x, y and z components of this vector to the specified values.
     *
     * @param x The new value for the x component.
     * @param y The new value for the y component.
     * @param z The new value for the z component.
     * @return This instance for method chaining.
     */
    fun set(x: Float, y: Float, z: Float): Vec3fMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The other vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3fAccessor): Vec3fMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3dAccessor): Vec3fMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3iAccessor): Vec3fMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3lAccessor): Vec3fMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively and uses the
     * specified w value for the last component.
     *
     * @param other The vector which component values should be taken / copied.
     * @param z The value for the last component.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2fAccessor, z: Float): Vec3fMutator

    /**
     * Sets the x, y and z components of this vector to zero (0).
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Vec3fMutator

    /**
     * Adds the given float value to the x, y and z components of this vector respectively.
     *
     * @param addend The value to add.
     * @return This instance for method chaining.
     */
    operator fun plus(addend: Float): Vec3fMutator

    /**
     * Adds the given values (x, y and z) to the x, y and z components of this vector respectively.
     *
     * @param addendX The value to add to this vectors x component.
     * @param addendY The value to add to this vectors y component.
     * @param addendZ The value to add to this vectors z component.
     * @return This instance for method chaining.
     */
    fun plus(addendX: Float, addendY: Float, addendZ: Float): Vec3fMutator

    /**
     * Adds the values of the x, y and z components of the specified [addends] vector to the x, y and z components of
     * this vector respectively.
     *
     * @param addends The vector from which to read the addends.
     * @return This instance for method chaining.
     */
    operator fun plus(addends: Vec3fAccessor): Vec3fMutator

    /**
     * Subtracts the given float value from the x, y and z components of this vector respectively.
     *
     * @param subtrahend The value to subtract.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahend: Float): Vec3fMutator

    /**
     * Subtracts the given values (x, y and z) from the x, y and z components of this vector respectively.
     *
     * @param subtrahendX The value to subtract from this vectors x component.
     * @param subtrahendY The value to subtract from this vectors y component.
     * @param subtrahendZ The value to subtract from this vectors z component.
     * @return This instance for method chaining.
     */
    fun minus(subtrahendX: Float, subtrahendY: Float, subtrahendZ: Float): Vec3fMutator

    /**
     * Subtracts the values of the x, y and z components of the specified [subtrahends] vector from the x, y and z
     * components of this vector respectively.
     *
     * @param subtrahends The vector from which to read the subtrahends.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahends: Vec3fAccessor): Vec3fMutator

    /**
     * Multiplies the given float value to the x, y and z components of this vector respectively.
     *
     * @param factor The value to multiply with.
     * @return This instance for method chaining.
     */
    operator fun times(factor: Float): Vec3fMutator

    /**
     * Multiplies the given values (x, y and z) to the x, y and z components of this vector respectively.
     *
     * @param factorX The value to multiply this vectors x component with.
     * @param factorY The value to multiply this vectors y component with.
     * @param factorZ The value to multiply this vectors z component with.
     * @return This instance for method chaining.
     */
    fun times(factorX: Float, factorY: Float, factorZ: Float): Vec3fMutator

    /**
     * Multiplies the values of the x, y and z components of the specified [factors] vector to the x, y and z components
     * of this vector respectively.
     *
     * @param factors The vector from which to read the factors.
     * @return This instance for method chaining.
     */
    operator fun times(factors: Vec3fAccessor): Vec3fMutator

    /**
     * Divides the given float value from the x, y and z components of this vector respectively.
     *
     * @param divisor The value to divide by.
     * @return This instance for method chaining.
     */
    operator fun div(divisor: Float): Vec3fMutator

    /**
     * Divides the x, y and z components of this vector by the given values (x, y and z) respectively.
     *
     * @param divisorX The value by which this vectors x component is to be divided.
     * @param divisorY The value by which this vectors y component is to be divided.
     * @param divisorZ The value by which this vectors z component is to be divided.
     * @return This instance for method chaining.
     */
    fun div(divisorX: Float, divisorY: Float, divisorZ: Float): Vec3fMutator

    /**
     * Divides the x, y and z components of this vector by the values of the x, y and z components of the specified
     * [divisors] vector respectively.
     *
     * @param divisors The vector from which to read the divisors.
     * @return This instance for method chaining.
     */
    operator fun div(divisors: Vec3fAccessor): Vec3fMutator

    /**
     * Normalizes this vector by setting its length to ~1.
     *
     * @return This instance for method chaining.
     */
    fun normalize(): Vec3fMutator

    /**
     * Sets the length of this vector to the specified [targetValue].
     *
     * @param targetValue The new length to scale to.
     * @return This instance for method chaining.
     */
    fun setLengthTo(targetValue: Float): Vec3fMutator

    /**
     * Inverts the x, y and z components of this vector.
     *
     *      x = -x
     *      y = -y
     *      z = -z
     *
     * @return This instance for method chaining.
     */
    fun invert(): Vec3fMutator

    /**
     * Sets the x, y and z components of this vector to their absolute values respectively.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Vec3fMutator

    /**
     * Makes the components more user / reader-friendly.
     *
     * @return This object for method chaining.
     */
    fun shorten(): Vec3fMutator

    /**
     * Sets this vector to be perpendicular to the plane spanned by this and the given vector.
     *
     * Order of calculation is important!
     *
     *      vector1.cross(vector2) != vector2.cross(vector1)!
     *
     * Then the direction will be reversed.
     *
     * @param other The vector with which the cross product gets calculated.
     * @return This instance for method chaining. It will be in a 90 degree angle to this and the given vector.
     */
    infix fun cross(other: Vec3fAccessor): Vec3fMutator

    /**
     * Reflects this vector at the plane specified by the planes normalized normal vector.
     *
     * @param normalizedNormal The normalized normal vector of the plane to reflect of.
     * @return This instance for method chaining. Properly reflected (angle in = angle out).
     */
    fun reflect(normalizedNormal: Vec3fAccessor): Vec3fMutator

    /**
     * Refracts this vector of a given plane specified by its normal vector and the refraction index eta. This vector
     * and the given normal vector must be normalized! Consider using a predefined refraction index for a specific
     * material.
     *
     * @param normalizedNormal The normalized normal vector of the plane to reflect of.
     * @return This instance for method chaining. Properly refracted. Result will be a unit vector.
     */
    fun refract(normalizedNormal: Vec3fAccessor, eta: Double): Vec3fMutator

    /**
     * Rotates this vector by the rotation information in the given [quaternion].
     *
     * @param quaternion The quaternion to rotate by.
     * @return This instance for method chaining.
     */
    fun rotate(quaternion: QuaternionFAccessor): Vec3fMutator

    /**
     * Rotates this vector [angle] degrees around the specified [axis].
     *
     * @param axis The axis to rotate around.
     * @param angle The angle to rotate by.
     * @return This instance for method chaining.
     */
    fun rotate(axis: Vec3fAccessor, angle: Float): Vec3fMutator

    /**
     * Creates an orthonormal basis with [a] and [b] as a basis and calculating the missing axis [c].
     *
     * Preconditions:
     * - Neither [a] or [b] must be normalized.
     * - [c] can be of any content.
     *
     * Possible modifications:
     * - [a] will be normalized, if it wasn't already
     * - [b] will be modified to be orthogonal to [a], if it wasn't already
     * - [c] will be set from calculating the axis orthogonal to [a] and [b]
     *
     * Postconditions:
     * - [a], [b], and [c] are all orthogonal to one-another
     * - [a], [b], and [c] are all normalized
     */
    fun makeOrthonormalBasis(a: Vec3f, b: Vec3f, c: Vec3f)

}
