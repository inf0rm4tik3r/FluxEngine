package de.colibriengine.math.vector.vec3f

/** Vec3fView. */
interface Vec3fView : Vec3fAccessor
