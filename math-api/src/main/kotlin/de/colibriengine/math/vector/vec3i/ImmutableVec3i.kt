package de.colibriengine.math.vector.vec3i

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec3i]. */
interface ImmutableVec3i : Vec3iAccessor, AsMutable<Vec3i> {

    interface Builder {
        fun of(value: Int): ImmutableVec3i
        fun of(x: Int, y: Int, z: Int): ImmutableVec3i
        fun of(vec3iAccessor: Vec3iAccessor): ImmutableVec3i

        fun setX(x: Int): Builder
        fun setY(y: Int): Builder
        fun setZ(z: Int): Builder

        fun set(value: Int): Builder
        fun set(x: Int, y: Int, z: Int): Builder
        fun set(vec3iAccessor: Vec3iAccessor): Builder

        fun build(): ImmutableVec3i
    }

}
