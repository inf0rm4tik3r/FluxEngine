package de.colibriengine.math.vector.vec3i

/** Every [Vec3i] factory implementation should expose these methods. */
interface Vec3iFactory {

    fun acquire(): Vec3i

    fun acquireDirty(): Vec3i

    fun free(vec3i: Vec3i)

    fun immutableVec3iBuilder(): ImmutableVec3i.Builder

    fun zero(): ImmutableVec3i

    fun one(): ImmutableVec3i

    fun unitXAxis(): ImmutableVec3i

    fun unitYAxis(): ImmutableVec3i

    fun unitZAxis(): ImmutableVec3i

    fun negativeUnitXAxis(): ImmutableVec3i

    fun negativeUnitYAxis(): ImmutableVec3i

    fun negativeUnitZAxis(): ImmutableVec3i

}
