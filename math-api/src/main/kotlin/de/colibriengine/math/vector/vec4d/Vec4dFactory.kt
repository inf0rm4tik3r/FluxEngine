package de.colibriengine.math.vector.vec4d

/** Every [Vec4d] factory implementation should expose these methods. */
interface Vec4dFactory {

    fun acquire(): Vec4d

    fun acquireDirty(): Vec4d

    fun free(vec4d: Vec4d)

    fun immutableVec4dBuilder(): ImmutableVec4d.Builder

    fun zero(): ImmutableVec4d

    fun one(): ImmutableVec4d

}
