package de.colibriengine.math.vector.vec4d

import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.math.vector.vec4i.Vec4iAccessor
import de.colibriengine.math.vector.vec4l.Vec4lAccessor

/** Defines all mutating methods of the four dimensional double vector. */
interface Vec4dMutator {

    /** The first component (x) of this vector. */
    var x: Double

    /** The second component (y) of this vector. */
    var y: Double

    /** The third component (z) of this vector. */
    var z: Double

    /** The fourth component (w) of this vector. */
    var w: Double

    /**
     * Sets the x, y, z and w components of this vector to the specified [value].
     *
     * @param value The new value assigned to each component.
     * @return This instance for method chaining.
     */
    fun set(value: Double): Vec4dMutator

    /**
     * Sets the x, y, z and w components of this vector to the specified values.
     *
     * @param x The new value for the x component.
     * @param y The new value for the y component.
     * @param z The new value for the z component.
     * @param w The new value for the w component.
     * @return This instance for method chaining.
     */
    fun set(x: Double, y: Double, z: Double, w: Double): Vec4dMutator

    /**
     * Sets the x, y, z and w components of this vector to the x, y, z and w components of [other] respectively.
     *
     * @param other The other vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec4fAccessor): Vec4dMutator

    /**
     * Sets the x, y, z and w components of this vector to the x, y, z and w components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec4dAccessor): Vec4dMutator

    /**
     * Sets the x, y, z and w components of this vector to the x, y, z and w components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec4iAccessor): Vec4dMutator

    /**
     * Sets the x, y, z and w components of this vector to the x, y, z and w components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec4lAccessor): Vec4dMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively and uses the
     * specified w value for the last component.
     *
     * @param other The vector which component values should be taken / copied.
     * @param w The value for the last component.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3dAccessor, w: Double): Vec4dMutator

    /**
     * Sets the x, y, z and w components of this vector to zero (0).
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Vec4dMutator

    /**
     * Adds the given double value to the x, y, z and w components of this vector respectively.
     *
     * @param addend The value to add.
     * @return This instance for method chaining.
     */
    operator fun plus(addend: Double): Vec4dMutator

    /**
     * Adds the given values (x, y, z and w) to the x, y, z and w components of this vector respectively.
     *
     * @param addendX The value to add to this vectors x component.
     * @param addendY The value to add to this vectors y component.
     * @param addendZ The value to add to this vectors z component.
     * @param addendW The value to add to this vectors w component.
     * @return This instance for method chaining.
     */
    fun plus(addendX: Double, addendY: Double, addendZ: Double, addendW: Double): Vec4dMutator

    /**
     * Adds the values of the x, y, z and w components of the specified [addends] vector to the x, y, z and w components
     * of this vector respectively.
     *
     * @param addends The vector from which to read the addends.
     * @return This instance for method chaining.
     */
    operator fun plus(addends: Vec4dAccessor): Vec4dMutator

    /**
     * Subtracts the given double value from the x, y, z and w components of this vector respectively.
     *
     * @param subtrahend The value to subtract.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahend: Double): Vec4dMutator

    /**
     * Subtracts the given values (x, y, z and w) from the x, y, z and w components of this vector respectively.
     *
     * @param subtrahendX The value to subtract from this vectors x component.
     * @param subtrahendY The value to subtract from this vectors y component.
     * @param subtrahendZ The value to subtract from this vectors z component.
     * @param subtrahendW The value to subtract from this vectors w component.
     * @return This instance for method chaining.
     */
    fun minus(subtrahendX: Double, subtrahendY: Double, subtrahendZ: Double, subtrahendW: Double): Vec4dMutator

    /**
     * Subtracts the values of the x, y, z and w components of the specified [subtrahends] vector from the x, y, z and w
     * components of this vector respectively.
     *
     * @param subtrahends The vector from which to read the subtrahends.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahends: Vec4dAccessor): Vec4dMutator

    /**
     * Multiplies the given double value to the x, y, z and w components of this vector respectively.
     *
     * @param factor The value to multiply with.
     * @return This instance for method chaining.
     */
    operator fun times(factor: Double): Vec4dMutator

    /**
     * Multiplies the given values (x, y, z and w) to the x, y, z and w components of this vector respectively.
     *
     * @param factorX The value to multiply this vectors x component with.
     * @param factorY The value to multiply this vectors y component with.
     * @param factorZ The value to multiply this vectors z component with.
     * @param factorW The value to multiply this vectors w component with.
     * @return This instance for method chaining.
     */
    fun times(factorX: Double, factorY: Double, factorZ: Double, factorW: Double): Vec4dMutator

    /**
     * Multiplies the values of the x, y, z and w components of the specified [factors] vector to the x, y, z and w
     * components of this vector respectively.
     *
     * @param factors The vector from which to read the factors.
     * @return This instance for method chaining.
     */
    operator fun times(factors: Vec4dAccessor): Vec4dMutator

    /**
     * Divides the given double value from the x, y, z and w components of this vector respectively.
     *
     * @param divisor The value to divide by.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisor: Double): Vec4dMutator

    /**
     * Divides the x, y, z and w components of this vector by the given values (x, y, z and w) respectively.
     *
     * @param divisorX The value by which this vectors x component is to be divided.
     * @param divisorY The value by which this vectors y component is to be divided.
     * @param divisorZ The value by which this vectors z component is to be divided.
     * @param divisorW The value by which this vectors w component is to be divided.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    fun div(divisorX: Double, divisorY: Double, divisorZ: Double, divisorW: Double): Vec4dMutator

    /**
     * Divides the x, y, z and w components of this vector by the values of the x, y, z and w components of the
     * specified [divisors] vector respectively.
     *
     * @param divisors The vector from which to read the divisors.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisors: Vec4dAccessor): Vec4dMutator

    /**
     * Normalizes this vector by setting its length to ~1.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun normalize(): Vec4dMutator

    /**
     * Sets the length of this vector to ~ the specified value.
     *
     * @param targetValue The new length to scale to.
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun setLengthTo(targetValue: Double): Vec4dMutator

    /**
     * Inverts the x, y, z and w components of this vector.
     *
     *      x = -x
     *      y = -y
     *      z = -z
     *      w = -w
     *
     * @return This instance for method chaining.
     */
    fun invert(): Vec4dMutator

    /**
     * Sets the x, y, z and w components of this vector to their absolute values respectively.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Vec4dMutator

    /**
     * Makes the components more user / reader-friendly.
     *
     * @return This object for method chaining.
     */
    fun shorten(): Vec4dMutator

}
