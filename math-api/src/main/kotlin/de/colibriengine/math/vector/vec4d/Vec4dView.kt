package de.colibriengine.math.vector.vec4d

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec4dView. */
interface Vec4dView : Vec4dAccessor, AsMutable<Vec4d>, AsImmutable<ImmutableVec4d>
