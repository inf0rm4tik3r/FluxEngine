package de.colibriengine.math.vector.vec4f

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec4f]. */
interface ImmutableVec4f : Vec4fAccessor, AsMutable<Vec4f> {

    interface Builder {
        fun of(value: Float): ImmutableVec4f
        fun of(x: Float, y: Float, z: Float, w: Float): ImmutableVec4f
        fun of(vec4fAccessor: Vec4fAccessor): ImmutableVec4f

        fun setX(x: Float): Builder
        fun setY(y: Float): Builder
        fun setZ(z: Float): Builder
        fun setW(w: Float): Builder

        fun set(value: Float): Builder
        fun set(x: Float, y: Float, z: Float, w: Float): Builder
        fun set(vec4fAccessor: Vec4fAccessor): Builder

        fun build(): ImmutableVec4f
    }

}
