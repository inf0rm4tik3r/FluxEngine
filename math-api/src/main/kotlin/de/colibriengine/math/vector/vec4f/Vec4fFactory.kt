package de.colibriengine.math.vector.vec4f

/** Every [Vec4f] factory implementation should expose these methods. */
interface Vec4fFactory {

    fun acquire(): Vec4f

    fun acquireDirty(): Vec4f

    fun free(vec4f: Vec4f)

    fun immutableVec4fBuilder(): ImmutableVec4f.Builder

    fun zero(): ImmutableVec4f

    fun one(): ImmutableVec4f

}
