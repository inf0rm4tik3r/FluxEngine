package de.colibriengine.math.vector.vec4f

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec4fView. */
interface Vec4fView : Vec4fAccessor, AsMutable<Vec4f>, AsImmutable<ImmutableVec4f>
