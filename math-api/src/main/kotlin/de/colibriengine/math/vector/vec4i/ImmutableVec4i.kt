package de.colibriengine.math.vector.vec4i

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec4i]. */
interface ImmutableVec4i : Vec4iAccessor, AsMutable<Vec4i> {

    interface Builder {
        fun of(value: Int): ImmutableVec4i
        fun of(x: Int, y: Int, z: Int, w: Int): ImmutableVec4i
        fun of(vec4iAccessor: Vec4iAccessor): ImmutableVec4i

        fun setX(x: Int): Builder
        fun setY(y: Int): Builder
        fun setZ(z: Int): Builder
        fun setW(w: Int): Builder

        fun set(value: Int): Builder
        fun set(x: Int, y: Int, z: Int, w: Int): Builder
        fun set(vec4iAccessor: Vec4iAccessor): Builder

        fun build(): ImmutableVec4i
    }

}
