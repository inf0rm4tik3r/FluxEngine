package de.colibriengine.math.vector.vec4i

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec4d.Vec4dAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.math.vector.vec4l.Vec4lAccessor

/** Provides the general functionality of a four dimensional vector in int precision. */
interface Vec4i : Vec4iAccessor, Vec4iMutator, Viewable<Vec4iView>, Copyable<Vec4i>, AsImmutable<ImmutableVec4i> {

    override var x: Int
    override var y: Int
    override var z: Int
    override var w: Int

    /*
    override fun setX(x: Int): Vec4i
    override fun setY(y: Int): Vec4i
    override fun setZ(z: Int): Vec4i
    override fun setW(w: Int): Vec4i
    */

    override fun set(value: Int): Vec4i
    override fun set(x: Int, y: Int, z: Int, w: Int): Vec4i
    override fun set(other: Vec4fAccessor): Vec4i
    override fun set(other: Vec4dAccessor): Vec4i
    override fun set(other: Vec4iAccessor): Vec4i
    override fun set(other: Vec4lAccessor): Vec4i
    override fun set(other: Vec3iAccessor, w: Int): Vec4i

    override fun initZero(): Vec4i

    override operator fun plus(addend: Int): Vec4i
    override operator fun plus(addends: Vec4iAccessor): Vec4i
    override fun plus(addendX: Int, addendY: Int, addendZ: Int, addendW: Int): Vec4i

    override operator fun minus(subtrahend: Int): Vec4i
    override operator fun minus(subtrahends: Vec4iAccessor): Vec4i
    override fun minus(subtrahendX: Int, subtrahendY: Int, subtrahendZ: Int, subtrahendW: Int): Vec4i

    override operator fun times(factor: Int): Vec4i
    override operator fun times(factors: Vec4iAccessor): Vec4i
    override fun times(factorX: Int, factorY: Int, factorZ: Int, factorW: Int): Vec4i

    override operator fun div(divisor: Int): Vec4i
    override operator fun div(divisors: Vec4iAccessor): Vec4i
    override fun div(divisorX: Int, divisorY: Int, divisorZ: Int, divisorW: Int): Vec4i

    override fun normalize(): Vec4i
    override fun setLengthTo(targetValue: Double): Vec4i
    override fun invert(): Vec4i
    override fun abs(): Vec4i

    companion object {
        /** The number of int values used to represent this vector. */
        const val INTS = 4

        /** The number of bytes used to represent this vector object. */
        const val BYTES = INTS * Integer.BYTES
    }

}
