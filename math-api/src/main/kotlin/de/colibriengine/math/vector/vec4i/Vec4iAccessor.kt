package de.colibriengine.math.vector.vec4i

import de.colibriengine.buffers.BufferStorable
import de.colibriengine.math.vector.vec3i.Vec3i

/** Defines all non-mutable methods of the four dimensional int vector. */
interface Vec4iAccessor : BufferStorable {

    /** The first component (x) of this vector. */
    val x: Int

    /** The second component (y) of this vector. */
    val y: Int

    /** The third component (z) of this vector. */
    val z: Int

    /** The fourth component (w) of this vector. */
    val w: Int

    /** Returns [x], [y] and [z] in a new [Vec3i]. */
    fun xyz(): Vec3i

    /** Returns [x], [y] and [w] in a new [Vec3i]. */
    fun xyw(): Vec3i

    /** Returns [x], [z] and [w] in a new [Vec3i]. */
    fun xzw(): Vec3i

    /** Returns [y], [z] and [w] in a new [Vec3i]. */
    fun yzw(): Vec3i

    /** Returns true if at least one component is zero. */
    fun hasZeroComponent(): Boolean

    /** Returns the length of this vector. */
    fun length(): Double

    /** Returns the squared length of this vector. Less computationally expensive then [length]. */
    fun squaredLength(): Double

    /** Returns the lowest component of this vector. */
    fun min(): Int

    /** Returns the highest component of this vector. */
    fun max(): Int

    /**
     * Performs a linear interpolation between this and the target vector.
     *
     * @param target The target vector.
     * @param lerpFactor Amount of interpolation. Use values in the [0,...,1] (both inclusive) range.
     * @return The interpolated vector.
     * @throws IllegalArgumentException If the specified [lerpFactor] is either negative or greater than 1.
     */
    fun lerp(target: Vec4iAccessor, lerpFactor: Int): Vec4i

    /**
     * Performs a linear interpolation between this and the target vector.
     *
     * @param target The target vector.
     * @param lerpFactor Amount of interpolation. Any value can be used.
     * @return The interpolated vector.
     */
    fun lerpFree(target: Vec4iAccessor, lerpFactor: Int): Vec4i

    /**
     * Returns a byte array which represents this vector.
     *
     * @return A byte array. Its length is equal to the dimension of this vector. The vectors components are stored in
     *     the ordinary ordering of the vectors components (first to last).
     */
    fun bytes(): ByteArray

    /**
     * Provides a shortened string representation of this vector. Decimal places of each component are restricted. Maybe
     * more reader-friendly.
     *
     * @return A descriptive string of this vector which contains its components.
     */
    fun toFormattedString(): String

}
