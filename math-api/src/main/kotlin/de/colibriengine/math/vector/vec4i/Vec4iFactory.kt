package de.colibriengine.math.vector.vec4i

/** Every [Vec4i] factory implementation should expose these methods. */
interface Vec4iFactory {

    fun acquire(): Vec4i

    fun acquireDirty(): Vec4i

    fun free(vec4i: Vec4i)

    fun immutableVec4iBuilder(): ImmutableVec4i.Builder

    fun zero(): ImmutableVec4i

    fun one(): ImmutableVec4i

}
