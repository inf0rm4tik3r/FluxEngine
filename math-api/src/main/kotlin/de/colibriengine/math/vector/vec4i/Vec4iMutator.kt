package de.colibriengine.math.vector.vec4i

import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec4d.Vec4dAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.math.vector.vec4l.Vec4lAccessor

/** Defines all mutating methods of the four dimensional int vector. */
interface Vec4iMutator {

    /** The first component (x) of this vector. */
    var x: Int

    /** The second component (y) of this vector. */
    var y: Int

    /** The third component (z) of this vector. */
    var z: Int

    /** The fourth component (w) of this vector. */
    var w: Int

    /**
     * Sets the x, y, z and w components of this vector to the specified [value].
     *
     * @param value The new value assigned to each component.
     * @return This instance for method chaining.
     */
    fun set(value: Int): Vec4iMutator

    /**
     * Sets the x, y, z and w components of this vector to the specified values.
     *
     * @param x The new value for the x component.
     * @param y The new value for the y component.
     * @param z The new value for the z component.
     * @param w The new value for the w component.
     * @return This instance for method chaining.
     */
    fun set(x: Int, y: Int, z: Int, w: Int): Vec4iMutator

    /**
     * Sets the x, y, z and w components of this vector to the x, y, z and w components of [other] respectively.
     *
     * @param other The other vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec4fAccessor): Vec4iMutator

    /**
     * Sets the x, y, z and w components of this vector to the x, y, z and w components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec4dAccessor): Vec4iMutator

    /**
     * Sets the x, y, z and w components of this vector to the x, y, z and w components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec4iAccessor): Vec4iMutator

    /**
     * Sets the x, y, z and w components of this vector to the x, y, z and w components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec4lAccessor): Vec4iMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively and uses the
     * specified w value for the last component.
     *
     * @param other The vector which component values should be taken / copied.
     * @param w The value for the last component.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3iAccessor, w: Int): Vec4iMutator

    /**
     * Sets the x, y, z and w components of this vector to zero (0).
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Vec4iMutator

    /**
     * Adds the given int value to the x, y, z and w components of this vector respectively.
     *
     * @param addend The value to add.
     * @return This instance for method chaining.
     */
    operator fun plus(addend: Int): Vec4iMutator

    /**
     * Adds the given values (x, y, z and w) to the x, y, z and w components of this vector respectively.
     *
     * @param addendX The value to add to this vectors x component.
     * @param addendY The value to add to this vectors y component.
     * @param addendZ The value to add to this vectors z component.
     * @param addendW The value to add to this vectors w component.
     * @return This instance for method chaining.
     */
    fun plus(addendX: Int, addendY: Int, addendZ: Int, addendW: Int): Vec4iMutator

    /**
     * Adds the values of the x, y, z and w components of the specified [addends] vector to the x, y, z and w components
     * of this vector respectively.
     *
     * @param addends The vector from which to read the addends.
     * @return This instance for method chaining.
     */
    operator fun plus(addends: Vec4iAccessor): Vec4iMutator

    /**
     * Subtracts the given int value from the x, y, z and w components of this vector respectively.
     *
     * @param subtrahend The value to subtract.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahend: Int): Vec4iMutator

    /**
     * Subtracts the given values (x, y, z and w) from the x, y, z and w components of this vector respectively.
     *
     * @param subtrahendX The value to subtract from this vectors x component.
     * @param subtrahendY The value to subtract from this vectors y component.
     * @param subtrahendZ The value to subtract from this vectors z component.
     * @param subtrahendW The value to subtract from this vectors w component.
     * @return This instance for method chaining.
     */
    fun minus(subtrahendX: Int, subtrahendY: Int, subtrahendZ: Int, subtrahendW: Int): Vec4iMutator

    /**
     * Subtracts the values of the x, y, z and w components of the specified [subtrahends] vector from the x, y, z and w
     * components of this vector respectively.
     *
     * @param subtrahends The vector from which to read the subtrahends.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahends: Vec4iAccessor): Vec4iMutator

    /**
     * Multiplies the given int value to the x, y, z and w components of this vector respectively.
     *
     * @param factor The value to multiply with.
     * @return This instance for method chaining.
     */
    operator fun times(factor: Int): Vec4iMutator

    /**
     * Multiplies the given values (x, y, z and w) to the x, y, z and w components of this vector respectively.
     *
     * @param factorX The value to multiply this vectors x component with.
     * @param factorY The value to multiply this vectors y component with.
     * @param factorZ The value to multiply this vectors z component with.
     * @param factorW The value to multiply this vectors w component with.
     * @return This instance for method chaining.
     */
    fun times(factorX: Int, factorY: Int, factorZ: Int, factorW: Int): Vec4iMutator

    /**
     * Multiplies the values of the x, y, z and w components of the specified [factors] vector to the x, y, z and w
     * components of this vector respectively.
     *
     * @param factors The vector from which to read the factors.
     * @return This instance for method chaining.
     */
    operator fun times(factors: Vec4iAccessor): Vec4iMutator

    /**
     * Divides the given int value from the x, y, z and w components of this vector respectively.
     *
     * @param divisor The value to divide by.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisor: Int): Vec4iMutator

    /**
     * Divides the x, y, z and w components of this vector by the given values (x, y, z and w) respectively.
     *
     * @param divisorX The value by which this vectors x component is to be divided.
     * @param divisorY The value by which this vectors y component is to be divided.
     * @param divisorZ The value by which this vectors z component is to be divided.
     * @param divisorW The value by which this vectors w component is to be divided.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    fun div(divisorX: Int, divisorY: Int, divisorZ: Int, divisorW: Int): Vec4iMutator

    /**
     * Divides the x, y, z and w components of this vector by the values of the x, y, z and w components of the
     * specified [divisors] vector respectively.
     *
     * @param divisors The vector from which to read the divisors.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisors: Vec4iAccessor): Vec4iMutator

    /**
     * Normalizes this vector by setting its length to ~1.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun normalize(): Vec4iMutator

    /**
     * Sets the length of this vector to ~ the specified value.
     *
     * @param targetValue The new length to scale to.
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun setLengthTo(targetValue: Double): Vec4iMutator

    /**
     * Inverts the x, y, z and w components of this vector.
     *
     *      x = -x
     *      y = -y
     *      z = -z
     *      w = -w
     *
     * @return This instance for method chaining.
     */
    fun invert(): Vec4iMutator

    /**
     * Sets the x, y, z and w components of this vector to their absolute values respectively.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Vec4iMutator

}
