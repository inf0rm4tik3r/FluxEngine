package de.colibriengine.math.vector.vec4l

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec4l]. */
interface ImmutableVec4l : Vec4lAccessor, AsMutable<Vec4l> {

    interface Builder {
        fun of(value: Long): ImmutableVec4l
        fun of(x: Long, y: Long, z: Long, w: Long): ImmutableVec4l
        fun of(vec4lAccessor: Vec4lAccessor): ImmutableVec4l

        fun setX(x: Long): Builder
        fun setY(y: Long): Builder
        fun setZ(z: Long): Builder
        fun setW(w: Long): Builder

        fun set(value: Long): Builder
        fun set(x: Long, y: Long, z: Long, w: Long): Builder
        fun set(vec4lAccessor: Vec4lAccessor): Builder

        fun build(): ImmutableVec4l
    }

}
