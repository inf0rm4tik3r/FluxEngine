package de.colibriengine.math.vector.vec4l

/** Every [Vec4l] factory implementation should expose these methods. */
interface Vec4lFactory {

    fun acquire(): Vec4l

    fun acquireDirty(): Vec4l

    fun free(vec4l: Vec4l)

    fun immutableVec4lBuilder(): ImmutableVec4l.Builder

    fun zero(): ImmutableVec4l

    fun one(): ImmutableVec4l

}
