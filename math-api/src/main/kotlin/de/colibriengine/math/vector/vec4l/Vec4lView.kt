package de.colibriengine.math.vector.vec4l

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec4lView. */
interface Vec4lView : Vec4lAccessor, AsMutable<Vec4l>, AsImmutable<ImmutableVec4l>
