package de.colibriengine.math.curve

import de.colibriengine.math.vector.vec3f.Vec3f

/**
 * Represents a bezier curve formed by a start and end point using any number of control points.
 * - 1 control point (+start and end) -> quadratic bezier curve
 * - 2 control points (+start and end) -> cubic bezier curve
 * - 3 control points (+start and end) -> quartic bezier curve
 * - 4 control points (+start and end) -> quintic bezier curve
 *
 * @since ColibriEngine 0.0.6.0
 */
class BezierCurve(
    /** Control points of this curve. */
    private val controlPoints: Array<Vec3f>
) {

    /**
     * Interpolates between the start and end point by using the specified control points.
     *
     * @param interpolation How far on the curve? Only use values in [0,...,1] range.
     * @return A point on the curve. Closer at the end point the higher the amount of interpolation is.
     * @throws IllegalArgumentException if the given amount of interpolation is not in [0,...,1] range.
     * @see <a href="https://en.wikipedia.org/wiki/B%C3%A9zier_curve">Wikipedia: Bezier curve</a>
     */
    fun pointOnCurve(interpolation: Float): Vec3f {
        // lerpFree() will not check for an illegal argument.
        require(!(interpolation < 0 || interpolation > 1)) { "BezierCurve: Parameter has to be in [0,...,1] range!" }
        return recursivePointFinding(
            controlPoints.copyOf(),
            controlPoints.size,
            interpolation
        )[0]
    }

    /**
     * Recursively interpolates between all given control points to find the point on the bezier curve. Works on the
     * array that gets handed to the method. Therefore call this method with a copy of the control point array.
     *
     * @param controlPoints Control points to calculate.
     * @param order How many control points are to process in the current iteration?
     * @param interpolation Amount of interpolation over the curve.
     * @return The given "in" array. Its first element is now the point on the curve! Other elements are filled with the
     *     results of calculations of previous recursive iterations.
     */
    private fun recursivePointFinding(controlPoints: Array<Vec3f>, order: Int, interpolation: Float): Array<Vec3f> {
        val newOrder = order - 1

        // Calculate (old)order - 1 new control points.
        for (i in 0 until newOrder) {
            controlPoints[i] = controlPoints[i].lerpFree(controlPoints[i + 1], interpolation)
        }

        return if (newOrder == 1) {
            controlPoints
        } else {
            recursivePointFinding(controlPoints, newOrder, interpolation)
        }
    }

}
