package de.colibriengine.math.matrix.mat2d

import de.colibriengine.math.vector.vec2d.Vec2d
import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

/**
 * StdImmutableMat2d.
 *
 * 
 */
class StdImmutableMat2d private constructor(
    m00: Double, m01: Double,
    m10: Double, m11: Double
) : ImmutableMat2d {

    private val mutableAccessor: Mat2dAccessor = StdMat2d(m00, m01, m10, m11)

    override val m00: Double = mutableAccessor.m00
    override val m01: Double = mutableAccessor.m01
    override val m10: Double = mutableAccessor.m10
    override val m11: Double = mutableAccessor.m11

    override fun get(row: Int, column: Int): Double = mutableAccessor.get(row, column)

    override fun getRow(row: Int, storeIn: Vec2d): Vec2d = mutableAccessor.getRow(row, storeIn)
    override fun getRow1(storeIn: Vec2d): Vec2d = mutableAccessor.getRow1(storeIn)
    override fun getRow2(storeIn: Vec2d): Vec2d = mutableAccessor.getRow2(storeIn)

    override fun getColumn(column: Int, storeIn: Vec2d): Vec2d = mutableAccessor.getColumn(column, storeIn)
    override fun getColumn1(storeIn: Vec2d): Vec2d = mutableAccessor.getColumn1(storeIn)
    override fun getColumn2(storeIn: Vec2d): Vec2d = mutableAccessor.getColumn2(storeIn)

    override fun getMajorDiagonal(storeIn: Vec2d): Vec2d = mutableAccessor.getMajorDiagonal(storeIn)

    override fun asArray(): Array<out DoubleArray> = mutableAccessor.asArray()
    override fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray> = mutableAccessor.asArray(storeIn)

    override fun transform(vector: Vec2d): Vec2d = mutableAccessor.transform(vector)

    override fun determinant(): Double = mutableAccessor.determinant()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString()

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableMat2d.Builder {
        private var m00: Double = 0.0
        private var m01: Double = 0.0
        private var m10: Double = 0.0
        private var m11: Double = 0.0

        override fun of(value: Double): ImmutableMat2d = set(value).build()

        override fun of(m00: Double, m01: Double, m10: Double, m11: Double): ImmutableMat2d =
            set(m00, m01, m10, m11).build()

        override fun of(other: Mat2dAccessor): ImmutableMat2d = set(other).build()

        override fun ofColumns(column1: Vec2dAccessor, column2: Vec2dAccessor): ImmutableMat2d =
            setColumn1(column1).setColumn2(column2).build()

        override fun ofRows(row1: Vec2dAccessor, row2: Vec2dAccessor): ImmutableMat2d =
            setRow1(row1).setRow2(row2).build()

        override fun setM00(m00: Double): ImmutableMat2d.Builder {
            this.m00 = m00
            return this
        }

        override fun setM01(m01: Double): ImmutableMat2d.Builder {
            this.m01 = m01
            return this
        }

        override fun setM10(m10: Double): ImmutableMat2d.Builder {
            this.m10 = m10
            return this
        }

        override fun setM11(m11: Double): ImmutableMat2d.Builder {
            this.m11 = m11
            return this
        }

        override fun set(value: Double): ImmutableMat2d.Builder {
            m00 = value
            m01 = value
            m10 = value
            m11 = value
            return this
        }

        override fun set(m00: Double, m01: Double, m10: Double, m11: Double): ImmutableMat2d.Builder {
            this.m00 = m00
            this.m01 = m01
            this.m10 = m10
            this.m11 = m11
            return this
        }

        override fun set(other: Mat2dAccessor): ImmutableMat2d.Builder {
            m00 = other.m00
            m01 = other.m01
            m10 = other.m10
            m11 = other.m11
            return this
        }

        override fun setColumn1(column1: Vec2dAccessor): ImmutableMat2d.Builder {
            m00 = column1.x
            m10 = column1.y
            return this
        }

        override fun setColumn2(column2: Vec2dAccessor): ImmutableMat2d.Builder {
            m01 = column2.x
            m11 = column2.y
            return this
        }

        override fun setRow1(row1: Vec2dAccessor): ImmutableMat2d.Builder {
            m00 = row1.x
            m01 = row1.y
            return this
        }

        override fun setRow2(row2: Vec2dAccessor): ImmutableMat2d.Builder {
            m10 = row2.x
            m11 = row2.y
            return this
        }

        override fun build(): ImmutableMat2d = StdImmutableMat2d(m00, m01, m10, m11)
    }

    companion object {
        fun builder(): ImmutableMat2d.Builder = Builder()
    }

}

inline fun buildImmutableMat2d(builderAction: ImmutableMat2d.Builder.() -> Unit): ImmutableMat2d {
    return StdImmutableMat2d.builder().apply(builderAction).build()
}
