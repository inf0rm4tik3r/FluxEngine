package de.colibriengine.math.matrix.mat2d

import de.colibriengine.math.matrix.mat2f.Mat2fAccessor
import de.colibriengine.math.vector.vec2d.Vec2d
import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.absoluteValue
import kotlin.math.max

/**
 * Mutable 2x2 matrix in double precision.
 *
 * @version 0.0.0.6
 * @since ColibriEngine 0.0.7.2
 */
@Suppress("DuplicatedCode")
class StdMat2d(
    /* First row */
    override var m00: Double = 0.0,
    override var m01: Double = 0.0,
    /* Second row */
    override var m10: Double = 0.0,
    override var m11: Double = 0.0
) : Mat2d {

    private var _view: Mat2dView? = null
    override val view: Mat2dView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    override fun initZero(): Mat2d = set(0.0)

    override fun initIdentity(): Mat2d {
        m10 = 0.0
        m01 = m10
        m11 = 1.0
        m00 = m11
        return this
    }

    override fun set(value: Double): Mat2d {
        m11 = value
        m10 = m11
        m01 = m10
        m00 = m01
        return this
    }

    override fun set(
        m00: Double, m01: Double,
        m10: Double, m11: Double
    ): Mat2d {
        this.m00 = m00
        this.m01 = m01
        this.m10 = m10
        this.m11 = m11
        return this
    }

    override fun set(other: Mat2fAccessor): Mat2d {
        m00 = other.m00.toDouble()
        m01 = other.m01.toDouble()
        m10 = other.m10.toDouble()
        m11 = other.m11.toDouble()
        return this
    }

    override fun set(other: Mat2dAccessor): Mat2d {
        m00 = other.m00
        m01 = other.m01
        m10 = other.m10
        m11 = other.m11
        return this
    }

    override fun set(matrixData: Array<DoubleArray>): Mat2d {
        require(!(matrixData.size != Mat2d.ROW_AMT || matrixData[0].size != Mat2d.COLUMN_AMT)) {
            "Array dimensions do not match. Copy not possible"
        }
        m00 = matrixData[0][0]
        m01 = matrixData[0][1]
        m10 = matrixData[1][0]
        m11 = matrixData[1][1]
        return this
    }

    override fun set(row: Int, column: Int, value: Double): Mat2d {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (row) {
            0 -> when (column) {
                0 -> {
                    m00 = value
                    return this
                }
                1 -> {
                    m01 = value
                    return this
                }
            }
            1 -> when (column) {
                0 -> {
                    m10 = value
                    return this
                }
                1 -> {
                    m11 = value
                    return this
                }
            }
        }
        throw IllegalArgumentException(
            "Row($row) and column($column) indices were not both in range [0..1]!"
        )
    }

    override fun setRow(row: Int, x: Double, y: Double): Mat2d {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        when (row) {
            0 -> {
                m00 = x
                m01 = y
                return this
            }
            1 -> {
                m10 = x
                m11 = y
                return this
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..1]!")
    }

    override fun setRow1(x: Double, y: Double): Mat2d {
        m00 = x
        m01 = y
        return this
    }

    override fun setRow2(x: Double, y: Double): Mat2d {
        m10 = x
        m11 = y
        return this
    }

    override fun setRow(row: Int, vector: Vec2dAccessor): Mat2d {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        when (row) {
            0 -> {
                m00 = vector.x
                m01 = vector.y
                return this
            }
            1 -> {
                m10 = vector.x
                m11 = vector.y
                return this
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..1]!")
    }

    override fun setRow1(vector: Vec2dAccessor): Mat2d {
        m00 = vector.x
        m01 = vector.y
        return this
    }

    override fun setRow2(vector: Vec2dAccessor): Mat2d {
        m10 = vector.x
        m11 = vector.y
        return this
    }

    override fun setColumn(column: Int, x: Double, y: Double): Mat2d {
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (column) {
            0 -> {
                m00 = x
                m10 = y
                return this
            }
            1 -> {
                m01 = x
                m11 = y
                return this
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..1]!")
    }

    override fun setColumn1(x: Double, y: Double): Mat2d {
        m00 = x
        m10 = y
        return this
    }

    override fun setColumn2(x: Double, y: Double): Mat2d {
        m01 = x
        m11 = y
        return this
    }

    override fun setColumn(column: Int, vector: Vec2dAccessor): Mat2d {
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (column) {
            0 -> {
                m00 = vector.x
                m10 = vector.y
                return this
            }
            1 -> {
                m01 = vector.x
                m11 = vector.y
                return this
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..1]!")
    }

    override fun setColumn1(vector: Vec2dAccessor): Mat2d {
        m00 = vector.x
        m10 = vector.y
        return this
    }

    override fun setColumn2(vector: Vec2dAccessor): Mat2d {
        m01 = vector.x
        m11 = vector.y
        return this
    }

    override fun times(value: Double): Mat2d {
        m00 *= value
        m01 *= value
        m10 *= value
        m11 *= value
        return this
    }

    override fun times(other: Mat2dAccessor): Mat2d {
        assert(this !== other) { "Cannot multiply with itself!" }
        // First new row.
        var r0: Double = m00 * other.m00 + m01 * other.m10
        var r1: Double = m00 * other.m01 + m01 * other.m11
        m00 = r0
        m01 = r1
        // Second new row.
        r0 = m10 * other.m00 + m11 * other.m10
        r1 = m10 * other.m01 + m11 * other.m11
        m10 = r0
        m11 = r1
        return this
    }

    override fun mulSelf(): Mat2d {
        val r00 = m00 * m00 + m01 * m10
        val r01 = m00 * m01 + m01 * m11
        val r10 = m10 * m00 + m11 * m10
        val r11 = m10 * m01 + m11 * m11
        m00 = r00
        m01 = r01
        m10 = r10
        m11 = r11
        return this
    }

    override fun transform(vector: Vec2d): Vec2d {
        return vector.set(
            m00 * vector.x + m01 * vector.y,
            m10 * vector.x + m11 * vector.y
        )
    }

    override fun plus(value: Double): Mat2d {
        m00 += value
        m01 += value
        m10 += value
        m11 += value
        return this
    }

    override fun plus(other: Mat2dAccessor): Mat2d {
        m00 += other.m00
        m01 += other.m01
        m10 += other.m10
        m11 += other.m11
        return this
    }

    override fun minus(value: Double): Mat2d {
        m00 -= value
        m01 -= value
        m10 -= value
        m11 -= value
        return this
    }

    override fun minus(other: Mat2dAccessor): Mat2d {
        m00 -= other.m00
        m01 -= other.m01
        m10 -= other.m10
        m11 -= other.m11
        return this
    }

    override fun inverse(): Mat2d {
        val det = determinant()
        check(det != 0.0) { "Determinant is zero. Unable to calculate the inverse matrix!" }
        val oneOverDet = 1.0f / det
        return set(
            oneOverDet * m11, -oneOverDet * m01,
            -oneOverDet * m10, oneOverDet * m00
        )
    }

    override fun transpose(): Mat2d {
        // Swap 10 with 01.
        val temp = m10
        m10 = m01
        m01 = temp
        return this
    }

    override fun pow(exponent: Int): Mat2d {
        require(exponent >= -1) { "Exponents below -1 are not supported. Given: $exponent" }
        return when (exponent) {
            -1 -> inverse()
            0 -> initIdentity()
            1 -> this
            else -> {
                for (i in 1 until exponent) {
                    mulSelf()
                }
                this
            }
        }
    }

    override fun abs(): Mat2d {
        m00 = m00.absoluteValue
        m01 = m01.absoluteValue
        m10 = m10.absoluteValue
        m11 = m11.absoluteValue
        return this
    }

    override fun shorten(): Mat2d {
        m00 = m00.toFourDecimalPlaces()
        m01 = m01.toFourDecimalPlaces()
        m10 = m10.toFourDecimalPlaces()
        m11 = m11.toFourDecimalPlaces()
        return this
    }

    override fun get(row: Int, column: Int): Double {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (row) {
            0 -> when (column) {
                0 -> return m00
                1 -> return m01
            }
            1 -> when (column) {
                0 -> return m10
                1 -> return m11
            }
        }
        throw IllegalArgumentException(
            "Row($row) and column($column) indices were not both in range [0..1]!"
        )
    }

    override fun getRow(row: Int, storeIn: Vec2d): Vec2d {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        when (row) {
            0 -> return storeIn.set(m00, m01)
            1 -> return storeIn.set(m10, m11)
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..1]!")
    }

    override fun getRow1(storeIn: Vec2d): Vec2d = storeIn.set(m00, m01)

    override fun getRow2(storeIn: Vec2d): Vec2d = storeIn.set(m10, m11)

    override fun getColumn(column: Int, storeIn: Vec2d): Vec2d {
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (column) {
            0 -> return storeIn.set(m00, m10)
            1 -> return storeIn.set(m01, m11)
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..1]!")
    }

    override fun getColumn1(storeIn: Vec2d): Vec2d = storeIn.set(m00, m10)

    override fun getColumn2(storeIn: Vec2d): Vec2d = storeIn.set(m01, m11)

    override fun getMajorDiagonal(storeIn: Vec2d): Vec2d = storeIn.set(m00, m11)

    override fun asArray(): Array<out DoubleArray> {
        return arrayOf(
            doubleArrayOf(m00, m01),
            doubleArrayOf(m10, m11)
        )
    }

    override fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray> {
        require(storeIn.size == Mat2d.ROW_AMT && storeIn[0].size == Mat2d.COLUMN_AMT) {
            "Given data array is not of size 2x2!"
        }
        storeIn[0][0] = m00
        storeIn[0][1] = m01
        storeIn[1][0] = m10
        storeIn[1][1] = m11
        return storeIn
    }

    override fun determinant(): Double = m00 * m11 - m10 * m01

    override fun hasZeroComponent(): Boolean = m00 == 0.0 || m01 == 0.0 || m10 == 0.0 || m11 == 0.0

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Mat2d.BYTES)).array()
    }

    override fun toFormattedString(): String = toString()

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        return buffer
            .putDouble(m00).putDouble(m01)
            .putDouble(m10).putDouble(m11)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        return buffer
            .put(m00.toFloat()).put(m01.toFloat())
            .put(m10.toFloat()).put(m11.toFloat())
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        return buffer
            .put(m00).put(m01)
            .put(m10).put(m11)
    }

    override fun setM00(value: Double): Mat2d {
        m00 = value
        return this
    }

    override fun setM01(value: Double): Mat2d {
        m01 = value
        return this
    }

    override fun setM10(value: Double): Mat2d {
        m10 = value
        return this
    }

    override fun setM11(value: Double): Mat2d {
        m11 = value
        return this
    }

    override fun hashCode(): Int {
        var result = 17
        result = 31 * result + m00.hashCode()
        result = 31 * result + m01.hashCode()
        result = 31 * result + m10.hashCode()
        result = 31 * result + m11.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Mat2d -> false
            other === this -> true
            else -> m00 == other.m00
                && m01 == other.m01
                && m10 == other.m10
                && m11 == other.m11
        }
    }

    override fun toString(): String = buildString {
        append("Mat2d ( ")
        appendValue(this, m00)
        append(" | ")
        appendValue(this, m01)
        append(" )\n      ( ")
        appendValue(this, m10)
        append(" | ")
        appendValue(this, m11)
        append(" )")
    }

    private fun appendValue(builder: StringBuilder, value: Double) {
        // Add a whitespace if the current value is not negative.
        // Otherwise we will see a "-" sign.
        builder.append(if (value > 0) " " + valueAsString(value) else valueAsString(value))
    }

    private fun valueAsString(value: Double): String = buildString {
        append(value.toFourDecimalPlaces())
        append(" ".repeat(max(0, 4 - toString().split('.')[1].length)))
    }

    /**
     * Mat2d viewer, granting read-only access to its parent data.
     *
     * @author Lukas Potthast
     * @since ColibriEngine 0.0.7.2
     */
    inner class Viewer : Mat2dView {
        override val m00: Double = this@StdMat2d.m00
        override val m01: Double = this@StdMat2d.m01
        override val m10: Double = this@StdMat2d.m10
        override val m11: Double = this@StdMat2d.m11

        override fun get(row: Int, column: Int): Double = this@StdMat2d.get(row, column)

        override fun getRow(row: Int, storeIn: Vec2d): Vec2d = this@StdMat2d.getRow(row, storeIn)
        override fun getRow1(storeIn: Vec2d): Vec2d = this@StdMat2d.getRow1(storeIn)
        override fun getRow2(storeIn: Vec2d): Vec2d = this@StdMat2d.getRow2(storeIn)

        override fun getColumn(column: Int, storeIn: Vec2d): Vec2d = this@StdMat2d.getColumn(column, storeIn)
        override fun getColumn1(storeIn: Vec2d): Vec2d = this@StdMat2d.getColumn1(storeIn)
        override fun getColumn2(storeIn: Vec2d): Vec2d = this@StdMat2d.getColumn2(storeIn)

        override fun getMajorDiagonal(storeIn: Vec2d): Vec2d = this@StdMat2d.getMajorDiagonal(storeIn)

        override fun asArray(): Array<out DoubleArray> = this@StdMat2d.asArray()
        override fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray> = this@StdMat2d.asArray(storeIn)

        override fun transform(vector: Vec2d): Vec2d = this@StdMat2d.transform(vector)

        override fun determinant(): Double = this@StdMat2d.determinant()

        override fun hasZeroComponent(): Boolean = this@StdMat2d.hasZeroComponent()

        override fun bytes(): ByteArray = this@StdMat2d.bytes()

        override fun toFormattedString(): String = this@StdMat2d.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdMat2d.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdMat2d.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdMat2d.storeIn(buffer)
    }

    companion object : Mat2dStaticOps {
        override fun determinantOf(m00: Double, m01: Double, m10: Double, m11: Double): Double {
            return m00 * m11 - m10 * m01
        }
    }

}
