package de.colibriengine.math.matrix.mat2d

/**
 * StdMat2dFactory.
 *
 *
 */
class StdMat2dFactory : Mat2dFactory {

    private val pool: StdMat2dPool = StdMat2dPool()

    override fun acquire(): Mat2d = pool.acquire()

    override fun acquireDirty(): Mat2d = pool.acquireDirty()

    override fun free(mat2d: Mat2d) = pool.free(mat2d)

    override fun immutableMat2dBuilder(): ImmutableMat2d.Builder = StdImmutableMat2d.builder()

    override fun zero(): ImmutableMat2d = ZERO

    override fun one(): ImmutableMat2d = ONE

    override fun identity(): ImmutableMat2d = IDENTITY

    companion object {
        val ZERO = StdImmutableMat2d.builder().of(0.0)
        val ONE = StdImmutableMat2d.builder().of(1.0)
        val IDENTITY = StdImmutableMat2d.builder().set(
            1.0, 0.0,
            0.0, 1.0
        ).build()
    }

}
