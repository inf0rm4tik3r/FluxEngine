package de.colibriengine.math.matrix.mat2f

import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableMat2f private constructor(
    m00: Float, m01: Float,
    m10: Float, m11: Float
) : ImmutableMat2f {

    private val mutableAccessor: Mat2fAccessor = StdMat2f(m00, m01, m10, m11)

    override val m00: Float = mutableAccessor.m00
    override val m01: Float = mutableAccessor.m01
    override val m10: Float = mutableAccessor.m10
    override val m11: Float = mutableAccessor.m11

    override operator fun get(row: Int, column: Int): Float = mutableAccessor[row, column]

    override fun getRow(row: Int, storeIn: Vec2f): Vec2f = mutableAccessor.getRow(row, storeIn)
    override fun getRow1(storeIn: Vec2f): Vec2f = mutableAccessor.getRow1(storeIn)
    override fun getRow2(storeIn: Vec2f): Vec2f = mutableAccessor.getRow2(storeIn)

    override fun getColumn(column: Int, storeIn: Vec2f): Vec2f = mutableAccessor.getColumn(column, storeIn)
    override fun getColumn1(storeIn: Vec2f): Vec2f = mutableAccessor.getColumn1(storeIn)
    override fun getColumn2(storeIn: Vec2f): Vec2f = mutableAccessor.getColumn2(storeIn)

    override fun getMajorDiagonal(storeIn: Vec2f): Vec2f = mutableAccessor.getMajorDiagonal(storeIn)

    override fun asArray(): Array<out FloatArray> = mutableAccessor.asArray()
    override fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray> = mutableAccessor.asArray(storeIn)

    override fun transform(vector: Vec2f): Vec2f = mutableAccessor.transform(vector)

    override fun determinant(): Float = mutableAccessor.determinant()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString()

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableMat2f.Builder {
        private var m00: Float = 0.0f
        private var m01: Float = 0.0f
        private var m10: Float = 0.0f
        private var m11: Float = 0.0f

        override fun of(value: Float): ImmutableMat2f = set(value).build()

        override fun of(m00: Float, m01: Float, m10: Float, m11: Float): ImmutableMat2f =
            set(m00, m01, m10, m11).build()

        override fun of(other: Mat2fAccessor): ImmutableMat2f = set(other).build()

        override fun ofColumns(column1: Vec2fAccessor, column2: Vec2fAccessor): ImmutableMat2f =
            setColumn1(column1).setColumn2(column2).build()

        override fun ofRows(row1: Vec2fAccessor, row2: Vec2fAccessor): ImmutableMat2f =
            setRow1(row1).setRow2(row2).build()

        override fun setM00(m00: Float): ImmutableMat2f.Builder {
            this.m00 = m00
            return this
        }

        override fun setM01(m01: Float): ImmutableMat2f.Builder {
            this.m01 = m01
            return this
        }

        override fun setM10(m10: Float): ImmutableMat2f.Builder {
            this.m10 = m10
            return this
        }

        override fun setM11(m11: Float): ImmutableMat2f.Builder {
            this.m11 = m11
            return this
        }

        override fun set(value: Float): ImmutableMat2f.Builder {
            m00 = value
            m01 = value
            m10 = value
            m11 = value
            return this
        }

        override fun set(m00: Float, m01: Float, m10: Float, m11: Float): ImmutableMat2f.Builder {
            this.m00 = m00
            this.m01 = m01
            this.m10 = m10
            this.m11 = m11
            return this
        }

        override fun set(other: Mat2fAccessor): ImmutableMat2f.Builder {
            m00 = other.m00
            m01 = other.m01
            m10 = other.m10
            m11 = other.m11
            return this
        }

        override fun setColumn1(column1: Vec2fAccessor): ImmutableMat2f.Builder {
            m00 = column1.x
            m10 = column1.y
            return this
        }

        override fun setColumn2(column2: Vec2fAccessor): ImmutableMat2f.Builder {
            m01 = column2.x
            m11 = column2.y
            return this
        }

        override fun setRow1(row1: Vec2fAccessor): ImmutableMat2f.Builder {
            m00 = row1.x
            m01 = row1.y
            return this
        }

        override fun setRow2(row2: Vec2fAccessor): ImmutableMat2f.Builder {
            m10 = row2.x
            m11 = row2.y
            return this
        }

        override fun build(): ImmutableMat2f = StdImmutableMat2f(m00, m01, m10, m11)
    }

    companion object {
        fun builder(): ImmutableMat2f.Builder = Builder()
    }

}

inline fun buildImmutableMat2f(builderAction: ImmutableMat2f.Builder.() -> Unit): ImmutableMat2f {
    return StdImmutableMat2f.builder().apply(builderAction).build()
}
