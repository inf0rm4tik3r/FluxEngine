package de.colibriengine.math.matrix.mat2f

import de.colibriengine.math.matrix.mat2d.Mat2dAccessor
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.absoluteValue
import kotlin.math.max

/**
 * Mutable 2x2 matrix in float precision.
 *
 * @version 0.0.0.6
 * @since ColibriEngine 0.0.7.2
 */
@Suppress("DuplicatedCode")
class StdMat2f(
    /* First row */
    override var m00: Float = 0.0f,
    override var m01: Float = 0.0f,
    /* Second row */
    override var m10: Float = 0.0f,
    override var m11: Float = 0.0f
) : Mat2f {

    private var _view: Mat2fView? = null
    override val view: Mat2fView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    override fun initZero(): Mat2f = set(0.0f)

    override fun initIdentity(): Mat2f {
        m10 = 0.0f
        m01 = m10
        m11 = 1.0f
        m00 = m11
        return this
    }

    override fun set(value: Float): Mat2f {
        m11 = value
        m10 = m11
        m01 = m10
        m00 = m01
        return this
    }

    override fun set(
        m00: Float, m01: Float,
        m10: Float, m11: Float
    ): Mat2f {
        this.m00 = m00
        this.m01 = m01
        this.m10 = m10
        this.m11 = m11
        return this
    }

    override fun set(other: Mat2dAccessor): Mat2f {
        m00 = other.m00.toFloat()
        m01 = other.m01.toFloat()
        m10 = other.m10.toFloat()
        m11 = other.m11.toFloat()
        return this
    }

    override fun set(other: Mat2fAccessor): Mat2f {
        m00 = other.m00
        m01 = other.m01
        m10 = other.m10
        m11 = other.m11
        return this
    }

    override fun set(matrixData: Array<FloatArray>): Mat2f {
        require(!(matrixData.size != Mat2f.ROW_AMT || matrixData[0].size != Mat2f.COLUMN_AMT)) {
            "Array dimensions do not match. Copy not possible"
        }
        m00 = matrixData[0][0]
        m01 = matrixData[0][1]
        m10 = matrixData[1][0]
        m11 = matrixData[1][1]
        return this
    }

    override fun set(row: Int, column: Int, value: Float): Mat2f {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (row) {
            0 -> when (column) {
                0 -> {
                    m00 = value
                    return this
                }
                1 -> {
                    m01 = value
                    return this
                }
            }
            1 -> when (column) {
                0 -> {
                    m10 = value
                    return this
                }
                1 -> {
                    m11 = value
                    return this
                }
            }
        }
        throw IllegalArgumentException(
            "Row($row) and column($column) indices were not both in range [0..1]!"
        )
    }

    override fun setRow(row: Int, x: Float, y: Float): Mat2f {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        when (row) {
            0 -> {
                m00 = x
                m01 = y
                return this
            }
            1 -> {
                m10 = x
                m11 = y
                return this
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..1]!")
    }

    override fun setRow(row: Int, vector: Vec2fAccessor): Mat2f {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        when (row) {
            0 -> {
                m00 = vector.x
                m01 = vector.y
                return this
            }
            1 -> {
                m10 = vector.x
                m11 = vector.y
                return this
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..1]!")
    }

    override fun setRow1(x: Float, y: Float): Mat2f {
        m00 = x
        m01 = y
        return this
    }

    override fun setRow2(x: Float, y: Float): Mat2f {
        m10 = x
        m11 = y
        return this
    }

    override fun setRow1(vector: Vec2fAccessor): Mat2f {
        m00 = vector.x
        m01 = vector.y
        return this
    }

    override fun setRow2(vector: Vec2fAccessor): Mat2f {
        m10 = vector.x
        m11 = vector.y
        return this
    }

    override fun setColumn(column: Int, x: Float, y: Float): Mat2f {
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (column) {
            0 -> {
                m00 = x
                m10 = y
                return this
            }
            1 -> {
                m01 = x
                m11 = y
                return this
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..1]!")
    }

    override fun setColumn(column: Int, vector: Vec2fAccessor): Mat2f {
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (column) {
            0 -> {
                m00 = vector.x
                m10 = vector.y
                return this
            }
            1 -> {
                m01 = vector.x
                m11 = vector.y
                return this
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..1]!")
    }

    override fun setColumn1(x: Float, y: Float): Mat2f {
        m00 = x
        m10 = y
        return this
    }

    override fun setColumn1(vector: Vec2fAccessor): Mat2f {
        m00 = vector.x
        m10 = vector.y
        return this
    }

    override fun setColumn2(x: Float, y: Float): Mat2f {
        m01 = x
        m11 = y
        return this
    }

    override fun setColumn2(vector: Vec2fAccessor): Mat2f {
        m01 = vector.x
        m11 = vector.y
        return this
    }

    override fun times(value: Float): Mat2f {
        m00 *= value
        m01 *= value
        m10 *= value
        m11 *= value
        return this
    }

    override fun times(other: Mat2fAccessor): Mat2f {
        assert(this !== other) { "Cannot multiply with itself!" }
        // First new row.
        var r0: Float = m00 * other.m00 + m01 * other.m10
        var r1: Float = m00 * other.m01 + m01 * other.m11
        m00 = r0
        m01 = r1
        // Second new row.
        r0 = m10 * other.m00 + m11 * other.m10
        r1 = m10 * other.m01 + m11 * other.m11
        m10 = r0
        m11 = r1
        return this
    }

    override fun mulSelf(): Mat2f {
        val r00 = m00 * m00 + m01 * m10
        val r01 = m00 * m01 + m01 * m11
        val r10 = m10 * m00 + m11 * m10
        val r11 = m10 * m01 + m11 * m11
        m00 = r00
        m01 = r01
        m10 = r10
        m11 = r11
        return this
    }

    override fun transform(vector: Vec2f): Vec2f {
        return vector.set(
            m00 * vector.x + m01 * vector.y,
            m10 * vector.x + m11 * vector.y
        )
    }

    override fun plus(value: Float): Mat2f {
        m00 += value
        m01 += value
        m10 += value
        m11 += value
        return this
    }

    override fun plus(other: Mat2fAccessor): Mat2f {
        m00 += other.m00
        m01 += other.m01
        m10 += other.m10
        m11 += other.m11
        return this
    }

    override fun minus(value: Float): Mat2f {
        m00 -= value
        m01 -= value
        m10 -= value
        m11 -= value
        return this
    }

    override fun minus(other: Mat2fAccessor): Mat2f {
        m00 -= other.m00
        m01 -= other.m01
        m10 -= other.m10
        m11 -= other.m11
        return this
    }

    override fun inverse(): Mat2f {
        val det = determinant()
        check(det != 0.0f) { "Determinant is zero. Unable to calculate the inverse matrix!" }
        val oneOverDet = 1.0f / det
        return set(
            oneOverDet * m11, -oneOverDet * m01,
            -oneOverDet * m10, oneOverDet * m00
        )
    }

    override fun transpose(): Mat2f {
        // Swap 10 with 01.
        val temp = m10
        m10 = m01
        m01 = temp
        return this
    }

    override fun pow(exponent: Int): Mat2f {
        require(exponent >= -1) { "Exponents below -1 are not supported. Given: $exponent" }
        return when (exponent) {
            -1 -> inverse()
            0 -> initIdentity()
            1 -> this
            else -> {
                for (i in 1 until exponent) {
                    mulSelf()
                }
                this
            }
        }
    }

    override fun abs(): Mat2f {
        m00 = m00.absoluteValue
        m01 = m01.absoluteValue
        m10 = m10.absoluteValue
        m11 = m11.absoluteValue
        return this
    }

    override fun shorten(): Mat2f {
        m00 = m00.toFourDecimalPlaces()
        m01 = m01.toFourDecimalPlaces()
        m10 = m10.toFourDecimalPlaces()
        m11 = m11.toFourDecimalPlaces()
        return this
    }

    override operator fun get(row: Int, column: Int): Float {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (row) {
            0 -> when (column) {
                0 -> return m00
                1 -> return m01
            }
            1 -> when (column) {
                0 -> return m10
                1 -> return m11
            }
        }
        throw IllegalArgumentException(
            "Row($row) and column($column) indices were not both in range [0..1]!"
        )
    }

    override fun getRow(row: Int, storeIn: Vec2f): Vec2f {
        assert(row in 0..1) { "Row index was not in range [0..1]!" }
        when (row) {
            0 -> return storeIn.set(m00, m01)
            1 -> return storeIn.set(m10, m11)
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..1]!")
    }

    override fun getRow1(storeIn: Vec2f): Vec2f = storeIn.set(m00, m01)

    override fun getRow2(storeIn: Vec2f): Vec2f = storeIn.set(m10, m11)

    override fun getColumn(column: Int, storeIn: Vec2f): Vec2f {
        assert(column in 0..1) { "Column index was not in range [0..1]!" }
        when (column) {
            0 -> return storeIn.set(m00, m10)
            1 -> return storeIn.set(m01, m11)
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..1]!")
    }

    override fun getColumn1(storeIn: Vec2f): Vec2f = storeIn.set(m00, m10)

    override fun getColumn2(storeIn: Vec2f): Vec2f = storeIn.set(m01, m11)

    override fun getMajorDiagonal(storeIn: Vec2f): Vec2f = storeIn.set(m00, m11)

    override fun setM00(value: Float): Mat2f {
        m00 = value
        return this
    }

    override fun setM01(value: Float): Mat2f {
        m00 = value
        return this
    }

    override fun setM10(value: Float): Mat2f {
        m00 = value
        return this
    }

    override fun setM11(value: Float): Mat2f {
        m00 = value
        return this
    }

    override fun asArray(): Array<out FloatArray> {
        return arrayOf(
            floatArrayOf(m00, m01),
            floatArrayOf(m10, m11)
        )
    }

    override fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray> {
        require(storeIn.size == Mat2f.ROW_AMT && storeIn[0].size == Mat2f.COLUMN_AMT) {
            "Given data array is not of size 2x2!"
        }
        storeIn[0][0] = m00
        storeIn[0][1] = m01
        storeIn[1][0] = m10
        storeIn[1][1] = m11
        return storeIn
    }

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Mat2f.BYTES)).array()
    }

    override fun determinant(): Float = m00 * m11 - m10 * m01

    override fun hasZeroComponent(): Boolean = m00 == 0.0f || m01 == 0.0f || m10 == 0.0f || m11 == 0.0f

    override fun toFormattedString(): String = toString()

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        return buffer
            .putFloat(m00).putFloat(m01)
            .putFloat(m10).putFloat(m11)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        return buffer
            .put(m00).put(m01)
            .put(m10).put(m11)
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        return buffer
            .put(m00.toDouble()).put(m01.toDouble())
            .put(m10.toDouble()).put(m11.toDouble())
    }

    override fun hashCode(): Int {
        var result = 17
        result = 31 * result + m00.hashCode()
        result = 31 * result + m01.hashCode()
        result = 31 * result + m10.hashCode()
        result = 31 * result + m11.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Mat2f -> false
            other === this -> true
            else -> m00 == other.m00
                && m01 == other.m01
                && m10 == other.m10
                && m11 == other.m11
        }
    }

    override fun toString(): String = buildString {
        append("Mat2f ( ")
        appendValue(this, m00)
        append(" | ")
        appendValue(this, m01)
        append(" )\n      ( ")
        appendValue(this, m10)
        append(" | ")
        appendValue(this, m11)
        append(" )")
    }

    private fun appendValue(builder: StringBuilder, value: Float) {
        // Add a whitespace if the current value is not negative.
        // Otherwise we will see a "-" sign.
        builder.append(if (value > 0) " " + valueAsString(value) else valueAsString(value))
    }

    private fun valueAsString(value: Float): String = buildString {
        append(value.toFourDecimalPlaces())
        append(" ".repeat(max(0, 4 - toString().split('.')[1].length)))
    }

    /**
     * Mat2f viewer, granting read-only access to its parent data.
     *
     * @author Lukas Potthast
     * @since ColibriEngine 0.0.7.2
     */
    inner class Viewer : Mat2fView {
        override val m00: Float = this@StdMat2f.m00
        override val m01: Float = this@StdMat2f.m01
        override val m10: Float = this@StdMat2f.m10
        override val m11: Float = this@StdMat2f.m11

        override operator fun get(row: Int, column: Int): Float = this@StdMat2f[row, column]

        override fun getRow(row: Int, storeIn: Vec2f): Vec2f = this@StdMat2f.getRow(row, storeIn)
        override fun getRow1(storeIn: Vec2f): Vec2f = this@StdMat2f.getRow1(storeIn)
        override fun getRow2(storeIn: Vec2f): Vec2f = this@StdMat2f.getRow2(storeIn)

        override fun getColumn(column: Int, storeIn: Vec2f): Vec2f = this@StdMat2f.getColumn(column, storeIn)
        override fun getColumn1(storeIn: Vec2f): Vec2f = this@StdMat2f.getColumn1(storeIn)
        override fun getColumn2(storeIn: Vec2f): Vec2f = this@StdMat2f.getColumn2(storeIn)

        override fun getMajorDiagonal(storeIn: Vec2f): Vec2f = this@StdMat2f.getMajorDiagonal(storeIn)

        override fun asArray(): Array<out FloatArray> = this@StdMat2f.asArray()
        override fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray> = this@StdMat2f.asArray(storeIn)

        override fun transform(vector: Vec2f): Vec2f = this@StdMat2f.transform(vector)

        override fun determinant(): Float = this@StdMat2f.determinant()

        override fun hasZeroComponent(): Boolean = this@StdMat2f.hasZeroComponent()

        override fun bytes(): ByteArray = this@StdMat2f.bytes()

        override fun toFormattedString(): String = this@StdMat2f.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdMat2f.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdMat2f.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdMat2f.storeIn(buffer)
    }

    companion object : Mat2fStaticOps {
        override fun determinantOf(m00: Float, m01: Float, m10: Float, m11: Float): Float {
            return m00 * m11 - m10 * m01
        }
    }

}
