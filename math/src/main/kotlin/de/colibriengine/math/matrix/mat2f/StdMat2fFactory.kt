package de.colibriengine.math.matrix.mat2f

/**
 * StdMat2fFactory.
 *
 * 
 */
class StdMat2fFactory : Mat2fFactory {

    private val pool: StdMat2fPool = StdMat2fPool()

    override fun acquire(): Mat2f = pool.acquire()

    override fun acquireDirty(): Mat2f = pool.acquireDirty()

    override fun free(mat2f: Mat2f) = pool.free(mat2f)

    override fun immutableMat2fBuilder(): ImmutableMat2f.Builder = StdImmutableMat2f.builder()

    override fun zero(): ImmutableMat2f = ZERO

    override fun one(): ImmutableMat2f = ONE

    override fun identity(): ImmutableMat2f = IDENTITY

    companion object {
        val ZERO = StdImmutableMat2f.builder().of(0.0f)
        val ONE = StdImmutableMat2f.builder().of(1.0f)
        val IDENTITY = StdImmutableMat2f.builder().set(
            1.0f, 0.0f,
            0.0f, 1.0f
        ).build()
    }

}
