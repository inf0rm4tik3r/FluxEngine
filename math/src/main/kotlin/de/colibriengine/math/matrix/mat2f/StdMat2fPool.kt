package de.colibriengine.math.matrix.mat2f

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

/**
 * Mat2fPool.
 *
 *
 */
class StdMat2fPool : ObjectPool<Mat2f>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdMat2f()
    },
    { instance: Mat2f -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdMat2fPool::class.java)
    }

}
