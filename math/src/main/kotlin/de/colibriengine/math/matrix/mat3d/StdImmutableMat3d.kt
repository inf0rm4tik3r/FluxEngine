package de.colibriengine.math.matrix.mat3d

import de.colibriengine.math.matrix.mat2d.Mat2d
import de.colibriengine.math.quaternion.quaternionD.QuaternionD
import de.colibriengine.math.vector.vec3d.Vec3d
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

/**
 * StdImmutableMat3d.
 *
 *
 */
@Suppress("DuplicatedCode")
class StdImmutableMat3d private constructor(
    m00: Double, m01: Double, m02: Double,
    m10: Double, m11: Double, m12: Double,
    m20: Double, m21: Double, m22: Double
) : ImmutableMat3d {

    private val mutableAccessor: Mat3dAccessor = StdMat3d(
        m00, m01, m02,
        m10, m11, m12,
        m20, m21, m22
    )

    override val m00: Double = mutableAccessor.m00
    override val m01: Double = mutableAccessor.m01
    override val m02: Double = mutableAccessor.m02
    override val m10: Double = mutableAccessor.m10
    override val m11: Double = mutableAccessor.m11
    override val m12: Double = mutableAccessor.m12
    override val m20: Double = mutableAccessor.m20
    override val m21: Double = mutableAccessor.m21
    override val m22: Double = mutableAccessor.m22

    override fun get(row: Int, column: Int): Double = mutableAccessor[row, column]
    override fun getRow(row: Int, storeIn: Vec3d): Vec3d = mutableAccessor.getRow(row, storeIn)
    override fun getRow1(storeIn: Vec3d): Vec3d = mutableAccessor.getRow1(storeIn)
    override fun getRow2(storeIn: Vec3d): Vec3d = mutableAccessor.getRow2(storeIn)
    override fun getRow3(storeIn: Vec3d): Vec3d = mutableAccessor.getRow3(storeIn)

    override fun getColumn(column: Int, storeIn: Vec3d): Vec3d = mutableAccessor.getColumn(column, storeIn)
    override fun getColumn1(storeIn: Vec3d): Vec3d = mutableAccessor.getColumn1(storeIn)
    override fun getColumn2(storeIn: Vec3d): Vec3d = mutableAccessor.getColumn2(storeIn)
    override fun getColumn3(storeIn: Vec3d): Vec3d = mutableAccessor.getColumn3(storeIn)

    override fun getMajorDiagonal(storeIn: Vec3d): Vec3d = mutableAccessor.getMajorDiagonal(storeIn)

    override fun getRight(storeIn: Vec3d): Vec3d = mutableAccessor.getRight(storeIn)
    override fun getUp(storeIn: Vec3d): Vec3d = mutableAccessor.getUp(storeIn)
    override fun getForward(storeIn: Vec3d): Vec3d = mutableAccessor.getForward(storeIn)

    override fun asQuaternion(storeIn: QuaternionD): QuaternionD = mutableAccessor.asQuaternion(storeIn)

    override fun asArray(): Array<out DoubleArray> = mutableAccessor.asArray()
    override fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray> = mutableAccessor.asArray(storeIn)
    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun getSubmatrix(row: Int, column: Int, storeIn: Mat2d): Mat2d =
        mutableAccessor.getSubmatrix(row, column, storeIn)

    override fun transform(vector: Vec3d): Vec3d = mutableAccessor.transform(vector)

    override fun determinant(): Double = mutableAccessor.determinant()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString()

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableMat3d.Builder {
        private var m00: Double = 0.0
        private var m01: Double = 0.0
        private var m02: Double = 0.0
        private var m10: Double = 0.0
        private var m11: Double = 0.0
        private var m12: Double = 0.0
        private var m20: Double = 0.0
        private var m21: Double = 0.0
        private var m22: Double = 0.0

        override fun of(value: Double): ImmutableMat3d = set(value).build()

        override fun of(
            m00: Double, m01: Double, m02: Double,
            m10: Double, m11: Double, m12: Double,
            m20: Double, m21: Double, m22: Double
        ): ImmutableMat3d {
            return set(
                m00, m01, m02,
                m10, m11, m12,
                m20, m21, m22
            ).build()
        }

        override fun of(other: Mat3dAccessor): ImmutableMat3d = set(other).build()

        override fun ofColumns(
            column1: Vec3dAccessor,
            column2: Vec3dAccessor,
            column3: Vec3dAccessor
        ): ImmutableMat3d {
            return setColumn1(column1)
                .setColumn2(column2)
                .setColumn3(column3)
                .build()
        }

        override fun ofRows(row1: Vec3dAccessor, row2: Vec3dAccessor, row3: Vec3dAccessor): ImmutableMat3d {
            return setRow1(row1)
                .setRow2(row2)
                .setRow3(row3)
                .build()
        }

        override fun set00(m00: Double): ImmutableMat3d.Builder {
            this.m00 = m00
            return this
        }

        override fun set01(m01: Double): ImmutableMat3d.Builder {
            this.m01 = m01
            return this
        }

        override fun set02(m02: Double): ImmutableMat3d.Builder {
            this.m02 = m02
            return this
        }

        override fun set10(m10: Double): ImmutableMat3d.Builder {
            this.m10 = m10
            return this
        }

        override fun set11(m11: Double): ImmutableMat3d.Builder {
            this.m11 = m11
            return this
        }

        override fun set12(m12: Double): ImmutableMat3d.Builder {
            this.m12 = m12
            return this
        }

        override fun set20(m20: Double): ImmutableMat3d.Builder {
            this.m20 = m20
            return this
        }

        override fun set21(m21: Double): ImmutableMat3d.Builder {
            this.m21 = m21
            return this
        }

        override fun set22(m22: Double): ImmutableMat3d.Builder {
            this.m22 = m22
            return this
        }

        override fun set(value: Double): ImmutableMat3d.Builder {
            m00 = value
            m01 = value
            m02 = value
            m10 = value
            m11 = value
            m12 = value
            m20 = value
            m21 = value
            m22 = value
            return this
        }

        override fun set(
            m00: Double, m01: Double, m02: Double,
            m10: Double, m11: Double, m12: Double,
            m20: Double, m21: Double, m22: Double
        ): ImmutableMat3d.Builder {
            this.m00 = m00
            this.m01 = m01
            this.m02 = m02
            this.m10 = m10
            this.m11 = m11
            this.m12 = m12
            this.m20 = m20
            this.m21 = m21
            this.m22 = m22
            return this
        }

        override fun set(other: Mat3dAccessor): ImmutableMat3d.Builder {
            m00 = other.m00
            m01 = other.m01
            m02 = other.m02
            m10 = other.m10
            m11 = other.m11
            m12 = other.m12
            m20 = other.m20
            m21 = other.m21
            m22 = other.m22
            return this
        }

        override fun setColumn1(column1: Vec3dAccessor): ImmutableMat3d.Builder {
            m00 = column1.x
            m10 = column1.y
            m20 = column1.z
            return this
        }

        override fun setColumn2(column2: Vec3dAccessor): ImmutableMat3d.Builder {
            m01 = column2.x
            m11 = column2.y
            m21 = column2.z
            return this
        }

        override fun setColumn3(column3: Vec3dAccessor): ImmutableMat3d.Builder {
            m02 = column3.x
            m12 = column3.y
            m22 = column3.z
            return this
        }

        override fun setRow1(row1: Vec3dAccessor): ImmutableMat3d.Builder {
            m00 = row1.x
            m01 = row1.y
            m02 = row1.z
            return this
        }

        override fun setRow2(row2: Vec3dAccessor): ImmutableMat3d.Builder {
            m10 = row2.x
            m11 = row2.y
            m12 = row2.z
            return this
        }

        override fun setRow3(row3: Vec3dAccessor): ImmutableMat3d.Builder {
            m20 = row3.x
            m21 = row3.y
            m22 = row3.z
            return this
        }

        override fun build(): ImmutableMat3d {
            return StdImmutableMat3d(
                m00, m01, m02,
                m10, m11, m12,
                m20, m21, m22
            )
        }
    }

    companion object {
        fun builder(): ImmutableMat3d.Builder = Builder()
    }

}

inline fun buildImmutableMat3d(builderAction: ImmutableMat3d.Builder.() -> Unit): ImmutableMat3d {
    return StdImmutableMat3d.builder().apply(builderAction).build()
}
