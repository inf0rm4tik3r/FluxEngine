package de.colibriengine.math.matrix.mat3d

import de.colibriengine.math.matrix.mat2d.Mat2d
import de.colibriengine.math.matrix.mat2d.StdMat2d
import de.colibriengine.math.matrix.mat3f.Mat3fAccessor
import de.colibriengine.math.quaternion.quaternionD.QuaternionD
import de.colibriengine.math.vector.vec3d.StdVec3d
import de.colibriengine.math.vector.vec3d.Vec3d
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.options.Options
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.*

/**
 * Mutable 3x3 matrix in double precision.
 *
 *
 */
@Suppress("DuplicatedCode")
class StdMat3d(
    /* First row */
    override var m00: Double = 0.0,
    override var m01: Double = 0.0,
    override var m02: Double = 0.0,
    /* Second row */
    override var m10: Double = 0.0,
    override var m11: Double = 0.0,
    override var m12: Double = 0.0,
    /* Third row */
    override var m20: Double = 0.0,
    override var m21: Double = 0.0,
    override var m22: Double = 0.0
) : Mat3d {

    private var _view: Mat3dView? = null
    override val view: Mat3dView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    override fun setM00(value: Double): Mat3d {
        m00 = value
        return this
    }

    override fun setM01(value: Double): Mat3d {
        m01 = value
        return this
    }

    override fun setM02(value: Double): Mat3d {
        m02 = value
        return this
    }

    override fun setM10(value: Double): Mat3d {
        m10 = value
        return this
    }

    override fun setM11(value: Double): Mat3d {
        m11 = value
        return this
    }

    override fun setM12(value: Double): Mat3d {
        m12 = value
        return this
    }

    override fun setM20(value: Double): Mat3d {
        m20 = value
        return this
    }

    override fun setM21(value: Double): Mat3d {
        m21 = value
        return this
    }

    override fun setM22(value: Double): Mat3d {
        m22 = value
        return this
    }

    override fun set(row: Int, column: Int, value: Double): Mat3d {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        if (Options.ARGUMENT_CHECKS) {
            if (row !in 0..2 || column !in 0..2) {
                throw IllegalArgumentException(
                    "Row($row) and column($column) indices were not both in range [0..2]!"
                )
            }
        }
        when (row) {
            0 -> when (column) {
                0 -> m00 = value
                1 -> m01 = value
                2 -> m02 = value
            }
            1 -> when (column) {
                0 -> m10 = value
                1 -> m11 = value
                2 -> m12 = value
            }
            2 -> when (column) {
                0 -> m20 = value
                1 -> m21 = value
                2 -> m22 = value
            }
        }
        return this
    }

    override fun setRow(row: Int, x: Double, y: Double, z: Double): Mat3d {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        when (row) {
            0 -> {
                m00 = x
                m01 = y
                m02 = z
            }
            1 -> {
                m10 = x
                m11 = y
                m12 = z
            }
            2 -> {
                m20 = x
                m21 = y
                m22 = z
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..2]!")
    }

    override fun setRow1(x: Double, y: Double, z: Double): Mat3d {
        m00 = x
        m01 = y
        m02 = z
        return this
    }

    override fun setRow2(x: Double, y: Double, z: Double): Mat3d {
        m10 = x
        m11 = y
        m12 = z
        return this
    }

    override fun setRow3(x: Double, y: Double, z: Double): Mat3d {
        m20 = x
        m21 = y
        m22 = z
        return this
    }

    override fun setRow(row: Int, vector: Vec3dAccessor): Mat3d {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        when (row) {
            0 -> {
                m00 = vector.x
                m01 = vector.y
                m02 = vector.z
            }
            1 -> {
                m10 = vector.x
                m11 = vector.y
                m12 = vector.z
            }
            2 -> {
                m20 = vector.x
                m21 = vector.y
                m22 = vector.z
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..2]!")
    }

    override fun setRow1(vector: Vec3dAccessor): Mat3d {
        m00 = vector.x
        m01 = vector.y
        m02 = vector.z
        return this
    }

    override fun setRow2(vector: Vec3dAccessor): Mat3d {
        m10 = vector.x
        m11 = vector.y
        m12 = vector.z
        return this
    }

    override fun setRow3(vector: Vec3dAccessor): Mat3d {
        m20 = vector.x
        m21 = vector.y
        m22 = vector.z
        return this
    }

    override fun setColumn(column: Int, x: Double, y: Double, z: Double): Mat3d {
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        when (column) {
            0 -> {
                m00 = x
                m10 = y
                m20 = z
            }
            1 -> {
                m01 = x
                m11 = y
                m21 = z
            }
            2 -> {
                m02 = x
                m12 = y
                m22 = z
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..2]!")
    }

    override fun setColumn1(x: Double, y: Double, z: Double): Mat3d {
        m00 = x
        m10 = y
        m20 = z
        return this
    }

    override fun setColumn2(x: Double, y: Double, z: Double): Mat3d {
        m01 = x
        m11 = y
        m21 = z
        return this
    }

    override fun setColumn3(x: Double, y: Double, z: Double): Mat3d {
        m02 = x
        m12 = y
        m22 = z
        return this
    }

    override fun setColumn(column: Int, vector: Vec3dAccessor): Mat3d {
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        when (column) {
            0 -> {
                m00 = vector.x
                m10 = vector.y
                m20 = vector.z
            }
            1 -> {
                m01 = vector.x
                m11 = vector.y
                m21 = vector.z
            }
            2 -> {
                m02 = vector.x
                m12 = vector.y
                m22 = vector.z
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..2]!")
    }

    override fun setColumn1(vector: Vec3dAccessor): Mat3d {
        m00 = vector.x
        m10 = vector.y
        m20 = vector.z
        return this
    }

    override fun setColumn2(vector: Vec3dAccessor): Mat3d {
        m01 = vector.x
        m11 = vector.y
        m21 = vector.z
        return this
    }

    override fun setColumn3(vector: Vec3dAccessor): Mat3d {
        m02 = vector.x
        m12 = vector.y
        m22 = vector.z
        return this
    }

    override fun set(value: Double): Mat3d {
        m22 = value
        m21 = m22
        m20 = m21
        m12 = m20
        m11 = m12
        m10 = m11
        m02 = m10
        m01 = m02
        m00 = m01
        return this
    }

    override fun set(
        m00: Double, m01: Double, m02: Double,
        m10: Double, m11: Double, m12: Double,
        m20: Double, m21: Double, m22: Double
    ): Mat3d {
        this.m00 = m00
        this.m01 = m01
        this.m02 = m02
        this.m10 = m10
        this.m11 = m11
        this.m12 = m12
        this.m20 = m20
        this.m21 = m21
        this.m22 = m22
        return this
    }

    override fun set(other: Mat3fAccessor): Mat3d {
        m00 = other.m00.toDouble()
        m01 = other.m01.toDouble()
        m02 = other.m02.toDouble()
        m10 = other.m10.toDouble()
        m11 = other.m11.toDouble()
        m12 = other.m12.toDouble()
        m20 = other.m20.toDouble()
        m21 = other.m21.toDouble()
        m22 = other.m22.toDouble()
        return this
    }

    override fun set(other: Mat3dAccessor): Mat3d {
        m00 = other.m00
        m01 = other.m01
        m02 = other.m02
        m10 = other.m10
        m11 = other.m11
        m12 = other.m12
        m20 = other.m20
        m21 = other.m21
        m22 = other.m22
        return this
    }

    override fun set(matrixData: Array<DoubleArray>): Mat3d {
        require(!(matrixData.size != Mat3d.ROW_AMT || matrixData[0].size != Mat3d.COLUMN_AMT)) {
            "Array dimensions do not match. Copy not possible"
        }
        m00 = matrixData[0][0]
        m01 = matrixData[0][1]
        m02 = matrixData[0][2]
        m10 = matrixData[1][0]
        m11 = matrixData[1][1]
        m12 = matrixData[1][2]
        m20 = matrixData[2][0]
        m21 = matrixData[2][1]
        m22 = matrixData[2][2]
        return this
    }

    override fun initZero(): Mat3d = set(0.0)

    override fun initIdentity(): Mat3d {
        m21 = 0.0
        m20 = m21
        m12 = m20
        m10 = m12
        m02 = m10
        m01 = m02
        m22 = 1.0
        m11 = m22
        m00 = m11
        return this
    }

    override fun initRotationX(angleAroundX: Double): Mat3d {
        val angleAroundXInRad = Math.toRadians(angleAroundX)
        val sinX = sin(angleAroundXInRad)
        val cosX = cos(angleAroundXInRad)
        return set(
            1.0, 0.0, 0.0, 0.0, cosX, sinX, 0.0, -sinX, cosX
        )
    }

    override fun initRotationY(angleAroundY: Double): Mat3d {
        val angleAroundYInRad = Math.toRadians(angleAroundY)
        val sinY = sin(angleAroundYInRad)
        val cosY = cos(angleAroundYInRad)
        return set(
            cosY, 0.0, -sinY, 0.0, 1.0, 0.0,
            sinY, 0.0, cosY
        )
    }

    override fun initRotationZ(angleAroundZ: Double): Mat3d {
        val angleAroundZInRad = Math.toRadians(angleAroundZ)
        val sinZ = sin(angleAroundZInRad)
        val cosZ = cos(angleAroundZInRad)
        return set(
            cosZ, -sinZ, 0.0,
            sinZ, cosZ, 0.0, 0.0, 0.0, 1.0
        )
    }

    /*
    fun initRotationSLOW(
        angleAroundX: Double,
        angleAroundY: Double,
        angleAroundZ: Double
    ): Mat3d {
        val yRotation: Mat3d = StdMat3d().initRotationY(angleAroundY)
        val xRotation: Mat3d = StdMat3d().initRotationX(angleAroundX)
        return initRotationZ(angleAroundZ).times(yRotation).times(xRotation)
    }

    fun initRotationSLOW(anglesView: Vec3dAccessor): Mat3d = initRotationSLOW(anglesView.x, anglesView.y, anglesView.z)
    */

    override fun initRotation(
        angleAroundX: Double,
        angleAroundY: Double,
        angleAroundZ: Double
    ): Mat3d {
        val angleAroundXInRad = Math.toRadians(angleAroundX)
        val angleAroundYInRad = Math.toRadians(angleAroundY)
        val angleAroundZInRad = Math.toRadians(angleAroundZ)
        val sinX = sin(angleAroundXInRad)
        val cosX = cos(angleAroundXInRad)
        val sinY = sin(angleAroundYInRad)
        val cosY = cos(angleAroundYInRad)
        val sinZ = sin(angleAroundZInRad)
        val cosZ = cos(angleAroundZInRad)
        return set(
            cosY * cosZ, cosX * sinZ + sinX * sinY * cosZ, sinX * sinZ - cosX * sinY * cosZ,
            -cosY * sinZ, cosX * cosZ - sinX * sinY * sinZ, sinX * cosZ + cosX * sinY * sinZ,
            sinY, -sinX * cosY, cosX * cosY
        )
    }

    override fun initRotation(angles: Vec3dAccessor): Mat3d = initRotation(angles.x, angles.y, angles.z)

    override fun initRotation(axis: Vec3dAccessor, angle: Double): Mat3d {
        // 											 (+0,  -Rz, +Ry)
        // M = r*rT + cos(a) * (I - r*rT) + sin(a) * (+Rz, +0,  -Rx)
        //											 (-Ry, +Rx, +0 )
        val normalizedAxis: StdVec3d = StdVec3d().set(axis).normalize()
        val rot = StdMat3d(
            0.0, -axis.z, +axis.y,
            +axis.z, 0.0, -axis.x,
            -axis.y, +axis.x, 0.0
        )
        val xx: Double = normalizedAxis.x * normalizedAxis.x
        val xy: Double = normalizedAxis.x * normalizedAxis.y
        val xz: Double = normalizedAxis.x * normalizedAxis.z
        val yy: Double = normalizedAxis.y * normalizedAxis.y
        val yz: Double = normalizedAxis.y * normalizedAxis.z
        val zz: Double = normalizedAxis.z * normalizedAxis.z

        //r*r^T
        set(
            xx, xy, xz,
            xy, yy, yz,
            xz, yz, zz
        )
        val angleInRad = Math.toRadians(angle)
        val sin = sin(angleInRad)
        val cos = cos(angleInRad)
        val term1 = StdMat3d().initIdentity().minus(this).times(cos)
        val term2 = rot.times(sin)
        return plus(term1).plus(term2)
    }

    override fun transform(vector: Vec3d): Vec3d {
        return vector.set(
            m00 * vector.x + m01 * vector.y + m02 * vector.z,
            m10 * vector.x + m11 * vector.y + m12 * vector.z,
            m20 * vector.x + m21 * vector.y + m22 * vector.z
        )
    }

    override fun plus(value: Double): Mat3d {
        m00 += value
        m01 += value
        m02 += value
        m10 += value
        m11 += value
        m12 += value
        m20 += value
        m21 += value
        m22 += value
        return this
    }

    override fun plus(other: Mat3dAccessor): Mat3d {
        m00 += other.m00
        m01 += other.m01
        m02 += other.m02
        m10 += other.m10
        m11 += other.m11
        m12 += other.m12
        m20 += other.m20
        m21 += other.m21
        m22 += other.m22
        return this
    }

    override fun minus(value: Double): Mat3d {
        m00 -= value
        m01 -= value
        m02 -= value
        m10 -= value
        m11 -= value
        m12 -= value
        m20 -= value
        m21 -= value
        m22 -= value
        return this
    }

    override fun minus(other: Mat3dAccessor): Mat3d {
        m00 -= other.m00
        m01 -= other.m01
        m02 -= other.m02
        m10 -= other.m10
        m11 -= other.m11
        m12 -= other.m12
        m20 -= other.m20
        m21 -= other.m21
        m22 -= other.m22
        return this
    }

    override fun times(value: Double): Mat3d {
        m00 *= value
        m01 *= value
        m02 *= value
        m10 *= value
        m11 *= value
        m12 *= value
        m20 *= value
        m21 *= value
        m22 *= value
        return this
    }

    override fun times(other: Mat3dAccessor): Mat3d {
        assert(this !== other) { "Cannot multiply with itself!" }
        // First row.
        var r0: Double = m00 * other.m00 + m01 * other.m10 + m02 * other.m20
        var r1: Double = m00 * other.m01 + m01 * other.m11 + m02 * other.m21
        var r2: Double = m00 * other.m02 + m01 * other.m12 + m02 * other.m22
        m00 = r0
        m01 = r1
        m02 = r2
        // Second row.
        r0 = m10 * other.m00 + m11 * other.m10 + m12 * other.m20
        r1 = m10 * other.m01 + m11 * other.m11 + m12 * other.m21
        r2 = m10 * other.m02 + m11 * other.m12 + m12 * other.m22
        m10 = r0
        m11 = r1
        m12 = r2
        // Third row.
        r0 = m20 * other.m00 + m21 * other.m10 + m22 * other.m20
        r1 = m20 * other.m01 + m21 * other.m11 + m22 * other.m21
        r2 = m20 * other.m02 + m21 * other.m12 + m22 * other.m22
        m20 = r0
        m21 = r1
        m22 = r2
        return this
    }

    override fun mulSelf(): Mat3d {
        // First new row.
        val r00 = m00 * m00 + m01 * m10 + m02 * m20
        val r01 = m00 * m01 + m01 * m11 + m02 * m21
        val r02 = m00 * m02 + m01 * m12 + m02 * m22
        // Second new row.
        val r10 = m10 * m00 + m11 * m10 + m12 * m20
        val r11 = m10 * m01 + m11 * m11 + m12 * m21
        val r12 = m10 * m02 + m11 * m12 + m12 * m22
        // Third new row.
        val r20 = m20 * m00 + m21 * m10 + m22 * m20
        val r21 = m20 * m01 + m21 * m11 + m22 * m21
        val r22 = m20 * m02 + m21 * m12 + m22 * m22
        // Update this instance.
        m00 = r00
        m01 = r01
        m02 = r02
        m10 = r10
        m11 = r11
        m12 = r12
        m20 = r20
        m21 = r21
        m22 = r22
        return this
    }

    override fun inverse(): Mat3d {
        // Calculate 1 over the determinant of the initial matrix.
        val oneOverDeterminant = 1.0f / determinant()

        // Calculate cofactors using submatrix determinants.
        val c00 = +StdMat2d.determinantOf(m11, m12, m21, m22)
        val c01 = -StdMat2d.determinantOf(m10, m12, m20, m22)
        val c02 = +StdMat2d.determinantOf(m10, m11, m20, m21)
        val c10 = -StdMat2d.determinantOf(m01, m02, m21, m22)
        val c11 = +StdMat2d.determinantOf(m00, m02, m20, m22)
        val c12 = -StdMat2d.determinantOf(m00, m01, m20, m21)
        val c20 = +StdMat2d.determinantOf(m01, m02, m11, m12)
        val c21 = -StdMat2d.determinantOf(m00, m02, m10, m12)
        val c22 = +StdMat2d.determinantOf(m00, m01, m10, m11)

        // ... and return the result multiplied with the transposed cofactor matrix.
        return set(c00, c01, c02, c10, c11, c12, c20, c21, c22).transpose().times(oneOverDeterminant)
    }

    override fun transpose(): Mat3d {
        // Swap 10 with 01.
        var temp: Double = m10
        m10 = m01
        m01 = temp
        // Swap 21 with 12.
        temp = m21
        m21 = m12
        m12 = temp
        // Swap 20 with 02.
        temp = m20
        m20 = m02
        m02 = temp
        return this
    }

    @Suppress("ReplaceGetOrSet")
    override fun getSubmatrix(row: Int, column: Int, storeIn: Mat2d): Mat2d {
        assert(row in 0..2) { "Row to ignore (row) is not in range [0..2]!" }
        assert(column in 0..2) { "Column to ignore (column) is not in range [0..2]!" }
        when (row) {
            0 -> when (column) {
                0 -> return storeIn.set(m11, m12, m21, m22)
                1 -> return storeIn.set(m10, m12, m20, m22)
                2 -> return storeIn.set(m10, m11, m20, m21)
            }
            1 -> when (column) {
                0 -> return storeIn.set(m01, m02, m21, m22)
                1 -> return storeIn.set(m00, m02, m20, m22)
                2 -> return storeIn.set(m00, m01, m20, m21)
            }
            2 -> when (column) {
                0 -> return storeIn.set(m01, m02, m11, m12)
                1 -> return storeIn.set(m00, m02, m10, m12)
                2 -> return storeIn.set(m00, m01, m10, m11)
            }
        }
        throw IllegalArgumentException("Row or column not in range [0..2]!")
    }

    override fun pow(exponent: Int): Mat3d {
        require(exponent >= -1) { "Exponents below -1 are not supported. Given: $exponent" }
        return when (exponent) {
            -1 -> inverse()
            0 -> initIdentity()
            1 -> this
            else -> {
                var i = 1
                while (i < exponent) {
                    mulSelf()
                    i++
                }
                this
            }
        }
    }

    override fun abs(): Mat3d {
        m00 = m00.absoluteValue
        m01 = m01.absoluteValue
        m02 = m02.absoluteValue
        m10 = m10.absoluteValue
        m11 = m11.absoluteValue
        m12 = m12.absoluteValue
        m20 = m20.absoluteValue
        m21 = m21.absoluteValue
        m22 = m22.absoluteValue
        return this
    }

    override fun shorten(): Mat3d {
        m00 = m00.toFourDecimalPlaces()
        m01 = m01.toFourDecimalPlaces()
        m02 = m02.toFourDecimalPlaces()
        m10 = m10.toFourDecimalPlaces()
        m11 = m11.toFourDecimalPlaces()
        m12 = m12.toFourDecimalPlaces()
        m20 = m20.toFourDecimalPlaces()
        m21 = m21.toFourDecimalPlaces()
        m22 = m22.toFourDecimalPlaces()
        return this
    }

    override fun get(row: Int, column: Int): Double {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        when (row) {
            0 -> when (column) {
                0 -> return m00
                1 -> return m01
                2 -> return m02
            }
            1 -> when (column) {
                0 -> return m10
                1 -> return m11
                2 -> return m12
            }
            2 -> when (column) {
                0 -> return m20
                1 -> return m21
                2 -> return m22
            }
        }
        throw IllegalArgumentException(
            "Row($row) and column($column) indices were not both in range [0..2]!"
        )
    }

    override fun getRow(row: Int, storeIn: Vec3d): Vec3d {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        when (row) {
            0 -> return storeIn.set(m00, m01, m02)
            1 -> return storeIn.set(m10, m11, m12)
            2 -> return storeIn.set(m20, m21, m22)
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..2]!")
    }

    override fun getRow1(storeIn: Vec3d): Vec3d = storeIn.set(m00, m01, m02)

    override fun getRow2(storeIn: Vec3d): Vec3d = storeIn.set(m10, m11, m12)

    override fun getRow3(storeIn: Vec3d): Vec3d = storeIn.set(m20, m21, m22)

    override fun getColumn(column: Int, storeIn: Vec3d): Vec3d {
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        when (column) {
            0 -> return storeIn.set(m00, m10, m20)
            1 -> return storeIn.set(m01, m11, m21)
            2 -> return storeIn.set(m02, m12, m22)
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..2]!")
    }

    override fun getColumn1(storeIn: Vec3d): Vec3d = storeIn.set(m00, m10, m20)

    override fun getColumn2(storeIn: Vec3d): Vec3d = storeIn.set(m01, m11, m21)

    override fun getColumn3(storeIn: Vec3d): Vec3d = storeIn.set(m02, m12, m22)

    override fun getMajorDiagonal(storeIn: Vec3d): Vec3d = storeIn.set(m00, m11, m22)

    override fun getRight(storeIn: Vec3d): Vec3d = storeIn.set(m00, m01, m02)

    override fun getUp(storeIn: Vec3d): Vec3d = storeIn.set(m10, m11, m12)

    override fun getForward(storeIn: Vec3d): Vec3d = storeIn.set(m20, m21, m22)

    override fun asArray(): Array<out DoubleArray> = arrayOf(
        doubleArrayOf(m00, m01, m02),
        doubleArrayOf(m10, m11, m12),
        doubleArrayOf(m20, m21, m22)
    )

    override fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray> {
        assert(storeIn.size == Mat3d.ROW_AMT && storeIn[0].size == Mat3d.COLUMN_AMT) {
            "Given data array is not of size 3x3!"
        }
        storeIn[0][0] = m00
        storeIn[0][1] = m01
        storeIn[0][2] = m02
        storeIn[1][0] = m10
        storeIn[1][1] = m11
        storeIn[1][2] = m12
        storeIn[2][0] = m20
        storeIn[2][1] = m21
        storeIn[2][2] = m22
        return storeIn
    }

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Mat3d.BYTES)).array()
    }

    override fun asQuaternion(storeIn: QuaternionD): QuaternionD {
        val t: Double
        if (m22 < 0.0) {
            if (m00 > m11) {
                t = 1.0 + m00 - m11 - m22
                storeIn.set(t, m01 + m10, m20 + m02, m12 - m21)
            } else {
                t = 1.0 - m00 + m11 - m22
                storeIn.set(m01 + m10, t, m12 + m21, m20 - m02)
            }
        } else {
            if (m00 < -m11) {
                t = 1.0 - m00 - m11 + m22
                storeIn.set(m20 + m02, m12 + m21, t, m01 - m10)
            } else {
                t = 1.0 + m00 + m11 + m22
                storeIn.set(m12 - m21, m20 - m02, m01 - m10, t)
            }
        }
        return storeIn.times(0.5 / sqrt(t))
    }

    override fun determinant(): Double {
        return m00 * m11 * m22 +
            m01 * m12 * m20 +
            m02 * m10 * m21 -
            m20 * m11 * m02 -
            m21 * m12 * m00 -
            m22 * m10 * m01
    }

    override fun hasZeroComponent(): Boolean {
        return m00 == 0.0 || m01 == 0.0 || m02 == 0.0 ||
            m10 == 0.0 || m11 == 0.0 || m12 == 0.0 ||
            m20 == 0.0 || m21 == 0.0 || m22 == 0.0
    }

    override fun toFormattedString(): String = toString()

    override fun hashCode(): Int {
        var result = 17
        result = 31 * result + java.lang.Double.hashCode(m00)
        result = 31 * result + java.lang.Double.hashCode(m01)
        result = 31 * result + java.lang.Double.hashCode(m02)
        result = 31 * result + java.lang.Double.hashCode(m10)
        result = 31 * result + java.lang.Double.hashCode(m11)
        result = 31 * result + java.lang.Double.hashCode(m12)
        result = 31 * result + java.lang.Double.hashCode(m20)
        result = 31 * result + java.lang.Double.hashCode(m21)
        result = 31 * result + java.lang.Double.hashCode(m22)
        return result
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Mat3d -> false
            other === this -> true
            else -> m00 == other.m00 && m01 == other.m01 && m02 == other.m02 &&
                m10 == other.m10 && m11 == other.m11 && m12 == other.m12 &&
                m20 == other.m20 && m21 == other.m21 && m22 == other.m22
        }
    }

    override fun toString(): String = buildString {
        append("Mat3d ( ")
        appendValue(this, m00)
        append(" | ")
        appendValue(this, m01)
        append(" | ")
        appendValue(this, m02)
        append(" )\n      ( ")
        appendValue(this, m10)
        append(" | ")
        appendValue(this, m11)
        append(" | ")
        appendValue(this, m12)
        append(" )\n      ( ")
        appendValue(this, m20)
        append(" | ")
        appendValue(this, m21)
        append(" | ")
        appendValue(this, m22)
        append(" )")
    }

    private fun appendValue(builder: StringBuilder, value: Double) {
        // Add a whitespace if the current value is not negative. Otherwise we will see a "-" sign.
        builder.append(if (value > 0) " " + valueAsString(value) else valueAsString(value))
    }

    private fun valueAsString(value: Double): String = buildString {
        append(value.toFourDecimalPlaces())
        append(" ".repeat(max(0, 4 - toString().split('.')[1].length)))
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        buffer.putDouble(m00)
        buffer.putDouble(m01)
        buffer.putDouble(m02)
        buffer.putDouble(m10)
        buffer.putDouble(m11)
        buffer.putDouble(m12)
        buffer.putDouble(m20)
        buffer.putDouble(m21)
        buffer.putDouble(m22)
        return buffer
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        buffer.put(m00.toFloat())
        buffer.put(m01.toFloat())
        buffer.put(m02.toFloat())
        buffer.put(m10.toFloat())
        buffer.put(m11.toFloat())
        buffer.put(m12.toFloat())
        buffer.put(m20.toFloat())
        buffer.put(m21.toFloat())
        buffer.put(m22.toFloat())
        return buffer
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        buffer.put(m00)
        buffer.put(m01)
        buffer.put(m02)
        buffer.put(m10)
        buffer.put(m11)
        buffer.put(m12)
        buffer.put(m20)
        buffer.put(m21)
        buffer.put(m22)
        return buffer
    }

    /**
     * Mat3d viewer, granting read-only access to its parent data.
     *
     * @author Lukas Potthast
     *
     */
    inner class Viewer : Mat3dView {
        override val m00: Double = this@StdMat3d.m00
        override val m01: Double = this@StdMat3d.m01
        override val m02: Double = this@StdMat3d.m02
        override val m10: Double = this@StdMat3d.m10
        override val m11: Double = this@StdMat3d.m11
        override val m12: Double = this@StdMat3d.m12
        override val m20: Double = this@StdMat3d.m20
        override val m21: Double = this@StdMat3d.m21
        override val m22: Double = this@StdMat3d.m22

        override operator fun get(row: Int, column: Int): Double = this@StdMat3d[row, column]

        override fun getRow(row: Int, storeIn: Vec3d): Vec3d = this@StdMat3d.getRow(row, storeIn)
        override fun getRow1(storeIn: Vec3d): Vec3d = this@StdMat3d.getRow1(storeIn)
        override fun getRow2(storeIn: Vec3d): Vec3d = this@StdMat3d.getRow2(storeIn)
        override fun getRow3(storeIn: Vec3d): Vec3d = this@StdMat3d.getRow3(storeIn)

        override fun getColumn(column: Int, storeIn: Vec3d): Vec3d = this@StdMat3d.getColumn(column, storeIn)
        override fun getColumn1(storeIn: Vec3d): Vec3d = this@StdMat3d.getColumn1(storeIn)
        override fun getColumn2(storeIn: Vec3d): Vec3d = this@StdMat3d.getColumn2(storeIn)
        override fun getColumn3(storeIn: Vec3d): Vec3d = this@StdMat3d.getColumn3(storeIn)

        override fun getMajorDiagonal(storeIn: Vec3d): Vec3d = this@StdMat3d.getMajorDiagonal(storeIn)

        override fun getRight(storeIn: Vec3d): Vec3d = this@StdMat3d.getRight(storeIn)
        override fun getUp(storeIn: Vec3d): Vec3d = this@StdMat3d.getUp(storeIn)
        override fun getForward(storeIn: Vec3d): Vec3d = this@StdMat3d.getForward(storeIn)

        override fun asArray(): Array<out DoubleArray> = this@StdMat3d.asArray()
        override fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray> = this@StdMat3d.asArray(storeIn)
        override fun bytes(): ByteArray = this@StdMat3d.bytes()

        override fun asQuaternion(storeIn: QuaternionD): QuaternionD = this@StdMat3d.asQuaternion(storeIn)

        override fun getSubmatrix(row: Int, column: Int, storeIn: Mat2d): Mat2d =
            this@StdMat3d.getSubmatrix(row, column, storeIn)

        override fun transform(vector: Vec3d): Vec3d = this@StdMat3d.transform(vector)

        override fun determinant(): Double = this@StdMat3d.determinant()
        override fun hasZeroComponent(): Boolean = this@StdMat3d.hasZeroComponent()

        override fun toFormattedString(): String = this@StdMat3d.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdMat3d.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdMat3d.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdMat3d.storeIn(buffer)
    }

    companion object : Mat3dStaticOps {
        override fun determinantOf(
            m00: Double, m01: Double, m02: Double,
            m10: Double, m11: Double, m12: Double,
            m20: Double, m21: Double, m22: Double
        ): Double {
            return m00 * m11 * m22 +
                m01 * m12 * m20 +
                m02 * m10 * m21 -
                m20 * m11 * m02 -
                m21 * m12 * m00 -
                m22 * m10 * m01
        }
    }

}
