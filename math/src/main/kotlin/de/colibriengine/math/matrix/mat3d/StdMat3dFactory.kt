package de.colibriengine.math.matrix.mat3d

/**
 * StdMat3dFactory.
 *
 *
 */
class StdMat3dFactory : Mat3dFactory {

    private val pool: StdMat3dPool = StdMat3dPool()

    override fun acquire(): Mat3d = pool.acquire()

    override fun acquireDirty(): Mat3d = pool.acquireDirty()

    override fun free(mat3d: Mat3d) = pool.free(mat3d)

    override fun immutableMat3dBuilder(): ImmutableMat3d.Builder = StdImmutableMat3d.builder()

    override fun zero(): ImmutableMat3d = ZERO

    override fun one(): ImmutableMat3d = ONE

    override fun identity(): ImmutableMat3d = IDENTITY

    companion object {
        val ZERO = StdImmutableMat3d.builder().of(0.0)
        val ONE = StdImmutableMat3d.builder().of(1.0)
        val IDENTITY = StdImmutableMat3d.builder().set(
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0
        ).build()
    }

}
