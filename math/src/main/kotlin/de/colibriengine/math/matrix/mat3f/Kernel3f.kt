package de.colibriengine.math.matrix.mat3f

/**
 * Kernel3f.
 *
 * 
 */
@Suppress("ReplaceGetOrSet", "unused")
class Kernel3f(val mat3f: StdMat3f) {

    /**
     * Initializes the identity kernel. This kernel will have no effect when applied to an image.
     *
     *      ( 0.0, 0.0, 0.0 )
     *      ( 0.0, 1.0, 0.0 )
     *      ( 0.0, 0.0, 0.0 )
     *
     * @return this
     */
    fun initIdentityKernel(): Kernel3f {
        mat3f.set(
            0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f
        )
        return this
    }

    /**
     * Initializes the sharpen kernel.
     *
     *      (  0.0, -1.0,  0.0 )
     *      ( -1.0,  4.0, -1.0 )
     *      (  0.0, -1.0,  0.0 )
     *
     * @return this
     */
    fun initSharpenKernelV1(): Kernel3f {
        mat3f.set(
            +0.0f, -1.0f, +0.0f,
            -1.0f, +4.0f, -1.0f,
            +0.0f, -1.0f, +0.0f
        )
        return this
    }

    /**
     * Initializes the sharpen kernel.
     *
     *      ( -1.0, -1.0, -1.0 )
     *      ( -1.0,  8.0, -1.0 )
     *      ( -1.0, -1.0, -1.0 )
     *
     * @return this
     */
    fun initSharpenKernelV2(): Kernel3f {
        mat3f.set(
            -1.0f, -1.0f, -1.0f,
            -1.0f, +8.0f, -1.0f,
            -1.0f, -1.0f, -1.0f
        )
        return this
    }

    /**
     * Initializes the box blur kernel. Each pixel value is (evenly) based on the average of its neighboring pixels.
     *
     *      ( 1.0, 1.0, 1.0 )
     *      ( 1.0, 1.0, 1.0 )  /  9.0f
     *      ( 1.0, 1.0, 1.0 )
     *
     * @return this
     */
    fun initBoxBlurKernel(): Kernel3f {
        mat3f.set(1.0f / 9.0f)
        return this
    }

    /**
     * Initializes the blur kernel. Only resembles an approximation of the gaussian blur function.
     *
     *      ( 1.0, 2.0, 1.0 )
     *      ( 2.0, 4.0, 2.0 )  /  16.0f
     *      ( 1.0, 2.0, 1.0 )
     *
     * @return this
     */
    fun initGaussianBlurKernel(): Kernel3f {
        mat3f.set(
            1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f,
            2.0f / 16.0f, 4.0f / 16.0f, 2.0f / 16.0f,
            1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f
        )
        return this
    }

    /**
     * Initializes the edge detection kernel.
     *
     *      (  1.0,  0.0, -1.0 )
     *      (  0.0,  0.0,  0.0 )
     *      ( -1.0,  0.0,  1.0 )
     *
     * @return this
     */
    fun initEdgeDetectionKernelCross(): Kernel3f {
        mat3f.set(
            +1.0f, +0.0f, -1.0f,
            +0.0f, +0.0f, +0.0f,
            -1.0f, +0.0f, +1.0f
        )
        return this
    }

    /**
     * Initializes the edge detection kernel.
     *
     *      (  0.0,  1.0,  0.0 )
     *      (  1.0, -4.0,  1.0 )
     *      (  0.0,  1.0,  0.0 )
     *
     * @return this
     */
    fun initEdgeDetectionKernelV1(): Kernel3f {
        mat3f.set(
            +0.0f, +1.0f, +0.0f,
            +1.0f, -4.0f, +1.0f,
            +0.0f, +1.0f, +0.0f
        )
        return this
    }

    /**
     * Initializes the edge detection kernel.
     *
     *      (  1.0,  1.0,  1.0 )
     *      (  1.0, -8.0,  1.0 )
     *      (  1.0,  1.0,  1.0 )
     *
     * @return this
     */
    fun initEdgeDetectionKernelV2(): Kernel3f {
        mat3f.set(
            +1.0f, +1.0f, +1.0f,
            +1.0f, -8.0f, +1.0f,
            +1.0f, +1.0f, +1.0f
        )
        return this
    }

    /**
     * Initializes the edge detection kernel.
     *
     *      ( -1.0,  0.0,  1.0 )
     *      ( -2.0,  0.0,  2.0 )
     *      ( -1.0,  0.0,  1.0 )
     *
     * @return this
     */
    fun initSobelXKernel(): Kernel3f {
        mat3f.set(
            -1.0f, +0.0f, +1.0f,
            -2.0f, +0.0f, +2.0f,
            -1.0f, +0.0f, +1.0f
        )
        return this
    }

    /**
     * Initializes the edge detection kernel.
     *
     *      ( -1.0, -2.0, -1.0 )
     *      (  0.0,  0.0,  0.0 )
     *      ( +1.0, +2.0, +1.0 )
     *
     * @return this
     */
    fun initSobelYKernel(): Kernel3f {
        mat3f.set(
            -1.0f, -2.0f, -1.0f,
            +0.0f, +0.0f, +0.0f,
            +1.0f, +2.0f, +1.0f
        )
        return this
    }

    // TODO: combined "sobel" operator
    /**
     * Initializes the emboss kernel.
     *
     *      ( -2.0, -2.0,  0.0 )
     *      ( -2.0,  6.0,  0.0 )
     *      (  0.0,  0.0,  0.0 )
     *
     * @return this
     */
    fun initEmbossKernelV1(): Kernel3f {
        mat3f.set(
            -2.0f, -2.0f, +0.0f,
            -2.0f, +6.0f, +0.0f,
            +0.0f, +0.0f, +0.0f
        )
        return this
    }

    /**
     * Initializes the emboss kernel.
     *
     *      ( -2.0, -1.0,  0.0 )
     *      ( -1.0,  0.0,  1.0 )
     *      (  0.0,  1.0,  2.0 )
     *
     * @return this
     */
    fun initEmbossKernelV2(): Kernel3f {
        mat3f.set(
            -2.0f, -1.0f, +0.0f,
            -1.0f, +0.0f, +1.0f,
            +0.0f, +1.0f, +2.0f
        )
        return this
    }

}
