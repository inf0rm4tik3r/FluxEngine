package de.colibriengine.math.matrix.mat3f

import de.colibriengine.math.matrix.mat2f.Mat2f
import de.colibriengine.math.quaternion.quaternionF.QuaternionF
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

/**
 * StdImmutableMat3f.
 *
 * 
 */
class StdImmutableMat3f private constructor(
    m00: Float, m01: Float, m02: Float,
    m10: Float, m11: Float, m12: Float,
    m20: Float, m21: Float, m22: Float
) : ImmutableMat3f {

    private val mutableAccessor: Mat3fAccessor = StdMat3f(
        m00, m01, m02,
        m10, m11, m12,
        m20, m21, m22
    )

    override val m00: Float = mutableAccessor.m00
    override val m01: Float = mutableAccessor.m01
    override val m02: Float = mutableAccessor.m02
    override val m10: Float = mutableAccessor.m10
    override val m11: Float = mutableAccessor.m11
    override val m12: Float = mutableAccessor.m12
    override val m20: Float = mutableAccessor.m20
    override val m21: Float = mutableAccessor.m21
    override val m22: Float = mutableAccessor.m22

    override fun get(row: Int, column: Int): Float = mutableAccessor[row, column]
    override fun getRow(row: Int, storeIn: Vec3f): Vec3f = mutableAccessor.getRow(row, storeIn)
    override fun getRow1(storeIn: Vec3f): Vec3f = mutableAccessor.getRow1(storeIn)
    override fun getRow2(storeIn: Vec3f): Vec3f = mutableAccessor.getRow2(storeIn)
    override fun getRow3(storeIn: Vec3f): Vec3f = mutableAccessor.getRow3(storeIn)

    override fun getColumn(column: Int, storeIn: Vec3f): Vec3f = mutableAccessor.getColumn(column, storeIn)
    override fun getColumn1(storeIn: Vec3f): Vec3f = mutableAccessor.getColumn1(storeIn)
    override fun getColumn2(storeIn: Vec3f): Vec3f = mutableAccessor.getColumn2(storeIn)
    override fun getColumn3(storeIn: Vec3f): Vec3f = mutableAccessor.getColumn3(storeIn)

    override fun getMajorDiagonal(storeIn: Vec3f): Vec3f = mutableAccessor.getMajorDiagonal(storeIn)

    override fun getRight(storeIn: Vec3f): Vec3f = mutableAccessor.getRight(storeIn)
    override fun getUp(storeIn: Vec3f): Vec3f = mutableAccessor.getUp(storeIn)
    override fun getForward(storeIn: Vec3f): Vec3f = mutableAccessor.getForward(storeIn)

    override fun asQuaternion(storeIn: QuaternionF): QuaternionF = mutableAccessor.asQuaternion(storeIn)

    override fun asArray(): Array<out FloatArray> = mutableAccessor.asArray()
    override fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray> = mutableAccessor.asArray(storeIn)
    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun getSubmatrix(row: Int, column: Int, storeIn: Mat2f): Mat2f =
        mutableAccessor.getSubmatrix(row, column, storeIn)

    override fun transform(vector: Vec3f): Vec3f = mutableAccessor.transform(vector)

    override fun determinant(): Float = mutableAccessor.determinant()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString()

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableMat3f.Builder {
        private var m00: Float = 0.0f
        private var m01: Float = 0.0f
        private var m02: Float = 0.0f
        private var m10: Float = 0.0f
        private var m11: Float = 0.0f
        private var m12: Float = 0.0f
        private var m20: Float = 0.0f
        private var m21: Float = 0.0f
        private var m22: Float = 0.0f

        override fun of(value: Float): ImmutableMat3f = set(value).build()

        override fun of(
            m00: Float, m01: Float, m02: Float,
            m10: Float, m11: Float, m12: Float,
            m20: Float, m21: Float, m22: Float
        ): ImmutableMat3f {
            return set(
                m00, m01, m02,
                m10, m11, m12,
                m20, m21, m22
            ).build()
        }

        override fun of(other: Mat3fAccessor): ImmutableMat3f = set(other).build()

        override fun ofColumns(
            column1: Vec3fAccessor,
            column2: Vec3fAccessor,
            column3: Vec3fAccessor
        ): ImmutableMat3f {
            return setColumn1(column1)
                .setColumn2(column2)
                .setColumn3(column3)
                .build()
        }

        override fun ofRows(row1: Vec3fAccessor, row2: Vec3fAccessor, row3: Vec3fAccessor): ImmutableMat3f {
            return setRow1(row1)
                .setRow2(row2)
                .setRow3(row3)
                .build()
        }

        override fun set00(m00: Float): ImmutableMat3f.Builder {
            this.m00 = m00
            return this
        }

        override fun set01(m01: Float): ImmutableMat3f.Builder {
            this.m01 = m01
            return this
        }

        override fun set02(m02: Float): ImmutableMat3f.Builder {
            this.m02 = m02
            return this
        }

        override fun set10(m10: Float): ImmutableMat3f.Builder {
            this.m10 = m10
            return this
        }

        override fun set11(m11: Float): ImmutableMat3f.Builder {
            this.m11 = m11
            return this
        }

        override fun set12(m12: Float): ImmutableMat3f.Builder {
            this.m12 = m12
            return this
        }

        override fun set20(m20: Float): ImmutableMat3f.Builder {
            this.m20 = m20
            return this
        }

        override fun set21(m21: Float): ImmutableMat3f.Builder {
            this.m21 = m21
            return this
        }

        override fun set22(m22: Float): ImmutableMat3f.Builder {
            this.m22 = m22
            return this
        }

        override fun set(value: Float): ImmutableMat3f.Builder {
            m00 = value
            m01 = value
            m02 = value
            m10 = value
            m11 = value
            m12 = value
            m20 = value
            m21 = value
            m22 = value
            return this
        }

        override fun set(
            m00: Float, m01: Float, m02: Float,
            m10: Float, m11: Float, m12: Float,
            m20: Float, m21: Float, m22: Float
        ): ImmutableMat3f.Builder {
            this.m00 = m00
            this.m01 = m01
            this.m02 = m02
            this.m10 = m10
            this.m11 = m11
            this.m12 = m12
            this.m20 = m20
            this.m21 = m21
            this.m22 = m22
            return this
        }

        override fun set(other: Mat3fAccessor): ImmutableMat3f.Builder {
            m00 = other.m00
            m01 = other.m01
            m02 = other.m02
            m10 = other.m10
            m11 = other.m11
            m12 = other.m12
            m20 = other.m20
            m21 = other.m21
            m22 = other.m22
            return this
        }

        override fun setColumn1(column1: Vec3fAccessor): ImmutableMat3f.Builder {
            m00 = column1.x
            m10 = column1.y
            m20 = column1.z
            return this
        }

        override fun setColumn2(column2: Vec3fAccessor): ImmutableMat3f.Builder {
            m01 = column2.x
            m11 = column2.y
            m21 = column2.z
            return this
        }

        override fun setColumn3(column3: Vec3fAccessor): ImmutableMat3f.Builder {
            m02 = column3.x
            m12 = column3.y
            m22 = column3.z
            return this
        }

        override fun setRow1(row1: Vec3fAccessor): ImmutableMat3f.Builder {
            m00 = row1.x
            m01 = row1.y
            m02 = row1.z
            return this
        }

        override fun setRow2(row2: Vec3fAccessor): ImmutableMat3f.Builder {
            m10 = row2.x
            m11 = row2.y
            m12 = row2.z
            return this
        }

        override fun setRow3(row3: Vec3fAccessor): ImmutableMat3f.Builder {
            m20 = row3.x
            m21 = row3.y
            m22 = row3.z
            return this
        }

        override fun build(): ImmutableMat3f {
            return StdImmutableMat3f(
                m00, m01, m02,
                m10, m11, m12,
                m20, m21, m22
            )
        }
    }

    companion object {
        fun builder(): ImmutableMat3f.Builder = Builder()
    }

}

inline fun buildImmutableMat3f(builderAction: ImmutableMat3f.Builder.() -> Unit): ImmutableMat3f {
    return StdImmutableMat3f.builder().apply(builderAction).build()
}
