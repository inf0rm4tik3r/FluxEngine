package de.colibriengine.math.matrix.mat3f

import de.colibriengine.math.matrix.mat2f.Mat2f
import de.colibriengine.math.matrix.mat2f.StdMat2f
import de.colibriengine.math.matrix.mat3d.Mat3dAccessor
import de.colibriengine.math.quaternion.quaternionF.QuaternionF
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.options.Options
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.*

/**
 * Mutable 3x3 matrix in float precision.
 *
 * 
 */
@Suppress("DuplicatedCode")
class StdMat3f(
    /* First row */
    override var m00: Float = 0.0f,
    override var m01: Float = 0.0f,
    override var m02: Float = 0.0f,
    /* Second row */
    override var m10: Float = 0.0f,
    override var m11: Float = 0.0f,
    override var m12: Float = 0.0f,
    /* Third row */
    override var m20: Float = 0.0f,
    override var m21: Float = 0.0f,
    override var m22: Float = 0.0f
) : Mat3f {

    private var _view: Mat3fView? = null
    override val view: Mat3fView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    override fun setM00(value: Float): Mat3f {
        m00 = value
        return this
    }

    override fun setM01(value: Float): Mat3f {
        m01 = value
        return this
    }

    override fun setM02(value: Float): Mat3f {
        m02 = value
        return this
    }

    override fun setM10(value: Float): Mat3f {
        m10 = value
        return this
    }

    override fun setM11(value: Float): Mat3f {
        m11 = value
        return this
    }

    override fun setM12(value: Float): Mat3f {
        m12 = value
        return this
    }

    override fun setM20(value: Float): Mat3f {
        m20 = value
        return this
    }

    override fun setM21(value: Float): Mat3f {
        m21 = value
        return this
    }

    override fun setM22(value: Float): Mat3f {
        m22 = value
        return this
    }

    override fun set(row: Int, column: Int, value: Float): Mat3f {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        if (Options.ARGUMENT_CHECKS) {
            if (row !in 0..2 || column !in 0..2) {
                throw IllegalArgumentException(
                    "Row($row) and column($column) indices were not both in range [0..2]!"
                )
            }
        }
        when (row) {
            0 -> when (column) {
                0 -> m00 = value
                1 -> m01 = value
                2 -> m02 = value
            }
            1 -> when (column) {
                0 -> m10 = value
                1 -> m11 = value
                2 -> m12 = value
            }
            2 -> when (column) {
                0 -> m20 = value
                1 -> m21 = value
                2 -> m22 = value
            }
        }
        return this
    }

    override fun setRow(row: Int, x: Float, y: Float, z: Float): Mat3f {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        when (row) {
            0 -> {
                m00 = x
                m01 = y
                m02 = z
            }
            1 -> {
                m10 = x
                m11 = y
                m12 = z
            }
            2 -> {
                m20 = x
                m21 = y
                m22 = z
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..2]!")
    }

    override fun setRow1(x: Float, y: Float, z: Float): Mat3f {
        m00 = x
        m01 = y
        m02 = z
        return this
    }

    override fun setRow2(x: Float, y: Float, z: Float): Mat3f {
        m10 = x
        m11 = y
        m12 = z
        return this
    }

    override fun setRow3(x: Float, y: Float, z: Float): Mat3f {
        m20 = x
        m21 = y
        m22 = z
        return this
    }

    override fun setRow(row: Int, vector: Vec3fAccessor): Mat3f {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        when (row) {
            0 -> {
                m00 = vector.x
                m01 = vector.y
                m02 = vector.z
            }
            1 -> {
                m10 = vector.x
                m11 = vector.y
                m12 = vector.z
            }
            2 -> {
                m20 = vector.x
                m21 = vector.y
                m22 = vector.z
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..2]!")
    }

    override fun setRow1(vector: Vec3fAccessor): Mat3f {
        m00 = vector.x
        m01 = vector.y
        m02 = vector.z
        return this
    }

    override fun setRow2(vector: Vec3fAccessor): Mat3f {
        m10 = vector.x
        m11 = vector.y
        m12 = vector.z
        return this
    }

    override fun setRow3(vector: Vec3fAccessor): Mat3f {
        m20 = vector.x
        m21 = vector.y
        m22 = vector.z
        return this
    }

    override fun setColumn(column: Int, x: Float, y: Float, z: Float): Mat3f {
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        when (column) {
            0 -> {
                m00 = x
                m10 = y
                m20 = z
            }
            1 -> {
                m01 = x
                m11 = y
                m21 = z
            }
            2 -> {
                m02 = x
                m12 = y
                m22 = z
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..2]!")
    }

    override fun setColumn1(x: Float, y: Float, z: Float): Mat3f {
        m00 = x
        m10 = y
        m20 = z
        return this
    }

    override fun setColumn2(x: Float, y: Float, z: Float): Mat3f {
        m01 = x
        m11 = y
        m21 = z
        return this
    }

    override fun setColumn3(x: Float, y: Float, z: Float): Mat3f {
        m02 = x
        m12 = y
        m22 = z
        return this
    }

    override fun setColumn(column: Int, vector: Vec3fAccessor): Mat3f {
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        when (column) {
            0 -> {
                m00 = vector.x
                m10 = vector.y
                m20 = vector.z
            }
            1 -> {
                m01 = vector.x
                m11 = vector.y
                m21 = vector.z
            }
            2 -> {
                m02 = vector.x
                m12 = vector.y
                m22 = vector.z
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..2]!")
    }

    override fun setColumn1(vector: Vec3fAccessor): Mat3f {
        m00 = vector.x
        m10 = vector.y
        m20 = vector.z
        return this
    }

    override fun setColumn2(vector: Vec3fAccessor): Mat3f {
        m01 = vector.x
        m11 = vector.y
        m21 = vector.z
        return this
    }

    override fun setColumn3(vector: Vec3fAccessor): Mat3f {
        m02 = vector.x
        m12 = vector.y
        m22 = vector.z
        return this
    }

    override fun set(value: Float): Mat3f {
        m22 = value
        m21 = m22
        m20 = m21
        m12 = m20
        m11 = m12
        m10 = m11
        m02 = m10
        m01 = m02
        m00 = m01
        return this
    }

    override fun set(
        m00: Float, m01: Float, m02: Float,
        m10: Float, m11: Float, m12: Float,
        m20: Float, m21: Float, m22: Float
    ): Mat3f {
        this.m00 = m00
        this.m01 = m01
        this.m02 = m02
        this.m10 = m10
        this.m11 = m11
        this.m12 = m12
        this.m20 = m20
        this.m21 = m21
        this.m22 = m22
        return this
    }

    override fun set(other: Mat3fAccessor): Mat3f {
        m00 = other.m00
        m01 = other.m01
        m02 = other.m02
        m10 = other.m10
        m11 = other.m11
        m12 = other.m12
        m20 = other.m20
        m21 = other.m21
        m22 = other.m22
        return this
    }

    override fun set(other: Mat3dAccessor): Mat3f {
        m00 = other.m00.toFloat()
        m01 = other.m01.toFloat()
        m02 = other.m02.toFloat()
        m10 = other.m10.toFloat()
        m11 = other.m11.toFloat()
        m12 = other.m12.toFloat()
        m20 = other.m20.toFloat()
        m21 = other.m21.toFloat()
        m22 = other.m22.toFloat()
        return this
    }

    override fun set(matrixData: Array<FloatArray>): Mat3f {
        require(!(matrixData.size != Mat3f.ROW_AMT || matrixData[0].size != Mat3f.COLUMN_AMT)) {
            "Array dimensions do not match. Copy not possible"
        }
        m00 = matrixData[0][0]
        m01 = matrixData[0][1]
        m02 = matrixData[0][2]
        m10 = matrixData[1][0]
        m11 = matrixData[1][1]
        m12 = matrixData[1][2]
        m20 = matrixData[2][0]
        m21 = matrixData[2][1]
        m22 = matrixData[2][2]
        return this
    }

    override fun initZero(): Mat3f = set(0.0f)

    override fun initIdentity(): Mat3f {
        m21 = 0.0f
        m20 = m21
        m12 = m20
        m10 = m12
        m02 = m10
        m01 = m02
        m22 = 1.0f
        m11 = m22
        m00 = m11
        return this
    }

    override fun initRotationX(angleAroundX: Float): Mat3f {
        val angleAroundXInRad = Math.toRadians(angleAroundX.toDouble())
        val sinX = sin(angleAroundXInRad).toFloat()
        val cosX = cos(angleAroundXInRad).toFloat()
        return set(
            1.0f, 0.0f, 0.0f, 0.0f, cosX, sinX, 0.0f, -sinX, cosX
        )
    }

    override fun initRotationY(angleAroundY: Float): Mat3f {
        val angleAroundYInRad = Math.toRadians(angleAroundY.toDouble())
        val sinY = sin(angleAroundYInRad).toFloat()
        val cosY = cos(angleAroundYInRad).toFloat()
        return set(
            cosY, 0.0f, -sinY, 0.0f, 1.0f, 0.0f,
            sinY, 0.0f, cosY
        )
    }

    override fun initRotationZ(angleAroundZ: Float): Mat3f {
        val angleAroundZInRad = Math.toRadians(angleAroundZ.toDouble())
        val sinZ = sin(angleAroundZInRad).toFloat()
        val cosZ = cos(angleAroundZInRad).toFloat()
        return set(
            cosZ, -sinZ, 0.0f,
            sinZ, cosZ, 0.0f, 0.0f, 0.0f, 1.0f
        )
    }

    /*
    fun initRotationSLOW(
        angleAroundX: Float,
        angleAroundY: Float,
        angleAroundZ: Float
    ): Mat3f {
        val yRotation: Mat3f = StdMat3f().initRotationY(angleAroundY)
        val xRotation: Mat3f = StdMat3f().initRotationX(angleAroundX)
        return initRotationZ(angleAroundZ).times(yRotation).times(xRotation)
    }

    fun initRotationSLOW(anglesView: Vec3fAccessor): Mat3f = initRotationSLOW(anglesView.x, anglesView.y, anglesView.z)
    */

    override fun initRotation(
        angleAroundX: Float,
        angleAroundY: Float,
        angleAroundZ: Float
    ): Mat3f {
        val angleAroundXInRad = Math.toRadians(angleAroundX.toDouble())
        val angleAroundYInRad = Math.toRadians(angleAroundY.toDouble())
        val angleAroundZInRad = Math.toRadians(angleAroundZ.toDouble())
        val sinX = sin(angleAroundXInRad).toFloat()
        val cosX = cos(angleAroundXInRad).toFloat()
        val sinY = sin(angleAroundYInRad).toFloat()
        val cosY = cos(angleAroundYInRad).toFloat()
        val sinZ = sin(angleAroundZInRad).toFloat()
        val cosZ = cos(angleAroundZInRad).toFloat()
        return set(
            cosY * cosZ, cosX * sinZ + sinX * sinY * cosZ, sinX * sinZ - cosX * sinY * cosZ,
            -cosY * sinZ, cosX * cosZ - sinX * sinY * sinZ, sinX * cosZ + cosX * sinY * sinZ,
            sinY, -sinX * cosY, cosX * cosY
        )
    }

    override fun initRotation(angles: Vec3fAccessor): Mat3f = initRotation(angles.x, angles.y, angles.z)

    override fun initRotation(axis: Vec3fAccessor, angle: Float): Mat3f {
        // 											 (+0,  -Rz, +Ry)
        // M = r*rT + cos(a) * (I - r*rT) + sin(a) * (+Rz, +0,  -Rx)
        //											 (-Ry, +Rx, +0 )
        val normalizedAxis: StdVec3f = StdVec3f().set(axis).normalize()
        val rot = StdMat3f(
            0.0f, -axis.z, +axis.y,
            +axis.z, 0.0f, -axis.x,
            -axis.y, +axis.x, 0.0f
        )
        val xx: Float = normalizedAxis.x * normalizedAxis.x
        val xy: Float = normalizedAxis.x * normalizedAxis.y
        val xz: Float = normalizedAxis.x * normalizedAxis.z
        val yy: Float = normalizedAxis.y * normalizedAxis.y
        val yz: Float = normalizedAxis.y * normalizedAxis.z
        val zz: Float = normalizedAxis.z * normalizedAxis.z

        //r*r^T
        set(
            xx, xy, xz,
            xy, yy, yz,
            xz, yz, zz
        )
        val angleInRad = Math.toRadians(angle.toDouble())
        val sin = sin(angleInRad).toFloat()
        val cos = cos(angleInRad).toFloat()
        val term1 = StdMat3f().initIdentity().minus(this).times(cos)
        val term2 = rot.times(sin)
        return plus(term1).plus(term2)
    }

    override fun transform(vector: Vec3f): Vec3f {
        return vector.set(
            m00 * vector.x + m01 * vector.y + m02 * vector.z,
            m10 * vector.x + m11 * vector.y + m12 * vector.z,
            m20 * vector.x + m21 * vector.y + m22 * vector.z
        )
    }

    override fun plus(value: Float): Mat3f {
        m00 += value
        m01 += value
        m02 += value
        m10 += value
        m11 += value
        m12 += value
        m20 += value
        m21 += value
        m22 += value
        return this
    }

    override fun plus(other: Mat3fAccessor): Mat3f {
        m00 += other.m00
        m01 += other.m01
        m02 += other.m02
        m10 += other.m10
        m11 += other.m11
        m12 += other.m12
        m20 += other.m20
        m21 += other.m21
        m22 += other.m22
        return this
    }

    override fun minus(value: Float): Mat3f {
        m00 -= value
        m01 -= value
        m02 -= value
        m10 -= value
        m11 -= value
        m12 -= value
        m20 -= value
        m21 -= value
        m22 -= value
        return this
    }

    override fun minus(other: Mat3fAccessor): Mat3f {
        m00 -= other.m00
        m01 -= other.m01
        m02 -= other.m02
        m10 -= other.m10
        m11 -= other.m11
        m12 -= other.m12
        m20 -= other.m20
        m21 -= other.m21
        m22 -= other.m22
        return this
    }

    override fun times(value: Float): Mat3f {
        m00 *= value
        m01 *= value
        m02 *= value
        m10 *= value
        m11 *= value
        m12 *= value
        m20 *= value
        m21 *= value
        m22 *= value
        return this
    }

    override fun times(other: Mat3fAccessor): Mat3f {
        assert(this !== other) { "Cannot multiply with itself!" }
        // First row.
        var r0: Float = m00 * other.m00 + m01 * other.m10 + m02 * other.m20
        var r1: Float = m00 * other.m01 + m01 * other.m11 + m02 * other.m21
        var r2: Float = m00 * other.m02 + m01 * other.m12 + m02 * other.m22
        m00 = r0
        m01 = r1
        m02 = r2
        // Second row.
        r0 = m10 * other.m00 + m11 * other.m10 + m12 * other.m20
        r1 = m10 * other.m01 + m11 * other.m11 + m12 * other.m21
        r2 = m10 * other.m02 + m11 * other.m12 + m12 * other.m22
        m10 = r0
        m11 = r1
        m12 = r2
        // Third row.
        r0 = m20 * other.m00 + m21 * other.m10 + m22 * other.m20
        r1 = m20 * other.m01 + m21 * other.m11 + m22 * other.m21
        r2 = m20 * other.m02 + m21 * other.m12 + m22 * other.m22
        m20 = r0
        m21 = r1
        m22 = r2
        return this
    }

    override fun mulSelf(): Mat3f {
        // First row.
        val r00 = m00 * m00 + m01 * m10 + m02 * m20
        val r01 = m00 * m01 + m01 * m11 + m02 * m21
        val r02 = m00 * m02 + m01 * m12 + m02 * m22
        // Second row.
        val r10 = m10 * m00 + m11 * m10 + m12 * m20
        val r11 = m10 * m01 + m11 * m11 + m12 * m21
        val r12 = m10 * m02 + m11 * m12 + m12 * m22
        // Third row.
        val r20 = m20 * m00 + m21 * m10 + m22 * m20
        val r21 = m20 * m01 + m21 * m11 + m22 * m21
        val r22 = m20 * m02 + m21 * m12 + m22 * m22
        // Update this instance.
        m00 = r00
        m01 = r01
        m02 = r02
        m10 = r10
        m11 = r11
        m12 = r12
        m20 = r20
        m21 = r21
        m22 = r22
        return this
    }

    override fun inverse(): Mat3f {
        // Calculate 1 over the determinant of the initial matrix.
        val oneOverDeterminant = 1.0f / determinant()

        // Calculate cofactors using submatrix determinants.
        val c00 = +StdMat2f.determinantOf(m11, m12, m21, m22)
        val c01 = -StdMat2f.determinantOf(m10, m12, m20, m22)
        val c02 = +StdMat2f.determinantOf(m10, m11, m20, m21)
        val c10 = -StdMat2f.determinantOf(m01, m02, m21, m22)
        val c11 = +StdMat2f.determinantOf(m00, m02, m20, m22)
        val c12 = -StdMat2f.determinantOf(m00, m01, m20, m21)
        val c20 = +StdMat2f.determinantOf(m01, m02, m11, m12)
        val c21 = -StdMat2f.determinantOf(m00, m02, m10, m12)
        val c22 = +StdMat2f.determinantOf(m00, m01, m10, m11)

        // ... and return the result multiplied with the transposed cofactor matrix.
        return set(c00, c01, c02, c10, c11, c12, c20, c21, c22).transpose().times(oneOverDeterminant)
    }

    override fun transpose(): Mat3f {
        // Swap 10 with 01.
        var temp: Float = m10
        m10 = m01
        m01 = temp
        // Swap 21 with 12.
        temp = m21
        m21 = m12
        m12 = temp
        // Swap 20 with 02.
        temp = m20
        m20 = m02
        m02 = temp
        return this
    }

    @Suppress("ReplaceGetOrSet")
    override fun getSubmatrix(row: Int, column: Int, storeIn: Mat2f): Mat2f {
        assert(row in 0..2) { "Row to ignore (row) is not in range [0..2]!" }
        assert(column in 0..2) { "Column to ignore (column) is not in range [0..2]!" }
        when (row) {
            0 -> when (column) {
                0 -> return storeIn.set(m11, m12, m21, m22)
                1 -> return storeIn.set(m10, m12, m20, m22)
                2 -> return storeIn.set(m10, m11, m20, m21)
            }
            1 -> when (column) {
                0 -> return storeIn.set(m01, m02, m21, m22)
                1 -> return storeIn.set(m00, m02, m20, m22)
                2 -> return storeIn.set(m00, m01, m20, m21)
            }
            2 -> when (column) {
                0 -> return storeIn.set(m01, m02, m11, m12)
                1 -> return storeIn.set(m00, m02, m10, m12)
                2 -> return storeIn.set(m00, m01, m10, m11)
            }
        }
        throw IllegalArgumentException("Row or column not in range [0..2]!")
    }

    override fun pow(exponent: Int): Mat3f {
        require(exponent >= -1) { "Exponents below -1 are not supported. Given: $exponent" }
        return when (exponent) {
            -1 -> inverse()
            0 -> initIdentity()
            1 -> this
            else -> {
                var i = 1
                while (i < exponent) {
                    mulSelf()
                    i++
                }
                this
            }
        }
    }

    override fun abs(): Mat3f {
        m00 = m00.absoluteValue
        m01 = m01.absoluteValue
        m02 = m02.absoluteValue
        m10 = m10.absoluteValue
        m11 = m11.absoluteValue
        m12 = m12.absoluteValue
        m20 = m20.absoluteValue
        m21 = m21.absoluteValue
        m22 = m22.absoluteValue
        return this
    }

    override fun shorten(): Mat3f {
        m00 = m00.toFourDecimalPlaces()
        m01 = m01.toFourDecimalPlaces()
        m02 = m02.toFourDecimalPlaces()
        m10 = m10.toFourDecimalPlaces()
        m11 = m11.toFourDecimalPlaces()
        m12 = m12.toFourDecimalPlaces()
        m20 = m20.toFourDecimalPlaces()
        m21 = m21.toFourDecimalPlaces()
        m22 = m22.toFourDecimalPlaces()
        return this
    }

    override fun get(row: Int, column: Int): Float {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        when (row) {
            0 -> when (column) {
                0 -> return m00
                1 -> return m01
                2 -> return m02
            }
            1 -> when (column) {
                0 -> return m10
                1 -> return m11
                2 -> return m12
            }
            2 -> when (column) {
                0 -> return m20
                1 -> return m21
                2 -> return m22
            }
        }
        throw IllegalArgumentException(
            "Row($row) and column($column) indices were not both in range [0..2]!"
        )
    }

    override fun getRow(row: Int, storeIn: Vec3f): Vec3f {
        assert(row in 0..2) { "Row index was not in range [0..2]!" }
        when (row) {
            0 -> return storeIn.set(m00, m01, m02)
            1 -> return storeIn.set(m10, m11, m12)
            2 -> return storeIn.set(m20, m21, m22)
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..2]!")
    }

    override fun getRow1(storeIn: Vec3f): Vec3f = storeIn.set(m00, m01, m02)

    override fun getRow2(storeIn: Vec3f): Vec3f = storeIn.set(m10, m11, m12)

    override fun getRow3(storeIn: Vec3f): Vec3f = storeIn.set(m20, m21, m22)

    override fun getColumn(column: Int, storeIn: Vec3f): Vec3f {
        assert(column in 0..2) { "Column index was not in range [0..2]!" }
        when (column) {
            0 -> return storeIn.set(m00, m10, m20)
            1 -> return storeIn.set(m01, m11, m21)
            2 -> return storeIn.set(m02, m12, m22)
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..2]!")
    }

    override fun getColumn1(storeIn: Vec3f): Vec3f = storeIn.set(m00, m10, m20)

    override fun getColumn2(storeIn: Vec3f): Vec3f = storeIn.set(m01, m11, m21)

    override fun getColumn3(storeIn: Vec3f): Vec3f = storeIn.set(m02, m12, m22)

    override fun getMajorDiagonal(storeIn: Vec3f): Vec3f = storeIn.set(m00, m11, m22)

    override fun getRight(storeIn: Vec3f): Vec3f = storeIn.set(m00, m01, m02)

    override fun getUp(storeIn: Vec3f): Vec3f = storeIn.set(m10, m11, m12)

    override fun getForward(storeIn: Vec3f): Vec3f = storeIn.set(m20, m21, m22)

    override fun asArray(): Array<out FloatArray> = arrayOf(
        floatArrayOf(m00, m01, m02),
        floatArrayOf(m10, m11, m12),
        floatArrayOf(m20, m21, m22)
    )

    override fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray> {
        assert(storeIn.size == Mat3f.ROW_AMT && storeIn[0].size == Mat3f.COLUMN_AMT) {
            "Given data array is not of size 3x3!"
        }
        storeIn[0][0] = m00
        storeIn[0][1] = m01
        storeIn[0][2] = m02
        storeIn[1][0] = m10
        storeIn[1][1] = m11
        storeIn[1][2] = m12
        storeIn[2][0] = m20
        storeIn[2][1] = m21
        storeIn[2][2] = m22
        return storeIn
    }

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Mat3f.BYTES)).array()
    }

    override fun asQuaternion(storeIn: QuaternionF): QuaternionF {
        val t: Float
        if (m22 < 0.0f) {
            if (m00 > m11) {
                t = 1.0f + m00 - m11 - m22
                storeIn.set(t, m01 + m10, m20 + m02, m12 - m21)
            } else {
                t = 1.0f - m00 + m11 - m22
                storeIn.set(m01 + m10, t, m12 + m21, m20 - m02)
            }
        } else {
            if (m00 < -m11) {
                t = 1.0f - m00 - m11 + m22
                storeIn.set(m20 + m02, m12 + m21, t, m01 - m10)
            } else {
                t = 1.0f + m00 + m11 + m22
                storeIn.set(m12 - m21, m20 - m02, m01 - m10, t)
            }
        }
        return storeIn.times(0.5f / sqrt(t))
    }

    override fun determinant(): Float {
        return m00 * m11 * m22 +
            m01 * m12 * m20 +
            m02 * m10 * m21 -
            m20 * m11 * m02 -
            m21 * m12 * m00 -
            m22 * m10 * m01
    }

    override fun hasZeroComponent(): Boolean {
        return m00 == 0.0f || m01 == 0.0f || m02 == 0.0f ||
            m10 == 0.0f || m11 == 0.0f || m12 == 0.0f ||
            m20 == 0.0f || m21 == 0.0f || m22 == 0.0f
    }

    override fun toFormattedString(): String = toString()

    override fun hashCode(): Int {
        var result = 17
        result = 31 * result + java.lang.Float.hashCode(m00)
        result = 31 * result + java.lang.Float.hashCode(m01)
        result = 31 * result + java.lang.Float.hashCode(m02)
        result = 31 * result + java.lang.Float.hashCode(m10)
        result = 31 * result + java.lang.Float.hashCode(m11)
        result = 31 * result + java.lang.Float.hashCode(m12)
        result = 31 * result + java.lang.Float.hashCode(m20)
        result = 31 * result + java.lang.Float.hashCode(m21)
        result = 31 * result + java.lang.Float.hashCode(m22)
        return result
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Mat3f -> false
            other === this -> true
            else -> m00 == other.m00 && m01 == other.m01 && m02 == other.m02 &&
                m10 == other.m10 && m11 == other.m11 && m12 == other.m12 &&
                m20 == other.m20 && m21 == other.m21 && m22 == other.m22
        }
    }

    override fun toString(): String = buildString {
        append("Mat3f ( ")
        appendValue(this, m00)
        append(" | ")
        appendValue(this, m01)
        append(" | ")
        appendValue(this, m02)
        append(" )\n      ( ")
        appendValue(this, m10)
        append(" | ")
        appendValue(this, m11)
        append(" | ")
        appendValue(this, m12)
        append(" )\n      ( ")
        appendValue(this, m20)
        append(" | ")
        appendValue(this, m21)
        append(" | ")
        appendValue(this, m22)
        append(" )")
    }

    private fun appendValue(builder: StringBuilder, value: Float) {
        // Add a whitespace if the current value is not negative. Otherwise we will see a "-" sign.
        builder.append(if (value > 0) " " + valueAsString(value) else valueAsString(value))
    }

    private fun valueAsString(value: Float): String = buildString {
        append(value.toFourDecimalPlaces())
        append(" ".repeat(max(0, 4 - toString().split('.')[1].length)))
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        buffer.putFloat(m00)
        buffer.putFloat(m01)
        buffer.putFloat(m02)
        buffer.putFloat(m10)
        buffer.putFloat(m11)
        buffer.putFloat(m12)
        buffer.putFloat(m20)
        buffer.putFloat(m21)
        buffer.putFloat(m22)
        return buffer
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        buffer.put(m00)
        buffer.put(m01)
        buffer.put(m02)
        buffer.put(m10)
        buffer.put(m11)
        buffer.put(m12)
        buffer.put(m20)
        buffer.put(m21)
        buffer.put(m22)
        return buffer
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        buffer.put(m00.toDouble())
        buffer.put(m01.toDouble())
        buffer.put(m02.toDouble())
        buffer.put(m10.toDouble())
        buffer.put(m11.toDouble())
        buffer.put(m12.toDouble())
        buffer.put(m20.toDouble())
        buffer.put(m21.toDouble())
        buffer.put(m22.toDouble())
        return buffer
    }

    /**
     * Mat3f viewer, granting read-only access to its parent data.
     *
     * @author Lukas Potthast
     * 
     */
    inner class Viewer : Mat3fView {
        override val m00: Float = this@StdMat3f.m00
        override val m01: Float = this@StdMat3f.m01
        override val m02: Float = this@StdMat3f.m02
        override val m10: Float = this@StdMat3f.m10
        override val m11: Float = this@StdMat3f.m11
        override val m12: Float = this@StdMat3f.m12
        override val m20: Float = this@StdMat3f.m20
        override val m21: Float = this@StdMat3f.m21
        override val m22: Float = this@StdMat3f.m22

        override operator fun get(row: Int, column: Int): Float = this@StdMat3f[row, column]

        override fun getRow(row: Int, storeIn: Vec3f): Vec3f = this@StdMat3f.getRow(row, storeIn)
        override fun getRow1(storeIn: Vec3f): Vec3f = this@StdMat3f.getRow1(storeIn)
        override fun getRow2(storeIn: Vec3f): Vec3f = this@StdMat3f.getRow2(storeIn)
        override fun getRow3(storeIn: Vec3f): Vec3f = this@StdMat3f.getRow3(storeIn)

        override fun getColumn(column: Int, storeIn: Vec3f): Vec3f = this@StdMat3f.getColumn(column, storeIn)
        override fun getColumn1(storeIn: Vec3f): Vec3f = this@StdMat3f.getColumn1(storeIn)
        override fun getColumn2(storeIn: Vec3f): Vec3f = this@StdMat3f.getColumn2(storeIn)
        override fun getColumn3(storeIn: Vec3f): Vec3f = this@StdMat3f.getColumn3(storeIn)

        override fun getMajorDiagonal(storeIn: Vec3f): Vec3f = this@StdMat3f.getMajorDiagonal(storeIn)

        override fun getRight(storeIn: Vec3f): Vec3f = this@StdMat3f.getRight(storeIn)
        override fun getUp(storeIn: Vec3f): Vec3f = this@StdMat3f.getUp(storeIn)
        override fun getForward(storeIn: Vec3f): Vec3f = this@StdMat3f.getForward(storeIn)

        override fun asArray(): Array<out FloatArray> = this@StdMat3f.asArray()
        override fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray> = this@StdMat3f.asArray(storeIn)
        override fun bytes(): ByteArray = this@StdMat3f.bytes()

        override fun asQuaternion(storeIn: QuaternionF): QuaternionF = this@StdMat3f.asQuaternion(storeIn)

        override fun getSubmatrix(row: Int, column: Int, storeIn: Mat2f): Mat2f =
            this@StdMat3f.getSubmatrix(row, column, storeIn)

        override fun transform(vector: Vec3f): Vec3f = this@StdMat3f.transform(vector)

        override fun determinant(): Float = this@StdMat3f.determinant()
        override fun hasZeroComponent(): Boolean = this@StdMat3f.hasZeroComponent()

        override fun toFormattedString(): String = this@StdMat3f.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdMat3f.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdMat3f.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdMat3f.storeIn(buffer)
    }

    companion object : Mat3fStaticOps {
        override fun determinantOf(
            m00: Float, m01: Float, m02: Float,
            m10: Float, m11: Float, m12: Float,
            m20: Float, m21: Float, m22: Float
        ): Float {
            return m00 * m11 * m22 +
                m01 * m12 * m20 +
                m02 * m10 * m21 -
                m20 * m11 * m02 -
                m21 * m12 * m00 -
                m22 * m10 * m01
        }
    }

}
