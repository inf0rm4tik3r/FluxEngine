package de.colibriengine.math.matrix.mat3f

/**
 * StdMat3fFactory.
 *
 * 
 */
class StdMat3fFactory : Mat3fFactory {

    private val pool: StdMat3fPool = StdMat3fPool()

    override fun acquire(): Mat3f = pool.acquire()

    override fun acquireDirty(): Mat3f = pool.acquireDirty()

    override fun free(mat3f: Mat3f) = pool.free(mat3f)

    override fun immutableMat3fBuilder(): ImmutableMat3f.Builder = StdImmutableMat3f.builder()

    override fun zero(): ImmutableMat3f = ZERO

    override fun one(): ImmutableMat3f = ONE

    override fun identity(): ImmutableMat3f = IDENTITY

    companion object {
        val ZERO = StdImmutableMat3f.builder().of(0.0f)
        val ONE = StdImmutableMat3f.builder().of(1.0f)
        val IDENTITY = StdImmutableMat3f.builder().set(
            1.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f
        ).build()
    }

}
