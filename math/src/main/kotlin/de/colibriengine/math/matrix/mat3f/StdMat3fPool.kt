package de.colibriengine.math.matrix.mat3f

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

/**
 * Mat3fPool.
 *
 * 
 */
class StdMat3fPool : ObjectPool<Mat3f>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdMat3f()
    },
    { instance: Mat3f -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdMat3fPool::class.java)
    }

}
