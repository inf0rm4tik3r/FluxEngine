package de.colibriengine.math.matrix.mat4d

import de.colibriengine.math.matrix.mat3d.Mat3d
import de.colibriengine.math.quaternion.quaternionD.QuaternionD
import de.colibriengine.math.vector.vec3d.Vec3d
import de.colibriengine.math.vector.vec4d.Vec4d
import de.colibriengine.math.vector.vec4d.Vec4dAccessor
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

/**
 * StdImmutableMat4d.
 *
 * 
 */
@Suppress("DuplicatedCode")
class StdImmutableMat4d private constructor(
    m00: Double, m01: Double, m02: Double, m03: Double,
    m10: Double, m11: Double, m12: Double, m13: Double,
    m20: Double, m21: Double, m22: Double, m23: Double,
    m30: Double, m31: Double, m32: Double, m33: Double
) : ImmutableMat4d {

    private val mutableAccessor: Mat4dAccessor = StdMat4d(
        m00, m01, m02, m03,
        m10, m11, m12, m13,
        m20, m21, m22, m23,
        m30, m31, m32, m33
    )

    override val m00: Double = mutableAccessor.m00
    override val m01: Double = mutableAccessor.m01
    override val m02: Double = mutableAccessor.m02
    override val m03: Double = mutableAccessor.m03
    override val m10: Double = mutableAccessor.m10
    override val m11: Double = mutableAccessor.m11
    override val m12: Double = mutableAccessor.m12
    override val m13: Double = mutableAccessor.m13
    override val m20: Double = mutableAccessor.m20
    override val m21: Double = mutableAccessor.m21
    override val m22: Double = mutableAccessor.m22
    override val m23: Double = mutableAccessor.m23
    override val m30: Double = mutableAccessor.m30
    override val m31: Double = mutableAccessor.m31
    override val m32: Double = mutableAccessor.m32
    override val m33: Double = mutableAccessor.m33

    override fun get(row: Int, column: Int): Double = mutableAccessor[row, column]
    override fun getRow(row: Int, storeIn: Vec4d): Vec4d = mutableAccessor.getRow(row, storeIn)
    override fun getRow1(storeIn: Vec4d): Vec4d = mutableAccessor.getRow1(storeIn)
    override fun getRow2(storeIn: Vec4d): Vec4d = mutableAccessor.getRow2(storeIn)
    override fun getRow3(storeIn: Vec4d): Vec4d = mutableAccessor.getRow3(storeIn)
    override fun getRow4(storeIn: Vec4d): Vec4d = mutableAccessor.getRow4(storeIn)

    override fun getColumn(column: Int, storeIn: Vec4d): Vec4d = mutableAccessor.getColumn(column, storeIn)
    override fun getColumn1(storeIn: Vec4d): Vec4d = mutableAccessor.getColumn1(storeIn)
    override fun getColumn2(storeIn: Vec4d): Vec4d = mutableAccessor.getColumn2(storeIn)
    override fun getColumn3(storeIn: Vec4d): Vec4d = mutableAccessor.getColumn3(storeIn)
    override fun getColumn4(storeIn: Vec4d): Vec4d = mutableAccessor.getColumn4(storeIn)

    override fun getMajorDiagonal(storeIn: Vec4d): Vec4d = mutableAccessor.getMajorDiagonal(storeIn)

    override fun getRight(storeIn: Vec3d): Vec3d = mutableAccessor.getRight(storeIn)
    override fun getUp(storeIn: Vec3d): Vec3d = mutableAccessor.getUp(storeIn)
    override fun getForward(storeIn: Vec3d): Vec3d = mutableAccessor.getForward(storeIn)

    override fun asQuaternion(storeIn: QuaternionD): QuaternionD = mutableAccessor.asQuaternion(storeIn)

    override fun asArray(): Array<out DoubleArray> = mutableAccessor.asArray()
    override fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray> = mutableAccessor.asArray(storeIn)
    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun getSubmatrix(row: Int, column: Int, storeIn: Mat3d): Mat3d =
        mutableAccessor.getSubmatrix(row, column, storeIn)

    override fun transform(vector: Vec4d): Vec4d = mutableAccessor.transform(vector)

    override fun determinant(): Double = mutableAccessor.determinant()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString()

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableMat4d.Builder {
        private var m00: Double = 0.0
        private var m01: Double = 0.0
        private var m02: Double = 0.0
        private var m03: Double = 0.0
        private var m10: Double = 0.0
        private var m11: Double = 0.0
        private var m12: Double = 0.0
        private var m13: Double = 0.0
        private var m20: Double = 0.0
        private var m21: Double = 0.0
        private var m22: Double = 0.0
        private var m23: Double = 0.0
        private var m30: Double = 0.0
        private var m31: Double = 0.0
        private var m32: Double = 0.0
        private var m33: Double = 0.0

        override fun of(value: Double): ImmutableMat4d = set(value).build()

        override fun of(
            m00: Double, m01: Double, m02: Double, m03: Double,
            m10: Double, m11: Double, m12: Double, m13: Double,
            m20: Double, m21: Double, m22: Double, m23: Double,
            m30: Double, m31: Double, m32: Double, m33: Double
        ): ImmutableMat4d {
            return set(
                m00, m01, m02, m03,
                m10, m11, m12, m13,
                m20, m21, m22, m23,
                m30, m31, m32, m33
            ).build()
        }

        override fun of(other: Mat4dAccessor): ImmutableMat4d = set(other).build()

        override fun ofColumns(
            column1: Vec4dAccessor,
            column2: Vec4dAccessor,
            column3: Vec4dAccessor,
            column4: Vec4dAccessor
        ): ImmutableMat4d {
            return setColumn1(column1)
                .setColumn2(column2)
                .setColumn3(column3)
                .setColumn4(column4)
                .build()
        }

        override fun ofRows(
            row1: Vec4dAccessor,
            row2: Vec4dAccessor,
            row3: Vec4dAccessor,
            row4: Vec4dAccessor
        ): ImmutableMat4d {
            return setRow1(row1)
                .setRow2(row2)
                .setRow3(row3)
                .setRow4(row4)
                .build()
        }

        override fun set00(m00: Double): ImmutableMat4d.Builder {
            this.m00 = m00
            return this
        }

        override fun set01(m01: Double): ImmutableMat4d.Builder {
            this.m01 = m01
            return this
        }

        override fun set02(m02: Double): ImmutableMat4d.Builder {
            this.m02 = m02
            return this
        }

        override fun set03(m03: Double): ImmutableMat4d.Builder {
            this.m03 = m03
            return this
        }

        override fun set10(m10: Double): ImmutableMat4d.Builder {
            this.m10 = m10
            return this
        }

        override fun set11(m11: Double): ImmutableMat4d.Builder {
            this.m11 = m11
            return this
        }

        override fun set12(m12: Double): ImmutableMat4d.Builder {
            this.m12 = m12
            return this
        }

        override fun set13(m13: Double): ImmutableMat4d.Builder {
            this.m13 = m13
            return this
        }

        override fun set20(m20: Double): ImmutableMat4d.Builder {
            this.m20 = m20
            return this
        }

        override fun set21(m21: Double): ImmutableMat4d.Builder {
            this.m21 = m21
            return this
        }

        override fun set22(m22: Double): ImmutableMat4d.Builder {
            this.m22 = m22
            return this
        }

        override fun set23(m23: Double): ImmutableMat4d.Builder {
            this.m23 = m23
            return this
        }

        override fun set30(m30: Double): ImmutableMat4d.Builder {
            this.m30 = m30
            return this
        }

        override fun set31(m31: Double): ImmutableMat4d.Builder {
            this.m31 = m31
            return this
        }

        override fun set32(m32: Double): ImmutableMat4d.Builder {
            this.m32 = m32
            return this
        }

        override fun set33(m33: Double): ImmutableMat4d.Builder {
            this.m33 = m33
            return this
        }

        override fun set(value: Double): ImmutableMat4d.Builder {
            m00 = value
            m01 = value
            m02 = value
            m03 = value
            m10 = value
            m11 = value
            m12 = value
            m13 = value
            m20 = value
            m21 = value
            m22 = value
            m23 = value
            m30 = value
            m31 = value
            m32 = value
            m33 = value
            return this
        }

        override fun set(
            m00: Double, m01: Double, m02: Double, m03: Double,
            m10: Double, m11: Double, m12: Double, m13: Double,
            m20: Double, m21: Double, m22: Double, m23: Double,
            m30: Double, m31: Double, m32: Double, m33: Double
        ): ImmutableMat4d.Builder {
            this.m00 = m00
            this.m01 = m01
            this.m02 = m02
            this.m03 = m03
            this.m10 = m10
            this.m11 = m11
            this.m12 = m12
            this.m13 = m13
            this.m20 = m20
            this.m21 = m21
            this.m22 = m22
            this.m23 = m23
            this.m30 = m30
            this.m31 = m31
            this.m32 = m32
            this.m33 = m33
            return this
        }

        override fun set(other: Mat4dAccessor): ImmutableMat4d.Builder {
            m00 = other.m00
            m01 = other.m01
            m02 = other.m02
            m03 = other.m03
            m10 = other.m10
            m11 = other.m11
            m12 = other.m12
            m13 = other.m13
            m20 = other.m20
            m21 = other.m21
            m22 = other.m22
            m23 = other.m23
            m30 = other.m30
            m31 = other.m31
            m32 = other.m32
            m33 = other.m33
            return this
        }

        override fun setColumn1(column1: Vec4dAccessor): ImmutableMat4d.Builder {
            m00 = column1.x
            m10 = column1.y
            m20 = column1.z
            m30 = column1.w
            return this
        }

        override fun setColumn2(column2: Vec4dAccessor): ImmutableMat4d.Builder {
            m01 = column2.x
            m11 = column2.y
            m21 = column2.z
            m31 = column2.w
            return this
        }

        override fun setColumn3(column3: Vec4dAccessor): ImmutableMat4d.Builder {
            m02 = column3.x
            m12 = column3.y
            m22 = column3.z
            m32 = column3.w
            return this
        }

        override fun setColumn4(column4: Vec4dAccessor): ImmutableMat4d.Builder {
            m03 = column4.x
            m13 = column4.y
            m23 = column4.z
            m33 = column4.w
            return this
        }

        override fun setRow1(row1: Vec4dAccessor): ImmutableMat4d.Builder {
            m00 = row1.x
            m01 = row1.y
            m02 = row1.z
            m03 = row1.w
            return this
        }

        override fun setRow2(row2: Vec4dAccessor): ImmutableMat4d.Builder {
            m10 = row2.x
            m11 = row2.y
            m12 = row2.z
            m13 = row2.w
            return this
        }

        override fun setRow3(row3: Vec4dAccessor): ImmutableMat4d.Builder {
            m20 = row3.x
            m21 = row3.y
            m22 = row3.z
            m23 = row3.w
            return this
        }

        override fun setRow4(row4: Vec4dAccessor): ImmutableMat4d.Builder {
            m30 = row4.x
            m31 = row4.y
            m32 = row4.z
            m33 = row4.w
            return this
        }

        override fun build(): ImmutableMat4d {
            return StdImmutableMat4d(
                m00, m01, m02, m03,
                m10, m11, m12, m13,
                m20, m21, m22, m23,
                m30, m31, m32, m33
            )
        }
    }

    companion object {
        fun builder(): ImmutableMat4d.Builder = Builder()
    }

}

inline fun buildImmutableMat4d(builderAction: ImmutableMat4d.Builder.() -> Unit): ImmutableMat4d {
    return StdImmutableMat4d.builder().apply(builderAction).build()
}
