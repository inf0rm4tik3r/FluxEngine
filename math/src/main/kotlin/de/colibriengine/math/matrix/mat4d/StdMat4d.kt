package de.colibriengine.math.matrix.mat4d

import de.colibriengine.logging.LogUtil
import de.colibriengine.math.matrix.mat3d.Mat3d
import de.colibriengine.math.matrix.mat3d.Mat3dAccessor
import de.colibriengine.math.matrix.mat3d.StdMat3d
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.quaternion.quaternionD.QuaternionD
import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.vector.vec3d.StdVec3d
import de.colibriengine.math.vector.vec3d.Vec3d
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3f.StdVec3fFactory
import de.colibriengine.math.vector.vec4d.Vec4d
import de.colibriengine.math.vector.vec4d.Vec4dAccessor
import de.colibriengine.options.Options
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import de.colibriengine.util.isApproximately
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.absoluteValue
import kotlin.math.tan

/**
 * Mutable 4x4 matrix in double precision.
 *
 *
 */
@Suppress("DuplicatedCode")
class StdMat4d(
    /* First row */
    override var m00: Double = 0.0,
    override var m01: Double = 0.0,
    override var m02: Double = 0.0,
    override var m03: Double = 0.0,
    /* Second row */
    override var m10: Double = 0.0,
    override var m11: Double = 0.0,
    override var m12: Double = 0.0,
    override var m13: Double = 0.0,
    /* Third row */
    override var m20: Double = 0.0,
    override var m21: Double = 0.0,
    override var m22: Double = 0.0,
    override var m23: Double = 0.0,
    /* Fourth row */
    override var m30: Double = 0.0,
    override var m31: Double = 0.0,
    override var m32: Double = 0.0,
    override var m33: Double = 0.0
) : Mat4d {

    private var _view: Mat4dView? = null
    override val view: Mat4dView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    override fun setM00(value: Double): Mat4d {
        m00 = value
        return this
    }

    override fun setM01(value: Double): Mat4d {
        m01 = value
        return this
    }

    override fun setM02(value: Double): Mat4d {
        m02 = value
        return this
    }

    override fun setM03(value: Double): Mat4d {
        m03 = value
        return this
    }

    override fun setM10(value: Double): Mat4d {
        m10 = value
        return this
    }

    override fun setM11(value: Double): Mat4d {
        m11 = value
        return this
    }

    override fun setM12(value: Double): Mat4d {
        m12 = value
        return this
    }

    override fun setM13(value: Double): Mat4d {
        m13 = value
        return this
    }

    override fun setM20(value: Double): Mat4d {
        m20 = value
        return this
    }

    override fun setM21(value: Double): Mat4d {
        m21 = value
        return this
    }

    override fun setM22(value: Double): Mat4d {
        m22 = value
        return this
    }

    override fun setM23(value: Double): Mat4d {
        m23 = value
        return this
    }

    override fun setM30(value: Double): Mat4d {
        m30 = value
        return this
    }

    override fun setM31(value: Double): Mat4d {
        m31 = value
        return this
    }

    override fun setM32(value: Double): Mat4d {
        m32 = value
        return this
    }

    override fun setM33(value: Double): Mat4d {
        m33 = value
        return this
    }

    override fun set(row: Int, column: Int, value: Double): Mat4d {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        if (Options.ARGUMENT_CHECKS) {
            if (row !in 0..3 || column !in 0..3) {
                throw IllegalArgumentException("Row($row) and column($column) indices were not both in range [0..3]!")
            }
        }
        when (row) {
            0 -> when (column) {
                0 -> m00 = value
                1 -> m01 = value
                2 -> m02 = value
                3 -> m03 = value
            }
            1 -> when (column) {
                0 -> m10 = value
                1 -> m11 = value
                2 -> m12 = value
                3 -> m13 = value
            }
            2 -> when (column) {
                0 -> m20 = value
                1 -> m21 = value
                2 -> m22 = value
                3 -> m23 = value
            }
            3 -> when (column) {
                0 -> m30 = value
                1 -> m31 = value
                2 -> m32 = value
                3 -> m33 = value
            }
        }
        return this
    }

    override fun setRow(row: Int, x: Double, y: Double, z: Double, w: Double): Mat4d {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        when (row) {
            0 -> {
                m00 = x
                m01 = y
                m02 = z
                m03 = w
            }
            1 -> {
                m10 = x
                m11 = y
                m12 = z
                m13 = w
            }
            2 -> {
                m20 = x
                m21 = y
                m22 = z
                m23 = w
            }
            3 -> {
                m30 = x
                m31 = y
                m32 = z
                m33 = w
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..3]!")
    }

    override fun setRow1(x: Double, y: Double, z: Double, w: Double): Mat4d {
        m00 = x
        m01 = y
        m02 = z
        m03 = w
        return this
    }

    override fun setRow2(x: Double, y: Double, z: Double, w: Double): Mat4d {
        m10 = x
        m11 = y
        m12 = z
        m13 = w
        return this
    }

    override fun setRow3(x: Double, y: Double, z: Double, w: Double): Mat4d {
        m20 = x
        m21 = y
        m22 = z
        m23 = w
        return this
    }

    override fun setRow4(x: Double, y: Double, z: Double, w: Double): Mat4d {
        m30 = x
        m31 = y
        m32 = z
        m33 = w
        return this
    }

    override fun setRow(row: Int, vector: Vec4dAccessor): Mat4d {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        when (row) {
            0 -> {
                m00 = vector.x
                m01 = vector.y
                m02 = vector.z
                m03 = vector.w
            }
            1 -> {
                m10 = vector.x
                m11 = vector.y
                m12 = vector.z
                m13 = vector.w
            }
            2 -> {
                m20 = vector.x
                m21 = vector.y
                m22 = vector.z
                m23 = vector.w
            }
            3 -> {
                m30 = vector.x
                m31 = vector.y
                m32 = vector.z
                m33 = vector.w
            }
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..3]!")
    }

    override fun setRow1(vector: Vec4dAccessor): Mat4d {
        m00 = vector.x
        m01 = vector.y
        m02 = vector.z
        m03 = vector.w
        return this
    }

    override fun setRow2(vector: Vec4dAccessor): Mat4d {
        m10 = vector.x
        m11 = vector.y
        m12 = vector.z
        m13 = vector.w
        return this
    }

    override fun setRow3(vector: Vec4dAccessor): Mat4d {
        m20 = vector.x
        m21 = vector.y
        m22 = vector.z
        m23 = vector.w
        return this
    }

    override fun setRow4(vector: Vec4dAccessor): Mat4d {
        m30 = vector.x
        m31 = vector.y
        m32 = vector.z
        m33 = vector.w
        return this
    }

    override fun setColumn(column: Int, x: Double, y: Double, z: Double, w: Double): Mat4d {
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        when (column) {
            0 -> {
                m00 = x
                m10 = y
                m20 = z
                m30 = w
            }
            1 -> {
                m01 = x
                m11 = y
                m21 = z
                m31 = w
            }
            2 -> {
                m02 = x
                m12 = y
                m22 = z
                m32 = w
            }
            3 -> {
                m03 = x
                m13 = y
                m23 = z
                m33 = w
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..3]!")
    }

    override fun setColumn1(x: Double, y: Double, z: Double, w: Double): Mat4d {
        m00 = x
        m10 = y
        m20 = z
        m30 = w
        return this
    }

    override fun setColumn2(x: Double, y: Double, z: Double, w: Double): Mat4d {
        m01 = x
        m11 = y
        m21 = z
        m31 = w
        return this
    }

    override fun setColumn3(x: Double, y: Double, z: Double, w: Double): Mat4d {
        m02 = x
        m12 = y
        m22 = z
        m32 = w
        return this
    }

    override fun setColumn4(x: Double, y: Double, z: Double, w: Double): Mat4d {
        m03 = x
        m13 = y
        m23 = z
        m33 = w
        return this
    }

    override fun setColumn(column: Int, vector: Vec4dAccessor): Mat4d {
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        when (column) {
            0 -> {
                m00 = vector.x
                m10 = vector.y
                m20 = vector.z
                m30 = vector.w
            }
            1 -> {
                m01 = vector.x
                m11 = vector.y
                m21 = vector.z
                m31 = vector.w
            }
            2 -> {
                m02 = vector.x
                m12 = vector.y
                m22 = vector.z
                m32 = vector.w
            }
            3 -> {
                m03 = vector.x
                m13 = vector.y
                m23 = vector.z
                m33 = vector.w
            }
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..3]!")
    }

    override fun setColumn1(vector: Vec4dAccessor): Mat4d {
        m00 = vector.x
        m10 = vector.y
        m20 = vector.z
        m30 = vector.w
        return this
    }

    override fun setColumn2(vector: Vec4dAccessor): Mat4d {
        m01 = vector.x
        m11 = vector.y
        m21 = vector.z
        m31 = vector.w
        return this
    }

    override fun setColumn3(vector: Vec4dAccessor): Mat4d {
        m02 = vector.x
        m12 = vector.y
        m22 = vector.z
        m32 = vector.w
        return this
    }

    override fun setColumn4(vector: Vec4dAccessor): Mat4d {
        m03 = vector.x
        m13 = vector.y
        m23 = vector.z
        m33 = vector.w
        return this
    }

    override fun set(value: Double): Mat4d {
        m00 = value
        m01 = value
        m02 = value
        m03 = value
        m10 = value
        m11 = value
        m12 = value
        m13 = value
        m20 = value
        m21 = value
        m22 = value
        m23 = value
        m30 = value
        m31 = value
        m32 = value
        m33 = value
        return this
    }

    override fun set(
        m00: Double, m01: Double, m02: Double, m03: Double,
        m10: Double, m11: Double, m12: Double, m13: Double,
        m20: Double, m21: Double, m22: Double, m23: Double,
        m30: Double, m31: Double, m32: Double, m33: Double
    ): Mat4d {
        this.m00 = m00
        this.m01 = m01
        this.m02 = m02
        this.m03 = m03
        this.m10 = m10
        this.m11 = m11
        this.m12 = m12
        this.m13 = m13
        this.m20 = m20
        this.m21 = m21
        this.m22 = m22
        this.m23 = m23
        this.m30 = m30
        this.m31 = m31
        this.m32 = m32
        this.m33 = m33
        return this
    }

    override fun set(other: Mat4fAccessor): Mat4d {
        m00 = other.m00.toDouble()
        m01 = other.m01.toDouble()
        m02 = other.m02.toDouble()
        m03 = other.m03.toDouble()
        m10 = other.m10.toDouble()
        m11 = other.m11.toDouble()
        m12 = other.m12.toDouble()
        m13 = other.m13.toDouble()
        m20 = other.m20.toDouble()
        m21 = other.m21.toDouble()
        m22 = other.m22.toDouble()
        m23 = other.m23.toDouble()
        m30 = other.m30.toDouble()
        m31 = other.m31.toDouble()
        m32 = other.m32.toDouble()
        m33 = other.m33.toDouble()
        return this
    }

    override fun set(other: Mat4dAccessor): Mat4d {
        m00 = other.m00
        m01 = other.m01
        m02 = other.m02
        m03 = other.m03
        m10 = other.m10
        m11 = other.m11
        m12 = other.m12
        m13 = other.m13
        m20 = other.m20
        m21 = other.m21
        m22 = other.m22
        m23 = other.m23
        m30 = other.m30
        m31 = other.m31
        m32 = other.m32
        m33 = other.m33
        return this
    }

    override fun set(other: Mat3dAccessor): Mat4d {
        m00 = other.m00
        m01 = other.m01
        m02 = other.m02
        m03 = 0.0
        m10 = other.m10
        m11 = other.m11
        m12 = other.m12
        m13 = 0.0
        m20 = other.m20
        m21 = other.m21
        m22 = other.m22
        m23 = 0.0
        m30 = 0.0
        m31 = 0.0
        m32 = 0.0
        m33 = 1.0
        return this
    }

    override fun set(other: Mat3dAccessor, fillWith: Double): Mat4d {
        m00 = other.m00
        m01 = other.m01
        m02 = other.m02
        m03 = fillWith
        m10 = other.m10
        m11 = other.m11
        m12 = other.m12
        m13 = fillWith
        m20 = other.m20
        m21 = other.m21
        m22 = other.m22
        m23 = fillWith
        m33 = fillWith
        m32 = fillWith
        m31 = fillWith
        m30 = fillWith
        return this
    }

    override fun set(
        other: Mat3dAccessor,
        fillWith03: Double, fillWith13: Double, fillWith23: Double,
        fillWith30: Double, fillWith31: Double, fillWith32: Double,
        fillWith33: Double
    ): Mat4d {
        m00 = other.m00
        m01 = other.m01
        m02 = other.m02
        m03 = fillWith03
        m10 = other.m10
        m11 = other.m11
        m12 = other.m12
        m13 = fillWith13
        m20 = other.m20
        m21 = other.m21
        m22 = other.m22
        m23 = fillWith23
        m30 = fillWith30
        m31 = fillWith31
        m32 = fillWith32
        m33 = fillWith33
        return this
    }

    override fun set(matrixData: Array<DoubleArray>): Mat4d {
        require(!(matrixData.size != Mat4d.ROW_AMT || matrixData[0].size != Mat4d.COLUMN_AMT)) {
            "Array dimensions do not match. Copy not possible"
        }
        m00 = matrixData[0][0]
        m01 = matrixData[0][1]
        m02 = matrixData[0][2]
        m03 = matrixData[0][3]
        m10 = matrixData[1][0]
        m11 = matrixData[1][1]
        m12 = matrixData[1][2]
        m13 = matrixData[1][3]
        m20 = matrixData[2][0]
        m21 = matrixData[2][1]
        m22 = matrixData[2][2]
        m23 = matrixData[2][3]
        m30 = matrixData[3][0]
        m31 = matrixData[3][1]
        m32 = matrixData[3][2]
        m33 = matrixData[3][3]
        return this
    }

    override fun initZero(): Mat4d = set(0.0)

    override fun initIdentity(): Mat4d {
        m00 = 1.0
        m01 = 0.0
        m02 = 0.0
        m03 = 0.0
        m10 = 0.0
        m11 = 1.0
        m12 = 0.0
        m13 = 0.0
        m20 = 0.0
        m21 = 0.0
        m22 = 1.0
        m23 = 0.0
        m30 = 0.0
        m31 = 0.0
        m32 = 0.0
        m33 = 1.0
        return this
    }

    override fun initBias(): Mat4d {
        m00 = 0.5
        m01 = 0.0
        m02 = 0.0
        m03 = 0.5
        m10 = 0.0
        m11 = 0.5
        m12 = 0.0
        m13 = 0.5
        m20 = 0.0
        m21 = 0.0
        m22 = 0.5
        m23 = 0.5
        m30 = 0.0
        m31 = 0.0
        m32 = 0.0
        m33 = 1.0
        return this
    }

    override fun initTranslation(x: Double, y: Double, z: Double): Mat4d {
        m00 = 1.0
        m01 = 0.0
        m02 = 0.0
        m03 = x
        m10 = 0.0
        m11 = 1.0
        m12 = 0.0
        m13 = y
        m20 = 0.0
        m21 = 0.0
        m22 = 1.0
        m23 = z
        m30 = 0.0
        m31 = 0.0
        m32 = 0.0
        m33 = 1.0
        return this
    }

    override fun initTranslation(translation: Vec3dAccessor): Mat4d =
        initTranslation(translation.x, translation.y, translation.z)

    // TODO: Optimize?
    override fun initRotation(angleAroundX: Double, angleAroundY: Double, angleAroundZ: Double): Mat4d =
        set(StdMat3d().initRotation(angleAroundX, angleAroundY, angleAroundZ))

    override fun initRotation(angles: Vec3dAccessor): Mat4d =
        initRotation(angles.x, angles.y, angles.z)

    // TODO: Optimize?
    override fun initRotation(axis: Vec3dAccessor, angle: Double): Mat4d =
        set(StdMat3d().initRotation(axis, angle))

    override fun initRotation(forward: Vec3dAccessor, up: Vec3dAccessor, right: Vec3dAccessor): Mat4d {
        // Assuming that forward, up and right are normalized! // TODO: assert?
        assert(isApproximately(forward.length(), 1.0, 0.01)) { "Forward vector not normalised." }
        assert(isApproximately(up.length(), 1.0, 0.01)) { "Up vector not normalised." }
        assert(isApproximately(right.length(), 1.0, 0.01)) { "Right vector not normalised." }
        // First row.
        m00 = right.x
        m01 = right.y
        m02 = right.z
        // Second row.
        m10 = up.x
        m11 = up.y
        m12 = up.z
        // Third row.
        m20 = forward.x
        m21 = forward.y
        m22 = forward.z
        // Fourth row and column.
        m32 = 0.0
        m31 = m32
        m30 = m31
        m23 = m30
        m13 = m23
        m03 = m13
        m33 = 1.0
        return this
    }

    // TODO: Optimize?
    override fun initRotation(forward: Vec3dAccessor, up: Vec3dAccessor): Mat4d {
        // Assuming that forward and up are normalized. // TODO: assert?
        val right: Vec3d = StdVec3d().set(up).cross(forward)
        return initRotation(forward, up, right)
    }

    // TODO: Optimize?
    override fun initRotation(rotation: QuaternionDAccessor): Mat4d {
        val forward: Vec3d = rotation.forward(StdVec3d())
        val up: Vec3d = rotation.up(StdVec3d())
        val right: Vec3d = rotation.right(StdVec3d())
        return initRotation(forward, up, right)
        // TODO: Compare! This would be a lot faster and easier!
        //return rotation.asMatrix(this);
    }

    override fun initScale(scaleX: Double, scaleY: Double, scaleZ: Double): Mat4d {
        m00 = scaleX
        m11 = scaleY
        m22 = scaleZ
        m33 = 1.0
        m32 = 0.0
        m31 = 0.0
        m30 = 0.0
        m23 = 0.0
        m21 = 0.0
        m20 = 0.0
        m13 = 0.0
        m12 = 0.0
        m10 = 0.0
        m03 = 0.0
        m02 = 0.0
        m01 = 0.0
        return this
    }

    override fun initScale(scale: Double): Mat4d = initScale(scale, scale, scale)

    override fun initScale(scaleView: Vec3dAccessor): Mat4d =
        initScale(scaleView.x, scaleView.y, scaleView.z)

    override fun initFrustumProjection(
        left: Double, right: Double,
        bottom: Double, top: Double,
        zNear: Double, zFar: Double
    ): Mat4d {
        // First row.
        m00 = 2.0 * zNear / (right - left)
        m01 = 0.0
        m02 = (right + left) / (right - left)
        m03 = 0.0
        // Second row.
        m10 = 0.0
        m11 = 2.0 * zNear / (top - bottom)
        m12 = (top + bottom) / (top - bottom)
        m13 = 0.0
        // Third row.
        m20 = 0.0
        m21 = 0.0
        m22 = (zNear + zFar) / (zNear - zFar)
        m23 = 2.0 * zNear * zFar / (zNear - zFar)
        // Fourth row.
        m30 = 0.0
        m31 = 0.0
        m32 = -1.0
        m33 = 0.0
        return this
    }

    override fun initPerspectiveProjectionLH(
        fov: Double, aspectRatio: Double,
        zNear: Double, zFar: Double
    ): Mat4d {
        val cotanHalfFOV = 1.0 / tan(Math.toRadians(fov / 2.0))
        val zRange = zNear - zFar
        // First row.
        m00 = cotanHalfFOV / aspectRatio
        m01 = 0.0
        m02 = 0.0
        m03 = 0.0
        // Second row.
        m10 = 0.0
        m11 = cotanHalfFOV
        m12 = 0.0
        m13 = 0.0
        // Third row.
        m20 = 0.0
        m21 = 0.0
        m22 = zFar / (zFar - zNear) // z-Axis flipped!
        m23 = zNear * zFar / zRange
        // Fourth row.
        m30 = 0.0
        m31 = 0.0
        m32 = 1.0 // <<<---- Defines LH coordinate system.
        m33 = 0.0
        return this
    }

    override fun initPerspectiveProjectionRH(
        fov: Double, aspectRatio: Double,
        zNear: Double, zFar: Double
    ): Mat4d {
        val cotanHalfFOV = 1.0 / tan(Math.toRadians(fov / 2.0))
        val zRange = zNear - zFar
        // First row.
        m00 = cotanHalfFOV / aspectRatio
        m01 = 0.0
        m02 = 0.0
        m03 = 0.0
        // Second row.
        m10 = 0.0
        m11 = cotanHalfFOV
        m12 = 0.0
        m13 = 0.0
        // Third row.
        m20 = 0.0
        m21 = 0.0
        m22 = zFar / zRange
        m23 = zNear * zFar / zRange
        // Fourth row.
        m30 = 0.0
        m31 = 0.0
        m32 = -1.0 // <<<---- Defines RH coordinate system.
        m33 = 0.0
        return this
    }

    override fun initOrthographicProjectionLH(
        left: Double, right: Double,
        bottom: Double, top: Double,
        zNear: Double, zFar: Double
    ): Mat4d {
        val width = right - left
        val height = top - bottom
        val zRange = zFar - zNear
        // First row.
        m00 = 2.0 / width
        m01 = 0.0
        m02 = 0.0
        m03 = -(right + left) / width
        // Second row.
        m10 = 0.0
        m11 = 2.0 / height
        m12 = 0.0
        m13 = -(top + bottom) / height
        // Third row.
        m20 = 0.0
        m21 = 0.0
        m22 = -(2.0 / zRange)
        m23 = -(zFar + zNear) / zRange // TODO: <- Is this correct? See difference in perspective.
        // Fourth row.
        m30 = 0.0
        m31 = 0.0
        m32 = 1.0 // TODO: <- Is this correct?
        m33 = 1.0
        return this
    }

    override fun initOrthographicProjectionRH(
        left: Double, right: Double,
        bottom: Double, top: Double,
        zNear: Double, zFar: Double
    ): Mat4d {
        val width = right - left
        val height = top - bottom
        val zRange = zFar - zNear
        // First row.
        m00 = 2.0 / width
        m01 = 0.0
        m02 = 0.0
        m03 = -(right + left) / width
        // Second row.
        m10 = 0.0
        m11 = 2.0 / height
        m12 = 0.0
        m13 = -(top + bottom) / height
        // Third row.
        m20 = 0.0
        m21 = 0.0
        m22 = 2.0 / zRange
        m23 = -(zFar + zNear) / zRange
        // Fourth row.
        m30 = 0.0
        m31 = 0.0
        m32 = 0.0
        m33 = 1.0
        return this
    }

    // TODO: Optimize?
    override fun initLookAtRH(
        source: Vec3dAccessor,
        targetUp: Vec3dAccessor,
        pointOfInterest: Vec3dAccessor
    ): Mat4d {
        // Construct the normalized forward vector.
        val forward: Vec3d = StdVec3d().set(source).minus(pointOfInterest)
        when (forward.length()) {
            0.0 -> {
                LOG.warn("Source and PointOfInterest were equal. Was unable to calculate proper forward vector.")
                forward.set(StdVec3fFactory.NEG_UNIT_Z_AXIS)
            }
            else -> {
                forward.normalize()
            }
        }

        // Construct a side vector right by taking the cross product of the new forward vector and the camera up vector.
        val right: Vec3d = StdVec3d().set(targetUp).cross(forward)
        when (right.length()) {
            0.0 -> {
                LOG.warn("Was unable to calculate proper right vector.")
                right.set(StdVec3fFactory.UNIT_X_AXIS)
            }
            else -> {
                right.normalize()
            }
        }

        // There is no certainty that the targetUp and our new forward vector are perpendicular to each other.
        // => Construct a NEW up vector. No normalization necessary, as forward and right are already normalized.
        val up: Vec3d = StdVec3d().set(forward).cross(right)

        // First row.
        m00 = right.x
        m01 = right.y
        m02 = right.z
        m03 = -source.dot(right)
        // Second row.
        m10 = up.x
        m11 = up.y
        m12 = up.z
        m13 = -source.dot(up)
        // Third row.
        m20 = forward.x
        m21 = forward.y
        m22 = forward.z
        m23 = -source.dot(forward)
        // Fourth row.
        m30 = 0.0
        m31 = 0.0
        m32 = 0.0
        m33 = 1.0
        return this
    }

    override fun transform(vector: Vec4d): Vec4d {
        return vector.set(
            m00 * vector.x + m01 * vector.y + m02 * vector.z + m03 * vector.w,
            m10 * vector.x + m11 * vector.y + m12 * vector.z + m13 * vector.w,
            m20 * vector.x + m21 * vector.y + m22 * vector.z + m23 * vector.w,
            m30 * vector.x + m31 * vector.y + m32 * vector.z + m33 * vector.w
        )
    }

    override fun plus(value: Double): Mat4d {
        m00 += value
        m01 += value
        m02 += value
        m03 += value
        m10 += value
        m11 += value
        m12 += value
        m13 += value
        m20 += value
        m21 += value
        m22 += value
        m23 += value
        m30 += value
        m31 += value
        m32 += value
        m33 += value
        return this
    }

    override fun plus(other: Mat4dAccessor): Mat4d {
        m00 += other.m00
        m01 += other.m01
        m02 += other.m02
        m03 += other.m03
        m10 += other.m10
        m11 += other.m11
        m12 += other.m12
        m13 += other.m13
        m20 += other.m20
        m21 += other.m21
        m22 += other.m22
        m23 += other.m23
        m30 += other.m30
        m31 += other.m31
        m32 += other.m32
        m33 += other.m33
        return this
    }

    override fun minus(value: Double): Mat4d {
        m00 -= value
        m01 -= value
        m02 -= value
        m03 -= value
        m10 -= value
        m11 -= value
        m12 -= value
        m13 -= value
        m20 -= value
        m21 -= value
        m22 -= value
        m23 -= value
        m30 -= value
        m31 -= value
        m32 -= value
        m33 -= value
        return this
    }

    override fun minus(other: Mat4dAccessor): Mat4d {
        m00 -= other.m00
        m01 -= other.m01
        m02 -= other.m02
        m03 -= other.m03
        m10 -= other.m10
        m11 -= other.m11
        m12 -= other.m12
        m13 -= other.m13
        m20 -= other.m20
        m21 -= other.m21
        m22 -= other.m22
        m23 -= other.m23
        m30 -= other.m30
        m31 -= other.m31
        m32 -= other.m32
        m33 -= other.m33
        return this
    }

    override fun times(value: Double): Mat4d {
        m00 *= value
        m01 *= value
        m02 *= value
        m03 *= value
        m10 *= value
        m11 *= value
        m12 *= value
        m13 *= value
        m20 *= value
        m21 *= value
        m22 *= value
        m23 *= value
        m30 *= value
        m31 *= value
        m32 *= value
        m33 *= value
        return this
    }

    /*
     * - 64 multiplications
     * - 48 additions
     * - 32 assignments (16 final)
     */
    override fun times(other: Mat4dAccessor): Mat4d {
        assert(this !== other) { "Cannot multiply with itself!" }
        // First new row.
        var r0: Double = m00 * other.m00 + m01 * other.m10 + m02 * other.m20 + m03 * other.m30
        var r1: Double = m00 * other.m01 + m01 * other.m11 + m02 * other.m21 + m03 * other.m31
        var r2: Double = m00 * other.m02 + m01 * other.m12 + m02 * other.m22 + m03 * other.m32
        var r3: Double = m00 * other.m03 + m01 * other.m13 + m02 * other.m23 + m03 * other.m33
        m00 = r0
        m01 = r1
        m02 = r2
        m03 = r3
        // Second new row.
        r0 = m10 * other.m00 + m11 * other.m10 + m12 * other.m20 + m13 * other.m30
        r1 = m10 * other.m01 + m11 * other.m11 + m12 * other.m21 + m13 * other.m31
        r2 = m10 * other.m02 + m11 * other.m12 + m12 * other.m22 + m13 * other.m32
        r3 = m10 * other.m03 + m11 * other.m13 + m12 * other.m23 + m13 * other.m33
        m10 = r0
        m11 = r1
        m12 = r2
        m13 = r3
        // Third new row.
        r0 = m20 * other.m00 + m21 * other.m10 + m22 * other.m20 + m23 * other.m30
        r1 = m20 * other.m01 + m21 * other.m11 + m22 * other.m21 + m23 * other.m31
        r2 = m20 * other.m02 + m21 * other.m12 + m22 * other.m22 + m23 * other.m32
        r3 = m20 * other.m03 + m21 * other.m13 + m22 * other.m23 + m23 * other.m33
        m20 = r0
        m21 = r1
        m22 = r2
        m23 = r3
        // Fourth new row.
        r0 = m30 * other.m00 + m31 * other.m10 + m32 * other.m20 + m33 * other.m30
        r1 = m30 * other.m01 + m31 * other.m11 + m32 * other.m21 + m33 * other.m31
        r2 = m30 * other.m02 + m31 * other.m12 + m32 * other.m22 + m33 * other.m32
        r3 = m30 * other.m03 + m31 * other.m13 + m32 * other.m23 + m33 * other.m33
        m30 = r0
        m31 = r1
        m32 = r2
        m33 = r3
        return this
    }

    override fun mulSelf(): Mat4d {
        // First new row.
        val r00 = m00 * m00 + m01 * m10 + m02 * m20 + m03 * m30
        val r01 = m00 * m01 + m01 * m11 + m02 * m21 + m03 * m31
        val r02 = m00 * m02 + m01 * m12 + m02 * m22 + m03 * m32
        val r03 = m00 * m03 + m01 * m13 + m02 * m23 + m03 * m33
        // Second new row.
        val r10 = m10 * m00 + m11 * m10 + m12 * m20 + m13 * m30
        val r11 = m10 * m01 + m11 * m11 + m12 * m21 + m13 * m31
        val r12 = m10 * m02 + m11 * m12 + m12 * m22 + m13 * m32
        val r13 = m10 * m03 + m11 * m13 + m12 * m23 + m13 * m33
        // Third new row.
        val r20 = m20 * m00 + m21 * m10 + m22 * m20 + m23 * m30
        val r21 = m20 * m01 + m21 * m11 + m22 * m21 + m23 * m31
        val r22 = m20 * m02 + m21 * m12 + m22 * m22 + m23 * m32
        val r23 = m20 * m03 + m21 * m13 + m22 * m23 + m23 * m33
        // Fourth new row.
        val r30 = m30 * m00 + m31 * m10 + m32 * m20 + m33 * m30
        val r31 = m30 * m01 + m31 * m11 + m32 * m21 + m33 * m31
        val r32 = m30 * m02 + m31 * m12 + m32 * m22 + m33 * m32
        val r33 = m30 * m03 + m31 * m13 + m32 * m23 + m33 * m33
        // Update this instance.
        m00 = r00
        m01 = r01
        m02 = r02
        m03 = r03
        m10 = r10
        m11 = r11
        m12 = r12
        m13 = r13
        m20 = r20
        m21 = r21
        m22 = r22
        m23 = r23
        m30 = r30
        m31 = r31
        m32 = r32
        m33 = r33
        return this
    }

    override fun inverse(): Mat4d {
        // Calculate 1 over the determinant of the initial matrix.
        val oneOverDeterminant = 1.0 / determinant()

        // Calculate cofactors.
        val c00 = +StdMat3d.determinantOf(m11, m12, m13, m21, m22, m23, m31, m32, m33)
        val c01 = -StdMat3d.determinantOf(m10, m12, m13, m20, m22, m23, m30, m32, m33)
        val c02 = +StdMat3d.determinantOf(m10, m11, m13, m20, m21, m23, m30, m31, m33)
        val c03 = -StdMat3d.determinantOf(m10, m11, m12, m20, m21, m22, m30, m31, m32)
        val c10 = -StdMat3d.determinantOf(m01, m02, m03, m21, m22, m23, m31, m32, m33)
        val c11 = +StdMat3d.determinantOf(m00, m02, m03, m20, m22, m23, m30, m32, m33)
        val c12 = -StdMat3d.determinantOf(m00, m01, m03, m20, m21, m23, m30, m31, m33)
        val c13 = +StdMat3d.determinantOf(m00, m01, m02, m20, m21, m22, m30, m31, m32)
        val c20 = +StdMat3d.determinantOf(m01, m02, m03, m11, m12, m13, m31, m32, m33)
        val c21 = -StdMat3d.determinantOf(m00, m02, m03, m10, m12, m13, m30, m32, m33)
        val c22 = +StdMat3d.determinantOf(m00, m01, m03, m10, m11, m13, m30, m31, m33)
        val c23 = -StdMat3d.determinantOf(m00, m01, m02, m10, m11, m12, m30, m31, m32)
        val c30 = -StdMat3d.determinantOf(m01, m02, m03, m11, m12, m13, m21, m22, m23)
        val c31 = +StdMat3d.determinantOf(m00, m02, m03, m10, m12, m13, m20, m22, m23)
        val c32 = -StdMat3d.determinantOf(m00, m01, m03, m10, m11, m13, m20, m21, m23)
        val c33 = +StdMat3d.determinantOf(m00, m01, m02, m10, m11, m12, m20, m21, m22)

        // ... and return the result multiplied with the transposed cofactor matrix.
        return set(c00, c01, c02, c03, c10, c11, c12, c13, c20, c21, c22, c23, c30, c31, c32, c33)
            .transpose().times(oneOverDeterminant)
    }

    override fun transpose(): Mat4d {
        // Swap 10 with 01.
        var temp: Double = m10
        m10 = m01
        m01 = temp
        // Swap 21 with 12.
        temp = m21
        m21 = m12
        m12 = temp
        // Swap 32 with 23.
        temp = m32
        m32 = m23
        m23 = temp
        // Swap 31 with 13.
        temp = m31
        m31 = m13
        m13 = temp
        // Swap 20 with 02.
        temp = m20
        m20 = m02
        m02 = temp
        // Swap 30 with 03.
        temp = m30
        m30 = m03
        m03 = temp
        return this
    }

    @Suppress("ReplaceGetOrSet")
    override fun getSubmatrix(row: Int, column: Int, storeIn: Mat3d): Mat3d {
        assert(row in 0..3) { "Row to ignore (row) is not in range [0..3]!" }
        assert(column in 0..3) { "Column to ignore (column) is not in range [0..3]!" }
        when (row) {
            0 -> when (column) {
                0 -> return storeIn.set(m11, m12, m13, m21, m22, m23, m31, m32, m33)
                1 -> return storeIn.set(m10, m12, m13, m20, m22, m23, m30, m32, m33)
                2 -> return storeIn.set(m10, m11, m13, m20, m21, m23, m30, m31, m33)
                3 -> return storeIn.set(m10, m11, m12, m20, m21, m22, m30, m31, m32)
            }
            1 -> when (column) {
                0 -> return storeIn.set(m01, m02, m03, m21, m22, m23, m31, m32, m33)
                1 -> return storeIn.set(m00, m02, m03, m20, m22, m23, m30, m32, m33)
                2 -> return storeIn.set(m00, m01, m03, m20, m21, m23, m30, m31, m33)
                3 -> return storeIn.set(m00, m01, m02, m20, m21, m22, m30, m31, m32)
            }
            2 -> when (column) {
                0 -> return storeIn.set(m01, m02, m03, m11, m12, m13, m31, m32, m33)
                1 -> return storeIn.set(m00, m02, m03, m10, m12, m13, m30, m32, m33)
                2 -> return storeIn.set(m00, m01, m03, m10, m11, m13, m30, m31, m33)
                3 -> return storeIn.set(m00, m01, m02, m10, m11, m12, m30, m31, m32)
            }
            3 -> when (column) {
                0 -> return storeIn.set(m01, m02, m03, m11, m12, m13, m21, m22, m23)
                1 -> return storeIn.set(m00, m02, m03, m10, m12, m13, m20, m22, m23)
                2 -> return storeIn.set(m00, m01, m03, m10, m11, m13, m20, m21, m23)
                3 -> return storeIn.set(m00, m01, m02, m10, m11, m12, m20, m21, m22)
            }
        }
        throw IllegalArgumentException("Row or column not in range [0..2]!")
    }

    override fun pow(exponent: Int): Mat4d {
        require(exponent >= -1) { "Exponents below -1 are not supported. Given: $exponent" }
        return when (exponent) {
            -1 -> inverse()
            0 -> initIdentity()
            1 -> this
            else -> {
                var i = 1
                while (i < exponent) {
                    mulSelf()
                    i++
                }
                this
            }
        }
    }

    override fun abs(): Mat4d {
        m00 = m00.absoluteValue
        m01 = m01.absoluteValue
        m02 = m02.absoluteValue
        m03 = m03.absoluteValue
        m10 = m10.absoluteValue
        m11 = m11.absoluteValue
        m12 = m12.absoluteValue
        m13 = m13.absoluteValue
        m20 = m20.absoluteValue
        m21 = m21.absoluteValue
        m22 = m22.absoluteValue
        m23 = m23.absoluteValue
        m30 = m30.absoluteValue
        m31 = m31.absoluteValue
        m32 = m32.absoluteValue
        m33 = m33.absoluteValue
        return this
    }

    override fun shorten(): Mat4d {
        m00 = m00.toFourDecimalPlaces()
        m01 = m01.toFourDecimalPlaces()
        m02 = m02.toFourDecimalPlaces()
        m03 = m03.toFourDecimalPlaces()
        m10 = m10.toFourDecimalPlaces()
        m11 = m11.toFourDecimalPlaces()
        m12 = m12.toFourDecimalPlaces()
        m13 = m13.toFourDecimalPlaces()
        m20 = m20.toFourDecimalPlaces()
        m21 = m21.toFourDecimalPlaces()
        m22 = m22.toFourDecimalPlaces()
        m23 = m23.toFourDecimalPlaces()
        m30 = m30.toFourDecimalPlaces()
        m31 = m31.toFourDecimalPlaces()
        m32 = m32.toFourDecimalPlaces()
        m33 = m33.toFourDecimalPlaces()
        return this
    }

    override operator fun get(row: Int, column: Int): Double {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        when (row) {
            0 -> when (column) {
                0 -> return m00
                1 -> return m01
                2 -> return m02
                3 -> return m03
            }
            1 -> when (column) {
                0 -> return m10
                1 -> return m11
                2 -> return m12
                3 -> return m13
            }
            2 -> when (column) {
                0 -> return m20
                1 -> return m21
                2 -> return m22
                3 -> return m23
            }
            3 -> when (column) {
                0 -> return m30
                1 -> return m31
                2 -> return m32
                3 -> return m33
            }
        }
        throw IllegalArgumentException(
            "Row($row) and column($column) indices were not both in range [0..3]!"
        )
    }

    override fun getRow(row: Int, storeIn: Vec4d): Vec4d {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        when (row) {
            0 -> return storeIn.set(m00, m01, m02, m03)
            1 -> return storeIn.set(m10, m11, m12, m13)
            2 -> return storeIn.set(m20, m21, m22, m23)
            3 -> return storeIn.set(m30, m31, m32, m33)
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..3]!")
    }

    override fun getRow1(storeIn: Vec4d): Vec4d = storeIn.set(m00, m01, m02, m03)

    override fun getRow2(storeIn: Vec4d): Vec4d = storeIn.set(m10, m11, m12, m13)

    override fun getRow3(storeIn: Vec4d): Vec4d = storeIn.set(m20, m21, m22, m23)

    override fun getRow4(storeIn: Vec4d): Vec4d = storeIn.set(m30, m31, m32, m33)

    override fun getColumn(column: Int, storeIn: Vec4d): Vec4d {
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        when (column) {
            0 -> return storeIn.set(m00, m10, m20, m30)
            1 -> return storeIn.set(m01, m11, m21, m31)
            2 -> return storeIn.set(m02, m12, m22, m32)
            3 -> return storeIn.set(m03, m13, m23, m33)
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..3]!")
    }

    override fun getColumn1(storeIn: Vec4d): Vec4d = storeIn.set(m00, m10, m20, m30)

    override fun getColumn2(storeIn: Vec4d): Vec4d = storeIn.set(m01, m11, m21, m31)

    override fun getColumn3(storeIn: Vec4d): Vec4d = storeIn.set(m02, m12, m22, m32)

    override fun getColumn4(storeIn: Vec4d): Vec4d = storeIn.set(m03, m13, m23, m33)

    override fun getMajorDiagonal(storeIn: Vec4d): Vec4d = storeIn.set(m00, m11, m22, m33)

    override fun getRight(storeIn: Vec3d): Vec3d = storeIn.set(m00, m01, m02)

    override fun getUp(storeIn: Vec3d): Vec3d = storeIn.set(m10, m11, m12)

    override fun getForward(storeIn: Vec3d): Vec3d = storeIn.set(m20, m21, m22)

    override fun asArray(): Array<out DoubleArray> {
        return arrayOf(
            doubleArrayOf(m00, m01, m02, m03),
            doubleArrayOf(m10, m11, m12, m13),
            doubleArrayOf(m20, m21, m22, m23),
            doubleArrayOf(m30, m31, m32, m33)
        )
    }

    override fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray> {
        require(storeIn.size == Mat4d.ROW_AMT && storeIn[0].size == Mat4d.COLUMN_AMT) {
            "Given data array is not of size 4x4!"
        }
        storeIn[0][0] = m00
        storeIn[0][1] = m01
        storeIn[0][2] = m02
        storeIn[0][3] = m03
        storeIn[1][0] = m10
        storeIn[1][1] = m11
        storeIn[1][2] = m12
        storeIn[1][3] = m13
        storeIn[2][0] = m20
        storeIn[2][1] = m21
        storeIn[2][2] = m22
        storeIn[2][3] = m23
        storeIn[3][0] = m30
        storeIn[3][1] = m31
        storeIn[3][2] = m32
        storeIn[3][3] = m33
        return storeIn
    }

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Mat4d.BYTES)).array()
    }

    // TODO: Optimize?
    override fun asQuaternion(storeIn: QuaternionD): QuaternionD =
        storeIn.initLookRotation(getForward(StdVec3d()), getUp(StdVec3d()))

    override fun determinant(): Double {
        var determinant = 0.0
        // Elimination of the 1st column
        determinant += m00 * StdMat3d.determinantOf(m11, m12, m13, m21, m22, m23, m31, m32, m33)
        // Elimination of the 2nd column
        determinant -= m01 * StdMat3d.determinantOf(m10, m12, m13, m20, m22, m23, m30, m32, m33)
        // Elimination of the 3rd column
        determinant += m02 * StdMat3d.determinantOf(m10, m11, m13, m20, m21, m23, m30, m31, m33)
        // Elimination of the 4th column
        determinant -= m03 * StdMat3d.determinantOf(m10, m11, m12, m20, m21, m22, m30, m31, m32)
        return determinant
    }

    override fun hasZeroComponent(): Boolean {
        return m00 == 0.0 || m01 == 0.0 || m02 == 0.0 || m03 == 0.0 ||
            m10 == 0.0 || m11 == 0.0 || m12 == 0.0 || m13 == 0.0 ||
            m20 == 0.0 || m21 == 0.0 || m22 == 0.0 || m23 == 0.0 ||
            m30 == 0.0 || m31 == 0.0 || m32 == 0.0 || m33 == 0.0
    }

    override fun toFormattedString(): String = toString()

    override fun hashCode(): Int {
        var result = 17
        result = 31 * result + java.lang.Double.hashCode(m00)
        result = 31 * result + java.lang.Double.hashCode(m01)
        result = 31 * result + java.lang.Double.hashCode(m02)
        result = 31 * result + java.lang.Double.hashCode(m03)
        result = 31 * result + java.lang.Double.hashCode(m10)
        result = 31 * result + java.lang.Double.hashCode(m11)
        result = 31 * result + java.lang.Double.hashCode(m12)
        result = 31 * result + java.lang.Double.hashCode(m13)
        result = 31 * result + java.lang.Double.hashCode(m20)
        result = 31 * result + java.lang.Double.hashCode(m21)
        result = 31 * result + java.lang.Double.hashCode(m22)
        result = 31 * result + java.lang.Double.hashCode(m23)
        result = 31 * result + java.lang.Double.hashCode(m30)
        result = 31 * result + java.lang.Double.hashCode(m31)
        result = 31 * result + java.lang.Double.hashCode(m32)
        result = 31 * result + java.lang.Double.hashCode(m33)
        return result
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is StdMat4d -> false
            other === this -> true
            else -> m00 == other.m00 && m01 == other.m01 && m02 == other.m02 && m03 == other.m03 &&
                m10 == other.m10 && m11 == other.m11 && m12 == other.m12 && m13 == other.m13 &&
                m20 == other.m20 && m21 == other.m21 && m22 == other.m22 && m23 == other.m23 &&
                m30 == other.m30 && m31 == other.m31 && m32 == other.m32 && m33 == other.m33
        }
    }

    override fun toString(): String = buildString {
        append("Mat4d ( ")
        appendValue(this, m00)
        append(" | ")
        appendValue(this, m01)
        append(" | ")
        appendValue(this, m02)
        append(" | ")
        appendValue(this, m03)
        append(" )\n      ( ")
        appendValue(this, m10)
        append(" | ")
        appendValue(this, m11)
        append(" | ")
        appendValue(this, m12)
        append(" | ")
        appendValue(this, m13)
        append(" )\n      ( ")
        appendValue(this, m20)
        append(" | ")
        appendValue(this, m21)
        append(" | ")
        appendValue(this, m22)
        append(" | ")
        appendValue(this, m23)
        append(" )\n      ( ")
        appendValue(this, m30)
        append(" | ")
        appendValue(this, m31)
        append(" | ")
        appendValue(this, m32)
        append(" | ")
        appendValue(this, m33)
        append(" )")
    }

    private fun appendValue(builder: StringBuilder, value: Double) {
        // Add a whitespace if the current value is not negative.
        // Otherwise we will see a "-" sign.
        builder.append(if (value > 0) " " + valueAsString(value) else valueAsString(value))
    }

    private fun valueAsString(value: Double): String = buildString {
        append(value.toFourDecimalPlaces())
        append(" ".repeat(kotlin.math.max(0, 4 - toString().split('.')[1].length)))
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        buffer.putDouble(m00)
        buffer.putDouble(m01)
        buffer.putDouble(m02)
        buffer.putDouble(m03)
        buffer.putDouble(m10)
        buffer.putDouble(m11)
        buffer.putDouble(m12)
        buffer.putDouble(m13)
        buffer.putDouble(m20)
        buffer.putDouble(m21)
        buffer.putDouble(m22)
        buffer.putDouble(m23)
        buffer.putDouble(m30)
        buffer.putDouble(m31)
        buffer.putDouble(m32)
        buffer.putDouble(m33)
        return buffer
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        buffer.put(m00.toFloat())
        buffer.put(m01.toFloat())
        buffer.put(m02.toFloat())
        buffer.put(m03.toFloat())
        buffer.put(m10.toFloat())
        buffer.put(m11.toFloat())
        buffer.put(m12.toFloat())
        buffer.put(m13.toFloat())
        buffer.put(m20.toFloat())
        buffer.put(m21.toFloat())
        buffer.put(m22.toFloat())
        buffer.put(m23.toFloat())
        buffer.put(m30.toFloat())
        buffer.put(m31.toFloat())
        buffer.put(m32.toFloat())
        buffer.put(m33.toFloat())
        return buffer
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        buffer.put(m00)
        buffer.put(m01)
        buffer.put(m02)
        buffer.put(m03)
        buffer.put(m10)
        buffer.put(m11)
        buffer.put(m12)
        buffer.put(m13)
        buffer.put(m20)
        buffer.put(m21)
        buffer.put(m22)
        buffer.put(m23)
        buffer.put(m30)
        buffer.put(m31)
        buffer.put(m32)
        buffer.put(m33)
        return buffer
    }

    /**
     * Mat4d viewer, granting read-only access to its parent data.
     *
     * @author Lukas Potthast
     * @since ColibriEngine 0.0.7.2
     */
    inner class Viewer : Mat4dView {
        override val m00: Double = this@StdMat4d.m00
        override val m01: Double = this@StdMat4d.m01
        override val m02: Double = this@StdMat4d.m02
        override val m03: Double = this@StdMat4d.m03
        override val m10: Double = this@StdMat4d.m10
        override val m11: Double = this@StdMat4d.m11
        override val m12: Double = this@StdMat4d.m12
        override val m13: Double = this@StdMat4d.m13
        override val m20: Double = this@StdMat4d.m20
        override val m21: Double = this@StdMat4d.m21
        override val m22: Double = this@StdMat4d.m22
        override val m23: Double = this@StdMat4d.m23
        override val m30: Double = this@StdMat4d.m30
        override val m31: Double = this@StdMat4d.m31
        override val m32: Double = this@StdMat4d.m32
        override val m33: Double = this@StdMat4d.m33

        override fun get(row: Int, column: Int): Double = this@StdMat4d[row, column]

        override fun getRow(row: Int, storeIn: Vec4d): Vec4d = this@StdMat4d.getRow(row, storeIn)
        override fun getRow1(storeIn: Vec4d): Vec4d = this@StdMat4d.getRow1(storeIn)
        override fun getRow2(storeIn: Vec4d): Vec4d = this@StdMat4d.getRow2(storeIn)
        override fun getRow3(storeIn: Vec4d): Vec4d = this@StdMat4d.getRow3(storeIn)
        override fun getRow4(storeIn: Vec4d): Vec4d = this@StdMat4d.getRow4(storeIn)

        override fun getColumn(column: Int, storeIn: Vec4d): Vec4d = this@StdMat4d.getColumn(column, storeIn)
        override fun getColumn1(storeIn: Vec4d): Vec4d = this@StdMat4d.getColumn1(storeIn)
        override fun getColumn2(storeIn: Vec4d): Vec4d = this@StdMat4d.getColumn2(storeIn)
        override fun getColumn3(storeIn: Vec4d): Vec4d = this@StdMat4d.getColumn3(storeIn)
        override fun getColumn4(storeIn: Vec4d): Vec4d = this@StdMat4d.getColumn4(storeIn)

        override fun getMajorDiagonal(storeIn: Vec4d): Vec4d = this@StdMat4d.getMajorDiagonal(storeIn)

        override fun getRight(storeIn: Vec3d): Vec3d = this@StdMat4d.getRight(storeIn)
        override fun getUp(storeIn: Vec3d): Vec3d = this@StdMat4d.getUp(storeIn)
        override fun getForward(storeIn: Vec3d): Vec3d = this@StdMat4d.getForward(storeIn)

        override fun asArray(): Array<out DoubleArray> = this@StdMat4d.asArray()
        override fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray> = this@StdMat4d.asArray(storeIn)

        override fun asQuaternion(storeIn: QuaternionD): QuaternionD = this@StdMat4d.asQuaternion(storeIn)

        override fun getSubmatrix(row: Int, column: Int, storeIn: Mat3d): Mat3d =
            this@StdMat4d.getSubmatrix(row, column, storeIn)

        override fun transform(vector: Vec4d): Vec4d = this@StdMat4d.transform(vector)

        override fun determinant(): Double = this@StdMat4d.determinant()
        override fun hasZeroComponent(): Boolean = this@StdMat4d.hasZeroComponent()

        override fun bytes(): ByteArray = this@StdMat4d.bytes()

        override fun toFormattedString(): String = this@StdMat4d.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdMat4d.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdMat4d.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdMat4d.storeIn(buffer)
    }

    companion object {
        private val LOG = LogUtil.getLogger(StdMat4d::class.java)
    }

}
