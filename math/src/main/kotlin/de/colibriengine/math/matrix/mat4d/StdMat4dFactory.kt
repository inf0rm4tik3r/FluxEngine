package de.colibriengine.math.matrix.mat4d

/**
 * StdMat4dFactory.
 *
 *
 */
class StdMat4dFactory : Mat4dFactory {

    private val pool: StdMat4dPool = StdMat4dPool()

    override fun acquire(): Mat4d = pool.acquire()

    override fun acquireDirty(): Mat4d = pool.acquireDirty()

    override fun free(mat4d: Mat4d) = pool.free(mat4d)

    override fun immutableMat4dBuilder(): ImmutableMat4d.Builder = StdImmutableMat4d.builder()

    override fun zero(): ImmutableMat4d = ZERO

    override fun one(): ImmutableMat4d = ONE

    override fun identity(): ImmutableMat4d = IDENTITY

    companion object {
        val ZERO = StdImmutableMat4d.builder().of(0.0)
        val ONE = StdImmutableMat4d.builder().of(1.0)
        val IDENTITY = StdImmutableMat4d.builder().set(
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0
        ).build()
    }

}
