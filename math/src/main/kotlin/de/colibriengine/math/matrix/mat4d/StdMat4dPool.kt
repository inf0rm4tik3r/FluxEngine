package de.colibriengine.math.matrix.mat4d

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

/**
 * Mat4dPool.
 *
 * 
 */
class StdMat4dPool : ObjectPool<Mat4d>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdMat4d()
    },
    { instance: Mat4d -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdMat4dPool::class.java)
    }

}
