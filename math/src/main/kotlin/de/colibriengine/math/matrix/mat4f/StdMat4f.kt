package de.colibriengine.math.matrix.mat4f

import de.colibriengine.logging.LogUtil
import de.colibriengine.math.matrix.mat3f.Mat3f
import de.colibriengine.math.matrix.mat3f.Mat3fAccessor
import de.colibriengine.math.matrix.mat3f.StdMat3f
import de.colibriengine.math.matrix.mat4d.Mat4dAccessor
import de.colibriengine.math.quaternion.quaternionF.QuaternionF
import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.StdVec3fFactory
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec4f.Vec4f
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import de.colibriengine.util.isApproximately
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.absoluteValue
import kotlin.math.tan

/**
 * Mutable 4x4 matrix in float precision.
 *
 * 
 */
@Suppress("DuplicatedCode")
class StdMat4f(
    /* First row */
    m00: Float = 0.0f,
    m01: Float = 0.0f,
    m02: Float = 0.0f,
    m03: Float = 0.0f,
    /* Second row */
    m10: Float = 0.0f,
    m11: Float = 0.0f,
    m12: Float = 0.0f,
    m13: Float = 0.0f,
    /* Third row */
    m20: Float = 0.0f,
    m21: Float = 0.0f,
    m22: Float = 0.0f,
    m23: Float = 0.0f,
    /* Fourth row */
    m30: Float = 0.0f,
    m31: Float = 0.0f,
    m32: Float = 0.0f,
    m33: Float = 0.0f
) : Mat4f {

    private var _m00: Float = m00
    override var m00: Float
        get() = _m00
        set(value) {
            _m00 = value
        }

    private var _m01: Float = m01
    override var m01: Float
        get() = _m01
        set(value) {
            _m01 = value
        }

    private var _m02: Float = m02
    override var m02: Float
        get() = _m02
        set(value) {
            _m02 = value
        }

    private var _m03: Float = m03
    override var m03: Float
        get() = _m03
        set(value) {
            _m03 = value
        }

    private var _m10: Float = m10
    override var m10: Float
        get() = _m10
        set(value) {
            _m10 = value
        }

    private var _m11: Float = m11
    override var m11: Float
        get() = _m11
        set(value) {
            _m11 = value
        }

    private var _m12: Float = m12
    override var m12: Float
        get() = _m12
        set(value) {
            _m12 = value
        }

    private var _m13: Float = m13
    override var m13: Float
        get() = _m13
        set(value) {
            _m13 = value
        }

    private var _m20: Float = m20
    override var m20: Float
        get() = _m20
        set(value) {
            _m20 = value
        }

    private var _m21: Float = m21
    override var m21: Float
        get() = _m21
        set(value) {
            _m21 = value
        }

    private var _m22: Float = m22
    override var m22: Float
        get() = _m22
        set(value) {
            _m22 = value
        }

    private var _m23: Float = m23
    override var m23: Float
        get() = _m23
        set(value) {
            _m23 = value
        }

    private var _m30: Float = m30
    override var m30: Float
        get() = _m30
        set(value) {
            _m30 = value
        }

    private var _m31: Float = m31
    override var m31: Float
        get() = _m31
        set(value) {
            _m31 = value
        }

    private var _m32: Float = m32
    override var m32: Float
        get() = _m32
        set(value) {
            _m32 = value
        }

    private var _m33: Float = m33
    override var m33: Float
        get() = _m33
        set(value) {
            _m33 = value
        }

    private var _view: Mat4fView? = null
    override val view: Mat4fView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    override fun setM00(value: Float): Mat4f {
        _m00 = value
        return this
    }

    override fun setM01(value: Float): Mat4f {
        _m01 = value
        return this
    }

    override fun setM02(value: Float): Mat4f {
        _m02 = value
        return this
    }

    override fun setM03(value: Float): Mat4f {
        _m03 = value
        return this
    }

    override fun setM10(value: Float): Mat4f {
        _m10 = value
        return this
    }

    override fun setM11(value: Float): Mat4f {
        _m11 = value
        return this
    }

    override fun setM12(value: Float): Mat4f {
        _m12 = value
        return this
    }

    override fun setM13(value: Float): Mat4f {
        _m13 = value
        return this
    }

    override fun setM20(value: Float): Mat4f {
        _m20 = value
        return this
    }

    override fun setM21(value: Float): Mat4f {
        _m21 = value
        return this
    }

    override fun setM22(value: Float): Mat4f {
        _m22 = value
        return this
    }

    override fun setM23(value: Float): Mat4f {
        _m23 = value
        return this
    }

    override fun setM30(value: Float): Mat4f {
        _m30 = value
        return this
    }

    override fun setM31(value: Float): Mat4f {
        _m31 = value
        return this
    }

    override fun setM32(value: Float): Mat4f {
        _m32 = value
        return this
    }

    override fun setM33(value: Float): Mat4f {
        _m33 = value
        return this
    }

    override fun set(row: Int, column: Int, value: Float): Mat4f {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        when (row) {
            0 -> when (column) {
                0 -> _m00 = value
                1 -> _m01 = value
                2 -> _m02 = value
                3 -> _m03 = value
            }
            1 -> when (column) {
                0 -> _m10 = value
                1 -> _m11 = value
                2 -> _m12 = value
                3 -> _m13 = value
            }
            2 -> when (column) {
                0 -> _m20 = value
                1 -> _m21 = value
                2 -> _m22 = value
                3 -> _m23 = value
            }
            3 -> when (column) {
                0 -> _m30 = value
                1 -> _m31 = value
                2 -> _m32 = value
                3 -> _m33 = value
            }
        }
        return this
    }

    override fun setRow(row: Int, x: Float, y: Float, z: Float, w: Float): Mat4f {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        when (row) {
            0 -> {
                _m00 = x
                _m01 = y
                _m02 = z
                _m03 = w
            }
            1 -> {
                _m10 = x
                _m11 = y
                _m12 = z
                _m13 = w
            }
            2 -> {
                _m20 = x
                _m21 = y
                _m22 = z
                _m23 = w
            }
            3 -> {
                _m30 = x
                _m31 = y
                _m32 = z
                _m33 = w
            }
        }
        return this
    }

    override fun setRow1(x: Float, y: Float, z: Float, w: Float): Mat4f {
        _m00 = x
        _m01 = y
        _m02 = z
        _m03 = w
        return this
    }

    override fun setRow2(x: Float, y: Float, z: Float, w: Float): Mat4f {
        _m10 = x
        _m11 = y
        _m12 = z
        _m13 = w
        return this
    }

    override fun setRow3(x: Float, y: Float, z: Float, w: Float): Mat4f {
        _m20 = x
        _m21 = y
        _m22 = z
        _m23 = w
        return this
    }

    override fun setRow4(x: Float, y: Float, z: Float, w: Float): Mat4f {
        _m30 = x
        _m31 = y
        _m32 = z
        _m33 = w
        return this
    }

    override fun setRow(row: Int, vector: Vec4fAccessor): Mat4f {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        when (row) {
            0 -> {
                _m00 = vector.x
                _m01 = vector.y
                _m02 = vector.z
                _m03 = vector.w
            }
            1 -> {
                _m10 = vector.x
                _m11 = vector.y
                _m12 = vector.z
                _m13 = vector.w
            }
            2 -> {
                _m20 = vector.x
                _m21 = vector.y
                _m22 = vector.z
                _m23 = vector.w
            }
            3 -> {
                _m30 = vector.x
                _m31 = vector.y
                _m32 = vector.z
                _m33 = vector.w
            }
        }
        return this
    }

    override fun setRow1(vector: Vec4fAccessor): Mat4f {
        _m00 = vector.x
        _m01 = vector.y
        _m02 = vector.z
        _m03 = vector.w
        return this
    }

    override fun setRow2(vector: Vec4fAccessor): Mat4f {
        _m10 = vector.x
        _m11 = vector.y
        _m12 = vector.z
        _m13 = vector.w
        return this
    }

    override fun setRow3(vector: Vec4fAccessor): Mat4f {
        _m20 = vector.x
        _m21 = vector.y
        _m22 = vector.z
        _m23 = vector.w
        return this
    }

    override fun setRow4(vector: Vec4fAccessor): Mat4f {
        _m30 = vector.x
        _m31 = vector.y
        _m32 = vector.z
        _m33 = vector.w
        return this
    }

    override fun setColumn(column: Int, x: Float, y: Float, z: Float, w: Float): Mat4f {
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        when (column) {
            0 -> {
                _m00 = x
                _m10 = y
                _m20 = z
                _m30 = w
            }
            1 -> {
                _m01 = x
                _m11 = y
                _m21 = z
                _m31 = w
            }
            2 -> {
                _m02 = x
                _m12 = y
                _m22 = z
                _m32 = w
            }
            3 -> {
                _m03 = x
                _m13 = y
                _m23 = z
                _m33 = w
            }
        }
        return this
    }

    override fun setColumn1(x: Float, y: Float, z: Float, w: Float): Mat4f {
        _m00 = x
        _m10 = y
        _m20 = z
        _m30 = w
        return this
    }

    override fun setColumn2(x: Float, y: Float, z: Float, w: Float): Mat4f {
        _m01 = x
        _m11 = y
        _m21 = z
        _m31 = w
        return this
    }

    override fun setColumn3(x: Float, y: Float, z: Float, w: Float): Mat4f {
        _m02 = x
        _m12 = y
        _m22 = z
        _m32 = w
        return this
    }

    override fun setColumn4(x: Float, y: Float, z: Float, w: Float): Mat4f {
        _m03 = x
        _m13 = y
        _m23 = z
        _m33 = w
        return this
    }

    override fun setColumn(column: Int, vector: Vec4fAccessor): Mat4f {
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        when (column) {
            0 -> {
                _m00 = vector.x
                _m10 = vector.y
                _m20 = vector.z
                _m30 = vector.w
            }
            1 -> {
                _m01 = vector.x
                _m11 = vector.y
                _m21 = vector.z
                _m31 = vector.w
            }
            2 -> {
                _m02 = vector.x
                _m12 = vector.y
                _m22 = vector.z
                _m32 = vector.w
            }
            3 -> {
                _m03 = vector.x
                _m13 = vector.y
                _m23 = vector.z
                _m33 = vector.w
            }
        }
        return this
    }

    override fun setColumn1(vector: Vec4fAccessor): Mat4f {
        _m00 = vector.x
        _m10 = vector.y
        _m20 = vector.z
        _m30 = vector.w
        return this
    }

    override fun setColumn2(vector: Vec4fAccessor): Mat4f {
        _m01 = vector.x
        _m11 = vector.y
        _m21 = vector.z
        _m31 = vector.w
        return this
    }

    override fun setColumn3(vector: Vec4fAccessor): Mat4f {
        _m02 = vector.x
        _m12 = vector.y
        _m22 = vector.z
        _m32 = vector.w
        return this
    }

    override fun setColumn4(vector: Vec4fAccessor): Mat4f {
        _m03 = vector.x
        _m13 = vector.y
        _m23 = vector.z
        _m33 = vector.w
        return this
    }

    override fun set(value: Float): Mat4f {
        _m00 = value
        _m01 = value
        _m02 = value
        _m03 = value
        _m10 = value
        _m11 = value
        _m12 = value
        _m13 = value
        _m20 = value
        _m21 = value
        _m22 = value
        _m23 = value
        _m30 = value
        _m31 = value
        _m32 = value
        _m33 = value
        return this
    }

    override fun set(
        m00: Float, m01: Float, m02: Float, m03: Float,
        m10: Float, m11: Float, m12: Float, m13: Float,
        m20: Float, m21: Float, m22: Float, m23: Float,
        m30: Float, m31: Float, m32: Float, m33: Float
    ): Mat4f {
        this._m00 = m00
        this._m01 = m01
        this._m02 = m02
        this._m03 = m03
        this._m10 = m10
        this._m11 = m11
        this._m12 = m12
        this._m13 = m13
        this._m20 = m20
        this._m21 = m21
        this._m22 = m22
        this._m23 = m23
        this._m30 = m30
        this._m31 = m31
        this._m32 = m32
        this._m33 = m33
        return this
    }

    override fun set(other: Mat4fAccessor): Mat4f {
        _m00 = other.m00
        _m01 = other.m01
        _m02 = other.m02
        _m03 = other.m03
        _m10 = other.m10
        _m11 = other.m11
        _m12 = other.m12
        _m13 = other.m13
        _m20 = other.m20
        _m21 = other.m21
        _m22 = other.m22
        _m23 = other.m23
        _m30 = other.m30
        _m31 = other.m31
        _m32 = other.m32
        _m33 = other.m33
        return this
    }

    override fun set(other: Mat4dAccessor): Mat4f {
        _m00 = other.m00.toFloat()
        _m01 = other.m01.toFloat()
        _m02 = other.m02.toFloat()
        _m03 = other.m03.toFloat()
        _m10 = other.m10.toFloat()
        _m11 = other.m11.toFloat()
        _m12 = other.m12.toFloat()
        _m13 = other.m13.toFloat()
        _m20 = other.m20.toFloat()
        _m21 = other.m21.toFloat()
        _m22 = other.m22.toFloat()
        _m23 = other.m23.toFloat()
        _m30 = other.m30.toFloat()
        _m31 = other.m31.toFloat()
        _m32 = other.m32.toFloat()
        _m33 = other.m33.toFloat()
        return this
    }

    override fun set(other: Mat3fAccessor): Mat4f {
        _m00 = other.m00
        _m01 = other.m01
        _m02 = other.m02
        _m03 = 0.0f
        _m10 = other.m10
        _m11 = other.m11
        _m12 = other.m12
        _m13 = 0.0f
        _m20 = other.m20
        _m21 = other.m21
        _m22 = other.m22
        _m23 = 0.0f
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 0.0f
        _m33 = 1.0f
        return this
    }

    override fun set(other: Mat3fAccessor, fillWith: Float): Mat4f {
        _m00 = other.m00
        _m01 = other.m01
        _m02 = other.m02
        _m03 = fillWith
        _m10 = other.m10
        _m11 = other.m11
        _m12 = other.m12
        _m13 = fillWith
        _m20 = other.m20
        _m21 = other.m21
        _m22 = other.m22
        _m23 = fillWith
        _m33 = fillWith
        _m32 = fillWith
        _m31 = fillWith
        _m30 = fillWith
        return this
    }

    override fun set(
        other: Mat3fAccessor,
        fillWith03: Float, fillWith13: Float, fillWith23: Float,
        fillWith30: Float, fillWith31: Float, fillWith32: Float,
        fillWith33: Float
    ): Mat4f {
        _m00 = other.m00
        _m01 = other.m01
        _m02 = other.m02
        _m03 = fillWith03
        _m10 = other.m10
        _m11 = other.m11
        _m12 = other.m12
        _m13 = fillWith13
        _m20 = other.m20
        _m21 = other.m21
        _m22 = other.m22
        _m23 = fillWith23
        _m30 = fillWith30
        _m31 = fillWith31
        _m32 = fillWith32
        _m33 = fillWith33
        return this
    }

    override fun set(matrixData: Array<FloatArray>): Mat4f {
        assert(matrixData.size == Mat4f.ROW_AMT && matrixData[0].size == Mat4f.COLUMN_AMT) {
            "Array dimensions do not match. Copy not possible"
        }
        _m00 = matrixData[0][0]
        _m01 = matrixData[0][1]
        _m02 = matrixData[0][2]
        _m03 = matrixData[0][3]
        _m10 = matrixData[1][0]
        _m11 = matrixData[1][1]
        _m12 = matrixData[1][2]
        _m13 = matrixData[1][3]
        _m20 = matrixData[2][0]
        _m21 = matrixData[2][1]
        _m22 = matrixData[2][2]
        _m23 = matrixData[2][3]
        _m30 = matrixData[3][0]
        _m31 = matrixData[3][1]
        _m32 = matrixData[3][2]
        _m33 = matrixData[3][3]
        return this
    }

    override fun initZero(): Mat4f = set(0.0f)

    override fun initIdentity(): Mat4f {
        _m00 = 1.0f
        _m01 = 0.0f
        _m02 = 0.0f
        _m03 = 0.0f
        _m10 = 0.0f
        _m11 = 1.0f
        _m12 = 0.0f
        _m13 = 0.0f
        _m20 = 0.0f
        _m21 = 0.0f
        _m22 = 1.0f
        _m23 = 0.0f
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 0.0f
        _m33 = 1.0f
        return this
    }

    override fun initBias(): Mat4f {
        _m00 = 0.5f
        _m01 = 0.0f
        _m02 = 0.0f
        _m03 = 0.5f
        _m10 = 0.0f
        _m11 = 0.5f
        _m12 = 0.0f
        _m13 = 0.5f
        _m20 = 0.0f
        _m21 = 0.0f
        _m22 = 0.5f
        _m23 = 0.5f
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 0.0f
        _m33 = 1.0f
        return this
    }

    override fun initTranslation(x: Float, y: Float, z: Float): Mat4f {
        _m00 = 1.0f
        _m01 = 0.0f
        _m02 = 0.0f
        _m03 = x
        _m10 = 0.0f
        _m11 = 1.0f
        _m12 = 0.0f
        _m13 = y
        _m20 = 0.0f
        _m21 = 0.0f
        _m22 = 1.0f
        _m23 = z
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 0.0f
        _m33 = 1.0f
        return this
    }

    override fun initTranslation(translation: Vec3fAccessor): Mat4f =
        initTranslation(translation.x, translation.y, translation.z)

    // TODO: Optimize?
    override fun initRotation(angleAroundX: Float, angleAroundY: Float, angleAroundZ: Float): Mat4f =
        set(StdMat3f().initRotation(angleAroundX, angleAroundY, angleAroundZ))

    override fun initRotation(angles: Vec3fAccessor): Mat4f =
        initRotation(angles.x, angles.y, angles.z)

    // TODO: Optimize?
    override fun initRotation(axis: Vec3fAccessor, angle: Float): Mat4f =
        set(StdMat3f().initRotation(axis, angle))

    override fun initRotation(forward: Vec3fAccessor, up: Vec3fAccessor, right: Vec3fAccessor): Mat4f {
        assert(isApproximately(forward.length(), 1.0f, 0.01f)) { "Forward vector not normalised." }
        assert(isApproximately(up.length(), 1.0f, 0.01f)) { "Up vector not normalised." }
        assert(isApproximately(right.length(), 1.0f, 0.01f)) { "Right vector not normalised." }
        // First row.
        _m00 = right.x
        _m01 = right.y
        _m02 = right.z
        _m03 = m13
        // Second row.
        _m10 = up.x
        _m11 = up.y
        _m12 = up.z
        _m13 = 0.0f
        // Third row.
        _m20 = forward.x
        _m21 = forward.y
        _m22 = forward.z
        _m23 = 0.0f
        // Fourth row and column.
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 0.0f
        _m33 = 1.0f
        return this
    }

    // TODO: Optimize?
    override fun initRotation(forward: Vec3fAccessor, up: Vec3fAccessor): Mat4f {
        // Assuming that forward and up are normalized. // TODO: assert?
        val right: Vec3f = StdVec3f().set(up).cross(forward)
        return initRotation(forward, up, right)
    }

    // TODO: Optimize?
    override fun initRotation(rotation: QuaternionFAccessor): Mat4f {
        val forward: Vec3f = rotation.forward(StdVec3f())
        val up: Vec3f = rotation.up(StdVec3f())
        val right: Vec3f = rotation.right(StdVec3f())
        return initRotation(forward, up, right)
        // TODO: Compare! This would be a lot faster and easier!
        //return rotation.asMatrix(this);
    }

    override fun initScale(scaleX: Float, scaleY: Float, scaleZ: Float): Mat4f {
        _m00 = scaleX
        _m01 = 0.0f
        _m02 = 0.0f
        _m03 = 0.0f
        _m10 = 0.0f
        _m11 = scaleY
        _m12 = 0.0f
        _m13 = 0.0f
        _m20 = 0.0f
        _m21 = 0.0f
        _m22 = scaleZ
        _m23 = 0.0f
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 0.0f
        _m33 = 1.0f
        return this
    }

    override fun initScale(scale: Float): Mat4f = initScale(scale, scale, scale)

    override fun initScale(scaleView: Vec3fAccessor): Mat4f =
        initScale(scaleView.x, scaleView.y, scaleView.z)

    override fun initFrustumProjection(
        left: Float, right: Float,
        bottom: Float, top: Float,
        zNear: Float, zFar: Float
    ): Mat4f {
        // First row.
        _m00 = 2.0f * zNear / (right - left)
        _m01 = 0.0f
        _m02 = (right + left) / (right - left)
        _m03 = 0.0f
        // Second row.
        _m10 = 0.0f
        _m11 = 2.0f * zNear / (top - bottom)
        _m12 = (top + bottom) / (top - bottom)
        _m13 = 0.0f
        // Third row.
        _m20 = 0.0f
        _m21 = 0.0f
        _m22 = (zNear + zFar) / (zNear - zFar)
        _m23 = 2.0f * zNear * zFar / (zNear - zFar)
        // Fourth row.
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = -1.0f
        _m33 = 0.0f
        return this
    }

    override fun initPerspectiveProjectionLH(
        fov: Float, aspectRatio: Float,
        zNear: Float, zFar: Float
    ): Mat4f {
        assert(fov > 0)
        assert(aspectRatio > 0)
        assert(zNear < zFar)
        val cotanHalfFOV = 1.0f / tan(Math.toRadians(fov / 2.0)).toFloat()
        val zRange = zNear - zFar
        // First row.
        _m00 = cotanHalfFOV / aspectRatio
        _m01 = 0.0f
        _m02 = 0.0f
        _m03 = 0.0f
        // Second row.
        _m10 = 0.0f
        _m11 = cotanHalfFOV
        _m12 = 0.0f
        _m13 = 0.0f
        // Third row.
        _m20 = 0.0f
        _m21 = 0.0f
        _m22 = zFar / (zFar - zNear) // z-Axis flipped!
        _m23 = zNear * zFar / zRange
        // Fourth row.
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 1.0f // <<<---- Defines LH coordinate system.
        _m33 = 0.0f
        return this
    }

    override fun initPerspectiveProjectionRH(
        fov: Float, aspectRatio: Float,
        zNear: Float, zFar: Float
    ): Mat4f {
        assert(fov > 0)
        assert(aspectRatio > 0)
        assert(zNear < zFar)
        val cotanHalfFOV = 1.0f / tan(Math.toRadians(fov / 2.0)).toFloat()
        val zRange = zNear - zFar
        // First row.
        _m00 = cotanHalfFOV / aspectRatio
        _m01 = 0.0f
        _m02 = 0.0f
        _m03 = 0.0f
        // Second row.
        _m10 = 0.0f
        _m11 = cotanHalfFOV
        _m12 = 0.0f
        _m13 = 0.0f
        // Third row.
        _m20 = 0.0f
        _m21 = 0.0f
        _m22 = zFar / zRange
        _m23 = zNear * zFar / zRange
        // Fourth row.
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = -1.0f // <<<---- Defines RH coordinate system.
        _m33 = 0.0f
        return this
    }

    override fun initOrthographicProjectionLH(
        left: Float, right: Float,
        bottom: Float, top: Float,
        zNear: Float, zFar: Float
    ): Mat4f {
        val width = right - left
        val height = top - bottom
        val zRange = zFar - zNear
        // First row.
        _m00 = 2.0f / width
        _m01 = 0.0f
        _m02 = 0.0f
        _m03 = -(right + left) / width
        // Second row.
        _m10 = 0.0f
        _m11 = 2.0f / height
        _m12 = 0.0f
        _m13 = -(top + bottom) / height
        // Third row.
        _m20 = 0.0f
        _m21 = 0.0f
        _m22 = -(2.0f / zRange)
        _m23 = -(zFar + zNear) / zRange // TODO: <- Is this correct? See difference in perspective.
        // Fourth row.
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 1.0f // TODO: <- Is this correct?
        _m33 = 1.0f
        return this
    }

    override fun initOrthographicProjectionRH(
        left: Float, right: Float,
        bottom: Float, top: Float,
        zNear: Float, zFar: Float
    ): Mat4f {
        val width = right - left
        val height = top - bottom
        val zRange = zFar - zNear
        // First row.
        _m00 = 2.0f / width
        _m01 = 0.0f
        _m02 = 0.0f
        _m03 = -(right + left) / width
        // Second row.
        _m10 = 0.0f
        _m11 = 2.0f / height
        _m12 = 0.0f
        _m13 = -(top + bottom) / height
        // Third row.
        _m20 = 0.0f
        _m21 = 0.0f
        _m22 = 2.0f / zRange
        _m23 = -(zFar + zNear) / zRange
        // Fourth row.
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 0.0f
        _m33 = 1.0f
        return this
    }

    override fun initLookAtRH(
        source: Vec3fAccessor,
        targetUp: Vec3fAccessor,
        pointOfInterest: Vec3fAccessor
    ): Mat4f {
        // Construct the normalized forward vector.
        val forward: Vec3f = StdVec3f().set(source).minus(pointOfInterest)
        when (forward.length()) {
            0.0f -> {
                LOG.warn("Source and PointOfInterest were equal. Was unable to calculate proper forward vector.")
                forward.set(StdVec3fFactory.NEG_UNIT_Z_AXIS)
            }
            else -> {
                forward.normalize()
            }
        }

        // Construct a side vector right by taking the cross product of the new forward vector and the camera up vector.
        val right: Vec3f = StdVec3f().set(targetUp).cross(forward)
        when (right.length()) {
            0.0f -> {
                LOG.warn("Was unable to calculate proper right vector.")
                right.set(StdVec3fFactory.UNIT_X_AXIS)
            }
            else -> {
                right.normalize()
            }
        }

        // There is no certainty that the targetUp and our new forward vector are perpendicular to each other.
        // => Construct a NEW up vector. No normalization necessary, as forward and right are already normalized.
        val up: Vec3f = StdVec3f().set(forward).cross(right)

        // First row.
        _m00 = right.x
        _m01 = right.y
        _m02 = right.z
        _m03 = -source.dot(right)
        // Second row.
        _m10 = up.x
        _m11 = up.y
        _m12 = up.z
        _m13 = -source.dot(up)
        // Third row.
        _m20 = forward.x
        _m21 = forward.y
        _m22 = forward.z
        _m23 = -source.dot(forward)
        // Fourth row.
        _m30 = 0.0f
        _m31 = 0.0f
        _m32 = 0.0f
        _m33 = 1.0f
        return this
    }

    override fun transform(vector: Vec4f): Vec4f {
        return vector.set(
            _m00 * vector.x + _m01 * vector.y + _m02 * vector.z + _m03 * vector.w,
            _m10 * vector.x + _m11 * vector.y + _m12 * vector.z + _m13 * vector.w,
            _m20 * vector.x + _m21 * vector.y + _m22 * vector.z + _m23 * vector.w,
            _m30 * vector.x + _m31 * vector.y + _m32 * vector.z + _m33 * vector.w
        )
    }

    override fun plus(value: Float): Mat4f {
        _m00 += value
        _m01 += value
        _m02 += value
        _m03 += value
        _m10 += value
        _m11 += value
        _m12 += value
        _m13 += value
        _m20 += value
        _m21 += value
        _m22 += value
        _m23 += value
        _m30 += value
        _m31 += value
        _m32 += value
        _m33 += value
        return this
    }

    override fun plus(other: Mat4fAccessor): Mat4f {
        _m00 += other.m00
        _m01 += other.m01
        _m02 += other.m02
        _m03 += other.m03
        _m10 += other.m10
        _m11 += other.m11
        _m12 += other.m12
        _m13 += other.m13
        _m20 += other.m20
        _m21 += other.m21
        _m22 += other.m22
        _m23 += other.m23
        _m30 += other.m30
        _m31 += other.m31
        _m32 += other.m32
        _m33 += other.m33
        return this
    }

    override fun minus(value: Float): Mat4f {
        _m00 -= value
        _m01 -= value
        _m02 -= value
        _m03 -= value
        _m10 -= value
        _m11 -= value
        _m12 -= value
        _m13 -= value
        _m20 -= value
        _m21 -= value
        _m22 -= value
        _m23 -= value
        _m30 -= value
        _m31 -= value
        _m32 -= value
        _m33 -= value
        return this
    }

    override fun minus(other: Mat4fAccessor): Mat4f {
        _m00 -= other.m00
        _m01 -= other.m01
        _m02 -= other.m02
        _m03 -= other.m03
        _m10 -= other.m10
        _m11 -= other.m11
        _m12 -= other.m12
        _m13 -= other.m13
        _m20 -= other.m20
        _m21 -= other.m21
        _m22 -= other.m22
        _m23 -= other.m23
        _m30 -= other.m30
        _m31 -= other.m31
        _m32 -= other.m32
        _m33 -= other.m33
        return this
    }

    override fun times(value: Float): Mat4f {
        _m00 *= value
        _m01 *= value
        _m02 *= value
        _m03 *= value
        _m10 *= value
        _m11 *= value
        _m12 *= value
        _m13 *= value
        _m20 *= value
        _m21 *= value
        _m22 *= value
        _m23 *= value
        _m30 *= value
        _m31 *= value
        _m32 *= value
        _m33 *= value
        return this
    }

    /*
     * - 64 multiplications
     * - 48 additions
     * - 32 assignments (16 final)
     */
    override fun times(other: Mat4fAccessor): Mat4f {
        assert(this !== other) { "Cannot multiply with itself!" }
        // First new row.
        var r0: Float = _m00 * other.m00 + _m01 * other.m10 + _m02 * other.m20 + _m03 * other.m30
        var r1: Float = _m00 * other.m01 + _m01 * other.m11 + _m02 * other.m21 + _m03 * other.m31
        var r2: Float = _m00 * other.m02 + _m01 * other.m12 + _m02 * other.m22 + _m03 * other.m32
        var r3: Float = _m00 * other.m03 + _m01 * other.m13 + _m02 * other.m23 + _m03 * other.m33
        _m00 = r0
        _m01 = r1
        _m02 = r2
        _m03 = r3
        // Second new row.
        r0 = _m10 * other.m00 + _m11 * other.m10 + _m12 * other.m20 + _m13 * other.m30
        r1 = _m10 * other.m01 + _m11 * other.m11 + _m12 * other.m21 + _m13 * other.m31
        r2 = _m10 * other.m02 + _m11 * other.m12 + _m12 * other.m22 + _m13 * other.m32
        r3 = _m10 * other.m03 + _m11 * other.m13 + _m12 * other.m23 + _m13 * other.m33
        _m10 = r0
        _m11 = r1
        _m12 = r2
        _m13 = r3
        // Third new row.
        r0 = _m20 * other.m00 + _m21 * other.m10 + _m22 * other.m20 + _m23 * other.m30
        r1 = _m20 * other.m01 + _m21 * other.m11 + _m22 * other.m21 + _m23 * other.m31
        r2 = _m20 * other.m02 + _m21 * other.m12 + _m22 * other.m22 + _m23 * other.m32
        r3 = _m20 * other.m03 + _m21 * other.m13 + _m22 * other.m23 + _m23 * other.m33
        _m20 = r0
        _m21 = r1
        _m22 = r2
        _m23 = r3
        // Fourth new row.
        r0 = _m30 * other.m00 + _m31 * other.m10 + _m32 * other.m20 + _m33 * other.m30
        r1 = _m30 * other.m01 + _m31 * other.m11 + _m32 * other.m21 + _m33 * other.m31
        r2 = _m30 * other.m02 + _m31 * other.m12 + _m32 * other.m22 + _m33 * other.m32
        r3 = _m30 * other.m03 + _m31 * other.m13 + _m32 * other.m23 + _m33 * other.m33
        _m30 = r0
        _m31 = r1
        _m32 = r2
        _m33 = r3
        return this
    }

    override fun mulSelf(): Mat4f {
        // First new row.
        val r00 = _m00 * _m00 + _m01 * _m10 + _m02 * _m20 + _m03 * _m30
        val r01 = _m00 * _m01 + _m01 * _m11 + _m02 * _m21 + _m03 * _m31
        val r02 = _m00 * _m02 + _m01 * _m12 + _m02 * _m22 + _m03 * _m32
        val r03 = _m00 * _m03 + _m01 * _m13 + _m02 * _m23 + _m03 * _m33
        // Second new row.
        val r10 = _m10 * _m00 + _m11 * _m10 + _m12 * _m20 + _m13 * _m30
        val r11 = _m10 * _m01 + _m11 * _m11 + _m12 * _m21 + _m13 * _m31
        val r12 = _m10 * _m02 + _m11 * _m12 + _m12 * _m22 + _m13 * _m32
        val r13 = _m10 * _m03 + _m11 * _m13 + _m12 * _m23 + _m13 * _m33
        // Third new row.
        val r20 = _m20 * _m00 + _m21 * _m10 + _m22 * _m20 + _m23 * _m30
        val r21 = _m20 * _m01 + _m21 * _m11 + _m22 * _m21 + _m23 * _m31
        val r22 = _m20 * _m02 + _m21 * _m12 + _m22 * _m22 + _m23 * _m32
        val r23 = _m20 * _m03 + _m21 * _m13 + _m22 * _m23 + _m23 * _m33
        // Fourth new row.
        val r30 = _m30 * _m00 + _m31 * _m10 + _m32 * _m20 + _m33 * _m30
        val r31 = _m30 * _m01 + _m31 * _m11 + _m32 * _m21 + _m33 * _m31
        val r32 = _m30 * _m02 + _m31 * _m12 + _m32 * _m22 + _m33 * _m32
        val r33 = _m30 * _m03 + _m31 * _m13 + _m32 * _m23 + _m33 * _m33
        // Update this instance.
        _m00 = r00
        _m01 = r01
        _m02 = r02
        _m03 = r03
        _m10 = r10
        _m11 = r11
        _m12 = r12
        _m13 = r13
        _m20 = r20
        _m21 = r21
        _m22 = r22
        _m23 = r23
        _m30 = r30
        _m31 = r31
        _m32 = r32
        _m33 = r33
        return this
    }

    override fun inverse(): Mat4f {
        // Calculate 1 over the determinant of the initial matrix.
        val oneOverDeterminant = 1.0f / determinant()

        // Calculate cofactors.
        val c00 = +StdMat3f.determinantOf(_m11, _m12, _m13, _m21, _m22, _m23, _m31, _m32, _m33)
        val c01 = -StdMat3f.determinantOf(_m10, _m12, _m13, _m20, _m22, _m23, _m30, _m32, _m33)
        val c02 = +StdMat3f.determinantOf(_m10, _m11, _m13, _m20, _m21, _m23, _m30, _m31, _m33)
        val c03 = -StdMat3f.determinantOf(_m10, _m11, _m12, _m20, _m21, _m22, _m30, _m31, _m32)
        val c10 = -StdMat3f.determinantOf(_m01, _m02, _m03, _m21, _m22, _m23, _m31, _m32, _m33)
        val c11 = +StdMat3f.determinantOf(_m00, _m02, _m03, _m20, _m22, _m23, _m30, _m32, _m33)
        val c12 = -StdMat3f.determinantOf(_m00, _m01, _m03, _m20, _m21, _m23, _m30, _m31, _m33)
        val c13 = +StdMat3f.determinantOf(_m00, _m01, _m02, _m20, _m21, _m22, _m30, _m31, _m32)
        val c20 = +StdMat3f.determinantOf(_m01, _m02, _m03, _m11, _m12, _m13, _m31, _m32, _m33)
        val c21 = -StdMat3f.determinantOf(_m00, _m02, _m03, _m10, _m12, _m13, _m30, _m32, _m33)
        val c22 = +StdMat3f.determinantOf(_m00, _m01, _m03, _m10, _m11, _m13, _m30, _m31, _m33)
        val c23 = -StdMat3f.determinantOf(_m00, _m01, _m02, _m10, _m11, _m12, _m30, _m31, _m32)
        val c30 = -StdMat3f.determinantOf(_m01, _m02, _m03, _m11, _m12, _m13, _m21, _m22, _m23)
        val c31 = +StdMat3f.determinantOf(_m00, _m02, _m03, _m10, _m12, _m13, _m20, _m22, _m23)
        val c32 = -StdMat3f.determinantOf(_m00, _m01, _m03, _m10, _m11, _m13, _m20, _m21, _m23)
        val c33 = +StdMat3f.determinantOf(_m00, _m01, _m02, _m10, _m11, _m12, _m20, _m21, _m22)

        // ... and return the result multiplied with the transposed cofactor matrix.
        return set(c00, c01, c02, c03, c10, c11, c12, c13, c20, c21, c22, c23, c30, c31, c32, c33)
            .transpose().times(oneOverDeterminant)
    }

    override fun transpose(): Mat4f {
        // Swap 10 with 01.
        var temp: Float = _m10
        _m10 = _m01
        _m01 = temp
        // Swap 21 with 12.
        temp = _m21
        _m21 = _m12
        _m12 = temp
        // Swap 32 with 23.
        temp = _m32
        _m32 = _m23
        _m23 = temp
        // Swap 31 with 13.
        temp = _m31
        _m31 = _m13
        _m13 = temp
        // Swap 20 with 02.
        temp = _m20
        _m20 = _m02
        _m02 = temp
        // Swap 30 with 03.
        temp = _m30
        _m30 = _m03
        _m03 = temp
        return this
    }

    @Suppress("ReplaceGetOrSet")
    override fun getSubmatrix(row: Int, column: Int, storeIn: Mat3f): Mat3f {
        assert(row in 0..3) { "Row to ignore (row) is not in range [0..3]!" }
        assert(column in 0..3) { "Column to ignore (column) is not in range [0..3]!" }
        when (row) {
            0 -> when (column) {
                0 -> return storeIn.set(_m11, _m12, _m13, _m21, _m22, _m23, _m31, _m32, _m33)
                1 -> return storeIn.set(_m10, _m12, _m13, _m20, _m22, _m23, _m30, _m32, _m33)
                2 -> return storeIn.set(_m10, _m11, _m13, _m20, _m21, _m23, _m30, _m31, _m33)
                3 -> return storeIn.set(_m10, _m11, _m12, _m20, _m21, _m22, _m30, _m31, _m32)
            }
            1 -> when (column) {
                0 -> return storeIn.set(_m01, _m02, _m03, _m21, _m22, _m23, _m31, _m32, _m33)
                1 -> return storeIn.set(_m00, _m02, _m03, _m20, _m22, _m23, _m30, _m32, _m33)
                2 -> return storeIn.set(_m00, _m01, _m03, _m20, _m21, _m23, _m30, _m31, _m33)
                3 -> return storeIn.set(_m00, _m01, _m02, _m20, _m21, _m22, _m30, _m31, _m32)
            }
            2 -> when (column) {
                0 -> return storeIn.set(_m01, _m02, _m03, _m11, _m12, _m13, _m31, _m32, _m33)
                1 -> return storeIn.set(_m00, _m02, _m03, _m10, _m12, _m13, _m30, _m32, _m33)
                2 -> return storeIn.set(_m00, _m01, _m03, _m10, _m11, _m13, _m30, _m31, _m33)
                3 -> return storeIn.set(_m00, _m01, _m02, _m10, _m11, _m12, _m30, _m31, _m32)
            }
            3 -> when (column) {
                0 -> return storeIn.set(_m01, _m02, _m03, _m11, _m12, _m13, _m21, _m22, _m23)
                1 -> return storeIn.set(_m00, _m02, _m03, _m10, _m12, _m13, _m20, _m22, _m23)
                2 -> return storeIn.set(_m00, _m01, _m03, _m10, _m11, _m13, _m20, _m21, _m23)
                3 -> return storeIn.set(_m00, _m01, _m02, _m10, _m11, _m12, _m20, _m21, _m22)
            }
        }
        throw IllegalArgumentException("Row or column not in range [0..2]!")
    }

    override fun pow(exponent: Int): Mat4f {
        require(exponent >= -1) { "Exponents below -1 are not supported. Given: $exponent" }
        return when (exponent) {
            -1 -> inverse()
            0 -> initIdentity()
            1 -> this
            else -> {
                var i = 1
                while (i < exponent) {
                    mulSelf()
                    i++
                }
                this
            }
        }
    }

    override fun abs(): Mat4f {
        _m00 = _m00.absoluteValue
        _m01 = _m01.absoluteValue
        _m02 = _m02.absoluteValue
        _m03 = _m03.absoluteValue
        _m10 = _m10.absoluteValue
        _m11 = _m11.absoluteValue
        _m12 = _m12.absoluteValue
        _m13 = _m13.absoluteValue
        _m20 = _m20.absoluteValue
        _m21 = _m21.absoluteValue
        _m22 = _m22.absoluteValue
        _m23 = _m23.absoluteValue
        _m30 = _m30.absoluteValue
        _m31 = _m31.absoluteValue
        _m32 = _m32.absoluteValue
        _m33 = _m33.absoluteValue
        return this
    }

    override fun shorten(): Mat4f {
        _m00 = _m00.toFourDecimalPlaces()
        _m01 = _m01.toFourDecimalPlaces()
        _m02 = _m02.toFourDecimalPlaces()
        _m03 = _m03.toFourDecimalPlaces()
        _m10 = _m10.toFourDecimalPlaces()
        _m11 = _m11.toFourDecimalPlaces()
        _m12 = _m12.toFourDecimalPlaces()
        _m13 = _m13.toFourDecimalPlaces()
        _m20 = _m20.toFourDecimalPlaces()
        _m21 = _m21.toFourDecimalPlaces()
        _m22 = _m22.toFourDecimalPlaces()
        _m23 = _m23.toFourDecimalPlaces()
        _m30 = _m30.toFourDecimalPlaces()
        _m31 = _m31.toFourDecimalPlaces()
        _m32 = _m32.toFourDecimalPlaces()
        _m33 = _m33.toFourDecimalPlaces()
        return this
    }

    override operator fun get(row: Int, column: Int): Float {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        when (row) {
            0 -> when (column) {
                0 -> return _m00
                1 -> return _m01
                2 -> return _m02
                3 -> return _m03
            }
            1 -> when (column) {
                0 -> return _m10
                1 -> return _m11
                2 -> return _m12
                3 -> return _m13
            }
            2 -> when (column) {
                0 -> return _m20
                1 -> return _m21
                2 -> return _m22
                3 -> return _m23
            }
            3 -> when (column) {
                0 -> return _m30
                1 -> return _m31
                2 -> return _m32
                3 -> return _m33
            }
        }
        throw IllegalArgumentException(
            "Row($row) and column($column) indices were not both in range [0..3]!"
        )
    }

    override fun getRow(row: Int, storeIn: Vec4f): Vec4f {
        assert(row in 0..3) { "Row index was not in range [0..3]!" }
        when (row) {
            0 -> return storeIn.set(_m00, _m01, _m02, _m03)
            1 -> return storeIn.set(_m10, _m11, _m12, _m13)
            2 -> return storeIn.set(_m20, _m21, _m22, _m23)
            3 -> return storeIn.set(_m30, _m31, _m32, _m33)
        }
        throw IllegalArgumentException("Row($row) index was not in range [0..3]!")
    }

    override fun getRow1(storeIn: Vec4f): Vec4f = storeIn.set(_m00, _m01, _m02, _m03)

    override fun getRow2(storeIn: Vec4f): Vec4f = storeIn.set(_m10, _m11, _m12, _m13)

    override fun getRow3(storeIn: Vec4f): Vec4f = storeIn.set(_m20, _m21, _m22, _m23)

    override fun getRow4(storeIn: Vec4f): Vec4f = storeIn.set(_m30, _m31, _m32, _m33)

    override fun getColumn(column: Int, storeIn: Vec4f): Vec4f {
        assert(column in 0..3) { "Column index was not in range [0..3]!" }
        when (column) {
            0 -> return storeIn.set(_m00, _m10, _m20, _m30)
            1 -> return storeIn.set(_m01, _m11, _m21, _m31)
            2 -> return storeIn.set(_m02, _m12, _m22, _m32)
            3 -> return storeIn.set(_m03, _m13, _m23, _m33)
        }
        throw IllegalArgumentException("Column($column) index was not in range [0..3]!")
    }

    override fun getColumn1(storeIn: Vec4f): Vec4f = storeIn.set(_m00, _m10, _m20, _m30)

    override fun getColumn2(storeIn: Vec4f): Vec4f = storeIn.set(_m01, _m11, _m21, _m31)

    override fun getColumn3(storeIn: Vec4f): Vec4f = storeIn.set(_m02, _m12, _m22, _m32)

    override fun getColumn4(storeIn: Vec4f): Vec4f = storeIn.set(_m03, _m13, _m23, _m33)

    override fun getMajorDiagonal(storeIn: Vec4f): Vec4f = storeIn.set(_m00, _m11, _m22, _m33)

    override fun getRight(storeIn: Vec3f): Vec3f = storeIn.set(_m00, _m01, _m02)

    override fun getUp(storeIn: Vec3f): Vec3f = storeIn.set(_m10, _m11, _m12)

    override fun getForward(storeIn: Vec3f): Vec3f = storeIn.set(_m20, _m21, _m22)

    override fun asArray(): Array<out FloatArray> {
        return arrayOf(
            floatArrayOf(_m00, _m01, _m02, _m03),
            floatArrayOf(_m10, _m11, _m12, _m13),
            floatArrayOf(_m20, _m21, _m22, _m23),
            floatArrayOf(_m30, _m31, _m32, _m33)
        )
    }

    override fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray> {
        assert(storeIn.size == Mat4f.ROW_AMT && storeIn[0].size == Mat4f.COLUMN_AMT) {
            "Given data array is not of size 4x4!"
        }
        storeIn[0][0] = _m00
        storeIn[0][1] = _m01
        storeIn[0][2] = _m02
        storeIn[0][3] = _m03
        storeIn[1][0] = _m10
        storeIn[1][1] = _m11
        storeIn[1][2] = _m12
        storeIn[1][3] = _m13
        storeIn[2][0] = _m20
        storeIn[2][1] = _m21
        storeIn[2][2] = _m22
        storeIn[2][3] = _m23
        storeIn[3][0] = _m30
        storeIn[3][1] = _m31
        storeIn[3][2] = _m32
        storeIn[3][3] = _m33
        return storeIn
    }

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Mat4f.BYTES)).array()
    }

    // TODO: Optimize?
    override fun asQuaternion(storeIn: QuaternionF): QuaternionF =
        storeIn.initLookRotation(getForward(StdVec3f()), getUp(StdVec3f()))

    override fun determinant(): Float {
        var determinant = 0.0f
        // Elimination of columns 1 to 4.
        determinant += _m00 * StdMat3f.determinantOf(_m11, _m12, _m13, _m21, _m22, _m23, _m31, _m32, _m33)
        determinant -= _m01 * StdMat3f.determinantOf(_m10, _m12, _m13, _m20, _m22, _m23, _m30, _m32, _m33)
        determinant += _m02 * StdMat3f.determinantOf(_m10, _m11, _m13, _m20, _m21, _m23, _m30, _m31, _m33)
        determinant -= _m03 * StdMat3f.determinantOf(_m10, _m11, _m12, _m20, _m21, _m22, _m30, _m31, _m32)
        return determinant
    }

    override fun hasZeroComponent(): Boolean {
        return _m00 == 0.0f || _m01 == 0.0f || _m02 == 0.0f || _m03 == 0.0f ||
            _m10 == 0.0f || _m11 == 0.0f || _m12 == 0.0f || _m13 == 0.0f ||
            _m20 == 0.0f || _m21 == 0.0f || _m22 == 0.0f || _m23 == 0.0f ||
            _m30 == 0.0f || _m31 == 0.0f || _m32 == 0.0f || _m33 == 0.0f
    }

    override fun toFormattedString(): String = toString()

    override fun hashCode(): Int {
        var result = 17
        result = 31 * result + java.lang.Float.hashCode(_m00)
        result = 31 * result + java.lang.Float.hashCode(_m01)
        result = 31 * result + java.lang.Float.hashCode(_m02)
        result = 31 * result + java.lang.Float.hashCode(_m03)
        result = 31 * result + java.lang.Float.hashCode(_m10)
        result = 31 * result + java.lang.Float.hashCode(_m11)
        result = 31 * result + java.lang.Float.hashCode(_m12)
        result = 31 * result + java.lang.Float.hashCode(_m13)
        result = 31 * result + java.lang.Float.hashCode(_m20)
        result = 31 * result + java.lang.Float.hashCode(_m21)
        result = 31 * result + java.lang.Float.hashCode(_m22)
        result = 31 * result + java.lang.Float.hashCode(_m23)
        result = 31 * result + java.lang.Float.hashCode(_m30)
        result = 31 * result + java.lang.Float.hashCode(_m31)
        result = 31 * result + java.lang.Float.hashCode(_m32)
        result = 31 * result + java.lang.Float.hashCode(_m33)
        return result
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is StdMat4f -> false
            other === this -> true
            else -> _m00 == other.m00 && _m01 == other.m01 && _m02 == other.m02 && _m03 == other.m03 &&
                _m10 == other.m10 && _m11 == other.m11 && _m12 == other.m12 && _m13 == other.m13 &&
                _m20 == other.m20 && _m21 == other.m21 && _m22 == other.m22 && _m23 == other.m23 &&
                _m30 == other.m30 && _m31 == other.m31 && _m32 == other.m32 && _m33 == other.m33
        }
    }

    override fun toString(): String = buildString {
        append("Mat4f ( ")
        appendValue(this, _m00)
        append(" | ")
        appendValue(this, _m01)
        append(" | ")
        appendValue(this, _m02)
        append(" | ")
        appendValue(this, _m03)
        append(" )\n      ( ")
        appendValue(this, _m10)
        append(" | ")
        appendValue(this, _m11)
        append(" | ")
        appendValue(this, _m12)
        append(" | ")
        appendValue(this, _m13)
        append(" )\n      ( ")
        appendValue(this, _m20)
        append(" | ")
        appendValue(this, _m21)
        append(" | ")
        appendValue(this, _m22)
        append(" | ")
        appendValue(this, _m23)
        append(" )\n      ( ")
        appendValue(this, _m30)
        append(" | ")
        appendValue(this, _m31)
        append(" | ")
        appendValue(this, _m32)
        append(" | ")
        appendValue(this, _m33)
        append(" )")
    }

    private fun appendValue(builder: StringBuilder, value: Float) {
        // Add a whitespace if the current value is not negative.
        // Otherwise we will see a "-" sign.
        builder.append(if (value > 0) " " + valueAsString(value) else valueAsString(value))
    }

    private fun valueAsString(value: Float): String = buildString {
        append(value.toFourDecimalPlaces())
        append(" ".repeat(kotlin.math.max(0, 4 - toString().split('.')[1].length)))
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        buffer.putFloat(_m00)
        buffer.putFloat(_m01)
        buffer.putFloat(_m02)
        buffer.putFloat(_m03)
        buffer.putFloat(_m10)
        buffer.putFloat(_m11)
        buffer.putFloat(_m12)
        buffer.putFloat(_m13)
        buffer.putFloat(_m20)
        buffer.putFloat(_m21)
        buffer.putFloat(_m22)
        buffer.putFloat(_m23)
        buffer.putFloat(_m30)
        buffer.putFloat(_m31)
        buffer.putFloat(_m32)
        buffer.putFloat(_m33)
        return buffer
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        buffer.put(_m00)
        buffer.put(_m01)
        buffer.put(_m02)
        buffer.put(_m03)
        buffer.put(_m10)
        buffer.put(_m11)
        buffer.put(_m12)
        buffer.put(_m13)
        buffer.put(_m20)
        buffer.put(_m21)
        buffer.put(_m22)
        buffer.put(_m23)
        buffer.put(_m30)
        buffer.put(_m31)
        buffer.put(_m32)
        buffer.put(_m33)
        return buffer
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        buffer.put(_m00.toDouble())
        buffer.put(_m01.toDouble())
        buffer.put(_m02.toDouble())
        buffer.put(_m03.toDouble())
        buffer.put(_m10.toDouble())
        buffer.put(_m11.toDouble())
        buffer.put(_m12.toDouble())
        buffer.put(_m13.toDouble())
        buffer.put(_m20.toDouble())
        buffer.put(_m21.toDouble())
        buffer.put(_m22.toDouble())
        buffer.put(_m23.toDouble())
        buffer.put(_m30.toDouble())
        buffer.put(_m31.toDouble())
        buffer.put(_m32.toDouble())
        buffer.put(_m33.toDouble())
        return buffer
    }

    /**
     * Mat4f viewer, granting read-only access to its parent data.
     *
     * @author Lukas Potthast
     * 
     */
    inner class Viewer : Mat4fView {
        override val m00: Float = this@StdMat4f._m00
        override val m01: Float = this@StdMat4f._m01
        override val m02: Float = this@StdMat4f._m02
        override val m03: Float = this@StdMat4f._m03
        override val m10: Float = this@StdMat4f._m10
        override val m11: Float = this@StdMat4f._m11
        override val m12: Float = this@StdMat4f._m12
        override val m13: Float = this@StdMat4f._m13
        override val m20: Float = this@StdMat4f._m20
        override val m21: Float = this@StdMat4f._m21
        override val m22: Float = this@StdMat4f._m22
        override val m23: Float = this@StdMat4f._m23
        override val m30: Float = this@StdMat4f._m30
        override val m31: Float = this@StdMat4f._m31
        override val m32: Float = this@StdMat4f._m32
        override val m33: Float = this@StdMat4f._m33

        override fun get(row: Int, column: Int): Float = this@StdMat4f[row, column]

        override fun getRow(row: Int, storeIn: Vec4f): Vec4f = this@StdMat4f.getRow(row, storeIn)
        override fun getRow1(storeIn: Vec4f): Vec4f = this@StdMat4f.getRow1(storeIn)
        override fun getRow2(storeIn: Vec4f): Vec4f = this@StdMat4f.getRow2(storeIn)
        override fun getRow3(storeIn: Vec4f): Vec4f = this@StdMat4f.getRow3(storeIn)
        override fun getRow4(storeIn: Vec4f): Vec4f = this@StdMat4f.getRow4(storeIn)

        override fun getColumn(column: Int, storeIn: Vec4f): Vec4f = this@StdMat4f.getColumn(column, storeIn)
        override fun getColumn1(storeIn: Vec4f): Vec4f = this@StdMat4f.getColumn1(storeIn)
        override fun getColumn2(storeIn: Vec4f): Vec4f = this@StdMat4f.getColumn2(storeIn)
        override fun getColumn3(storeIn: Vec4f): Vec4f = this@StdMat4f.getColumn3(storeIn)
        override fun getColumn4(storeIn: Vec4f): Vec4f = this@StdMat4f.getColumn4(storeIn)

        override fun getMajorDiagonal(storeIn: Vec4f): Vec4f = this@StdMat4f.getMajorDiagonal(storeIn)

        override fun getRight(storeIn: Vec3f): Vec3f = this@StdMat4f.getRight(storeIn)
        override fun getUp(storeIn: Vec3f): Vec3f = this@StdMat4f.getUp(storeIn)
        override fun getForward(storeIn: Vec3f): Vec3f = this@StdMat4f.getForward(storeIn)

        override fun asArray(): Array<out FloatArray> = this@StdMat4f.asArray()
        override fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray> = this@StdMat4f.asArray(storeIn)

        override fun asQuaternion(storeIn: QuaternionF): QuaternionF = this@StdMat4f.asQuaternion(storeIn)

        override fun getSubmatrix(row: Int, column: Int, storeIn: Mat3f): Mat3f =
            this@StdMat4f.getSubmatrix(row, column, storeIn)

        override fun transform(vector: Vec4f): Vec4f = this@StdMat4f.transform(vector)

        override fun determinant(): Float = this@StdMat4f.determinant()
        override fun hasZeroComponent(): Boolean = this@StdMat4f.hasZeroComponent()

        override fun bytes(): ByteArray = this@StdMat4f.bytes()

        override fun toFormattedString(): String = this@StdMat4f.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdMat4f.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdMat4f.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdMat4f.storeIn(buffer)
    }

    companion object {
        private val LOG = LogUtil.getLogger(StdMat4f::class.java)
    }

}
