package de.colibriengine.math.quaternion.quaternionD

import de.colibriengine.math.insert
import de.colibriengine.math.insertInverted
import de.colibriengine.math.matrix.mat4d.Mat4d
import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.vector.vec3d.StdVec3d
import de.colibriengine.math.vector.vec3d.StdVec3dFactory
import de.colibriengine.math.vector.vec3d.Vec3d
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.*

/** Mutable quaternion in double precision. */
@Suppress("DuplicatedCode")
class StdQuaternionD(
    override var x: Double = 0.0,
    override var y: Double = 0.0,
    override var z: Double = 0.0,
    override var w: Double = 0.0
) : QuaternionD {

    private var _view: QuaternionDView? = null
    override val view: QuaternionDView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    override fun setX(x: Double): QuaternionD {
        this.x = x
        return this
    }

    override fun setY(y: Double): QuaternionD {
        this.y = y
        return this
    }

    override fun setZ(z: Double): QuaternionD {
        this.z = z
        return this
    }

    override fun setW(w: Double): QuaternionD {
        this.w = w
        return this
    }

    override operator fun set(x: Double, y: Double, z: Double, w: Double): QuaternionD {
        this.x = x
        this.y = y
        this.z = z
        this.w = w
        return this
    }

    override fun set(other: QuaternionFAccessor): QuaternionD {
        x = other.x.toDouble()
        y = other.y.toDouble()
        z = other.z.toDouble()
        w = other.w.toDouble()
        return this
    }

    override fun set(other: QuaternionDAccessor): QuaternionD {
        x = other.x
        y = other.y
        z = other.z
        w = other.w
        return this
    }

    override fun initIdentity(): QuaternionD {
        x = 0.0
        y = 0.0
        z = 0.0
        w = 1.0
        return this
    }

    override fun initRotation(axisX: Double, axisY: Double, axisZ: Double, angle: Double): QuaternionD {
        val radHalfAngle = Math.toRadians(angle) * 0.5f
        val sinHalfAngle = sin(radHalfAngle)
        val cosHalfAngle = cos(radHalfAngle)
        x = axisX * sinHalfAngle
        y = axisY * sinHalfAngle
        z = axisZ * sinHalfAngle
        w = cosHalfAngle
        return this
    }

    override fun initRotation(axis: Vec3dAccessor, angle: Double): QuaternionD =
        initRotation(axis.x, axis.y, axis.z, angle)

    override fun initXRotation(angle: Double): QuaternionD = initRotation(StdVec3dFactory.UNIT_X_AXIS, angle)

    override fun initYRotation(angle: Double): QuaternionD = initRotation(StdVec3dFactory.UNIT_Y_AXIS, angle)

    override fun initZRotation(angle: Double): QuaternionD = initRotation(StdVec3dFactory.UNIT_Z_AXIS, angle)

    override fun initRotation(angleAroundX: Double, angleAroundY: Double, angleAroundZ: Double): QuaternionD {
        val yRotation = StdQuaternionD().initYRotation(angleAroundY)
        val zRotation = StdQuaternionD().initZRotation(angleAroundZ)
        initXRotation(angleAroundX).times(yRotation).times(zRotation)
        return this
    }

    override fun initRotation(angles: Vec3dAccessor): QuaternionD = this.initRotation(angles.x, angles.y, angles.z)

    override fun initLookAt(
        sourcePosition: Vec3dAccessor,
        targetPosition: Vec3dAccessor,
        upDirection: Vec3dAccessor
    ): QuaternionD {
        // TODO: up is not used!
        val worldForward = StdVec3d().set(StdVec3dFactory.UNIT_Z_AXIS)
        val sourceToTarget = StdVec3d().set(targetPosition).minus(sourcePosition).normalize()

        // The axis around which the rotation should be made.
        val rotAxis = StdVec3d().set(worldForward).cross(sourceToTarget)

        // The angle to rotate.
        val angle = worldForward.dot(sourceToTarget)

        /* if (Math.abs(dot + 1.0f) < 0.000001.0f) {
            initRotation(ImmutableVec3d.UNIT_Y_AXIS, (double) Math.toDegrees(FEMath.PI_F));
            return this;
        } else if (Math.abs(dot - 1.0f) < 0.000001.0f) {
            initIdentity();
            return this;
        } */

        x = rotAxis.x
        y = rotAxis.y
        z = rotAxis.z
        w = angle + 1
        normalize()

        return this
    }

    override fun initLookRotation(
        forward: Vec3dAccessor,
        up: Vec3dAccessor
    ): QuaternionD {
        val finalForward = StdVec3d().set(forward).normalize().invert()
        val finalRight = StdVec3d().set(up).cross(finalForward).normalize()
        val finalUp = StdVec3d().set(forward).cross(finalRight)
        /*
        // This would be a correct solution for the task.
        return StdMat3d().set(
            finalRight.x, finalUp.x, finalForward.x,
            finalRight.y, finalUp.y, finalForward.y,
            finalRight.z, finalUp.z, finalForward.z
        ).asQuaternion(this)
        */
        // Following code is optimized, so that we do not need to create a temporary matrix instance.
        val m00 = finalRight.x
        val m01 = finalUp.x
        val m02 = finalForward.x
        val m10 = finalRight.y
        val m11 = finalUp.y
        val m12 = finalForward.y
        val m20 = finalRight.z
        val m21 = finalUp.z
        val m22 = finalForward.z
        val t: Double
        if (m22 < 0.0) {
            if (m00 > m11) {
                t = 1.0 + m00 - m11 - m22
                set(t, m01 + m10, m20 + m02, m12 - m21)
            } else {
                t = 1.0 - m00 + m11 - m22
                set(m01 + m10, t, m12 + m21, m20 - m02)
            }
        } else {
            if (m00 < -m11) {
                t = 1.0 - m00 - m11 + m22
                set(m20 + m02, m12 + m21, t, m01 - m10)
            } else {
                t = 1.0 + m00 + m11 + m22
                set(m12 - m21, m20 - m02, m01 - m10, t)
            }
        }
        return times(0.5 / sqrt(t))
    }

    override fun normalize(): QuaternionD {
        val length = length()
        check(length != 0.0) { "Length must not be 0!" }
        x /= length
        y /= length
        z /= length
        w /= length
        return this
    }

    override fun invert(): QuaternionD {
        val lengthSquared = lengthSquared()
        check(lengthSquared != 0.0) { "The squared length must not be 0!" }
        this.x = -x / lengthSquared
        this.y = -y / lengthSquared
        this.z = -z / lengthSquared
        this.w = +w / lengthSquared
        return this
    }

    override fun conjugate(): QuaternionD {
        x = -x
        y = -y
        z = -z
        return this
    }

    override fun times(scalar: Double): QuaternionD {
        x *= scalar
        y *= scalar
        z *= scalar
        w *= scalar
        return this
    }

    override fun times(vector: Vec3dAccessor): QuaternionD {
        x = w * vector.x + y * vector.z - z * vector.y
        y = w * vector.y + z * vector.x - x * vector.z
        z = w * vector.z + x * vector.y - y * vector.x
        w = -x * vector.x - y * vector.y - z * vector.z
        return this
    }

    override fun times(other: QuaternionDAccessor): QuaternionD {
        val tempX = w * other.x - z * other.y + y * other.z + x * other.w
        val tempY = w * other.y + z * other.x - x * other.z + y * other.w
        val tempZ = w * other.z - y * other.x + x * other.y + z * other.w
        val tempW = w * other.w - x * other.x - y * other.y - z * other.z
        x = tempX
        y = tempY
        z = tempZ
        w = tempW
        return this
    }

    override fun div(other: QuaternionDAccessor): QuaternionD = invert().times(other)

    override fun lerp(target: QuaternionDAccessor, lerpFactor: Double): QuaternionD {
        val tInv: Double = 1.0 - lerpFactor
        x = tInv * x + lerpFactor * target.x
        y = tInv * y + lerpFactor * target.y
        z = tInv * z + lerpFactor * target.z
        w = tInv * w + lerpFactor * target.w
        return normalize()
    }

    override fun slerp(target: QuaternionDAccessor, lerpFactor: Double): QuaternionD {
        val tInv: Double = 1.0 - lerpFactor
        val intermediate = x * target.x + y * target.y + z * target.z + w * target.w
        // min() with 0.999f to avoid values >1, for which acos would return NaN and 1, fir which sin would return 0
        // which would lead to a divide by zero. -> NaN
        val theta: Double = acos(min(intermediate, 0.999))
        val sn: Double = sin(theta)
        val wa: Double = sin(tInv * theta) / sn
        val wb: Double = sin(lerpFactor * theta) / sn
        x = wa * x + wb * target.x
        y = wa * y + wb * target.y
        z = wa * z + wb * target.z
        w = wa * w + wb * target.w
        return normalize()
    }

    override fun xyz(storeIn: Vec3d): Vec3d = storeIn.set(x, y, z)

    override fun vectorPart(storeIn: Vec3d): Vec3d = xyz(storeIn)

    override fun scalarPart(): Double = w

    override fun isIdentity(): Boolean = x == 0.0 && y == 0.0 && z == 0.0 && w == 1.0

    override fun lengthSquared(): Double = x * x + y * y + z * z + w * w

    override fun length(): Double = sqrt(lengthSquared())

    private fun fwdX(): Double = 2.0 * (x * z - y * w)
    private fun fwdY(): Double = 2.0 * (y * z + x * w)
    private fun fwdZ(): Double = 1.0 - 2.0 * (x * x + y * y)
    private fun upX(): Double = 2.0 * (x * y + z * w)
    private fun upY(): Double = 1.0 - 2.0 * (x * x + z * z)
    private fun upZ(): Double = 2.0 * (y * z - x * w)
    private fun rightX(): Double = 1.0 - 2.0 * (y * y + z * z)
    private fun rightY(): Double = 2.0 * (x * y - z * w)
    private fun rightZ(): Double = 2.0 * (x * z + y * w)

    override fun forward(storeIn: Vec3d): Vec3d = storeIn.set(fwdX(), fwdY(), fwdZ())
    override fun forward(storeIn: DoubleArray, index: Int) = storeIn.insert(index, fwdX(), fwdY(), fwdZ())

    override fun backward(storeIn: Vec3d): Vec3d = forward(storeIn).invert()
    override fun backward(storeIn: DoubleArray, index: Int) = storeIn.insertInverted(index, fwdX(), fwdY(), fwdZ())

    override fun up(storeIn: Vec3d): Vec3d = storeIn.set(upX(), upY(), upZ())
    override fun up(storeIn: DoubleArray, index: Int) = storeIn.insert(index, upX(), upY(), upZ())

    override fun down(storeIn: Vec3d): Vec3d = up(storeIn).invert()
    override fun down(storeIn: DoubleArray, index: Int) = storeIn.insertInverted(index, upX(), upY(), upZ())

    override fun right(storeIn: Vec3d): Vec3d = storeIn.set(rightX(), rightY(), rightZ())
    override fun right(storeIn: DoubleArray, index: Int) = storeIn.insert(index, rightX(), rightY(), rightZ())

    override fun left(storeIn: Vec3d): Vec3d = right(storeIn).invert()
    override fun left(storeIn: DoubleArray, index: Int) = storeIn.insertInverted(index, rightX(), rightY(), rightZ())

    override fun asMatrix(storeIn: Mat4d): Mat4d = calcMatrix(storeIn)

    /**
     * TODO: Check if this is correct! Constructs a matrix instance from the current state of this quaternion.
     *
     * @return A new matrix instance holding the conversion of this quaternion to its matrix form.
     */
    private fun calcMatrix(store: Mat4d): Mat4d {
        return store.set(
            w, -z, y, x,
            z, w, -x, y,
            -y, x, w, z,
            -x, -y, -z, w
        )
    }

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(QuaternionD.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("QuaternionD [ ")
            .appendAligned(x).append(", ")
            .appendAligned(y).append(", ")
            .appendAligned(z).append(", ")
            .appendAligned(w)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.putDouble(x).putDouble(y).putDouble(z).putDouble(w)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x.toFloat()).put(y.toFloat()).put(z.toFloat()).put(w.toFloat())
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x).put(y).put(z).put(w)
    }

    override fun hashCode(): Int {
        var hash = 17
        hash = 31 * hash + x.hashCode()
        hash = 31 * hash + y.hashCode()
        hash = 31 * hash + z.hashCode()
        hash = 31 * hash + w.hashCode()
        return hash
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is QuaternionD -> false
            other === this -> true
            else -> this.x == other.x
                && this.y == other.y
                && this.z == other.z
                && this.w == other.w
        }
    }

    override fun toString(): String = "QuaternionD [ $x, $y, $z, $w ]"

    /**
     * QuaternionD viewer, granting read-only access to its parent data.
     *
     * @author Lukas Potthast
     * @since ColibriEngine 0.0.7.2
     */
    inner class Viewer : QuaternionDView {
        override val x: Double = this@StdQuaternionD.x
        override val y: Double = this@StdQuaternionD.y
        override val z: Double = this@StdQuaternionD.y
        override val w: Double = this@StdQuaternionD.y

        override fun xyz(storeIn: Vec3d): Vec3d = this@StdQuaternionD.xyz(storeIn)
        override fun vectorPart(storeIn: Vec3d): Vec3d = this@StdQuaternionD.vectorPart(storeIn)
        override fun scalarPart(): Double = w

        override fun isIdentity(): Boolean = this@StdQuaternionD.isIdentity()

        override fun lengthSquared(): Double = this@StdQuaternionD.lengthSquared()
        override fun length(): Double = this@StdQuaternionD.length()

        override fun forward(storeIn: Vec3d): Vec3d = this@StdQuaternionD.forward(storeIn)
        override fun forward(storeIn: DoubleArray, index: Int) = this@StdQuaternionD.forward(storeIn, index)
        override fun backward(storeIn: Vec3d): Vec3d = this@StdQuaternionD.backward(storeIn)
        override fun backward(storeIn: DoubleArray, index: Int) = this@StdQuaternionD.backward(storeIn, index)
        override fun up(storeIn: Vec3d): Vec3d = this@StdQuaternionD.up(storeIn)
        override fun up(storeIn: DoubleArray, index: Int) = this@StdQuaternionD.up(storeIn, index)
        override fun down(storeIn: Vec3d): Vec3d = this@StdQuaternionD.down(storeIn)
        override fun down(storeIn: DoubleArray, index: Int) = this@StdQuaternionD.down(storeIn, index)
        override fun right(storeIn: Vec3d): Vec3d = this@StdQuaternionD.right(storeIn)
        override fun right(storeIn: DoubleArray, index: Int) = this@StdQuaternionD.right(storeIn, index)
        override fun left(storeIn: Vec3d): Vec3d = this@StdQuaternionD.left(storeIn)
        override fun left(storeIn: DoubleArray, index: Int) = this@StdQuaternionD.left(storeIn, index)

        override fun asMatrix(storeIn: Mat4d): Mat4d = this@StdQuaternionD.asMatrix(storeIn)
        override fun bytes(): ByteArray = this@StdQuaternionD.bytes()

        override fun toFormattedString(): String = this@StdQuaternionD.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdQuaternionD.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdQuaternionD.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdQuaternionD.storeIn(buffer)
    }

}
