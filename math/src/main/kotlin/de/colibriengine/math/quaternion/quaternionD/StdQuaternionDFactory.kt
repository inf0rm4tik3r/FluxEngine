package de.colibriengine.math.quaternion.quaternionD

/** StdQuaternionDFactory. */
class StdQuaternionDFactory : QuaternionDFactory {

    private val pool: StdQuaternionDPool = StdQuaternionDPool()

    override fun acquire(): QuaternionD = pool.acquire()

    override fun acquireDirty(): QuaternionD = pool.acquireDirty()

    override fun free(quaternionD: QuaternionD) = pool.free(quaternionD)

    override fun immutableQuaternionDBuilder(): ImmutableQuaternionD.Builder = StdImmutableQuaternionD.builder()

    override fun identity(): ImmutableQuaternionD = IDENTITY

    companion object {
        val IDENTITY = StdImmutableQuaternionD.builder().of(0.0, 0.0, 0.0, 1.0)
    }

}
