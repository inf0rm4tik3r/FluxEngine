package de.colibriengine.math.quaternion.quaternionD

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

/** QuaternionDPool. */
class StdQuaternionDPool : ObjectPool<QuaternionD>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdQuaternionD()
    },
    { instance: QuaternionD -> instance.initIdentity() }
) {

    companion object {
        private val LOG = getLogger(StdQuaternionDPool::class.java)
    }

}
