package de.colibriengine.math.quaternion.quaternionF

import de.colibriengine.math.insert
import de.colibriengine.math.insertInverted
import de.colibriengine.math.matrix.mat4f.Mat4f
import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.StdVec3fFactory
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.UNIT_Z_AXIS
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.*

/** Mutable quaternion in float precision. */
@Suppress("DuplicatedCode")
class StdQuaternionF(
    override var x: Float = 0.0f,
    override var y: Float = 0.0f,
    override var z: Float = 0.0f,
    override var w: Float = 0.0f
) : QuaternionF {

    private var _view: QuaternionFView? = null
    override val view: QuaternionFView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    override fun setX(x: Float): QuaternionF {
        this.x = x
        return this
    }

    override fun setY(y: Float): QuaternionF {
        this.y = y
        return this
    }

    override fun setZ(z: Float): QuaternionF {
        this.z = z
        return this
    }

    override fun setW(w: Float): QuaternionF {
        this.w = w
        return this
    }

    override fun set(x: Float, y: Float, z: Float, w: Float): QuaternionF {
        this.x = x
        this.y = y
        this.z = z
        this.w = w
        return this
    }

    override fun set(other: QuaternionFAccessor): QuaternionF {
        x = other.x
        y = other.y
        z = other.z
        w = other.w
        return this
    }

    override fun set(other: QuaternionDAccessor): QuaternionF {
        x = other.x.toFloat()
        y = other.y.toFloat()
        z = other.z.toFloat()
        w = other.w.toFloat()
        return this
    }

    override fun initIdentity(): QuaternionF {
        x = 0.0f
        y = 0.0f
        z = 0.0f
        w = 1.0f
        return this
    }

    override fun initRotation(axisX: Float, axisY: Float, axisZ: Float, angle: Float): QuaternionF {
        val radHalfAngle = (Math.toRadians(angle.toDouble()) * 0.5f).toFloat()
        val sinHalfAngle = sin(radHalfAngle.toDouble()).toFloat()
        val cosHalfAngle = cos(radHalfAngle.toDouble()).toFloat()
        x = axisX * sinHalfAngle
        y = axisY * sinHalfAngle
        z = axisZ * sinHalfAngle
        w = cosHalfAngle
        return this
    }

    override fun initRotation(axis: Vec3fAccessor, angle: Float): QuaternionF =
        initRotation(axis.x, axis.y, axis.z, angle)

    override fun initXRotation(angle: Float): QuaternionF = initRotation(StdVec3fFactory.UNIT_X_AXIS, angle)

    override fun initYRotation(angle: Float): QuaternionF = initRotation(StdVec3fFactory.UNIT_Y_AXIS, angle)

    override fun initZRotation(angle: Float): QuaternionF = initRotation(UNIT_Z_AXIS, angle)

    override fun initRotation(angleAroundX: Float, angleAroundY: Float, angleAroundZ: Float): QuaternionF {
        val yRotation = StdQuaternionF().initYRotation(angleAroundY)
        val zRotation = StdQuaternionF().initZRotation(angleAroundZ)
        initXRotation(angleAroundX).times(yRotation).times(zRotation)
        return this
    }

    override fun initRotation(angles: Vec3fAccessor): QuaternionF = initRotation(angles.x, angles.y, angles.z)

    override fun initLookAt(
        sourcePosition: Vec3fAccessor,
        targetPosition: Vec3fAccessor,
        upDirection: Vec3fAccessor
    ): QuaternionF {
        return initLookRotation(StdVec3f().set(sourcePosition).minus(targetPosition), upDirection)
    }

    override fun initLookRotation(forward: Vec3fAccessor, up: Vec3fAccessor): QuaternionF {
        val finalForward = StdVec3f().set(forward).normalize().invert()
        val finalRight = StdVec3f().set(up).cross(finalForward).normalize()
        val finalUp = StdVec3f().set(finalForward).cross(finalRight)
        /*
        // This would be a correct solution for the task.
        return StdMat3f().set(
            finalRight.x, finalUp.x, finalForward.x,
            finalRight.y, finalUp.y, finalForward.y,
            finalRight.z, finalUp.z, finalForward.z
        ).asQuaternion(this)
        */
        // Following code is optimized, so that we do not need to create a temporary matrix instance.
        val m00 = finalRight.x
        val m01 = finalUp.x
        val m02 = finalForward.x
        val m10 = finalRight.y
        val m11 = finalUp.y
        val m12 = finalForward.y
        val m20 = finalRight.z
        val m21 = finalUp.z
        val m22 = finalForward.z
        val t: Float
        if (m22 < 0.0f) {
            if (m00 > m11) {
                t = 1.0f + m00 - m11 - m22
                set(t, m01 + m10, m20 + m02, m12 - m21)
            } else {
                t = 1.0f - m00 + m11 - m22
                set(m01 + m10, t, m12 + m21, m20 - m02)
            }
        } else {
            if (m00 < -m11) {
                t = 1.0f - m00 - m11 + m22
                set(m20 + m02, m12 + m21, t, m01 - m10)
            } else {
                t = 1.0f + m00 + m11 + m22
                set(m12 - m21, m20 - m02, m01 - m10, t)
            }
        }
        return times(0.5f / sqrt(t))
    }

    override fun normalize(): QuaternionF {
        val length = length()
        check(length != 0f) { "Length must not be 0!" }
        x /= length
        y /= length
        z /= length
        w /= length
        return this
    }

    override fun invert(): QuaternionF {
        val lengthSquared = lengthSquared()
        check(lengthSquared != 0f) { "The squared length must not be 0!" }
        this.x = -x / lengthSquared
        this.y = -y / lengthSquared
        this.z = -z / lengthSquared
        this.w = +w / lengthSquared
        return this
    }

    override fun conjugate(): QuaternionF {
        x = -x
        y = -y
        z = -z
        return this
    }

    override fun times(scalar: Float): QuaternionF {
        x *= scalar
        y *= scalar
        z *= scalar
        w *= scalar
        return this
    }

    override fun times(vector: Vec3fAccessor): QuaternionF {
        x = w * vector.x + y * vector.z - z * vector.y
        y = w * vector.y + z * vector.x - x * vector.z
        z = w * vector.z + x * vector.y - y * vector.x
        w = -x * vector.x - y * vector.y - z * vector.z
        return this
    }

    override fun times(other: QuaternionFAccessor): QuaternionF {
        val tempX = w * other.x - z * other.y + y * other.z + x * other.w
        val tempY = w * other.y + z * other.x - x * other.z + y * other.w
        val tempZ = w * other.z - y * other.x + x * other.y + z * other.w
        val tempW = w * other.w - x * other.x - y * other.y - z * other.z
        x = tempX
        y = tempY
        z = tempZ
        w = tempW
        return this
    }

    override fun div(other: QuaternionFAccessor): QuaternionF = invert().times(other)

    override fun lerp(target: QuaternionFAccessor, lerpFactor: Float): QuaternionF {
        val tInv: Float = 1.0f - lerpFactor
        x = tInv * x + lerpFactor * target.x
        y = tInv * y + lerpFactor * target.y
        z = tInv * z + lerpFactor * target.z
        w = tInv * w + lerpFactor * target.w
        return normalize()
    }

    override fun slerp(target: QuaternionFAccessor, lerpFactor: Float): QuaternionF {
        val tInv: Float = 1.0f - lerpFactor
        val intermediate = x * target.x + y * target.y + z * target.z + w * target.w
        // min() with 0.999f to avoid values >1, for which acos would return NaN and 1, fir which sin would return 0
        // which would lead to a divide by zero. -> NaN
        val theta: Float = acos(min(intermediate, 0.999f))
        val sn: Float = sin(theta)
        val wa: Float = sin(tInv * theta) / sn
        val wb: Float = sin(lerpFactor * theta) / sn
        x = wa * x + wb * target.x
        y = wa * y + wb * target.y
        z = wa * z + wb * target.z
        w = wa * w + wb * target.w
        return normalize()
    }

    override fun xyz(storeIn: Vec3f): Vec3f = storeIn.set(x, y, z)

    override fun vectorPart(storeIn: Vec3f): Vec3f = xyz(storeIn)

    override fun scalarPart(): Float = w

    override fun isIdentity(): Boolean = x == 0.0f && y == 0.0f && z == 0.0f && w == 1.0f

    override fun lengthSquared(): Float = x * x + y * y + z * z + w * w

    override fun length(): Float = sqrt(lengthSquared().toDouble()).toFloat()

    private fun fwdX(): Float = 2.0f * (x * z - y * w)
    private fun fwdY(): Float = 2.0f * (y * z + x * w)
    private fun fwdZ(): Float = 1.0f - 2.0f * (x * x + y * y)
    private fun upX(): Float = 2.0f * (x * y + z * w)
    private fun upY(): Float = 1.0f - 2.0f * (x * x + z * z)
    private fun upZ(): Float = 2.0f * (y * z - x * w)
    private fun rightX(): Float = 1.0f - 2.0f * (y * y + z * z)
    private fun rightY(): Float = 2.0f * (x * y - z * w)
    private fun rightZ(): Float = 2.0f * (x * z + y * w)

    override fun forward(storeIn: Vec3f): Vec3f = storeIn.set(fwdX(), fwdY(), fwdZ())
    override fun forward(storeIn: FloatArray, index: Int) = storeIn.insert(index, fwdX(), fwdY(), fwdZ())

    override fun backward(storeIn: Vec3f): Vec3f = forward(storeIn).invert()
    override fun backward(storeIn: FloatArray, index: Int) = storeIn.insertInverted(index, fwdX(), fwdY(), fwdZ())

    override fun up(storeIn: Vec3f): Vec3f = storeIn.set(upX(), upY(), upZ())
    override fun up(storeIn: FloatArray, index: Int) = storeIn.insert(index, upX(), upY(), upZ())

    override fun down(storeIn: Vec3f): Vec3f = up(storeIn).invert()
    override fun down(storeIn: FloatArray, index: Int) = storeIn.insertInverted(index, upX(), upY(), upZ())

    override fun right(storeIn: Vec3f): Vec3f = storeIn.set(rightX(), rightY(), rightZ())
    override fun right(storeIn: FloatArray, index: Int) = storeIn.insert(index, rightX(), rightY(), rightZ())

    override fun left(storeIn: Vec3f): Vec3f = right(storeIn).invert()
    override fun left(storeIn: FloatArray, index: Int) = storeIn.insertInverted(index, rightX(), rightY(), rightZ())

    override fun asMatrix(storeIn: Mat4f): Mat4f {
        // TODO: Check if this is correct.
        return storeIn.set(
            w, -z, y, x,
            z, w, -x, y,
            -y, x, w, z,
            -x, -y, -z, w
        )
    }

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(QuaternionF.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("QuaternionF [ ")
            .appendAligned(x).append(", ")
            .appendAligned(y).append(", ")
            .appendAligned(z).append(", ")
            .appendAligned(w)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.putFloat(x).putFloat(y).putFloat(z).putFloat(w)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x).put(y).put(z).put(w)
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x.toDouble()).put(y.toDouble()).put(z.toDouble()).put(w.toDouble())
    }

    override fun hashCode(): Int {
        var hash = 17
        hash = 31 * hash + x.hashCode()
        hash = 31 * hash + y.hashCode()
        hash = 31 * hash + z.hashCode()
        hash = 31 * hash + w.hashCode()
        return hash
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is QuaternionF -> false
            other === this -> true
            else -> this.x == other.x
                && this.y == other.y
                && this.z == other.z
                && this.w == other.w
        }
    }

    override fun toString(): String = "QuaternionF [ $x, $y, $z, $w ]"

    /**
     * QuaternionF viewer, granting read-only access to its parent data.
     *
     * @author Lukas Potthast
     * @since ColibriEngine 0.0.7.2
     */
    inner class Viewer : QuaternionFView {
        override val x: Float = this@StdQuaternionF.x
        override val y: Float = this@StdQuaternionF.y
        override val z: Float = this@StdQuaternionF.y
        override val w: Float = this@StdQuaternionF.y

        override fun xyz(storeIn: Vec3f): Vec3f = this@StdQuaternionF.xyz(storeIn)
        override fun vectorPart(storeIn: Vec3f): Vec3f = this@StdQuaternionF.vectorPart(storeIn)
        override fun scalarPart(): Float = w

        override fun isIdentity(): Boolean = this@StdQuaternionF.isIdentity()

        override fun lengthSquared(): Float = this@StdQuaternionF.lengthSquared()
        override fun length(): Float = this@StdQuaternionF.length()

        override fun forward(storeIn: Vec3f): Vec3f = this@StdQuaternionF.forward(storeIn)
        override fun forward(storeIn: FloatArray, index: Int) = this@StdQuaternionF.forward(storeIn, index)
        override fun backward(storeIn: Vec3f): Vec3f = this@StdQuaternionF.backward(storeIn)
        override fun backward(storeIn: FloatArray, index: Int) = this@StdQuaternionF.backward(storeIn, index)
        override fun up(storeIn: Vec3f): Vec3f = this@StdQuaternionF.up(storeIn)
        override fun up(storeIn: FloatArray, index: Int) = this@StdQuaternionF.up(storeIn, index)
        override fun down(storeIn: Vec3f): Vec3f = this@StdQuaternionF.down(storeIn)
        override fun down(storeIn: FloatArray, index: Int) = this@StdQuaternionF.down(storeIn, index)
        override fun right(storeIn: Vec3f): Vec3f = this@StdQuaternionF.right(storeIn)
        override fun right(storeIn: FloatArray, index: Int) = this@StdQuaternionF.right(storeIn, index)
        override fun left(storeIn: Vec3f): Vec3f = this@StdQuaternionF.left(storeIn)
        override fun left(storeIn: FloatArray, index: Int) = this@StdQuaternionF.left(storeIn, index)

        override fun asMatrix(storeIn: Mat4f): Mat4f = this@StdQuaternionF.asMatrix(storeIn)
        override fun bytes(): ByteArray = this@StdQuaternionF.bytes()

        override fun toFormattedString(): String = this@StdQuaternionF.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdQuaternionF.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdQuaternionF.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdQuaternionF.storeIn(buffer)
    }

}
