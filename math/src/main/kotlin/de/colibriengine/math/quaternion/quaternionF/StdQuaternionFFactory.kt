package de.colibriengine.math.quaternion.quaternionF

/** StdQuaternionFFactory. */
class StdQuaternionFFactory : QuaternionFFactory {

    private val pool: StdQuaternionFPool = StdQuaternionFPool()

    override fun acquire(): QuaternionF = pool.acquire()

    override fun acquireDirty(): QuaternionF = pool.acquireDirty()

    override fun free(quaternionF: QuaternionF) = pool.free(quaternionF)

    override fun immutableQuaternionFBuilder(): ImmutableQuaternionF.Builder = StdImmutableQuaternionF.builder()

    override fun identity(): ImmutableQuaternionF = IDENTITY

    companion object {
        val IDENTITY = StdImmutableQuaternionF.builder().of(0.0f, 0.0f, 0.0f, 1.0f)
    }

}
