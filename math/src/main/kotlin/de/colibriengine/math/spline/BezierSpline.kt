package de.colibriengine.math.spline

import de.colibriengine.math.curve.BezierCurve
import de.colibriengine.math.vector.vec3f.Vec3f
import kotlin.math.floor

/**
 * A bezier spline, constructed from multiple bezier curve segments.
 * Every segment can be a bezier curve of any order!
 * Mixed orders are possible. Use connected curve: Starting point of the second curve needs to be the end point
 * of the first curve, and so on... Nevertheless it is technically possible to create disconnected curves.
 *
 * @param segments
 *     Amount of bezier curves which form the spline. Preferably connected. Smoothness of the spline is
 *     not guaranteed. A shared point needs to be in line with its previous and following control point
 *     to create a smooth transition between curves.
 *
 * @since ColibriEngine 0.0.6.0
 */
class BezierSpline(
    /**
     * The curve segments that define the spline.
     */
    private vararg val segments: BezierCurve
) {

    //TODO check visually
    fun evaluate() {
        /* TODO: Check
        if (segments.length != 0) {
            
            // Declare a reference to a vector array. Will be used to reference the control points of a specific
            // spline segment.
            @NotNull Vec3f[] currentSegCpRef;
            @NotNull Vec3f   last, prev, velocity;
            
            // Calculate the velocity of each (segments-1) last control point...
            for (int i = 0; i < segments.length - 1; i++) {
                if ((currentSegCpRef = segments[i].getControlPoints()).length > 0) {
                    // Get the last element of the current segment as well as its predecessor.
                    last = currentSegCpRef[currentSegCpRef.length - 1];
                    prev = currentSegCpRef[currentSegCpRef.length - 2];
                    
                    // Calculate the velocity.
                    velocity = last.subN(prev);
                    
                    // and align the starting point of the next segment.
                    segments[i + 1].getControlPoints()[0] = last.addN(velocity);
                }
            }
        }
        */
    }

    /**
     * Interpolates over this bezier spline.
     *
     * @param interpolation
     * How far on the spline? Only specify values in [0,...,1] range.
     * 0 represents the starting point of the first curve,
     * 1 the end point of the last curve.
     *
     * @return A point on the spline. Progress on the line is described by the interpolation factor.
     *
     * @throws IllegalArgumentException
     * if the given amount of interpolation is not in [0,...,1] range.
     */
    fun pointOnSpline(interpolation: Float): Vec3f {
        // Check whether the specified interpolation is acceptable.
        require(!(interpolation < 0 || interpolation > 1)) {
            "BezierSpline: Parameter has to be in [0,...,1] range!"
        }
        val f = interpolation * segments.size
        val segment = floor(f.toDouble()).toInt()
        val interpolationInSegment = f - segment
        return segments[segment].pointOnCurve(interpolationInSegment)
    }

}
