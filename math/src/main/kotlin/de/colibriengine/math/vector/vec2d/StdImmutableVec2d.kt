package de.colibriengine.math.vector.vec2d

import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec2d private constructor(
    x: Double,
    y: Double
) : ImmutableVec2d {

    private val mutableAccessor: Vec2dAccessor = StdVec2d(x, y)

    override val x: Double = mutableAccessor.x
    override val y: Double = mutableAccessor.y

    override fun asMutable(): Vec2d = StdVec2d().set(this)

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Double = mutableAccessor.min()
    override fun max(): Double = mutableAccessor.max()

    override infix fun dot(other: Vec2dAccessor): Double = mutableAccessor.dot(other)

    override fun lerp(target: Vec2dAccessor, lerpFactor: Double): Vec2d = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec2dAccessor, lerpFactor: Double): Vec2d =
        mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec2d", "ImmutableVec2d")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec2d.Builder {

        private var x: Double = 0.toDouble()
        private var y: Double = 0.toDouble()

        override fun of(value: Double): ImmutableVec2d = set(value).build()

        override fun of(x: Double, y: Double): ImmutableVec2d = set(x, y).build()

        override fun of(other: Vec2dAccessor): ImmutableVec2d = set(other).build()

        override fun setX(x: Double): ImmutableVec2d.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Double): ImmutableVec2d.Builder {
            this.y = y
            return this
        }

        override fun set(value: Double): ImmutableVec2d.Builder {
            x = value
            y = value
            return this
        }

        override fun set(x: Double, y: Double): ImmutableVec2d.Builder {
            this.x = x
            this.y = y
            return this
        }

        override fun set(other: Vec2dAccessor): ImmutableVec2d.Builder {
            x = other.x
            y = other.y
            return this
        }

        override fun build(): ImmutableVec2d = StdImmutableVec2d(x, y)
    }

    companion object {
        fun builder(): ImmutableVec2d.Builder = Builder()
    }

}

inline fun buildImmutableVec2d(builderAction: ImmutableVec2d.Builder.() -> Unit): ImmutableVec2d {
    return StdImmutableVec2d.builder().apply(builderAction).build()
}
