package de.colibriengine.math.vector.vec2d

/** StdVec2dFactory. */
class StdVec2dFactory : Vec2dFactory {

    private val pool = StdVec2dPool()

    override fun acquire(): Vec2d = pool.acquire()

    override fun acquireDirty(): Vec2d = pool.acquireDirty()

    override fun free(vec2d: Vec2d) = pool.free(vec2d)

    override fun immutableVec2dBuilder(): ImmutableVec2d.Builder = StdImmutableVec2d.builder()

    override fun zero(): ImmutableVec2d = ZERO

    override fun one(): ImmutableVec2d = ONE

    companion object {
        val ZERO = StdImmutableVec2d.builder().of(0.0)
        val ONE = StdImmutableVec2d.builder().of(1.0)
    }

}
