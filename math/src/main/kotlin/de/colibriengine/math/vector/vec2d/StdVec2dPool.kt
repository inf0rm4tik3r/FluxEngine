package de.colibriengine.math.vector.vec2d

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec2dPool : ObjectPool<Vec2d>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec2d()
    },
    { instance: Vec2d -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec2dPool::class.java)
    }

}
