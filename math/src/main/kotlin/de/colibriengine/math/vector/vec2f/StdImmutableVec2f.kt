package de.colibriengine.math.vector.vec2f

import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec2f private constructor(
    x: Float,
    y: Float
) : ImmutableVec2f {

    private val mutableAccessor: Vec2fAccessor = StdVec2f(x, y)

    override val x: Float = mutableAccessor.x
    override val y: Float = mutableAccessor.y

    override fun asMutable(): Vec2f = StdVec2f().set(this)

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Float = mutableAccessor.min()
    override fun max(): Float = mutableAccessor.max()

    override infix fun dot(other: Vec2fAccessor): Double = mutableAccessor.dot(other)

    override fun lerp(target: Vec2fAccessor, lerpFactor: Float): Vec2f = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec2fAccessor, lerpFactor: Float): Vec2f =
        mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec2f", "ImmutableVec2f")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec2f.Builder {

        private var x: Float = 0.toFloat()
        private var y: Float = 0.toFloat()

        override fun of(value: Float): ImmutableVec2f = set(value).build()

        override fun of(x: Float, y: Float): ImmutableVec2f = set(x, y).build()

        override fun of(other: Vec2fAccessor): ImmutableVec2f = set(other).build()

        override fun setX(x: Float): ImmutableVec2f.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Float): ImmutableVec2f.Builder {
            this.y = y
            return this
        }

        override fun set(value: Float): ImmutableVec2f.Builder {
            x = value
            y = value
            return this
        }

        override fun set(x: Float, y: Float): ImmutableVec2f.Builder {
            this.x = x
            this.y = y
            return this
        }

        override fun set(other: Vec2fAccessor): ImmutableVec2f.Builder {
            x = other.x
            y = other.y
            return this
        }

        override fun build(): ImmutableVec2f = StdImmutableVec2f(x, y)
    }

    companion object {
        fun builder(): ImmutableVec2f.Builder = Builder()
    }

}

inline fun buildImmutableVec2f(builderAction: ImmutableVec2f.Builder.() -> Unit): ImmutableVec2f {
    return StdImmutableVec2f.builder().apply(builderAction).build()
}
