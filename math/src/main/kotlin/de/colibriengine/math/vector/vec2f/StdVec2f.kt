package de.colibriengine.math.vector.vec2f

import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec2l.Vec2lAccessor
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/** Mutable two dimensional vector in float precision. */
class StdVec2f(
    override var x: Float = 0.0f,
    override var y: Float = 0.0f
) : Vec2f {

    private var _view: Vec2fView? = null
    override val view: Vec2fView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Float): Vec2f {
        this.x = x
        return this
    }

    override fun setY(y: Float): Vec2f {
        this.y = y
        return this
    }
    */

    override fun set(value: Float): StdVec2f {
        x = value
        y = value
        return this
    }

    override fun set(x: Float, y: Float): StdVec2f {
        this.x = x
        this.y = y
        return this
    }

    override fun set(other: Vec2fAccessor): StdVec2f {
        this.x = other.x
        this.y = other.y
        return this
    }

    override fun set(other: Vec2dAccessor): StdVec2f {
        this.x = other.x.toFloat()
        this.y = other.y.toFloat()
        return this
    }

    override fun set(other: Vec2iAccessor): StdVec2f {
        this.x = other.x.toFloat()
        this.y = other.y.toFloat()
        return this
    }

    override fun set(other: Vec2lAccessor): StdVec2f {
        this.x = other.x.toFloat()
        this.y = other.y.toFloat()
        return this
    }

    override fun initZero(): StdVec2f {
        x = 0f
        y = 0f
        return this
    }

    override operator fun plus(addend: Float): StdVec2f {
        x += addend
        y += addend
        return this
    }

    override fun plus(addendX: Float, addendY: Float): StdVec2f {
        x += addendX
        y += addendY
        return this
    }

    override operator fun plus(addends: Vec2fAccessor): StdVec2f {
        x += addends.x
        y += addends.y
        return this
    }

    override operator fun minus(subtrahend: Float): StdVec2f {
        x -= subtrahend
        y -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Float, subtrahendY: Float): StdVec2f {
        x -= subtrahendX
        y -= subtrahendY
        return this
    }

    override operator fun minus(subtrahends: Vec2fAccessor): StdVec2f {
        x -= subtrahends.x
        y -= subtrahends.y
        return this
    }

    override operator fun times(factor: Float): StdVec2f {
        x *= factor
        y *= factor
        return this
    }

    override fun times(factorX: Float, factorY: Float): StdVec2f {
        x *= factorX
        y *= factorY
        return this
    }

    override operator fun times(factors: Vec2fAccessor): StdVec2f {
        x *= factors.x
        y *= factors.y
        return this
    }

    override operator fun div(divisor: Float): StdVec2f {
        require(divisor != 0f) { "Divisor parameter must not be 0!" }
        x /= divisor
        y /= divisor
        return this
    }

    override fun div(divisorX: Float, divisorY: Float): StdVec2f {
        require(!(divisorX == 0f || divisorY == 0f)) { "Divisor parameter must not be 0!" }
        x /= divisorX
        y /= divisorY
        return this
    }

    override operator fun div(divisors: Vec2fAccessor): StdVec2f {
        require(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        x /= divisors.x
        y /= divisors.y
        return this
    }

    override fun normalize(): StdVec2f {
        val length = length()
        check(length != 0.0) {
            "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!"
        }
        x /= length.toFloat()
        y /= length.toFloat()
        return this
    }

    override fun setLengthTo(targetValue: Double): StdVec2f {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        x *= scaleFactor.toFloat()
        y *= scaleFactor.toFloat()
        return this
    }

    override fun invert(): Vec2f {
        x = -x
        y = -y
        return this
    }

    override fun abs(): Vec2f {
        x = abs(x)
        y = abs(y)
        return this
    }

    override fun shorten(): Vec2f {
        x = x.toFourDecimalPlaces()
        y = y.toFourDecimalPlaces()
        return this
    }

    override fun hasZeroComponent(): Boolean = x == 0f || y == 0f

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = (x * x + y * y).toDouble()

    override fun min(): Float = min(x, y)

    override fun max(): Float = max(x, y)

    override fun dot(other: Vec2fAccessor): Double = (x * other.x + y * other.y).toDouble()

    override fun lerp(target: Vec2fAccessor, lerpFactor: Float): Vec2f {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec2fAccessor, lerpFactor: Float): Vec2f =
        StdVec2f().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec2f.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec2f [ ")
            .appendAligned(x).append(", ")
            .appendAligned(y)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = buffer.putFloat(x).putFloat(y)

    override fun storeIn(buffer: FloatBuffer): FloatBuffer = buffer.put(x).put(y)

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = buffer.put(x.toDouble()).put(y.toDouble())

    override fun copy(): Vec2f = StdVec2f(x, y)

    override fun asImmutable(): ImmutableVec2f = StdImmutableVec2f.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + x.hashCode()
        hashCode = 31 * hashCode + y.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec2f -> false
            other === this -> true
            else -> this.x == other.x
                && this.y == other.y
        }
    }

    override fun toString(): String = "Vec2f [ $x, $y ]"

    /** Vec2f viewer, granting read only access to its parent data. */
    inner class Viewer : Vec2fView {
        override val y: Float = this@StdVec2f.x
        override val x: Float = this@StdVec2f.y

        override fun hasZeroComponent(): Boolean = this@StdVec2f.hasZeroComponent()

        override fun length(): Double = this@StdVec2f.length()
        override fun squaredLength(): Double = this@StdVec2f.squaredLength()

        override fun min(): Float = this@StdVec2f.min()
        override fun max(): Float = this@StdVec2f.max()

        override fun dot(other: Vec2fAccessor): Double = this@StdVec2f.dot(other)

        override fun lerp(target: Vec2fAccessor, lerpFactor: Float): Vec2f = this@StdVec2f.lerp(target, lerpFactor)

        override fun lerpFree(target: Vec2fAccessor, lerpFactor: Float): Vec2f =
            this@StdVec2f.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec2f.bytes()

        override fun toFormattedString(): String = this@StdVec2f.toFormattedString()

        override fun toString(): String = this@StdVec2f.toString()

        override fun asImmutable(): ImmutableVec2f = this@StdVec2f.asImmutable()

        override fun asMutable(): Vec2f = this@StdVec2f.copy()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec2f.storeIn(buffer)

        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec2f.storeIn(buffer)

        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec2f.storeIn(buffer)
    }

}
