package de.colibriengine.math.vector.vec2f

/** StdVec2fFactory. */
class StdVec2fFactory : Vec2fFactory {

    private val pool = StdVec2fPool()

    override fun acquire(): Vec2f = pool.acquire()

    override fun acquireDirty(): Vec2f = pool.acquireDirty()

    override fun free(vec2f: Vec2f) = pool.free(vec2f)

    override fun immutableVec2fBuilder(): ImmutableVec2f.Builder = StdImmutableVec2f.builder()

    override fun zero(): ImmutableVec2f = ZERO

    override fun one(): ImmutableVec2f = ONE

    companion object {
        val ZERO = StdImmutableVec2f.builder().of(0f)
        val ONE = StdImmutableVec2f.builder().of(1f)
    }

}
