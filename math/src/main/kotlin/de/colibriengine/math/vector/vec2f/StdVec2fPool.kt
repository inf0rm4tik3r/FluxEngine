package de.colibriengine.math.vector.vec2f

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec2fPool : ObjectPool<Vec2f>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec2f()
    },
    { instance: Vec2f -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec2fPool::class.java)
    }

}
