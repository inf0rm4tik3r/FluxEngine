package de.colibriengine.math.vector.vec2i

import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec2i private constructor(
    x: Int,
    y: Int
) : ImmutableVec2i {

    private val mutableAccessor: Vec2iAccessor = StdVec2i(x, y)

    override val x: Int = mutableAccessor.x
    override val y: Int = mutableAccessor.y

    override fun asMutable(): Vec2i = StdVec2i().set(this)

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Int = mutableAccessor.min()
    override fun max(): Int = mutableAccessor.max()

    override fun dot(other: Vec2iAccessor): Double = mutableAccessor.dot(other)

    override fun lerp(target: Vec2iAccessor, lerpFactor: Int): Vec2i = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec2iAccessor, lerpFactor: Int): Vec2i = mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec2i", "ImmutableVec2i")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec2i.Builder {

        private var x: Int = 0
        private var y: Int = 0

        override fun of(value: Int): ImmutableVec2i = set(value).build()

        override fun of(x: Int, y: Int): ImmutableVec2i = set(x, y).build()

        override fun of(other: Vec2iAccessor): ImmutableVec2i = set(other).build()

        override fun setX(x: Int): ImmutableVec2i.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Int): ImmutableVec2i.Builder {
            this.y = y
            return this
        }

        override fun set(value: Int): ImmutableVec2i.Builder {
            x = value
            y = value
            return this
        }

        override fun set(x: Int, y: Int): ImmutableVec2i.Builder {
            this.x = x
            this.y = y
            return this
        }

        override fun set(other: Vec2iAccessor): ImmutableVec2i.Builder {
            x = other.x
            y = other.y
            return this
        }

        override fun build(): ImmutableVec2i = StdImmutableVec2i(x, y)
    }

    companion object {
        fun builder(): ImmutableVec2i.Builder = Builder()
    }

}

inline fun buildImmutableVec2i(builderAction: ImmutableVec2i.Builder.() -> Unit): ImmutableVec2i {
    return StdImmutableVec2i.builder().apply(builderAction).build()
}
