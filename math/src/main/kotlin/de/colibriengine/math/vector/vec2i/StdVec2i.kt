package de.colibriengine.math.vector.vec2i

import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2l.Vec2lAccessor
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/** Mutable two dimensional vector in int precision. */
class StdVec2i(
    x: Int = 0,
    y: Int = 0
) : Vec2i {

    private var _x: Int = x
    override var x: Int
        get() = _x
        set(value) {
            _x = value
        }

    private var _y: Int = y
    override var y: Int
        get() = _y
        set(value) {
            _y = value
        }

    private var _view: Vec2iView? = null
    override val view: Vec2iView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    override fun set(value: Int): StdVec2i {
        _x = value
        _y = value
        return this
    }

    override fun set(x: Int, y: Int): StdVec2i {
        this._x = x
        this._y = y
        return this
    }

    override fun set(other: Vec2fAccessor): StdVec2i {
        this._x = other.x.toInt()
        this._y = other.y.toInt()
        return this
    }

    override fun set(other: Vec2dAccessor): StdVec2i {
        this._x = other.x.toInt()
        this._y = other.y.toInt()
        return this
    }

    override fun set(other: Vec2iAccessor): StdVec2i {
        this._x = other.x
        this._y = other.y
        return this
    }

    override fun set(other: Vec2lAccessor): StdVec2i {
        this._x = other.x.toInt()
        this._y = other.y.toInt()
        return this
    }

    override fun initZero(): StdVec2i {
        _x = 0
        _y = 0
        return this
    }

    override fun plus(addend: Int): StdVec2i {
        _x += addend
        _y += addend
        return this
    }

    override fun plus(addendX: Int, addendY: Int): StdVec2i {
        _x += addendX
        _y += addendY
        return this
    }

    override fun plus(addends: Vec2iAccessor): StdVec2i {
        _x += addends.x
        _y += addends.y
        return this
    }

    override fun minus(subtrahend: Int): StdVec2i {
        _x -= subtrahend
        _y -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Int, subtrahendY: Int): StdVec2i {
        _x -= subtrahendX
        _y -= subtrahendY
        return this
    }

    override fun minus(subtrahends: Vec2iAccessor): StdVec2i {
        _x -= subtrahends.x
        _y -= subtrahends.y
        return this
    }

    override fun times(factor: Int): StdVec2i {
        _x *= factor
        _y *= factor
        return this
    }

    override fun times(factorX: Int, factorY: Int): StdVec2i {
        _x *= factorX
        _y *= factorY
        return this
    }

    override fun times(factors: Vec2iAccessor): StdVec2i {
        _x *= factors.x
        _y *= factors.y
        return this
    }

    override fun div(divisor: Int): StdVec2i {
        assert(divisor != 0) { "Divisor parameter must not be 0!" }
        _x /= divisor
        _y /= divisor
        return this
    }

    override fun div(divisorX: Int, divisorY: Int): StdVec2i {
        assert(!(divisorX == 0 || divisorY == 0)) { "Divisor parameter must not be 0!" }
        _x /= divisorX
        _y /= divisorY
        return this
    }

    override fun div(divisors: Vec2iAccessor): StdVec2i {
        assert(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        _x /= divisors.x
        _y /= divisors.y
        return this
    }

    override fun normalize(): StdVec2i {
        val length = length()
        assert(length != 0.0) {
            "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!"
        }
        _x /= length.toInt()
        _y /= length.toInt()
        return this
    }

    override fun setLengthTo(targetValue: Double): Vec2i {
        val length = length()
        assert(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        _x *= scaleFactor.toInt()
        _y *= scaleFactor.toInt()
        return this
    }

    override fun invert(): Vec2i {
        _x = -_x
        _y = -_y
        return this
    }

    override fun abs(): Vec2i {
        _x = abs(_x)
        _y = abs(_y)
        return this
    }

    override fun hasZeroComponent(): Boolean = _x == 0 || _y == 0

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = (_x * _x + _y * _y).toDouble()

    override fun min(): Int = min(_x, _y)

    override fun max(): Int = max(_x, _y)

    override fun dot(other: Vec2iAccessor): Double = (_x * other.x + _y * other.y).toDouble()

    override fun lerp(target: Vec2iAccessor, lerpFactor: Int): Vec2i {
        assert(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec2iAccessor, lerpFactor: Int): Vec2i =
        StdVec2i().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec2i.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec2i [ ")
            .appendAligned(_x).append(", ")
            .appendAligned(_y)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = buffer
        .putFloat(_x.toFloat())
        .putFloat(_y.toFloat())

    override fun storeIn(buffer: FloatBuffer): FloatBuffer = buffer
        .put(_x.toFloat())
        .put(_y.toFloat())

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = buffer
        .put(_x.toDouble())
        .put(_y.toDouble())

    override fun copy(): Vec2i = StdVec2i(_x, _y)

    override fun asImmutable(): ImmutableVec2i = StdImmutableVec2i.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + _x.hashCode()
        hashCode = 31 * hashCode + _y.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec2i -> false
            other === this -> true
            else -> this._x == other.x && this._y == other.y
        }
    }

    override fun toString(): String = "Vec2i [ $_x, $_y ]"

    /** Vec2i viewer, granting read-only access to its parent data. */
    inner class Viewer : Vec2iView {
        override val x: Int = this@StdVec2i._x
        override val y: Int = this@StdVec2i._y

        override fun hasZeroComponent(): Boolean = this@StdVec2i.hasZeroComponent()

        override fun length(): Double = this@StdVec2i.length()
        override fun squaredLength(): Double = this@StdVec2i.squaredLength()

        override fun min(): Int = this@StdVec2i.min()
        override fun max(): Int = this@StdVec2i.max()

        override fun dot(other: Vec2iAccessor): Double = this@StdVec2i.dot(other)

        override fun lerp(target: Vec2iAccessor, lerpFactor: Int): Vec2i = this@StdVec2i.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec2iAccessor, lerpFactor: Int): Vec2i =
            this@StdVec2i.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec2i.bytes()

        override fun toFormattedString(): String = this@StdVec2i.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec2i.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec2i.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec2i.storeIn(buffer)

        override fun asImmutable(): ImmutableVec2i = this@StdVec2i.asImmutable()
        override fun asMutable(): Vec2i = this@StdVec2i.copy()
    }

}
