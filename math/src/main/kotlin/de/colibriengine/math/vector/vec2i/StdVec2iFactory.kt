package de.colibriengine.math.vector.vec2i

/** StdVec2iFactory */
class StdVec2iFactory : Vec2iFactory {

    private val pool = StdVec2iPool()

    override fun acquire(): Vec2i = pool.acquire()

    override fun acquireDirty(): Vec2i = pool.acquireDirty()

    override fun free(vec2i: Vec2i) = pool.free(vec2i)

    override fun immutableVec2iBuilder(): ImmutableVec2i.Builder = StdImmutableVec2i.builder()

    override fun zero(): ImmutableVec2i = ZERO

    override fun one(): ImmutableVec2i = ONE

    companion object {
        val ZERO = StdImmutableVec2i.builder().of(0)
        val ONE = StdImmutableVec2i.builder().of(1)
    }

}
