package de.colibriengine.math.vector.vec2i

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec2iPool : ObjectPool<Vec2i>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec2i()
    },
    { instance: Vec2i -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec2iPool::class.java)
    }

}
