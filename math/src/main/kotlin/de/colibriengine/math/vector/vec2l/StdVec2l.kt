package de.colibriengine.math.vector.vec2l

import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/** Mutable two dimensional vector in long precision. */
class StdVec2l(
    override var x: Long = 0,
    override var y: Long = 0
) : Vec2l {

    private var _view: Vec2lView? = null
    override val view: Vec2lView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Long): StdVec2l {
        this.x = x
        return this
    }

    override fun setY(y: Long): StdVec2l {
        this.y = y
        return this
    }
    */

    override fun set(value: Long): StdVec2l {
        x = value
        y = value
        return this
    }

    override fun set(x: Long, y: Long): StdVec2l {
        this.x = x
        this.y = y
        return this
    }

    override fun set(other: Vec2fAccessor): StdVec2l {
        this.x = other.x.toLong()
        this.y = other.y.toLong()
        return this
    }

    override fun set(other: Vec2dAccessor): StdVec2l {
        this.x = other.x.toLong()
        this.y = other.y.toLong()
        return this
    }

    override fun set(other: Vec2iAccessor): StdVec2l {
        this.x = other.x.toLong()
        this.y = other.y.toLong()
        return this
    }

    override fun set(other: Vec2lAccessor): StdVec2l {
        this.x = other.x
        this.y = other.y
        return this
    }

    override fun initZero(): StdVec2l {
        x = 0
        y = 0
        return this
    }

    override operator fun plus(addend: Long): StdVec2l {
        x += addend
        y += addend
        return this
    }

    override fun plus(addendX: Long, addendY: Long): StdVec2l {
        x += addendX
        y += addendY
        return this
    }

    override operator fun plus(addends: Vec2lAccessor): StdVec2l {
        x += addends.x
        y += addends.y
        return this
    }

    override operator fun minus(subtrahend: Long): StdVec2l {
        x -= subtrahend
        y -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Long, subtrahendY: Long): StdVec2l {
        x -= subtrahendX
        y -= subtrahendY
        return this
    }

    override operator fun minus(subtrahends: Vec2lAccessor): StdVec2l {
        x -= subtrahends.x
        y -= subtrahends.y
        return this
    }

    override operator fun times(factor: Long): StdVec2l {
        x *= factor
        y *= factor
        return this
    }

    override fun times(factorX: Long, factorY: Long): StdVec2l {
        x *= factorX
        y *= factorY
        return this
    }

    override operator fun times(factors: Vec2lAccessor): StdVec2l {
        x *= factors.x
        y *= factors.y
        return this
    }

    override operator fun div(divisor: Long): StdVec2l {
        require(divisor != 0L) { "Divisor parameter must not be 0!" }
        x /= divisor
        y /= divisor
        return this
    }

    override fun div(divisorX: Long, divisorY: Long): StdVec2l {
        require(!(divisorX == 0L || divisorY == 0L)) { "Divisor parameter must not be 0!" }
        x /= divisorX
        y /= divisorY
        return this
    }

    override operator fun div(divisors: Vec2lAccessor): StdVec2l {
        require(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        x /= divisors.x
        y /= divisors.y
        return this
    }

    override fun normalize(): StdVec2l {
        val length = length()
        check(length != 0.0) {
            "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!"
        }
        x /= length.toLong()
        y /= length.toLong()
        return this
    }

    override fun setLengthTo(targetValue: Double): Vec2l {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        x *= scaleFactor.toLong()
        y *= scaleFactor.toLong()
        return this
    }

    override fun invert(): Vec2l {
        x = -x
        y = -y
        return this
    }

    override fun abs(): Vec2l {
        x = kotlin.math.abs(x)
        y = kotlin.math.abs(y)
        return this
    }

    override fun hasZeroComponent(): Boolean = x == 0L || y == 0L

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = (x * x + y * y).toDouble()

    override fun min(): Long = min(x, y)

    override fun max(): Long = max(x, y)

    override fun dot(other: Vec2lAccessor): Double = (x * other.x + y * other.y).toDouble()

    override fun lerp(target: Vec2lAccessor, lerpFactor: Long): Vec2l {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec2lAccessor, lerpFactor: Long): Vec2l =
        StdVec2l().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec2l.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec2l [ ")
            .appendAligned(x).append(", ")
            .appendAligned(y)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = buffer.putFloat(x.toFloat()).putFloat(y.toFloat())

    override fun storeIn(buffer: FloatBuffer): FloatBuffer = buffer.put(x.toFloat()).put(y.toFloat())

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = buffer.put(x.toDouble()).put(y.toDouble())

    override fun copy(): Vec2l = StdVec2l(x, y)

    override fun asImmutable(): ImmutableVec2l = StdImmutableVec2l.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + x.hashCode()
        hashCode = 31 * hashCode + y.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec2l -> false
            other === this -> true
            else -> this.x == other.x && this.y == other.y
        }
    }

    override fun toString(): String = "Vec2l [ $x, $y ]"

    /** Vec2l viewer, granting read-only access to its parent data. */
    inner class Viewer : Vec2lView {
        override val x: Long = this@StdVec2l.x
        override val y: Long = this@StdVec2l.y

        override fun hasZeroComponent(): Boolean = this@StdVec2l.hasZeroComponent()

        override fun length(): Double = this@StdVec2l.length()
        override fun squaredLength(): Double = this@StdVec2l.squaredLength()

        override fun min(): Long = this@StdVec2l.min()
        override fun max(): Long = this@StdVec2l.max()

        override fun dot(other: Vec2lAccessor): Double = this@StdVec2l.dot(other)

        override fun lerp(target: Vec2lAccessor, lerpFactor: Long): Vec2l = this@StdVec2l.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec2lAccessor, lerpFactor: Long): Vec2l =
            this@StdVec2l.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec2l.bytes()

        override fun toFormattedString(): String = this@StdVec2l.toFormattedString()
        override fun toString(): String = this@StdVec2l.toString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec2l.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec2l.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec2l.storeIn(buffer)

        override fun asImmutable(): ImmutableVec2l = this@StdVec2l.asImmutable()
        override fun asMutable(): Vec2l = this@StdVec2l.copy()
    }

}
