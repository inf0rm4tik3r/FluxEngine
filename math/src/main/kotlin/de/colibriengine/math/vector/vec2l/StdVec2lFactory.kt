package de.colibriengine.math.vector.vec2l

/** StdVec2lFactory */
class StdVec2lFactory : Vec2lFactory {

    private val pool = StdVec2lPool()

    override fun acquire(): Vec2l = pool.acquire()

    override fun acquireDirty(): Vec2l = pool.acquireDirty()

    override fun free(vec2l: Vec2l) = pool.free(vec2l)

    override fun immutableVec2lBuilder(): ImmutableVec2l.Builder = StdImmutableVec2l.builder()

    override fun zero(): ImmutableVec2l = ZERO

    override fun one(): ImmutableVec2l = ONE

    companion object {
        val ZERO = StdImmutableVec2l.builder().of(0)
        val ONE = StdImmutableVec2l.builder().of(1)
    }

}
