package de.colibriengine.math.vector.vec3d

import de.colibriengine.math.vector.vec2d.Vec2d
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec3d private constructor(
    x: Double,
    y: Double,
    z: Double
) : ImmutableVec3d {

    private val mutableAccessor: Vec3dAccessor = StdVec3d(x, y, z)

    override val x: Double = mutableAccessor.x
    override val y: Double = mutableAccessor.y
    override val z: Double = mutableAccessor.z

    override fun asMutable(): Vec3d = StdVec3d().set(this)

    override fun xy(): Vec2d = mutableAccessor.xy()
    override fun yz(): Vec2d = mutableAccessor.yz()
    override fun xz(): Vec2d = mutableAccessor.xz()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Double = mutableAccessor.min()
    override fun mid(): Double = mutableAccessor.mid()
    override fun max(): Double = mutableAccessor.max()

    override infix fun dot(other: Vec3dAccessor): Double = mutableAccessor.dot(other)

    override fun angleRadians(other: Vec3dAccessor): Double = mutableAccessor.angleRadians(other)
    override fun angleDegrees(other: Vec3dAccessor): Double = mutableAccessor.angleDegrees(other)

    override fun lerp(target: Vec3dAccessor, lerpFactor: Double): Vec3d = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec3dAccessor, lerpFactor: Double): Vec3d =
        mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec3d", "ImmutableVec3d")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec3d.Builder {

        private var x: Double = 0.0
        private var y: Double = 0.0
        private var z: Double = 0.0

        override fun of(value: Double): ImmutableVec3d = set(value).build()

        override fun of(x: Double, y: Double, z: Double): ImmutableVec3d = set(x, y, z).build()

        override fun of(vec3dAccessor: Vec3dAccessor): ImmutableVec3d = set(vec3dAccessor).build()

        override fun setX(x: Double): ImmutableVec3d.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Double): ImmutableVec3d.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Double): ImmutableVec3d.Builder {
            this.z = z
            return this
        }

        override fun set(value: Double): ImmutableVec3d.Builder {
            x = value
            y = value
            z = value
            return this
        }

        override fun set(x: Double, y: Double, z: Double): ImmutableVec3d.Builder {
            this.x = x
            this.y = y
            this.z = z
            return this
        }

        override fun set(vec3dAccessor: Vec3dAccessor): ImmutableVec3d.Builder {
            x = vec3dAccessor.x
            y = vec3dAccessor.y
            z = vec3dAccessor.z
            return this
        }

        override fun build(): ImmutableVec3d = StdImmutableVec3d(x, y, z)
    }

    companion object {
        fun builder(): ImmutableVec3d.Builder = Builder()
    }

}

inline fun buildImmutableVec3d(builderAction: ImmutableVec3d.Builder.() -> Unit): ImmutableVec3d {
    return StdImmutableVec3d.builder().apply(builderAction).build()
}
