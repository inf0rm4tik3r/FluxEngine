package de.colibriengine.math.vector.vec3d

import de.colibriengine.logging.LogUtil
import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.quaternion.quaternionD.StdQuaternionD
import de.colibriengine.math.vector.vec2d.StdVec2d
import de.colibriengine.math.vector.vec2d.Vec2d
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec3l.Vec3lAccessor
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.*

/** Mutable three dimensional vector in double precision. */
@Suppress("DuplicatedCode")
class StdVec3d(
    override var x: Double = 0.0,
    override var y: Double = 0.0,
    override var z: Double = 0.0
) : Vec3d {

    private var _view: Vec3dView? = null
    override val view: Vec3dView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Double): StdVec3d {
        this.x = x
        return this
    }

    override fun setY(y: Double): StdVec3d {
        this.y = y
        return this
    }

    override fun setZ(z: Double): StdVec3d {
        this.z = z
        return this
    }
    */

    override fun set(value: Double): StdVec3d {
        z = value
        y = z
        x = y
        return this
    }

    override fun set(x: Double, y: Double, z: Double): StdVec3d {
        this.x = x
        this.y = y
        this.z = z
        return this
    }

    override fun set(other: Vec3fAccessor): StdVec3d {
        x = other.x.toDouble()
        y = other.y.toDouble()
        z = other.z.toDouble()
        return this
    }

    override fun set(other: Vec3dAccessor): StdVec3d {
        x = other.x
        y = other.y
        z = other.z
        return this
    }

    override fun set(other: Vec3iAccessor): StdVec3d {
        x = other.x.toDouble()
        y = other.y.toDouble()
        z = other.z.toDouble()
        return this
    }

    override fun set(other: Vec3lAccessor): StdVec3d {
        x = other.x.toDouble()
        y = other.y.toDouble()
        z = other.z.toDouble()
        return this
    }

    override fun set(other: Vec2fAccessor, z: Double): Vec3d {
        x = other.x.toDouble()
        y = other.y.toDouble()
        this.z = z
        return this
    }

    override fun initZero(): StdVec3d {
        x = 0.0
        y = 0.0
        z = 0.0
        return this
    }

    override operator fun plus(addend: Double): StdVec3d {
        x += addend
        y += addend
        z += addend
        return this
    }

    override fun plus(addendX: Double, addendY: Double, addendZ: Double): StdVec3d {
        x += addendX
        y += addendY
        z += addendZ
        return this
    }

    override operator fun plus(addends: Vec3dAccessor): StdVec3d {
        x += addends.x
        y += addends.y
        z += addends.z
        return this
    }

    override operator fun minus(subtrahend: Double): StdVec3d {
        x -= subtrahend
        y -= subtrahend
        z -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Double, subtrahendY: Double, subtrahendZ: Double): StdVec3d {
        x -= subtrahendX
        y -= subtrahendY
        z -= subtrahendZ
        return this
    }

    override operator fun minus(subtrahends: Vec3dAccessor): StdVec3d {
        x -= subtrahends.x
        y -= subtrahends.y
        z -= subtrahends.z
        return this
    }

    override operator fun times(factor: Double): StdVec3d {
        x *= factor
        y *= factor
        z *= factor
        return this
    }

    override fun times(factorX: Double, factorY: Double, factorZ: Double): StdVec3d {
        x *= factorX
        y *= factorY
        z *= factorZ
        return this
    }

    override operator fun times(factors: Vec3dAccessor): StdVec3d {
        x *= factors.x
        y *= factors.y
        z *= factors.z
        return this
    }

    override operator fun div(divisor: Double): StdVec3d {
        require(divisor != 0.0) { "Divisor parameter must not be 0!" }
        x /= divisor
        y /= divisor
        z /= divisor
        return this
    }

    override fun div(divisorX: Double, divisorY: Double, divisorZ: Double): StdVec3d {
        require(!(divisorX == 0.0 || divisorY == 0.0 || divisorZ == 0.0)) { "Divisor parameter must not be 0!" }
        x /= divisorX
        y /= divisorY
        z /= divisorZ
        return this
    }

    override operator fun div(divisors: Vec3dAccessor): StdVec3d {
        require(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        x /= divisors.x
        y /= divisors.y
        z /= divisors.z
        return this
    }

    override fun normalize(): StdVec3d {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!" }
        x /= length
        y /= length
        z /= length
        return this
    }

    override fun setLengthTo(targetValue: Double): StdVec3d {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        x *= scaleFactor
        y *= scaleFactor
        z *= scaleFactor
        return this
    }

    override fun invert(): Vec3d {
        x = -x
        y = -y
        z = -z
        return this
    }

    override fun abs(): Vec3d {
        x = abs(x)
        y = abs(y)
        z = abs(z)
        return this
    }

    override fun shorten(): StdVec3d {
        x = x.toFourDecimalPlaces()
        y = y.toFourDecimalPlaces()
        z = z.toFourDecimalPlaces()
        return this
    }

    override fun cross(other: Vec3dAccessor): Vec3d {
        val xNew = y * other.z - z * other.y
        val yNew = z * other.x - x * other.z
        val zNew = x * other.y - y * other.x
        x = xNew
        y = yNew
        z = zNew
        return this
    }

    override fun reflect(normalizedNormal: Vec3dAccessor): Vec3d {
        // reflect = initial - normalizedNormal * (2 * (initial o normalizedNormal));
        val normalizedNormalCopy = StdVec3d().set(normalizedNormal)
        this.minus(normalizedNormalCopy.times(2 * dot(normalizedNormalCopy)))
        // free(normalizedNormalCopy);
        return this
    }

    override fun refract(normalizedNormal: Vec3dAccessor, eta: Double): Vec3d {
        assert(abs(1 - length()) < 0.01) { "This vector was not normalized before." }

        // Store the dot product for later calculations.
        val dotNormal = this.dot(normalizedNormal)
        val k = 1.0f - eta * eta * (1 - dotNormal * dotNormal)

        // A negative k would result in a reflection of the incoming vector... and the sqrt of k would then be NaN.
        if (k < 0.0f) {
            LOG.warn("Unable to calculate refraction vector. K is negative. Leaving this vector unchanged.")
            return this
        }

        val factor = eta * dotNormal + sqrt(k)
        val normalizedNormalCopy = StdVec3d().set(normalizedNormal)
        this.times(eta).minus(normalizedNormalCopy.times(factor))
        // free(normalizedNormalCopy);
        return this
    }

    override fun rotate(quaternion: QuaternionDAccessor): Vec3d {
        val crossProduct1 = quaternion.vectorPart(StdVec3d()).cross(this).times(2.0)
        val crossProduct2 = quaternion.vectorPart(StdVec3d()).cross(crossProduct1)
        crossProduct1.times(quaternion.scalarPart())
        return plus(crossProduct1).plus(crossProduct2)
    }

    override fun rotate(axis: Vec3dAccessor, angle: Double): Vec3d = rotate(StdQuaternionD().initRotation(axis, angle))

    // This creates an orthonormal basis for a right-handed coordinate system. For LH-system, swap both cross operands!
    override fun makeOrthonormalBasis(a: Vec3d, b: Vec3d, c: Vec3d) {
        a.normalize()
        c.set(a).cross(b)
        if (c.squaredLength() == 0.0) {
            throw IllegalArgumentException("Vectors a=$a and b=$a are parallel. Unable to compute orthogonal axis.")
        }
        c.normalize()
        b.set(c).cross(a)
    }

    override fun xy(): Vec2d = StdVec2d(x, y)

    override fun yz(): Vec2d = StdVec2d(y, z)

    override fun xz(): Vec2d = StdVec2d(x, z)

    override fun hasZeroComponent(): Boolean = x == 0.0 || y == 0.0 || z == 0.0

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = x * x + y * y + z * z

    override fun min(): Double = min(x, min(y, z))

    override fun mid(): Double = min(max(x, y), max(y, z))

    override fun max(): Double = max(x, max(y, z))

    override infix fun dot(other: Vec3dAccessor): Double = x * other.x + y * other.y + z * other.z

    override fun angleRadians(other: Vec3dAccessor): Double {
        val fromLength = this.length()
        val toLength = other.length()
        if (fromLength == 0.0 || toLength == 0.0) {
            throw IllegalStateException(
                "Both vectors need a length greater than 0! "
                    + "A vector of length 0 has no orientation, "
                    + "and therefore no angel to another vector."
            )
        }
        return acos(this.dot(other) / (fromLength * toLength))
    }

    override fun angleDegrees(other: Vec3dAccessor): Double = Math.toDegrees(angleRadians(other))

    override fun lerp(target: Vec3dAccessor, lerpFactor: Double): Vec3d {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(
        target: Vec3dAccessor,
        lerpFactor: Double
    ): Vec3d = StdVec3d().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec3d.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec3d [ ")
            .appendAligned(x).append(", ")
            .appendAligned(y).append(", ")
            .appendAligned(z)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.putDouble(x).putDouble(y).putDouble(z)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.put(x.toFloat()).put(y.toFloat()).put(z.toFloat())
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.put(x).put(y).put(z)
    }

    override fun copy(): Vec3d = StdVec3d(x, y, z)

    override fun asImmutable(): ImmutableVec3d = StdImmutableVec3d.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + x.hashCode()
        hashCode = 31 * hashCode + y.hashCode()
        hashCode = 31 * hashCode + z.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec3d -> false
            other === this -> true
            else -> x == other.x && y == other.y && z == other.z
        }
    }

    override fun toString(): String = "Vec3d [ $x, $y, $z ]"

    /** Vec3d viewer, granting read-only access to its parent data. */
    inner class Viewer : Vec3dView {
        override val x: Double = this@StdVec3d.x
        override val y: Double = this@StdVec3d.y
        override val z: Double = this@StdVec3d.y

        override fun xy(): Vec2d = this@StdVec3d.xy()
        override fun yz(): Vec2d = this@StdVec3d.yz()
        override fun xz(): Vec2d = this@StdVec3d.xz()

        override fun hasZeroComponent(): Boolean = this@StdVec3d.hasZeroComponent()

        override fun length(): Double = this@StdVec3d.length()
        override fun squaredLength(): Double = this@StdVec3d.squaredLength()

        override fun min(): Double = this@StdVec3d.min()
        override fun mid(): Double = this@StdVec3d.mid()
        override fun max(): Double = this@StdVec3d.max()

        override infix fun dot(other: Vec3dAccessor): Double = this@StdVec3d.dot(other)

        override fun angleRadians(other: Vec3dAccessor): Double = this@StdVec3d.angleRadians(other)
        override fun angleDegrees(other: Vec3dAccessor): Double = this@StdVec3d.angleDegrees(other)

        override fun lerp(target: Vec3dAccessor, lerpFactor: Double): Vec3d = this@StdVec3d.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec3dAccessor, lerpFactor: Double): Vec3d =
            this@StdVec3d.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec3d.bytes()

        override fun toFormattedString(): String = this@StdVec3d.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec3d.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec3d.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec3d.storeIn(buffer)

        override fun asImmutable(): ImmutableVec3d = this@StdVec3d.asImmutable()
        override fun asMutable(): Vec3d = this@StdVec3d.copy()
    }

    companion object {
        private val LOG = LogUtil.getLogger(StdVec3d::class.java)
    }

}
