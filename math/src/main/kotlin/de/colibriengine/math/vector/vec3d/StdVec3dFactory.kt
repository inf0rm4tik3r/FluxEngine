package de.colibriengine.math.vector.vec3d

/** StdVec3dFactory */
class StdVec3dFactory : Vec3dFactory {

    private val pool = StdVec3dPool()

    override fun acquire(): Vec3d = pool.acquire()

    override fun acquireDirty(): Vec3d = pool.acquireDirty()

    override fun free(vec3d: Vec3d) = pool.free(vec3d)

    override fun immutableVec3dBuilder(): ImmutableVec3d.Builder = StdImmutableVec3d.builder()

    override fun zero(): ImmutableVec3d = ZERO

    override fun one(): ImmutableVec3d = ONE

    override fun unitXAxis(): ImmutableVec3d = UNIT_X_AXIS

    override fun unitYAxis(): ImmutableVec3d = UNIT_Y_AXIS

    override fun unitZAxis(): ImmutableVec3d = UNIT_Z_AXIS

    override fun negativeUnitXAxis(): ImmutableVec3d = NEG_UNIT_X_AXIS

    override fun negativeUnitYAxis(): ImmutableVec3d = NEG_UNIT_Y_AXIS

    override fun negativeUnitZAxis(): ImmutableVec3d = NEG_UNIT_Z_AXIS

    companion object {
        val ZERO = StdImmutableVec3d.builder().of(0.0)
        val ONE = StdImmutableVec3d.builder().of(1.0)
        val UNIT_X_AXIS = StdImmutableVec3d.builder().of(1.0, 0.0, 0.0)
        val UNIT_Y_AXIS = StdImmutableVec3d.builder().of(0.0, 1.0, 0.0)
        val UNIT_Z_AXIS = StdImmutableVec3d.builder().of(0.0, 0.0, 1.0)
        val NEG_UNIT_X_AXIS = StdImmutableVec3d.builder().of(-1.0, 0.0, 0.0)
        val NEG_UNIT_Y_AXIS = StdImmutableVec3d.builder().of(0.0, -1.0, 0.0)
        val NEG_UNIT_Z_AXIS = StdImmutableVec3d.builder().of(0.0, 0.0, -1.0)
    }

}
