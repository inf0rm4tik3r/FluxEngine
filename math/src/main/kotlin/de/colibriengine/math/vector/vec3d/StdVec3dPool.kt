package de.colibriengine.math.vector.vec3d

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec3dPool : ObjectPool<Vec3d>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec3d()
    },
    { instance: Vec3d -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec3dPool::class.java)
    }

}
