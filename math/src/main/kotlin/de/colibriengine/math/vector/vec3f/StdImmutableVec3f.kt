package de.colibriengine.math.vector.vec3f

import de.colibriengine.math.vector.vec2f.Vec2f
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec3f private constructor(
    x: Float,
    y: Float,
    z: Float
) : ImmutableVec3f {

    private val mutableAccessor: Vec3fAccessor = StdVec3f(x, y, z)

    override val x: Float = mutableAccessor.x
    override val y: Float = mutableAccessor.y
    override val z: Float = mutableAccessor.z

    override fun asMutable(): Vec3f = StdVec3f().set(this)

    override fun xy(): Vec2f = mutableAccessor.xy()
    override fun yz(): Vec2f = mutableAccessor.yz()
    override fun xz(): Vec2f = mutableAccessor.xz()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Float = mutableAccessor.length()
    override fun squaredLength(): Float = mutableAccessor.squaredLength()

    override fun min(): Float = mutableAccessor.min()
    override fun mid(): Float = mutableAccessor.mid()
    override fun max(): Float = mutableAccessor.max()

    override infix fun dot(other: Vec3fAccessor): Float = mutableAccessor.dot(other)

    override fun angleRadians(other: Vec3fAccessor): Float = mutableAccessor.angleRadians(other)
    override fun angleDegrees(other: Vec3fAccessor): Float = mutableAccessor.angleDegrees(other)

    override fun lerp(target: Vec3fAccessor, lerpFactor: Float): Vec3f = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec3fAccessor, lerpFactor: Float): Vec3f =
        mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec3f", "ImmutableVec3f")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec3f.Builder {

        private var x: Float = 0f
        private var y: Float = 0f
        private var z: Float = 0f

        override fun of(value: Float): ImmutableVec3f = set(value).build()

        override fun of(x: Float, y: Float, z: Float): ImmutableVec3f = set(x, y, z).build()

        override fun of(other: Vec3fAccessor): ImmutableVec3f = set(other).build()

        override fun setX(x: Float): ImmutableVec3f.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Float): ImmutableVec3f.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Float): ImmutableVec3f.Builder {
            this.z = z
            return this
        }

        override fun set(value: Float): ImmutableVec3f.Builder {
            x = value
            y = value
            z = value
            return this
        }

        override fun set(x: Float, y: Float, z: Float): ImmutableVec3f.Builder {
            this.x = x
            this.y = y
            this.z = z
            return this
        }

        override fun set(other: Vec3fAccessor): ImmutableVec3f.Builder {
            x = other.x
            y = other.y
            z = other.z
            return this
        }

        override fun build(): ImmutableVec3f = StdImmutableVec3f(x, y, z)
    }

    companion object {
        fun builder(): ImmutableVec3f.Builder = Builder()
    }

}

inline fun buildImmutableVec3f(builderAction: ImmutableVec3f.Builder.() -> Unit): ImmutableVec3f {
    return StdImmutableVec3f.builder().apply(builderAction).build()
}
