package de.colibriengine.math.vector.vec3f

import de.colibriengine.logging.LogUtil
import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.quaternion.quaternionF.StdQuaternionF
import de.colibriengine.math.vector.vec2f.StdVec2f
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec3l.Vec3lAccessor
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.*

/** Mutable three dimensional vector in float precision. */
@Suppress("DuplicatedCode")
class StdVec3f(
    x: Float = 0.0f,
    y: Float = 0.0f,
    z: Float = 0.0f
) : Vec3f {

    private var _x: Float = x
    override var x: Float
        get() = _x
        set(value) {
            _x = value
        }

    private var _y: Float = y
    override var y: Float
        get() = _y
        set(value) {
            _y = value
        }

    private var _z: Float = z
    override var z: Float
        get() = _z
        set(value) {
            _z = value
        }

    private var _view: Vec3fView? = null
    override val view: Vec3fView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Float): StdVec3f {
        this.x = x
        return this
    }

    override fun setY(y: Float): StdVec3f {
        this.y = y
        return this
    }

    override fun setZ(z: Float): StdVec3f {
        this.z = z
        return this
    }
    */

    override fun set(value: Float): StdVec3f {
        _z = value
        _y = value
        _x = value
        return this
    }

    override fun set(x: Float, y: Float, z: Float): StdVec3f {
        this._x = x
        this._y = y
        this._z = z
        return this
    }

    override fun set(other: Vec3fAccessor): StdVec3f {
        _x = other.x
        _y = other.y
        _z = other.z
        return this
    }

    override fun set(other: Vec3dAccessor): StdVec3f {
        _x = other.x.toFloat()
        _y = other.y.toFloat()
        _z = other.z.toFloat()
        return this
    }

    override fun set(other: Vec3iAccessor): StdVec3f {
        _x = other.x.toFloat()
        _y = other.y.toFloat()
        _z = other.z.toFloat()
        return this
    }

    override fun set(other: Vec3lAccessor): StdVec3f {
        _x = other.x.toFloat()
        _y = other.y.toFloat()
        _z = other.z.toFloat()
        return this
    }

    override fun set(other: Vec2fAccessor, z: Float): Vec3f {
        _x = other.x
        _y = other.y
        this._z = z
        return this
    }

    override fun initZero(): StdVec3f {
        _x = 0.0f
        _y = 0.0f
        _z = 0.0f
        return this
    }

    override operator fun plus(addend: Float): StdVec3f {
        _x += addend
        _y += addend
        _z += addend
        return this
    }

    override fun plus(addendX: Float, addendY: Float, addendZ: Float): StdVec3f {
        _x += addendX
        _y += addendY
        _z += addendZ
        return this
    }

    override operator fun plus(addends: Vec3fAccessor): StdVec3f {
        _x += addends.x
        _y += addends.y
        _z += addends.z
        return this
    }

    override operator fun minus(subtrahend: Float): StdVec3f {
        _x -= subtrahend
        _y -= subtrahend
        _z -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Float, subtrahendY: Float, subtrahendZ: Float): StdVec3f {
        _x -= subtrahendX
        _y -= subtrahendY
        _z -= subtrahendZ
        return this
    }

    override operator fun minus(subtrahends: Vec3fAccessor): StdVec3f {
        _x -= subtrahends.x
        _y -= subtrahends.y
        _z -= subtrahends.z
        return this
    }

    override operator fun times(factor: Float): StdVec3f {
        _x *= factor
        _y *= factor
        _z *= factor
        return this
    }

    override fun times(factorX: Float, factorY: Float, factorZ: Float): StdVec3f {
        _x *= factorX
        _y *= factorY
        _z *= factorZ
        return this
    }

    override operator fun times(factors: Vec3fAccessor): StdVec3f {
        _x *= factors.x
        _y *= factors.y
        _z *= factors.z
        return this
    }

    override operator fun div(divisor: Float): StdVec3f {
        assert(divisor != 0.0f) { "Divisor parameter must not be 0!" }
        _x /= divisor
        _y /= divisor
        _z /= divisor
        return this
    }

    override fun div(divisorX: Float, divisorY: Float, divisorZ: Float): StdVec3f {
        assert(!(divisorX == 0.0f || divisorY == 0.0f || divisorZ == 0.0f)) {
            "Divisor parameter must not be 0!"
        }
        _x /= divisorX
        _y /= divisorY
        _z /= divisorZ
        return this
    }

    override operator fun div(divisors: Vec3fAccessor): StdVec3f {
        assert(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        _x /= divisors.x
        _y /= divisors.y
        _z /= divisors.z
        return this
    }

    override fun normalize(): StdVec3f {
        val length = length()
        assert(length != 0.0f) {
            "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!"
        }
        _x /= length
        _y /= length
        _z /= length
        return this
    }

    override fun setLengthTo(targetValue: Float): StdVec3f {
        val length = length()
        assert(length != 0.0f) {
            "This vector has a length of 0! Unable to calculate the scale factor."
        }
        val scaleFactor = targetValue / length
        _x *= scaleFactor
        _y *= scaleFactor
        _z *= scaleFactor
        return this
    }

    override fun invert(): Vec3f {
        _x = -x
        _y = -y
        _z = -z
        return this
    }

    override fun abs(): Vec3f {
        _x = abs(_x)
        _y = abs(_y)
        _z = abs(_z)
        return this
    }

    override fun shorten(): StdVec3f {
        _x = _x.toFourDecimalPlaces()
        _y = _y.toFourDecimalPlaces()
        _z = _z.toFourDecimalPlaces()
        return this
    }

    override infix fun cross(other: Vec3fAccessor): Vec3f {
        val xNew = _y * other.z - _z * other.y
        val yNew = _z * other.x - _x * other.z
        val zNew = _x * other.y - _y * other.x
        _x = xNew
        _y = yNew
        _z = zNew
        return this
    }

    override fun reflect(normalizedNormal: Vec3fAccessor): Vec3f {
        // r = d - n * (2 * (d o n))
        val nx = normalizedNormal.x
        val ny = normalizedNormal.y
        val nz = normalizedNormal.z
        val c = 2f * _x * nx + _y * ny + _z * nz
        val nxc = nx * c
        val nyc = ny * c
        val nzc = nz * c
        return minus(nxc, nyc, nzc)
    }

    override fun refract(normalizedNormal: Vec3fAccessor, eta: Double): Vec3f {
        assert(abs(1 - length()) < 0.01) { "This vector was not normalized before." }

        // Store the dot product for later calculations.
        val dotNormal = dot(normalizedNormal)
        val k = 1.0f - eta * eta * (1.0f - dotNormal * dotNormal)

        // A negative k would result in a reflection of the incoming vector... and the sqrt of k would then be NaN.
        if (k < 0.0f) {
            LOG.warn("Unable to calculate refraction vector. K is negative. Leaving this vector unchanged.")
            return this
        }

        val factor = (eta * dotNormal + sqrt(k)).toFloat()

        // val normalizedNormalCopy = StdVec3f().set(normalizedNormal)    <- We dont want copies!
        // this * eta.toFloat() - (normalizedNormalCopy * factor)

        val factoredNormalX = normalizedNormal.x * factor
        val factoredNormalY = normalizedNormal.y * factor
        val factoredNormalZ = normalizedNormal.z * factor
        _x = _x * eta.toFloat() - factoredNormalX
        _y = _y * eta.toFloat() - factoredNormalY
        _z = _z * eta.toFloat() - factoredNormalZ
        return this
    }

    override fun rotate(quaternion: QuaternionFAccessor): Vec3f {
        val crossProduct1 = quaternion.vectorPart(StdVec3f()).cross(this).times(2.0f)
        val crossProduct2 = quaternion.vectorPart(StdVec3f()).cross(crossProduct1)
        crossProduct1.times(quaternion.scalarPart())
        return plus(crossProduct1).plus(crossProduct2)
    }

    override fun rotate(axis: Vec3fAccessor, angle: Float): Vec3f = rotate(StdQuaternionF().initRotation(axis, angle))

    // This creates an orthonormal basis for a right-handed coordinate system. For LH-system, swap both cross operands!
    override fun makeOrthonormalBasis(a: Vec3f, b: Vec3f, c: Vec3f) {
        a.normalize()
        c.set(a).cross(b)
        if (c.squaredLength() == 0f) {
            throw IllegalArgumentException("Vectors a=$a and b=$a are parallel. Unable to compute orthogonal axis.")
        }
        c.normalize()
        b.set(c).cross(a)
    }

    override fun xy(): Vec2f = StdVec2f(_x, _y)

    override fun yz(): Vec2f = StdVec2f(_y, _z)

    override fun xz(): Vec2f = StdVec2f(_x, _z)

    override fun hasZeroComponent(): Boolean = _x == 0f || _y == 0f || _z == 0f

    override fun length(): Float = sqrt(squaredLength())

    override fun squaredLength(): Float = _x * _x + _y * _y + _z * _z

    override fun min(): Float = min(_x, min(_y, _z))

    override fun mid(): Float = min(max(_x, _y), max(_y, _z))

    override fun max(): Float = max(_x, max(_y, _z))

    override infix fun dot(other: Vec3fAccessor): Float = (_x * other.x + _y * other.y + _z * other.z)

    override fun angleRadians(other: Vec3fAccessor): Float {
        val fromLength = this.length()
        val toLength = other.length()
        assert(!(fromLength == 0.0f || toLength == 0.0f)) {
            "Both vectors need a length greater than 0! " +
                "A vector of length 0 has no orientation, " +
                "and therefore no angel to another vector."
        }
        return acos(this.dot(other) / (fromLength * toLength))
    }

    override fun angleDegrees(other: Vec3fAccessor): Float = Math.toDegrees(angleRadians(other).toDouble()).toFloat()

    override fun lerp(target: Vec3fAccessor, lerpFactor: Float): Vec3f {
        assert(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec3fAccessor, lerpFactor: Float): Vec3f =
        (StdVec3f().set(target) - this) * lerpFactor + this

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec3f.BYTES)).array()
    }

    override fun toFormattedString(): String {

        return StringBuilder()
            .append("Vec3f [ ")
            .appendAligned(_x).append(", ")
            .appendAligned(_y).append(", ")
            .appendAligned(_z)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.putFloat(_x).putFloat(_y).putFloat(_z)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.put(_x).put(_y).put(_z)
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.put(_x.toDouble()).put(_y.toDouble()).put(_z.toDouble())
    }

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + _x.hashCode()
        hashCode = 31 * hashCode + _y.hashCode()
        hashCode = 31 * hashCode + _z.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec3f -> false
            other === this -> true
            else -> _x == other.x && _y == other.y && _z == other.z
        }
    }

    override fun toString(): String = "Vec3f [ $_x, $_y, $_z ]"

    /** Vec3f viewer, granting read-only access to its parent data. */
    inner class Viewer : Vec3fView {
        override val x = this@StdVec3f._x
        override val y = this@StdVec3f._y
        override val z = this@StdVec3f._z

        override fun xy(): Vec2f = this@StdVec3f.xy()
        override fun yz(): Vec2f = this@StdVec3f.yz()
        override fun xz(): Vec2f = this@StdVec3f.xz()

        override fun hasZeroComponent(): Boolean = this@StdVec3f.hasZeroComponent()

        override fun length(): Float = this@StdVec3f.length()
        override fun squaredLength(): Float = this@StdVec3f.squaredLength()

        override fun min(): Float = this@StdVec3f.min()
        override fun mid(): Float = this@StdVec3f.mid()
        override fun max(): Float = this@StdVec3f.max()

        override infix fun dot(other: Vec3fAccessor): Float = this@StdVec3f.dot(other)

        override fun angleRadians(other: Vec3fAccessor): Float = this@StdVec3f.angleRadians(other)
        override fun angleDegrees(other: Vec3fAccessor): Float = this@StdVec3f.angleDegrees(other)

        override fun lerp(target: Vec3fAccessor, lerpFactor: Float): Vec3f = this@StdVec3f.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec3fAccessor, lerpFactor: Float): Vec3f =
            this@StdVec3f.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec3f.bytes()

        override fun equals(other: Any?): Boolean = this@StdVec3f == other
        override fun hashCode(): Int = this@StdVec3f.hashCode()
        override fun toString(): String = this@StdVec3f.toString()
        override fun toFormattedString(): String = this@StdVec3f.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec3f.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec3f.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec3f.storeIn(buffer)
    }

    companion object {
        private val LOG = LogUtil.getLogger(StdVec3f::class.java)
    }

}
