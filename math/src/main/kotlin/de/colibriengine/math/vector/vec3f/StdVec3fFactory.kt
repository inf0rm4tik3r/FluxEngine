package de.colibriengine.math.vector.vec3f

/** StdVec3fFactory. */
class StdVec3fFactory : Vec3fFactory {

    private val pool = StdVec3fPool()

    override fun acquire(): Vec3f = pool.acquire()

    override fun acquireDirty(): Vec3f = pool.acquireDirty()

    override fun free(vec3f: Vec3f) = pool.free(vec3f)

    override fun immutableVec3fBuilder(): ImmutableVec3f.Builder = StdImmutableVec3f.builder()

    override fun zero(): ImmutableVec3f = ZERO

    override fun one(): ImmutableVec3f = ONE

    override fun unitXAxis(): ImmutableVec3f = UNIT_X_AXIS

    override fun unitYAxis(): ImmutableVec3f = UNIT_Y_AXIS

    override fun unitZAxis(): ImmutableVec3f = UNIT_Z_AXIS

    override fun negativeUnitXAxis(): ImmutableVec3f = NEG_UNIT_X_AXIS

    override fun negativeUnitYAxis(): ImmutableVec3f = NEG_UNIT_Y_AXIS

    override fun negativeUnitZAxis(): ImmutableVec3f = NEG_UNIT_Z_AXIS

    companion object {
        val ZERO = StdImmutableVec3f.builder().of(0f)
        val ONE = StdImmutableVec3f.builder().of(1f)
        val UNIT_X_AXIS = StdImmutableVec3f.builder().of(1f, 0f, 0f)
        val UNIT_Y_AXIS = StdImmutableVec3f.builder().of(0f, 1f, 0f)
        val UNIT_Z_AXIS = StdImmutableVec3f.builder().of(0f, 0f, 1f)
        val NEG_UNIT_X_AXIS = StdImmutableVec3f.builder().of(-1f, 0f, 0f)
        val NEG_UNIT_Y_AXIS = StdImmutableVec3f.builder().of(0f, -1f, 0f)
        val NEG_UNIT_Z_AXIS = StdImmutableVec3f.builder().of(0f, 0f, -1f)
    }

}
