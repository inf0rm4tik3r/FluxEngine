package de.colibriengine.math.vector.vec3i

import de.colibriengine.math.vector.vec2i.Vec2i
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec3i private constructor(
    x: Int,
    y: Int,
    z: Int
) : ImmutableVec3i {

    private val mutableAccessor: Vec3iAccessor = StdVec3i(x, y, z)

    override val x: Int = mutableAccessor.x
    override val y: Int = mutableAccessor.y
    override val z: Int = mutableAccessor.z

    override fun asMutable(): Vec3i = StdVec3i().set(this)

    override fun xy(): Vec2i = mutableAccessor.xy()
    override fun yz(): Vec2i = mutableAccessor.yz()
    override fun xz(): Vec2i = mutableAccessor.xz()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Int = mutableAccessor.min()
    override fun mid(): Int = mutableAccessor.mid()
    override fun max(): Int = mutableAccessor.max()

    override infix fun dot(other: Vec3iAccessor): Double = mutableAccessor.dot(other)

    override fun angleRadians(other: Vec3iAccessor): Double = mutableAccessor.angleRadians(other)
    override fun angleDegrees(other: Vec3iAccessor): Double = mutableAccessor.angleDegrees(other)

    override fun lerp(target: Vec3iAccessor, lerpFactor: Int): Vec3i = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec3iAccessor, lerpFactor: Int): Vec3i = mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec3i", "ImmutableVec3i")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec3i.Builder {

        private var x: Int = 0
        private var y: Int = 0
        private var z: Int = 0

        override fun of(value: Int): ImmutableVec3i = set(value).build()

        override fun of(x: Int, y: Int, z: Int): ImmutableVec3i = set(x, y, z).build()

        override fun of(vec3iAccessor: Vec3iAccessor): ImmutableVec3i = set(vec3iAccessor).build()

        override fun setX(x: Int): ImmutableVec3i.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Int): ImmutableVec3i.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Int): ImmutableVec3i.Builder {
            this.z = z
            return this
        }

        override fun set(value: Int): ImmutableVec3i.Builder {
            x = value
            y = value
            z = value
            return this
        }

        override fun set(x: Int, y: Int, z: Int): ImmutableVec3i.Builder {
            this.x = x
            this.y = y
            this.z = z
            return this
        }

        override fun set(vec3iAccessor: Vec3iAccessor): ImmutableVec3i.Builder {
            x = vec3iAccessor.x
            y = vec3iAccessor.y
            z = vec3iAccessor.z
            return this
        }

        override fun build(): ImmutableVec3i = StdImmutableVec3i(x, y, z)
    }

    companion object {
        fun builder(): ImmutableVec3i.Builder = Builder()
    }

}

inline fun buildImmutableVec3i(builderAction: ImmutableVec3i.Builder.() -> Unit): ImmutableVec3i {
    return StdImmutableVec3i.builder().apply(builderAction).build()
}
