package de.colibriengine.math.vector.vec3i

import de.colibriengine.math.vector.vec2i.StdVec2i
import de.colibriengine.math.vector.vec2i.Vec2i
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3l.Vec3lAccessor
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.acos
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/** Mutable three dimensional vector in int precision. */
class StdVec3i(
    override var x: Int = 0,
    override var y: Int = 0,
    override var z: Int = 0
) : Vec3i {

    private var _view: Vec3iView? = null
    override val view: Vec3iView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Int): StdVec3i {
        this.x = x
        return this
    }

    override fun setY(y: Int): StdVec3i {
        this.y = y
        return this
    }

    override fun setZ(z: Int): StdVec3i {
        this.z = z
        return this
    }
    */

    override fun set(value: Int): StdVec3i {
        x = value
        y = value
        z = value
        return this
    }

    override fun set(x: Int, y: Int, z: Int): StdVec3i {
        this.x = x
        this.y = y
        this.z = z
        return this
    }

    override fun set(other: Vec3fAccessor): StdVec3i {
        x = other.x.toInt()
        y = other.y.toInt()
        z = other.z.toInt()
        return this
    }

    override fun set(other: Vec3dAccessor): StdVec3i {
        x = other.x.toInt()
        y = other.y.toInt()
        z = other.z.toInt()
        return this
    }

    override fun set(other: Vec3iAccessor): StdVec3i {
        x = other.x
        y = other.y
        z = other.z
        return this
    }

    override fun set(other: Vec3lAccessor): StdVec3i {
        x = other.x.toInt()
        y = other.y.toInt()
        z = other.z.toInt()
        return this
    }

    override fun set(other: Vec2iAccessor, z: Int): Vec3i {
        x = other.x
        y = other.y
        this.z = z
        return this
    }

    override fun initZero(): StdVec3i {
        x = 0
        y = 0
        z = 0
        return this
    }

    override operator fun plus(addend: Int): StdVec3i {
        x += addend
        y += addend
        z += addend
        return this
    }

    override fun plus(addendX: Int, addendY: Int, addendZ: Int): StdVec3i {
        x += addendX
        y += addendY
        z += addendZ
        return this
    }

    override operator fun plus(addends: Vec3iAccessor): StdVec3i {
        x += addends.x
        y += addends.y
        z += addends.z
        return this
    }

    override operator fun minus(subtrahend: Int): StdVec3i {
        x -= subtrahend
        y -= subtrahend
        z -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Int, subtrahendY: Int, subtrahendZ: Int): StdVec3i {
        x -= subtrahendX
        y -= subtrahendY
        z -= subtrahendZ
        return this
    }

    override operator fun minus(subtrahends: Vec3iAccessor): StdVec3i {
        x -= subtrahends.x
        y -= subtrahends.y
        z -= subtrahends.z
        return this
    }

    override operator fun times(factor: Int): StdVec3i {
        x *= factor
        y *= factor
        z *= factor
        return this
    }

    override fun times(factorX: Int, factorY: Int, factorZ: Int): StdVec3i {
        x *= factorX
        y *= factorY
        z *= factorZ
        return this
    }

    override operator fun times(factors: Vec3iAccessor): StdVec3i {
        x *= factors.x
        y *= factors.y
        z *= factors.z
        return this
    }

    override operator fun div(divisor: Int): StdVec3i {
        require(divisor != 0) { "Divisor parameter must not be 0!" }
        x /= divisor
        y /= divisor
        z /= divisor
        return this
    }

    override fun div(divisorX: Int, divisorY: Int, divisorZ: Int): StdVec3i {
        require(!(divisorX == 0 || divisorY == 0 || divisorZ == 0)) { "Divisor parameter must not be 0!" }
        x /= divisorX
        y /= divisorY
        z /= divisorZ
        return this
    }

    override operator fun div(divisors: Vec3iAccessor): StdVec3i {
        require(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        x /= divisors.x
        y /= divisors.y
        z /= divisors.z
        return this
    }

    override fun normalize(): StdVec3i {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!" }
        x /= length.toInt()
        y /= length.toInt()
        z /= length.toInt()
        return this
    }

    override fun setLengthTo(targetValue: Double): StdVec3i {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        x *= scaleFactor.toInt()
        y *= scaleFactor.toInt()
        z *= scaleFactor.toInt()
        return this
    }

    override fun invert(): Vec3i {
        x = -x
        y = -y
        z = -z
        return this
    }

    override fun abs(): Vec3i {
        x = kotlin.math.abs(x)
        y = kotlin.math.abs(y)
        z = kotlin.math.abs(z)
        return this
    }

    override fun cross(other: Vec3iAccessor): Vec3i {
        val xNew = y * other.z - z * other.y
        val yNew = z * other.x - x * other.z
        val zNew = x * other.y - y * other.x
        x = xNew
        y = yNew
        z = zNew
        return this
    }

    override fun xy(): Vec2i = StdVec2i(x, y)

    override fun yz(): Vec2i = StdVec2i(y, z)

    override fun xz(): Vec2i = StdVec2i(x, z)

    override fun hasZeroComponent(): Boolean = x == 0 || y == 0 || z == 0

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = (x * x + y * y + z * z).toDouble()

    override fun min(): Int = min(x, min(y, z))

    override fun mid(): Int = min(max(x, y), max(y, z))

    override fun max(): Int = max(x, max(y, z))

    override infix fun dot(other: Vec3iAccessor): Double = (x * other.x + y * other.y + z * other.z).toDouble()

    override fun angleRadians(other: Vec3iAccessor): Double {
        val fromLength = this.length()
        val toLength = other.length()
        check(!(fromLength == 0.0 || toLength == 0.0)) { "Both vectors need a length greater than 0! A vector of length 0 has no orientation, " + "and therefore no angel to another vector." }
        return acos(this.dot(other) / (fromLength * toLength))
    }

    override fun angleDegrees(other: Vec3iAccessor): Double = Math.toDegrees(angleRadians(other))

    override fun lerp(target: Vec3iAccessor, lerpFactor: Int): Vec3i {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec3iAccessor, lerpFactor: Int): Vec3i =
        StdVec3i().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec3i.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec3i [ ")
            .appendAligned(x.toFloat()).append(", ")
            .appendAligned(y.toFloat()).append(", ")
            .appendAligned(z.toFloat())
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.putFloat(x.toFloat()).putFloat(y.toFloat()).putFloat(z.toFloat())
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.put(x.toFloat()).put(y.toFloat()).put(z.toFloat())
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.put(x.toDouble()).put(y.toDouble()).put(z.toDouble())
    }

    override fun copy(): Vec3i = StdVec3i(x, y, z)

    override fun asImmutable(): ImmutableVec3i = StdImmutableVec3i.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + x.hashCode()
        hashCode = 31 * hashCode + y.hashCode()
        hashCode = 31 * hashCode + z.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec3i -> false
            other === this -> true
            else -> this.x == other.x
                && this.y == other.y
                && this.z == other.z
        }
    }

    override fun toString(): String = "Vec3i [ $x, $y, $z ]"

    /** Vec3i viewer, granting read-only access to its parent data. */
    inner class Viewer : Vec3iView {
        override val x: Int = this@StdVec3i.x
        override val y: Int = this@StdVec3i.y
        override val z: Int = this@StdVec3i.y

        override fun xy(): Vec2i = this@StdVec3i.xy()
        override fun yz(): Vec2i = this@StdVec3i.yz()
        override fun xz(): Vec2i = this@StdVec3i.xz()

        override fun hasZeroComponent(): Boolean = this@StdVec3i.hasZeroComponent()

        override fun length(): Double = this@StdVec3i.length()
        override fun squaredLength(): Double = this@StdVec3i.squaredLength()

        override fun min(): Int = this@StdVec3i.min()
        override fun mid(): Int = this@StdVec3i.mid()
        override fun max(): Int = this@StdVec3i.max()

        override infix fun dot(other: Vec3iAccessor): Double = this@StdVec3i.dot(other)

        override fun angleRadians(other: Vec3iAccessor): Double = this@StdVec3i.angleRadians(other)
        override fun angleDegrees(other: Vec3iAccessor): Double = this@StdVec3i.angleDegrees(other)

        override fun lerp(target: Vec3iAccessor, lerpFactor: Int): Vec3i = this@StdVec3i.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec3iAccessor, lerpFactor: Int): Vec3i =
            this@StdVec3i.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec3i.bytes()

        override fun toFormattedString(): String = this@StdVec3i.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec3i.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec3i.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec3i.storeIn(buffer)

        override fun asImmutable(): ImmutableVec3i = this@StdVec3i.asImmutable()
        override fun asMutable(): Vec3i = this@StdVec3i.copy()

    }

}
