package de.colibriengine.math.vector.vec3l

/** StdVec3lFactory. */
class StdVec3lFactory : Vec3lFactory {

    private val pool = StdVec3lPool()

    override fun acquire(): Vec3l = pool.acquire()

    override fun acquireDirty(): Vec3l = pool.acquireDirty()

    override fun free(vec3l: Vec3l) = pool.free(vec3l)

    override fun immutableVec3lBuilder(): ImmutableVec3l.Builder = StdImmutableVec3l.builder()

    override fun zero(): ImmutableVec3l = ZERO

    override fun one(): ImmutableVec3l = ONE

    override fun unitXAxis(): ImmutableVec3l = UNIT_X_AXIS

    override fun unitYAxis(): ImmutableVec3l = UNIT_Y_AXIS

    override fun unitZAxis(): ImmutableVec3l = UNIT_Z_AXIS

    override fun negativeUnitXAxis(): ImmutableVec3l = NEG_UNIT_X_AXIS

    override fun negativeUnitYAxis(): ImmutableVec3l = NEG_UNIT_Y_AXIS

    override fun negativeUnitZAxis(): ImmutableVec3l = NEG_UNIT_Z_AXIS

    companion object {
        val ZERO = StdImmutableVec3l.builder().of(0)
        val ONE = StdImmutableVec3l.builder().of(1)
        val UNIT_X_AXIS = StdImmutableVec3l.builder().of(1, 0, 0)
        val UNIT_Y_AXIS = StdImmutableVec3l.builder().of(0, 1, 0)
        val UNIT_Z_AXIS = StdImmutableVec3l.builder().of(0, 0, 1)
        val NEG_UNIT_X_AXIS = StdImmutableVec3l.builder().of(-1, 0, 0)
        val NEG_UNIT_Y_AXIS = StdImmutableVec3l.builder().of(0, -1, 0)
        val NEG_UNIT_Z_AXIS = StdImmutableVec3l.builder().of(0, 0, -1)
    }

}
