package de.colibriengine.math.vector.vec3l

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec3lPool : ObjectPool<Vec3l>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec3l()
    },
    { instance: Vec3l -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec3lPool::class.java)
    }

}
