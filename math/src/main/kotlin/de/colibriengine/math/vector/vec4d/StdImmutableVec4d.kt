package de.colibriengine.math.vector.vec4d

import de.colibriengine.math.vector.vec3d.Vec3d
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec4d private constructor(
    x: Double,
    y: Double,
    z: Double,
    w: Double
) : ImmutableVec4d {

    private val mutableAccessor: Vec4dAccessor = StdVec4d(x, y, z, w)

    override val x: Double = mutableAccessor.x
    override val y: Double = mutableAccessor.y
    override val z: Double = mutableAccessor.z
    override val w: Double = mutableAccessor.w

    override fun asMutable(): Vec4d = StdVec4d().set(this)

    override fun xyz(): Vec3d = mutableAccessor.xyz()
    override fun xyw(): Vec3d = mutableAccessor.xyw()
    override fun xzw(): Vec3d = mutableAccessor.xzw()
    override fun yzw(): Vec3d = mutableAccessor.yzw()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Double = mutableAccessor.min()
    override fun max(): Double = mutableAccessor.max()

    override fun lerp(target: Vec4dAccessor, lerpFactor: Double): Vec4d = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec4dAccessor, lerpFactor: Double): Vec4d =
        mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec4f", "ImmutableVec4d")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec4d.Builder {

        private var x: Double = 0.toDouble()
        private var y: Double = 0.toDouble()
        private var z: Double = 0.toDouble()
        private var w: Double = 0.toDouble()

        override fun of(value: Double): ImmutableVec4d = set(value).build()

        override fun of(x: Double, y: Double, z: Double, w: Double): ImmutableVec4d = set(x, y, z, w).build()

        override fun of(vec4dAccessor: Vec4dAccessor): ImmutableVec4d = set(vec4dAccessor).build()

        override fun setX(x: Double): ImmutableVec4d.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Double): ImmutableVec4d.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Double): ImmutableVec4d.Builder {
            this.z = z
            return this
        }

        override fun setW(w: Double): ImmutableVec4d.Builder {
            this.w = w
            return this
        }

        override fun set(value: Double): ImmutableVec4d.Builder {
            x = value
            y = value
            z = value
            w = value
            return this
        }

        override fun set(x: Double, y: Double, z: Double, w: Double): ImmutableVec4d.Builder {
            this.x = x
            this.y = y
            this.z = z
            this.w = w
            return this
        }

        override fun set(vec4dAccessor: Vec4dAccessor): ImmutableVec4d.Builder {
            x = vec4dAccessor.x
            y = vec4dAccessor.y
            z = vec4dAccessor.z
            w = vec4dAccessor.w
            return this
        }

        override fun build(): ImmutableVec4d = StdImmutableVec4d(x, y, z, w)
    }

    companion object {
        fun builder(): ImmutableVec4d.Builder = Builder()
    }

}

inline fun buildImmutableVec4d(builderAction: ImmutableVec4d.Builder.() -> Unit): ImmutableVec4d {
    return StdImmutableVec4d.builder().apply(builderAction).build()
}
