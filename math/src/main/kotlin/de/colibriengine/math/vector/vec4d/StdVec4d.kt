package de.colibriengine.math.vector.vec4d

import de.colibriengine.math.vector.vec3d.StdVec3d
import de.colibriengine.math.vector.vec3d.Vec3d
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.math.vector.vec4i.Vec4iAccessor
import de.colibriengine.math.vector.vec4l.Vec4lAccessor
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/** Mutable four dimensional vector in double precision. */
@Suppress("DuplicatedCode")
class StdVec4d(
    override var x: Double = 0.0,
    override var y: Double = 0.0,
    override var z: Double = 0.0,
    override var w: Double = 0.0
) : Vec4d {

    private var _view: Vec4dView? = null
    override val view: Vec4dView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Double): Vec4d {
        this.x = x
        return this
    }

    override fun setY(y: Double): Vec4d {
        this.y = y
        return this
    }

    override fun setZ(z: Double): Vec4d {
        this.z = z
        return this
    }

    override fun setW(w: Double): Vec4d {
        this.w = w
        return this
    }
    */

    override fun set(value: Double): Vec4d {
        x = value
        y = value
        z = value
        w = value
        return this
    }

    override operator fun set(x: Double, y: Double, z: Double, w: Double): Vec4d {
        this.x = x
        this.y = y
        this.z = z
        this.w = w
        return this
    }

    override fun set(other: Vec4fAccessor): Vec4d {
        this.x = other.x.toDouble()
        this.y = other.y.toDouble()
        this.z = other.z.toDouble()
        this.w = other.w.toDouble()
        return this
    }

    override fun set(other: Vec4dAccessor): Vec4d {
        this.x = other.x
        this.y = other.y
        this.z = other.z
        this.w = other.w
        return this
    }

    override fun set(other: Vec4iAccessor): Vec4d {
        this.x = other.x.toDouble()
        this.y = other.y.toDouble()
        this.z = other.z.toDouble()
        this.w = other.w.toDouble()
        return this
    }

    override fun set(other: Vec4lAccessor): Vec4d {
        this.x = other.x.toDouble()
        this.y = other.y.toDouble()
        this.z = other.z.toDouble()
        this.w = other.w.toDouble()
        return this
    }

    override operator fun set(other: Vec3dAccessor, w: Double): Vec4d {
        this.x = other.x
        this.y = other.y
        this.z = other.z
        this.w = w
        return this
    }

    override fun initZero(): Vec4d {
        x = 0.0
        y = 0.0
        z = 0.0
        w = 0.0
        return this
    }

    override operator fun plus(addend: Double): Vec4d {
        x += addend
        y += addend
        z += addend
        w += addend
        return this
    }

    override fun plus(addendX: Double, addendY: Double, addendZ: Double, addendW: Double): Vec4d {
        x += addendX
        y += addendY
        z += addendZ
        w += addendW
        return this
    }

    override operator fun plus(addends: Vec4dAccessor): Vec4d {
        x += addends.x
        y += addends.y
        z += addends.z
        w += addends.w
        return this
    }

    override operator fun minus(subtrahend: Double): Vec4d {
        x -= subtrahend
        y -= subtrahend
        z -= subtrahend
        w -= subtrahend
        return this
    }

    override fun minus(
        subtrahendX: Double, subtrahendY: Double,
        subtrahendZ: Double, subtrahendW: Double
    ): Vec4d {
        x -= subtrahendX
        y -= subtrahendY
        z -= subtrahendZ
        w -= subtrahendW
        return this
    }

    override operator fun minus(subtrahends: Vec4dAccessor): Vec4d {
        x -= subtrahends.x
        y -= subtrahends.y
        z -= subtrahends.z
        w -= subtrahends.w
        return this
    }

    override operator fun times(factor: Double): Vec4d {
        x *= factor
        y *= factor
        z *= factor
        w *= factor
        return this
    }

    override fun times(factorX: Double, factorY: Double, factorZ: Double, factorW: Double): Vec4d {
        x *= factorX
        y *= factorY
        z *= factorZ
        w *= factorW
        return this
    }

    override operator fun times(factors: Vec4dAccessor): Vec4d {
        x *= factors.x
        y *= factors.y
        z *= factors.z
        w *= factors.w
        return this
    }

    override operator fun div(divisor: Double): Vec4d {
        require(divisor != 0.0) { "Divisor parameter must not be 0!" }
        x /= divisor
        y /= divisor
        z /= divisor
        w /= divisor
        return this
    }

    override fun div(
        divisorX: Double, divisorY: Double,
        divisorZ: Double, divisorW: Double
    ): Vec4d {
        require(!(divisorX == 0.0 || divisorY == 0.0 || divisorZ == 0.0 || divisorW == 0.0)) { "Divisor parameter must not be 0!" }
        x /= divisorX
        y /= divisorY
        z /= divisorZ
        w /= divisorW
        return this
    }

    override operator fun div(divisors: Vec4dAccessor): Vec4d {
        require(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        x /= divisors.x
        y /= divisors.y
        z /= divisors.z
        w /= divisors.w
        return this
    }

    override fun normalize(): Vec4d {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!" }
        x /= length
        y /= length
        z /= length
        w /= length
        return this
    }

    override fun setLengthTo(targetValue: Double): Vec4d {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        x *= scaleFactor
        y *= scaleFactor
        z *= scaleFactor
        w *= scaleFactor
        return this
    }

    override fun invert(): Vec4d {
        x = -x
        y = -y
        z = -z
        w = -w
        return this
    }

    override fun abs(): Vec4d {
        x = abs(x)
        y = abs(y)
        z = abs(z)
        w = abs(w)
        return this
    }

    override fun shorten(): Vec4d {
        x = x.toFourDecimalPlaces()
        y = y.toFourDecimalPlaces()
        z = z.toFourDecimalPlaces()
        w = w.toFourDecimalPlaces()
        return this
    }

    override fun xyz(): Vec3d = StdVec3d().set(x, y, z)

    override fun xyw(): Vec3d = StdVec3d().set(x, y, w)

    override fun xzw(): Vec3d = StdVec3d().set(x, z, w)

    override fun yzw(): Vec3d = StdVec3d().set(y, z, w)

    override fun hasZeroComponent(): Boolean = x == 0.0 || y == 0.0 || z == 0.0 || w == 0.0

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = x * x + y * y + z * z + w * w

    override fun min(): Double = min(x, min(y, min(z, w)))

    override fun max(): Double = max(x, max(y, max(z, w)))

    override fun lerp(target: Vec4dAccessor, lerpFactor: Double): Vec4d {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec4dAccessor, lerpFactor: Double): Vec4d =
        StdVec4d().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec4d.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec4d [ ")
            .appendAligned(x).append(", ")
            .appendAligned(y).append(", ")
            .appendAligned(z).append(", ")
            .appendAligned(w)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.putDouble(x).putDouble(y).putDouble(z).putDouble(w)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x.toFloat()).put(y.toFloat()).put(z.toFloat()).put(w.toFloat())
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x).put(y).put(z).put(w)
    }

    override fun copy(): Vec4d = StdVec4d().set(x, y, z, w)

    override fun asImmutable(): ImmutableVec4d = StdImmutableVec4d.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + java.lang.Double.hashCode(x)
        hashCode = 31 * hashCode + java.lang.Double.hashCode(y)
        hashCode = 31 * hashCode + java.lang.Double.hashCode(z)
        hashCode = 31 * hashCode + java.lang.Double.hashCode(w)
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is StdVec4d -> false
            other === this -> true
            else -> this.x == other.x
                && this.y == other.y
                && this.z == other.z
                && this.w == other.w
        }
    }

    override fun toString(): String = "Vec4d [ $x, $y, $z, $w ]"

    /** Vec4d viewer, granting read-only access to its parent data. */
    private inner class Viewer : Vec4dView {
        override val x: Double = this@StdVec4d.x
        override val y: Double = this@StdVec4d.y
        override val z: Double = this@StdVec4d.y
        override val w: Double = this@StdVec4d.y

        override fun xyz(): Vec3d = this@StdVec4d.xyz()
        override fun xyw(): Vec3d = this@StdVec4d.xyw()
        override fun xzw(): Vec3d = this@StdVec4d.xzw()
        override fun yzw(): Vec3d = this@StdVec4d.yzw()

        override fun hasZeroComponent(): Boolean = this@StdVec4d.hasZeroComponent()

        override fun length(): Double = this@StdVec4d.length()
        override fun squaredLength(): Double = this@StdVec4d.squaredLength()

        override fun min(): Double = this@StdVec4d.min()
        override fun max(): Double = this@StdVec4d.max()

        override fun lerp(target: Vec4dAccessor, lerpFactor: Double): Vec4d = this@StdVec4d.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec4dAccessor, lerpFactor: Double): Vec4d =
            this@StdVec4d.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec4d.bytes()

        override fun toFormattedString(): String = this@StdVec4d.toFormattedString()
        override fun toString(): String = this@StdVec4d.toString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec4d.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec4d.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec4d.storeIn(buffer)

        override fun asImmutable(): ImmutableVec4d = StdImmutableVec4d.builder().of(this)
        override fun asMutable(): Vec4d = StdVec4d().set(this)
    }

}
