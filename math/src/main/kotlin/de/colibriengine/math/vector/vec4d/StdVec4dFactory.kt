package de.colibriengine.math.vector.vec4d

/** StdVec4dFactory. */
class StdVec4dFactory : Vec4dFactory {

    private val pool = StdVec4dPool()

    override fun acquire(): Vec4d = pool.acquire()

    override fun acquireDirty(): Vec4d = pool.acquireDirty()

    override fun free(vec4d: Vec4d) = pool.free(vec4d)

    override fun immutableVec4dBuilder(): ImmutableVec4d.Builder = StdImmutableVec4d.builder()

    override fun zero(): ImmutableVec4d = ZERO

    override fun one(): ImmutableVec4d = ONE

    companion object {
        val ZERO = StdImmutableVec4d.builder().of(0.0)
        val ONE = StdImmutableVec4d.builder().of(1.0)
    }

}
