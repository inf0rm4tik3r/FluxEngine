package de.colibriengine.math.vector.vec4d

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec4dPool : ObjectPool<Vec4d>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec4d()
    },
    { instance: Vec4d -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec4dPool::class.java)
    }

}
