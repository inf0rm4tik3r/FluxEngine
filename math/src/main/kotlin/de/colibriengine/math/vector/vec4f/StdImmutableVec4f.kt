package de.colibriengine.math.vector.vec4f

import de.colibriengine.math.vector.vec3f.Vec3f
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec4f private constructor(
    x: Float,
    y: Float,
    z: Float,
    w: Float
) : ImmutableVec4f {

    private val mutableAccessor: Vec4fAccessor = StdVec4f().set(x, y, z, w)

    override val x: Float = mutableAccessor.x
    override val y: Float = mutableAccessor.y
    override val z: Float = mutableAccessor.z
    override val w: Float = mutableAccessor.w

    override fun asMutable(): Vec4f = StdVec4f().set(this)

    override fun xyz(storeIn: Vec3f): Vec3f = mutableAccessor.xyz(storeIn)
    override fun xyw(storeIn: Vec3f): Vec3f = mutableAccessor.xyw(storeIn)
    override fun xzw(storeIn: Vec3f): Vec3f = mutableAccessor.xzw(storeIn)
    override fun yzw(storeIn: Vec3f): Vec3f = mutableAccessor.yzw(storeIn)

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Float = mutableAccessor.min()
    override fun max(): Float = mutableAccessor.max()

    override fun lerp(target: Vec4fAccessor, lerpFactor: Double): Vec4f = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec4fAccessor, lerpFactor: Double): Vec4f =
        mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec4f", "ImmutableVec4f")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec4f.Builder {

        private var x: Float = 0.toFloat()
        private var y: Float = 0.toFloat()
        private var z: Float = 0.toFloat()
        private var w: Float = 0.toFloat()

        override fun of(value: Float): ImmutableVec4f = set(value).build()

        override fun of(x: Float, y: Float, z: Float, w: Float): ImmutableVec4f = set(x, y, z, w).build()

        override fun of(vec4fAccessor: Vec4fAccessor): ImmutableVec4f = set(vec4fAccessor).build()

        override fun setX(x: Float): ImmutableVec4f.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Float): ImmutableVec4f.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Float): ImmutableVec4f.Builder {
            this.z = z
            return this
        }

        override fun setW(w: Float): ImmutableVec4f.Builder {
            this.w = w
            return this
        }

        override fun set(value: Float): ImmutableVec4f.Builder {
            x = value
            y = value
            z = value
            w = value
            return this
        }

        override fun set(x: Float, y: Float, z: Float, w: Float): ImmutableVec4f.Builder {
            this.x = x
            this.y = y
            this.z = z
            this.w = w
            return this
        }

        override fun set(vec4fAccessor: Vec4fAccessor): ImmutableVec4f.Builder {
            x = vec4fAccessor.x
            y = vec4fAccessor.y
            z = vec4fAccessor.z
            w = vec4fAccessor.w
            return this
        }

        override fun build(): ImmutableVec4f = StdImmutableVec4f(x, y, z, w)
    }

    companion object {
        fun builder(): ImmutableVec4f.Builder = Builder()
    }

}

inline fun buildImmutableVec4f(builderAction: ImmutableVec4f.Builder.() -> Unit): ImmutableVec4f {
    return StdImmutableVec4f.builder().apply(builderAction).build()
}

fun buildImmutableVec4f(x: Float, y: Float, z: Float, w: Float): ImmutableVec4f {
    return StdImmutableVec4f.builder().of(x, y, z, w)
}
