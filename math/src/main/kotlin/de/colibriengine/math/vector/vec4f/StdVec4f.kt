package de.colibriengine.math.vector.vec4f

import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec4d.Vec4dAccessor
import de.colibriengine.math.vector.vec4i.Vec4iAccessor
import de.colibriengine.math.vector.vec4l.Vec4lAccessor
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/** Mutable four dimensional vector in float precision. */
@Suppress("DuplicatedCode")
class StdVec4f(
    x: Float = 0.0f,
    y: Float = 0.0f,
    z: Float = 0.0f,
    w: Float = 0.0f
) : Vec4f {

    private var _x: Float = x
    override var x: Float
        get() = _x
        set(value) {
            _x = value
        }

    private var _y: Float = y
    override var y: Float
        get() = _y
        set(value) {
            _y = value
        }

    private var _z: Float = z
    override var z: Float
        get() = _z
        set(value) {
            _z = value
        }

    private var _w: Float = w
    override var w: Float
        get() = _w
        set(value) {
            _w = value
        }

    private var _view: Vec4fView? = null
    override val view: Vec4fView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Float): Vec4f {
        this.x = x
        return this
    }

    override fun setY(y: Float): Vec4f {
        this.y = y
        return this
    }

    override fun setZ(z: Float): Vec4f {
        this.z = z
        return this
    }

    override fun setW(w: Float): Vec4f {
        this.w = w
        return this
    }
    */

    override fun set(value: Float): Vec4f {
        _x = value
        _y = value
        _z = value
        _w = value
        return this
    }

    override fun set(x: Float, y: Float, z: Float, w: Float): Vec4f {
        this._x = x
        this._y = y
        this._z = z
        this._w = w
        return this
    }

    override fun set(other: Vec4fAccessor): Vec4f {
        this._x = other.x
        this._y = other.y
        this._z = other.z
        this._w = other.w
        return this
    }

    override fun set(other: Vec4dAccessor): Vec4f {
        this._x = other.x.toFloat()
        this._y = other.y.toFloat()
        this._z = other.z.toFloat()
        this._w = other.w.toFloat()
        return this
    }

    override fun set(other: Vec4iAccessor): Vec4f {
        this._x = other.x.toFloat()
        this._y = other.y.toFloat()
        this._z = other.z.toFloat()
        this._w = other.w.toFloat()
        return this
    }

    override fun set(other: Vec4lAccessor): Vec4f {
        this._x = other.x.toFloat()
        this._y = other.y.toFloat()
        this._z = other.z.toFloat()
        this._w = other.w.toFloat()
        return this
    }

    override fun set(other: Vec3fAccessor, w: Float): Vec4f {
        this._x = other.x
        this._y = other.y
        this._z = other.z
        this._w = w
        return this
    }

    override fun initZero(): Vec4f {
        _x = 0f
        _y = 0f
        _z = 0f
        _w = 0f
        return this
    }

    override operator fun plus(addend: Float): Vec4f {
        _x += addend
        _y += addend
        _z += addend
        _w += addend
        return this
    }

    override fun plus(addendX: Float, addendY: Float, addendZ: Float, addendW: Float): Vec4f {
        _x += addendX
        _y += addendY
        _z += addendZ
        _w += addendW
        return this
    }

    override operator fun plus(addends: Vec4fAccessor): Vec4f {
        _x += addends.x
        _y += addends.y
        _z += addends.z
        _w += addends.w
        return this
    }

    override operator fun minus(subtrahend: Float): Vec4f {
        _x -= subtrahend
        _y -= subtrahend
        _z -= subtrahend
        _w -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Float, subtrahendY: Float, subtrahendZ: Float, subtrahendW: Float): Vec4f {
        _x -= subtrahendX
        _y -= subtrahendY
        _z -= subtrahendZ
        _w -= subtrahendW
        return this
    }

    override operator fun minus(subtrahends: Vec4fAccessor): Vec4f {
        _x -= subtrahends.x
        _y -= subtrahends.y
        _z -= subtrahends.z
        _w -= subtrahends.w
        return this
    }

    override operator fun times(factor: Float): Vec4f {
        _x *= factor
        _y *= factor
        _z *= factor
        _w *= factor
        return this
    }

    override fun times(factorX: Float, factorY: Float, factorZ: Float, factorW: Float): Vec4f {
        _x *= factorX
        _y *= factorY
        _z *= factorZ
        _w *= factorW
        return this
    }

    override operator fun times(factors: Vec4fAccessor): Vec4f {
        _x *= factors.x
        _y *= factors.y
        _z *= factors.z
        _w *= factors.w
        return this
    }

    override operator fun div(divisor: Float): Vec4f {
        assert(divisor != 0.0f) { "Divisor parameter must not be 0!" }
        _x /= divisor
        _y /= divisor
        _z /= divisor
        _w /= divisor
        return this
    }

    override fun div(divisorX: Float, divisorY: Float, divisorZ: Float, divisorW: Float): Vec4f {
        assert(!(divisorX == 0f || divisorY == 0f || divisorZ == 0f || divisorW == 0f)) {
            "Divisor parameter must not be 0!"
        }
        _x /= divisorX
        _y /= divisorY
        _z /= divisorZ
        _w /= divisorW
        return this
    }

    override operator fun div(divisors: Vec4fAccessor): Vec4f {
        assert(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        _x /= divisors.x
        _y /= divisors.y
        _z /= divisors.z
        _w /= divisors.w
        return this
    }

    override fun normalize(): Vec4f {
        val length = length()
        assert(length != 0.0) {
            "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!"
        }
        _x /= length.toFloat()
        _y /= length.toFloat()
        _z /= length.toFloat()
        _w /= length.toFloat()
        return this
    }

    override fun setLengthTo(targetValue: Double): Vec4f {
        val length = length()
        assert(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        _x *= scaleFactor.toFloat()
        _y *= scaleFactor.toFloat()
        _z *= scaleFactor.toFloat()
        _w *= scaleFactor.toFloat()
        return this
    }

    override fun invert(): Vec4f {
        _x = -_x
        _y = -_y
        _z = -_z
        _w = -_w
        return this
    }

    override fun abs(): Vec4f {
        _x = abs(_x)
        _y = abs(_y)
        _z = abs(_z)
        _w = abs(_w)
        return this
    }

    override fun shorten(): Vec4f {
        _x = _x.toFourDecimalPlaces()
        _y = _y.toFourDecimalPlaces()
        _z = _z.toFourDecimalPlaces()
        _w = _w.toFourDecimalPlaces()
        return this
    }

    override fun xyz(storeIn: Vec3f): Vec3f = storeIn.set(_x, _y, _z)

    override fun xyw(storeIn: Vec3f): Vec3f = storeIn.set(_x, _y, _w)

    override fun xzw(storeIn: Vec3f): Vec3f = storeIn.set(_x, _z, _w)

    override fun yzw(storeIn: Vec3f): Vec3f = storeIn.set(_y, _z, _w)

    override fun hasZeroComponent(): Boolean = _x == 0f || _y == 0f || _z == 0f || _w == 0f

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = (_x * _x + _y * _y + _z * _z + _w * _w).toDouble()

    override fun min(): Float = min(_x, min(_y, min(_z, _w)))

    override fun max(): Float = max(_x, max(_y, max(_z, _w)))

    override fun lerp(target: Vec4fAccessor, lerpFactor: Double): Vec4f {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec4fAccessor, lerpFactor: Double): Vec4f =
        StdVec4f().set(target).minus(this).times(lerpFactor.toFloat()).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec4f.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec4f [ ")
            .appendAligned(_x).append(", ")
            .appendAligned(_y).append(", ")
            .appendAligned(_z).append(", ")
            .appendAligned(_w)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.putFloat(_x).putFloat(_y).putFloat(_z).putFloat(_w)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(_x).put(_y).put(_z).put(_w)
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(_x.toDouble()).put(_y.toDouble()).put(_z.toDouble()).put(_w.toDouble())
    }

    override fun copy(): Vec4f = StdVec4f().set(_x, _y, _z, _w)

    override fun asImmutable(): ImmutableVec4f = StdImmutableVec4f.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + _x.hashCode()
        hashCode = 31 * hashCode + _y.hashCode()
        hashCode = 31 * hashCode + _z.hashCode()
        hashCode = 31 * hashCode + _w.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is StdVec4f -> false
            other === this -> true
            else -> this._x == other._x
                && this._y == other._y
                && this._z == other._z
                && this._w == other._w
        }
    }

    override fun toString(): String = "Vec4f [ $_x, $_y, $_z, $_w ]"

    /** Vec4f viewer, granting read-only access to its parent data. */
    private inner class Viewer : Vec4fView {
        override val x: Float = this@StdVec4f._x
        override val y: Float = this@StdVec4f._y
        override val z: Float = this@StdVec4f._y
        override val w: Float = this@StdVec4f._y

        override fun xyz(storeIn: Vec3f): Vec3f = this@StdVec4f.xyz(storeIn)
        override fun xyw(storeIn: Vec3f): Vec3f = this@StdVec4f.xyw(storeIn)
        override fun xzw(storeIn: Vec3f): Vec3f = this@StdVec4f.xzw(storeIn)
        override fun yzw(storeIn: Vec3f): Vec3f = this@StdVec4f.yzw(storeIn)

        override fun hasZeroComponent(): Boolean = this@StdVec4f.hasZeroComponent()

        override fun length(): Double = this@StdVec4f.length()
        override fun squaredLength(): Double = this@StdVec4f.squaredLength()

        override fun min(): Float = this@StdVec4f.min()
        override fun max(): Float = this@StdVec4f.max()

        override fun lerp(target: Vec4fAccessor, lerpFactor: Double): Vec4f = this@StdVec4f.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec4fAccessor, lerpFactor: Double): Vec4f =
            this@StdVec4f.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec4f.bytes()

        override fun toFormattedString(): String = this@StdVec4f.toFormattedString()
        override fun toString(): String = this@StdVec4f.toString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec4f.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec4f.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec4f.storeIn(buffer)

        override fun asImmutable(): ImmutableVec4f = StdImmutableVec4f.builder().of(this)
        override fun asMutable(): Vec4f = StdVec4f().set(this)
    }

}
