package de.colibriengine.math.vector.vec4f

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec4fPool : ObjectPool<Vec4f>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec4f()
    },
    { instance: Vec4f -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec4fPool::class.java)
    }

}
