package de.colibriengine.math.vector.vec4i

import de.colibriengine.math.vector.vec2i.ImmutableVec2i
import de.colibriengine.math.vector.vec2i.StdImmutableVec2i
import de.colibriengine.math.vector.vec3i.Vec3i

import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec4i private constructor(
    x: Int,
    y: Int,
    z: Int,
    w: Int
) : ImmutableVec4i {

    private val mutableAccessor: Vec4iAccessor = StdVec4i(x, y, z, w)

    override val x: Int = mutableAccessor.x
    override val y: Int = mutableAccessor.y
    override val z: Int = mutableAccessor.z
    override val w: Int = mutableAccessor.w

    override fun asMutable(): Vec4i = StdVec4i().set(this)

    override fun xyz(): Vec3i = mutableAccessor.xyz()
    override fun xyw(): Vec3i = mutableAccessor.xyw()
    override fun xzw(): Vec3i = mutableAccessor.xzw()
    override fun yzw(): Vec3i = mutableAccessor.yzw()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Int = mutableAccessor.min()
    override fun max(): Int = mutableAccessor.max()

    override fun lerp(target: Vec4iAccessor, lerpFactor: Int): Vec4i = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec4iAccessor, lerpFactor: Int): Vec4i = mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec4i", "ImmutableVec4i")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec4i.Builder {

        private var x: Int = 0
        private var y: Int = 0
        private var z: Int = 0
        private var w: Int = 0

        override fun of(value: Int): ImmutableVec4i = set(value).build()

        override fun of(x: Int, y: Int, z: Int, w: Int): ImmutableVec4i = set(x, y, z, w).build()

        override fun of(vec4iAccessor: Vec4iAccessor): ImmutableVec4i = set(vec4iAccessor).build()

        override fun setX(x: Int): ImmutableVec4i.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Int): ImmutableVec4i.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Int): ImmutableVec4i.Builder {
            this.z = z
            return this
        }

        override fun setW(w: Int): ImmutableVec4i.Builder {
            this.w = w
            return this
        }

        override fun set(value: Int): ImmutableVec4i.Builder {
            x = value
            y = value
            z = value
            w = value
            return this
        }

        override fun set(x: Int, y: Int, z: Int, w: Int): ImmutableVec4i.Builder {
            this.x = x
            this.y = y
            this.z = z
            this.w = w
            return this
        }

        override fun set(vec4iAccessor: Vec4iAccessor): ImmutableVec4i.Builder {
            x = vec4iAccessor.x
            y = vec4iAccessor.y
            z = vec4iAccessor.z
            w = vec4iAccessor.w
            return this
        }

        override fun build(): ImmutableVec4i = StdImmutableVec4i(x, y, z, w)
    }

    companion object {
        fun builder(): ImmutableVec4i.Builder = Builder()
    }

}

inline fun buildImmutableVec4i(builderAction: ImmutableVec4i.Builder.() -> Unit): ImmutableVec4i {
    return StdImmutableVec4i.builder().apply(builderAction).build()
}
