package de.colibriengine.math.vector.vec4i

/**
 * StdVec4iFactory.
 *
 * @since 0.0.8.0
 */
class StdVec4iFactory : Vec4iFactory {

    private val pool = StdVec4iPool()

    override fun acquire(): Vec4i = pool.acquire()

    override fun acquireDirty(): Vec4i = pool.acquireDirty()

    override fun free(vec4i: Vec4i) = pool.free(vec4i)

    override fun immutableVec4iBuilder(): ImmutableVec4i.Builder = StdImmutableVec4i.builder()

    override fun zero(): ImmutableVec4i = ZERO

    override fun one(): ImmutableVec4i = ONE

    companion object {
        val ZERO = StdImmutableVec4i.builder().of(0)
        val ONE = StdImmutableVec4i.builder().of(1)
    }

}
