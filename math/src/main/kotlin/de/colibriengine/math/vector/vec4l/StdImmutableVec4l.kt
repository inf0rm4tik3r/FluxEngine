package de.colibriengine.math.vector.vec4l

import de.colibriengine.math.vector.vec3l.Vec3l
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec4l private constructor(
    x: Long,
    y: Long,
    z: Long,
    w: Long
) : ImmutableVec4l {

    private val mutableAccessor: Vec4lAccessor = StdVec4l().set(x, y, z, w)

    override val x: Long = mutableAccessor.x
    override val y: Long = mutableAccessor.y
    override val z: Long = mutableAccessor.z
    override val w: Long = mutableAccessor.w

    override fun asMutable(): Vec4l = StdVec4l().set(this)

    override fun xyz(): Vec3l = mutableAccessor.xyz()
    override fun xyw(): Vec3l = mutableAccessor.xyw()
    override fun xzw(): Vec3l = mutableAccessor.xzw()
    override fun yzw(): Vec3l = mutableAccessor.yzw()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Long = mutableAccessor.min()
    override fun max(): Long = mutableAccessor.max()

    override fun lerp(target: Vec4lAccessor, lerpFactor: Long): Vec4l = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec4lAccessor, lerpFactor: Long): Vec4l =
        mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec4l", "ImmutableVec4l")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec4l.Builder {

        private var x: Long = 0
        private var y: Long = 0
        private var z: Long = 0
        private var w: Long = 0

        override fun of(value: Long): ImmutableVec4l = set(value).build()

        override fun of(x: Long, y: Long, z: Long, w: Long): ImmutableVec4l = set(x, y, z, w).build()

        override fun of(vec4lAccessor: Vec4lAccessor): ImmutableVec4l = set(vec4lAccessor).build()

        override fun setX(x: Long): ImmutableVec4l.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Long): ImmutableVec4l.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Long): ImmutableVec4l.Builder {
            this.z = z
            return this
        }

        override fun setW(w: Long): ImmutableVec4l.Builder {
            this.w = w
            return this
        }

        override fun set(value: Long): ImmutableVec4l.Builder {
            x = value
            y = value
            z = value
            w = value
            return this
        }

        override fun set(x: Long, y: Long, z: Long, w: Long): ImmutableVec4l.Builder {
            this.x = x
            this.y = y
            this.z = z
            this.w = w
            return this
        }

        override fun set(vec4lAccessor: Vec4lAccessor): ImmutableVec4l.Builder {
            x = vec4lAccessor.x
            y = vec4lAccessor.y
            z = vec4lAccessor.z
            w = vec4lAccessor.w
            return this
        }

        override fun build(): ImmutableVec4l = StdImmutableVec4l(x, y, z, w)
    }

    companion object {
        fun builder(): ImmutableVec4l.Builder = Builder()
    }

}

inline fun buildImmutableVec4l(builderAction: ImmutableVec4l.Builder.() -> Unit): ImmutableVec4l {
    return StdImmutableVec4l.builder().apply(builderAction).build()
}
