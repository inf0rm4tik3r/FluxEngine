package de.colibriengine.math.vector.vec4l

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec4lPool : ObjectPool<Vec4l>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec4l()
    },
    { instance: Vec4l -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec4lPool::class.java)
    }

}
