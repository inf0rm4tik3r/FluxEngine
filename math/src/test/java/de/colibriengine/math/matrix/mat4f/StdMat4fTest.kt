package de.colibriengine.math.matrix.mat4f

import de.colibriengine.math.quaternion.quaternionF.StdQuaternionF
import de.colibriengine.math.vector.vec3f.StdVec3f
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class StdMat4fTest {

    @Test
    fun initLookAtRH() {
        val m = StdMat4f().initLookAtRH(
            source = StdVec3f(1f, 1f, 1f),
            targetUp = StdVec3f(0f, 1f, 0f),
            pointOfInterest = StdVec3f(0f, 0f, 0f)
        )
        assertEquals(
            StdVec3f(-0.5773503f, -0.57735044f, -0.5773505f),
            m.asQuaternion(StdQuaternionF()).forward(StdVec3f())
        )
    }

}
