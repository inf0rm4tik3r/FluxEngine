package de.colibriengine.math.quaternion.quaternionF

import de.colibriengine.math.vector.vec3f.StdVec3f
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class StdQuaternionFTest {

    @Test
    fun forward() {
        val q = StdQuaternionF().initRotation(45f, 0f, 0f)
        val forward = q.forward(StdVec3f())
        assertEquals(StdVec3f(0.0f, 0.7071068f, 0.7071067f), forward)
    }

    @Test
    fun backward() {
        val q = StdQuaternionF().initRotation(45f, 0f, 0f)
        val forward = q.backward(StdVec3f())
        assertEquals(StdVec3f(-0.0f, -0.7071068f, -0.7071067f), forward)
    }

    @Test
    fun initLookAt() {
        val q = StdQuaternionF().initLookAt(
            StdVec3f(1f, 1f, 1f),
            StdVec3f(0f, 0f, 0f),
            StdVec3f(0f, 1f, 0f)
        )
        val forward = q.forward(StdVec3f())
        assertEquals(StdVec3f(-0.5773504f, -0.5773504f, -0.5773505f), forward)
    }

}
