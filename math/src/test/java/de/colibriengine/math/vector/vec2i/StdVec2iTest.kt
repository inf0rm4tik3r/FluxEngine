package de.colibriengine.math.vector.vec2i

import org.junit.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach

internal class StdVec2iTest {

    var vec: Vec2i = StdVec2i()

    @BeforeEach
    fun setUp() {
        vec = StdVec2i()
    }

    @Test
    fun set() {
        vec.set(5)
        assertEquals(5, vec.x)
        assertEquals(5, vec.y)
    }

}
