package de.colibriengine.objectpooling

import de.colibriengine.datastructures.stack.DequeStack
import de.colibriengine.datastructures.stack.Stack

/** Provides a pooling mechanism to reduce the creation of small objects. */
open class ObjectPool<T> : Pool<T> {

    /** The DequeStack used to hold object references. */
    val store: Stack<T>

    /** The strategy which is used (called) to reuse / reset a pooled instance. */
    final override val reuseStrategy: (T) -> T

    /** The factory which is used to create new objects whenever there are no freed objects left. */
    private val factory: () -> T

    /**
     * Constructs an empty object pool with an initial size sufficient to hold [initialCapacity] objects. If the amount
     * of released objects exceeds this value, the reserved memory gets doubled, leading into the possibility to hold
     * twice as many objects. Therefore all contained element need to be copied to the new memory
     *
     * @param initialCapacity The initial size of the underlying data structure to hold released objects.
     * @param factory The factory used to create new objects of the type T if necessary.
     * @param reuseStrategy The strategy which is used (called) to reuse / reset a pooled instance.
     */
    constructor(initialCapacity: Int, factory: () -> T, reuseStrategy: (T) -> T) {
        store = DequeStack(initialCapacity)
        this.factory = factory
        this.reuseStrategy = reuseStrategy
    }

    /**
     * Constructs an empty object pool with an initial size sufficient to hold [INITIAL_CAPACITY_DEFAULT] objects. The
     * default is `32`.
     *
     * @param factory The factory used to create new objects of T if necessary.
     * @param reuseStrategy The strategy which is used (called) to reuse / reset a pooled instance.
     */
    constructor(factory: () -> T, reuseStrategy: (T) -> T) : this(INITIAL_CAPACITY_DEFAULT, factory, reuseStrategy)

    override fun acquire(): T {
        return when {
            store.isEmpty() -> reuseStrategy(factory())
            else -> reuseStrategy(store.pop())
        }
    }

    override fun acquireDirty(): T {
        return when {
            store.isEmpty() -> factory()
            else -> store.pop()
        }
    }

    override fun free(instance: T) {
        assert(!store.containsReference(instance)) { "Object was already freed!" }
        store.push(instance)
    }

    override fun clear() {
        store.clear()
    }

    companion object {
        /** The initial capacity of the pool. */
        private const val INITIAL_CAPACITY_DEFAULT = 32
    }

}
