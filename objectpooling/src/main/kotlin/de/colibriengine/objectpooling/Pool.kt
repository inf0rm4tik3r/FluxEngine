package de.colibriengine.objectpooling

interface Pool<T> {

  /** The reuse strategy is used to reset an object of type [T] in order to be reused. */
  val reuseStrategy: (T) -> T

  /**
   * Returns an object of type [T]. It will either be retrieved from the underlying data structure that holds unused
   * objects or newly created. The objects state is defined by the reuse strategy set for this pool.
   *
   * @return Returns an element of type [T] in its **default state**.
   * @see Reusable.reset
   */
  fun acquire(): T

  /**
   * Returns an object of type T. It will either be retrieved from the underlying data structure that holds unused
   * objects or newly created. The objects state is UNDEFINED as the reuse strategy of this pool will not be taken into
   * account. **Use with caution!**
   *
   * @return Returns an element of type [T] in an **undefined state**.
   * @see Reusable.reset
   */
  fun acquireDirty(): T

  /**
   * Frees the specified object by putting it in the underlying data structure. Freed objects can be reused later. Use
   * the [acquire] method to obtain an object.
   *
   * @param instance The instance that is to be freed. **NOTE: An instance must not be mutated in any way after being
   *     freed!**
   * @throws IllegalArgumentException If the given object is already in the pool.
   * @see Reusable.reset
   */
  fun free(instance: T)

  /** Resets the pool by losing all references to stored objects. The pool can then considered to be empty. */
  fun clear()

}
