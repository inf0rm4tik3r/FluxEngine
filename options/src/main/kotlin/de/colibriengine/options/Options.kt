package de.colibriengine.options

object Options {

  const val DEBUG = false
  const val ARGUMENT_CHECKS = false
  const val DEBUG_OBJECT_CREATION = false

}
