package de.colibriengine.physics

/** Average sea level pressure in [kPa]. */
const val EARTH_AVG_SEA_LEVEL_PRESSURE = 101325

/** Molar mass of Earth's air in [kg/mol]. */
const val EARTH_AIR_MOLAR_MASS = 0.02896f

/** Earth's gravitational acceleration in [m/s^2]. */
const val EARTH_GRAVITATIONAL_ACCELERATION = 9.807f

/** Universal gas constant in [(N*m)/(mol*kg)]. */
const val EARTH_UNIVERSAL_GAS_CONSTANT = 8.3143f

/** Specific gas constant for dry air in [J/(kg*K)]. */
const val EARTH_GAS_CONSTANT_DRY_AIR = 287.05f

/** A base temperature. */
const val BASE_TEMPERATURE = 15.0f

/**
 * Refraction values for specific materials. You may use these constants as the eta parameter in the [StdVec3f.refract]
 * method.
 */
const val REFRACTION_VACUUM = 1.0000f
const val REFRACTION_AIR = 1.0003f
const val REFRACTION_ICE = 1.3100f
const val REFRACTION_WATER = 1.3330f
const val REFRACTION_GASOLINE = 1.3980f
const val REFRACTION_GLASS = 1.5500f
const val REFRACTION_SAPPHIRE = 1.7700f
const val REFRACTION_DIAMOND = 2.4190f
