package de.colibriengine.physics

import de.colibriengine.util.UnitConverter.degreeCToKelvin
import kotlin.math.pow

@Suppress("MemberVisibilityCanBePrivate")
class Planet(
    val groundLevel: Float,
    val seaLevelPressure: Int,
    val airMolarMass: Float,
    val gravitationalAcceleration: Float,
    val universalGasConstant: Float,
    val gasConstantDryAir: Float
) {

    /** Calculates the air density on this planet at [height](meters) and [temperature](degree celsius): (kg/m^3) */
    fun airDensity(height: Float, temperature: Float): Float {
        // See: [Air density calculation](https://www.brisbanehotairballooning.com.au/calculate-air-density/)
        return airPressure(height, temperature) / (gasConstantDryAir * degreeCToKelvin(temperature))
    }

    /** Calculates the air density on this planet at [groundLevel] and [temperature] in degree celsius. */
    fun airDensity(temperature: Float): Float {
        return airDensity(0f, temperature)
    }

    /** Calculates the air pressure on this planet at [height](meters) and [temperature](degree celsius): (kPa) */
    fun airPressure(height: Float, temperature: Float): Float {
        // See: [Air pressure calculation](https://www.math24.net/barometric-formula/)
        val fl: Float = -(airMolarMass * gravitationalAcceleration /
            (universalGasConstant * degreeCToKelvin(temperature))) * height
        val pow: Double = Math.E.pow(fl.toDouble())
        return (seaLevelPressure * pow).toFloat()
    }

    /** Calculates the air pressure on this planet at [groundLevel] and [temperature] in degree celsius. */
    fun airPressure(temperature: Float): Float {
        return airPressure(0f, temperature)
    }

    companion object {
        val EARTH = Planet(
            groundLevel = 0.0f,
            seaLevelPressure = EARTH_AVG_SEA_LEVEL_PRESSURE,
            airMolarMass = EARTH_AIR_MOLAR_MASS,
            gravitationalAcceleration = EARTH_GRAVITATIONAL_ACCELERATION,
            universalGasConstant = EARTH_UNIVERSAL_GAS_CONSTANT,
            gasConstantDryAir = EARTH_GAS_CONSTANT_DRY_AIR
        )
    }

}
