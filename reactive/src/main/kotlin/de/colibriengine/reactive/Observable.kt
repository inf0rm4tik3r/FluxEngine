package de.colibriengine.reactive

interface Observable<T> {

    fun subscribe(
        next: (T) -> Unit
    ): Subscription<T> {
        return subscribe(next, error = {}, complete = {})
    }

    fun subscribe(
        next: (T) -> Unit,
        error: (Throwable) -> Unit = {},
        complete: () -> Unit = {}
    ): Subscription<T>

}
