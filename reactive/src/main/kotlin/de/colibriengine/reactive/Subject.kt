package de.colibriengine.reactive

open class Subject<T> : Observable<T> {

    val subscribers: MutableList<Subscription<T>> = mutableListOf()

    private var completed: Boolean = false

    fun next(element: T) {
        if (!completed) {
            for (subscriber in subscribers) {
                subscriber.next(element)
            }
        }
    }

    fun error(throwable: Throwable) {
        if (!completed) {
            for (subscriber in subscribers) {
                subscriber.error(throwable)
            }
        }
    }

    fun complete() {
        if (!completed) {
            for (subscriber in subscribers) {
                subscriber.complete()
            }
            completed = true
        }
    }

    fun asObservable(): Observable<T> {
        return this
    }

    override fun subscribe(
        next: (T) -> Unit,
        error: (Throwable) -> Unit,
        complete: () -> Unit
    ): Subscription<T> {
        val subscription: Subscription<T> = Subscription(this, next, error, complete)
        subscribers.add(subscription)
        return subscription
    }

    fun unsubscribe(subscription: Subscription<T>) {
        subscribers.remove(subscription)
    }

}
