package de.colibriengine.renderer

import de.colibriengine.graphics.camera.Camera
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec4f.StdVec4fFactory
import de.colibriengine.math.vector.vec4f.Vec4fAccessor

interface DebugRenderer {

    fun point(
        at: Vec3fAccessor,
        color: Vec4fAccessor = StdVec4fFactory.WHITE,
        size: Float = 20f
    )

    fun line(
        from: Vec3fAccessor,
        to: Vec3fAccessor,
        color: Vec4fAccessor = StdVec4fFactory.WHITE,
        thickness: Float = 10f
    )

    fun startFrame()
    fun render(camera: Camera)

}
