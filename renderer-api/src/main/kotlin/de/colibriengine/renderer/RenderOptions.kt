package de.colibriengine.renderer

import de.colibriengine.graphics.lighting.shadow.VarianceShadowMapSettings
import de.colibriengine.math.vector.vec2i.Vec2iAccessor

interface RenderOptions {

    var useDynamicResolution: Boolean
    var sceneResolution: Vec2iAccessor
    var minSceneResolution: Vec2iAccessor
    var guiResolution: Vec2iAccessor
    var targetFramerate: Float
    var useFXAA: Boolean
    var globalVarianceShadowMapSettings: VarianceShadowMapSettings

    fun addListener(listener: RenderOptionsListener)
    fun removeListener(listener: RenderOptionsListener)

}
