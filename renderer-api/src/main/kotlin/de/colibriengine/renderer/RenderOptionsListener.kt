package de.colibriengine.renderer

import de.colibriengine.math.vector.vec2i.Vec2iAccessor

interface RenderOptionsListener {

    /**
     * Called whenever the [RenderOptionsImpl.useDynamicResolution] flag changes.
     *
     * @param usesDynamicResolution
     * Whether or not the resolution of the main/scene rendering can be dynamically adjusted based on the current
     * performance of the system.
     */
    fun rolUseDynamicResolutionChanged(usesDynamicResolution: Boolean)

    /**
     * Called whenever the [RenderOptionsImpl.sceneResolution] changes.
     *
     * @param resolution
     * The new resolution in which the scene should be rendered.
     */
    fun rolSceneResolutionChanged(resolution: Vec2iAccessor)

    /**
     * Called whenever the [RenderOptionsImpl.minSceneResolution] changes.
     *
     * @param resolution
     * The new minimum resolution in which the scene should be rendered.
     */
    fun rolMinSceneResolutionChanged(resolution: Vec2iAccessor)

    /**
     * Called whenever the [RenderOptionsImpl.guiResolution] changes.
     *
     * @param resolution
     * The new resolution in which the GUI's should be rendered.
     */
    fun rolGuiResolutionChanged(resolution: Vec2iAccessor)

    /**
     * Called whenever the [RenderOptionsImpl.targetFramerate] changes.
     *
     * @param framerate
     * The new targeted framerate.
     */
    fun rolTargetFramerateChanged(framerate: Float)

}
