package de.colibriengine.graphics.effects

import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPTextureShader
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor

class GaussianBlur7x1Shader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPTextureShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUniform: Int
    private val texelSizeUniform: Int
    private val blurScaleUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/basic/BasicPosTexPassThrough_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/fx/blur/GaussianBlur7x1_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUniform = addUniformLocation("tex")
        texelSizeUniform = addUniformLocation("texelSize")
        blurScaleUniform = addUniformLocation("blurScale")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    override fun setTexture(textureUnit: Int) {
        setUniform1i(textureUniform, textureUnit)
    }

    fun setBlurScale(onX: Float, onY: Float) {
        setUniform2f(blurScaleUniform, onX, onY)
    }

    fun setBlurScale(blurScale: Vec2fAccessor) {
        setUniform2f(blurScaleUniform, blurScale)
    }

    fun setTexelSize(width: Float, height: Float) {
        setUniform2f(texelSizeUniform, width, height)
    }

    fun setTexelSize(texelSize: Vec2fAccessor) {
        setUniform2f(texelSizeUniform, texelSize)
    }

}
