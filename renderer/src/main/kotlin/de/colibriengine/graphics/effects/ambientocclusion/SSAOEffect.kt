package de.colibriengine.graphics.effects.ambientocclusion

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.GBuffer
import de.colibriengine.graphics.camera.AbstractCamera
import de.colibriengine.graphics.camera.Camera
import de.colibriengine.graphics.fbo.buildFBOTextureRenderer
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.opengl.GLUtil
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBO
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOTextureRenderer
import de.colibriengine.graphics.opengl.texture.*
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.texture.Texture
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.math.Random
import de.colibriengine.math.vector.vec2i.StdVec2iFactory.Companion.ZERO
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec4f.StdImmutableVec4f.Companion.builder
import de.colibriengine.util.ifNotNull
import org.lwjgl.system.MemoryStack

// TODO: Move effects back to graphics-core?
// TODO: Use TextureFactory to create noise texture?
class SSAOEffect(
    shaderManager: ShaderManager,
    meshFactory: MeshFactory,
    ecs: ECS,
    private val activeCameraResolver: () -> Camera?
) {

    val ssaoFramebuffer: SSAOFramebuffer = SSAOFramebuffer()

    private val ssaoTextureGenerationShader: SSAOTextureGenerationShader = SSAOTextureGenerationShader(shaderManager)

    private val ssaoTexturePostProcessorShader: SSAOTexturePostProcessorShader =
        SSAOTexturePostProcessorShader(shaderManager)

    private val ssaoTextureApplicationShader: SSAOTextureApplicationShader = SSAOTextureApplicationShader(shaderManager)

    val ssaoTextureViewerShader: SSAOTextureViewerShader = SSAOTextureViewerShader(shaderManager)

    private val ssaoGenerationRenderer: GLFBOTextureRenderer = buildFBOTextureRenderer(meshFactory) {
        this@buildFBOTextureRenderer.ecs = ecs
        shader = ssaoTextureGenerationShader
    }

    private val ssaoPostProcessingRenderer: GLFBOTextureRenderer = buildFBOTextureRenderer(meshFactory) {
        this@buildFBOTextureRenderer.ecs = ecs
        shader = ssaoTexturePostProcessorShader
    }

    private val ssaoApplicationRenderer: GLFBOTextureRenderer = buildFBOTextureRenderer(meshFactory) {
        this@buildFBOTextureRenderer.ecs = ecs
        shader = ssaoTextureApplicationShader
    }

    private val noiseTexture: OpenGlTexture = OpenGlTexture()
    private val settings: SSAOEffectSettings = SSAOEffectSettings()
    private val random: Random = Random()

    init {
        initGenerationShader()
    }

    private fun initFramebuffer(resolution: Vec2iAccessor) {
        ssaoFramebuffer.resolution = resolution
        ssaoFramebuffer.clearColor = builder().of(0f, 0f, 1f, 1f) // BLUE
        ssaoFramebuffer.init()
    }

    private fun readyFramebuffer() {
        assert(ssaoFramebuffer.isCreated) { "The framebuffer must be created!" }
        ssaoFramebuffer.bindForDrawing()
        ssaoFramebuffer.clearAttachments()
        ssaoFramebuffer.clearDepthAndStencil()
    }

    /**
     * Creates / initializes the noise texture used to render the SSAO texture.
     * The dimensions of the created texture depend on the value specified in
     * [SSAOEffectSettings.getNoiseTextureDimensions] stored in [SSAOEffect.getSettings].
     * NOTE: Calling this method will recreate the texture on the GPU! Only call when necessary.
     */
    private fun createNoiseTexture() {
        val noise = SSAOEffectHelper.computeNoise(random, settings.noiseTextureDimensions)
        MemoryStack.stackPush().use { stack ->
            val noiseData = stack.malloc(
                noise.size * Vec3f.FLOATS * java.lang.Float.BYTES
            ).rewind()
            for (vec3f in noise) {
                vec3f.storeIn(noiseData)
            }
            noiseData.rewind()

            // TODO: Create through OpenGLTextureFactory
            noiseTexture.create() // Will issue a call to release() if necessary (already created).
            noiseTexture.levels = 1
            noiseTexture.setDimensions(settings.noiseTextureDimensions)
            noiseTexture.format = GLImageFormat.GL_RGB16F
            noiseTexture.createDataStore()
            noiseTexture.fill(1, GLTexelDataFormat.GL_RGB, GLTexelDataType.GL_FLOAT, noiseData)
            noiseTexture.setMinFilterType(GLTextureFilterType.GL_NEAREST)
            noiseTexture.setMagFilterType(GLTextureFilterType.GL_NEAREST)
            noiseTexture.setWrapType(GLTextureWrapType.GL_REPEAT)
            noiseTexture.finish()
        }
    }

    private fun initGenerationShader() {
        ssaoTextureGenerationShader.setKernel(SSAOEffectHelper.computeKernel(random, settings.kernelSize))
        ssaoTextureGenerationShader.setSampleRadius(settings.sampleRadius)
        ssaoTextureGenerationShader.setSamplePosDepthBias(settings.samplePosDepthBias)
        ssaoTextureGenerationShader.setPower(settings.power)
    }

    private fun setUpGenerationShader(gBuffer: GBuffer) {
        assert(ssaoFramebuffer.isCreated) { "SSAO framebuffer is not created!" }
        assert(noiseTexture.isCreated) { "SSAO noise texture is not created!" }
        ssaoTextureGenerationShader.setNoiseTextureCoordScale(
            ssaoFramebuffer.resolution.x.toFloat() / noiseTexture.dimensions.x.toFloat(),
            ssaoFramebuffer.resolution.y.toFloat() / noiseTexture.dimensions.y.toFloat()
        )
        ssaoTextureGenerationShader.setScreenSize(ssaoFramebuffer.resolution)

        // Bind the textures which are necessary for the generation process.
        gBuffer.depthTexture.bind(0)
        gBuffer.normalTexture.bind(1)
        noiseTexture.bind(2)

        // And tell the shader where to find them.
        ssaoTextureGenerationShader.setDepthTextureUnit(0)
        ssaoTextureGenerationShader.setNormalTextureUnit(1)
        ssaoTextureGenerationShader.setNoiseTextureUnit(2)
    }

    private fun setUpSSAORenderer(activeCamera: Camera) {
        ssaoGenerationRenderer.updateDestFBO(ssaoFramebuffer, ssaoFramebuffer.mainTextureIndex)
        ssaoGenerationRenderer.updateDestWritePosAndDimension(ZERO, ssaoFramebuffer.resolution)
        // TODO: Avoid object creation by not using a lambda here...
        ssaoGenerationRenderer.shaderSetup = {
            val shader = it.shader as SSAOTextureGenerationShader
            shader.bind()
            shader.setMVPMatrix(it.getQuadOrthographicMVP())
            shader.setViewMatrix(activeCamera.viewMatrix)
            shader.setProjectionMatrix(activeCamera.perspectiveProjectionMatrix)
            shader.setInverseProjectionMatrix(activeCamera.inversePerspectiveProjectionMatrix)
        }
    }

    private fun applyPostProcessing(centeredCamera: Camera) {
        GLUtil.disableDepthTest()
        GLUtil.disableDepthMask()
        ssaoPostProcessingRenderer.updateSrcFBO(ssaoFramebuffer, ssaoFramebuffer.mainTextureIndex)
        ssaoPostProcessingRenderer.updateDestFBO(ssaoFramebuffer, ssaoFramebuffer.postProcessedTextureIndex)
        ssaoPostProcessingRenderer.updateDestWritePosAndDimension(ZERO, ssaoFramebuffer.resolution)
        ssaoPostProcessingRenderer.shaderSetup = {
            val shader = it.shader as SSAOTexturePostProcessorShader
            shader.bind()
            shader.setMVPMatrix(it.getQuadOrthographicMVP())
            shader.setTexture(ssaoFramebuffer.mainTextureIndex)
            shader.setTexelSize(
                1.0f / ssaoFramebuffer.resolution.x,
                1.0f / ssaoFramebuffer.resolution.y
            )
            shader.setNoiseTextureDimension(settings.noiseTextureDimensions)
        }
        ssaoPostProcessingRenderer.render(centeredCamera)
        GLUtil.enableDepthMask()
        GLUtil.enableDepthTest()
    }

    fun generate(
        resolution: Vec2iAccessor,
        gBuffer: GBuffer,
        centeredCam: AbstractCamera
    ) {
        if (!ssaoFramebuffer.isCreated) {
            initFramebuffer(resolution)
            LOG.info("Initializing the SSAO framebuffer.")
        }
        if (!noiseTexture.isCreated) {
            createNoiseTexture()
            LOG.info("Creating the SSAO noise texture.")
        }
        readyFramebuffer()
        setUpGenerationShader(gBuffer)
        ifNotNull(activeCameraResolver.invoke()) { activeCamera: Camera ->
            setUpSSAORenderer(activeCamera)
            ssaoGenerationRenderer.render(centeredCam)
            applyPostProcessing(centeredCam)
        }
    }

    /**
     * @param storeInFBO             The framebuffer in which the `destFBOTextureIndex` exists.
     * @param storeInFBOTextureIndex The index of the texture in framebuffer `destFBO` to which the SSAO effect will be applied.
     */
    fun apply(
        from: Texture,
        storeInFBO: GLFBO, storeInFBOTextureIndex: Int,
        position: Vec2iAccessor,
        dimension: Vec2iAccessor,
        centeredCam: AbstractCamera
    ) {
        GLUtil.disableDepthTest() // TODO: necessary?
        GLUtil.disableDepthMask()
        from.bind(3) // TODO: remove constant

        // Set up the application renderer.
        ssaoApplicationRenderer.updateSrcFBO(ssaoFramebuffer, ssaoFramebuffer.postProcessedTextureIndex)
        ssaoApplicationRenderer.updateDestFBO(storeInFBO, storeInFBOTextureIndex)
        ssaoApplicationRenderer.updateDestWritePosAndDimension(position, dimension)
        ssaoApplicationRenderer.shaderSetup = { `object`: GLFBOTextureRenderer ->
            val shader = `object`.shader as SSAOTextureApplicationShader
            shader.bind()
            shader.setMVPMatrix(`object`.getQuadOrthographicMVP())
            shader.setSSAOTextureUnit(ssaoFramebuffer.postProcessedTextureIndex)
            shader.setSourceTextureUnit(3)
            Unit
        }

        // Execute the renderer.
        ssaoApplicationRenderer.render(centeredCam)
        GLUtil.enableDepthMask()
        GLUtil.enableDepthTest()
    }

    companion object {
        private val LOG = getLogger(SSAOEffect::class.java)
    }

}
