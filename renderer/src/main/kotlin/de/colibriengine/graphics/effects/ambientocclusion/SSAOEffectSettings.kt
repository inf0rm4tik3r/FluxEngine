package de.colibriengine.graphics.effects.ambientocclusion

class SSAOEffectSettings {

    var kernelSize = DEFAULT_KERNEL_SIZE
    var sampleRadius = DEFAULT_SAMPLE_RADIUS
    var samplePosDepthBias = DEFAULT_SAMPLE_POS_DEPTH_BIAS
    var power = DEFAULT_POWER
    var noiseTextureDimensions = DEFAULT_NOISE_TEXTURE_DIMENSIONS

    companion object {
        /** This value must always be changed in combination with the constant defined in SSAOTextureGenerator_fs.glsl!  */
        const val DEFAULT_KERNEL_SIZE = 16

        const val DEFAULT_SAMPLE_RADIUS = 0.5f

        const val DEFAULT_SAMPLE_POS_DEPTH_BIAS = 0.025f

        const val DEFAULT_POWER = 2.0f // Could be 1.0...

        /** If this value is not 4, the blur in SSAOTexturePostProcessor_fs.glsl will be off!  */
        const val DEFAULT_NOISE_TEXTURE_DIMENSIONS = 4
    }

}
