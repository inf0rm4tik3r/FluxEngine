package de.colibriengine.graphics.effects.ambientocclusion

import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPTextureShader
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor

class SSAOTexturePostProcessorShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPTextureShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUnitUniform: Int
    private val texelSizeUniform: Int
    private val noiseTextureDimensionUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/basic/BasicPosTexPassThrough_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/fx/ambientocclusion/SSAOTexturePostProcessor_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUnitUniform = addUniformLocation("tex")
        texelSizeUniform = addUniformLocation("texelSize")
        noiseTextureDimensionUniform = addUniformLocation("noiseTextureDimension")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    override fun setTexture(textureUnit: Int) {
        setUniform1i(textureUnitUniform, textureUnit)
    }

    fun setTexelSize(texelSizeOnX: Float, texelSizeOnY: Float) {
        setUniform2f(texelSizeUniform, texelSizeOnX, texelSizeOnY)
    }

    fun setTexelSize(texelSize: Vec2fAccessor) {
        setTexelSize(texelSize.x, texelSize.y)
    }

    fun setNoiseTextureDimension(noiseTextureDimension: Int) {
        setUniform1i(noiseTextureDimensionUniform, noiseTextureDimension)
    }

}
