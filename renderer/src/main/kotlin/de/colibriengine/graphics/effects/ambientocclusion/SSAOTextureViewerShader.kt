package de.colibriengine.graphics.effects.ambientocclusion

import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPTextureShader
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

class SSAOTextureViewerShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPTextureShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUnitUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/basic/BasicPosTexPassThrough_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/fx/ambientocclusion/SSAOTextureViewer_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUnitUniform = addUniformLocation("tex")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    override fun setTexture(textureUnit: Int) {
        setUniform1i(textureUnitUniform, textureUnit)
    }

}
