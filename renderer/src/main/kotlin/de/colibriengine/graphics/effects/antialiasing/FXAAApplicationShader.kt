package de.colibriengine.graphics.effects.antialiasing

import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec2f.StdVec2f

class FXAAApplicationShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUniform: Int
    private val screenSizeUniform: Int
    private val invScreenSizeUniform: Int

    init {
        attachVertexShader(
            shaderManager.requestShaderSource(
                "shaders/fx/passthrough_vs.glsl"
            )
        )
        attachFragmentShader(
            shaderManager.requestShaderSource(
                "shaders/fx/antialiasing/FXAAApplicator_fs.glsl"
            )
        )
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUniform = addUniformLocation("tex")
        screenSizeUniform = addUniformLocation("screenSize")
        invScreenSizeUniform = addUniformLocation("invScreenSize")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    fun setTexture(texture: Int) {
        setUniform1i(textureUniform, texture)
    }

    fun setScreenSize(screenSize: StdVec2f) {
        setUniform2f(screenSizeUniform, screenSize)
    }

    fun setScreenSize(width: Float, height: Float) {
        setUniform2f(screenSizeUniform, width, height)
    }

    fun setInvScreenSize(invScreenSize: StdVec2f) {
        setUniform2f(invScreenSizeUniform, invScreenSize)
    }

    fun setOffsets(invWidth: Float, invHeight: Float) {
        setUniform2f(invScreenSizeUniform, invWidth, invHeight)
    }

}
