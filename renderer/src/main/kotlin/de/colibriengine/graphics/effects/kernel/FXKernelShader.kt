package de.colibriengine.graphics.effects.kernel

import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.matrix.mat3f.Kernel3f
import de.colibriengine.math.vector.vec2f.StdVec2f

class FXKernelShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUniform: Int
    private val kernelUniform: Int
    private val offsetsUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/fx/passthrough_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/fx/kernel3f_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUniform = addUniformLocation("tex")
        kernelUniform = addUniformLocation("kernel")
        offsetsUniform = addUniformLocation("offsets")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    fun setTexture(texture: Int) {
        setUniform1i(textureUniform, texture)
    }

    fun setKernelMatrix(kernelMatrix: Kernel3f) {
        setUniformM3f(kernelUniform, kernelMatrix.mat3f)
    }

    fun setOffsets(offsets: StdVec2f) {
        setUniform2f(offsetsUniform, offsets)
    }

    fun setOffsets(xOffset: Float, yOffset: Float) {
        setUniform2f(offsetsUniform, xOffset, yOffset)
    }

}
