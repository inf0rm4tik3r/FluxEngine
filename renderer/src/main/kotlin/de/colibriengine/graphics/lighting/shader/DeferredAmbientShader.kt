package de.colibriengine.graphics.lighting.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

class DeferredAmbientShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPShader {

    /* UNIFORM LOCATIONS */
    private val mvpMatrixUniform: Int
    private val colorTextureUnitUniform: Int
    private val ambientStrengthUniform: Int
    private val screenSizeUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/deferredRendering/deferredAmbient_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/deferredRendering/deferredAmbient_fs.glsl"))
        linkProgram()

        //VS Uniforms
        mvpMatrixUniform = addUniformLocation("mvpMatrix")

        //FS Uniforms
        colorTextureUnitUniform = addUniformLocation("colorTexture")
        ambientStrengthUniform = addUniformLocation("ambientStrength")
        screenSizeUniform = addUniformLocation("screenSize")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    fun setColorTextureUnit(colorTextureUnit: Int) {
        setUniform1i(colorTextureUnitUniform, colorTextureUnit)
    }

    fun setAmbientStrength(ambientStrength: Vec3fAccessor) {
        setUniform3f(ambientStrengthUniform, ambientStrength)
    }

    fun setScreenSize(screenSize: Vec2iAccessor) {
        setUniform2i(screenSizeUniform, screenSize)
    }

}
