package de.colibriengine.graphics.lighting.shader

import de.colibriengine.graphics.lighting.light.AbstractLight
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

interface DeferredLightingShader : MVPShader {

    fun setDiffuseTextureUnit(diffuseTextureUnit: Int)
    fun setNormalTextureUnit(normalTextureUnit: Int)
    fun setSpecularTextureUnit(specularTextureUnit: Int)
    fun setDepthTextureUnit(depthTextureUnit: Int)
    fun setScreenSize(screenSize: Vec2iAccessor)
    fun setCameraPosition(cameraPosition: Vec3fAccessor)
    fun setInverseVP(inverseVP: Mat4fAccessor)
    fun setBaseLight(baseLight: AbstractLight)

}
