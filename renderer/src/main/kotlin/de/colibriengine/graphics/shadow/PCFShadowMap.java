package de.colibriengine.graphics.shadow;

import de.colibriengine.graphics.texture.ImageFormat;
import de.colibriengine.logging.LogUtil;
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOAttachment;
import de.colibriengine.graphics.opengl.buffer.fbo.FixedResolutionFBO;

import org.apache.logging.log4j.Logger;

public class PCFShadowMap extends FixedResolutionFBO {
    
    private static final Logger LOG = LogUtil.getLogger(PCFShadowMap.class);
    
    /**
     * Constructs a new {@link PCFShadowMap} without initializing it (no OpenGL initialization!).
     * The {@link PCFShadowMap#setResolution(int, int)} method must be called manually.
     * The {@link PCFShadowMap#init()} function must be called manually.
     */
    public PCFShadowMap() {
        super();
    }
    
    /**
     * Constructs and initializes a new {@link PCFShadowMap}.
     * Manually calling {@link GBuffer#init()} after this is not necessary.
     *
     * @param textureWidth
     *     The width of the FBO textures.
     * @param textureHeight
     *     The height of the FBO textures.
     */
    public PCFShadowMap(final int textureWidth, final int textureHeight) {
        super(textureWidth, textureHeight);
        
        // Initialize this ShadowMap instance for the first time.
        init();
    }
    
    /**
     * Initializes this {@code GBuffer} instance.
     * Each created instance is automatically initialized upon creation.
     * This method must be called manually after this instance got invalidated. For example through a call to the
     * {@link GBuffer#setXResolution(int)} method.
     */
    @Override
    public void init() {
        makeValid();
        ensureResolutionIsSet();
        
        // The underlying FBO must be destroyed first if it is already created (was previously initialized).
        if (super.isCreated()) {
            super.destroy();
        }
        
        // Initialize the underlying frame buffer object.
        super.create();
        
        // Add depth texture. (Allows lighting calculations)
        super.initDepthTexture(ImageFormat.DEPTH_COMPONENT32, GLFBOAttachment.GL_DEPTH_ATTACHMENT);
        
        setDrawBufferToNONE();
        setReadBufferToNONE();
        
        // Create the FBO.
        super.compile();
        
        // The creation of the underlying FBO might have left us with that FBO being bound to the context.
        // We have to make sure that the default FBO is bound.
        bindDefaultFBO();
    }
    
}
