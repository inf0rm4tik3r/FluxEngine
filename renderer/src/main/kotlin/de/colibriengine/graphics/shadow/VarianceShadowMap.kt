package de.colibriengine.graphics.shadow

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.camera.AbstractCamera
import de.colibriengine.graphics.effects.GaussianBlur7x1Shader
import de.colibriengine.graphics.fbo.buildFBOTextureRenderer
import de.colibriengine.graphics.lighting.shadow.VarianceShadowMapSettings
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.opengl.buffer.fbo.FixedResolutionFBO
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOAttachment
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOTextureRenderer
import de.colibriengine.graphics.opengl.texture.GLTexelDataFormat
import de.colibriengine.graphics.opengl.texture.GLTexelDataType
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.texture.ImageFormat
import de.colibriengine.graphics.texture.TextureFilterType
import de.colibriengine.graphics.texture.TextureWrapType
import de.colibriengine.math.vector.vec2i.StdVec2iFactory.Companion.ZERO
import de.colibriengine.math.vector.vec4f.StdVec4fFactory.Companion.WHITE

class VarianceShadowMap : FixedResolutionFBO {

    private val varianceShadowMapRenderer: GLFBOTextureRenderer

    var settings: VarianceShadowMapSettings? = null

    val mainTextureIndex: Int
        get() = 0

    val tempTextureIndex: Int
        get() = 1

    /**
     * Constructs a new [VarianceShadowMap] without initializing it (no OpenGL initialization!).
     * The [VarianceShadowMap.setResolution] method must be called manually.
     * The [VarianceShadowMap.init] function must be called manually.
     */
    constructor(ecs: ECS, shaderManager: ShaderManager, meshFactory: MeshFactory) : super() {
        varianceShadowMapRenderer = buildFBOTextureRenderer(meshFactory) {
            this.ecs = ecs
            shader = VarianceShadowMapPostProcessingShader(shaderManager)
        }
    }

    /**
     * Constructs and initializes a new [VarianceShadowMap].
     * Manually calling [GBuffer.init] after this is not necessary.
     *
     * @param textureWidth  The width of the FBO textures.
     * @param textureHeight The height of the FBO textures.
     */
    constructor(
        ecs: ECS, shaderManager: ShaderManager, meshFactory: MeshFactory,
        textureWidth: Int, textureHeight: Int
    ) : super(textureWidth, textureHeight) {
        varianceShadowMapRenderer = buildFBOTextureRenderer(meshFactory) {
            this.ecs = ecs
            shader = GaussianBlur7x1Shader(shaderManager)
        }

        // Initialize this ShadowMap instance for the first time.
        init()
    }

    /**
     * Initializes this `GBuffer` instance.
     * Each created instance is automatically initialized upon creation.
     * This method must be called manually after this instance got invalidated. For example through a call to the
     * [GBuffer.setXResolution] method.
     */
    public override fun init() {
        makeValid()
        ensureResolutionIsSet()

        // The underlying FBO must be destroyed first if it is already created (was previously initialized).
        if (super.isCreated()) {
            super.destroy()
        }

        // Initialize the underlying frame buffer object.
        super.create()

        // Texture 0: Main
        super.addTexture(
            resolution.x, resolution.y,
            ImageFormat.RGB32F, GLTexelDataFormat.GL_RGB,
            GLTexelDataType.GL_FLOAT,
            TextureFilterType.LINEAR, TextureFilterType.LINEAR,
            TextureWrapType.CLAMP_TO_BORDER, WHITE
        )

        // Texture 1: Temp
        super.addTexture(
            resolution.x, resolution.y,
            ImageFormat.RGB32F, GLTexelDataFormat.GL_RGB,
            GLTexelDataType.GL_FLOAT,
            TextureFilterType.LINEAR, TextureFilterType.LINEAR,
            TextureWrapType.CLAMP_TO_BORDER, WHITE
        )

        // Add depth texture. (Allows lighting calculations)
        super.initDepthTexture(ImageFormat.DEPTH_COMPONENT32, GLFBOAttachment.GL_DEPTH_ATTACHMENT)
        setDrawBufferToNONE()
        setReadBufferToNONE()

        // Create the FBO.
        super.compile()

        // The creation of the underlying FBO might have left us with that FBO being bound to the context.
        // We have to make sure that the default FBO is bound.
        bindDefaultFBO()
    }

    private fun setUpVarianceShadowMapRendererShader(textureUnit: Int, onX: Float, onY: Float) {
        varianceShadowMapRenderer.shaderSetup = { `object`: GLFBOTextureRenderer ->
            val shader = `object`.shader as VarianceShadowMapPostProcessingShader
            shader.bind()
            shader.setMVPMatrix(`object`.getQuadOrthographicMVP())
            shader.setTexture(textureUnit)
            shader.setTexelSize(1.0f / resolution.x, 1.0f / resolution.y)
            shader.setLookUpScale(onX, onY)
        }
    }

    private fun postProcessMainToTemp(
        lookUpScaleOnX: Float, lookUpScaleOnY: Float,
        byUsingCamera: AbstractCamera
    ) {
        varianceShadowMapRenderer.updateSrcFBO(this, mainTextureIndex)
        varianceShadowMapRenderer.updateDestFBO(this, tempTextureIndex)
        varianceShadowMapRenderer.updateDestWritePosAndDimension(ZERO, resolution)
        setUpVarianceShadowMapRendererShader(mainTextureIndex, lookUpScaleOnX, lookUpScaleOnY)
        varianceShadowMapRenderer.render(byUsingCamera)
    }

    private fun postProcessTempToMain(
        lookUpScaleOnX: Float, lookUpScaleOnY: Float,
        byUsingCamera: AbstractCamera
    ) {
        varianceShadowMapRenderer.updateSrcFBO(this, tempTextureIndex)
        varianceShadowMapRenderer.updateDestFBO(this, mainTextureIndex)
        varianceShadowMapRenderer.updateDestWritePosAndDimension(ZERO, resolution)
        setUpVarianceShadowMapRendererShader(tempTextureIndex, lookUpScaleOnX, lookUpScaleOnY)
        varianceShadowMapRenderer.render(byUsingCamera)
    }

    fun applyPostProcessing(
        lookUpScaleOnX: Float, lookUpScaleOnY: Float,
        byUsingCamera: AbstractCamera
    ) {
        postProcessMainToTemp(lookUpScaleOnX, 0.0f, byUsingCamera)
        postProcessTempToMain(0.0f, lookUpScaleOnY, byUsingCamera)
    }

}
