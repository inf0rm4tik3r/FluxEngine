package de.colibriengine.graphics.shadow

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPTextureShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec2f.Vec2fAccessor

class VarianceShadowMapPostProcessingShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPTextureShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUniform: Int
    private val texelSizeUniform: Int
    private val lookUpScaleUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/basic/BasicPosTexPassThrough_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/shadows/VarianceShadowMapPostProcessing_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUniform = addUniformLocation("tex")
        texelSizeUniform = addUniformLocation("texelSize")
        lookUpScaleUniform = addUniformLocation("lookUpScale")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    override fun setTexture(textureUnit: Int) {
        setUniform1i(textureUniform, textureUnit)
    }

    fun setLookUpScale(onX: Float, onY: Float) {
        setUniform2f(lookUpScaleUniform, onX, onY)
    }

    fun setLookUpScale(lookUpScale: Vec2fAccessor) {
        setUniform2f(lookUpScaleUniform, lookUpScale)
    }

    fun setTexelSize(width: Float, height: Float) {
        setUniform2f(texelSizeUniform, width, height)
    }

    fun setTexelSize(texelSize: Vec2fAccessor) {
        setUniform2f(texelSizeUniform, texelSize)
    }

}
