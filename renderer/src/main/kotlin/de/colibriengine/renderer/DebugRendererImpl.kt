package de.colibriengine.renderer

import de.colibriengine.graphics.camera.Camera
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.renderer.debug.DebugLineRenderer
import de.colibriengine.renderer.debug.DebugPointRenderer
import de.colibriengine.renderer.shader.DebugLineMeshShader
import de.colibriengine.renderer.shader.DebugPointMeshShader

class DebugRendererImpl(shaderManager: ShaderManager) : DebugRenderer {

    private val pointRenderer = DebugPointRenderer()
    private val lineRenderer = DebugLineRenderer()

    private val debugPointMeshShader = DebugPointMeshShader(shaderManager)
    private val debugLineMeshShader = DebugLineMeshShader(shaderManager)

    override fun point(at: Vec3fAccessor, color: Vec4fAccessor, size: Float) {
        pointRenderer.drawPoint(at, color, size)
    }

    override fun line(from: Vec3fAccessor, to: Vec3fAccessor, color: Vec4fAccessor, thickness: Float) {
        lineRenderer.drawLine(from, to, color, thickness)
    }

    override fun startFrame() {
        pointRenderer.startFrame()
        lineRenderer.startFrame()
    }

    override fun render(camera: Camera) {
        // Draw points.
        debugPointMeshShader.bind()
        debugPointMeshShader.setMVPMatrix(camera.perspectiveViewProjectionMatrix)
        pointRenderer.renderMesh()

        // Draw lines.
        debugLineMeshShader.bind()
        debugLineMeshShader.setMVPMatrix(camera.perspectiveViewProjectionMatrix)
        lineRenderer.renderMesh()
    }

}
