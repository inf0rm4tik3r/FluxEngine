package de.colibriengine.renderer

import de.colibriengine.graphics.camera.Camera
import de.colibriengine.graphics.lighting.light.DirectionalLightImpl
import de.colibriengine.graphics.lighting.light.PointLightImpl
import de.colibriengine.graphics.lighting.light.SpotLightImpl
import de.colibriengine.graphics.lighting.shader.*
import de.colibriengine.graphics.opengl.GLUtil
import de.colibriengine.graphics.opengl.rendering.GLBlendFactor
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.graphics.shadow.VarianceShadowMap
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3f.ImmutableVec3f
import de.colibriengine.math.vector.vec3f.StdImmutableVec3f.Companion.builder
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.scene.graph.SceneGraphNode

// TODO: Do not use *LightImpl. Use interfaces instead.
class LightingEngine(private val renderer: RenderingEngineImpl) { // TODO: Use RenderingEngine interface?

    var ambientStrength: Vec3f = StdVec3f().set(DEFAULT_AMBIENT_STRENGTH)
        private set
    private val ambientStrengthBackup: Vec3f = StdVec3f()

    val directionalLights: MutableList<DirectionalLightImpl> = ArrayList(LIGHT_LIST_INITIAL_CAPACITY)
    val pointLights: MutableList<PointLightImpl> = ArrayList(LIGHT_LIST_INITIAL_CAPACITY)
    val spotLights: MutableList<SpotLightImpl> = ArrayList(LIGHT_LIST_INITIAL_CAPACITY)

    val shadowMaps: MutableMap<DirectionalLightImpl, VarianceShadowMap> = mutableMapOf()

    private val deferredAmbientShader: DeferredAmbientShader
    private val deferredDirectionalShader: DeferredDirectionalShader
    private val deferredPointShader: DeferredPointShader
    private val deferredSpotShader: DeferredSpotShader

    init {
        val shaderManager = renderer.shaderManager
        deferredAmbientShader = DeferredAmbientShader(shaderManager)
        deferredDirectionalShader =
            DeferredDirectionalShader(shaderManager)
        deferredPointShader = DeferredPointShader(shaderManager)
        deferredSpotShader = DeferredSpotShader(shaderManager)
    }

    fun destroy() {
        deferredAmbientShader.release()
        deferredDirectionalShader.release()
        deferredPointShader.release()
        deferredSpotShader.release()
    }

    fun computeLightingInformation(root: AbstractSceneGraphNode) {
        clearLightLists()
        extractLights(root)
    }

    private fun clearLightLists() {
        directionalLights.clear()
        pointLights.clear()
        spotLights.clear()
    }

    private fun extractLights(root: SceneGraphNode) {
        val childNodeIterator = root.childNodeIterator
        while (childNodeIterator.hasNext()) {
            val node = childNodeIterator.next()
            if (node is DirectionalLightImpl) {
                directionalLights.add(node)
            } else if (node is PointLightImpl) {
                pointLights.add(node)
            } else if (node is SpotLightImpl) {
                spotLights.add(node)
            }
            extractLights(node)
        }
    }

    fun doLighting() {
        prepareLighting()
        computeLighting()
        finishLighting()
    }

    private fun prepareLighting() {
        val gBuffer = renderer.gBuffer
        val gBufferInternalRenderer = renderer.gBufferInternalRenderer
        gBufferInternalRenderer.updateSrcFBO(gBuffer) {
            add(gBuffer.albedoTextureIndex)
            add(gBuffer.normalTextureIndex)
            add(gBuffer.specularTextureIndex)
        }
        gBufferInternalRenderer.updateDestFBO(gBuffer, gBuffer.pp2TextureIndex)
        gBufferInternalRenderer.shaderSetup = {
            val shader = it.shader as MVPShader
            shader.bind()
            shader.setMVPMatrix(it.getQuadOrthographicMVP())
        }
        renderer.gBuffer.bindDepthTexture() // TODO: move into FBOTextureRenderer
        GLUtil.disableDepthMask() // Don't write depth values.
        GLUtil.disableDepthTest() // Don't compare depth values.
    }

    private fun computeLighting() {
        ambientPass()
        renderer.windowManager.activeWindow?.cameraManager?.activeCamera?.let { camera: Camera ->
            val gBuffer = renderer.gBuffer
            val renderResolution = gBuffer.resolution
            val cameraPosition = camera.position
            val inverseVP = camera.inversePerspectiveViewProjectionMatrix
            GLUtil.enableBlending(GLBlendFactor.GL_ONE, GLBlendFactor.GL_ONE)
            directionalLightPasses(renderResolution, cameraPosition, inverseVP)
            pointLightPasses(renderResolution, cameraPosition, inverseVP)
            GLUtil.disableBlending()
        }
        spotLightPasses()
    }

    private fun ambientPass() {
        deferredAmbientShader.bind()
        deferredAmbientShader.setAmbientStrength(ambientStrength)
        deferredAmbientShader.setScreenSize(renderer.renderOptions.sceneResolution)
        deferredAmbientShader.setColorTextureUnit(renderer.gBuffer.albedoTextureIndex)
        renderer.gBufferInternalRenderer.shader = deferredAmbientShader
        renderer.gBufferInternalRenderer.render(renderer.centeredCam)
    }

    private fun setUpDeferredLightingShader(
        shader: DeferredLightingShader,
        screenSize: Vec2iAccessor,
        cameraPosition: Vec3fAccessor,
        inverseVP: Mat4fAccessor
    ) {
        val gBuffer = renderer.gBuffer
        shader.setDiffuseTextureUnit(gBuffer.albedoTextureIndex)
        shader.setNormalTextureUnit(gBuffer.normalTextureIndex)
        shader.setSpecularTextureUnit(gBuffer.specularTextureIndex)
        shader.setDepthTextureUnit(gBuffer.depthTextureIndex)
        shader.setScreenSize(screenSize)
        shader.setCameraPosition(cameraPosition)
        shader.setInverseVP(inverseVP)
    }

    private fun directionalLightPasses(
        renderResolution: Vec2iAccessor,
        cameraPosition: Vec3fAccessor,
        inversePerspectiveCameraProjection: Mat4fAccessor
    ) {
        deferredDirectionalShader.bind()
        setUpDeferredLightingShader(
            deferredDirectionalShader, renderResolution, cameraPosition, inversePerspectiveCameraProjection
        )
        for (directionalLight in directionalLights) {
            // General directional light setup...
            deferredDirectionalShader.setDirectionalLight(directionalLight)

            // Shadow map specific setup...
            if (directionalLight.castsShadows) {
                val varianceShadowMap = getShadowMapFor(directionalLight)
                val settings = if (varianceShadowMap.settings == null)
                    renderer.renderOptions.globalVarianceShadowMapSettings
                else
                    varianceShadowMap.settings!!
                deferredDirectionalShader.setLightSpaceMatrix(
                    renderer.shadowMapCamera.orthographicViewProjectionMatrix
                )
                varianceShadowMap.textures[0].bind(10) // TODO: do not bind at fixed index.
                deferredDirectionalShader.setShadowMap(10 /*renderer.shadowMap.getDepthTextureIndex()*/)
                deferredDirectionalShader.setEarlyReturnReferenceDepthBias(settings.earlyReturnReferenceDepthBias)
                deferredDirectionalShader.setVarianceMaxBound(settings.varianceMaxBound)
                deferredDirectionalShader.setProbabilityMinBound(settings.probabilityMinBound)
            }
            renderer.gBufferInternalRenderer.shader = deferredDirectionalShader
            renderer.gBufferInternalRenderer.render(renderer.centeredCam)
        }
    }

    private fun pointLightPasses(
        renderResolution: Vec2iAccessor,
        cameraPosition: Vec3fAccessor,
        inversePerspectiveCameraProjection: Mat4fAccessor
    ) {
        deferredPointShader.bind()
        setUpDeferredLightingShader(
            deferredPointShader, renderResolution, cameraPosition, inversePerspectiveCameraProjection
        )
        for (pointLight in pointLights) {
            deferredPointShader.setPointLight(pointLight)
            renderer.gBufferInternalRenderer.shader = deferredPointShader
            renderer.gBufferInternalRenderer.render(renderer.centeredCam)
        }
    }

    private fun spotLightPasses() {}

    private fun finishLighting() {
        GLUtil.enableDepthMask()
        GLUtil.enableDepthTest()
    }

    fun getShadowMapFor(directionalLight: DirectionalLightImpl): VarianceShadowMap {
        return shadowMaps.getOrPut(directionalLight) {
            VarianceShadowMap(renderer.internalEcs, renderer.shaderManager, renderer.modelFactory.meshFactory)
        }
        // TODO: Remove entries from this map when lights die!!
    }

    fun backupAmbientLight() {
        ambientStrengthBackup.set(ambientStrength)
    }

    fun restoreAmbientLight() {
        ambientStrength.set(ambientStrengthBackup)
    }

    fun setAmbientStrength(ambientStrength: StdVec3f) {
        this.ambientStrength = ambientStrength
    }

    fun setAmbientLight(x: Float, y: Float, z: Float) {
        ambientStrength.set(x, y, z)
    }

    fun addDirectionalLight(directionalLight: DirectionalLightImpl) {
        directionalLights.add(directionalLight)
    }

    fun removeDirectionalLight(directionalLight: DirectionalLightImpl) {
        directionalLights.remove(directionalLight)
    }

    fun removeAllDirectionalLights() {
        directionalLights.clear()
    }

    val directionalLightAmount: Int
        get() = directionalLights.size

    fun addPointLight(pointLight: PointLightImpl) {
        pointLights.add(pointLight)
    }

    fun removePointLight(pointLight: PointLightImpl) {
        pointLights.remove(pointLight)
    }

    fun removeAllPointLights() {
        pointLights.clear()
    }

    val pointLightAmount: Int
        get() = pointLights.size

    fun addSpotLight(spotLight: SpotLightImpl) {
        spotLights.add(spotLight)
    }

    fun removeSpotLight(spotLight: SpotLightImpl) {
        spotLights.remove(spotLight)
    }

    fun removeAllSpotLights() {
        spotLights.clear()
    }

    val spotLightAmount: Int
        get() = spotLights.size

    companion object {
        private const val LIGHT_LIST_INITIAL_CAPACITY = 8
        private val DEFAULT_AMBIENT_STRENGTH: ImmutableVec3f = builder().of(0.01f, 0.01f, 0.01f)
    }

}
