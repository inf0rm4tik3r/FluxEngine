package de.colibriengine.renderer

import de.colibriengine.ecs.*
import de.colibriengine.graphics.PickingInfo
import de.colibriengine.graphics.camera.AbstractCamera
import de.colibriengine.graphics.camera.concrete.ActionCameraImpl
import de.colibriengine.graphics.components.InternalRenderComponent
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.effects.ambientocclusion.SSAOEffect
import de.colibriengine.graphics.effects.ambientocclusion.SSAOTextureViewerShader
import de.colibriengine.graphics.effects.antialiasing.FXAAEffect
import de.colibriengine.graphics.effects.kernel.FXKernel
import de.colibriengine.graphics.effects.kernel.FXKernelShader
import de.colibriengine.graphics.fbo.buildFBOTextureRenderer
import de.colibriengine.graphics.model.ModelFactory
import de.colibriengine.graphics.model.sceneentities.AlignedQuad
import de.colibriengine.graphics.opengl.GLSet
import de.colibriengine.graphics.opengl.GLUtil
import de.colibriengine.graphics.opengl.buffer.fbo.FixedResolutionFBO
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOTextureRenderer
import de.colibriengine.graphics.opengl.rendering.GLBlendFactor
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.graphics.shader.MVPTextureShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.shadow.VarianceShadowMap
import de.colibriengine.graphics.texture.Texture
import de.colibriengine.graphics.window.Window
import de.colibriengine.graphics.window.WindowManager
import de.colibriengine.math.vector.vec2i.StdVec2i
import de.colibriengine.math.vector.vec2i.StdVec2iFactory.Companion.ZERO
import de.colibriengine.math.vector.vec2i.Vec2i
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec4f.StdVec4fFactory.Companion.TRANSPARENT
import de.colibriengine.renderer.fbo.GBufferImpl
import de.colibriengine.renderer.fbo.GUIBuffer
import de.colibriengine.renderer.shader.*
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.scene.graph.SceneGraphNode
import de.colibriengine.ui.GUI
import de.colibriengine.ui.components.PanelImpl
import de.colibriengine.ui.components.TEXT2D
import de.colibriengine.util.Timer
import org.lwjgl.opengl.GL11

class RenderingEngineImpl(
    private val userEcs: ECS,
    internal val internalEcs: ECS,
    val windowManager: WindowManager,
    val shaderManager: ShaderManager,
    val modelFactory: ModelFactory,
    override val renderOptions: RenderOptions,
    override val debugRenderer: DebugRenderer
) : RenderingEngine, RenderOptionsListener {

    /**
     * The geometry buffer. All geometry of a scene will be rendered to this buffer. It can then be used to perform post
     * processing.
     */
    val gBuffer: GBufferImpl
    private lateinit var ppTexToRead: Texture
    private var ppTexIndexToRead = 0
    private lateinit var ppTexToWrite: Texture
    private var ppTexIndexToWrite = 0

    private val uiBuffer: GUIBuffer
    val shadowMapCamera: AbstractCamera
    val centeredCam: AbstractCamera

    /**
     * @return The [LightingEngine] of this renderer. Used to perform the lighting of the scene.
     */
    private val lightingEngine: LightingEngine
    private val texturedQuadShader: TexturedQuadShader
    private val reinhardToneMappingWithGammaAdjustmentShader: ReinhardToneMappingWithGammaAdjustmentShader
    private val fxKernel: FXKernel
    private val ssaoEffect: SSAOEffect
    private val fxaaEffect: FXAAEffect
    private val gBufferQuad: AlignedQuad
    val gBufferInternalRenderer: GLFBOTextureRenderer
    private val gBufferToScreenRenderer: GLFBOTextureRenderer
    private val gBufferViewer: GLFBOTextureRenderer
    private val shadowMapViewer: GLFBOTextureRenderer
    private val ssaoTextureViewer: GLFBOTextureRenderer
    private val guiToScreenRenderer: GLFBOTextureRenderer
    var ppEnabled = true
    var ssaoEnabled = true
    var ssaoDebugEnabled = false
    var fxaaEnabled = true
    var debugGBuffer = false
    private val tmpVec2i: Vec2i = StdVec2i()

    // TODO: ---------- OPTIMIZE -------------------
    var mainRenderShader: MVPShader = BasicShader(shaderManager)
    var mainTransparencyFilterRenderShader: BasicTransparencyFilterShader = BasicTransparencyFilterShader(shaderManager)
    var shadowGenerationShader: MVPShader = VarianceShadowMapGenerationShader(shaderManager)

    // GUI
    var guiComponentShader: GuiComponentShader = GuiComponentShader(shaderManager);
    var guiComponentBackgroundShader: GuiComponentBackgroundShader = GuiComponentBackgroundShader(shaderManager)

    private val renderSystem: RenderSystem
    private val shadowRenderSystem: ShadowRenderSystem
    private val gBufferPublicationRenderSystem: GBufferPublicationRenderSystem

    init {
        renderOptions.addListener(this)
        gBuffer = GBufferImpl()
        uiBuffer = GUIBuffer()
        initFBOs()
        shadowMapCamera = ActionCameraImpl(
            internalEcs.createEntity(),
            windowManager.activeWindow
        )
        centeredCam = ActionCameraImpl(
            internalEcs.createEntity(),
            windowManager.activeWindow
        )
        // TODO move
        lightingEngine = LightingEngine(this)

        gBufferQuad = AlignedQuad(internalEcs.createEntity(), modelFactory)
        gBufferQuad.entity.useOrThrow<RenderComponent> {
            gBufferQuad.entity.add(InternalRenderComponent(model).also {
                shadowCaster = false
            })
        }
        gBufferQuad.entity.remove<RenderComponent>()

        val shaderManager = shaderManager
        texturedQuadShader = TexturedQuadShader(shaderManager)
        fxKernel = FXKernel(shaderManager)
        fxKernel.kernel.initSobelXKernel()
        ssaoEffect = SSAOEffect(shaderManager, modelFactory.meshFactory, internalEcs) {
            windowManager.activeWindow?.cameraManager?.activeCamera
        }
        fxaaEffect = FXAAEffect(shaderManager, modelFactory.meshFactory, internalEcs)
        reinhardToneMappingWithGammaAdjustmentShader = ReinhardToneMappingWithGammaAdjustmentShader(shaderManager)
        windowManager.activeWindow
        val activeWindow = windowManager.activeWindow ?: error("No active window!")
        gBufferInternalRenderer = buildFBOTextureRenderer(modelFactory.meshFactory) {
            this@buildFBOTextureRenderer.ecs = this@RenderingEngineImpl.internalEcs
            destWriteDimension.set(gBuffer.xResolution().toFloat(), gBuffer.yResolution().toFloat())
            shader = texturedQuadShader
        }
        gBufferToScreenRenderer = buildFBOTextureRenderer(modelFactory.meshFactory) {
            this@buildFBOTextureRenderer.ecs = this@RenderingEngineImpl.internalEcs
            srcFBO = gBuffer
            srcInputTextureIndices.add(gBuffer.pp2TextureIndex)
            shader = reinhardToneMappingWithGammaAdjustmentShader
            shaderSetup = {
                val shader = shader as ReinhardToneMappingWithGammaAdjustmentShader
                shader.bind()
                shader.setMVPMatrix(it.getQuadOrthographicMVP())
                shader.setTexture(gBuffer.pp2TextureIndex)
            }
        }
        gBufferViewer = buildFBOTextureRenderer(modelFactory.meshFactory) {
            this@buildFBOTextureRenderer.ecs = this@RenderingEngineImpl.internalEcs
            srcFBO = gBuffer
            srcInputTextureIndices.add(gBuffer.normalTextureIndex)
            srcSamplePos.set(0.25f, 0.25f)
            srcSampleDimension.set(0.5f, 0.5f)
            destWritePos.set(
                activeWindow.framebufferSize.x / 4.0f,
                activeWindow.framebufferSize.y / 4.0f
            )
            destWriteDimension.set(
                activeWindow.framebufferSize.x / 2.0f,
                activeWindow.framebufferSize.y / 2.0f
            )
            shader = texturedQuadShader
            shaderSetup = {
                val shader = shader as MVPTextureShader
                shader.bind()
                shader.setMVPMatrix(it.getQuadOrthographicMVP())
                shader.setTexture(gBuffer.normalTextureIndex)
            }
        }

        //DepthViewerShader
        val depthViewerShader = DepthViewerShader(shaderManager)
        shadowMapViewer = buildFBOTextureRenderer(modelFactory.meshFactory) {
            this@buildFBOTextureRenderer.ecs = this@RenderingEngineImpl.internalEcs
            shader = depthViewerShader
        }
        ssaoTextureViewer = buildFBOTextureRenderer(modelFactory.meshFactory) {
            this@buildFBOTextureRenderer.ecs = this@RenderingEngineImpl.internalEcs
            shader = ssaoEffect.ssaoTextureViewerShader
        }
        val guiToScreenShader = TexturedQuadTransparencyFilterShader(shaderManager)
        guiToScreenRenderer = buildFBOTextureRenderer(modelFactory.meshFactory) {
            this@buildFBOTextureRenderer.ecs = this@RenderingEngineImpl.internalEcs
            srcFBO = uiBuffer
            srcInputTextureIndices.add(uiBuffer.guiTextureIndex)
            shader = guiToScreenShader
            shaderSetup = {
                val shader = shader as TexturedQuadTransparencyFilterShader
                shader.bind()
                shader.setMVPMatrix(it.getQuadOrthographicMVP())
                shader.setTexture(uiBuffer.guiTextureIndex)
            }
        }

        renderSystem = userEcs.createSystem2 {
            RenderSystem(
                mainRenderShader,
                mainTransparencyFilterRenderShader,
                shadowGenerationShader
            )
        }
        shadowRenderSystem = userEcs.createSystem2 {
            ShadowRenderSystem(
                shadowGenerationShader
            )
        }
        gBufferPublicationRenderSystem = userEcs.createSystem2 {
            GBufferPublicationRenderSystem(
                mainRenderShader,
                mainTransparencyFilterRenderShader,
            )
        }
    }

    fun destroy() {
        lightingEngine.destroy()
    }

    private fun initFBOs() {
        initGBuffer()
        initGUIBuffer()
    }

    private fun initGBuffer() {
        gBuffer.destroy()
        gBuffer.resolution = renderOptions.sceneResolution
        windowManager.activeWindow?.let {
            gBuffer.clearColor = it.clearColor
        }
        gBuffer.init()
        resetReadWritePPTextures()
    }

    private fun initGUIBuffer() {
        uiBuffer.destroy()
        uiBuffer.resolution = renderOptions.guiResolution
        uiBuffer.clearColor = TRANSPARENT
        uiBuffer.init()
    }

    fun update() {
        // TODO: necessary?
    }

    private fun resetReadWritePPTextures() {
        ppTexToRead = gBuffer.pp2Texture
        ppTexIndexToRead = gBuffer.pp2TextureIndex
        ppTexToWrite = gBuffer.pp1Texture
        ppTexIndexToWrite = gBuffer.pp1TextureIndex
    }

    private fun swapReadWritePPTextures() {
        val tempTexture = ppTexToRead
        val tempIndex = ppTexIndexToRead
        ppTexToRead = ppTexToWrite
        ppTexIndexToRead = ppTexIndexToWrite
        ppTexToWrite = tempTexture
        ppTexIndexToWrite = tempIndex
    }

    /**
     * Renders the GUI (graphical user interface) of the application.
     *
     * @param root The node at which the rendering starts.
     */
    fun renderGUI(root: GUI) {
        // Bind the GUI FBO.
        uiBuffer.bind()
        uiBuffer.clearAttachments()
        uiBuffer.clearDepthAndStencil()

        // Set the viewport to the resolution of the GUI buffer.
        GLSet.viewport(0, 0, uiBuffer.resolution)

        // Render the GUI.
        GLUtil.disableDepthMask()
        GLUtil.disableDepthTest()
        GLUtil.enableDepthClamp() // TODO: Can we render the UI without depth clamping enabled?
        renderNode(root.rootNode, RenderPass.GUI_GEOMETRY)
        GLUtil.disableDepthClamp()
        GLUtil.enableDepthTest()
        GLUtil.enableDepthMask()

        // Blend the rendering onto the final image in the default framebuffer.
        setUpScreenRenderer(guiToScreenRenderer, uiBuffer)

        GLUtil.enableBlending()
        guiToScreenRenderer.render(centeredCam)
        GLUtil.disableBlending()
    }

    /**
     * Renders the specified scenegraph.
     *
     * @param root Root object from which the rendering should start.
     */
    fun render(root: AbstractSceneGraphNode, timer: Timer.View) {
        lightingEngine.computeLightingInformation(root)
        geometryPass(timer)
        debugPass()
        shadowMapPass(root, timer)
        lightingPass()

        // Post processing:
        resetReadWritePPTextures()
        if (ppEnabled) {
            if (ssaoEnabled) {
                ssaoEffect.generate(renderOptions.sceneResolution, gBuffer, centeredCam)
                // TODO: Change to better target!
                ssaoEffect.apply(
                    ppTexToRead, gBuffer, ppTexIndexToWrite,
                    ZERO, gBuffer.resolution, centeredCam
                )
                swapReadWritePPTextures()
            }
            if (fxaaEnabled) {
                fxaaEffect.apply(
                    ppTexToRead, gBuffer, ppTexIndexToWrite,
                    ZERO, gBuffer.resolution, centeredCam
                )
                swapReadWritePPTextures()
            }
        }

        // final -> default fbo
        finalPass()

        if (debugGBuffer) {
            gBufferViewer.render(centeredCam)
        }
        if (ppEnabled) {
            if (ssaoEnabled && ssaoDebugEnabled) {
                showSSAOTexture(
                    ssaoEffect,
                    ZERO,
                    tmpVec2i.set(windowManager.activeWindow!!.framebufferSize).div(3)
                )
            }
        }
    }

    private fun debugPass() {
        windowManager.activeWindow?.cameraManager?.activeCamera?.let {
            debugRenderer.render(it)
        }
    }

    private fun setUpScreenRenderer(
        screenRenderer: GLFBOTextureRenderer,
        srcFBO: FixedResolutionFBO
    ) {
        val activeWindow = windowManager.activeWindow ?: error("There is no active window.")
        screenRenderer.updateDestWritePosAndDimensionStretchAligned(
            activeWindow.framebufferSize, srcFBO.resolution
        )
    }

    private fun showVarianceShadowMap(
        varianceShadowMap: VarianceShadowMap,
        position: Vec2iAccessor,
        dimension: Vec2iAccessor
    ) {
        shadowMapViewer.updateSrcFBO(varianceShadowMap, varianceShadowMap.mainTextureIndex)
        shadowMapViewer.updateDestWritePosAndDimension(position, dimension)
        shadowMapViewer.shaderSetup = {
            val shader = it.shader as DepthViewerShader
            shader.bind()
            shader.setMVPMatrix(it.getQuadOrthographicMVP())
            shader.setTexture(varianceShadowMap.mainTextureIndex)
            shader.setZNear(shadowMapCamera.zNear)
            shader.setZFar(shadowMapCamera.zFar)
        }
        shadowMapViewer.render(centeredCam)
    }

    private fun showSSAOTexture(
        ssaoEffect: SSAOEffect,
        position: Vec2iAccessor,
        dimension: Vec2iAccessor
    ) {
        ssaoTextureViewer.updateSrcFBO(
            ssaoEffect.ssaoFramebuffer,
            ssaoEffect.ssaoFramebuffer.postProcessedTextureIndex
        )
        ssaoTextureViewer.updateDestWritePosAndDimension(position, dimension)
        ssaoTextureViewer.shaderSetup = {
            val shader = it.shader as SSAOTextureViewerShader
            shader.bind()
            shader.setMVPMatrix(it.getQuadOrthographicMVP())
            shader.setTexture(ssaoEffect.ssaoFramebuffer.postProcessedTextureIndex)
        }
        ssaoTextureViewer.render(centeredCam)
    }

    private fun fxApplyKernel() {
        gBufferInternalRenderer.updateSrcFBO(gBuffer, gBuffer.pp2TextureIndex)
        gBufferInternalRenderer.updateDestFBO(gBuffer, gBuffer.pp1TextureIndex)
        gBufferInternalRenderer.shader = fxKernel.shader
        gBufferInternalRenderer.shaderSetup = {
            val shader = it.shader as FXKernelShader
            shader.bind()
            shader.setMVPMatrix(it.getQuadOrthographicMVP())
            shader.setTexture(gBuffer.pp2TextureIndex)
            shader.setKernelMatrix(fxKernel.kernel)
            shader.setOffsets(1.0f / gBuffer.xResolution(), 1.0f / gBuffer.yResolution())
        }
        gBufferInternalRenderer.render(centeredCam)
    }

    /**
     * Must be called before rendering to the GBuffer textures.
     */
    private fun setUpGBufferRendering() {
        gBuffer.bindForDrawing()
        gBufferQuad.alignSimple(renderOptions.sceneResolution)

        // Set the viewport to the resolution of the GBuffer.
        GLSet.viewport(0, 0, gBuffer.resolution)

        // TODO: necessary?
        windowManager.activeWindow?.cameraManager?.activeCamera?.let {
            //camera.initProjectionMatrices(renderOptions.getSceneResolution());
            //camera.calculateViewProjectionMatrices();
            gBufferQuad.computeFinalOrthographicTransformation(it.orthographicViewProjectionMatrix)
        }
    }

    private fun geometryPass(timer: Timer.View) {
        setUpGBufferRendering()

        // Clear the GBuffer of any data from the previous frame.
        gBuffer.clearAttachments()
        gBuffer.clearDepthAndStencil()

        // Bind all color textures except the "final" texture for drawing..
        gBuffer.setDrawBuffers(
            gBuffer.albedoTextureIndex,
            gBuffer.normalTextureIndex,
            gBuffer.specularTextureIndex,
            gBuffer.pickingRenderbuffer
        )

        // Render all drawable entities.
        userEcs.process(gBufferPublicationRenderSystem, timer)
    }

    // TODO: Remove this. Use ECS systems all the way.
    @Deprecated("Use ECS systems instead.")
    private fun renderNode(node: SceneGraphNode, renderPass: RenderPass) {
        node.entity.useIfPresent<RenderComponent> {
            var shader: MVPShader = mainRenderShader

            // Disable face culling if necessary.
            if (!cullBackFaces) {
                GL11.glDisable(GL11.GL_CULL_FACE)
            }

            if (transparency.isTransparent) {
                shader = mainTransparencyFilterRenderShader
            }

            // Enable blending if necessary.
            if (blending.active) {
                GL11.glEnable(GL11.GL_BLEND)
                GL11.glBlendFunc(
                    GLBlendFactor.from(blending.blendFuncSourceFactor).id,
                    GLBlendFactor.from(blending.blendFuncDestinationFactor).id
                )
            }

            when {
                // SHADOW MAP GENERATION
                renderPass === RenderPass.SHADOW_MAP_GENERATION -> {
                    shadowGenerationShader.bind()
                    shadowGenerationShader.setMVPMatrix(
                        node.transformCalculation.orthographicTransformation
                    )
                    model.render()
                }
                // DEFERRED
                renderPass === RenderPass.DEFERRED_GEOMETRY -> {
                    shader.bind()
                    shader.setMVPMatrix(
                        node.transformCalculation.perspectiveTransformation
                    )
                    if (transparency.isTransparent) {
                        (shader as BasicTransparencyFilterShader).setAlphaThreshold(
                            transparency.alphaThreshold
                        )
                    }
                    model.render()
                }
                // GEOMETRY
                renderPass === RenderPass.GUI_GEOMETRY -> {
                    if (node is PanelImpl) {
                        if (node.background.useColor || node.background.useGradient || node.background.useImage) {
                            shader = guiComponentBackgroundShader
                            shader.bind()
                            shader.setMVPMatrix(node.transformCalculation.orthographicTransformation)
                            shader.setUse(
                                node.background.useColor,
                                node.background.useGradient,
                                node.background.useImage
                            )
                            if (node.background.useColor) {
                                shader.setColor(node.background.color, node.background.opacity)
                            }
                            if (node.background.useGradient) {
                                shader.setGradient(node.background.gradient)
                            }
                            if (node.background.useImage) {
                                requireNotNull(node.background.image)
                                node.background.image!!.bind(0)
                                //shader.setImageTextureUnit(0);
                            }
                            model.render()
                        }
                    } else if (node is TEXT2D) {
                        shader = guiComponentShader
                        shader.bind()
                        shader.setMVPMatrix(node.transformCalculation.orthographicTransformation)
                        shader.setColor(node.color)
                        if (transparency.isTransparent) {
                            (shader as BasicTransparencyFilterShader).setAlphaThreshold(
                                transparency.alphaThreshold
                            )
                        }
                        model.render()
                    } else {
                        shader.bind()
                        shader.setMVPMatrix(node.transformCalculation.perspectiveTransformation)
                        if (transparency.isTransparent) {
                            (shader as BasicTransparencyFilterShader).setAlphaThreshold(
                                transparency.alphaThreshold
                            )
                        }
                        model.render()
                    }
                }
            }

            // Disable blending
            if (blending.active) {
                GL11.glDisable(GL11.GL_BLEND)
            }

            // Enable face culling
            if (!cullBackFaces) {
                GL11.glEnable(GL11.GL_CULL_FACE)
            }
        }

        // Every sub component must render its childComponents.
        for (child in node.childNodeIterator) {
            renderNode(child, renderPass)
        }
    }
    // TODO: --------------------------------------

    /**
     * This pass creates all necessary shadow maps.
     */
    private fun shadowMapPass(root: AbstractSceneGraphNode, timer: Timer.View) {
        windowManager.activeWindow?.let { activeWindow: Window ->
            val cameraManager = activeWindow.cameraManager
            val currentCameraBackup = cameraManager.activeCamera
            shadowMapCamera.parentWindow = activeWindow
            for (directionalLight in lightingEngine.directionalLights) {
                // Skip this light if it should not cast shadows.
                if (!directionalLight.castsShadows) {
                    continue
                }
                val varianceShadowMap = lightingEngine.getShadowMapFor(directionalLight)

                // Use the global variance shadow map settings if no specific settings for this light are present.
                val settings =
                    if (varianceShadowMap.settings == null)
                        renderOptions.globalVarianceShadowMapSettings
                    else
                        varianceShadowMap.settings!!

                // We otherwise must ensure that the lights shadow map is ready!
                if (!varianceShadowMap.isCreated) {
                    // TODO: Change the resolution on the fly? Recreate shadow map. Where? When?
                    varianceShadowMap.resolution = settings.resolution
                    varianceShadowMap.init()
                }

                // cameraManager.setActiveCamera(shadowMapCamera);
                shadowMapCamera.use()
                shadowMapCamera.transform.setLike(directionalLight.transform)
                shadowMapCamera.initProjectionMatrices(varianceShadowMap.resolution)
                shadowMapCamera.initOrthographicProjectionMatrix(
                    -15f, 15f, -15f, 15f, 15f, -15f
                )
                shadowMapCamera.transform.calculateTransformation()
                shadowMapCamera.calculateViewProjectionMatrices()
                root.updateNodeTransformation(
                    shadowMapCamera.orthographicViewProjectionMatrix,
                    shadowMapCamera.perspectiveViewProjectionMatrix
                )
                varianceShadowMap.bindForDrawing()
                varianceShadowMap.clearAttachments()
                varianceShadowMap.clearDepthAndStencil()
                GLSet.viewport(0, 0, varianceShadowMap.xResolution(), varianceShadowMap.yResolution())

                userEcs.process(shadowRenderSystem, timer)

                cameraManager.activeCamera = currentCameraBackup

                // Blur the shadow map.
                GLUtil.disableDepthTest()
                GLUtil.disableDepthMask()
                varianceShadowMap.applyPostProcessing(
                    settings.blurTextureLookUpScale,
                    settings.blurTextureLookUpScale,
                    centeredCam
                )
                GLUtil.enableDepthMask()
                GLUtil.enableDepthTest()
            }
        }
    }

    private fun lightingPass() {
        setUpGBufferRendering() // TODO: replace
        lightingEngine.doLighting()
    }

    /** Finalizes the deferred rendering process by copying the "final" image into the default framebuffer. */
    private fun finalPass() {
        // Blend the rendering onto the final image in the default framebuffer.
        GLUtil.disableDepthTest()
        GLUtil.disableDepthMask()
        gBufferToScreenRenderer.updateSrcFBO(gBuffer, ppTexIndexToRead)
        setUpScreenRenderer(gBufferToScreenRenderer, gBuffer)
        gBufferToScreenRenderer.render(centeredCam)
        GLUtil.enableDepthMask()
        GLUtil.enableDepthTest()
    }

    override fun readPickingInfo(x: Int, y: Int): PickingInfo = gBuffer.readPickingInfo(x, y)

    override fun rolUseDynamicResolutionChanged(usesDynamicResolution: Boolean) = initFBOs()
    override fun rolSceneResolutionChanged(resolution: Vec2iAccessor) = initFBOs()
    override fun rolMinSceneResolutionChanged(resolution: Vec2iAccessor) = initFBOs()
    override fun rolGuiResolutionChanged(resolution: Vec2iAccessor) = initFBOs()
    override fun rolTargetFramerateChanged(framerate: Float) = initFBOs()

}
