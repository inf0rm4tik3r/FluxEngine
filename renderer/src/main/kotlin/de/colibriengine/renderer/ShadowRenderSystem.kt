@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")

package de.colibriengine.renderer

import de.colibriengine.components.TransformCalculationComponent
import de.colibriengine.ecs.System2
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.util.Timer
import org.lwjgl.opengl.GL11

class ShadowRenderSystem(
    private var shadowGenerationShader: MVPShader
) : System2<RenderComponent, TransformCalculationComponent> {

    override fun beforeAll() {
        shadowGenerationShader.bind()
    }

    override fun processEntity(
        render: RenderComponent,
        transformCalculation: TransformCalculationComponent,
        timer: Timer.View
    ) {
        if (!render.cullBackFaces) {
            GL11.glDisable(GL11.GL_CULL_FACE)
        }

        // TODO: Handle shadows of transparent objects by combining the shadowGenerationShader and the mainTransparencyFilterRenderShader
        /*
        if (render.transparency.isTransparent) {
            shader = mainTransparencyFilterRenderShader
        }
        */

        // We do not use blending when rendering shadows.

        if (render.shadowCaster) {
            shadowGenerationShader.setMVPMatrix(transformCalculation.orthographicTransformation)
            render.model.render()
        }

        if (!render.cullBackFaces) {
            GL11.glEnable(GL11.GL_CULL_FACE)
        }
    }

}
