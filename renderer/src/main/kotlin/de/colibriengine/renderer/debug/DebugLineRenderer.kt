package de.colibriengine.renderer.debug

import de.colibriengine.graphics.DataType
import de.colibriengine.graphics.model.VertexAttribute
import de.colibriengine.graphics.model.buildVertexAttributes
import de.colibriengine.graphics.opengl.model.DynamicOpenGLMesh
import de.colibriengine.graphics.rendering.PrimitiveMode
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec4f.Vec4f
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.util.BYTES_PER_FLOAT

class DebugLineRenderer {

    val mesh: DynamicOpenGLMesh = DynamicOpenGLMesh(
        primitiveMode = PrimitiveMode.LINES,
        capacity = 1000,
        attributes = buildVertexAttributes {
            add("position", VertexAttribute(0, 3, DataType.FLOAT, Vec3f.BYTES))
            add("color", VertexAttribute(1, 4, DataType.FLOAT, Vec4f.BYTES))
            add("thickness", VertexAttribute(2, 1, DataType.FLOAT, BYTES_PER_FLOAT))
        },
        allowAttributeChanges = false
    )

    fun startFrame() {
        mesh.clear()
    }

    // TODO: Cache lines each frame and only submit to mesh once.
    fun drawLine(start: Vec3fAccessor, end: Vec3fAccessor, color: Vec4fAccessor, thickness: Float) {
        mesh.store(2) { posBuffer, colBuffer, thicknessBuffer ->
            // START
            start.storeIn(posBuffer)
            color.storeIn(colBuffer)
            thicknessBuffer.putFloat(thickness)
            // END
            end.storeIn(posBuffer)
            color.storeIn(colBuffer)
            thicknessBuffer.putFloat(thickness)
        }
    }

    fun renderMesh() {
        mesh.render()
    }

}
