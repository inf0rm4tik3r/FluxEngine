package de.colibriengine.renderer.debug

import de.colibriengine.graphics.DataType
import de.colibriengine.graphics.model.VertexAttribute
import de.colibriengine.graphics.model.buildVertexAttributes
import de.colibriengine.graphics.opengl.model.DynamicOpenGLMesh
import de.colibriengine.graphics.rendering.PrimitiveMode
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec4f.Vec4f
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.util.BYTES_PER_FLOAT
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL20

class DebugPointRenderer {

    val mesh: DynamicOpenGLMesh = DynamicOpenGLMesh(
        primitiveMode = PrimitiveMode.POINTS,
        capacity = 1000,
        attributes = buildVertexAttributes {
            add("position", VertexAttribute(0, 3, DataType.FLOAT, Vec3f.BYTES))
            add("color", VertexAttribute(1, 4, DataType.FLOAT, Vec4f.BYTES))
            add("size", VertexAttribute(2, 1, DataType.FLOAT, BYTES_PER_FLOAT))
        },
        allowAttributeChanges = false
    )

    fun startFrame() {
        mesh.clear()
    }

    // TODO: Cache points each frame and only submit to mesh once.
    fun drawPoint(position: Vec3fAccessor, color: Vec4fAccessor, size: Float) {
        require(size > 0f) { "Negative point size is not allowed." }
        mesh.store(1) { posBuffer, colBuffer, sizeBuffer ->
            position.storeIn(posBuffer)
            color.storeIn(colBuffer)
            sizeBuffer.putFloat(size)
        }
    }

    fun renderMesh() {
        GL11.glEnable(GL20.GL_VERTEX_PROGRAM_POINT_SIZE)
        mesh.render()
        GL11.glDisable(GL20.GL_VERTEX_PROGRAM_POINT_SIZE)
    }

}
