package de.colibriengine.renderer.fbo

import de.colibriengine.graphics.GBuffer
import de.colibriengine.graphics.PickingInfo
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOAttachment
import de.colibriengine.graphics.opengl.buffer.fbo.FixedResolutionFBO
import de.colibriengine.graphics.opengl.texture.GLTexelDataFormat
import de.colibriengine.graphics.opengl.texture.GLTexelDataType
import de.colibriengine.graphics.texture.ImageFormat
import de.colibriengine.graphics.texture.Texture
import de.colibriengine.graphics.texture.TextureFilterType
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL30
import org.lwjgl.system.MemoryStack
import java.nio.IntBuffer

/**
 * The GBuffer is a part of ColibriEngine's deferred renderer. Geometry is rendered once to this buffer. Info: The
 * GBuffer is not initialized upon creation (no OpenGL initialization). [setResolution] must be called manually. Then
 * the GBuffer can be initialized by calling [init]. This instance can be reused. The buffer can be [destroy]ed an
 * [init]ialized at will.
 */
class GBufferImpl : GBuffer, FixedResolutionFBO() {

    // Indices of a texture of this GBuffer.
    val albedoTextureIndex = 0
    val normalTextureIndex = 1
    val specularTextureIndex = 2
    val pp1TextureIndex = 3
    val pp2TextureIndex = 4
    val pickingRenderbuffer = 5

    override val albedoTexture: Texture
        get() = textures[albedoTextureIndex]

    override val normalTexture: Texture
        get() = textures[normalTextureIndex]

    override val specularTexture: Texture
        get() = textures[specularTextureIndex]

    override val pp1Texture: Texture
        get() = textures[pp1TextureIndex]

    override val pp2Texture: Texture
        get() = textures[pp2TextureIndex]

    override val depthTexture: Texture
        get() = super.depthTexture

    public override fun init() {
        makeValid()
        ensureResolutionIsSet()

        // The underlying FBO must be destroyed first if it is already created (was previously initialized).
        if (isCreated) {
            destroy()
        }

        // Initialize the underlying frame buffer object.
        create()

        // Add albedo texture.
        addTexture(ImageFormat.RGB8, TextureFilterType.NEAREST)

        // Add normal texture.
        addTexture(
            resolution.x, resolution.y,
            ImageFormat.RGB10_A2, GLTexelDataFormat.GL_RGBA,
            GLTexelDataType.GL_UNSIGNED_BYTE,
            TextureFilterType.NEAREST, TextureFilterType.NEAREST
        )

        //addTexture(ImageFormat.GL_RGB10_A2, TextureFilterType.GL_NEAREST);

        // Add specular texture.
        addTexture(ImageFormat.RG16F, TextureFilterType.NEAREST)

        // Add PP1 texture.
        addTexture(ImageFormat.RGB16F, TextureFilterType.LINEAR) // GL_LINEAR might improve the effects.

        // Add PP2 texture.
        addTexture(ImageFormat.RGB16F, TextureFilterType.LINEAR)

        // Add depth texture. (Allows lighting calculations)
        initDepthTexture(ImageFormat.DEPTH24_STENCIL8, GLFBOAttachment.GL_DEPTH_STENCIL_ATTACHMENT)

        // Add depth stencil renderbuffer.
        //super.initDepthStencilRenderbuffer(TextureFormat.GL_DEPTH24_STENCIL8);

        // Add draw data (picking) renderbuffer.
        addRenderbuffer(ImageFormat.RGB16UI)

        // Create the FBO.
        compile()

        // The creation of the underlying FBO might have left us with that FBO being bound to the context.
        // We have to make sure that the default FBO is bound.
        bindDefaultFBO()
    }

    fun readPickingInfo(position: Vec2iAccessor): PickingInfo {
        return readPickingInfo(position.x, position.y)
    }

    /**
     * Reads the specified pixel in the picking texture.
     * Returns the ObjectID, DrawCallID and PrimitiveID in a PickingInfo object.
     *
     * @param x Position from the left
     * @param y Position from the bottom
     * @return PickingInfo
     * @throws UnsupportedOperationException If this object is currently invalidated.
     */
    // TODO: Create high performant read() method in RBO class. Call it here.
    fun readPickingInfo(x: Int, y: Int): PickingInfo {
        ensureNotInvalidated()
        bindForReading()
        setReadBuffer(pickingRenderbuffer)

        // Retrieves the RGB values of the pixel in the pickingTexture located at position (x,y).
        val ib: IntBuffer
        MemoryStack.stackPush().use { stack ->
            ib = stack.mallocInt(3)
            GL11.glReadPixels(x, y, 1, 1, GL30.GL_RGB_INTEGER, GL11.GL_UNSIGNED_INT, ib)
        }

        // TODO: reset really necessary?
        setReadBufferToNONE()
        bindDefaultFBOForReading()
        return PickingInfo(ib[0], ib[1], ib[2])
    }

}
