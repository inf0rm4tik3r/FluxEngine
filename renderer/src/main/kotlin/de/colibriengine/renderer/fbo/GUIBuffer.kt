package de.colibriengine.renderer.fbo

import de.colibriengine.graphics.opengl.buffer.fbo.FixedResolutionFBO
import de.colibriengine.graphics.opengl.texture.GLTexelDataFormat
import de.colibriengine.graphics.opengl.texture.GLTexelDataType
import de.colibriengine.graphics.texture.ImageFormat
import de.colibriengine.graphics.texture.TextureFilterType

/** Frame buffer object ([de.colibriengine.graphics.fbo.FBO]) in which the graphical user interface is rendered. */
class GUIBuffer : FixedResolutionFBO {

    /** Index of the main gui texture used in this framebuffer. */
    val guiTextureIndex = 0

    /**
     * Constructs a new [GUIBuffer] without initializing it (no OpenGL initialization!). The [GUIBuffer.setResolution]
     * method must be called manually. The [GUIBuffer.init] function must be called manually.
     */
    constructor() : super()

    /**
     * Constructs and initializes a new [GUIBuffer]. Manually calling [GUIBuffer.init] after this is not necessary.
     *
     * @param textureWidth The width of the FBO textures.
     * @param textureHeight The height of the FBO textures.
     */
    constructor(textureWidth: Int, textureHeight: Int) : super(textureWidth, textureHeight) {
        init()
    }

    /**
     * Initializes this `GBuffer` instance.
     * Each created instance is automatically initialized upon creation.
     * This method must be called manually after this instance got invalidated. For example through a call to the
     * [GBufferImpl.setXResolution] method.
     */
    public override fun init() {
        makeValid()
        ensureResolutionIsSet()

        // The underlying FBO must be destroyed first if it is already created (was previously initialized).
        if (isCreated) {
            destroy()
        }

        // Initialize the underlying frame buffer object.
        create()

        // Add color texture.
        addTexture(
            ImageFormat.RGBA8, GLTexelDataFormat.GL_RGBA, GLTexelDataType.GL_INT,
            TextureFilterType.LINEAR, TextureFilterType.LINEAR
        )
        setDrawBufferToNONE()
        setReadBufferToNONE()

        // Create the FBO.
        compile()

        // The creation of the underlying FBO might have left us with that FBO being bound to the context.
        // We have to make sure that the default FBO is bound.
        bindDefaultFBO()
    }

}
