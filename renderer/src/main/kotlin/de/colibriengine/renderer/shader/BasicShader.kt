package de.colibriengine.renderer.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

class BasicShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val ambientTextureUniform: Int
    private val objectIdUniform: Int
    private val drawCallIdUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/basic/basic_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/basic/basic_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        ambientTextureUniform = addUniformLocation("ambientTexture")
        objectIdUniform = addUniformLocation("objectID")
        drawCallIdUniform = addUniformLocation("drawCallID")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    fun setAmbientTextureBinding(ambientTextureBinding: Int) {
        setUniform1i(ambientTextureUniform, ambientTextureBinding)
    }

    fun setObjectId(objectId: Int) {
        setUniform1ui(objectIdUniform, objectId)
    }

    fun setDrawCallId(drawCallId: Int) {
        setUniform1ui(drawCallIdUniform, drawCallId)
    }

}
