package de.colibriengine.renderer.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

class BasicShadowMapGenerationShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/basic/BasicPosPassThrough_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/shadows/DepthWriteOnly_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

}
