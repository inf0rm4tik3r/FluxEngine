package de.colibriengine.renderer.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

class BasicTransparencyFilterShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val ambientTextureUniform: Int
    private val alphaThresholdUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/basic/basic_transparency_filter_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/basic/basic_transparency_filter_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        ambientTextureUniform = addUniformLocation("ambientTexture")
        alphaThresholdUniform = addUniformLocation("alphaThreshold")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    fun setAlphaThreshold(alphaThreshold: Float) {
        setUniform1f(alphaThresholdUniform, alphaThreshold)
    }

}
