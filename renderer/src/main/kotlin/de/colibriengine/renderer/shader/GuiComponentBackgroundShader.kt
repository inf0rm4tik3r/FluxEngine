package de.colibriengine.renderer.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3i.StdVec3i
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.ui.Gradient

// TODO: Move to "ui"
class GuiComponentBackgroundShader(shaders: ShaderManager) : AbstractOpenGLShader(), MVPShader {

    private val mvpMatrixUniform: Int
    private val useUniform: Int
    private val colorUniform: Int
    private val imageTextureUnitUniform: Int
    private val gradientUniform: Int

    init {
        attachVertexShader(shaders.requestShaderSource("shaders/gui/gui_component_background_vs.glsl"))
        attachFragmentShader(shaders.requestShaderSource("shaders/gui/gui_component_background_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        useUniform = addUniformLocation("use")
        colorUniform = addUniformLocation("color")
        imageTextureUnitUniform = addUniformLocation("imageTextureUnit")
        gradientUniform = addUniformLocation("gradient") // 4x4 matrix..
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    fun setUse(useColor: Boolean, useGradient: Boolean, useImage: Boolean) = setUniform3i(
        useUniform, StdVec3i().set(
            if (useColor) 1 else 0,
            if (useImage) 1 else 0,
            if (useGradient) 1 else 0
        )
    )

    fun setColor(color: Vec4fAccessor?) = setUniform4f(colorUniform, color!!)

    fun setColor(color: Vec3fAccessor, opacity: Float) = setUniform4f(colorUniform, color.x, color.y, color.z, opacity)

    fun setImageTextureUnit(imageTextureUnit: Int) = setUniform1i(imageTextureUnitUniform, imageTextureUnit)

    fun setGradient(gradient: Gradient?) {
        /*
        final Mat4f temp = Mat4f.acquire();
        Vec4f color;
        
        color = gradient.getTopLeft();
        temp.setRow(Mat4f.FIRST, color.getR(), color.getG(), color.getB(), color.getA());
        color = gradient.getTopRight();
        temp.setRow(Mat4f.SECOND, color.getR(), color.getG(), color.getB(), color.getA());
        color = gradient.getBottomLeft();
        temp.setRow(Mat4f.THIRD, color.getR(), color.getG(), color.getB(), color.getA());
        color = gradient.getBottomRight();
        temp.setRow(Mat4f.FOURTH, color.getR(), color.getG(), color.getB(), color.getA());
    
        setUniformM4f(GRADIENT_UNIFORM, temp);
        temp.free();
        */
    }

}
