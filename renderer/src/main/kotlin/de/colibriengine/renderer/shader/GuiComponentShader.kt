package de.colibriengine.renderer.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPTextureColorShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor

// TODO: Move to "ui"
class GuiComponentShader(shaders: ShaderManager) : AbstractOpenGLShader(), MVPTextureColorShader {

    private val mvpMatrixUniform: Int
    private val textureUniform: Int
    private val colorUniform: Int

    init {
        attachVertexShader(shaders.requestShaderSource("shaders/gui/gui_component_vs.glsl"))
        attachFragmentShader(shaders.requestShaderSource("shaders/gui/gui_component_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUniform = addUniformLocation("tex")
        colorUniform = addUniformLocation("color")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) = setUniformM4f(mvpMatrixUniform, mvpMatrix)
    override fun setTexture(textureUnit: Int) = setUniform1i(textureUniform, textureUnit)
    override fun setColor(color: Vec4fAccessor) = setUniform4f(colorUniform, color)

}
