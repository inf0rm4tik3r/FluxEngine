package de.colibriengine.renderer.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPTextureShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

class TexturedQuadTransparencyFilterShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPTextureShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUniform: Int

    init {
        attachVertexShader(
            shaderManager.requestShaderSource(
                "shaders/basic/basic_vs.glsl"
            )
        )
        attachFragmentShader(
            shaderManager.requestShaderSource(
                "shaders/basic/textured_quad_transparency_filter_fs.glsl"
            )
        )
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUniform = addUniformLocation("tex")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    override fun setTexture(textureUnit: Int) {
        setUniform1i(textureUniform, textureUnit)
    }

}
