#version 330 core

layout (location = 0) in vec3 position;
layout (location = 2) in vec2 texCoord;

uniform mat4 mvpMatrix;

out vec2 vsTexCoord;

void main(void) {
	vsTexCoord = texCoord;
	gl_Position = mvpMatrix * vec4(position, 1);
}