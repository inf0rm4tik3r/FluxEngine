#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec4 color;
layout (location = 2) in float size;

uniform mat4 mvpMatrix;

out vec4 vsColor;

void main(void) {
    vsColor = color;

    gl_PointSize = size;
    gl_Position = mvpMatrix * vec4(position, 1);
}
