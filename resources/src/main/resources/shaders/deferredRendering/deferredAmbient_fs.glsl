#version 330

layout (location = 0) out vec3 fragColor;

uniform sampler2D colorTexture;

uniform vec3 ambientStrength;

uniform ivec2 screenSize;

/**
 * Returns a vec2 with values in the range [0 ... 1] perfectly suited to sample a texture with.
 * screenSize must be set to the actual size of the WINDOW, not the size of the underlying GBuffer!
 */
vec2 calcTexCoord() {
	return gl_FragCoord.xy / screenSize;
}

void main() {
    vec2 texCoord = calcTexCoord();

    vec3 color = texture(colorTexture, texCoord).rgb;

	fragColor =  color * ambientStrength;
}