#version 330

out vec4 fragColor;

struct BaseLight {
	vec3 color;
	float intensity;
};

struct DirectionalLight {
	BaseLight baseLight;
	vec3 lightDirection;
};

uniform mat4 inverseViewProjectionMatrix;
uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform sampler2D specularTexture;
uniform sampler2D depthTexture;
uniform vec3 cameraPosition;
uniform ivec2 screenSize;
uniform DirectionalLight directionalLight;

uniform mat4 lightSpaceMatrix;
uniform sampler2D shadowMap;
uniform float earlyReturnReferenceDepthBias = 0.01f;
uniform float varianceMaxBound = 0.00002f;
uniform float probabilityMinBound = 0.04f;

float sampleShadowMap(sampler2D shadowMap,
                      vec2 shadowMapTexCoords,
                      float referenceDepth)
{
    float lightViewDepth = texture(shadowMap, shadowMapTexCoords).r;
    return lightViewDepth < referenceDepth ? 0.0 : 1.0;
}

/*
 * Software linear interpolation of a shadow map. Must be used whenever the hardware does not support this natively.
 */
float sampleShadowMapLinear(sampler2D shadowMap,
                            vec2 shadowMapTexCoords,
                            float referenceDepth,
                            vec2 texelSize)
{
    vec2 pixelPos = shadowMapTexCoords / texelSize; // To pixel space (e.g. [1,1024])
    vec2 fraction = fract(pixelPos);
    vec2 startTexel = (pixelPos - fraction) * texelSize; // 1. Center on pixel; 2. Back to [0,1] range.

    // Sample a 2x2 grid.
    float texelBL = sampleShadowMap(shadowMap, startTexel                          , referenceDepth);
    float texelTL = sampleShadowMap(shadowMap, startTexel + vec2(0.0f, texelSize.y), referenceDepth);
    float texelBR = sampleShadowMap(shadowMap, startTexel + vec2(texelSize.x, 0.0f), referenceDepth);
    float texelTR = sampleShadowMap(shadowMap, startTexel + texelSize              , referenceDepth);

    float mixLeft = mix(texelBL, texelTL, fraction.y);
    float mixRight = mix(texelBR, texelTR, fraction.y);

    return mix(mixLeft, mixRight, fraction.x);
}

float samplePCFShadowMap(float numSamples,
                         sampler2D pcfShadowMap,
                         vec2 shadowMapTexCoords,
                         float referenceDepth,
                         vec2 texelSize)
{
    // This variable saves if and how much the current fragment is in shadow. Must be in [0,1] range when returned!
    float shadow = 0.0;

    float sampleStepX = texelSize.x;
    float sampleStepY = texelSize.y;
    float sampleStartX = -(numSamples-1.0f) / 2.0f * sampleStepX;
    float sampleStartY = -(numSamples-1.0f) / 2.0f * sampleStepY;

    for(float x = sampleStartX; x <= -sampleStartX; x += sampleStepX)
    {
        for(float y = sampleStartY; y <= -sampleStartY; y += sampleStepY)
        {
            vec2 texOffset = vec2(x, y);
            shadow += sampleShadowMapLinear(pcfShadowMap, shadowMapTexCoords + texOffset, referenceDepth, texelSize);
        }
    }

    return shadow / (numSamples * numSamples); // Bring shadow in [0,1] range.
}

/**
 *
 */
float linstep(float compareLow, float compareHigh, float toTest)
{
    return clamp((toTest - compareLow) / (compareHigh - compareLow), 0.0f, 1.0f);
}

float sampleVarianceShadowMap(sampler2D varianceShadowMap,
                              vec2 shadowMapTexCoords,
                              float referenceDepth)
{
    vec3 depthInfo = texture(varianceShadowMap, shadowMapTexCoords).rgb;

    float maxDepthInRegion = depthInfo.b;

    if (maxDepthInRegion == 0) {
        // Nothing rendered to the shadow map -> Not in shadow.
        return 1;
    }
    if (maxDepthInRegion < referenceDepth - earlyReturnReferenceDepthBias) {
        // In shadow
        return 0;
    }

    float Ex = depthInfo.r; // Average value over filter region. E(x)
    float Ex_2 = Ex * Ex; // E(x)^2
    float E_x2 = depthInfo.g; // Average squared value over filter region. E(x^2)

    // Note: if fragement is completely in light, probability should be 1.
    float probability = step(referenceDepth, Ex); // Ret: 0 if average < referenceDepth, 1 otherwise.
    float variance = max(E_x2 - Ex_2, varianceMaxBound);

    float distanceFromEx = Ex - referenceDepth;

    // Calculate an upper bound of the probability.
    // "Maximum percentage of values greater or equal to the referenceDepth value."
    // Note: If referenceDepth is exactly the average, probabilityMax will be 1!
    float probabilityMax = variance / (variance + distanceFromEx * distanceFromEx);

    probabilityMax = linstep(probabilityMinBound, 1.0f, probabilityMax); // TODO make uniform.

    // Compensate for errors. If fragment is fully in light, 1 should be returned!
    // TODO: Why can this equation exceed 1?
    return min(max(probability, probabilityMax), 1.0f);
    //return probability;
}

float samplePCFVarianceShadowMap(float numSamples,
                                 sampler2D varianceShadowMap,
                                 vec2 shadowMapTexCoords,
                                 float referenceDepth,
                                 vec2 texelSize)
{
    // This variable saves if and how much the current fragment is in shadow. Must be in [0,1] range when returned!
    float shadow = 0.0;

    float sampleStepX = texelSize.x;
    float sampleStepY = texelSize.y;
    float sampleStartX = -(numSamples-1.0f) / 2.0f * sampleStepX;
    float sampleStartY = -(numSamples-1.0f) / 2.0f * sampleStepY;

    for(float x = sampleStartX; x <= -sampleStartX; x += sampleStepX)
    {
        for(float y = sampleStartY; y <= -sampleStartY; y += sampleStepY)
        {
            vec2 texOffset = vec2(x, y);
            shadow += sampleVarianceShadowMap(varianceShadowMap, shadowMapTexCoords + texOffset, referenceDepth);
        }
    }

    return shadow / (numSamples * numSamples); // Bring shadow in [0,1] range.
}

float calcPCFShadow(sampler2D pcfShadowMap, vec4 fragPosLightSpace, float angleNormalToLightDir, vec2 shadowMapTexelSize)
{
    // Perform perspective division. -> [-1,1] range. -> Then transform to [0,1] range.
    vec3 projCoords = (fragPosLightSpace.xyz / fragPosLightSpace.w) * 0.5 + 0.5;
    vec2 shadowMapTexCoords = projCoords.xy;
    float currentDepth = projCoords.z;

    // No object can interfere. Return early.
    if(currentDepth > 1.0) {
        return 1;
    }

    // tan(acos(angleNormalToLightDir)) -> sqrt(...) // See: https://www.youtube.com/watch?v=uJsQD5FqZoc
    float bias = 0.005 * (sqrt(1 - angleNormalToLightDir*angleNormalToLightDir) / angleNormalToLightDir);
    bias = clamp(bias, 0.0f, 0.01f);

    float referenceDepth = currentDepth - bias;

    return samplePCFShadowMap(1, pcfShadowMap, shadowMapTexCoords, referenceDepth, shadowMapTexelSize);
}

float calcVarianceShadow(sampler2D varianceShadowMap, vec4 fragPosLightSpace)
{
    // Perform perspective division. -> [-1,1] range. -> Then transform to [0,1] range.
    vec3 projCoords = (fragPosLightSpace.xyz / fragPosLightSpace.w) * 0.5 + 0.5;
    vec2 shadowMapTexCoords = projCoords.xy;
    float currentDepth = projCoords.z;

    // No object can interfere. Return early.
    if(currentDepth > 1.0) {
        return 1;
    }

    float referenceDepth = currentDepth;

    return sampleVarianceShadowMap(varianceShadowMap, shadowMapTexCoords, referenceDepth);
}

vec4 calcLight(BaseLight baseLight,
               vec3 lightDirection,
               vec3 normal,
               float angleNormalToLightDir,
               vec3 worldPosition,
               float specularIntensity,
               float specularExponent) {
	vec4 lightColor = vec4(baseLight.color, 1.0f);
	float lightIntensity = baseLight.intensity;

	// Diffuse calculation.
	float diffuseFactor = max(angleNormalToLightDir, 0.0f);
    vec4 diffuseColor = (lightIntensity * diffuseFactor) * lightColor;

    // Specular calculation.
    vec3 directionToEye = normalize(cameraPosition - worldPosition);
    vec3 reflectDirection = normalize(reflect(-lightDirection, normal));

    float specularFactor = dot(directionToEye, reflectDirection);
    vec4 specularColor = vec4(0, 0, 0, 0);
    if (specularFactor > 0) {
        specularFactor = pow(specularFactor, specularExponent);
        specularColor = vec4(baseLight.color * specularIntensity * specularFactor, 1.0f);
    }

    vec4 lighting = diffuseColor + specularColor;
	return lighting;
}

vec4 calcDirectionalLight(vec3 normal, vec3 worldPosition, float specularIntensity, float specularExponent, float angleNormalToLightDir) {
	return calcLight(
	    directionalLight.baseLight,
	    normalize(directionalLight.lightDirection),
	    normal,
	    angleNormalToLightDir,
	    worldPosition,
	    specularIntensity,
	    specularExponent
	);
}

vec3 reconstructWorldPosition(vec2 gBufferTexCoord, float currentDepth) {
	float depth = currentDepth;
	depth = (depth * 2.0f) - 1.0f;

	vec2 ndc = (gBufferTexCoord * 2.0f) - vec2(1.0f);
	vec4 screenSpacePosition = vec4(ndc, depth, 1.0f);
	vec4 worldSpacePosition = inverseViewProjectionMatrix * screenSpacePosition;

	return worldSpacePosition.xyz / worldSpacePosition.w;
}

/**
 * See: https://aras-p.info/texts/CompactNormalStorage.html
 */
vec3 decodeNormal(vec2 encodedNormal) {
    // Method #5: Cry Engine 3
    vec4 nn = vec4(encodedNormal, 1.0, -1.0);
    float l = dot(nn.xyz, -nn.xyw);
    nn.z = l;
    nn.xy *= sqrt(l);
    return nn.xyz * 2 - vec3(0, 0, 1);
}

/**
 * Returns a vec2 with values in the range [0 ... 1] perfectly suited to sample a texture with.
 * screenSize must be set to the actual size of the WINDOW, not the size of the underlying GBuffer!
 */
vec2 calcTexCoord() {
	return gl_FragCoord.xy / screenSize;
}

// TODO: Just calculate lighting information. Accumulate in accumulation buffer. Do not access the diffuse texture every time!
void main() {
	vec2 gBufferTexCoord = calcTexCoord();

    float currentDepth = texture(depthTexture, gBufferTexCoord).r; // In [0,1] range!

	vec3 diffuseColor = texture(diffuseTexture, gBufferTexCoord).xyz;


	// We normalize the normal vector so that the dot product between this normal and another vector returns values
	// from [0..1].
	//vec2 storedNormal = texture(normalTexture, gBufferTexCoord).rg;
	vec4 storedNormal = texture(normalTexture, gBufferTexCoord).rgba;
    //vec3 normal = decodeNormal(storedNormal * 2 - 1);
    //vec3 normal = decodeNormal(storedNormal);
    //vec3 normal = storedNormal * 2 - 1;
    //vec3 normal = storedNormal;
    vec3 normal = normalize(storedNormal.rgb * 2.0f - 1.0f);

    float angleNormalToLightDir = dot(normal, normalize(directionalLight.lightDirection));

	vec3 worldPosition = reconstructWorldPosition(gBufferTexCoord, currentDepth);

	vec2 specularData = texture(specularTexture, gBufferTexCoord).rg;
	float specularIntensity = specularData.r;
	float specularExponent = specularData.g;


    vec4 lightSpacePos = lightSpaceMatrix * vec4(worldPosition, 1.0f); // norm?

    float shadowAmount = calcVarianceShadow(shadowMap, lightSpacePos);

	vec4 lightAmount = calcDirectionalLight(
	    normal, worldPosition, specularIntensity, specularExponent, angleNormalToLightDir
	);

    // Display normal.
	//fragColor = (vec4(diffuseColor, 1.0f) * lightAmount * shadowAmount) * 0.0001f + vec4(normal, 1) * 0.5 + 0.5;
	fragColor = (vec4(diffuseColor, 1.0f) * lightAmount * shadowAmount);
}
