#version 330

layout (location = 0) in vec3 position;

uniform mat4 projectedTransformation;

void main() {
	gl_Position = projectedTransformation * vec4(position, 1.0);
}