#version 330

out vec4 fragColor;

struct BaseLight {
    vec3 position;
	vec3 color;
	float intensity;
};

struct Attenuation {
	float quadratic;
	float linear;
	float constant;
};

struct PointLight {
	BaseLight baseLight;
	Attenuation attenuation;
	float dropOffFactor;
};

uniform mat4 inverseViewProjectionMatrix;
uniform sampler2D colorTexture;
uniform sampler2D normalTexture;
uniform sampler2D specularTexture;
uniform sampler2D depthTexture;
uniform vec3 cameraPosition;
uniform ivec2 screenSize;
uniform PointLight pointLight;

vec4 calcLight(vec3 lightDirection, vec3 normal, vec3 worldPosition, float specularIntensity, float specularExponent) {
	vec4 lightColor = vec4(pointLight.baseLight.color, 1.0f);
	float lightIntensity = pointLight.baseLight.intensity;

	// Diffuse calculation.
	float diffuseFactor = max(dot(normal, lightDirection), 0.0f);
    vec4 diffuseColor = (lightIntensity * diffuseFactor) * lightColor;

    // Specular calculation.
    vec3 directionToEye = normalize(cameraPosition - worldPosition);
    vec3 reflectDirection = normalize(reflect(-lightDirection, normal));

    float specularFactor = dot(directionToEye, reflectDirection);
    vec4 specularColor = vec4(0, 0, 0, 0);
    if (specularFactor > 0) {
        specularFactor = pow(specularFactor, specularExponent);
        specularColor = vec4(pointLight.baseLight.color * specularIntensity * specularFactor, 1.0f);
    }

	return diffuseColor + specularColor;
}

vec4 calcPointLight(vec3 normal, vec3 worldPosition, float specularIntensity, float specularExponent) {
    // Calculates the direction from the lights position to the fragment (world position).
	vec3 lightDirection = pointLight.baseLight.position - worldPosition;
	float distance = length(lightDirection);
	float distance2 = distance * distance;

	lightDirection = normalize(lightDirection);
	
	float attenuation = pointLight.attenuation.quadratic * distance2 +
						pointLight.attenuation.linear * distance +
						pointLight.attenuation.constant;
	//attenuation = max(1.0, attenuation);

    float dropOff = distance2 / pointLight.dropOffFactor;
	float dropOffR = dropOff * pointLight.baseLight.color.r;
	float dropOffG = dropOff * pointLight.baseLight.color.g;
	float dropOffB = dropOff * pointLight.baseLight.color.b;

	vec4 lit = calcLight(lightDirection, normal, worldPosition, specularIntensity, specularExponent);
	vec4 attenuatedLit = lit / attenuation;

	vec4 col =  max(vec4(0), vec4(
	    attenuatedLit.r - dropOffR,
	    attenuatedLit.g - dropOffG,
	    attenuatedLit.b - dropOffB,
	    attenuatedLit.a
	));
	if (col.r == 0 && col.g == 0 && col.b == 0) {
	    discard;
	} else {
	    return col;
	}
}

vec3 reconstructWorldPosition(vec2 texCoord) {
	float depth = texture(depthTexture, texCoord).x;
	depth = (depth * 2.0f) - 1.0f;

	vec2 ndc = (texCoord * 2.0f) - vec2(1.0f);
	vec4 screenSpacePosition = vec4(ndc, depth, 1.0f);
	vec4 worldSpacePosition = inverseViewProjectionMatrix * screenSpacePosition;

	return worldSpacePosition.xyz / worldSpacePosition.w;
}

/**
 * See: https://aras-p.info/texts/CompactNormalStorage.html
 */
vec3 decodeNormal(vec2 encodedNormal) {
    // Method #5: Cry Engine 3
    vec4 nn = vec4(encodedNormal, 1.0, -1.0);
    float l = dot(nn.xyz, -nn.xyw);
    nn.z = l;
    nn.xy *= sqrt(l);
    return nn.xyz * 2 - vec3(0, 0, 1);
}

/**
 * Returns a vec2 with values in the range [0 ... 1] perfectly suited to sample a texture with.
 * screenSize must be set to the actual size of the WINDOW, not the size of the underlying GBuffer!
 */
vec2 calcTexCoord() {
	return gl_FragCoord.xy / screenSize;
}

// TODO: Just calculate lighting information. Accumulate in accumulation buffer. Do not access the diffuse texture every time!
void main() {
	vec2 gBufferTexCoord = calcTexCoord();

	vec3 color = texture(colorTexture, gBufferTexCoord).xyz;

	// We normalize the normal vector so that the dot product between this normal and another vector returns values
	// from [0..1].
	//vec2 storedNormal = texture(normalTexture, gBufferTexCoord).rg;
    vec4 storedNormal = texture(normalTexture, gBufferTexCoord).rgba;
    //vec3 normal = decodeNormal(storedNormal * 2 - 1);
    //vec3 normal = decodeNormal(storedNormal);
    //vec3 normal = storedNormal * 2 - 1;
    //vec3 normal = storedNormal;
    vec3 normal = storedNormal.rgb * 2.0f - 1.0f;

	vec3 worldPosition = reconstructWorldPosition(gBufferTexCoord);

	vec2 specularData = texture(specularTexture, gBufferTexCoord).rg;
	float specularIntensity = specularData.r;
	float specularExponent = specularData.g;

	vec4 pointLight = calcPointLight(normal, worldPosition, specularIntensity, specularExponent);

	fragColor = vec4(color, 1.0f) * pointLight;
}