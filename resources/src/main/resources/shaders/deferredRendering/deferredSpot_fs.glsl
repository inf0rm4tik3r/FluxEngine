#version 330

out vec4 fragColor;

struct BaseLight {
	vec3 color;
	float intensity;
};

struct Attenuation {
	float constant;
	float linear;
	float exponent;
};

struct PointLight {
	BaseLight baseLight;
	Attenuation attenuation;
	vec3 position;
	float range;
};

uniform PointLight pointLight;
uniform sampler2D colorTexture;
uniform sampler2D normalTexture;
uniform sampler2D positionTexture;
uniform vec3 eyeWorldPosition;
uniform vec2 screenSize;
uniform float specularIntensity;
uniform float specularExponent;

vec4 calcLight(vec3 lightDirection, vec3 worldPosition, vec3 normal) {
	vec4 diffuseColor  = vec4(0, 0, 0, 0);
	vec4 specularColor = vec4(0, 0, 0, 0);
	
	float diffuseFactor = dot(normal, -lightDirection);
	
	//Wenn der Pixel beleuchtet wird...
	if(diffuseFactor > 0.0) {
		diffuseColor = vec4(pointLight.baseLight.color, 1.0) * pointLight.baseLight.intensity * diffuseFactor;
		
		//SpecularLight
		vec3 directionToEye = normalize(eyeWorldPosition - worldPosition);
		vec3 reflectDirection = normalize(reflect(lightDirection, normal));
		
		float specularFactor = pow(dot(directionToEye, reflectDirection), specularExponent);
		
		if(specularFactor > 0) {
			specularColor = vec4(pointLight.baseLight.color, 1.0) * pointLight.baseLight.intensity * specularIntensity * specularFactor;
		}
	}
	
	return diffuseColor + specularColor;
}

vec4 calcPointLight(vec3 worldPosition, vec3 normal) {
	vec3 lightDirection = worldPosition - pointLight.position;
	float distanceToPoint = length(lightDirection);
	//Pruefung auf Lichtreichweite hier gegenueber ForwardRendering nicht noetig,
	//da lediglich Kugel in Groesse der Lichtreichweite verarbeitet wird.
	
	if(distanceToPoint > pointLight.range)
		return vec4(0,0,0,0);
	
	lightDirection = normalize(lightDirection);
	
	float attenuation = pointLight.attenuation.exponent * distanceToPoint * distanceToPoint +
						pointLight.attenuation.linear * distanceToPoint + 
						pointLight.attenuation.constant;
	
	attenuation = max(1.0, attenuation);
	
	return calcLight(lightDirection, worldPosition, normal) / attenuation;
}

vec2 calcTexCoord() {
	return gl_FragCoord.xy / screenSize;
}

void main() {
	vec2 texCoord = calcTexCoord();
	vec3 color = texture(colorTexture, texCoord).xyz;
	vec3 normal = texture(normalTexture, texCoord).xyz;
	vec3 worldPosition = texture(positionTexture, texCoord).xyz;
	
	fragColor = vec4(color, 1.0) * calcPointLight(worldPosition, normal);
}