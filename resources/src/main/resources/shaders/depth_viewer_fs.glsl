#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;
in vec4 vsColor;

out vec4 color;

uniform sampler2D depthMap;
uniform float zNear = 1.0f;
uniform float zFar = 1000.0f;

float linearizeDepth(vec2 uv) {
    float depth = texture(depthMap, uv).x;
    return ((2.0 * zNear) / (zFar + zNear - depth * (zFar - zNear)));
}

void main(void) {
    //color = vec4(vec3(linearizeDepth(vsTexCoord)), 1.0);
    color = vec4(texture(depthMap, vsTexCoord).rgb, 1.0) + 0.0001f * linearizeDepth(vsTexCoord);
}