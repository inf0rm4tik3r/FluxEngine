#version 330 core

layout (location = 0) out float fragColor;

uniform ivec2 screenSize;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 inverseProjectionMatrix;

const int kernelSize = 16;

// The radius of the hemisphere used to smaple the scene.
uniform float sampleRadius = 0.5f;
uniform float samplePosDepthBias = 0.025f;
uniform float power = 1.0f;

uniform vec3[kernelSize] kernel;

uniform sampler2D normalTexture;
uniform sampler2D depthTexture;
uniform sampler2D noiseTexture;

uniform vec2 noiseTexCoordScale; // Must be set to vec2(screenSize.width / noiseTexture.width, ... )!

vec3 reconstructViewSpacePosition(vec2 texCoord, float currentDepth)
{
	float depth = currentDepth;
	depth = (depth * 2.0f) - 1.0f;

	vec2 ndc = (texCoord * 2.0f) - vec2(1.0f);
	vec4 screenSpacePosition = vec4(ndc, depth, 1.0f);
	vec4 viewSpacePosition = inverseProjectionMatrix * screenSpacePosition;

	return viewSpacePosition.xyz / viewSpacePosition.w;
}

vec2 convertViewSpaceToScreenSpace(vec3 viewSpaceVec, mat4 projectionMatrix)
{
    vec4 offset = vec4(viewSpaceVec, 1.0f);
    offset      = projectionMatrix * offset; // From view-space to screen-space.
    offset.xyz /= offset.w;                  // Perspective divide -> NDC.
    offset.xyz  = offset.xyz * 0.5f + 0.5f;  // Transform to range [0,..,1].
    return offset.xy;
}

vec3 transformWorldSpaceNormalToViewSpaceNormal(vec3 wsNormal)
{
    return (viewMatrix * vec4(wsNormal, 0.0f)).xyz;
}

/**
 * Returns a vec2 with values in the range [0 ... 1] perfectly suited to sample a texture with.
 * screenSize must be set to the size of the texture we are rendering into!
 */
vec2 calcTexCoord()
{
	return gl_FragCoord.xy / screenSize;
}

void main()
{
    vec2 texCoord = calcTexCoord();

    float currentDepth        = texture(depthTexture, texCoord).r; // In [0,1] range!
    vec3 fragViewSpacePos     = reconstructViewSpacePosition(texCoord, currentDepth);
    vec3 fragWorldSpaceNormal = normalize(texture(normalTexture, texCoord).rgb * 2.0f - 1.0f);
    vec3 fragViewSpaceNormal  = transformWorldSpaceNormalToViewSpaceNormal(fragWorldSpaceNormal);
    // A pseudo-random vector. As the noiseTexture uses GL_REPEAT, it is tiled over the screen.
    vec3 noiseVec             = texture(noiseTexture, texCoord * noiseTexCoordScale).xyz;

    vec3 tangent   = normalize(noiseVec - fragViewSpaceNormal * dot(noiseVec, fragViewSpaceNormal));
    vec3 bitangent = cross(fragViewSpaceNormal, tangent);
    mat3 TBN       = mat3(tangent, bitangent, fragViewSpaceNormal);

    float occlusion = 0;
    for(int i = 0; i < kernelSize; i++)
    {
        vec3 samplePos = TBN * kernel[i]; // From tangent to view-space.
        samplePos = fragViewSpacePos + samplePos * sampleRadius;

        vec2 samplePosScreenSpace = convertViewSpaceToScreenSpace(samplePos, projectionMatrix);

        float depthLookup = texture(depthTexture, samplePosScreenSpace).r;
        float sampleDepthFromViewer = reconstructViewSpacePosition(samplePosScreenSpace, depthLookup).z;

        float rangeCheck = smoothstep(0.0f, 1.0f, sampleRadius / abs(fragViewSpacePos.z - sampleDepthFromViewer));
        occlusion += (sampleDepthFromViewer >= samplePos.z + samplePosDepthBias ? 1.0f : 0.0f) * rangeCheck;
    }

    // Normalize the occlusion factor in the [0,..,1] range.
    float normalizedOcclusion = occlusion / kernelSize;

    // We want the amount of light which should "reach" any given fragment. We therefore subtract the occlusion from 1.
    float lightAmt = 1.0f - normalizedOcclusion;
    lightAmt = pow(lightAmt, power);

    fragColor = lightAmt;
}
