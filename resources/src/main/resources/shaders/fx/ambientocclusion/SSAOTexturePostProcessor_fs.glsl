#version 330 core

in vec2 vsTexCoord;

layout (location = 0) out float fragColor;

uniform sampler2D tex;
uniform vec2 texelSize;
uniform int noiseTextureDimension;

void main(void)
{
    float sum = 0.0;
    for (int x = -2; x < 2; x++)
    {
        for (int y = -2; y < 2; y++)
        {
            vec2 offset = vec2(x, y) * texelSize;
            sum += texture(tex, vsTexCoord + offset).r;
        }
    }
    float average = sum / (4.0f * 4.0f);
    fragColor = average;
}
