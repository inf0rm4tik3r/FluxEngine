#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;

out vec4 color;

uniform sampler2D tex;

void main(void) {
	color = vec4(vec3(1.0 - texture(tex, vsTexCoord)), 1.0);
}