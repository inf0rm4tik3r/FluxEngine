#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;

out vec4 color;

uniform sampler2D tex;
uniform mat3 kernel;
uniform vec2 offsets = vec2(1.0f / 1280.0f, 1.0f / 720.0f); // TODO: pass texel size !

void main(void) {
    vec3 col = vec3(0.0);
    
    col += vec3(texture(tex, vec2(vsTexCoord.s - offsets.s, vsTexCoord.t + offsets.t))) * kernel[0][0]; // top-left
    col += vec3(texture(tex, vec2(vsTexCoord.s            , vsTexCoord.t + offsets.t))) * kernel[0][1]; // top-center
    col += vec3(texture(tex, vec2(vsTexCoord.s + offsets.s, vsTexCoord.t + offsets.t))) * kernel[0][2]; // top-right
    
    col += vec3(texture(tex, vec2(vsTexCoord.s - offsets.s, vsTexCoord.t          ))) * kernel[1][0]; // center-left
    col += vec3(texture(tex, vec2(vsTexCoord.s            , vsTexCoord.t          ))) * kernel[1][1]; // center-center
    col += vec3(texture(tex, vec2(vsTexCoord.s + offsets.s, vsTexCoord.t          ))) * kernel[1][2]; // center-right
    
    col += vec3(texture(tex, vec2(vsTexCoord.s - offsets.s, vsTexCoord.t - offsets.t))) * kernel[2][2]; // bottom-left
    col += vec3(texture(tex, vec2(vsTexCoord.s            , vsTexCoord.t - offsets.t))) * kernel[2][2]; // bottom-center
    col += vec3(texture(tex, vec2(vsTexCoord.s + offsets.s, vsTexCoord.t - offsets.t))) * kernel[2][2]; // bottom-right
    
	color = vec4(col, 1.0);
}