#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;

out vec4 color;

uniform sampler2D tex;
uniform mat3 kernel;

const float offset = 1.0f / 300.0f;

const vec2 offsets[9] = vec2[](
    vec2(-offset, offset), // top-left
    vec2( 0.0f, offset), // top-center
    vec2( offset, offset), // top-right
    
    vec2(-offset, 0.0f), // center-left
    vec2( 0.0f, 0.0f), // center-center
    vec2( offset, 0.0f), // center-right
    
    vec2(-offset, -offset), // bottom-left
    vec2( 0.0f, -offset), // bottom-center
    vec2( offset, -offset) // bottom-right
);

void main(void) {
    
    vec3 col = vec3(0.0);
    
    for (int i = 0; i < 3; i++) {
        col += vec3(texture(tex, vsTexCoord + offsets[i])) * kernel[0][i];
    }
    for (int i = 0; i < 3; i++) {
        col += vec3(texture(tex, vsTexCoord + offsets[i+3])) * kernel[1][i];
    }
    for (int i = 0; i < 3; i++) {
        col += vec3(texture(tex, vsTexCoord + offsets[i+6])) * kernel[2][i];
    }
    
	color = vec4(col, 1.0);
}