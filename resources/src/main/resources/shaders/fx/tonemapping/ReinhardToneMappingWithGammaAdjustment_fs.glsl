#version 330 core

in vec2 vsTexCoord;

out vec4 fragColor;

uniform sampler2D hdrTex;
uniform float gamma = 2.2f;
uniform float exposure = 1.0f;

void main(void) {
    // Final color value in high dynamic range.
    vec3 hdrColor = texture(hdrTex, vsTexCoord).rgb;

    // Apply the Reinhard tone mapping.
    //vec3 mapped = hdrColor / (hdrColor + vec3(1.0f));
    vec3 mapped = vec3(1.0) - exp(-hdrColor * exposure);

    // Apply gamma correction.
    mapped = pow(mapped, vec3(1.0f / gamma));

    fragColor = vec4(mapped, 1.0f);
}
