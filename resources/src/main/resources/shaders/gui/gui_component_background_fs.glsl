#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;
in vec4 vsColor; // remove

out vec4 fragColor;

uniform ivec3 use;
uniform vec4 color;
uniform sampler2D imageTextureUnit;
uniform mat4 gradient;

void main(void) {
    bool useColor = use[0] != 0;
    bool useImage = use[1] != 0;
    bool useGradient = use[2] != 0;// irellevant

    if (useImage && !useColor) {
        fragColor += texture(imageTextureUnit, vsTexCoord);
    }
    else if (!useImage && useColor) {
        fragColor = color;
    }
    else if (useImage && useColor) {
        fragColor = texture(imageTextureUnit, vsTexCoord) * color;
    } else {
        fragColor = vec4(0.7f, 0.2f, 0.2f, 0.5f);
    }
}
