#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;
in vec4 vsColor;

out vec4 fragColor;

uniform sampler2D tex;
uniform vec4 color;

void main(void) {
    fragColor = texture(tex, vsTexCoord) * color;
}
