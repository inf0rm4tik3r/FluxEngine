#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;
layout (location = 3) in vec4 color;
layout (location = 4) in vec4 materialIndex;

uniform mat4 mvpMatrix;

out vec2 vsTexCoord;
out vec3 vsNormal;
out vec4 vsColor;

void main(void) {
	vsTexCoord = texCoord;
	vsNormal = normal;
	vsColor = color;

	gl_Position = mvpMatrix * vec4(position, 1);
}