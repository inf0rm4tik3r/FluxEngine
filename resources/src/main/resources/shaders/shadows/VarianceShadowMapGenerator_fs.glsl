#version 330 core

layout (location = 0) out vec3 varianceInfo;

void main()
{
    float depth = gl_FragCoord.z;
    float depthSquared = depth * depth;

    // Biasing the depthSquared.
    float dx = dFdx(depth);
    float dy = dFdy(depth);
    depthSquared += 0.25f * (dx * dx + dy * dy);

    // These two values will later become "average", "avariance" and "maximum depth".
    varianceInfo = vec3(depth, depthSquared, depth);
}