#version 330 core

in vec2 vsTexCoord;

layout (location = 0) out vec3 color;

uniform sampler2D tex;

uniform vec2 texelSize;

/**
 * This scale defines the axis on which the blur should be applied.
 */
uniform vec2 lookUpScale;

void main(void) {
    vec2 factor = lookUpScale * texelSize;

    vec3 tex1 = texture(tex, vsTexCoord + vec2(-4.0f) * factor).rgb;
    vec3 tex2 = texture(tex, vsTexCoord + vec2(-3.0f) * factor).rgb;
    vec3 tex3 = texture(tex, vsTexCoord + vec2(-2.0f) * factor).rgb;
    vec3 tex4 = texture(tex, vsTexCoord + vec2(-1.0f) * factor).rgb;
    vec3 tex5 = texture(tex, vsTexCoord + vec2( 0.0f) * factor).rgb;
    vec3 tex6 = texture(tex, vsTexCoord + vec2( 1.0f) * factor).rgb;
    vec3 tex7 = texture(tex, vsTexCoord + vec2( 2.0f) * factor).rgb;
    vec3 tex8 = texture(tex, vsTexCoord + vec2( 3.0f) * factor).rgb;
    vec3 tex9 = texture(tex, vsTexCoord + vec2( 4.0f) * factor).rgb;

    vec2 averages = vec2(0.0);
    averages += tex1.rg * 0.000229; // ( 1.0f / 64.0f)
    averages += tex2.rg * 0.005977; // ( 1.0f / 64.0f)
    averages += tex3.rg * 0.060598; // ( 6.0f / 64.0f)
    averages += tex4.rg * 0.241732; // (15.0f / 64.0f)
    averages += tex5.rg * 0.382928; // (20.0f / 64.0f)
    averages += tex6.rg * 0.241732; // (15.0f / 64.0f)
    averages += tex7.rg * 0.060598; // ( 6.0f / 64.0f)
    averages += tex8.rg * 0.005977; // ( 1.0f / 64.0f)
    averages += tex9.rg * 0.000229; // ( 1.0f / 64.0f)

    float maximum = max(tex1.b,
                        max(tex2.b,
                            max(tex3.b,
                                max(tex4.b,
                                    max(tex5.b,
                                        max(tex6.b,
                                            max(tex7.b,
                                                max(tex8.b, tex9.b)
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    );

    color = vec3(averages.x, averages.y, maximum);
}