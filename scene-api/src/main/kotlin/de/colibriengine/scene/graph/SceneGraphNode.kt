package de.colibriengine.scene.graph

import de.colibriengine.components.TransformCalculationComponent
import de.colibriengine.components.TransformComponent
import de.colibriengine.datastructures.list.DoublyLinkedListIterator
import de.colibriengine.datastructures.list.DoublyLinkedListNode
import de.colibriengine.ecs.Entity
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.util.Timer

interface SceneGraphNode : Comparable<SceneGraphNode> {

    val entity: Entity
    val transform: TransformComponent
    val transformCalculation: TransformCalculationComponent

    val childNodeIterator: DoublyLinkedListIterator<SceneGraphNode, DoublyLinkedListNode<SceneGraphNode>>

    fun add(child: SceneGraphNode)
    fun remove(child: SceneGraphNode)

    fun addParent(parent: SceneGraphNode)
    fun removeParent(parent: SceneGraphNode)

    fun updateNodeTransformation(orthographicParentMVP: Mat4fAccessor, perspectiveParentMVP: Mat4fAccessor)
    fun updateNode(timing: Timer.View)
    fun update(timing: Timer.View)

    fun destroyNode()
    fun destroy()

    override fun compareTo(other: SceneGraphNode): Int {
        return entity.entityId.compareTo(other.entity.entityId)
    }

}
