package de.colibriengine.scene

import de.colibriengine.ecs.Entity
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.ui.GUI

abstract class AbstractScene protected constructor(entity: Entity) : AbstractSceneGraphNode(entity) {

    val guis: ArrayList<GUI> = ArrayList(0)

    abstract fun destroyScene()

}
