package de.colibriengine.ui

import de.colibriengine.ecs.Entity
import de.colibriengine.ui.structure.AbstractGUIComponent
import de.colibriengine.util.Timer

class GUIRootNode(entity: Entity, parent: GUI) : AbstractGUIComponent(entity, "root", parent) {

    override fun init() {}
    override fun update(timing: Timer.View) {}
    override fun destroy() {}

}
