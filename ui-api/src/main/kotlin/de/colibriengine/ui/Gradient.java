package de.colibriengine.ui;

import de.colibriengine.math.vector.vec4f.ImmutableVec4f;
import de.colibriengine.math.vector.vec4f.StdImmutableVec4f;
import de.colibriengine.math.vector.vec4f.StdVec4fFactory;

public class Gradient {

    private ImmutableVec4f topLeft = StdVec4fFactory.Companion.getWHITE();
    private ImmutableVec4f topRight = StdVec4fFactory.Companion.getWHITE();
    private ImmutableVec4f bottomLeft = StdVec4fFactory.Companion.getWHITE();
    private ImmutableVec4f bottomRight = StdVec4fFactory.Companion.getWHITE();

    public static Gradient white() {
        return new Gradient().setAll(StdVec4fFactory.Companion.getWHITE());
    }

    public static Gradient black() {
        return new Gradient().setAll(StdVec4fFactory.Companion.getBLACK());
    }

    public static Gradient red() {
        return new Gradient().setAll(StdVec4fFactory.Companion.getRED());
    }

    public static Gradient green() {
        return new Gradient().setAll(StdVec4fFactory.Companion.getGREEN());
    }

    public static Gradient blue() {
        return new Gradient().setAll(StdVec4fFactory.Companion.getBLUE());
    }

    public static Gradient rgba(final float r, final float g, final float b, final float a) {
        return new Gradient().setAll(StdImmutableVec4f.Companion.builder().of(r, g, b, a));
    }

    public static Gradient rgb(final float r, final float g, final float b) {
        return new Gradient().setAll(StdImmutableVec4f.Companion.builder().of(r, g, b, 1));
    }

    public Gradient setAll(final ImmutableVec4f color) {
        this.topLeft = color;
        this.topRight = color;
        this.bottomLeft = color;
        this.bottomRight = color;
        return this;
    }

    public ImmutableVec4f getTopLeft() {
        return topLeft;
    }

    public void setTopLeft(final ImmutableVec4f topLeft) {
        this.topLeft = topLeft;
    }

    public ImmutableVec4f getTopRight() {
        return topRight;
    }

    public void setTopRight(final ImmutableVec4f topRight) {
        this.topRight = topRight;
    }

    public ImmutableVec4f getBottomLeft() {
        return bottomLeft;
    }

    public void setBottomLeft(final ImmutableVec4f bottomLeft) {
        this.bottomLeft = bottomLeft;
    }

    public ImmutableVec4f getBottomRight() {
        return bottomRight;
    }

    public void setBottomRight(final ImmutableVec4f bottomRight) {
        this.bottomRight = bottomRight;
    }

}
