package de.colibriengine.ui

import de.colibriengine.ui.components.Panel

class UIBuilder(
    private val uiComponentFactory: UIComponentFactory,
) {

    fun buildUI(name: String = "UI"): Panel {
        return uiComponentFactory.panel(name)
    }

}

