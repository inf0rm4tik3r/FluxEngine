package de.colibriengine.ui

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.font.TextFactory
import de.colibriengine.graphics.material.MaterialFactory
import de.colibriengine.graphics.model.Model
import de.colibriengine.graphics.model.ModelFactory
import de.colibriengine.ui.components.Panel

interface UIComponentFactory {

    val mother: GUI

    val ecs: ECS
    val textFactory: TextFactory
    val materialFactory: MaterialFactory
    val modelFactory: ModelFactory

    /** Camera facing quad. Suited for orthographic rendering. No rotation necessary. */
    val quad: Model

    fun panel( name: String): Panel

}
