package de.colibriengine.ui.components

import de.colibriengine.ui.Gradient
import de.colibriengine.math.vector.vec3f.Vec3f

interface Background {

    var useColor: Boolean
    var useGradient: Boolean
    var useImage: Boolean

    val color: Vec3f
    var opacity: Float

    var gradient: Gradient

}
