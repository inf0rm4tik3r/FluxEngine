package de.colibriengine.ui.structure

import de.colibriengine.ecs.Entity
import de.colibriengine.ui.GUI
import de.colibriengine.ui.structure.anchoring.Anchor
import de.colibriengine.ui.structure.anchoring.AnchorPosition
import de.colibriengine.ui.structure.anchoring.Anchors
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.scene.graph.SceneGraphNode
import de.colibriengine.util.PhysicalUnit
import kotlin.math.max
import kotlin.math.min

abstract class AbstractGUIComponent(entity: Entity, val name: String, var mother: GUI) :
    AbstractSceneGraphNode(entity) {

    /** Stores the anchors which links this component to others. */
    private val anchors: Anchors = Anchors()

    // Position
    private var x = UNSET
    private var y = UNSET
    private var z = UNSET

    // Dimension
    var width = UNSET
        private set
    var widthUnit = PhysicalUnit.PX
        private set
    var height = UNSET
        private set
    var heightUnit = PhysicalUnit.PX
        private set
    private var depth = UNSET
    private var depthUnit = PhysicalUnit.PX

    // Margins
    var marginTop = MARGIN_AUTO
        private set
    var marginTopUnit = PhysicalUnit.PX
        private set
    var marginBottom = MARGIN_AUTO
        private set
    var marginBottomUnit = PhysicalUnit.PX
        private set
    var marginLeft = MARGIN_AUTO
        private set
    var marginLeftUnit = PhysicalUnit.PX
        private set
    var marginRight = MARGIN_AUTO
        private set
    var marginRightUnit = PhysicalUnit.PX
        private set
    var isTransformCalculated = false
        private set
    private val tmpTopRight: Vec3f = StdVec3f()
    private val tmpBottomRight: Vec3f = StdVec3f()
    private val tmpBottomLeft: Vec3f = StdVec3f()
    private val tmpTopLeft: Vec3f = StdVec3f()

    override fun add(child: SceneGraphNode) {
        val casted = child as AbstractGUIComponent
        super.add(casted)
        mother.added(casted)
    }

    override fun remove(child: SceneGraphNode) {
        val casted = child as AbstractGUIComponent
        super.remove(casted)
        mother.removed(casted)
    }

    /**
     * Releases all data of this GUI component. Should be called before this instance gets garbage collected.
     */
    override fun destroy() {
        //anchorList.forEach(Anchor::free);
    }

    fun invalidateTransform() {
        isTransformCalculated = false
    }

    private fun allAnchorsCalculated(): Boolean {
        return (anchors.topLeft == null || anchors.topLeft!!.target.isTransformCalculated) and
            (anchors.topRight == null || anchors.topRight!!.target.isTransformCalculated) and
            (anchors.bottomLeft == null || anchors.bottomLeft!!.target.isTransformCalculated) and
            (anchors.bottomRight == null || anchors.bottomRight!!.target.isTransformCalculated)

        // TODO: What about just calling anchors.?.getTarget().calculateTransform() for each non calculated???
    }

    fun calculateTransform(): Boolean {
        // Check if all components this component is anchored to have their transform calculated.
        if (!allAnchorsCalculated()) {
            /*LOG.warn(
                "Unable to calculate transform of {}. Anchored component was not jet calculated or no anchors are " +
                "present!", this
            );*/
            // TODO: INVESTIGATE !
            return false
        }

        // Calculate the transform of this object based on its anchors and own parameters.
        val topLeft = tmpTopLeft
        var topLeftSet = false
        val topRight = tmpTopRight
        var topRightSet = false
        val bottomLeft = tmpBottomLeft
        var bottomLeftSet = false
        val bottomRight = tmpBottomRight
        var bottomRightSet = false

        // --- 1 ---
        // Find the points which define the rectangle of this component.
        if (anchors.topLeft != null) {
            setPointBasedOnAnchorTarget(
                topLeft, anchors.topLeft!!.target, anchors.topLeft!!.targetAnchorPosition
            )
            topLeftSet = true
        }
        if (anchors.topRight != null) {
            setPointBasedOnAnchorTarget(
                topRight, anchors.topRight!!.target, anchors.topRight!!.targetAnchorPosition
            )
            topRightSet = true
        }
        if (anchors.bottomLeft != null) {
            setPointBasedOnAnchorTarget(
                bottomLeft, anchors.bottomLeft!!.target, anchors.bottomLeft!!.targetAnchorPosition
            )
            bottomLeftSet = true
        }
        if (anchors.bottomRight != null) {
            setPointBasedOnAnchorTarget(
                bottomRight, anchors.bottomRight!!.target, anchors.bottomRight!!.targetAnchorPosition
            )
            bottomRightSet = true
        }
        // Some (maybe all) of the points are still unset! We have to resolve the missing values.

        // --- 1b ---
        // Calculate the available width and height.
        val minX = min(
            if (topLeftSet) topLeft.x else Float.MAX_VALUE,
            if (bottomLeftSet) bottomLeft.x else Float.MAX_VALUE
        )
        val maxX = max(
            if (topRightSet) topRight.x else -Float.MAX_VALUE,
            if (bottomRightSet) bottomRight.x else -Float.MAX_VALUE
        )
        val minY = min(
            if (bottomLeftSet) bottomLeft.y else Float.MAX_VALUE,
            if (bottomRightSet) bottomRight.y else Float.MAX_VALUE
        )
        val maxY = max(
            if (topLeftSet) topLeft.y else -Float.MAX_VALUE,
            if (topRightSet) topRight.y else -Float.MAX_VALUE
        )
        val availableWidth = if (maxX != -Float.MAX_VALUE && minX != Float.MAX_VALUE) maxX - minX else UNSET
        val availableHeight = if (maxY != -Float.MAX_VALUE && minY != Float.MAX_VALUE) maxY - minY else UNSET
        val availableWidthSet = availableWidth != UNSET
        val availableHeightSet = availableHeight != UNSET

        // --- 1c ---
        // The width and height are needed for part 2. If they were not set, use the maximum available width / height.
        val preferredWidth = if (width != UNSET) width else if (availableWidthSet) availableWidth else UNSET
        val preferredHeight = if (height != UNSET) height else if (availableHeightSet) availableHeight else UNSET
        val preferredWidthSet = preferredWidth != UNSET
        val preferredHeightSet = preferredHeight != UNSET

        // --- 2 ---
        // Calculate the margins to use.
        val mLCalculated: Float
        val mRCalculated: Float
        val mTCalculated: Float
        val mBCalculated: Float

        // Calculate left and right margin.
        if (availableWidthSet and preferredWidthSet) {
            if (marginLeft == MARGIN_AUTO && marginRight == MARGIN_AUTO) {
                val halfMargin = (availableWidth - preferredWidth) / 2
                mLCalculated = halfMargin
                mRCalculated = halfMargin
            } else if (marginLeft != MARGIN_AUTO && marginRight == MARGIN_AUTO) {
                mLCalculated = calculateMargin(availableWidth, marginLeft, marginLeftUnit)
                mRCalculated = max(availableWidth - mLCalculated - preferredWidth, 0f)
            } else if (marginLeft == MARGIN_AUTO) { //& marginRight != MARGIN_AUTO) {
                mRCalculated = calculateMargin(availableWidth, marginRight, marginRightUnit)
                mLCalculated = max(availableWidth - mRCalculated - preferredWidth, 0f)
            } else { //if (marginLeft != MARGIN_AUTO & marginRight != MARGIN_AUTO) {
                mLCalculated = calculateMargin(availableWidth, marginLeft, marginLeftUnit)
                mRCalculated = calculateMargin(availableWidth, marginRight, marginRightUnit)
                if (mLCalculated + mRCalculated >= availableWidth) {
                    LOG.warn("LeftMargin + RightMargin exceeds available width!")
                }
            }
        } else {
            mLCalculated = 0f
            mRCalculated = 0f
        }

        // Calculate top and bottom margin.
        if (availableHeightSet and preferredHeightSet) {
            if (marginTop == MARGIN_AUTO && marginBottom == MARGIN_AUTO) {
                val halfMargin = (availableHeight - preferredHeight) / 2
                mTCalculated = halfMargin
                mBCalculated = halfMargin
            } else if (marginTop != MARGIN_AUTO && marginBottom == MARGIN_AUTO) {
                mTCalculated = calculateMargin(availableHeight, marginTop, marginTopUnit)
                mBCalculated = max(availableHeight - mTCalculated - preferredHeight, 0f)
            } else if (marginTop == MARGIN_AUTO) { //& marginBottom != MARGIN_AUTO) {
                mBCalculated = calculateMargin(availableHeight, marginBottom, marginBottomUnit)
                mTCalculated = max(availableHeight - mBCalculated - preferredHeight, 0f)
            } else { //if (marginTop != MARGIN_AUTO & marginBottom != MARGIN_AUTO) {
                mTCalculated = calculateMargin(availableHeight, marginTop, marginTopUnit)
                mBCalculated = calculateMargin(availableHeight, marginBottom, marginBottomUnit)
                if (mTCalculated + mBCalculated >= availableHeight) {
                    LOG.warn("TopMargin + BottomMargin exceeds available height!")
                }
            }
        } else {
            mTCalculated = 0f
            mBCalculated = 0f
        }

        // LOG.info(name + " mL: " + mLCalculated + ", mT: " + mTCalculated + ", mR: " + mRCalculated + ", mB: " + mBCalculated);

        // --- 3 ---
        // Update the points based on the calculated information.
        if (topLeftSet) {
            topLeft.x = topLeft.x + mLCalculated
            topLeft.y = topLeft.y - mTCalculated
        }
        if (topRightSet) {
            topRight.x = topRight.x - mRCalculated
            topRight.y = topRight.y - mTCalculated
        }
        if (bottomLeftSet) {
            bottomLeft.x = bottomLeft.x + mLCalculated
            bottomLeft.y = bottomLeft.y + mBCalculated
        }
        if (bottomRightSet) {
            bottomRight.x = bottomRight.x - mRCalculated
            bottomRight.y = bottomRight.y + mBCalculated
        }

        // --- 3b ---
        // Calculate the actual height scale. TODO:


        // --- 4 ---
        // Set the transform object of this component.
        // Only TOP_LEFT
        if (topLeftSet and !topRightSet and !bottomLeftSet and !bottomRightSet) {
            transform.setTranslation(
                topLeft.x,
                topLeft.y,
                topLeft.z
            )
            transform.scale.set(
                preferredWidth,
                preferredHeight, 1f
            )
        } else if (!topLeftSet and topRightSet and !bottomLeftSet and !bottomRightSet) {
            transform.setTranslation(
                topRight.x - preferredWidth,
                topRight.y,
                topRight.z
            )
            transform.scale.set(
                preferredWidth,
                preferredHeight, 1f
            )
        } else if (!topLeftSet and !topRightSet and bottomLeftSet and !bottomRightSet) {
            transform.setTranslation(
                bottomLeft.x,
                bottomLeft.y + preferredHeight,
                bottomLeft.z
            )
            transform.scale.set(
                preferredWidth,
                preferredHeight, 1f
            )
        } else if (!topLeftSet and !topRightSet and !bottomLeftSet and bottomRightSet) {
            transform.setTranslation(
                bottomRight.x - preferredWidth,
                bottomRight.y + preferredHeight,
                bottomRight.z
            )
            transform.scale.set(
                preferredWidth,
                preferredHeight, 1f
            )
        } else if (topLeftSet and !topRightSet and !bottomLeftSet and bottomRightSet) {
            transform.setTranslation(
                topLeft.x,
                topLeft.y,
                topLeft.z
            )
            transform.scale.set(
                bottomRight.x - topLeft.x,
                topLeft.y - bottomRight.y, 1f
            )
        } else if (!topLeftSet and !topRightSet and bottomLeftSet and bottomRightSet) {
            transform.setTranslation(
                topLeft.x,
                topLeft.y,
                topLeft.z
            )
            transform.scale.set(
                bottomRight.x - topLeft.x,
                topLeft.y - bottomRight.y, 1f
            )
        } else {
            if (DEBUG) {
                LOG.warn("Fallthrough on: $this")
            }
            // x, y and z must be present.
            check(!(x == UNSET || y == UNSET || z == UNSET)) { MSG_NO_LAYOUT_OPTIONS_SPECIFIED }
            // width, height and depth must be present.
            check(!(width == UNSET || height == UNSET || depth == UNSET)) { MSG_NO_LAYOUT_OPTIONS_SPECIFIED }
            transform.setTranslation(x, y, z)
            transform.scale.set(width, height, depth)
        }

        // Calculate all the matrices.
        val orthographicVP = mother.orthographicVP
        val perspectiveVP = mother.perspectiveVP
        //if (orthographicVP != null && perspectiveVP != null) {
        updateNodeTransformation(orthographicVP, perspectiveVP)
        //}
        if (DEBUG) {
            LOG.debug("Calculated: {}", this)
            LOG.debug("availableWidth: {}", availableWidth)
            LOG.debug("availableWidthSet: {}", availableWidthSet)
            LOG.debug("availableHeight: {}", availableHeight)
            LOG.debug("availableHeightSet: {}", availableHeightSet)
            LOG.debug("preferredWidth: {}", preferredWidth)
            LOG.debug("preferredWidthSet: {}", preferredWidthSet)
            LOG.debug("preferredHeight: {}", preferredHeight)
            LOG.debug("preferredHeightSet: {}", preferredHeightSet)
            LOG.debug("mLCalculated: {}", mLCalculated)
            LOG.debug("mRCalculated: {}", mRCalculated)
            LOG.debug("mTCalculated: {}", mTCalculated)
            LOG.debug("mBCalculated: {}", mBCalculated)
            LOG.debug(transform.translation)
            LOG.debug(transform.scale)
        }

        // The transform got calculated. isTransformCalculated must now return true.
        isTransformCalculated = true

        // Inform the caller that the calculation was successful.
        return true
    }

    private fun calculateMargin(availableSpace: Float, margin: Float, unit: PhysicalUnit): Float {
        return when (unit) {
            PhysicalUnit.PX -> margin
            PhysicalUnit.PERCENT -> (margin / 100.0f) * availableSpace
            else -> throw RuntimeException(MSG_UNEXPECTED_UNIT + unit)
        }
    }

    private fun setPointBasedOnAnchorTarget(
        point: Vec3f,
        target: AbstractGUIComponent,
        targetPosition: AnchorPosition
    ) {
        /* TODO: getScale() does not return values in pixel space. Convert! */
        when (targetPosition) {
            AnchorPosition.TOP_LEFT -> {
                point.x = target.transform.translation.x
                point.y = target.transform.translation.y
                point.z = target.transform.translation.z
            }
            AnchorPosition.TOP_RIGHT -> {
                point.x = target.transform.translation.x + target.transform.scale.x
                point.y = target.transform.translation.y
                point.z = target.transform.translation.z
            }
            AnchorPosition.BOTTOM_LEFT -> {
                point.x = target.transform.translation.x
                point.y = target.transform.translation.y - target.transform.scale.y
                point.z = target.transform.translation.z
            }
            AnchorPosition.BOTTOM_RIGHT -> {
                point.x = target.transform.translation.x + target.transform.scale.x
                point.y = target.transform.translation.y - target.transform.scale.y
                point.z = target.transform.translation.z
            }
        }
    }

    override fun updateNodeTransformation(orthographicParentMVP: Mat4fAccessor, perspectiveParentMVP: Mat4fAccessor) {
        computeFinalTransformation(orthographicParentMVP, perspectiveParentMVP)
    }

    /** Defines an anchor between this element and the {@code target}. */
    fun anchor(
        sourceAnchor: AnchorPosition,
        target: AbstractGUIComponent,
        targetAnchor: AnchorPosition
    ) {
        require(target != this) { "Cannot anchor to itself!" }
        when (sourceAnchor) {
            AnchorPosition.TOP_LEFT -> anchors.topLeft = Anchor(sourceAnchor, target, targetAnchor)
            AnchorPosition.TOP_RIGHT -> anchors.topRight = Anchor(sourceAnchor, target, targetAnchor)
            AnchorPosition.BOTTOM_LEFT -> anchors.bottomLeft = Anchor(sourceAnchor, target, targetAnchor)
            AnchorPosition.BOTTOM_RIGHT -> anchors.bottomRight = Anchor(sourceAnchor, target, targetAnchor)
        }
    }

    fun x(x: Float) {
        this.x = x
    }

    fun y(y: Float) {
        this.y = y
    }

    fun z(z: Float) {
        this.z = z
    }

    fun width(width: Float, widthUnit: PhysicalUnit) {
        this.width = width
        this.widthUnit = widthUnit
    }

    fun height(height: Float, heightUnit: PhysicalUnit) {
        this.height = height
        this.heightUnit = heightUnit
    }

    fun depth(depth: Float, depthUnit: PhysicalUnit) {
        this.depth = depth
        this.depthUnit = depthUnit
    }

    fun margin(
        marginTop: Float, marginTopUnit: PhysicalUnit,
        marginBottom: Float, marginBottomUnit: PhysicalUnit,
        marginLeft: Float, marginLeftUnit: PhysicalUnit,
        marginRight: Float, marginRightUnit: PhysicalUnit
    ) {
        marginTop(marginTop, marginTopUnit)
        marginBottom(marginBottom, marginBottomUnit)
        marginLeft(marginLeft, marginLeftUnit)
        marginRight(marginRight, marginRightUnit)
    }

    fun margin(
        marginTopBottom: Float, marginTopBottomUnit: PhysicalUnit,
        marginLeftRight: Float, marginLeftUnitRight: PhysicalUnit
    ) {
        marginTop(marginTopBottom, marginTopBottomUnit)
        marginBottom(marginTopBottom, marginTopBottomUnit)
        marginLeft(marginLeftRight, marginLeftUnitRight)
        marginRight(marginLeftRight, marginLeftUnitRight)
    }

    fun marginTop(marginTop: Float, marginTopUnit: PhysicalUnit) {
        this.marginTop = marginTop
        this.marginTopUnit = marginTopUnit
    }

    fun marginBottom(marginBottom: Float, marginBottomUnit: PhysicalUnit) {
        this.marginBottom = marginBottom
        this.marginBottomUnit = marginBottomUnit
    }

    fun marginLeft(marginLeft: Float, marginLeftUnit: PhysicalUnit) {
        this.marginLeft = marginLeft
        this.marginLeftUnit = marginLeftUnit
    }

    fun marginRight(marginRight: Float, marginRightUnit: PhysicalUnit) {
        this.marginRight = marginRight
        this.marginRightUnit = marginRightUnit
    }

    companion object {
        private const val MSG_UNEXPECTED_UNIT = "This unit was not expected / could not be processed: "
        private const val MSG_NO_LAYOUT_OPTIONS_SPECIFIED = "No layout options specified!"
        private val LOG = getLogger(AbstractGUIComponent::class.java)
        private const val DEBUG = false
        private const val UNSET = -Float.MAX_VALUE
        private const val MARGIN_AUTO = Float.MAX_VALUE
    }

}
