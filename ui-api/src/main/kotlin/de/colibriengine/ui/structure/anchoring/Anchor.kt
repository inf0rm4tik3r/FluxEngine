package de.colibriengine.ui.structure.anchoring

import de.colibriengine.ui.structure.AbstractGUIComponent

data class Anchor(

    /** The anchor position of the [AbstractGUIComponent] which possesses this anchor. */
    val sourceAnchorPosition: AnchorPosition,

    /** The [AbstractGUIComponent] which gets targeted by this anchor. */
    val target: AbstractGUIComponent,

    /** The anchor position of the target [AbstractGUIComponent]. */
    val targetAnchorPosition: AnchorPosition

)
