package de.colibriengine.ui.components

import de.colibriengine.ecs.Entity
import de.colibriengine.ui.GUI
import de.colibriengine.ui.structure.AbstractGUIComponent
import de.colibriengine.util.Timer

class Canvas(entity: Entity, mother: GUI) : AbstractGUIComponent(entity, "Canvas", mother) {

    override fun init() {}
    override fun update(timing: Timer.View) {}
    override fun destroy() {}

}
