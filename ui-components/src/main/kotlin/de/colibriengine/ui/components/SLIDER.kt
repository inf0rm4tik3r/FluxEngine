package de.colibriengine.ui.components

import de.colibriengine.ecs.ECS
import de.colibriengine.ui.GUI
import de.colibriengine.ui.structure.AbstractGUIComponent
import de.colibriengine.util.Timer
import kotlin.math.roundToInt

/**
 * A movable knob on a bar.
 * Should be easily implemented.
 * Render a long div (in any direction!)
 * Render a knob (centered on the div, as a point?)
 *
 * Possible UI events:
 * onClick, onRightClick, onDragStart, onDrag, onDragEnd
 *
 * EventSystem:
 * Click inside viewport:
 * -> Engine determines UI element that was clicked and sends click event
 * -> Mouse is no released in next update iteration and mouse moved -> Engine sends onDrag(Start) event
 * -> Mouse moved in next update iteration -> Engine send onDrag event
 *      event contains: new position of the mouse
 * -> Mouse is released -> Engine sends onDragEndEvent, Engine sends onClickEnd event (to original element!)
 *
 * How to translate mouse-movement to new slider values:
 *
 * transform the sliders start and end planes into clip -> persp divide -> then screen space using viewport settings
 * calculate line between planes
 * project cursor on line
 * calculate distance from start (percentage 0..1) -> use it to update the sliders value!
 */
class SLIDER(ecs: ECS, mother: GUI, name: String = "SLIDER") :
    AbstractGUIComponent(ecs.createEntity(), name, mother) {


    var range: ClosedFloatingPointRange<Float> = 0f..1f
    var step: Float = 0f
        set(value) {
            require(value >= 0)
            require(value <= range.endInclusive)
            field = value
        }
    var value: Float = 0f
        set(t) {
            field = (t / step).roundToInt() * step
        }

    init {
        step = .1f
        value = range.start
    }

    override fun init() {
    }

    override fun update(timing: Timer.View) {
    }

}
