package de.colibriengine.ui.widgets

import de.colibriengine.graphics.font.BitmapFont
import de.colibriengine.graphics.font.TextMode.FIXED
import de.colibriengine.ui.UIComponentFactory
import de.colibriengine.ui.components.PanelImpl
import de.colibriengine.ui.components.TEXT2D
import de.colibriengine.ui.structure.anchoring.AnchorPosition.*
import de.colibriengine.util.PhysicalUnit.PERCENT
import de.colibriengine.util.ReactiveCpuReporter
import de.colibriengine.util.ReactiveSystemMemoryReporter
import de.colibriengine.util.Timer
import de.colibriengine.util.UnitConverter.byteToMebibyte
import kotlin.math.roundToInt

class FrameInfo(uiComponentFactory: UIComponentFactory, font: BitmapFont) :
    PanelImpl(
        uiComponentFactory.ecs,
        uiComponentFactory.textFactory,
        uiComponentFactory.materialFactory,
        uiComponentFactory.quad,
        uiComponentFactory.mother,
        "FrameInfo"
    ) {

    private val timer: Timer = Timer()
    private val cpuReporter: ReactiveCpuReporter = ReactiveCpuReporter()
    private val memoryReporter: ReactiveSystemMemoryReporter = ReactiveSystemMemoryReporter()
    private val fontSize = 1f

    private lateinit var fpsText: TEXT2D
    private lateinit var frameTimeText: TEXT2D

    init {
        background.useColor = true
        background.color.set(0f, 0f, 0f)
        background.opacity = 0.8f

        val topRight = panel {
            anchor(TOP_RIGHT, this@FrameInfo, TOP_RIGHT)
            width(100f, PERCENT)
            height(40f, PERCENT)

            text2D(FIXED, 2u, font) {
                anchor(TOP_RIGHT, this@panel, TOP_RIGHT)
                text.update { put("TR") }
                size = fontSize
                color.set(1f, 0f, 1f, 1f)
            }
        }

        val bottomRight = panel {
            anchor(BOTTOM_RIGHT, this@FrameInfo, BOTTOM_RIGHT)
            width(100f, PERCENT)
            height(40f, PERCENT)

            text2D(FIXED, 2u, font) {
                anchor(BOTTOM_RIGHT, this@panel, BOTTOM_RIGHT)
                text.update { put("BR") }
                size = fontSize
                color.set(1f, 0f, 1f, 1f)
            }
        }

        val bottomLeft = panel {
            anchor(BOTTOM_LEFT, this@FrameInfo, BOTTOM_LEFT)
            width(100f, PERCENT)
            height(40f, PERCENT)

            text2D(FIXED, 2u, font) {
                anchor(BOTTOM_LEFT, this@panel, BOTTOM_LEFT)
                text.update { put("BL") }
                size = fontSize
                color.set(1f, 0f, 1f, 1f)
            }
        }

        val topLeft = panel {
            anchor(TOP_LEFT, this@FrameInfo, TOP_LEFT)
            width(100f, PERCENT)
            height(40f, PERCENT)

            text2D(FIXED, 2u, font) {
                anchor(TOP_LEFT, this@panel, TOP_LEFT)
                text.update { put("TL") }
                size = fontSize
                color.set(1f, 0f, 1f, 1f)
            }

            val fps = panel fps@ {
                anchor(TOP_LEFT, this@panel, TOP_LEFT)
                width(100f, PERCENT)
                height(40f, PERCENT)
                background.color.set(1f, 1f, 1f)

                fpsText = text2D(FIXED, 23u, font) {
                    anchor(TOP_LEFT, this@fps, TOP_LEFT)
                    text.update { put("        FPS:") }
                    color.set(1.0f, 0.0f, 1.0f, 1.0f)
                    size = fontSize
                }
            }

            val lastFrameTime = panel lastFrameTime@ {
                anchor(TOP_LEFT, fps, BOTTOM_LEFT)
                width(100f, PERCENT)
                height(40f, PERCENT)

                frameTimeText = text2D(FIXED, 23u, font) {
                    anchor(TOP_LEFT, this@lastFrameTime, TOP_LEFT)
                    text.update {
                        put("  FrameTime:")
                        putAtEnd("ms")
                    }
                    color.set(1f, 1f, 0f, 1f)
                    size = fontSize
                }
            }

            val cpuProc = panel cpuProc@ {
                anchor(TOP_LEFT, lastFrameTime, BOTTOM_LEFT)
                width(100f, PERCENT)
                height(40F, PERCENT)

                text2D(FIXED, 23u, font) {
                    anchor(TOP_LEFT, this@cpuProc, TOP_LEFT)
                    size = fontSize
                    text.update {
                        put("CPU [ proc]:")
                        putAtEnd("%")
                    }
                    cpuReporter.processCpuLoad.subscribe {
                        text.update {
                            clear(' ', 13, length.toInt() - 13 - 1)
                            putAtEnd((it * 100.0).roundToInt(), 1)
                        }
                    }
                }
            }

            val cpuSys = panel cpuSys@ {
                anchor(TOP_LEFT, cpuProc, BOTTOM_LEFT)
                width(100f, PERCENT)
                height(40F, PERCENT)

                text2D(FIXED, 23u, font) {
                    anchor(TOP_LEFT, this@cpuSys, TOP_LEFT)
                    size = fontSize
                    text.update {
                        put("CPU [  sys]:")
                        putAtEnd("%")
                    }
                    cpuReporter.systemCpuLoad.subscribe {
                        text.update {
                            clear(' ', 13, length.toInt() - 13 - 1)
                            putAtEnd((it * 100.0).roundToInt(), 1)
                        }
                    }
                }
            }

            val totalMemory = panel totalMemory@ {
                anchor(TOP_LEFT, cpuSys, BOTTOM_LEFT)
                width(100f, PERCENT)
                height(40F, PERCENT)

                text2D(FIXED, 23u, font) {
                    anchor(TOP_LEFT, this@totalMemory, TOP_LEFT)
                    size = fontSize
                    text.update {
                        put("Mem [total]:")
                        putAtEnd("mb")
                    }
                    memoryReporter.total.subscribe {
                        text.update {
                            clear(' ', 13, length.toInt() - 13 - 2)
                            putAtEnd(byteToMebibyte(it), 2, 2)
                        }
                    }
                }
            }

            val usedMemory = panel usedMemory@ {
                anchor(TOP_LEFT, totalMemory, BOTTOM_LEFT)
                width(100f, PERCENT)
                height(40F, PERCENT)

                text2D(FIXED, 23u, font) {
                    anchor(TOP_LEFT, this@usedMemory, TOP_LEFT)
                    size = fontSize
                    text.update {
                        put("Mem [ used]:")
                        putAtEnd("mb")
                    }
                    memoryReporter.used.subscribe {
                        text.update {
                            clear(' ', 13, length.toInt() - 13 - 2)
                            putAtEnd(byteToMebibyte(it), 2, 2)
                        }
                    }
                }
            }

            val freeMemory = panel freeMemory@ {
                anchor(TOP_LEFT, usedMemory, BOTTOM_LEFT)
                width(100f, PERCENT)
                height(40F, PERCENT)

                text2D(FIXED, 23u, font) {
                    anchor(TOP_LEFT, this@freeMemory, TOP_LEFT)
                    size = fontSize
                    text.update {
                        put("Mem [ free]:")
                        putAtEnd("mb")
                    }
                    memoryReporter.free.subscribe {
                        text.update {
                            clear(' ', 13, length.toInt() - 13 - 2)
                            putAtEnd(byteToMebibyte(it), 2, 2)
                        }
                    }
                }
            }

            val slider = slider {
                anchor(TOP_LEFT, freeMemory, BOTTOM_LEFT)
                width(100f, PERCENT)
                height(40F, PERCENT)
            }
        }

    }

    override fun init() {}

    override fun update(timing: Timer.View) {
        timer.tick()
        if (timer.view.elapsedTimeSP() >= 0.5) {
            /*
            runBlocking {
                launch {
                    cpuReporter.updateState()
                }
            }
            cpuReporter.update()
            */
            memoryReporter.update()

            fpsText.text.update {
                val fps: Long = timer.view.ticks() * 2
                clear(' ', 13)
                putAtEnd(fps.coerceAtMost(9999))
            }
            timer.resetElapsedTime()
            timer.resetTicks()
            timer.resetMaxDelta()
        }

        frameTimeText.text.update {
            val ms: Double = timer.view.deltaInSeconds()
            clear(' ', 13, length.toInt() - 13 - 2)
            putAtEnd(ms.coerceAtMost(999.9), 4, 2)
        }
    }

    override fun destroy() {}

}
