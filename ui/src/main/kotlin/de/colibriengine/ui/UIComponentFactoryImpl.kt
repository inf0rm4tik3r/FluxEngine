package de.colibriengine.ui

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.font.TextFactory
import de.colibriengine.graphics.material.MaterialFactory
import de.colibriengine.graphics.model.ModelFactory
import de.colibriengine.graphics.model.concrete.QuadAlignment
import de.colibriengine.graphics.model.concrete.QuadDirection
import de.colibriengine.ui.components.PanelImpl
import de.colibriengine.ui.components.Panel

class UIComponentFactoryImpl(
    override val mother: GUI,
    override val ecs: ECS,
    override val textFactory: TextFactory,
    override val materialFactory: MaterialFactory,
    override val modelFactory: ModelFactory
) : UIComponentFactory {

    override val quad = modelFactory.quad(QuadDirection.XY, QuadAlignment.TOP_LEFT_ALIGNED)

    override fun panel(name: String): Panel {
        return PanelImpl(ecs, textFactory, materialFactory, quad, mother, name)
    }

}
