abstract class Component {
    var id: Int = -1

    var parent: Component? = null

    var children: MutableList<Component> = mutableListOf()
    var childrenIndex: MutableMap<Int, Int> = mutableMapOf()
    var currentIndex = 0

    var needsUpdate = false
    var updated = false

    var onClick: suspend (ClickEvent) -> Unit = {
        println("default onClick handler")
    }

    suspend fun processClick(clickEvent: ClickEvent): Unit {
        println("processClick")
        onClick(clickEvent)
        if (!clickEvent.stopPropagation) {
            parent?.processClick(clickEvent)
        }
    }

    fun updateIfNecessary() {
        if (!updated) {
            update()
        }
    }

    fun update() {
        currentIndex = 0
        // Rebuild the ui.
        ui()
        // Destroy every unseen child, as it was no longer included!
        destroyUnseen()
        // TODO: Propagate change to children of comp! (Only if child needs to be informed / uses inputs)
        // The component was updated.
        updated = true
    }

    abstract fun ui()

    inline fun <reified T : Component> Component.include(name: String, setUp: T.() -> Unit): T {
        val id = hashCode(parent?.id or 7, name)

        // If id of component to include is known in child list, take its data!
        val childIndex: Int? = childrenIndex[id]
        val child: T? = childIndex?.let { children[it] as T }
        if (child != null) {
            if (childIndex < currentIndex) {
                throw IllegalArgumentException(
                    """
                        Trying to include a component with name '$name'. 
                        But a component with ID '$id' is already present. 
                        The given name must have already been taken by another child in the current hierarchy level.
                    """.trimIndent()
                )
            }
            children.swap(currentIndex, childIndex)
            child.setUp()
            child.updateIfNecessary()
            currentIndex++
            return child
        }

        // If id of component to include is not already known, create data!
        // TODO: Get instance from an object pool
        val included = T::class.java.getConstructor(/* no args */).newInstance(/* no args */)
        included.id = id
        included.parent = this
        addChild(included, currentIndex)
        setUp(included)
        /* It should not be necessary to update the component.
         * If the setUp did not trigger an update, the last ui from the last update must be sufficient */
        //included.updateIfNecessary()
        currentIndex++
        return included
    }

    fun addChild(component: Component, atIndex: Int) {
        children.add(atIndex, component)
        childrenIndex[component.id] = atIndex
    }

    private fun destroyUnseen() {
        var destroyed = 0
        for (i in currentIndex until children.size) {
            children.removeAt(i)
            destroyed++
        }
        println("destroyed $destroyed unseen children")
    }

}

/**
 * A remembered value leads to updates to this ui whenever the value changes.
 */
fun <T : Any> Component.remember(value: T): Prop<T> {
    return Prop(this, value)
}

fun hashCode(initial: Int, with: String): Int {
    return 31 * initial + with.hashCode()
}

@Suppress("NOTHING_TO_INLINE")
inline infix fun Int?.or(other: Int): Int {
    return this ?: other
}

fun <T> MutableList<T>.swap(indexA: Int, indexB: Int) {
    if (indexA != indexB) {
        val backup = this[indexA]
        this[indexA] = this[indexB]
        this[indexB] = backup
    }
}

