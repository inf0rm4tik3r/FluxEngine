class UI {

    val root: Component = RootComponent()

    inline fun <reified T : Component> buildUi(constr: () -> T): T {
        val comp = constr.invoke()
        root.addChild(comp, root.currentIndex)
        // Info: update() is not called in constructor,
        // as a setUp of a component should be possible before the first call to update() is executed.
        comp.update() // Might include() the first children..
        return comp
    }


    fun render() {

    }

}

class RootComponent : Component() {
    override fun ui() {}
}
