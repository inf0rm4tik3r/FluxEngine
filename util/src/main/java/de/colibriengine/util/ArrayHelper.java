package de.colibriengine.util;

import de.colibriengine.exception.InstantiationNotAllowedException;

import java.lang.reflect.Array;
import java.util.List;

import org.jetbrains.annotations.NotNull;

/**
 * ArrayHelper
 *
 * @version 0.0.0.4
 * @time 01.12.2018 00:05
 * @since ColibriEngine 0.0.6.6
 */
public final class ArrayHelper {
    
    /**
     * Prevent any instantiation of this class as it is a utility class.
     */
    private ArrayHelper() {
        throw new InstantiationNotAllowedException(getClass().getName());
    }
    
    /* GENERIC - implementations */
    
    @SuppressWarnings("unchecked")
    public static <T> @NotNull T[] createTypedArray(final @NotNull Class<T> clazz, final int length) {
        return (T[]) Array.newInstance(clazz, length);
    }
    
    public static <T> void swap(final @NotNull T[] array, final int i, final int j) {
        final @NotNull T temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    
    public static <T> void swap(final @NotNull T[][] array, final int i, final int j) {
        final @NotNull T temp = array[i][j];
        array[i][j] = array[j][i];
        array[j][i] = temp;
    }
    
    public static <T> @NotNull T[] copy(final @NotNull T[] src, final @NotNull Class<T> type) {
        return copy(src, type, 0, src.length);
    }
    
    public static <T> @NotNull T[] copy(final @NotNull T[] src, final @NotNull Class<T> type,
                                        final int srcPos, final int length) {
        final @NotNull T[] copy = createTypedArray(type, length);
        System.arraycopy(src, srcPos, copy, 0, length);
        return copy;
    }
    
    public static <T> int getCombinedLengthGeneric(final @NotNull List<T[]> arrays) {
        int combinedLength = 0;
        for (final T[] array : arrays) {
            combinedLength += array.length;
        }
        return combinedLength;
    }
    
    public static <T> @NotNull T[] combineAllGeneric(final @NotNull List<T[]> arrays, final @NotNull Class<T> type) {
        final T[] finalBuffer = createTypedArray(type, ArrayHelper.getCombinedLengthGeneric(arrays));
        int       bytesIn     = 0;
        for (final T[] data : arrays) {
            System.arraycopy(data, 0, finalBuffer, bytesIn, data.length);
            bytesIn += data.length;
        }
        return finalBuffer;
    }
    
    /* BYTE - implementations */
    
    public static void swap(final byte[] array, final int i, final int j) {
        final byte temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    
    public static void swap(final byte[][] array, final int i, final int j) {
        final byte temp = array[i][j];
        array[i][j] = array[j][i];
        array[j][i] = temp;
    }
    
    public static @NotNull Byte[] box(final byte[] primitiveArray) {
        final @NotNull Byte[] boxed = new Byte[primitiveArray.length];
        for (int i = 0; i < boxed.length; i++) {
            boxed[i] = primitiveArray[i];
        }
        return boxed;
    }
    
    public static byte[] unbox(final @NotNull Byte[] boxedArray) {
        final byte[] unboxed = new byte[boxedArray.length];
        for (int i = 0; i < unboxed.length; i++) {
            unboxed[i] = boxedArray[i];
        }
        return unboxed;
    }
    
    public static byte[] concat(final byte[] a, final byte[] b) {
        final byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    
    public static byte[] concat(final byte[] a, final int aStart, final int aLength,
                                         final byte[] b, final int bStart, final int bLength) {
        final byte[] c = new byte[aLength + bLength];
        System.arraycopy(a, aStart, c, 0, aLength);
        System.arraycopy(b, bStart, c, aLength, bLength);
        return c;
    }
    
    public static byte[] copy(final byte[] src) {
        return copy(src, 0, src.length);
    }
    
    public static byte[] copy(final byte[] src, final int srcPos, final int length) {
        final byte[] copy = new byte[length];
        System.arraycopy(src, srcPos, copy, 0, length);
        return copy;
    }
    
    public static int getCombinedLengthByte(final @NotNull List<byte[]> arrays) {
        int combinedLength = 0;
        for (final byte[] array : arrays) {
            combinedLength += array.length;
        }
        return combinedLength;
    }
    
    public static byte[] combineAllByte(final @NotNull List<byte[]> arrays) {
        final byte[] finalBuffer = new byte[ArrayHelper.getCombinedLengthByte(arrays)];
        int          bytesIn     = 0;
        for (final byte[] data : arrays) {
            System.arraycopy(data, 0, finalBuffer, bytesIn, data.length);
            bytesIn += data.length;
        }
        return finalBuffer;
    }
    
    /* BOOLEAN - implementations */
    
    public static void swap(final boolean[] array, final int i, final int j) {
        final boolean temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    
    public static void swap(final boolean[][] array, final int i, final int j) {
        final boolean temp = array[i][j];
        array[i][j] = array[j][i];
        array[j][i] = temp;
    }
    
    public static @NotNull Boolean[] box(final boolean[] primitiveArray) {
        final @NotNull Boolean[] boxed = new Boolean[primitiveArray.length];
        for (int i = 0; i < boxed.length; i++) {
            boxed[i] = primitiveArray[i];
        }
        return boxed;
    }
    
    public static boolean[] unbox(final @NotNull Boolean[] boxedArray) {
        final boolean[] unboxed = new boolean[boxedArray.length];
        for (int i = 0; i < unboxed.length; i++) {
            unboxed[i] = boxedArray[i];
        }
        return unboxed;
    }
    
    public static boolean[] concat(final boolean[] a, final boolean[] b) {
        final boolean[] c = new boolean[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    
    public static boolean[] copy(final boolean[] src) {
        return copy(src, 0, src.length);
    }
    
    public static boolean[] copy(final boolean[] src, final int srcPos, final int length) {
        final boolean[] copy = new boolean[src.length];
        System.arraycopy(src, srcPos, copy, 0, length);
        return copy;
    }
    
    public static int getCombinedLengthBoolean(final @NotNull List<boolean[]> arrays) {
        int combinedLength = 0;
        for (final boolean[] array : arrays) {
            combinedLength += array.length;
        }
        return combinedLength;
    }
    
    public static boolean[] combineAllBoolean(final @NotNull List<boolean[]> arrays) {
        final boolean[] finalBuffer = new boolean[ArrayHelper.getCombinedLengthBoolean(arrays)];
        int             bytesIn     = 0;
        for (final boolean[] data : arrays) {
            System.arraycopy(data, 0, finalBuffer, bytesIn, data.length);
            bytesIn += data.length;
        }
        return finalBuffer;
    }
    
    /* INT - implementations */
    
    public static void swap(final int[] array, final int i, final int j) {
        final int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    
    public static void swap(final int[][] array, final int i, final int j) {
        final int temp = array[i][j];
        array[i][j] = array[j][i];
        array[j][i] = temp;
    }
    
    public static @NotNull Integer[] box(final int[] primitiveArray) {
        final @NotNull Integer[] boxed = new Integer[primitiveArray.length];
        for (int i = 0; i < boxed.length; i++) {
            boxed[i] = primitiveArray[i];
        }
        return boxed;
    }
    
    public static int[] unbox(final @NotNull Integer[] boxedArray) {
        final int[] unboxed = new int[boxedArray.length];
        for (int i = 0; i < unboxed.length; i++) {
            unboxed[i] = boxedArray[i];
        }
        return unboxed;
    }
    
    public static int[] concat(final int[] a, final int[] b) {
        final int[] c = new int[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    
    public static int[] copy(final int[] src) {
        return copy(src, 0, src.length);
    }
    
    public static int[] copy(final int[] src, final int srcPos, final int length) {
        final int[] copy = new int[src.length];
        System.arraycopy(src, srcPos, copy, 0, length);
        return copy;
    }
    
    public static int getCombinedLengthInt(final @NotNull List<int[]> arrays) {
        int combinedLength = 0;
        for (final int[] array : arrays) {
            combinedLength += array.length;
        }
        return combinedLength;
    }
    
    public static int[] combineAllInt(final @NotNull List<int[]> arrays) {
        final int[] finalBuffer = new int[ArrayHelper.getCombinedLengthInt(arrays)];
        int         bytesIn     = 0;
        for (final int[] data : arrays) {
            System.arraycopy(data, 0, finalBuffer, bytesIn, data.length);
            bytesIn += data.length;
        }
        return finalBuffer;
    }
    
    /* LONG - implementations */
    
    public static void swap(final long[] array, final int i, final int j) {
        final long temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    
    public static void swap(final long[][] array, final int i, final int j) {
        final long temp = array[i][j];
        array[i][j] = array[j][i];
        array[j][i] = temp;
    }
    
    public static @NotNull Long[] box(final long[] primitiveArray) {
        final @NotNull Long[] boxed = new Long[primitiveArray.length];
        for (int i = 0; i < boxed.length; i++) {
            boxed[i] = primitiveArray[i];
        }
        return boxed;
    }
    
    public static long[] unbox(final @NotNull Long[] boxedArray) {
        final long[] unboxed = new long[boxedArray.length];
        for (int i = 0; i < unboxed.length; i++) {
            unboxed[i] = boxedArray[i];
        }
        return unboxed;
    }
    
    public static long[] concat(final long[] a, final long[] b) {
        final long[] c = new long[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    
    public static long[] copy(final long[] src) {
        return copy(src, 0, src.length);
    }
    
    public static long[] copy(final long[] src, final int srcPos, final int length) {
        final long[] copy = new long[src.length];
        System.arraycopy(src, srcPos, copy, 0, length);
        return copy;
    }
    
    public static int getCombinedLengthLong(final @NotNull List<long[]> arrays) {
        int combinedLength = 0;
        for (final long[] array : arrays) {
            combinedLength += array.length;
        }
        return combinedLength;
    }
    
    public static long[] combineAllLong(final @NotNull List<long[]> arrays) {
        final long[] finalBuffer = new long[ArrayHelper.getCombinedLengthLong(arrays)];
        int          bytesIn     = 0;
        for (final long[] data : arrays) {
            System.arraycopy(data, 0, finalBuffer, bytesIn, data.length);
            bytesIn += data.length;
        }
        return finalBuffer;
    }
    
    /* FLOAT - implementations */
    
    public static void swap(final float[] array, final int i, final int j) {
        final float temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    
    public static void swap(final float[][] array, final int i, final int j) {
        final float temp = array[i][j];
        array[i][j] = array[j][i];
        array[j][i] = temp;
    }
    
    public static @NotNull Float[] box(final float[] primitiveArray) {
        final @NotNull Float[] boxed = new Float[primitiveArray.length];
        for (int i = 0; i < boxed.length; i++) {
            boxed[i] = primitiveArray[i];
        }
        return boxed;
    }
    
    public static float[] unbox(final @NotNull Float[] boxedArray) {
        final float[] unboxed = new float[boxedArray.length];
        for (int i = 0; i < unboxed.length; i++) {
            unboxed[i] = boxedArray[i];
        }
        return unboxed;
    }
    
    public static float[] concat(final float[] a, final float[] b) {
        final float[] c = new float[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    
    public static float[] copy(final float[] src) {
        return copy(src, 0, src.length);
    }
    
    public static float[] copy(final float[] src, final int srcPos, final int length) {
        final float[] copy = new float[src.length];
        System.arraycopy(src, srcPos, copy, 0, length);
        return copy;
    }
    
    public static int getCombinedLengthFloat(final @NotNull List<float[]> arrays) {
        int combinedLength = 0;
        for (final float[] array : arrays) {
            combinedLength += array.length;
        }
        return combinedLength;
    }
    
    public static float[] combineAllFloat(final @NotNull List<float[]> arrays) {
        final float[] finalBuffer = new float[ArrayHelper.getCombinedLengthFloat(arrays)];
        int           bytesIn     = 0;
        for (final float[] data : arrays) {
            System.arraycopy(data, 0, finalBuffer, bytesIn, data.length);
            bytesIn += data.length;
        }
        return finalBuffer;
    }
    
    /* DOUBLE - implementations */
    
    public static void swap(final double[] array, final int i, final int j) {
        final double temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    
    public static void swap(final double[][] array, final int i, final int j) {
        final double temp = array[i][j];
        array[i][j] = array[j][i];
        array[j][i] = temp;
    }
    
    public static @NotNull Double[] box(final double[] primitiveArray) {
        final @NotNull Double[] boxed = new Double[primitiveArray.length];
        for (int i = 0; i < boxed.length; i++) {
            boxed[i] = primitiveArray[i];
        }
        return boxed;
    }
    
    public static double[] unbox(final @NotNull Double[] boxedArray) {
        final double[] unboxed = new double[boxedArray.length];
        for (int i = 0; i < unboxed.length; i++) {
            unboxed[i] = boxedArray[i];
        }
        return unboxed;
    }
    
    public static double[] concat(final double[] a, final double[] b) {
        final double[] c = new double[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    
    public static double[] copy(final double[] src) {
        return copy(src, 0, src.length);
    }
    
    public static double[] copy(final double[] src, final int srcPos, final int length) {
        final double[] copy = new double[src.length];
        System.arraycopy(src, srcPos, copy, 0, length);
        return copy;
    }
    
    public static int getCombinedLengthDouble(final @NotNull List<double[]> arrays) {
        int combinedLength = 0;
        for (final double[] array : arrays) {
            combinedLength += array.length;
        }
        return combinedLength;
    }
    
    public static double[] combineAllDouble(final @NotNull List<double[]> arrays) {
        final double[] finalBuffer = new double[ArrayHelper.getCombinedLengthDouble(arrays)];
        int            bytesIn     = 0;
        for (final double[] data : arrays) {
            System.arraycopy(data, 0, finalBuffer, bytesIn, data.length);
            bytesIn += data.length;
        }
        return finalBuffer;
    }
    
}
