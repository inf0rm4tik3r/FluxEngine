package de.colibriengine.util

@JvmInline
value class Bit(val value: Int) {

    init {
        require(value == 0 || value == 1) { "Bit's 'value' parameter must be either 0 or 1, but was $value!"}
    }

    /** True, if this bit is a ZERO bit. */
    val zero: Boolean
        get() = value == 0

    /** True, if this bit is a ONE bit. */
    val one: Boolean
        get() = value == 1

    companion object {
        val ZERO: Bit = Bit(0)
        val ONE: Bit = Bit(1)
    }

}
