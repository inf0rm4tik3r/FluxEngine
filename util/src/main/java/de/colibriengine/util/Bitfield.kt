package de.colibriengine.util

import de.colibriengine.util.Bitfield.AccessDirection.LEFT
import de.colibriengine.util.Bitfield.AccessDirection.RIGHT

@JvmInline
value class Bitfield(val byte: UByte) {

    enum class AccessDirection {
        LEFT, RIGHT
    }

    constructor(bit0: Int, bit1: Int, bit2: Int, bit3: Int, bit4: Int, bit5: Int, bit6: Int, bit7: Int)
        : this(bitsToUByte(bit0, bit1, bit2, bit3, bit4, bit5, bit6, bit7))

    fun get(index: Int, accessDirection: AccessDirection = DEFAULT_ACCESS_DIRECTION): Bit {
        require(index in 0..7) { "Index must lie in range ${0..7}!" }
        return when (accessDirection) {
            LEFT -> byte.bitFromLeft(index)
            RIGHT -> byte.bitFromRight(index)
        }
    }

    fun getAsInt(index: Int, accessDirection: AccessDirection = DEFAULT_ACCESS_DIRECTION): Int =
        get(index, accessDirection).value

    /** Returns the bit at [index] from the left. */
    fun getAsBoolean(index: Int, accessDirection: AccessDirection = DEFAULT_ACCESS_DIRECTION): Boolean =
        get(index, accessDirection).one

    fun toIntArray(): IntArray {
        return intArrayOf(
            getAsInt(0, LEFT),
            getAsInt(1, LEFT),
            getAsInt(2, LEFT),
            getAsInt(3, LEFT),
            getAsInt(4, LEFT),
            getAsInt(5, LEFT),
            getAsInt(6, LEFT),
            getAsInt(7, LEFT)
        )
    }

    companion object {
        val DEFAULT_ACCESS_DIRECTION = LEFT
    }

}
