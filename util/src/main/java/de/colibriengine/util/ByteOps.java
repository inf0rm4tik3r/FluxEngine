package de.colibriengine.util;

import de.colibriengine.exception.InstantiationNotAllowedException;

import java.nio.*;

/**
 * ByteOps - Provides utility function for byte-level operations.
 *
 * @since ColibriEngine 0.0.6.8
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class ByteOps {

    public static final int DEFAULT_OFFSET = 0;
    public static final ByteOrder DEFAULT_BYTE_ORDER = ByteOrder.nativeOrder();

    private static final ByteBuffer SINGLE_INT_BB = ByteBuffer.allocate(Integer.BYTES);

    private static final ByteBuffer SHORT_CONVERSION_BUFFER = ByteBuffer.allocate(Short.BYTES);
    private static final ByteBuffer INT_CONVERSION_BUFFER = ByteBuffer.allocate(Integer.BYTES);
    private static final ByteBuffer LONG_CONVERSION_BUFFER = ByteBuffer.allocate(Long.BYTES);
    private static final ByteBuffer FLOAT_CONVERSION_BUFFER = ByteBuffer.allocate(Float.BYTES);
    private static final ByteBuffer DOUBLE_CONVERSION_BUFFER = ByteBuffer.allocate(Double.BYTES);
    private static final ByteBuffer CHAR_CONVERSION_BUFFER = ByteBuffer.allocate(Character.BYTES);

    private static final String AT_PARAMETER_MUST_BE_IN_RANGE_0_7 = "The \"at\" parameter must be in range [0..7]!";

    /**
     * Prevent any instantiation of this class as it is a utility class.
     */
    private ByteOps() {
        throw new InstantiationNotAllowedException(getClass().getName());
    }

    /**
     * Converts the specified signed byte into an unsigned byte which is stored in an int.
     *
     * @param signedByte The signed byte to convert.
     * @return An unsigned byte.
     */
    public static int toUnsignedByte(final byte signedByte) {
        return signedByte & 0xFF;
    }

    /**
     * Swaps the byte order of a 32bit integer value.
     * "Little endian - used mostly in Intel machines"
     *
     * @param bigEndianValue The input value in big endianness.
     * @return the output value in little endianness.
     */
    public static int bigToLittleEndian(final int bigEndianValue) {
        SINGLE_INT_BB.clear();
        SINGLE_INT_BB.order(ByteOrder.BIG_ENDIAN);
        SINGLE_INT_BB.putInt(bigEndianValue);
        SINGLE_INT_BB.order(ByteOrder.LITTLE_ENDIAN);
        return SINGLE_INT_BB.getInt(0);
    }

    /**
     * Swaps the byte order of a 32bit integer value.
     * "Big endian - used mostly in Motorola machines"
     *
     * @param littleEndianValue The input value in little endianness.
     * @return the output value in big endianness.
     */
    public static int littleToBigEndian(final int littleEndianValue) {
        SINGLE_INT_BB.clear();
        SINGLE_INT_BB.order(ByteOrder.LITTLE_ENDIAN);
        SINGLE_INT_BB.putInt(littleEndianValue);
        SINGLE_INT_BB.order(ByteOrder.BIG_ENDIAN);
        return SINGLE_INT_BB.getInt(0);
    }

    /**
     * Retrieves the bit at position {@code at} in the byte {@code in}.
     * {@code at} selects the n'tn bit from the right.
     * {@code at = 7} will return the highest bit, {@code at = 0} the lowest bit.
     *
     * @param in The byte to access.
     * @param at Position of the bit to retrieve in the range [0..7].
     * @return the value of the bit. 0 or 1.
     * @throws IllegalArgumentException If {@code at} was not in the range [0..7].
     */
    public static int getBitFromRight(final byte in, final int at) {
        if (at < 0 || at > 7) {
            throw new IllegalArgumentException(AT_PARAMETER_MUST_BE_IN_RANGE_0_7);
        }
        return (byte) ((in >> at) & 1);
    }

    /**
     * Retrieves the bit at position {@code at} in the byte {@code in}.
     * {@code at} selects the n'tn bit from the left!
     * {@code at = 0} will return the highest bit, {@code at = 7} the lowest bit.
     *
     * @param in The byte to access.
     * @param at Position of the bit to retrieve.
     * @return the value of the bit. 0 or 1.
     * @throws IllegalArgumentException If {@code at} was not in the range [0..7].
     */
    public static int getBitFromLeft(final byte in, final int at) {
        if (at < 0 || at > 7) {
            throw new IllegalArgumentException(AT_PARAMETER_MUST_BE_IN_RANGE_0_7);
        }
        return (byte) ((in >> (7 - at)) & 1);
    }

    public static byte[] short2ByteArray(final short value) {
        SHORT_CONVERSION_BUFFER.clear();
        return SHORT_CONVERSION_BUFFER.putShort(value).array();
    }

    public static byte[] int2ByteArray(final int value) {
        INT_CONVERSION_BUFFER.clear();
        return INT_CONVERSION_BUFFER.putInt(value).array();
    }

    public static byte[] long2ByteArray(final long value) {
        LONG_CONVERSION_BUFFER.clear();
        return LONG_CONVERSION_BUFFER.putLong(value).array();
    }

    public static byte[] float2ByteArray(final float value) {
        FLOAT_CONVERSION_BUFFER.clear();
        return FLOAT_CONVERSION_BUFFER.putFloat(value).array();
    }

    public static byte[] double2ByteArray(final double value) {
        DOUBLE_CONVERSION_BUFFER.clear();
        return DOUBLE_CONVERSION_BUFFER.putDouble(value).array();
    }

    public static byte[] char2ByteArray(final char value) {
        CHAR_CONVERSION_BUFFER.clear();
        return CHAR_CONVERSION_BUFFER.putChar(value).array();
    }

    /*
     * byte[] => short
     */

    public static short byteArrayToShort(final byte[] data) {
        return byteArrayToShort(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static short byteArrayToShort(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToShort(data, DEFAULT_OFFSET, byteOrder);
    }

    public static short byteArrayToShort(final byte[] data, final int offset) {
        return byteArrayToShort(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static short byteArrayToShort(final byte[] data, final int offset, final ByteOrder byteOrder) {
        SHORT_CONVERSION_BUFFER.clear();
        SHORT_CONVERSION_BUFFER.order(byteOrder);
        SHORT_CONVERSION_BUFFER.put(data, offset, Short.BYTES);
        SHORT_CONVERSION_BUFFER.rewind();
        return SHORT_CONVERSION_BUFFER.getShort();
    }

    /*
     * byte[] => int
     */

    public static int byteArrayToInt(final byte[] data) {
        return byteArrayToInt(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static int byteArrayToInt(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToInt(data, DEFAULT_OFFSET, byteOrder);
    }

    public static int byteArrayToInt(final byte[] data, final int offset) {
        return byteArrayToInt(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static int byteArrayToInt(final byte[] data, final int offset, final ByteOrder byteOrder) {
        INT_CONVERSION_BUFFER.clear();
        INT_CONVERSION_BUFFER.order(byteOrder);
        INT_CONVERSION_BUFFER.put(data, offset, Integer.BYTES);
        INT_CONVERSION_BUFFER.rewind();
        return INT_CONVERSION_BUFFER.getInt();
    }

    /*
     * byte[] => long
     */

    public static long byteArrayToLong(final byte[] data) {
        return byteArrayToLong(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static long byteArrayToLong(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToLong(data, DEFAULT_OFFSET, byteOrder);
    }

    public static long byteArrayToLong(final byte[] data, final int offset) {
        return byteArrayToLong(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static long byteArrayToLong(final byte[] data, final int offset, final ByteOrder byteOrder) {
        LONG_CONVERSION_BUFFER.clear();
        LONG_CONVERSION_BUFFER.order(byteOrder);
        LONG_CONVERSION_BUFFER.put(data, offset, Long.BYTES);
        LONG_CONVERSION_BUFFER.rewind();
        return LONG_CONVERSION_BUFFER.getLong();
    }

    /*
     * byte[] => float
     */

    public static float byteArrayToFloat(final byte[] data) {
        return byteArrayToFloat(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static float byteArrayToFloat(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToFloat(data, DEFAULT_OFFSET, byteOrder);
    }

    public static float byteArrayToFloat(final byte[] data, final int offset) {
        return byteArrayToFloat(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static float byteArrayToFloat(final byte[] data, final int offset, final ByteOrder byteOrder) {
        FLOAT_CONVERSION_BUFFER.clear();
        FLOAT_CONVERSION_BUFFER.order(byteOrder);
        FLOAT_CONVERSION_BUFFER.put(data, offset, Float.BYTES);
        FLOAT_CONVERSION_BUFFER.rewind();
        return FLOAT_CONVERSION_BUFFER.getFloat();
    }

    /*
     * byte[] => double
     */

    public static double byteArrayToDouble(final byte[] data) {
        return byteArrayToDouble(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static double byteArrayToDouble(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToDouble(data, DEFAULT_OFFSET, byteOrder);
    }

    public static double byteArrayToDouble(final byte[] data, final int offset) {
        return byteArrayToDouble(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static double byteArrayToDouble(final byte[] data, final int offset, final ByteOrder byteOrder) {
        DOUBLE_CONVERSION_BUFFER.clear();
        DOUBLE_CONVERSION_BUFFER.order(byteOrder);
        DOUBLE_CONVERSION_BUFFER.put(data, offset, Double.BYTES);
        DOUBLE_CONVERSION_BUFFER.rewind();
        return DOUBLE_CONVERSION_BUFFER.getDouble();
    }

    /*
     * byte[] => char
     */

    public static char byteArrayToChar(final byte[] data) {
        return byteArrayToChar(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static char byteArrayToChar(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToChar(data, DEFAULT_OFFSET, byteOrder);
    }

    public static char byteArrayToChar(final byte[] data, final int offset) {
        return byteArrayToChar(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static char byteArrayToChar(final byte[] data, final int offset, final ByteOrder byteOrder) {
        CHAR_CONVERSION_BUFFER.clear();
        CHAR_CONVERSION_BUFFER.order(byteOrder);
        CHAR_CONVERSION_BUFFER.put(data, offset, Character.BYTES);
        CHAR_CONVERSION_BUFFER.rewind();
        return CHAR_CONVERSION_BUFFER.getChar();
    }

    /*
     * byte[] => boolean[]
     */

    public static boolean[] byteArrayToBooleanArray(final byte[] data) {
        return byteArrayToBooleanArray(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static boolean[] byteArrayToBooleanArray(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToBooleanArray(data, DEFAULT_OFFSET, byteOrder);
    }

    public static boolean[] byteArrayToBooleanArray(final byte[] data, final int offset) {
        return byteArrayToBooleanArray(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static boolean[] byteArrayToBooleanArray(final byte[] data, final int offset,
                                                    final ByteOrder byteOrder) {
        final boolean[] array = new boolean[data.length];
        for (int i = 0; i < data.length; i++) {
            // Note: data[i] == 0 => false
            //       data[i] != 0 => true
            array[i] = data[i] != 0;
        }
        return array;
    }

    /*
     * byte[] => int[]
     */

    public static int[] byteArrayToIntArray(final byte[] data) {
        return byteArrayToIntArray(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static int[] byteArrayToIntArray(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToIntArray(data, DEFAULT_OFFSET, byteOrder);
    }

    public static int[] byteArrayToIntArray(final byte[] data, final int offset) {
        return byteArrayToIntArray(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static int[] byteArrayToIntArray(final byte[] data, final int offset, final ByteOrder byteOrder) {
        final IntBuffer intBuffer = ByteBuffer.wrap(data).order(byteOrder).asIntBuffer();
        final int[] array = new int[intBuffer.remaining()];
        intBuffer.get(array);
        return array;
    }

    /*
     * byte[] => long[]
     */

    public static long[] byteArrayToLongArray(final byte[] data) {
        return byteArrayToLongArray(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static long[] byteArrayToLongArray(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToLongArray(data, DEFAULT_OFFSET, byteOrder);
    }

    public static long[] byteArrayToLongArray(final byte[] data, final int offset) {
        return byteArrayToLongArray(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static long[] byteArrayToLongArray(final byte[] data, final int offset, final ByteOrder byteOrder) {
        final LongBuffer longBuffer = ByteBuffer.wrap(data).order(byteOrder).asLongBuffer();
        final long[] array = new long[longBuffer.remaining()];
        longBuffer.get(array);
        return array;
    }

    /*
     * byte[] => float[]
     */

    public static float[] byteArrayToFloatArray(final byte[] data) {
        return byteArrayToFloatArray(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static float[] byteArrayToFloatArray(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToFloatArray(data, DEFAULT_OFFSET, byteOrder);
    }

    public static float[] byteArrayToFloatArray(final byte[] data, final int offset) {
        return byteArrayToFloatArray(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static float[] byteArrayToFloatArray(final byte[] data, final int offset,
                                                final ByteOrder byteOrder) {
        final FloatBuffer floatBuffer = ByteBuffer.wrap(data).order(byteOrder).asFloatBuffer();
        final float[] array = new float[floatBuffer.remaining()];
        floatBuffer.get(array);
        return array;
    }

    /*
     * byte[] => double[]
     */

    public static double[] byteArrayToDoubleArray(final byte[] data) {
        return byteArrayToDoubleArray(data, DEFAULT_OFFSET, DEFAULT_BYTE_ORDER);
    }

    public static double[] byteArrayToDoubleArray(final byte[] data, final ByteOrder byteOrder) {
        return byteArrayToDoubleArray(data, DEFAULT_OFFSET, byteOrder);
    }

    public static double[] byteArrayToDoubleArray(final byte[] data, final int offset) {
        return byteArrayToDoubleArray(data, offset, DEFAULT_BYTE_ORDER);
    }

    public static double[] byteArrayToDoubleArray(final byte[] data, final int offset,
                                                  final ByteOrder byteOrder) {
        final DoubleBuffer doubleBuffer = ByteBuffer.wrap(data).order(byteOrder).asDoubleBuffer();
        final double[] array = new double[doubleBuffer.remaining()];
        doubleBuffer.get(array);
        return array;
    }

}
