package de.colibriengine.util

import kotlin.math.abs

/** Calls [action] if [expression] is `true`. Does nothing if [expression] is `false`. */
inline fun ifTrue(expression: Boolean, action: () -> Unit) {
    if (expression) action.invoke()
}

/** Calls [action] if [expression] is `true`. Calls [elseAction] if [expression] is `false`. */
inline fun ifTrue(expression: Boolean, action: () -> Unit, elseAction: () -> Unit) {
    if (expression) action.invoke()
    else elseAction.invoke()
}

/** Calls [action] if [expression] is `false`. Does nothing if [expression] is `true`. */
inline fun ifFalse(expression: Boolean, action: () -> Unit) {
    if (!expression) action.invoke()
}

/** Calls [action] if [expression] is `false`. Calls [elseAction] if [expression] is `true`. */
inline fun ifFalse(expression: Boolean, action: () -> Unit, elseAction: () -> Unit) {
    if (!expression) action.invoke()
    else elseAction.invoke()
}

/** Calls [notPresentAction] if [argument] is `null`. Does nothing if [argument] is not `null`. */
inline fun <T : Any> ifNull(argument: T?, notPresentAction: () -> Unit) {
    if (argument == null) notPresentAction.invoke()
}

/** Calls [notPresentAction] if [argument] is `null`. Calls [presentAction] if [argument] is not `null` */
inline fun <T : Any> ifNull(argument: T?, notPresentAction: () -> Unit, presentAction: (T) -> Unit) {
    if (argument == null) notPresentAction.invoke()
    else presentAction.invoke(argument)
}

/** Calls [presentAction] with [argument] if [argument] is not `null`. Does nothing if [argument] is `null`. */
inline fun <T : Any> ifNotNull(argument: T?, presentAction: (T) -> Unit) {
    if (argument != null) presentAction.invoke(argument)
}

/**
 * Calls [presentAction] with [argument] if [argument] is not `null`. Calls [notPresentAction] if [argument] is `null`.
 */
inline fun <T : Any> ifNotNull(argument: T?, presentAction: (T) -> Unit, notPresentAction: () -> Unit) {
    if (argument != null) presentAction.invoke(argument)
    else notPresentAction.invoke()
}

/** Return true if the difference between [value] and [target] is below [threshold] */
fun isApproximately(value: Double, target: Double, threshold: Double): Boolean {
    return abs(target - value) <= threshold
}

/** Return true if the difference between [value] and [target] is below [threshold] */
fun isApproximately(value: Float, target: Float, threshold: Float): Boolean {
    return abs(target - value) <= threshold
}
