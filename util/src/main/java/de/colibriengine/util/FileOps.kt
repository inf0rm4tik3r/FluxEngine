package de.colibriengine.util

import de.colibriengine.logging.LogUtil
import java.io.File
import java.io.IOException
import java.lang.IllegalArgumentException
import java.net.URI
import java.net.URL

/**
 * FileOps
 *
 * @version 0.0.0.6
 * @since ColibriEngine 0.0.6.6
 */
object FileOps {

    private val LOG = LogUtil.getLogger(FileOps::class.java)

    /**
     * Returns an [URL] object representing the file specified in the [resourceName] argument.
     * Returns null if [resourceName] did not point to a valid file.
     * This method is able to access files inside a .jar file! Just specify a relative path for that to work.
     *
     * @param resourceName
     * Path to a file. Example: "models/dragon.obj"
     *
     * @return the [URL] to the specified file or null if the file could not be found or not be accessed.
     */
    fun getAsURL(path: String): URL? =
        FileOps::class.java.classLoader.getResource(path)

    fun getAsURLOrFail(path: String): URL =
        FileOps::class.java.classLoader.getResource(path) ?: throw IllegalArgumentException("'$path' not found!")

    fun getAsURI(path: String): URI? =
        Thread.currentThread().contextClassLoader.getResource(path)?.toURI()

    fun getAsURIOrFail(path: String): URI = getAsURLOrFail(path).toURI()

    fun getResourceAsFile(resourceName: String): File? = when (val uri = getAsURI(resourceName)) {
        null -> null
        else -> File(uri)
    }

    fun getResourceAsFileOrFail(resourceName: String, orFail: () -> Nothing): File = when (val uri = getAsURI(resourceName)) {
        null -> orFail()
        else -> File(uri)
    }

    /**
     * Returns a [File] object that points to the file specified in the [path] parameter. This method
     * guarantees that the returned [File] represents an existing file in the underlying file system.
     *
     * If it exists in the first place, this method ignores its second parameter and does not modify the system.
     * If it does not exist and [createIfNecessary] is [false] a [RuntimeException] is thrown.
     * If it does not exist and [createIfNecessary] is [true] the file will be created.
     * If it exists but is not a file after all, a [RuntimeException] is thrown.
     *
     * @param path
     * String, specifying a file.
     * @param createIfNecessary
     * Use [false] to prevent file creation if [path] does not exist.
     * The creation process will not create any necessary folders to reach the specified file!
     *
     * @return A [File] representing a file in the filesystem. Cannot return null.
     *
     * @throws RuntimeException
     * If file is not present and [createIfNecessary] is set to false or if the [path] argument
     * specified a directory instead of a file.
     */
    fun getFile(path: String, createIfNecessary: Boolean): File {
        val file = File(path)

        if (!file.exists()) {
            if (createIfNecessary) {
                try {
                    val createSuccessful = file.createNewFile()
                    if (createSuccessful) {
                        LOG.info("Created File: {}", file.absolutePath)
                    } // else case not possible: guaranteed that file does not exist.
                } catch (ioe: IOException) {
                    ioe.printStackTrace()
                }

            } else {
                throw RuntimeException("Specified file does not exist but we are not allowed to create it.")
            }
        }

        if (!file.isFile) {
            throw RuntimeException("Path argument did not specify a file.")
        }

        return file
    }

    /**
     * Returns a [File] object that points to the directory specified in the [directoryPath] parameter.
     * This method guarantees that the returned [File] represents an existing directory in the underlying file
     * system.
     *
     * - If it exists in the first place, this method ignores its second parameter and does not modify the system.
     * - If it does not exist and [createIfNecessary] is [false] a [RuntimeException] is thrown.
     * - If it does not exist and [createIfNecessary] is [true] the directories(!) will be created.
     * - If it exists but is not a directory after all, a [RuntimeException] is thrown.
     *
     * @param directoryPath
     * String, specifying a directory.
     * @param createIfNecessary
     * Use [false] to prevent directory creation if [directoryPath] does not exist.
     *
     * @return A [File] representing an existing directory. Cannot return null.
     *
     * @throws RuntimeException
     * If the file is not present and [createIfNecessary] was set to false or if the [directoryPath] argument specified
     * a directory instead of a file.
     */
    @Throws(RuntimeException::class)
    fun getAsDirectory(directoryPath: String, createIfNecessary: Boolean): File {
        val file = File(directoryPath)

        if (!file.exists()) {
            if (createIfNecessary) {
                if (file.mkdirs()) {
                    LOG.info("created Directories: " + file.absolutePath)
                } else {
                    throw RuntimeException("Unable to create the directories.")
                }
            } else {
                throw RuntimeException("Specified directory does not exist but we are not allowed to create it.")
            }
        }

        if (!file.isDirectory) {
            throw RuntimeException("Path argument did not specify a directory.")
        }

        return file
    }

}
