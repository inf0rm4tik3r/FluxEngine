package de.colibriengine.util

/**
 * Provides the ability to generate and release 32bit IDs. Multiple [generate] calls will never return a specific ID
 * twice, unless an ID was released in between calls to [generate] with a call to [release].
 *
 * All methods in this class are thread safe.
 */
class IdGenerator(
    /** States the next ID which is to be returned. Gets incremented on calls to [generate]. */
    private var nextID: UInt = 1u,
    /** Stores all ID's which got released through calls to [release]. */
    // TODO: Exchange with UIntArray when stable..
    private val releasedIDs: ArrayList<UInt> = ArrayList(RELEASED_IDS_INITIAL_CAPACITY)
) {

    /**
     * Generates a 32bit ID between [FIRST_ID] (1) and [UInt.MAX_VALUE] - 1. The returned value is guaranteed to be
     * unique. If an object is no longer needed, release its ID through a call to [release]. Released IDs will be
     * returned by this method first, instead of generating new IDs. As soon as [UInt.MAX_VALUE] is the next number to
     * be returned, an [UnsupportedOperationException] is thrown to prevent overflows and re-generation of previously
     * generated ID's.
     *
     * @return a new (or a previously generated and then released) object ID.
     * @throws UnsupportedOperationException If the amount of generated IDs would exceed [UInt.MAX_VALUE].
     */
    @Synchronized
    fun generate(): UInt {
        if (releasedIDs.isNotEmpty()) {
            return releasedIDs.removeLast()
        }
        if (nextID == UInt.MAX_VALUE) {
            throw ArithmeticException("IDGenerator: Already generated 2^64-2 ID's. Limit reached!")
        }
        return nextID++
    }

    @Synchronized
    @Deprecated("only for java interop..")
    fun generateJava(): Int {
        return generate().toInt()
    }

    /**
     * Releases a previously generated [id]. Further calls to [generate] will return released IDs before generating new
     * ones.
     *
     * @throws UnsupportedOperationException If the specified id was never generated or if it was already released!
     */
    @Synchronized
    fun release(id: UInt) {
        require(id <= nextID) { "ID ($id) was never created before using this generator." }
        require(id != UInt.MAX_VALUE) { "UInt.MAX_VALUE is not supported." }
        // TODO: Only perform this check in debugging. It is not efficient with our data structure.
        if (releasedIDs.contains(id)) {
            throw UnsupportedOperationException("ID ($id) was already released.")
        }
        releasedIDs.add(id)
    }

    /** Reports how many IDs are left to be [generate]d before [generate] throws an [UnsupportedOperationException]. */
    @Synchronized
    fun remaining(): UInt {
        return UInt.MAX_VALUE - 1u - nextID + releasedIDs.size.toUInt()
    }

    /** Releases all previously generated ID's and sets this object back to its initial state. */
    @Synchronized
    fun reset() {
        nextID = FIRST_ID
        releasedIDs.clear()
    }

    companion object {
        private const val FIRST_ID = 1u
        private const val RELEASED_IDS_INITIAL_CAPACITY = 32
    }

}
