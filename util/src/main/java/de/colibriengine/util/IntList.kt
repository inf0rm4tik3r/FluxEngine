package de.colibriengine.util

/** TODO: Finalize and implement tests */
class IntList(initialSize: Int = 8) {

    constructor(initialSize: Int, factory: (Int) -> Int) : this(initialSize) {
        for (index in 0 until initialSize) {
            add(factory.invoke(index))
        }
    }

    private val data: IntArray = IntArray(initialSize)
    private var next: Int = 0

    val size: Int
        get() = next

    fun clear() {
        next = 0
    }

    fun add(value: Int) {
        data[next] = value
        next++
    }

    operator fun get(index: Int): Int {
        // Custom bounds check to forbid access to non-visible storage!
        if (index !in 0 until next) {
            throw IndexOutOfBoundsException("Index $index is not in range [0, ${next - 1}]")
        }
        return data[index]
    }

    operator fun set(index: Int, value: Int) {
        // Custom bounds check to forbid access to non-visible storage!
        if (index !in 0 until next) {
            throw IndexOutOfBoundsException("Index $index is not in range [0, ${next - 1}]")
        }
        data[index] = value
    }

    inline fun forEach(action: (Int) -> Unit) {
        for (i in 0 until this.size) {
            action.invoke(this[i])
        }
    }

    inline fun forEachIndexed(action: (Int, Int) -> Unit) {
        for (i in 0 until this.size) {
            action.invoke(i, this[i])
        }
    }

}
