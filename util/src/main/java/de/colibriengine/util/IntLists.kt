package de.colibriengine.util

fun intListOf(): IntList = IntList()

fun intListOf(value: Int): IntList = IntList(1) { value }

fun intListOf(value1: Int, value2: Int): IntList {
    val list = IntList(2)
    list.add(value1)
    list.add(value2)
    return list
}

fun intListOf(value1: Int, value2: Int, value3: Int): IntList {
    val list = IntList(3)
    list.add(value1)
    list.add(value2)
    list.add(value3)
    return list
}

inline fun buildIntList(initialSize: Int, builderAction: IntList.() -> Unit): IntList {
    val list = IntList(initialSize)
    list.builderAction()
    return list
}

inline fun buildIntList(builderAction: IntList.() -> Unit): IntList {
    val list = IntList()
    list.builderAction()
    return list
}
