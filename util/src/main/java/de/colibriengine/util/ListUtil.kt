package de.colibriengine.util

fun <T> MutableList<T>.removeByReplacingWithLast(index: Int): T {
    val removed = this[index]
    if (index != this.lastIndex) {
        this[index] = this.last()
    }
    removeLast()
    return removed
}
