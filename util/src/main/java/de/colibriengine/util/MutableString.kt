package de.colibriengine.util

import kotlin.math.absoluteValue
import kotlin.math.pow
import kotlin.math.roundToInt

class MutableString {

    private var chars: CharArray
    private var tmpC20: CharArray = CharArray(20) { ' ' }

    val internal: CharArray get() = chars
    val length: UInt get() = chars.size.toUInt()

    constructor(length: UInt, init: (Int) -> Char = { ' ' }) {
        chars = CharArray(length.toInt(), init)
    }

    constructor(chars: CharArray) {
        this.chars = chars
    }

    constructor(string: String) {
        chars = string.toCharArray()
    }

    fun clear(char: Char, startingAtIndex: Int = 0, length: Int = -1) {
        val to: Int = if (length == -1) this.length.toInt() else startingAtIndex + length
        chars.fill(char, startingAtIndex, to)
    }

    fun put(charArray: CharArray, startingAtIndex: Int = 0, length: Int = -1) {
        val len: Int = if (length == -1) charArray.size else length
        require(startingAtIndex + len <= chars.size)
        require(startingAtIndex >= 0)
        for (i in 0 until len) {
            chars[startingAtIndex + i] = charArray[i]
        }
    }

    fun putAtEnd(charArray: CharArray, startingAtIndex: Int = 0, length: Int = -1) {
        val len: Int = if (length == -1) charArray.size else length
        require(startingAtIndex + len <= chars.size)
        require(startingAtIndex >= 0)
        for (i in len - 1 downTo 0) {
            chars[chars.size - 1 - startingAtIndex - i] = charArray[len - 1 - i]
        }
    }

    fun put(string: String, startingAtIndex: Int = 0, length: Int = -1) {
        put(string.toCharArray(), startingAtIndex, length)
    }

    fun putAtEnd(string: String, startingAtIndex: Int = 0, length: Int = -1) {
        putAtEnd(string.toCharArray(), startingAtIndex, length)
    }

    fun put(float: Float, withFractionalDigits: Int, startingAtIndex: Int = 0) {
        val endPos = float.format(tmpC20, withFractionalDigits, 0)
        val sizeWritten = tmpC20.size - endPos
        System.arraycopy(tmpC20, tmpC20.size - sizeWritten, chars, startingAtIndex, sizeWritten)
    }

    fun putAtEnd(float: Float, withFractionalDigits: Int, startingAtIndex: Int = 0) {
        val endPos = float.format(tmpC20, withFractionalDigits, 0)
        val sizeWritten = tmpC20.size - endPos
        System.arraycopy(
            tmpC20, tmpC20.size - sizeWritten,
            chars, chars.size - startingAtIndex - sizeWritten,
            sizeWritten
        )
    }

    fun putAtEnd(double: Double, withFractionalDigits: Int, startingAtIndex: Int = 0) {
        val endPos = double.format(tmpC20, withFractionalDigits, 0)
        val sizeWritten = tmpC20.size - endPos
        System.arraycopy(
            tmpC20, tmpC20.size - sizeWritten,
            chars, chars.size - startingAtIndex - sizeWritten,
            sizeWritten
        )
    }

    fun put(int: Int, startingAtIndex: Int = 0) {
        put(int.toFloat(), 0, startingAtIndex)
    }

    fun putAtEnd(int: Int, startingAtIndex: Int = 0) {
        putAtEnd(int.toFloat(), 0, startingAtIndex)
    }

    fun put(long: Long, startingAtIndex: Int = 0) {
        put(long.toFloat(), 0, startingAtIndex) // TODO: do not convert to float!
    }

    fun putAtEnd(long: Long, startingAtIndex: Int = 0) {
        putAtEnd(long.toDouble(), 0, startingAtIndex)
    }

    fun charAt(index: Int) = get(index)

    operator fun get(index: Int) = chars[index]

    override fun toString(): String = String(chars)

}

fun Float.format(buffer: CharArray, decimals: Int, offset: Int = 0): Int {
    val v: Float = this.absoluteValue * (10f.pow(decimals))
    return parseNumber(v.roundToInt(), decimals, this < 0, buffer, offset)
}

fun Double.format(buffer: CharArray, decimals: Int, offset: Int = 0): Int {
    val v: Double = this.absoluteValue * (10.0.pow(decimals))
    return parseNumber(v.roundToInt(), decimals, this < 0, buffer, offset)
}

private fun parseNumber(
    number: Int,
    decimals: Int,
    negative: Boolean,
    buffer: CharArray,
    offset: Int
): Int {
    var num = number
    var idx = buffer.size - offset
    var i = 0
    while (i < decimals) {
        buffer[--idx] = ('0'.code + num % 10).toChar()
        i++
        num /= 10
    }
    if (decimals > 0) {
        buffer[--idx] = '.'
    }
    do {
        buffer[--idx] = ('0'.code + num % 10).toChar()
    } while (10.let { num /= it; num } != 0)
    if (negative) {
        buffer[--idx] = '-'
    }
    return idx
}
