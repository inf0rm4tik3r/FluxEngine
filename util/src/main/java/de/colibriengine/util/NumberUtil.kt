package de.colibriengine.util

import kotlin.math.floor

/**
 * NumberUtil
 *
 * @since ColibriEngine 0.0.7.0
 */
object NumberUtil {

    fun Float.toOneDecimalPlace(): Float = (floor((this * 10.0f).toDouble()) / 10.0f).toFloat()
    fun Float.toTwoDecimalPlaces(): Float = (floor((this * 100.0f).toDouble()) / 100.0f).toFloat()
    fun Float.toThreeDecimalPlaces(): Float = (floor((this * 1000.0f).toDouble()) / 1000.0f).toFloat()
    fun Float.toFourDecimalPlaces(): Float = (floor((this * 10000.0f).toDouble()) / 10000.0f).toFloat()

    fun Double.toOneDecimalPlace(): Double = floor(this * 10.0) / 10.0
    fun Double.toTwoDecimalPlaces(): Double = floor(this * 100.0) / 100.0
    fun Double.toThreeDecimalPlaces(): Double = floor(this * 1000.0) / 1000.0
    fun Double.toFourDecimalPlaces(): Double = floor(this * 10000.0) / 10000.0

    // Partial specialization optimization for 32-bit numbers.
    fun numDigits(x: Int): Int = when {
        x < 0 -> numDigits(-x) + 1
        x < 10 -> 1
        x < 100 -> 2
        x < 1000 -> 3
        x < 10000 -> 4
        x < 100000 -> 5
        x < 1000000 -> 6
        x < 10000000 -> 7
        x < 100000000 -> 8
        x < 1000000000 -> 9
        x < 10000000000 -> 10
        else -> throw RuntimeException("Unexpected fallthrough on $x")
    }

}
