package de.colibriengine.util

import de.colibriengine.reactive.BehaviorSubject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.atomic.AtomicInteger

// TODO: Introduces stuttering!
class ReactiveCpuReporter {

    // var os: OperatingSystemMXBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean::class.java)

    val systemCpuLoad: BehaviorSubject<Double> = BehaviorSubject(0.0)
    val processCpuLoad: BehaviorSubject<Double> = BehaviorSubject(0.0)

    private val nextCpuLoad: AtomicInteger = AtomicInteger(0)
    private val nextProcessCpuLoad: AtomicInteger = AtomicInteger(0)

    fun update() {
        systemCpuLoad.value = nextCpuLoad.get() / 1000.0
        processCpuLoad.value = nextProcessCpuLoad.get() / 1000.0
    }

    suspend fun updateState() = withContext(Dispatchers.Default) {
        //nextCpuLoad.set((os.cpuLoad * 1000.0).toInt())
        //nextProcessCpuLoad.set((os.processCpuLoad * 1000.0).toInt())
    }

}
