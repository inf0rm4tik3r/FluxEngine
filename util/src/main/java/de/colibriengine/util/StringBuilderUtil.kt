package de.colibriengine.util

import de.colibriengine.util.NumberUtil.toFourDecimalPlaces

val regex = "\\.".toRegex()

/**
 * Appends the given value so that positive and negative values are properly aligned.
 *
 * @param value
 * The value which is o be appended to this StringBuilder.
 */
fun StringBuilder.appendAligned(value: Int): StringBuilder {
    appendAlignedLong(this, value.toLong())
    return this
}

/**
 * Appends the given value so that positive and negative values are properly aligned.
 *
 * @param value
 * The value which is o be appended to this StringBuilder.
 */
fun StringBuilder.appendAligned(value: Long): StringBuilder {
    appendAlignedLong(this, value)
    return this
}

/**
 * Appends the given value so that positive and negative values are properly aligned. Also ensures that
 * the fraction of the appended string is made of exactly 4 digits.
 *
 * @param value
 * The value which is o be appended to this StringBuilder.
 */
fun StringBuilder.appendAligned(value: Float): StringBuilder {
    appendAlignedDouble(this, value.toDouble())
    return this
}

/**
 * Appends the given value so that positive and negative values are properly aligned. Also ensures that
 * the fraction of the appended string is made of exactly 4 digits.
 *
 * @param value
 * The value which is o be appended to this StringBuilder.
 */
fun StringBuilder.appendAligned(value: Double): StringBuilder {
    appendAlignedDouble(this, value)
    return this
}

private fun appendAlignedLong(builder: StringBuilder, value: Long) {
    // When calling Long.toString, a "-" will prepend to string if the input is negative.
    // We add an empty space here so that positive and negative numbers will have the same alignment in a multiline
    // string.
    if (value >= 0) {
        builder.append(" ")
    }
    val valueAsString = value.toString()
    builder.append(valueAsString)
}

/**
 * Appends the given double value so that positive and negative values are properly aligned. Also ensures that
 * the fraction of the appended string is made of exactly 4 digits.
 *
 * @param builder
 * The StringBuilder to which the value should be appended.
 * @param value
 * The value which is o be appended to the StringBuilder.
 */
private fun appendAlignedDouble(builder: StringBuilder, value: Double) {
    // When calling Float.toString, a "-" will prepend to string if the input is negative.
    // We add an empty space here so that positive and negative numbers will have the same alignment in a multiline
    // string.
    if (value >= 0) {
        builder.append(" ")
    }
    val valueAsString = value.toFourDecimalPlaces().toString()
    builder.append(valueAsString)

    val missingCharacters = 4 - valueAsString.split(regex)
            .dropLastWhile { it.isEmpty() }
            .toTypedArray()[1].length
    builder.append(" ".repeat(missingCharacters))
}

