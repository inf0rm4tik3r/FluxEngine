package de.colibriengine.util;

import de.colibriengine.exception.InstantiationNotAllowedException;
import de.colibriengine.logging.LogUtil;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * StringUtil - Utility functions for string checks and manipulation.
 *
 * @version 0.0.0.1
 * @time 25.03.2018 18:01
 * @since ColibriEngine 0.0.6.9
 */
@SuppressWarnings("WeakerAccess")
public final class StringUtil {

    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private static final String FWD_SLASH_STR = "/";
    private static final char FWD_SLASH = '/';

    private static final int STRING_FIRST_INDEX = 0;

    private static final String FILE_PROTOCOL_DEF = "file:";
    private static final int FILE_PROTOCOL_DEF_LEN = 5;

    private static final Logger LOG = LogUtil.getLogger(StringUtil.class);

    /**
     * Creation of this class is not allowed as it is a utility class.
     */
    private StringUtil() {
        throw new InstantiationNotAllowedException(getClass().getName());
    }

    /**
     * Checks if the provided {@code string} ends  with a forward slash.
     *
     * @param string The string to look at.
     * @return True if the string ends with a forward slash. False otherwise.
     */
    public static boolean endsWithForwardSlash(final @NotNull String string) {
        return string.endsWith(FWD_SLASH_STR);
    }

    /**
     * Treats the specified string as a file name and returns its extension.
     *
     * @param string The string to look at.
     * @return The extension. Everything after the last dot.
     * @throws IllegalArgumentException If the specified string did not contain a dot.
     */
    public static @NotNull String getFileExtension(final @NotNull String string) {
        final int indexOfLastDot = string.lastIndexOf('.');
        if (indexOfLastDot == -1) {
            throw new IllegalArgumentException("Specified string: " + string + " has no file extension.");
        }
        return string.substring(indexOfLastDot + 1);
    }

    /**
     * Removes the last part of a path; Everything up to the last slash and that slash.
     *
     * @param string The string to modify.
     * @return The modified string.
     */
    public static @NotNull String removeLastPart(final @NotNull String string) {
        String result = toForwardSlashes(string);
        result = removeLastForwardSlash(result);
        final int lastSlash = result.lastIndexOf(FWD_SLASH);
        if (lastSlash != -1) {
            result = result.substring(STRING_FIRST_INDEX, lastSlash);
            result = removeLastForwardSlash(result);
        }
        return result;
    }

    /**
     * Removes the last forward slash from the specified string.
     *
     * @param string The string to modify.
     * @return The modified string.
     */
    public static @NotNull String removeLastForwardSlash(final @NotNull String string) {
        if (string.endsWith(FWD_SLASH_STR)) {
            return string.substring(STRING_FIRST_INDEX, string.length() - 1);
        }
        return string;
    }

    /**
     * Converts all slashes inside {@code string} to forward slashes.
     *
     * @param string The string to modify.
     * @return The modified string.
     */
    public static @NotNull String toForwardSlashes(final @NotNull String string) {
        return string.replace("\\", "/");
    }

    /**
     * Removes the class path and all other leading stuff (slashes, etc.) from the specified {@code absolute} path
     * and returns the result.
     *
     * @param absolute An absolute path to some location under the current class path.
     * @return A path relative to the main class path.
     * @throws IllegalArgumentException if the specified path is not under the main class path of this running application.
     * @see SystemKt CLASS_PATH TODO
     */
    public static @NotNull String getRelativePath(
            //final @NotNull String workingDirectory,
            final @NotNull String absolute
    ) throws IllegalArgumentException {
        // TODO: Does not work!
        // TODO: Class path is for example: "C:/Users/lupo-/IdeaProjects/ColibriEngine/util/target/classes"
        return removeLeadingForwardSlashes(
            removeLeadingExclamationMarks(removeLeadingForwardSlashes(removeLeadingFileProtocol(
                absolute
                    .replace("\\", "/")
                    .replace(SystemKt.getWORKING_DIRECTORY(), "")
            )))
        );
    }

    public static @NotNull String removeLeadingFileProtocol(final @NotNull String string) {
        if (string.startsWith(FILE_PROTOCOL_DEF)) {
            return string.substring(FILE_PROTOCOL_DEF_LEN);
        }
        return string;
    }

    public static @NotNull String removeLeadingExclamationMarks(final @NotNull String string) {
        return string.replaceAll("^!+", "");
    }

    /**
     * Removes all leading forward slashed from the specified string.
     *
     * @param string The string to strip.
     * @return Modified string.
     */
    public static @NotNull String removeLeadingForwardSlashes(final @NotNull String string) {
        return string.replaceAll("^/+", "");
    }

    /**
     * Tries to identify the file type by looking at its extension at the end of the specified path.
     * An extension could be: ".pdf", ".png", etc.
     * If no such extension could be found, an empty string is returned.
     * <p>
     * If the path
     *
     * @param path The path to look at.
     * @return Extension name of the file if found. Otherwise an empty string.
     */
    public static @NotNull String identifyFileTypeFromExtension(final @NotNull String path) {
        final int indexOfLastDot = path.lastIndexOf('.');
        return indexOfLastDot == -1 ? "" : path.substring(indexOfLastDot + 1);
    }

    /**
     * Identify file type of file with provided path and name using JDK 7's {@link Files#probeContentType(Path)}.
     *
     * @param path Name of file whose type is desired.
     * @return String representing identified type of file with provided name.
     * @throws IOException If {@code path} did not point to a valid file.
     */
    public static @Nullable String identifyFileTypeUsingFilesProbeContentType(final @NotNull Path path)
            throws IOException {
        return Files.probeContentType(path);
    }

    /**
     * Identify file type of file with provided path and name using JDK 7's {@link Files#probeContentType(Path)}.
     *
     * @param fileName Name of file whose type is desired.
     * @return String representing identified type of file with provided name.
     * @throws IOException If {@code path} did not point to a valid file.
     */
    public static @Nullable String identifyFileTypeUsingFilesProbeContentType(final @NotNull String fileName)
            throws IOException {
        return identifyFileTypeUsingFilesProbeContentType(new File(fileName).toPath());
    }

}
