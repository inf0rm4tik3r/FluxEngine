package de.colibriengine.util

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.util.UnitConverter.byteToGibibyte
import de.colibriengine.util.UnitConverter.byteToMebibyte
import java.io.File
import java.io.IOException

private const val PROPERTY_USER_DIR = "user.dir"
private const val PROPERTY_OS_NAME = "os.name"
private const val PROPERTY_OS_VERSION = "os.version"
private const val PROPERTY_OS_ARCH = "os.arch"
private const val PROPERTY_JAVA_CLASS_PATH = "java.class.path"

private val LOG = getLogger("System")

/**
 * The main class path of the running application.
 */
val CLASS_PATH = System.getProperty(PROPERTY_JAVA_CLASS_PATH)
    .split(File.pathSeparator.toRegex())
    .toTypedArray()[0]
    .replace("\\", "/")

/**
 * The working directory. Should point to the main folder from which the application runs
 * or to the applications jar file.
 */
val WORKING_DIRECTORY: String = File(PROPERTY_USER_DIR)
    .absolutePath.replace("\\", "/")

/**
 * The working directory. Should point to the main folder from which the application runs
 * or to the applications jar file.
 */
fun getWorkingDirectory(clazz: Class<Any>): String =
    File(clazz.javaClass.protectionDomain.codeSource.location.toURI().path)
        .absolutePath.replace("\\", "/")

/**
 * The name of the operating system as defined by the `os.name` system property.
 */
val OS_NAME: String = System.getProperty(PROPERTY_OS_NAME)

/**
 * The version of the operating system as defined by the `os.version` system property.
 */
val OS_VERSION: String = System.getProperty(PROPERTY_OS_VERSION)

/**
 * The architecture of the operating system as defined by the `os.arch` system property.
 */
val OS_ARCH: String = System.getProperty(PROPERTY_OS_ARCH)

/**
 * Information about the operating system.
 */
val OS_INFO: String = """
    OS: $OS_NAME
    Version: $OS_VERSION
    Architecture: $OS_ARCH
""".trimIndent()

/**
 * The number of cpu cores available to the java virtual machine.
 */
val CPU_CORES: Int = Runtime.getRuntime().availableProcessors()

/**
 * Information about the cpu.
 */
val CPU_INFO: String = "Available CPU cores: " + CPU_CORES


val REPORT_TO_LOG: SystemMemoryReporterCallback = { total, used, deltaUsed, free, deltaFree ->
    LOG.info("Memory usage: ")
    LOG.info("total: {}", byteToMebibyte(total))
    LOG.info("used:  {}", byteToMebibyte(used))
    LOG.info("∆used: {}", byteToMebibyte(deltaUsed))
    LOG.info("free:  {}", byteToMebibyte(free))
    LOG.info("∆free: {}", byteToMebibyte(deltaFree))
}
val REPORT_TO_NONE: SystemMemoryReporterCallback = { _, _, _, _, _ -> }
val MEMORY_REPORTER: SystemMemoryReporter = SystemMemoryReporter(REPORT_TO_LOG)

/**
 * The current memory usage.
 */
fun getMemoryUsage(): String {
    MEMORY_REPORTER.reporterCallback = REPORT_TO_NONE
    MEMORY_REPORTER.update()
    MEMORY_REPORTER.reporterCallback = REPORT_TO_LOG
    return """
       total: ${byteToMebibyte(MEMORY_REPORTER.total)} MB
       used: ${byteToMebibyte(MEMORY_REPORTER.used)} MB
       ∆sed: ${byteToMebibyte(MEMORY_REPORTER.deltaUsed)} MB
       free: ${byteToMebibyte(MEMORY_REPORTER.free)} MB
       ∆free: ${byteToMebibyte(MEMORY_REPORTER.deltaFree)} MB
    """.trimIndent()
}

/**
 * A list of all the available file system roots (partitions) with space information (total, used, free).
 */
fun getDiskUsage(): String {
    val sb = StringBuilder()
    for (root in File.listRoots()) {
        sb.append("Partition: ").append(root.absolutePath).append("\n")
        sb.append("total: ").append(byteToGibibyte(root.totalSpace)).append("GB \n")
        sb.append("free: ").append(byteToGibibyte(root.freeSpace)).append("GB \n")
        sb.append("used: ").append(byteToGibibyte(root.usableSpace)).append("GB \n")
    }
    return sb.toString()
}

/**
 * Returns the direct caller of the method from which the given stack trace was created.
 *
 * @param stackTraceElements
 * The stack trace to work with.
 *
 * @return The 1. caller: Second element of the stack, or null if the stack trace does not contain enough elements.
 */
fun getCaller(stackTraceElements: Array<StackTraceElement?>): StackTraceElement? {
    return if (stackTraceElements.size >= 2) stackTraceElements[2] else null
}

/**
 * Returns the method name of the direct caller of the method from which the given stack trace was created.
 *
 * @param stackTraceElements
 * The stack trace to work with.
 *
 * @return The name of the caller method or an empty string if there was no caller.
 */
fun getCallerMethodName(stackTraceElements: Array<StackTraceElement?>): String {
    val caller = getCaller(stackTraceElements)
    return if (caller != null) caller.methodName else ""
}

/**
 * Updates the system property [propertyName].
 *
 * @param propertyName
 * The name of the property which should be changed.
 * @param newPropertyValue
 * The new property value.
 */
fun setSystemProperty(propertyName: String, newPropertyValue: String) {
    val before = System.setProperty(propertyName, newPropertyValue)
    LOG.trace("System property of \"$propertyName\" (before): $before")
    LOG.trace("System property of \"$propertyName\" (now): $newPropertyValue")
}

/**
 * Prints information about the system to the log.
 */
fun logSystemInfo() {
    LOG.info("OS")
    for (line in OS_INFO.lines()) {
        LOG.info("-- ${line.trim()}")
    }
    LOG.info("CPU")
    for (line in CPU_INFO.lines()) {
        LOG.info("-- ${line.trim()}")
    }
    LOG.info("RAM")
    for (line in getMemoryUsage().lines()) {
        LOG.info("-- ${line.trim()}")
    }
}

inline fun waitForUserInput(then: (String?) -> Unit) {
    try {
        println("Press any key to continue...")
        // System.`in`.read()
        then(readLine())
    } catch (e: IOException) {
        e.printStackTrace()
    }
}
