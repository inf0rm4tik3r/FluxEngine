package de.colibriengine.util

import kotlin.Unit

/**
 * All arguments are in bytes.
 *
 * @param total The total amount of available memory to the jvm.
 * @param used The used memory.
 * @param deltaUsed The difference between the previously and currently used memory.
 * @param free The free memory.
 * @param deltaFree The difference between the previously and currently free memory.
 */
typealias SystemMemoryReporterCallback = (total: Long, used: Long, deltaUsed: Long, free: Long, deltaFree: Long) -> Unit

/** @since ColibriEngine 0.0.7.0 */
class SystemMemoryReporter(
    /** The callback which should be used to inform that the memory usage changed. */
    var reporterCallback: SystemMemoryReporterCallback
) {

    /** Stores the previously calculated total memory. */
    var total: Long = 0
        private set

    /** Stores the previously calculated used memory. */
    var used: Long = 0
        private set

    /** Stores the previously calculated delta of used memory. */
    var deltaUsed: Long = 0
        private set

    /** Stores the previously calculated free memory. */
    var free: Long = 0
        private set

    /** Stores the previously calculated delta of free memory. */
    var deltaFree: Long = 0
        private set

    init {
        total = runtime.totalMemory()
        free = runtime.freeMemory()
        used = total - free
    }

    /**
     * Updates this objects memory information knowledge. If a change in memory usage is detected this objects
     * [SystemMemoryReporterCallback] will be called with the new memory usage information.
     */
    fun update() {
        val total = runtime.totalMemory()
        val free = runtime.freeMemory()

        if (total != this.total || free != this.free) {
            // The currently "used" memory.
            val used = total - free

            // The difference between the previously and currently "used" memory.
            val deltaUsed = used - this.used

            // The difference between the previously and currently "free" memory.
            val deltaFree = free - this.free

            reporterCallback(total, used, deltaUsed, free, deltaFree)

            this.total = total
            this.used = used
            this.deltaUsed = deltaUsed
            this.free = free
            this.deltaFree = deltaFree
        }
    }

    companion object {
        /** The JVM runtime. */
        private val runtime = Runtime.getRuntime()
    }

}
