package de.colibriengine.util

import kotlin.math.pow

object UnitConverter {

    /** Constant to convert degree celsius to kelvin. */
    private const val DEGREE_C_TO_KELVIN = 273.15f

    /*
     * Constants to convert byte sizes in base 10.
     */
    private val BYTE_TO_KILOBYTE = 10.0.pow(3.0).toFloat()
    private val BYTE_TO_MEGABYTE = 10.0.pow(6.0).toFloat()
    private val BYTE_TO_GIGABYTE = 10.0.pow(9.0).toFloat()
    private val BYTE_TO_TERABYTE = 10.0.pow(12.0).toFloat()
    private val BYTE_TO_PETABYTE = 10.0.pow(15.0).toFloat()
    private val BYTE_TO_EXABYTE = 10.0.pow(18.0).toFloat()
    private val BYTE_TO_ZETTABYTE = 10.0.pow(21.0).toFloat()
    private val BYTE_TO_YOTTABYTE = 10.0.pow(24.0).toFloat()

    /*
     * Constants to convert byte sizes in base 2.
     */
    private val BYTE_TO_KIBIBYTE = 2.0.pow(10.0).toFloat()
    private val BYTE_TO_MEBIBYTE = 2.0.pow(20.0).toFloat()
    private val BYTE_TO_GIBIBYTE = 2.0.pow(30.0).toFloat()
    private val BYTE_TO_TEBIBYTE = 2.0.pow(40.0).toFloat()
    private val BYTE_TO_PEBIBYTE = 2.0.pow(50.0).toFloat()
    private val BYTE_TO_EXBIBYTE = 2.0.pow(60.0).toFloat()
    private val BYTE_TO_ZEBIBYTE = 2.0.pow(70.0).toFloat()
    private val BYTE_TO_YOBIBYTE = 2.0.pow(80.0).toFloat()

    fun degreeCToKelvin(degreeC: Float): Float = degreeC + DEGREE_C_TO_KELVIN
    fun kelvinToDegreeC(kelvin: Float): Float = kelvin - DEGREE_C_TO_KELVIN

    fun byteToKilobyte(bytes: Long): Float = bytes / BYTE_TO_KILOBYTE
    fun byteToMegabyte(bytes: Long): Float = bytes / BYTE_TO_MEGABYTE
    fun byteToGigabyte(bytes: Long): Float = bytes / BYTE_TO_GIGABYTE
    fun byteToTerabyte(bytes: Long): Float = bytes / BYTE_TO_TERABYTE
    fun byteToPetabyte(bytes: Long): Float = bytes / BYTE_TO_PETABYTE
    fun byteToExabyte(bytes: Long): Float = bytes / BYTE_TO_EXABYTE
    fun byteToZettabyte(bytes: Long): Float = bytes / BYTE_TO_ZETTABYTE
    fun byteToYottabyte(bytes: Long): Float = bytes / BYTE_TO_YOTTABYTE

    fun byteToKibibyte(bytes: Long): Float = bytes / BYTE_TO_KIBIBYTE
    fun byteToMebibyte(bytes: Long): Float = bytes / BYTE_TO_MEBIBYTE
    fun byteToGibibyte(bytes: Long): Float = bytes / BYTE_TO_GIBIBYTE
    fun byteToTebibyte(bytes: Long): Float = bytes / BYTE_TO_TEBIBYTE
    fun byteToPebibyte(bytes: Long): Float = bytes / BYTE_TO_PEBIBYTE
    fun byteToExbibyte(bytes: Long): Float = bytes / BYTE_TO_EXBIBYTE
    fun byteToZebibyte(bytes: Long): Float = bytes / BYTE_TO_ZEBIBYTE
    fun byteToYobibyte(bytes: Long): Float = bytes / BYTE_TO_YOBIBYTE

}
