package de.colibriengine.util

data class Version(val major: Int, val minor: Int, val release: Int, val build: Int) {

    override fun toString(): String = "$major.$minor.$release.$build"

}
