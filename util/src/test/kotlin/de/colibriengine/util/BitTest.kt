package de.colibriengine.util

import org.junit.jupiter.api.Test
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class BitTest {

    @Test
    fun construct_shouldFail_whenGivenNumbersBelowZeroOrAbove1() {
        assertFails { Bit(-1) }
        assertFails { Bit(2) }
    }

    @Test
    fun zero_shouldBeTrue_whenConstructedWithZero() {
        assertTrue { Bit(0).zero }
    }

    @Test
    fun zero_shouldBeFalse_whenConstructedWithOne() {
        assertFalse { Bit(1).zero }
    }

    @Test
    fun one_shouldBeFalse_whenConstructedWithZero() {
        assertFalse { Bit(0).one }
    }

    @Test
    fun one_shouldBeTrue_whenConstructedWithOne() {
        assertTrue { Bit(1).one }
    }

}
