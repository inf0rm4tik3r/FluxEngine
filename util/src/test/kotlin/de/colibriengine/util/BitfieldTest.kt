package de.colibriengine.util

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class BitfieldTest {

    @Test
    fun construct_shouldCreateCorrectBitfield_whenGivenCustomBitValues() {
        val bitfield = Bitfield(1, 0, 0, 1, 0, 1, 1, 0)
        assertBitfield(intArrayOf(1, 0, 0, 1, 0, 1, 1, 0), bitfield.toIntArray())
    }

    @Test
    fun testMaximumValue() {
        val bitfield = Bitfield(255.toUByte())
        assertBitfield(intArrayOf(1, 1, 1, 1, 1, 1, 1, 1), bitfield.toIntArray())
    }

    private fun assertBitfield(expected: IntArray, actual: IntArray) {
        assertEquals(expected.size, actual.size)
        for (i in expected.indices) {
            assertEquals(
                expected[i],
                actual[i],
                "Value at index $i was expected to be ${expected[i]} but was ${actual[i]}."
            )
        }
    }

}
