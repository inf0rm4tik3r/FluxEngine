package de.colibriengine.util

import de.colibriengine.util.Bit.Companion.ONE
import de.colibriengine.util.Bit.Companion.ZERO
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class ByteOperationsTest {

    /*
     * REMINDER:
     *
     *          0  1  2  3  4  5  6  7   | Index from left
     *          7  6  5  4  3  2  1  0   | Index from right
     *
     *        128 64 32 16  8  4  2  1  +| 255
     *
     *   0  ->  0  0  0  0  0  0  0  0
     *   1  ->  0  0  0  0  0  0  0  1
     *  46  ->  0  0  1  0  1  1  1  0
     * 118  ->  0  1  1  1  0  1  1  0
     * 255  ->  1  1  1  1  1  1  1  1
     *
     */

    @Test
    fun bitsToUByte_shouldConstructCorrectByteValue() {
        val actual = bitsToUByte(0, 0, 1, 0, 1, 1, 1, 0)
        assertEquals(46.toUByte(), actual)
    }

    @Test
    fun bitFromRight_shouldReturnCorrectResult_whenByteIs0() {
        assertBitsFromRight(0.toUByte(), ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO)
    }

    @Test
    fun bitFromRight_shouldReturnCorrectResult_whenByteIsPositive() {
        assertBitsFromRight(46.toUByte(), ZERO, ZERO, ONE, ZERO, ONE, ONE, ONE, ZERO)
    }

    @Test
    fun bitFromRight_shouldReturnCorrectResult_whenByteIs255() {
        assertBitsFromRight(255.toUByte(), ONE, ONE, ONE, ONE, ONE, ONE, ONE, ONE)
    }

    private fun assertBitsFromRight(
        byte: UByte, at7: Bit, at6: Bit, at5: Bit, at4: Bit, at3: Bit, at2: Bit, at1: Bit, at0: Bit
    ) {
        assertEquals(at0, byte.bitFromRight(0), "Bit at 0 was not $at0!")
        assertEquals(at1, byte.bitFromRight(1), "Bit at 1 was not $at1!")
        assertEquals(at2, byte.bitFromRight(2), "Bit at 2 was not $at2!")
        assertEquals(at3, byte.bitFromRight(3), "Bit at 3 was not $at3!")
        assertEquals(at4, byte.bitFromRight(4), "Bit at 4 was not $at4!")
        assertEquals(at5, byte.bitFromRight(5), "Bit at 5 was not $at5!")
        assertEquals(at6, byte.bitFromRight(6), "Bit at 6 was not $at6!")
        assertEquals(at7, byte.bitFromRight(7), "Bit at 7 was not $at7!")
    }

    @Test
    fun bitFromLeft_shouldReturnCorrectResult_whenByteIs0() {
        assertBitsFromLeft(0.toUByte(), ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO)
    }

    @Test
    fun bitFromLeft_shouldReturnCorrectResult_whenByteIsPositive() {
        assertBitsFromLeft(46.toUByte(), ZERO, ZERO, ONE, ZERO, ONE, ONE, ONE, ZERO)
    }

    @Test
    fun bitFromLeft_shouldReturnCorrectResult_whenByteIs255() {
        assertBitsFromLeft(255.toUByte(), ONE, ONE, ONE, ONE, ONE, ONE, ONE, ONE)
    }

    private fun assertBitsFromLeft(
        byte: UByte, at0: Bit, at1: Bit, at2: Bit, at3: Bit, at4: Bit, at5: Bit, at6: Bit, at7: Bit
    ) {
        assertEquals(at0, byte.bitFromLeft(0), "Bit at 0 was not $at0!")
        assertEquals(at1, byte.bitFromLeft(1), "Bit at 1 was not $at1!")
        assertEquals(at2, byte.bitFromLeft(2), "Bit at 2 was not $at2!")
        assertEquals(at3, byte.bitFromLeft(3), "Bit at 3 was not $at3!")
        assertEquals(at4, byte.bitFromLeft(4), "Bit at 4 was not $at4!")
        assertEquals(at5, byte.bitFromLeft(5), "Bit at 5 was not $at5!")
        assertEquals(at6, byte.bitFromLeft(6), "Bit at 6 was not $at6!")
        assertEquals(at7, byte.bitFromLeft(7), "Bit at 7 was not $at7!")
    }

}
