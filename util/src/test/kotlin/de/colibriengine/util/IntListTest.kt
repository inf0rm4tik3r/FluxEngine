package de.colibriengine.util

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class IntListTest {

    @Test
    fun createEmpty() {
        val list = IntList()
        assertEquals(0, list.size)
    }

    @Test
    fun createFilled() {
        val list = IntList(3) { it }
        assertEquals(3, list.size)
        assertEquals(0, list[0])
        assertEquals(1, list[1])
        assertEquals(2, list[2])
    }

    @Test
    fun forEach() {
        val list = IntList(3) { it }
        val actual = mutableListOf<Int>()
        list.forEach { actual.add(it) }
        assertEquals(listOf(0, 1, 2), actual)
    }

    @Test
    fun forEachIndexed() {
        val list = IntList(3) { it + 1 }
        val indices = mutableListOf<Int>()
        val actual = mutableListOf<Int>()
        list.forEachIndexed{ idx, value ->
            indices.add(idx)
            actual.add(value)
        }
        assertEquals(listOf(0, 1, 2), indices)
        assertEquals(listOf(1, 2, 3), actual)
    }

}
