package de.colibriengine.vr

interface OvrSessionManager {

    /**
     * Tries to open a new {@link OVRSession} to allow communication with the HMD. Conditions:
     * - The {@link OVRLib} must have been initialized. This typically happens when calling {@link
     *   ColibriEngineImpl#initializeLibraries(String)}.
     * - An HMD must be connected.
     *
     * Each created session will automatically be part of this OVRSessionManager instance and must not be added
     * manually.
     *
     * @return A {@code OVRSession} instance or null if a condition was not met or the session creation failed.
     */
    fun createSession(): OvrSession?

    /**
     * Destroys all {@link OvrSessionImpl} instances managed by this OVRSessionManager and gets rid of their references.
     *
     * @throws UnsupportedOperationException If the OVR library is currently not initialized.
     */
    fun releaseAll()

}
