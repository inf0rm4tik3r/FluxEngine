package de.colibriengine.vr.oculus;

import org.jetbrains.annotations.NotNull;

public class OVRInitializationException extends RuntimeException {

    private int errorID;
    private String internalErrorMsg;

    public OVRInitializationException() {
        super();
    }

    public OVRInitializationException(final @NotNull String message) {
        super(message, null);
    }

    public OVRInitializationException(final @NotNull String message, final @NotNull Throwable throwable) {
        super(message, throwable);
    }

    public OVRInitializationException(
            final @NotNull String message,
            final int errorID,
            final String internalErrorMsg
    ) {
        super(message);
        this.errorID = errorID;
        this.internalErrorMsg = internalErrorMsg;
    }

    public OVRInitializationException(
            final @NotNull String message,
            final int errorID,
            final String internalErrorMsg,
            final @NotNull Throwable throwable
    ) {
        super(message, throwable);
        this.errorID = errorID;
        this.internalErrorMsg = internalErrorMsg;
    }

    public int getErrorID() {
        return errorID;
    }

    public String getInternalErrorMsg() {
        return internalErrorMsg;
    }

}
