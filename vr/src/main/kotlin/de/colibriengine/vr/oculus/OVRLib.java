package de.colibriengine.vr.oculus;

import de.colibriengine.logging.LogUtil;
import org.apache.logging.log4j.Logger;
import org.lwjgl.ovr.*;
import org.lwjgl.system.MemoryUtil;

import static org.lwjgl.ovr.OVR.*;
import static org.lwjgl.ovr.OVRErrorCode.ovrSuccess;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * OVR - Library interface.
 */
public class OVRLib {

    /**
     * This message gets thrown in an UnsupportedOperationException if a method gets called which relies on the OVRLib
     * class to be initialized properly.
     */
    private static final String OVR_LIB_NOT_INITIALIZED_ERROR_MSG =
            "The OVR library must first be initialized through a call to OVRLib#initialize()!";

    /**
     * Defines if the OVR library is currently initialized.
     */
    private static boolean initialized = false;

    private static final Logger LOG = LogUtil.getLogger(OVRLib.class);

    /**
     * Initialized the OVR library. Must be called if the Oculus Rift should be used.
     * First check if a HMD is present / connected by calling {@link OVRLib#isHMDConnected()}.
     * You can then create an OVRSession by instantiating a new {@link OvrSessionImpl} instance after this method
     * successfully returned.
     * Any additional call to this method after a previous call successfully initialized the library has no effect and
     * will return immediately. Calling {@link OVRLib#terminate()} after using the OVR library is mandatory!
     * This method might fail to return correctly due to some errors. In each case, a
     * {@link OVRInitializationException} is thrown.
     * This method could be periodically called in order to try to initialize OVR.
     *
     * @throws OVRInitializationException If the OVR library could not be initialized. This might be the case due to one of the following conditions:
     *                                    <ul>
     *                                    <li>The OVRRuntimeService is not running</li>
     *                                    </ul>
     */
    public static void initialize() throws OVRInitializationException {
        if (initialized) {
            LOG.info("OVR#initialize: already initialized. Skipping...");
            return;
        }

        LOG.info("initializing...");

        // This callback should be set in the initialization parameter list. It allows the logging of OVR events.
        final OVRLogCallback logCallback = createLogCallback();

        // Create a parameter object. Used to pass options to the initialization function.
        final OVRInitParams initParams = OVRInitParams.mallocStack();
        initParams.set(ovrInit_Debug, 0, logCallback, 0, 0);

        // Try to initialize the OVR library.
        final int initResult = ovr_Initialize(initParams);
        if (initResult == ovrSuccess) {
            LOG.info("successful");
        } else {
            final OVRErrorInfo ovrErrorInfo = OVRErrorInfo.mallocStack();
            ovr_GetLastErrorInfo(ovrErrorInfo);
            final String errorMsg = "[ " + ovrErrorInfo.Result() + " ] - " + ovrErrorInfo.ErrorStringString();
            LOG.error("failed: " + errorMsg);
            throw new OVRInitializationException(
                    "OVR#initialize: failed: " + errorMsg, ovrErrorInfo.Result(), ovrErrorInfo.ErrorStringString()
            );
        }

        // Display the OVR version to the user.
        final String ovrVersion = ovr_GetVersionString();
        LOG.info("OVR Version = " + ovrVersion);

        initialized = true;
    }

    /**
     * Terminates the OVR library. Must be called at some point after a call to {@link OVRLib#initialize()} was made.
     */
    public static void terminate() {
        if (!initialized) {
            LOG.info("OVR#terminate: not initialized. Skipping...");
        }

        LOG.info("OVR#terminate: ...");

        // And terminate OVR.
        ovr_Shutdown();

        LOG.info("OVR#terminate: successful");

        initialized = false;
    }

    /**
     * @return If this class is initialized.
     */
    public static boolean isInitialized() {
        return initialized;
    }

    /**
     * Checks if this class is initialized.
     *
     * @throws UnsupportedOperationException If the OVR library is currently not initialized.
     */
    static void checkInitialized() throws UnsupportedOperationException {
        if (!initialized) {
            throw new UnsupportedOperationException(OVR_LIB_NOT_INITIALIZED_ERROR_MSG);
        }
    }

    /**
     * Creates a {@link OVRLogCallback} which can be used as a parameter in the
     * {@link OVRInitParams#set(int, int, OVRLogCallbackI, long, int)} function.
     *
     * @return A configured OVRLogCallback instance which prints human readable LOG messages to either the standard or
     * the error output stream, depending on the LOG-level of each message.
     */
    private static OVRLogCallback createLogCallback() {
        return new OVRLogCallback() {
            @Override
            public void invoke(final long userData, final int level, final long message) {
                final String levelDescription = switch (level) {
                    case ovrLogLevel_Debug -> "debug";
                    case ovrLogLevel_Info -> "info ";
                    case ovrLogLevel_Error -> "error";
                    default -> "?????";
                };

                // Constructs the message to display.
                final String logMessage = "OVR#LOG [" + levelDescription + "]: " + MemoryUtil.memASCII(message);

                // Print the message in either the default or the error output stream. Depending on its level.
                if (level == ovrLogLevel_Error) {
                    LOG.error(logMessage);
                } else {
                    LOG.info(logMessage);
                }
            }
        };
    }

    /**
     * Checks if an HMD is connected.
     * You can periodically call this method to poll for the presence of an HMD.
     *
     * @return {@code true} if an HMD is available / connected. Creating a new {@link OvrSessionImpl} should then not
     * throw an exception and successfully create a new instance.
     * @throws UnsupportedOperationException If the OVR library is currently not initialized.
     */
    public static boolean isHMDConnected() throws UnsupportedOperationException {
        checkInitialized();

        // Pull the HMD description from OVR without specifying a session handle.
        final OVRHmdDesc hmdDesc = OVRHmdDesc.mallocStack();
        ovr_GetHmdDesc(NULL, hmdDesc);

        // A HMD is connected if the returned type is not NONE!
        return hmdDesc.Type() != ovrHmd_None;
    }

}
