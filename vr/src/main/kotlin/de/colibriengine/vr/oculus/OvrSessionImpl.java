package de.colibriengine.vr.oculus;

import de.colibriengine.logging.LogUtil;
import de.colibriengine.vr.OvrSession;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.PointerBuffer;
import org.lwjgl.ovr.*;
import org.lwjgl.system.MemoryStack;

import static org.lwjgl.ovr.OVR.*;
import static org.lwjgl.ovr.OVRErrorCode.ovrSuccess;
import static org.lwjgl.system.MemoryUtil.NULL;

/*
 * ----- FURTHER READING -----
 *
 * https://developer.oculus.com/documentation/pcsdk/latest/concepts/dg-sensor/#dg_sensor
 * https://codelab.wordpress.com/2014/09/07/oculusvr-sdk-and-simple-oculus-rift-dk2-opengl-test-program/
 * http://forum.lwjgl.org/index.php?topic=6190.0
 * https://forums.oculusvr.com/developer/discussion/88/opengl-full-example-and-shader
 * http://forum.lwjgl.org/index.php?topic=4895.msg26630#msg26630
 * https://github.com/WhiteHexagon/example-lwjgl3-rift/blob/master/src/main/java/com/sunshineapps/riftexample
 * /HelloLibOVR.java
 * https://github.com/WhiteHexagon/example-lwjgl3-rift/blob/master/src/main/java/com/sunshineapps/riftexample
 * /RiftClient0600.java
 */

/**
 * TODO: Split into multiple classes.
 */
@SuppressWarnings("WeakerAccess")
public class OvrSessionImpl implements OvrSession {

    /**
     * OVRs handle to this session.
     */
    private final long sessionHandle;

    /**
     * Whether or not additional debug checks should be performed on this instance.
     * Might reduce performance if set true.
     */
    private boolean debugMode;
    private static final boolean DEBUG_MODE_DEFAULT = true;

    public static final int MIN_TRACKER_INDEX = 0;

    private static final Logger LOG = LogUtil.getLogger(OvrSessionImpl.class);

    /**
     * Instantiates a new OVR session and registers it in the session manager located in {@link OVRLib}.
     *
     * @throws OVRSessionException If the session could not be created.
     */
    public OvrSessionImpl() throws OVRSessionException {
        debugMode = DEBUG_MODE_DEFAULT;

        // The OVR library must have been initialized!
        OVRLib.checkInitialized();

        final PointerBuffer sessionPointer = MemoryStack.stackMallocPointer(1);
        final OVRGraphicsLuid graphicsLuid = OVRGraphicsLuid.mallocStack();

        // Try to create an OVR session.
        final int createResult = ovr_Create(sessionPointer, graphicsLuid);
        if (createResult == ovrSuccess) {
            LOG.info("successful");
        } else {
            final OVRErrorInfo ovrErrorInfo = OVRErrorInfo.mallocStack();
            ovr_GetLastErrorInfo(ovrErrorInfo);
            final String errorMsg = "[ " + ovrErrorInfo.Result() + " ] - " + ovrErrorInfo.ErrorStringString();
            LOG.error("failed: " + errorMsg);
            throw new OVRSessionException(
                    "failed: " + errorMsg, ovrErrorInfo.Result(), ovrErrorInfo.ErrorStringString()
            );
        }

        // Session was successfully created. We can now extract the sessionHandle form sessionPointer.
        sessionHandle = sessionPointer.get(0);
        if (sessionHandle == NULL) {
            throw new OVRSessionException("Created handle was NULL...");
        }

        // Print information about the current status of the created session.
        LOG.info(createSessionStatusInfoString(getSessionStatus()));

        // Query the description of the attached HMD.
        final OVRHmdDesc hmdDesc = getHMDDescription();

        // Print information about the connected HMD.
        LOG.info(createHMDInfoString(hmdDesc));

        // Print information about the available tracker devices.
        for (int i = MIN_TRACKER_INDEX; i < getTrackerCount(); i++) {
            LOG.info("Tracker[{}]: ", createTrackerDescriptionString(getTrackerDescription(i)));
        }
    }

    /*
    def getTextureSwapChainCurrentIndex(session: Long, chain: Long): Int = {
        val buffer = BufferUtils.createIntBuffer(1)
        ovr_GetTextureSwapChainCurrentIndex(session, chain, buffer)
        val currentIndex = buffer.get(0)
        return currentIndex
    }
    def getTextureSwapChainLength(session: Long, chain: Long): Int = {
        val buffer = BufferUtils.createIntBuffer(1)
        ovr_GetTextureSwapChainLength(session, chain, buffer)
        val textureCount = buffer.get(0)
        return textureCount
    }
    def getTextureSwapChainBufferGL(session: Long, chain: Long, index: Int): Int = {
        val buffer = BufferUtils.createIntBuffer(1)
        ovr_GetTextureSwapChainBufferGL(session, chain, index, buffer)
        val textureID = buffer.get(0)
        return textureID
    }
    public Mat4f convertPoseToMatrix(pose: OVRPosef): Mat4f = {
        val PcurInv = Mat4f.translate(-pose.Position.x, -pose.Position.y, -pose.Position.z)
        val QcurInv = new Quaternion(-pose.Orientation.x, -pose.Orientation.y, -pose.Orientation.z, pose.Orientation
        .w).castToOrientationMatrix
        QcurInv * PcurInv
    }


    public void getPredictedDisplayTime() {
        ovr_GetPredictedDisplayTime(sessionHandle, frameIndex);
    }

    public void getTrackingState() {
        ovr_GetTrackingState()
    }
*/

    /**
     * Tells how many tracking devices there are. Tracker indexes start at 0. So the returned number also defines the
     * maximum tracker index which gan be passed to the {@link OvrSessionImpl#getTrackerDescription(int)} method!
     *
     * @return The amount of available tracking devices. May change at runtime! So always call this method before
     * trying to retrieve a trackers state / description.
     */
    public int getTrackerCount() {
        return ovr_GetTrackerCount(sessionHandle);
    }

    /**
     * Returns the description of a specific tracker.
     *
     * @param trackerIndex The index of the tracker to query. Must lie between 0 and the value returned by
     *                     {@link OvrSessionImpl#getTrackerCount()}.
     * @return An {@link OVRTrackerDesc} instance.
     */
    public OVRTrackerDesc getTrackerDescription(final int trackerIndex) {
        if (debugMode) {
            if (trackerIndex < MIN_TRACKER_INDEX || trackerIndex > getTrackerCount()) {
                throw new IllegalArgumentException(
                        "The trackerIndex parameter must lie between 0 and OVRSession#getTrackerCount()!"
                                + " Received: " + trackerIndex
                );
            }
        }
        final OVRTrackerDesc trackerDesc = OVRTrackerDesc.mallocStack();
        ovr_GetTrackerDesc(sessionHandle, trackerIndex, trackerDesc);
        return trackerDesc;
    }

    /**
     * toString() method for OVRTrackerDesc.
     *
     * @param trackerDesc The OVR tracker description to parse.
     * @return The human readable string representation of the provided {@link OVRTrackerDesc} object.
     */
    public static String createTrackerDescriptionString(final @NotNull OVRTrackerDesc trackerDesc) {
        return
                "TrackerDescription [\n" +
                        "\tFrustumHFovInRadians: " + trackerDesc.FrustumHFovInRadians() + '\n' +
                        "\tFrustumVFovInRadians: " + trackerDesc.FrustumVFovInRadians() + '\n' +
                        "\tFrustumNearZInMeters: " + trackerDesc.FrustumNearZInMeters() + '\n' +
                        "\tFrustumFarZInMeters: " + trackerDesc.FrustumFarZInMeters() + '\n' +
                        ']';
    }

    /**
     * Querys the description of the attached HMD device.
     *
     * @return An {@link OVRHmdDesc} instance.
     */
    public OVRHmdDesc getHMDDescription() {
        final OVRHmdDesc hmdDesc = OVRHmdDesc.mallocStack(); // TODO: does mallocStack() work here?
        ovr_GetHmdDesc(sessionHandle, hmdDesc);
        return hmdDesc;
    }

    /**
     * toString() method for OVRHmdDesc.
     *
     * @param hmdDesc The HMD description to parse.
     * @return The human readable string representation of the provided {@link OVRHmdDesc} object.
     */
    public static String createHMDInfoString(final @NotNull OVRHmdDesc hmdDesc) {
        final OVRFovPort.Buffer defaultEyeFov = hmdDesc.DefaultEyeFov();
        final OVRFovPort.Buffer maxEyeFov = hmdDesc.MaxEyeFov();
        final OVRSizei resolution = hmdDesc.Resolution();

        return
                "HMD [\n" +
                        '\t' + "Type: " + getHMDTypeString(hmdDesc) + '\n' +
                        '\t' + "ProductName: " + hmdDesc.ProductNameString() + '\n' +
                        '\t' + "Manufacturer: " + hmdDesc.ManufacturerString() + '\n' +
                        '\t' + "VendorId: " + hmdDesc.VendorId() + '\n' +
                        '\t' + "ProductId: " + hmdDesc.ProductId() + '\n' +
                        '\t' + "SerialNumber: " + hmdDesc.SerialNumberString() + '\n' +
                        '\t' + "FirmwareMajor: " + hmdDesc.FirmwareMajor() + '\n' +
                        '\t' + "FirmwareMinor: " + hmdDesc.FirmwareMinor() + '\n' +
                        '\t' + "AvailableHmdCaps: " + hmdDesc.AvailableHmdCaps() + '\n' +
                        '\t' + "DefaultHmdCaps: " + hmdDesc.DefaultHmdCaps() + '\n' +
                        '\t' + "AvailableTrackingCaps: " + hmdDesc.AvailableTrackingCaps() + '\n' +
                        '\t' + "DefaultTrackingCaps: " + hmdDesc.DefaultTrackingCaps() + '\n' +
                        '\t' + "DefaultEyeFov: " +
                        "Up: " + defaultEyeFov.UpTan() +
                        " Right: " + defaultEyeFov.RightTan() +
                        " Down: " + defaultEyeFov.DownTan() +
                        " Left: " + defaultEyeFov.LeftTan() + '\n' +
                        '\t' + "MaxEyeFov: " +
                        "Up: " + maxEyeFov.UpTan() +
                        " Right: " + maxEyeFov.RightTan() +
                        " Down: " + maxEyeFov.DownTan() +
                        " Left: " + maxEyeFov.LeftTan() + '\n' +
                        '\t' + "Resolution: " +
                        resolution.w() + " x " + resolution.h() + '\n' +
                        '\t' + "DisplayRefreshRate: " + hmdDesc.DisplayRefreshRate() + '\n' +
                        "]";
    }

    /**
     * Converts the HMD-Type-ID into a human readable/understandable string.
     *
     * @param hmdDesc The HMD description to parse.
     * @return The type of the HMD as a string. (e.g. "CV1", which stands for: Consumer Version 1).
     */
    public static String getHMDTypeString(final @NotNull OVRHmdDesc hmdDesc) {
        return switch (hmdDesc.Type()) {
            case ovrHmd_None -> "NONE";
            case ovrHmd_DK1 -> "DK1";
            case ovrHmd_DKHD -> "DKHD";
            case ovrHmd_DK2 -> "DK2";
            case ovrHmd_CB -> "CB";
            case ovrHmd_Other -> "OTHER";
            case ovrHmd_E3_2015 -> "E3_2015";
            case ovrHmd_ES06 -> "ES06";
            case ovrHmd_ES09 -> "ES09";
            case ovrHmd_ES11 -> "ES11";
            case ovrHmd_CV1 -> "CV1";
            default -> "?????";
        };
    }

    /**
     * Returns the status of this session in an {@link OVRSessionStatus} object.
     *
     * @return The session status.
     */
    public OVRSessionStatus getSessionStatus() {
        final OVRSessionStatus sessionStatus = OVRSessionStatus.mallocStack();
        final int getSessionStatusResult = ovr_GetSessionStatus(sessionHandle, sessionStatus);
        if (getSessionStatusResult == ovrSuccess) {
            LOG.info("successful");
        } else {
            final OVRErrorInfo ovrErrorInfo = OVRErrorInfo.mallocStack();
            ovr_GetLastErrorInfo(ovrErrorInfo);
            final String errorMsg = "Unable to query the session status: " +
                    "[ " + ovrErrorInfo.Result() + " ] - " + ovrErrorInfo.ErrorStringString();
            LOG.error(errorMsg);
            throw new RuntimeException(errorMsg);
        }
        return sessionStatus;
    }

    /**
     * toString() method for OVRSessionStatus.
     *
     * @param sessionStatus The session status to parse.
     * @return The human readable string representation of the provided {@link OVRSessionStatus} object.
     */
    public static String createSessionStatusInfoString(final @NotNull OVRSessionStatus sessionStatus) {
        return
                "SessionStatus [\n" +
                        '\t' + "IsVisible: " + sessionStatus.IsVisible() + '\n' +
                        '\t' + "HmdPresent: " + sessionStatus.HmdPresent() + '\n' +
                        '\t' + "DisplayLost: " + sessionStatus.DisplayLost() + '\n' +
                        '\t' + "ShouldQuit: " + sessionStatus.ShouldQuit() + '\n' +
                        '\t' + "ShouldRecenter: " + sessionStatus.ShouldRecenter() + '\n' +
                        '\t' + "HasInputFocus: " + sessionStatus.HasInputFocus() + '\n' +
                        '\t' + "OverlayPresent: " + sessionStatus.OverlayPresent() + '\n' +
                        "]";
    }

    /**
     * Destroys this session.
     * After that all further method calls to this object will throw a {@link UnsupportedOperationException}.
     */
    void destroy() {
        ovr_Destroy(sessionHandle);
        LOG.info("Destroyed session: " + sessionHandle);
    }

    /**
     * Returns the OVR handle of this session. This should be valid until a call to {@link OvrSessionImpl#destroy()}
     * was made.
     *
     * @return The OVR session handle.
     */
    public long getSessionHandle() {
        return sessionHandle;
    }

    public boolean isDebugMode() {
        return debugMode;
    }

    public void setDebugMode(final boolean debugMode) {
        this.debugMode = debugMode;
    }

}
