package de.colibriengine.vr.oculus;

import de.colibriengine.vr.OvrSessionManager;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class OvrSessionManagerImpl implements OvrSessionManager {

    /**
     * Holds the handles of the previously created OVRSession's. Used to destroy them at termination.
     */
    private final ArrayList<OvrSessionImpl> sessions;

    private static final int SESSIONS_INITIAL_CAPACITY = 1;

    public OvrSessionManagerImpl() {
        sessions = new ArrayList<>(SESSIONS_INITIAL_CAPACITY);
    }

    @Override
    public @Nullable OvrSessionImpl createSession() {
        // The library must have been initialized.
        if (OVRLib.isInitialized()) {
            // An HMD must be connected.
            if (OVRLib.isHMDConnected()) {
                try {
                    final OvrSessionImpl session = new OvrSessionImpl();
                    sessions.add(session);
                    return session;
                } catch (final OVRSessionException e) {
                    e.printStackTrace();
                }
            }
        }
        // Return null if one of the conditions is not met or the session creation failed.
        return null;
    }

    /**
     * Destroys all {@link OvrSessionImpl} instances managed by this OVRSessionManager and gets rid of their references.
     *
     * @throws UnsupportedOperationException If the OVR library is currently not initialized.
     */
    public void releaseAll() throws UnsupportedOperationException {
        OVRLib.checkInitialized();

        // Destroy every registered session.
        for (final OvrSessionImpl session : sessions) {
            session.destroy();
        }

        // Clear the list of registered session.
        sessions.clear();
    }

}
